<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {
		$('#btnExcelDownload').click(function() {
			location.href = "/drug/insurance/excel/download?" + window.location.hash.substring(1);
	    });		
		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#searchField").val("");
	    	$("#searchText").val("");
	    	
	    	$("#year").val("${YEAR}");
	    	$("#month").val("${MONTH}");
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		
		var year = GetValue("year");
		var month = GetValue("month");
		
		var param = "page=" + page;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;
		
		param += "&year=" + year;
		param += "&month=" + month;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);		
		   
		   formData.append(key, value);
		}
		
	    var url = "/drug/selectPageInsurance";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	$("#period").html(response.period + " 기간 동안 입고된 약재입니다.");
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr>";
	            			sHTML += "	<td>" + startno + "</td>";
	            			sHTML += "	<td>" + response.list[i].HERBS_NM + "</td>";	            				            			
	            			sHTML += "	<td>" + response.list[i].INSURANCE_CODE1 + "</td>";
	            			sHTML += "	<td>" + response.list[i].INSURANCE_CODE2 + "</td>";
	            			sHTML += "	<td>" + response.list[i].INSURANCE_CODE3 + "</td>";	            			
	            			sHTML += "	<td>" + response.list[i].DRUG_NM2 + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORIGIN + "</td>";
	            			sHTML += "	<td>" + response.list[i].MAKER + "</td>";	            				    
	            			
	            			var price = parseFloat(response.list[i].PRICE);
	            			var spec = parseFloat(response.list[i].SPEC);
	            			var qntt = parseFloat(response.list[i].QNTT);
	            			
	            			var total_qntt = qntt * spec;
	            			total_qntt = parseFloat(total_qntt);
	            			
	            			var gram1 = 0;
	            			if(total_qntt != 0 && price != 0) {
	            				gram1 = price / total_qntt;
	            				
	            				gram1 = parseFloat(gram1);
	            			}	            			
	            			
	            			sHTML += "	<td>" + NumberFormat(total_qntt.toFixed(2)) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(gram1.toFixed(2)) + "원</td>";
	            			sHTML += "	<td>" + NumberFormat(price.toFixed(2)) + "원</td>";
	            			 
	            			sHTML += "</tr>";	            			
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='11'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goView(drug_cd) {
		location.href = "/drug/view/" + drug_cd + "/" + window.location.hash;
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">첩약 약재목록</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c50 sch">
						<p>검색</p>
						<select name="year" id="year" class="select1" style="width:200px">
							<c:forEach var="i" begin="${YEAR - 5}" end="${YEAR + 5}">
								<option value="${i}" <c:if test="${YEAR eq i}">selected</c:if>>${i}년</option>
							</c:forEach>
						</select>
						<select name="month" id="month" class="select1" style="width:150px">
							<option value="01" <c:if test="${MONTH eq '01'}">selected</c:if>>1월</option>
							<option value="02" <c:if test="${MONTH eq '02'}">selected</c:if>>2월</option>
							<option value="03" <c:if test="${MONTH eq '03'}">selected</c:if>>3월</option>
							<option value="04" <c:if test="${MONTH eq '04'}">selected</c:if>>4월</option>
							<option value="05" <c:if test="${MONTH eq '05'}">selected</c:if>>5월</option>
							<option value="06" <c:if test="${MONTH eq '06'}">selected</c:if>>6월</option>
							<option value="07" <c:if test="${MONTH eq '07'}">selected</c:if>>7월</option>
							<option value="08" <c:if test="${MONTH eq '08'}">selected</c:if>>8월</option>
							<option value="09" <c:if test="${MONTH eq '09'}">selected</c:if>>9월</option>
							<option value="10" <c:if test="${MONTH eq '10'}">selected</c:if>>10월</option>
							<option value="11" <c:if test="${MONTH eq '11'}">selected</c:if>>11월</option>
							<option value="12" <c:if test="${MONTH eq '12'}">selected</c:if>>12월</option>								
						</select>	
										
						<select name="searchField" id="searchField" class="select1 wid1">
							<option value="">전체</option>
							<option value="1">본초코드</option>
							<option value="2">본초명</option>
							<option value="3">주성분코드</option>
							<option value="4">대표코드</option>
							<option value="5">제품코드</option>
							<option value="6">약재코드</option>
							<option value="7">약재명</option>
							<option value="8">원산지</option>
							<option value="9">제조사</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력해 주세요.">
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				<p><span id="period"></span></p>
				<div class="rgh">
					<a href="javascript:;" id="btnExcelDownload" class="btn-pk blue2 n"><span>엑셀 다운로드</span></a>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num" />
						<col width="100px" />
						<col width="120px" />
						<col width="120px" />
						<col width="120px" />
						<col />
						<col width="80px" />
						<col />
						<col width="120px" />
						<col width="120px" />
						<col width="120px" />
					</colgroup>
					<thead>
						<tr>
							<th>순번</th>
							<th>본초명</th>
							<th>주성분코드</th>
							<th>대표코드</th>
							<th>제품코드</th>
							<th>약재명</th>
							<th>원산지</th>
							<th>제조사</th>
							<th>총 입고량(g)</th>
							<th>1g당 구입가</th>
							<th>구입가</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>