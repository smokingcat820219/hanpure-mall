<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#searchField").val("");
	    	$("#searchText").val("");
	    	
	    	$("#HABITUDE").val("");
	    	$("#TEMPER").val("");
	    	$("#ORGAN").val("");
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		var HABITUDE = GetValue("HABITUDE");
		var TEMPER = GetValue("TEMPER");
		var ORGAN = GetValue("ORGAN");
		
		var param = "page=" + page;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;
		param += "&HABITUDE=" + HABITUDE;
		param += "&TEMPER=" + TEMPER;
		param += "&ORGAN=" + ORGAN;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);		
		   
		   formData.append(key, value);
		}
		
	    var url = "/drug/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr onClick=\"goView('" + response.list[i].DRUG_CD + "')\">";
	            			
	            			sHTML += "	<td>" + startno + "</td>";
	            			
	            			if(response.list[i].FILEPATH) {
	            				sHTML += "	<td><img src='/download?filepath=" + response.list[i].FILEPATH + "&filename=" + response.list[i].FILENAME + "' alt='사진'></td>";	
	            			}
	            			else {
	            				sHTML += "	<td><img src='/images/common/img_noimg.jpg' alt='사진'></td>";	
	            			}	            			
	            			
	            			sHTML += "	<td>" + response.list[i].HERBS_CD + "</td>";
	            			sHTML += "	<td>" + response.list[i].HERBS_NM + "(黃芪)</td>";
	            			sHTML += "	<td>" + response.list[i].DRUG_CD + "</td>";
	            			sHTML += "	<td>" + response.list[i].DRUG_NM + "</td>";	  
	            			sHTML += "	<td>" + response.list[i].HABITUDE + "</td>";
	            			sHTML += "	<td>" + response.list[i].TEMPER + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORGAN + "</td>";	  
	            			sHTML += "	<td>" + response.list[i].NICK_NM + "</td>";
	            			sHTML += "</tr>";	            			
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='10'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goView(drug_cd) {
		location.href = "/drug/view/" + drug_cd + "/" + window.location.hash;
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">약재목록</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c10">
						<p>성</p>
						<select name="HABITUDE" id="HABITUDE" class="select1">
							<option value="">전체</option>
							<c:forEach var="i" begin="1" end="${LIST_HABITUDE_TP.size()}">
								<option value="${LIST_HABITUDE_TP.get(i - 1).SUB_NM_KR}">${LIST_HABITUDE_TP.get(i - 1).SUB_NM_KR}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col c10">
						<p>미</p>
						<select name="TEMPER" id="TEMPER" class="select1">
							<option value="">전체</option>
							<c:forEach var="i" begin="1" end="${LIST_TEMPER_TP.size()}">
								<option value="${LIST_TEMPER_TP.get(i - 1).SUB_NM_KR}">${LIST_TEMPER_TP.get(i - 1).SUB_NM_KR}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col c10">
						<p>귀경</p>
						<select name="ORGAN" id="ORGAN" class="select1">
							<option value="">전체</option>
							<c:forEach var="i" begin="1" end="${LIST_ORGAN_TP.size()}">
								<option value="${LIST_ORGAN_TP.get(i - 1).SUB_NM_KR}">${LIST_ORGAN_TP.get(i - 1).SUB_NM_KR}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col c50 sch">
						<p>검색</p>
						<select name="searchField" id="searchField" class="select1 wid1">
							<option value="">전체</option>
							<option value="1">본초코드</option>
							<option value="2">본초명</option>
							<option value="3">약재코드</option>
							<option value="4">약재명</option>
						</select>
						<input type="text" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력해 주세요.">
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num">
						<col class="img">
						<col class="name">
						<col class="code">
						<col class="day2">
						<col class="day2">
						<col class="name">
						<col class="code">
						<col class="code">
						<col>
					</colgroup>
					<thead>
						<tr>
							<th>순번</th>
							<th>이미지</th>
							<th>본초코드</th>
							<th>본초명</th>
							<th>약재코드</th>
							<th>약재명</th>
							<th>성(性)</th>
							<th>미</th>
							<th>귀경(歸經)</th>
							<th>별칭</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>