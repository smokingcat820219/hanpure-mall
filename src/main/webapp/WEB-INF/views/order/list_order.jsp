<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }
	    
	    $('#btnMypage').click(function() {
	    	parent.location.href = "https://hanpuremall.co.kr/bbs/login.php?url=https%3A%2F%2Fhanpuremall.co.kr%2Fshop%2Fmypage.php";
	    });
	    
	    $('#btnExcelDownload').click(function() {
	    	location.href = "/order/excel/download?" + window.location.hash.substring(1);
	    });
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnCancel').click(function() {
	    	goCancel();
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#ORDER_TP").val("");
			
	    	$("#select_date1").val("");
	    	$("#select_date2").val("");
	    	
	    	$("#sdate1").val("");
	    	$("#edate1").val("");
	    	
	    	$("#sdate2").val("");
	    	$("#edate2").val("");
	    	
	    	$(".payment_status").prop("checked", false);
	    	$(".order_status").prop("checked", false);
			
			$("#DELIVERY_NM").val("");			
			
	    	$("#searchField").val("");
			$("#searchText").val("");			
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var itemPerPage = GetValue("itemPerPage");
		var order_tp = GetValue("ORDER_TP");
		
		var order_status = "";
		$("input:checkbox[name='order_status']:checked").each(function() {
			var value = $(this).val();
			if(value) {
				if(order_status != "") order_status += ",";
				order_status += value;
			}			
		});		
		
		var payment_status = "";
		$("input:checkbox[name='payment_status']:checked").each(function() {
			var value = $(this).val();
			if(value) {
				if(payment_status != "") payment_status += ",";
				payment_status += value;
			}			
		});		
		
		var DELIVERY_NM = GetValue("DELIVERY_NM");		
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		var sdate1 = GetValue("sdate1");
		var edate1 = GetValue("edate1");
		var sdate2 = GetValue("sdate2");
		var edate2 = GetValue("edate2");
				
		var param = "page=" + page;
		param += "&itemPerPage=" + itemPerPage;
		param += "&ORDER_TP=" + order_tp;
		param += "&LIST_ORDER_STATUS=" + order_status;
		param += "&LIST_PAYMENT_STATUS=" + payment_status;
		param += "&DELIVERY_NM=" + DELIVERY_NM;
		param += "&sdate1=" + sdate1;
		param += "&edate1=" + edate1;
		param += "&sdate2=" + sdate2;
		param += "&edate2=" + edate2;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/order/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr>";
	            			sHTML += "	<td><label class='inp_radio'><input type='radio' name='chk' class='chk' value='" + response.list[i].ORDER_SEQ + "' order_tp='" + response.list[i].ORDER_TP + "' /><span class='d'></span></label></td>";
	            			sHTML += "	<td>" + startno + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORDER_TP_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].DOCTOR + "</td>";
	            			sHTML += "	<td><a href='javascript:' onClick=\"showOrder('" + response.list[i].ORDER_SEQ + "')\" style='text-decoration:underline'>" + response.list[i].ORDER_NM + "</a></td>";
	            			sHTML += "	<td><a href='javascript:' onClick=\"showPatient('" + response.list[i].PATIENT_SEQ + "')\" style='text-decoration:underline'>" + response.list[i].PATIENT_NM + "</a></td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_AMOUNT) + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORDER_DT + "</td>";	            			
	            			
	            			if(response.list[i].PAYMENT_STATUS == "Y") {
	            				sHTML += "	<td>" + "완료" + "</td>";	
	            			}
	            			else {
	            				sHTML += "	<td style='color:blue'>" + "대기" + "</td>";
	            			}
	            			
	            			var css = "style='color:red'";
	            			
	            			if(response.list[i].ORDER_STATUS == "Y") {
	            				console.log("response.list[i].POP", response.list[i].POP);
	            				if(response.list[i].POP == "007") {
	            					sHTML += "	<td " + css + ">" + "완료" + "</td>";	
	            				}
	            				else {
	            					sHTML += "	<td " + css + ">" + "조제중" + "</td>";	
	            				}
	            			}
							else {
								sHTML += "	<td>" + response.list[i].ORDER_STATUS_NM + "</td>";	
							}
	            				            			
	            			sHTML += "	<td>" + response.list[i].DELIVERY_DT + "</td>";
	            			sHTML += "	<td><a href='javascript:' onClick=\"showSender('" + response.list[i].ORDER_SEQ + "')\" style='text-decoration:underline'>" + response.list[i].SEND_NM + "</a></td>";
	            			sHTML += "	<td><a href='javascript:' onClick=\"showRecver('" + response.list[i].ORDER_SEQ + "')\" style='text-decoration:underline'>" + response.list[i].RECV_NM + "</a></td>";
	            			sHTML += "	<td>" + response.list[i].DELIVERY_NM + "</td>";
	            			
	            			if(response.list[i].DELIVERY_CD) {
	            				if(response.list[i].DELIVERY_NM == "CJ택배") {
	            					sHTML += "	<td><a href='https://www.doortodoor.co.kr/parcel/pa_004.jsp' target='_blank' style='text-decoration:underline'>" + response.list[i].DELIVERY_CD + "</a></td>";
	            				}
	            				else if(response.list[i].DELIVERY_NM == "한의사랑") {
	            					sHTML += "	<td><a href='https://hanips.com/delivery/hpldelivery.php' target='_blank' style='text-decoration:underline'>" + response.list[i].DELIVERY_CD + "</a></td>";
	            				}
	            				else {
	            					sHTML += "	<td>" + response.list[i].DELIVERY_CD + "</td>";
	            				}
	            			}
	            			else {
	            				sHTML += "	<td>" + response.list[i].DELIVERY_CD + "</td>";
	            			}
	            			
	            			
	            			
	            			//https://hanips.com/delivery/result.php?logicnum=13123132131
	            					
	            			sHTML += "</tr>";
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='15'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goUpdate(recipe_seq) {
		location.href = "/recipe/update/" + recipe_seq + "/" + window.location.hash;
	}
	
	function allCheck(obj) {
		if($(obj).prop("checked")) {
			$(".chk").prop("checked", true);
		}
		else {
			$(".chk").prop("checked", false);
		}
	}
	
	function showOrder(order_seq) {		
		showPopup(800, 800, "/common/pop_order_info?ORDER_SEQ=" + order_seq);
	}
	
	function showSender(order_seq) {		
		showPopup(800, 500, "/common/pop_sender?ORDER_SEQ=" + order_seq);
	}
	
	function showRecver(order_seq) {		
		showPopup(800, 500, "/common/pop_recver?ORDER_SEQ=" + order_seq);
	}
	
	function showPatient(patient_seq) {
		if(!patient_seq || patient_seq == 0 || patient_seq == "0") {
			return;
		}
		
		showPopup(500, 620, "/common/pop_patient_info?PATIENT_SEQ=" + patient_seq);
	}
	
	function setDate(type, period) {
		var today = getToday();
		if(period != "") {
			$("#sdate" + type).val(getPreMonth(today, parseInt(period)));
			$("#edate" + type).val(today);
		}
		else {
			$("#sdate" + type).val("");
			$("#edate" + type).val("");
		}
	}
	
	function goPrint(type) {
		var order_seq = GetRadioValue("chk");
		if(!order_seq) {
			alert("출력할 주문내역을 선택해 주세요.");
		}
		else {
			if(type == "1") {
				showPopup('800', '800', "/order/print?ORDER_SEQ=" + order_seq);
			}
			else if(type == "2") {
				var order_tp = $(":radio[name='chk']:checked").attr("order_tp");
				if(order_tp != "004") {
					alert("첩약처방만 출력이 가능합니다.");
					return;
				}
				
				showPopup('800', '800', "/order/insurance_print?ORDER_SEQ=" + order_seq);
			}
			else if(type == "3") {
				var order_tp = $(":radio[name='chk']:checked").attr("order_tp");
				if(order_tp != "004") {
					alert("첩약처방만 출력이 가능합니다.");
					return;
				}
				
				showPopup('800', '800', "/order/insurance_print2?ORDER_SEQ=" + order_seq);
			}
			else {
				alert("잘못된 선택힙니다.");
			}
		}
		
		$("#btnPrint").trigger("click");
	}
	
	function goCancel() {
		if(ajaxRunning) return;
		
		var order_seq = GetRadioValue("chk");
		if(!order_seq) {
			alert("취소 할 주문내역을 선택해 주세요.");
			return;
		}
		
		if(!confirm("선택한 주문내역을 취소 하시겠습니까?")) {
			return;
		}
		
		var formData = new FormData();
		formData.append("ORDER_SEQ", order_seq);         
		
	    var url = "/order/cancel";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {   
	            	alert("취소 되었습니다.");
	            	
	            	goPage();
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<div class="tbl_box_sch etc1">
				<div class="in">
					<div class="col">
						<p>처방 구분</p>
						<select name="ORDER_TP" id="ORDER_TP" class="select1">
							<option value="">전체</option>
							
							<c:forEach var="i" begin="1" end="${LIST_ORDER_TP.size()}">
								<c:if test="${LIST_ORDER_TP.get(i - 1).SUB_CD ne '007'}">
									<option value="${LIST_ORDER_TP.get(i - 1).SUB_CD}">${LIST_ORDER_TP.get(i - 1).SUB_NM_KR}</option>
								</c:if>
							</c:forEach>							
						</select>
						<select name="searchField" id="searchField" class="select1">
							<option value="">전체</option>
							<option value="1">처방의</option>
							<option value="2">처방명/상품명</option>
							<option value="3">환자명</option>
							<option value="4">보내는사람</option>
							<option value="5">받는사람</option>
							<option value="6">운송장번호</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력해주세요." />
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
					<div class="col">
						<p>주문 상태</p>
						
						<c:forEach var="i" begin="1" end="${LIST_ORDER_STATUS_TP.size()}">							
							<label class="inp_checkbox mr20"><input type="checkbox" name="order_status" class="order_status" value="${LIST_ORDER_STATUS_TP.get(i - 1).SUB_CD}" /><span>${LIST_ORDER_STATUS_TP.get(i - 1).SUB_NM_KR}</span></label>
						</c:forEach>	
					</div>
				</div>
				<div class="in">
					<div class="col c45">
						<p>주문 일자</p>
						<div class="inp_cal">
							<input type="text" name="sdate1" id="sdate1" class="inp_txt calender datepicker_first date" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate1" id="edate1" class="inp_txt calender datepicker_last date" readOnly placeholder="yyyy-mm-dd" />
						</div>
						<select class="select1" id="select_date1" onChange="setDate('1', this.value)">
							<option value="">전체</option>
							<option value="-3">3개월</option>
							<option value="-6">6개월</option>
							<option value="-9">9개월</option>
							<option value="-12">12개월</option>
						</select>
					</div>
					<div class="col">
						<p>배송요청일</p>
						<div class="inp_cal">
							<input type="text" name="sdate2" id="sdate2" class="inp_txt calender datepicker_first date" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate2" id="edate2" class="inp_txt calender datepicker_last date" readOnly placeholder="yyyy-mm-dd" />
						</div>
						<select class="select1" id="select_date2" onChange="setDate('2', this.value)">
							<option value="">전체</option>
							<option value="-3">3개월</option>
							<option value="-6">6개월</option>
							<option value="-9">9개월</option>
							<option value="-12">12개월</option>
						</select>
					</div>
				</div>
				<div class="in">
					<div class="col c25">
						<p>결제</p>
						<label class="inp_checkbox mr20"><input type="checkbox" name="payment_status" class="payment_status" value="N" /><span>결제대기</span></label>
						<label class="inp_checkbox"><input type="checkbox" name="payment_status" class="payment_status" value="Y" /><span>결제완료</span></label>
					</div>
					<div class="col c25">
						<p>택배사</p>
						<select name="DELIVERY_NM" id="DELIVERY_NM" class="select1">
							<option value="">전체</option>
							<option value="한의사랑">한의사랑</option>
							<option value="CJ택배">CJ택배</option>
						</select>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">2</strong>건</p>
				<select name="itemPerPage" id="itemPerPage" class="select1">
					<option value="10">10개씩 보기</option>
					<option value="20">20개씩 보기</option>
					<option value="30">30개씩 보기</option>
					<option value="40">40개씩 보기</option>
					<option value="50">50개씩 보기</option>
				</select>
				<div class="rgh">
					<a href="javascript:" id="btnMypage" class="btn-pk red n"><span>결제페이지</span></a>
					<a href="javascript:" id="btnCancel" class="btn-pk blue n"><span>주문취소</span></a>
					<div class="box_drop">
						<a href="#" id="btnPrint" class="btn-pk blue2 n bt"><span>인쇄하기</span></a>
						<div class="list">
							<ul>
								<li><a href="javascript:" onClick="goPrint('1')">처방전출력</a></li>
								<li><a href="javascript:" onClick="goPrint('2')">첩약보험(처방전)</a></li>
								<li><a href="javascript:" onClick="goPrint('3')">첩약보험(내역안내)</a></li>
							</ul>
						</div>
					</div>
					<a href="javascript:" id="btnExcelDownload" class="btn-pk gray2 n"><span>엑셀 다운로드</span></a>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num">
						<col class="num">
						<col class="size1">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col class="size3">
						<col class="size1">
						<col>
						<col>
						<col>
						<col class="size1">
						<col class="td1">
					</colgroup>
					<thead>
						<tr>
							<th></th>
							<th>순번</th>
							<th>구분</th>
							<th>처방의</th>
							<th>처방명/상품명</th>
							<th>환자명</th>
							<th>주문금액</th>
							<th>주문일시</th>
							<th>결제</th>
							<th>주문상태</th>
							<th>배송요청일</th>
							<th>보내는<br>사람</th>
							<th>받는<br>사람</th>
							<th>택배사</th>
							<th>운송장번호</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="pagenation">
				<ul>
					
				</ul>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>