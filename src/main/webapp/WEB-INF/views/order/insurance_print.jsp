<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="mask" uri="/WEB-INF/tld/TagLibMasking.tld" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="Location"		content="KR" />
<meta name="subject"		content="한퓨어" />
<meta name="title"			content="한퓨어" />
<meta name="copyright"		content="© 한퓨어대한녹용. All rights reserved." />
<meta name="keywords"		content="한약재,한약재제조,GMP녹용,녹용,녹용수입,녹용제조,녹용쇼핑몰,대한녹용" />
<meta name="description" 	content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE" />
<meta name="distribution"	content="Global" />
<meta name="publisher"		content="한퓨어대한녹용" />
<meta name="author"		content="http://zeronara.net" />
<meta name="robots" 		content="index,follow" />
<!-- 미투데이 -->
<meta property="me2:post_body"      content="한퓨어" />
<meta property="me2:post_tag"       content="한약재,한약재제조,GMP녹용,녹용,녹용수입,녹용제조,녹용쇼핑몰,대한녹용,https://hanpuremall.co.kr/shop/" />
<meta property="me2:image"          content="https://hanpuremall.co.kr/img/share.png" />
<!-- Google -->
<meta itemprop="name"				content="한퓨어" />
<meta itemprop="description"		content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE" />
<meta itemprop="image"				content="https://hanpuremall.co.kr/img/share.png" />

<meta property="og:type" content="website">
<meta property="og:title" content="한퓨어">
<meta property="og:description" content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE">
<meta property="og:image" content="https://hanpuremall.co.kr/img/share.png">
<meta property="og:url" content="https://hanpuremall.co.kr/shop/">

<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="한퓨어">
<meta name="twitter:description" content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE">
<meta name="twitter:image" content="https://hanpuremall.co.kr/img/share.png">
<meta name="twitter:domain" content="한퓨어">
<title>한퓨어</title>

<!-- link -->
<link rel="canonical" href=""/>
<link rel="shortcut icon" href="/images/favicon.ico">

<script src="/js/jquery-1.12.4.min.js"></script>
</head>
<body>


<style>
* {margin: 0; padding: 0; box-sizing:border-box;}
table {border-collapse:collapse; border-spacing:0; width:100%; table-layout:fixed;}
caption {display:none;}
caption, th, td {vertical-align:middle;}

.paper {font-size: 13px; color: #000; line-height: 1.2; font-family: 'Malgun Gothic', '맑은 고딕', '돋움', sans-serif;}
.wrap_print {
	border-radius: 0;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	position:relative; display:block; 
	background-color: #fff;
	max-width:800px;
	margin: 0 auto;
	padding: 30px;
}
.wrap_print > p {font-size: 14px; margin-bottom: 5px;}
.wrap_print .inner {}
.wrap_print .titbox {text-align: center; margin-bottom: 5px;}
.wrap_print .titbox td {padding: 10px; text-align: center; font-weight: 700; font-size: 20px;}

.wrap_print td,
.wrap_print th {height: 24px; padding: 0 3px; text-align: left; border: 1px solid #000; font-size: 13px; line-height: 1.2;}
.wrap_print thead th {text-align: center;}
.wrap_print td {height: auto; padding: 5px 3px;}
.wrap_print .bdb {margin-bottom: 5px;}
.wrap_print .bind {margin-bottom: 5px; display: flex; flex-wrap: wrap;}
.wrap_print .bind table {flex:1;}
.wrap_print .bind table.mr {margin-right: 0.8%;}
.wrap_print .bind > div {width: 28%;}
.wrap_print .bind > div:first-child {width: 71%; margin-right: 1%;}
.wrap_print .bind .b {position: relative; top: -1px; z-index: 1; width: 100%; padding: 8px 10px; clear: both; border: 1px solid #000;}
.wrap_print .bind .b span {display: inline-block; vertical-align: top; margin-left: 20px;}
.wrap_print .bind .b span:first-child {margin-left: 0;}

.btn-bot {text-align: center; margin-top: 20px; padding-bottom: 20px;}
.btn-bot .btn {display: inline-block; vertical-align: top; padding: 9px 10px; min-width:100px; font-size: 14px; color: #fff; text-align: center; text-decoration: none;}
.btn-bot .btn.ty1 {background-color: #085d9d;}
.btn-bot .btn.ty2 {background-color: #085d9d;}


.ta-c {text-align: center !important;}




@page {
	size: A4;
	margin: 0;
}
@media print {
	html, body {
		width: 210mm;
		height: 290mm;
		background: #fff;
	}
	body {-webkit-print-color-adjust: exact;}
	.wrap_print {
		overflow:hidden;
		margin: 0;
		border: initial;
		border-radius: initial;
		width: initial;
		min-height: initial;
		box-shadow: initial;
		background: initial;
		page-break-after: always;
		background-color: #fff;
	}

	.wrap_print {
		width: 210mm;
		min-height: 280mm;
		margin: 0mm auto;
		position:relative; display:block; 
		page-break-after:always;
		background-color: #fff;
	}
	.wrap_print .inner {
		padding:0;
	}
}


</style>



<div id="print" class="paper">
	<section class="wrap_print">
		<p>#.${ORDER.ORDER_SEQ}</p>
		<div class="inner">
			<header class="titbox">
				<h1>첩약 처방 · 조제내역 안내 [건강보험]</h1>
			</header>
	
			<table class="bdb">
				<colgroup>
					<col style="width: 14%;">
					<col style="width: 5%;">
					<col style="width: 8%;">
					<col style="width: 23%;">
					<col style="width: 14%;">
					<col style="width: 10%;">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th colspan="2">요양기관기호</th>
						<td colspan="5"></td>
					</tr>
					<tr>
						<th colspan="2">발급연월일 및 기호</th>
						<td colspan="2">2021년 12월 27일 - 제    호</td>
						<th rowspan="3">의료기관</th>
						<th>명칭</th>
						<td>${ORDER.HOSPITAL_NM}</td>
					</tr>
					<tr>
						<th rowspan="2">환자</th>
						<th colspan="2">성명</th>
						<td>${ORDER.PATIENT_NM}</td>
						<th>전화번호</th>
						<td>${ORDER.HOSPITAL_MOBILE}</td>
					</tr>
					<tr>
						<th colspan="2">주민등록번호</th>
						<td></td>
						<th>팩스번호</th>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table class="bdb">
				<colgroup>
					<col style="width: 14%;">
					<col style="width: 22%;">
					<col style="width: 14%;">
					<col style="width: 24%;">
					<col style="width: 10%;">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th rowspan="2">질병 <br>분류기호</th>
						<td rowspan="2"></td>
						<th rowspan="2">처방 <br>의료인 성명</th>
						<td rowspan="2">${ORDER.DOCTOR}<p style="text-align: right;">(서명 또는 날인)</p></td>
						<th>면허종류</th>
						<td>한의사</td>
					</tr>
					<tr>
						<th>면허번호</th>
						<td>제      호</td>
					</tr>
				</tbody>
			</table>
			<table class="bdb">
				<colgroup>
					<col style="width: 14%;">
					<col style="width: 10%;">
					<col style="width: 24%;">
					<col style="width: 10%;">
					<col style="width: 16%;">
					<col style="width: 10%;">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>조제탕전기관</th>
						<th>기관명</th>
						<td>한퓨어바른원외탕전</td>
						<th>기관기호</th>
						<td>11957221</td>
						<th>기관전화</th>
						<td>1544-7745</td>
					</tr>
					<tr>
						<th>기관 소재지</th>
						<td colspan="6"></td>
					</tr>
				</tbody>
			</table>
			<table class="bdb">
				<colgroup>
					<col style="width: 24%;">
					<col style="width: 24%;">
					<col style="width: 26%;">
					<col>
				</colgroup>
				<thead>
					<tr>
						<th>1회 복용 팩수</th>
						<th>1일 복용 팩수</th>
						<th>총 투약일수</th>
						<th>용법</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="ta-c">1</td>
						<td class="ta-c">6</td>
						<td class="ta-c">10</td>
						<td>
							매 식(전, 간, 후)<br>
							시 분 복용
						</td>
					</tr>
				</tbody>
			</table>
			<div class="het">
				<table class="bdb">
					<colgroup>
						<col>
						<col>
						<col>
						<col style="width: 20%;">
					</colgroup>
					<thead>
						<tr>
							<th colspan="3">한약재</th>
							<th rowspan="2">본인부담률<br>(50% 또는 100%)</th>
						</tr>
						<tr>
							<th>명칭</th>
							<th>코드</th>
							<th>원산지</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${LIST_RECIPE.size() > 0}">
							<tr>
								<td>${LIST_RECIPE.get(0).DRUG_NM}</td>
								<td></td>
								<td>${LIST_RECIPE.get(0).ORIGIN}</td>
								<td rowspan="${LIST_RECIPE.size()}"></td>
							</tr>
						
							<c:if test="${LIST_RECIPE.size() > 1}">							
								<c:forEach var="i" begin="1" end="${LIST_RECIPE.size() - 1}">							
									<tr>
										<td>${LIST_RECIPE.get(i).DRUG_NM}</td>
										<td></td>
										<td>${LIST_RECIPE.get(i).ORIGIN}</td>
									</tr>
								</c:forEach>
							</c:if>							
						</c:if>
					</tbody>
				</table>
			</div>
			<br/>
			<br/>
			<br/>			
			<table class="">
				<colgroup>
					<col style="width: 12%;">
					<col style="width: 20%;">
					<col style="width: 15%;">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>총 비용</th>
						<td><mask:display type="NUMBER" value="${ORDER.ORDER_AMOUNT}" />원</td>
						<th>환자부담비용</th>
						<td></td>
					</tr>
					<tr>
						<th>약재보관방법</th>
						<td colspan="3"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
</div>
<div class="btn-bot">
	<a href="javascript:" class="btn ty1 b-close">닫기</a>
	<a href="javascript:" id="btnPrint" class="btn ty2">출력</a>
</div>

<script type="text/javascript" src="/js/printThis.js"></script>
<script type="text/javascript">
$(function() {
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
	
	$('#btnPrint').click(function() {
		$("#print").printThis({
			debug : false,
			importCSS : false,
			importStyle : true,
			removeInline: false, 
			printContainer : true,
			//loadCss: "../css/print.css",
			pageTitle: "",
			removeInline : false
		});
    });
	
	$("#print").printThis({
		debug : false,
		importCSS : false,
		importStyle : true,
		removeInline: false, 
		printContainer : true,
		//loadCss: "../css/print.css",
		pageTitle: "",
		removeInline : false
	});
});
</script>


</body>
</html>