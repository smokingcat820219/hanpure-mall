<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="mask" uri="/WEB-INF/tld/TagLibMasking.tld" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="Location"		content="KR" />
<meta name="subject"		content="한퓨어" />
<meta name="title"			content="한퓨어" />
<meta name="copyright"		content="© 한퓨어대한녹용. All rights reserved." />
<meta name="keywords"		content="한약재,한약재제조,GMP녹용,녹용,녹용수입,녹용제조,녹용쇼핑몰,대한녹용" />
<meta name="description" 	content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE" />
<meta name="distribution"	content="Global" />
<meta name="publisher"		content="한퓨어대한녹용" />
<meta name="author"		content="http://zeronara.net" />
<meta name="robots" 		content="index,follow" />
<!-- 미투데이 -->
<meta property="me2:post_body"      content="한퓨어" />
<meta property="me2:post_tag"       content="한약재,한약재제조,GMP녹용,녹용,녹용수입,녹용제조,녹용쇼핑몰,대한녹용,https://hanpuremall.co.kr/shop/" />
<meta property="me2:image"          content="https://hanpuremall.co.kr/img/share.png" />
<!-- Google -->
<meta itemprop="name"				content="한퓨어" />
<meta itemprop="description"		content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE" />
<meta itemprop="image"				content="https://hanpuremall.co.kr/img/share.png" />

<meta property="og:type" content="website">
<meta property="og:title" content="한퓨어">
<meta property="og:description" content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE">
<meta property="og:image" content="https://hanpuremall.co.kr/img/share.png">
<meta property="og:url" content="https://hanpuremall.co.kr/shop/">

<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="한퓨어">
<meta name="twitter:description" content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE">
<meta name="twitter:image" content="https://hanpuremall.co.kr/img/share.png">
<meta name="twitter:domain" content="한퓨어">
<title>한퓨어</title>

<!-- link -->
<link rel="canonical" href=""/>
<link rel="shortcut icon" href="/images/favicon.ico">

<script src="/js/jquery-1.12.4.min.js"></script>
</head>
<body>


<style>
* {margin: 0; padding: 0; box-sizing:border-box;}
table {border-collapse:collapse; border-spacing:0; width:100%; table-layout:fixed;}
caption {display:none;}
caption, th, td {vertical-align:middle;}

.paper {font-size: 13px; color: #000; line-height: 1.2; font-family: 'Malgun Gothic', '맑은 고딕', '돋움', sans-serif;}
.wrap_print {
	border-radius: 0;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	position:relative; display:block; 
	background-color: #fff;
	max-width:800px;
	margin: 0 auto;
	padding: 30px;
}
.wrap_print > p {font-size: 14px; margin-bottom: 5px;}
.wrap_print .inner {}
.wrap_print .titbox {text-align: center; margin-bottom: 5px;}
.wrap_print .titbox td {padding: 10px; text-align: center; font-weight: 700; font-size: 20px;}

.wrap_print td,
.wrap_print th {height: 24px; padding: 0 3px; text-align: left; border: 1px solid #000; font-size: 13px; line-height: 1.2;}
.wrap_print thead th {text-align: center;}
.wrap_print td {height: auto; padding: 5px 3px;}
.wrap_print .bdb {margin-bottom: 5px;}
.wrap_print .het {height: calc(100vh - 455px);}
.wrap_print .bind {margin-bottom: 5px; display: flex; flex-wrap: wrap;}
.wrap_print .bind table {flex:1;}
.wrap_print .bind table.mr {margin-right: 0.8%;}
.wrap_print .bind > div {width: 28%;}
.wrap_print .bind > div:first-child {width: 71%; margin-right: 1%;}
.wrap_print .bind .b {position: relative; top: -1px; z-index: 1; width: 100%; padding: 8px 10px; clear: both; border: 1px solid #000;}
.wrap_print .bind .b span {display: inline-block; vertical-align: top; margin-left: 20px;}
.wrap_print .bind .b span:first-child {margin-left: 0;}

.btn-bot {text-align: center; margin-top: 20px; padding-bottom: 20px;}
.btn-bot .btn {display: inline-block; vertical-align: top; padding: 9px 10px; min-width:100px; font-size: 14px; color: #fff; text-align: center; text-decoration: none;}
.btn-bot .btn.ty1 {background-color: #085d9d;}
.btn-bot .btn.ty2 {background-color: #085d9d;}


.ta-c {text-align: center !important;}




@page {
	size: A4;
	margin: 0;
}
@media print {
	html, body {
		width: 210mm;
		height: 290mm;
		background: #fff;
	}
	body {-webkit-print-color-adjust: exact;}
	.wrap_print {
		overflow:hidden;
		margin: 0;
		border: initial;
		border-radius: initial;
		width: initial;
		min-height: initial;
		box-shadow: initial;
		background: initial;
		page-break-after: always;
		background-color: #fff;
	}

	.wrap_print {
		width: 210mm;
		min-height: 280mm;
		margin: 0mm auto;
		position:relative; display:block; 
		page-break-after:always;
		background-color: #fff;
	}
	.wrap_print .inner {
		padding:0;
	}
}


</style>



<div id="print" class="paper">
	<section class="wrap_print">
		<div class="img"><img src="${BARCODE}" alt="바코드"></div>
		<div class="inner">
			<header class="titbox">
				<table>
					<colgroup>
						<col style="width: 20%;">
						<col>
					</colgroup>
					<tbody>
						<tr>
							<td>송장출력</td>
							<td>${ORDER.ORDER_NM}</td>
						</tr>
					</tbody>
				</table>
			</header>
	
			<table class="bdb">
				<colgroup>
					<col style="width: 13%;">
					<col style="width: 20%;">
					<col style="width: 13%;">
					<col style="width: 20%;">
					<col style="width: 13%;">
					<col style="width: 20%;">
				</colgroup>
				<tbody>
					<tr>
						<th style="text-align:center;">주문번호</th>
						<td style="text-align:center;">${ORDER.ORDER_CD}</td>
						<th style="text-align:center;">한의원명</th>
						<td style="text-align:center;">${ORDER.HOSPITAL_NM}</td>
						<th style="text-align:center;">주문일자</th>
						<td style="text-align:center;"><mask:display type="DATE10" value="${ORDER.CREATED_AT}" /></td>
					</tr>
					<tr style="padding: 0px 0px">
						<td colspan="6" style="border:0px; padding: 0px 0px">
							<table class="">
								<colgroup>
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 15%;">
								</colgroup>
								<tbody>
									<tr>
										<th style="text-align:center; border-right:0px; border-top:0px;">생년월일</th>
										<td style="text-align:center; border-right:0px; border-top:0px;">${ORDER.PATIENT_BIRTH}</td>
										<th style="text-align:center; border-right:0px; border-top:0px;">환자명</th>
										<td style="text-align:center; border-right:0px; border-top:0px;">${ORDER.PATIENT_NM}</td>
										<th style="text-align:center; border-right:0px; border-top:0px;">성별</th>
										<td style="text-align:center; border-right:0px; border-top:0px;">${ORDER.PATIENT_SEX}</td>
										<th style="text-align:center; border-right:0px; border-top:0px;">발송요청일</th>
										<td style="text-align:center; border-top:0px;">${ORDER.DELIVERY_DT}</td>										
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="bind">
				<table class="mr">
					<colgroup>
						<col style="width: 30px;">
						<col>
						<col style="width: 70px;">
					</colgroup>
					<thead>
						<tr>
							<th colspan="2">품목</th>
							<th>용량</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" begin="1" end="11">
							<tr <c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1' or LIST_RECIPE.get(i - 1).TURN eq '3'}">style="background-color:yellow"</c:if>>
								<td class="ta-c">
									<c:choose>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">선</c:when>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">일</c:when>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">후</c:when>
										<c:otherwise>&nbsp;</c:otherwise>
									</c:choose>
								</td>
								<td class="ta-l">${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
								<td class="ta-c">${LIST_RECIPE.get(i - 1).TOTAL_QNTT}</td>
							</tr>	
						</c:forEach>
					</tbody>
				</table>
				<table class="mr">
					<colgroup>
						<col style="width: 30px;">
						<col>
						<col style="width: 70px;">
					</colgroup>
					<thead>
						<tr>
							<th colspan="2">품목</th>
							<th>용량</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" begin="12" end="22">
							<tr <c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1' or LIST_RECIPE.get(i - 1).TURN eq '3'}">style="background-color:yellow"</c:if>>
								<td class="ta-c">
									<c:choose>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">선</c:when>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">일</c:when>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">후</c:when>
										<c:otherwise>&nbsp;</c:otherwise>
									</c:choose>
								</td>
								<td class="ta-l">${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
								<td class="ta-c">${LIST_RECIPE.get(i - 1).TOTAL_QNTT}</td>
							</tr>	
						</c:forEach>
					</tbody>
				</table>
				<table>
					<colgroup>
						<col style="width: 30px;">
						<col>
						<col style="width: 70px;">
					</colgroup>
					<thead>
						<tr>
							<th colspan="2">품목</th>
							<th>용량</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" begin="23" end="33">
							<tr <c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1' or LIST_RECIPE.get(i - 1).TURN eq '3'}">style="background-color:yellow"</c:if>>
								<td class="ta-c">
									<c:choose>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">선</c:when>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">일</c:when>
										<c:when test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">후</c:when>
										<c:otherwise>&nbsp;</c:otherwise>
									</c:choose>
								</td>
								<td class="ta-l">${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
								<td class="ta-c">${LIST_RECIPE.get(i - 1).TOTAL_QNTT}</td>
							</tr>	
						</c:forEach>
					</tbody>
				</table>
				<div class="b">
					<p><span>약미 : ${DRUG_CNT}미</span><span>총용량: ${RECIPE.TOTAL_QNTT}g</span></p>
				</div>
			</div>
			<div class="bind">
				<div>
					<table class="bdb">
						<colgroup>
							<col style="width: 30%;">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th class="ta-c">택배사</th>
								<td class="ta-c">${ORDER.DELIVERY_NM}</td>
							</tr>
							<tr>
								<th class="ta-c">보내는사람</th>
								<td>${ORDER.SEND_NM} (Tel.${ORDER.SEND_TEL} / Mobile.${ORDER.SEND_MOBILE}<br>[${ORDER.SEND_ZIPCODE}] ${ORDER.SEND_ADDRESS} ${ORDER.SEND_ADDRESS2}</td>
							</tr>
							<tr>
								<th class="ta-c">받는사람</th>
								<td>${ORDER.RECV_NM} (Tel.${ORDER.RECV_TEL} / Mobile.${ORDER.RECV_MOBILE})<br/>[${ORDER.RECV_ZIPCODE}] ${ORDER.RECV_ADDRESS} ${ORDER.RECV_ADDRESS2}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<c:if test="${RECIPE.RECIPE_TP eq '001'}">
						<table>
							<tbody>
								<tr>
									<th class="ta-c">파우치</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM1.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM1.size()}">
												<img src="/download?filepath=${LIST_BOM1.get(i - 1).FILEPATH}" alt="" style="width: 150px;">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">한약박스</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM2.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM2.size()}">
												<img src="/download?filepath=${LIST_BOM2.get(i - 1).FILEPATH}" alt="" style="width: 150px;">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">배송박스</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM3.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM3.size()}">
												<img src="/download?filepath=${LIST_BOM3.get(i - 1).FILEPATH}" alt="" style="width: 150px;">
											</c:forEach>
										</c:if>
									</td>
								</tr>
							</tbody>
						</table>
					</c:if>
					<c:if test="${RECIPE.RECIPE_TP eq '002'}">
						<table>
							<tbody>
								<tr>
									<th class="ta-c">내지</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM1.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM1.size()}">
												<img src="/download?filepath=${LIST_BOM1.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">소포장</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM2.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM2.size()}">
												<img src="/download?filepath=${LIST_BOM2.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">포장박스</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM3.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM3.size()}">
												<img src="/download?filepath=${LIST_BOM3.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">배송박스</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM4.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM4.size()}">
												<img src="/download?filepath=${LIST_BOM4.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
							</tbody>
						</table>
					</c:if>
					<c:if test="${RECIPE.RECIPE_TP eq '003'}">
						<table>
							<tbody>
								<tr>
									<th class="ta-c">스틱</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM1.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM1.size()}">
												<img src="/download?filepath=${LIST_BOM1.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">케이스</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM2.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM2.size()}">
												<img src="/download?filepath=${LIST_BOM2.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
								<tr>
									<th class="ta-c">배송박스</th>
								</tr>
								<tr>
									<td class="ta-c">
										<c:if test="${LIST_BOM3.size() > 0}">
											<c:forEach var="i" begin="1" end="${LIST_BOM3.size()}">
												<img src="/download?filepath=${LIST_BOM3.get(i - 1).FILEPATH}" alt="" style="width: 150px; height:100px">
											</c:forEach>
										</c:if>
									</td>
								</tr>
							</tbody>
						</table>
					</c:if>
				</div>
			</div>
		</div>
	</section>	
</div>
<div class="btn-bot">
	<a href="javascript:" class="btn ty1 b-close">닫기</a>
	<a href="javascript:" id="btnPrint" class="btn ty2">출력</a>
</div>

<script type="text/javascript" src="/js/printThis.js"></script>
<script type="text/javascript">
$(function() {
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
	
	$('#btnPrint').click(function() {
		$("#print").printThis({
			debug : false,
			importCSS : false,
			importStyle : true,
			removeInline: false, 
			printContainer : true,
			//loadCss: "../css/print.css",
			pageTitle: "",
			removeInline : false
		});
    });
	
	$("#print").printThis({
		debug : false,
		importCSS : false,
		importStyle : true,
		removeInline: false, 
		printContainer : true,
		//loadCss: "../css/print.css",
		pageTitle: "",
		removeInline : false
	});
});
</script>


</body>
</html>