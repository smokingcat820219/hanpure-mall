<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="mask" uri="/WEB-INF/tld/TagLibMasking.tld" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="Location"		content="KR" />
<meta name="subject"		content="한퓨어" />
<meta name="title"			content="한퓨어" />
<meta name="copyright"		content="© 한퓨어대한녹용. All rights reserved." />
<meta name="keywords"		content="한약재,한약재제조,GMP녹용,녹용,녹용수입,녹용제조,녹용쇼핑몰,대한녹용" />
<meta name="description" 	content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE" />
<meta name="distribution"	content="Global" />
<meta name="publisher"		content="한퓨어대한녹용" />
<meta name="author"		content="http://zeronara.net" />
<meta name="robots" 		content="index,follow" />
<!-- 미투데이 -->
<meta property="me2:post_body"      content="한퓨어" />
<meta property="me2:post_tag"       content="한약재,한약재제조,GMP녹용,녹용,녹용수입,녹용제조,녹용쇼핑몰,대한녹용,https://hanpuremall.co.kr/shop/" />
<meta property="me2:image"          content="https://hanpuremall.co.kr/img/share.png" />
<!-- Google -->
<meta itemprop="name"				content="한퓨어" />
<meta itemprop="description"		content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE" />
<meta itemprop="image"				content="https://hanpuremall.co.kr/img/share.png" />

<meta property="og:type" content="website">
<meta property="og:title" content="한퓨어">
<meta property="og:description" content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE">
<meta property="og:image" content="https://hanpuremall.co.kr/img/share.png">
<meta property="og:url" content="https://hanpuremall.co.kr/shop/">

<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="한퓨어">
<meta name="twitter:description" content="한퓨어대한녹용 GMP우수한약재생산관리. 국내최대녹용생산. Clean Herb. GAP청정국산한약재. 고품질수입한약재. 청정 뉴질랜드산. 러시아산녹용. 7단계품질관리. 이력추적제도. KVIE">
<meta name="twitter:image" content="https://hanpuremall.co.kr/img/share.png">
<meta name="twitter:domain" content="한퓨어">
<title>한퓨어</title>

<!-- link -->
<link rel="canonical" href=""/>
<link rel="shortcut icon" href="images/favicon.ico">

<script src="/js/jquery-1.12.4.min.js"></script>
</head>
<body>


<style>
* {margin: 0; padding: 0; box-sizing:border-box;}
table {border-collapse:collapse; border-spacing:0; width:100%; table-layout:fixed;}
caption {display:none;}
caption, th, td {vertical-align:middle;}

.paper {font-size: 13px; color: #000; line-height: 1.2; font-family: 'Malgun Gothic', '맑은 고딕', '돋움', sans-serif;}
.wrap_print {
	border-radius: 0;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	position:relative; display:block; 
	background-color: #fff;
	max-width:800px;
	margin: 0 auto;
	padding: 30px;
}
.wrap_print > p {font-size: 14px; margin-bottom: 5px;}
.wrap_print .inner {}
.wrap_print .titbox {text-align: center; margin-bottom: 5px;}
.wrap_print .titbox td {padding: 10px; text-align: center; font-weight: 700; font-size: 20px;}

.wrap_print td,
.wrap_print th {height: 24px; padding: 0 3px; text-align: left; border: 1px solid #000; font-size: 13px; line-height: 1.2;}
.wrap_print thead th {text-align: center;}
.wrap_print td {height: auto; padding: 5px 3px;}
.wrap_print .bdb {margin-bottom: 5px;}
.wrap_print .bind {margin-bottom: 5px; display: flex; flex-wrap: wrap;}
.wrap_print .bind table {flex:1;}
.wrap_print .bind table.mr {margin-right: 0.8%;}
.wrap_print .bind > div {width: 28%;}
.wrap_print .bind > div:first-child {width: 71%; margin-right: 1%;}
.wrap_print .bind .b {position: relative; top: -1px; z-index: 1; width: 100%; padding: 8px 10px; clear: both; border: 1px solid #000;}
.wrap_print .bind .b span {display: inline-block; vertical-align: top; margin-left: 20px;}
.wrap_print .bind .b span:first-child {margin-left: 0;}

.btn-bot {text-align: center; margin-top: 20px; padding-bottom: 20px;}
.btn-bot .btn {display: inline-block; vertical-align: top; padding: 9px 10px; min-width:100px; font-size: 14px; color: #fff; text-align: center; text-decoration: none;}
.btn-bot .btn.ty1 {background-color: #085d9d;}
.btn-bot .btn.ty2 {background-color: #085d9d;}


.ta-c {text-align: center !important;}




@page {
	size: A4;
	margin: 0;
}
@media print {
	html, body {
		width: 210mm;
		height: 290mm;
		background: #fff;
	}
	body {-webkit-print-color-adjust: exact;}
	.wrap_print {
		overflow:hidden;
		margin: 0;
		border: initial;
		border-radius: initial;
		width: initial;
		min-height: initial;
		box-shadow: initial;
		background: initial;
		page-break-after: always;
		background-color: #fff;
	}

	.wrap_print {
		width: 210mm;
		min-height: 280mm;
		margin: 0mm auto;
		position:relative; display:block; 
		page-break-after:always;
		background-color: #fff;
	}
	.wrap_print .inner {
		padding:0;
	}
}


</style>



<div id="print" class="paper">
	<section class="wrap_print">
		<div class="img"><img src="${BARCODE}" alt="바코드"></div>
		<div class="inner">	
			<table class="bdb">
				<colgroup>
					<col style="width: 12%;">
					<col>
					<col style="width: 13%;">
					<col style="width: 30%;">
				</colgroup>
				<tbody>
					<tr>
						<td style="border-right: 0;">#.${ORDER.ORDER_SEQ}</td>
						<td style="border-left: 0;height: 30px;text-align: center;"><font style="font-size: 1.2em;font-weight: 700;">${ORDER.HOSPITAL_NM} 처방전</font></td>
						<th>처방일자</th>
						<td>${ORDER.ORDER_DT}</td>
					</tr>
				</tbody>
			</table>
			<table class="bdb">
				<colgroup>
					<col style="width: 12%;">
					<col style="width: 33%;">
					<col style="width: 12%;">
					<col style="width: 18%;">
					<col style="width: 12%;">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>환자명</th>
						<td><strong>${ORDER.PATIENT_NM} (${ORDER.PATIENT_SEX} 22세 ${ORDER.PATIENT_BIRTH})</strong></td>
						<th>한의원명</th>
						<td colspan="3"><strong>${ORDER.HOSPITAL_NM} ${ORDER.DOCTOR}</strong></td>
					</tr>
					<tr>
						<th>환자연락처</th>
						<td>${ORDER.PATIENT_MOBILE}</td>
						<th>한의원전화</th>
						<td>${ORDER.HOSPITAL_MOBILE}</td>
						<th>요양기관</th>
						<td></td>
					</tr>
					<tr>
						<th>배송방식</th>
						<td><strong>${ORDER.RECV_NM}</strong></td>
						<th>배송일자</th>
						<td><strong>${ORDER.DELIVERY_DT}</strong></td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="6">[${ORDER.RECV_ZIPCODE}] ${ORDER.RECV_ADDRESS} ${ORDER.RECV_ADDRESS2}</td>
					</tr>
					<tr>
						<th>주문정보</th>
						<td><font style="color: #fc0000;font-weight: 700;">첩약보험</font></td>
						<th>복용법</th>
						<td colspan="3">
							<mask:display type="EDITOR" value="${RECIPE.HOWTOEAT}" />
						</td>
					</tr>
					<tr>
						<th></th>
						<td colspan="5"></td>
					</tr>
					<tr>
						<th>처방메모</th>
						<td colspan="5"><mask:display type="BR" value="${RECIPE.REMARK}" /></td>
					</tr>
				</tbody>
			</table>
			<table class="">
				<colgroup>
					<col style="width: 12%;">
					<col style="width: 33%;">
					<col style="width: 8%;">
					<col style="width: 11%;">
					<col style="width: 8%;">
					<col>
					<col style="width: 8%;">
					<col>
				</colgroup>
				<tbody>		
					<tr>
						<th rowspan="2">처방명</th>
						<td>${RECIPE.RECIPE_NM}</td>
						<th rowspan="2">팩수</th>
						<td rowspan="2"><strong>${RECIPE.INFO2}팩</strong></td>
						<th rowspan="2">팩용량</th>
						<td rowspan="2"><strong>${RECIPE.INFO3}ml</strong></td>
						<th rowspan="2">첩수</th>
						<td rowspan="2">${RECIPE.INFO1}첩</td>
					</tr>
					<tr>
						<td class="ta-c">
							복용일 1일 6팩
						</td>
					</tr>
				</tbody>
			</table>
			<div class="het">
				<table>
					<colgroup>
						<col style="width: 6%;">
						<col style="width: 39%;">
						<col style="width: 8%;">
						<col style="width: 11%;">
						<col>
					</colgroup>
					<thead>
						<tr>
							<th>N</th>
							<th>약재명</th>
							<th>용량</th>
							<th>첩수량</th>
							<th>약재메모</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${LIST_RECIPE.size() > 0}">							
							<c:forEach var="i" begin="0" end="${LIST_RECIPE.size() - 1}">
								<tr>
									<td class="ta-c">${i + 1}</td>
									<td class="ta-c">${LIST_RECIPE.get(i).DRUG_NM}</td>
									<td class="ta-c">${LIST_RECIPE.get(i).CHUP1}</td>
									<td class="ta-c">${LIST_RECIPE.get(i).TOTAL_QNTT}</td>
									<th></th>
								</tr>
							</c:forEach>
						</c:if>			
					</tbody>
				</table>
				<table>
					<colgroup>
						<col style="width: 80%;">
						<col>
					</colgroup>
					<tbody>
						<tr>
							<td style="text-align: right;"><strong>총조제량</strong></td>
							<td><strong><mask:display type="NUMBER" value="${RECIPE.TOTAL_QNTT}" /></strong></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
<div class="btn-bot">
	<a href="javascript:" class="btn ty1 b-close">닫기</a>
	<a href="javascript:" id="btnPrint" class="btn ty2">출력</a>
</div>

<script type="text/javascript" src="/js/printThis.js"></script>
<script type="text/javascript">
$(function() {
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
	
	$('#btnPrint').click(function() {
		$("#print").printThis({
			debug : false,
			importCSS : false,
			importStyle : true,
			removeInline: false, 
			printContainer : true,
			//loadCss: "../css/print.css",
			pageTitle: "",
			removeInline : false
		});
    });
	
	$("#print").printThis({
		debug : false,
		importCSS : false,
		importStyle : true,
		removeInline: false, 
		printContainer : true,
		//loadCss: "../css/print.css",
		pageTitle: "",
		removeInline : false
	});
});
</script>



</body>
</html>