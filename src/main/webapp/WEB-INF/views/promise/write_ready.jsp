<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>	
	$(function() {		    
		$(".inp_num").each(function(){
			var $this = $(this);

			$this.find("button").on("click", function(){
				var number = $this.find("input").val();

				if($(this).hasClass("bt_up")) {
					number ++;
				} 
				else {
					if (number > 0){
						number --;
					}					
				}

				$this.find("input").val(number);
				
				calcAmount($this.find("input"));
			});
		});	
	    
		$('#btnList').click(function() {
			location.href = "/promise/list_product" + window.location.hash;
	    });
		
	    $('#btnSave').click(function() {
	    	goSave();
	    });	    
	});
	
	function deleteItem(obj) {
		$(obj).closest("tr").remove();
	}
	
	function setSelected(obj) {
		$(obj).select();
	}
	
	function calcAmount(obj) {
		var input = $(obj).val();
		var order_qntt = $(obj).attr("order_qntt");
		var amount = $(obj).attr("amount");
		
		input = parseInt(input);
		order_qntt = parseInt(order_qntt);
		amount = parseInt(amount);
		
		if(input > order_qntt) {
			alert("배송 요청량이 최대 수량보다 많을 수 없습니다.");
			$(obj).val(order_qntt);
			return;
		}
		
		$(obj).closest("tr").find(".amount").html(NumberFormat(input * amount));		
	}
	
	function goSave() {
		if(ajaxRunning) return;
		
		var formData = new FormData();
		
		var doctor = GetValue("doctor");
		if(!doctor) {
			alert("처방의를 선택해 주세요.");
			return;
		}
		
		formData.append("DOCTOR", doctor);
		
		var length = $("input[name=order_qntt]").length;
		for(var i = 0; i < length; i++) {
			var order_qntt = $("input[name=order_qntt]").eq(i).val();
			var promise_seq = $("input[name=order_qntt]").eq(i).attr("promise_seq");
			var bom_cd = $("input[name=order_qntt]").eq(i).attr("bom_cd");
			
			if(parseInt(order_qntt) == 0) {
				alert("배송 요청량을 입력해 주세요.");
				$("input[name=order_qntt]").eq(i).focus();
				$("input[name=order_qntt]").eq(i).select();
				return;
			}
			
			formData.append("LIST_PROMISE_SEQ[]", promise_seq);		
			formData.append("LIST_BOM_CD[]", bom_cd);		
			formData.append("LIST_ORDER_QNTT[]", order_qntt);		
		}
		
		var url = "/promise/setOrderQntt";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	location.href = "/promise/order";
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });	
	}
	
	function showAddPromise(type, promise_seq) {
		showPopup(800, 800, "/common/pop_promise_add?type=" + type + "&promise_seq=" + promise_seq);
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">약속처방 배송요청 준비</h2>
			</header>

			<h3 class="h_tit2">처방 정보</h3>
			<div class="tbl_basic pr-mb1">
				<table class="view">
					<colgroup>
						<col class="th1">
						<col>
						<col class="th1">
						<col>
						<col class="th1">
						<col>
					</colgroup>
					<tbody>
						<tr>
							<th>날짜</th>
							<td class="ta-c">${TODAY}</td>
							<th>한의원명</th>
							<td class="ta-c">${SESSION.COMPANY}</td>
							<th>처방의 <span class="i_emp">*</span></th>
							<td class="ta-c">
								<select name="doctor" id="doctor" class="select1 w100p">
									<option value="">선택하세요</option>
									
									<c:if test="${LIST_DOCTORS.size() > 0}">
										<c:forEach var="i" begin="1" end="${LIST_DOCTORS.size()}">
											<option value="${LIST_DOCTORS.get(i - 1)}" <c:if test="${i == 1}">selected</c:if>>${LIST_DOCTORS.get(i - 1)}</option>
										</c:forEach>
									</c:if>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<h3 class="h_tit2">상품 정보</h3>
			<div class="tbl_basic">
				<table class="list">
					<colgroup>
						<col>
						<col class="size1">
						<col class="size2">
						<col class="total">
						<col class="size1">
						<col class="total">
					</colgroup>
					<thead>
						<tr>
							<th>상품명</th>
							<th>최대 수량</th>
							<th>배송 요청량</th>
							<th>가격</th>
							<th>삭제</th>
							<th>처방 추가</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" begin="1" end="${LIST.size()}">
							<tr>
								<td class="subject">
									<div class="prd_order">
										<div class="im"><img src="/download?filepath=${LIST.get(i - 1).FILEPATH1}" alt=""></div>
										<div class="tx">
											<p>${LIST.get(i - 1).OPTION_NM}</p>
										</div>
									</div>
								</td>
								<td>
									<mask:display type="NUMBER" value="${LIST.get(i - 1).ORDER_QNTT}" />																	
								</td>
								<td>
									<div class="inp_num">
										<input type="number" value="0" name="order_qntt" id="order_qntt" onClick="setSelected(this)" onKeyUp="calcAmount(this)" promise_seq="${LIST.get(i - 1).PROMISE_SEQ}" bom_cd="${LIST.get(i - 1).BOM_CD}" order_qntt="${LIST.get(i - 1).ORDER_QNTT}" amount="${LIST.get(i - 1).AMOUNT}" />
										<button type="button" class="bt_up"><span>수량 증가</span></button>
										<button type="button" class="bt_down"><span>수량 감소</span></button>
									</div>
								</td>
								<td class="ta-r"><span class="amount">0</span> 원</td>
								<td><button type="button" class="c-gray" onClick="deleteItem(this)"><span>[삭제]</span></button></td>
								<td><button type="button" class="btn-pk s blue2" onclick="showAddPromise('3', '${LIST.get(i - 1).PROMISE_SEQ}');"><span>사전처방 추가</span></button></td>
							</tr>						
						</c:forEach>
					</tbody>
				</table>
			</div>

			<div class="btn-bot mbtn_n2">
				<a href="javascript:" id="btnList" class="btn-pk b white"><span>이전 페이지로 이동</span></a>
				<a href="javascript:" id="btnSave" class="btn-pk b blue"><span>배송 정보 입력하기</span></a>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>