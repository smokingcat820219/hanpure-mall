<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	var UUID = "";
</script>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script>	
	$(function() {		    
		$('#btnPrev').click(function() {
			location.href = "/promise/ready" + window.location.hash;
	    });
		
	    $('#btnSave').click(function() {
	    	goSave();
	    });	   
	    
	    setSender('1');
	    
	    setOneRecver();
	});
	
	function qnttCheck(obj) {
		var number = $(obj).val();		
		
		var promise_seq = $(obj).attr("promise_seq");
		var order_qntt = $(obj).attr("order_qntt");
		
		console.log("number", number);
		console.log("promise_seq", promise_seq);
		console.log("order_qntt", order_qntt);
		
		order_qntt = parseInt(order_qntt);
		
		var sum = 0;
		
		var length = $("input[name='order_qntt']").length;
		for(var i = 0; i < length; i++) {
			if($("input[name='order_qntt']").eq(i).attr("promise_seq") == promise_seq) {
				var order_qntt2 = $("input[name='order_qntt']").eq(i).val();
				console.log("order_qntt2", order_qntt2);
				
				order_qntt2 = parseInt(order_qntt2);
				
				sum += order_qntt2;
			}					
		}
				
		console.log(sum, order_qntt);
		
		if(sum > order_qntt) {
			sum = sum - number;
			$(obj).val(order_qntt - sum);
		}		
		else {
			$(obj).val(number);	
		}
		
		calcAmount($(obj));
	}
	
	function qnttUp(uuid, num) {
		var number = $("#order_qntt_" + num + "_" + uuid).val();		
		
		var promise_seq = $("#order_qntt_" + num + "_" + uuid).attr("promise_seq");
		var order_qntt = $("#order_qntt_" + num + "_" + uuid).attr("order_qntt");
		
		console.log("number", number);
		console.log("promise_seq", promise_seq);
		console.log("order_qntt", order_qntt);
		
		order_qntt = parseInt(order_qntt);
		
		var sum = 0;
		
		var length = $("input[name='order_qntt']").length;
		for(var i = 0; i < length; i++) {
			if($("input[name='order_qntt']").eq(i).attr("promise_seq") == promise_seq) {
				var order_qntt2 = $("input[name='order_qntt']").eq(i).val();
				console.log("order_qntt2", order_qntt2);
				
				order_qntt2 = parseInt(order_qntt2);
				
				sum += order_qntt2;
			}					
		}
		
		if(sum == order_qntt) {
			return;
		}		
		
		number++;
		
		$("#order_qntt_" + num + "_" + uuid).val(number);
		
		calcAmount($("#order_qntt_" + num + "_" + uuid));
	}
	
	function qnttDown(uuid, num) {
		var number = $("#order_qntt_" + num + "_" + uuid).val();		
		
		number--;
		
		if(number < 0) {
			number = 0;
		}
		
		$("#order_qntt_" + num + "_" + uuid).val(number);
		
		calcAmount($("#order_qntt_" + num + "_" + uuid));
	}
	
	function goSave() {
		if(ajaxRunning) return;
		
		var formData = new FormData();
	    
		var send_tp = GetRadioValue("send_tp");
		var send_nm = GetValue("send_nm");
		var send_tel1 = GetValue("send_tel1");
		var send_tel2 = GetValue("send_tel2");
		var send_tel3 = GetValue("send_tel3");
		var send_mobile1 = GetValue("send_mobile1");
		var send_mobile2 = GetValue("send_mobile2");
		var send_mobile3 = GetValue("send_mobile3");
		var send_zipcode = GetValue("send_zipcode");
		var send_address = GetValue("send_address");
		var send_address2 = GetValue("send_address2");
		
		if(!send_nm) {
			alert("발신인을 입력해 주세요.");
			$("#send_nm").focus();
			return;
		}
		
		if(!send_mobile1) {
			alert("발신인 휴대폰을 입력해 주세요.");
			$("#send_mobile1").focus();
			return;
		}
		
		if(!send_mobile2) {
			alert("발신인 휴대폰을 입력해 주세요.");
			$("#send_mobile2").focus();
			return;
		}
		
		if(!send_mobile3) {
			alert("발신인 휴대폰을 입력해 주세요.");
			$("#send_mobile3").focus();
			return;
		}
		
		if(!send_zipcode) {
			alert("발신인 우편번호를 검색해 주세요.");
			$("#send_zipcode").focus();
			return;
		}
		
		if(!send_address) {
			alert("발신인 우편번호를 검색해 주세요.");
			$("#send_address").focus();
			return;
		}
		
		if(!send_address2) {
			alert("발신인 상세주소를 입력해 주세요.");
			$("#send_address2").focus();
			return;
		}
		
		var send_tel = send_tel1 + "-" + send_tel2 + "-" + send_tel3;
		var send_mobile = send_mobile1 + "-" + send_mobile2 + "-" + send_mobile3;
		
		formData.append("DOCTOR", "${DOCTOR}");
		formData.append("SEND_TP", send_tp);
		formData.append("SEND_NM", send_nm);
		formData.append("SEND_TEL", send_tel);
		formData.append("SEND_MOBILE", send_mobile);
		formData.append("SEND_ZIPCODE", send_zipcode);
		formData.append("SEND_ADDRESS", send_address);
		formData.append("SEND_ADDRESS2", send_address2);
		
		var length = $("input[name='uuid']").length;
		console.log("uuid", length);
		
		for(var i = 0; i < length; i++) {
			var uuid = $("input[name='uuid']").eq(i).val();
			
			var recv_tp = GetRadioValue("recv_tp_" + uuid);
			var recv_nm = $("#recv_nm_" + uuid).val();
			var recv_tel1 = $("#recv_tel1_" + uuid).val();
			var recv_tel2 = $("#recv_tel2_" + uuid).val();
			var recv_tel3 = $("#recv_tel3_" + uuid).val();
			var recv_mobile1 = $("#recv_mobile1_" + uuid).val();
			var recv_mobile2 = $("#recv_mobile2_" + uuid).val();
			var recv_mobile3 = $("#recv_mobile3_" + uuid).val();
			var recv_zipcode = $("#recv_zipcode_" + uuid).val();
			var recv_address = $("#recv_address_" + uuid).val();
			var recv_address2 = $("#recv_address2_" + uuid).val();
			var msg_make = $("#msg_make_" + uuid).val();
			var msg_delivery = $("#msg_delivery_" + uuid).val();			
			
			if(!recv_nm) {
				alert("수신인을 입력해 주세요.");
				$("#recv_nm_" + uuid).focus();
				return;
			}
			
			if(!recv_mobile1) {
				alert("수신인 휴대폰을 입력해 주세요.");
				$("#recv_mobile1_" + uuid).focus();
				return;
			}
			
			if(!recv_mobile2) {
				alert("수신인 휴대폰을 입력해 주세요.");
				$("#recv_mobile2_" + uuid).focus();
				return;
			}
			
			if(!recv_mobile3) {
				alert("수신인 휴대폰을 입력해 주세요.");
				$("#recv_mobile3_" + uuid).focus();
				return;
			}
			
			if(!recv_zipcode) {
				alert("수신인 우편번호를 검색해 주세요.");
				$("#recv_zipcode_" + uuid).focus();
				return;
			}
			
			if(!recv_address) {
				alert("수신인 우편번호를 검색해 주세요.");
				$("#recv_address_" + uuid).focus();
				return;
			}
			
			if(!recv_address2) {
				alert("수신인 상세주소를 입력해 주세요.");
				$("#recv_address2_" + uuid).focus();
				return;
			}
			
			var recv_tel = recv_tel1 + "-" + recv_tel2 + "-" + recv_tel3;
			var recv_mobile = recv_mobile1 + "-" + recv_mobile2 + "-" + recv_mobile3;
			
			formData.append("LIST_RECV_TP[]", recv_tp);
			formData.append("LIST_RECV_NM[]", recv_nm);
			formData.append("LIST_RECV_TEL[]", recv_tel);
			formData.append("LIST_RECV_MOBILE[]", recv_mobile);
			formData.append("LIST_RECV_ZIPCODE[]", recv_zipcode);
			formData.append("LIST_RECV_ADDRESS[]", recv_address);
			formData.append("LIST_RECV_ADDRESS2[]", recv_address2);
			
			if(!msg_make) msg_make = "-";
			if(!msg_delivery) msg_delivery = "-";			
			
			formData.append("LIST_MSG_MAKE[]", msg_make);
			formData.append("LIST_MSG_DELIVERY[]", msg_delivery);			
		}		
		
		var length2 = $("input[name='order_qntt']").length;
		if(length2 == 0) {
			console.log("단일 배송");
			
			//단일 배송
			<c:forEach var="i" begin="1" end="${LIST.size()}">
				var promise_seq = "${LIST.get(i - 1).PROMISE_SEQ}";
				var bom_cd = "${LIST.get(i - 1).BOM_CD}";
				var order_qntt = "${LIST.get(i - 1).ORDER_QNTT}";
				
				formData.append("LIST_PROMISE_SEQ[]", promise_seq);
				formData.append("LIST_BOM_CD[]", bom_cd);
				formData.append("LIST_ORDER_QNTT[]", order_qntt);				
			</c:forEach>
			
			formData.append("LIST_DELIVERY_PRICE[]", "${DELIVERY_PRICE}");
		}
		else if(length2 > 0) {
			console.log("분리 배송");
			
			var length3 = $("input[name='delivery_price']").length;
			for(var k = 0; k < length3; k++) {
				var delivery_price = $("input[name='delivery_price']").eq(k).val();	
				console.log("delivery_price : " + delivery_price);
				formData.append("LIST_DELIVERY_PRICE[]", delivery_price);
			}
			
			//분리배송	
			for(var k = 0; k < length2; k++) {
				var order_qntt = $("input[name='order_qntt']").eq(k).val();				
				var promise_seq = $("input[name='order_qntt']").eq(k).attr("promise_seq");
				var bom_cd = $("input[name='order_qntt']").eq(k).attr("bom_cd");
				//var delivery_price = $("input[name='delivery_price']").eq(k).val();		
				
				//console.log("delivery_price : " + delivery_price);
				
				if(!order_qntt) {
					alert("분리배송 수량을 입력해 주세요.");
					$("input[name='order_qntt']").eq(k).focus();
					return;
				}
				
				var total_order_qntt = $("input[name='order_qntt']").eq(k).attr("order_qntt");
				var option_nm = $("input[name='order_qntt']").eq(k).attr("option_nm");
				total_order_qntt = parseInt(total_order_qntt);
				
				//전체 수량 체크
				var sum = 0;
				for(var z = 0; z < length2; z++) {
					if($("input[name='order_qntt']").eq(z).attr("promise_seq") == promise_seq) {
						var order_qntt2 = $("input[name='order_qntt']").eq(z).val();						
						order_qntt2 = parseInt(order_qntt2);	
						
						if(order_qntt2 == 0) {
							//alert("분리배송 수량은 0보다 커야 합니다.");
							//return;
						}
						
						sum += order_qntt2;
					}					
				}
				
				if(total_order_qntt != sum) {
					console.log(total_order_qntt, sum);
					alert(option_nm + " 총 수량이 일치하지 않습니다.");
					$("input[name='order_qntt']").eq(k).focus();
					return;
				}
				
				
				formData.append("LIST_PROMISE_SEQ[]", promise_seq);
				formData.append("LIST_BOM_CD[]", bom_cd);
				formData.append("LIST_ORDER_QNTT[]", order_qntt);
				
			}			
		}	
		
		if(!confirm("주문 하시겠습니까?")) {
			return;
		}
		
		var url = "/promise/order";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	location.href = "/order/list";
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });	
	}
	
	function OpenDaumSend() {
		new daum.Postcode({
			oncomplete: function(data) {
				$("#send_zipcode").val(data.zonecode);
				$("#send_address").val(data.roadAddress);
			}
		}).open();
	}
	
	function OpenDaumRecv(uuid) {
		new daum.Postcode({
			oncomplete: function(data) {
				$("#recv_zipcode_" + uuid).val(data.zonecode);
				$("#recv_address_" + uuid).val(data.roadAddress);
			}
		}).open();
	}
	
	function setOneRecver() {
		var uuid = getUUID();		
		
		var sHTML = "";		
		sHTML += "<div class='top_tit' name='div_" + uuid + "'>";
		sHTML += "	<input type='hidden' name='uuid' value='" + uuid + "' />";
		sHTML += "	<h3 class='h_tit2'>받는 사람</h3>";
		sHTML += "	<div class='rgh'>";
		sHTML += "		<button type='button' class='btn-pk blue2 s' onclick=\"setMultiRecver()\"><span>분리 배송</span></button>";
		sHTML += "		<button type='button' class='btn_q'><span>도움말</span></button>";
		sHTML += "		<div class='box_q'>";
		sHTML += "			<p class='t1'>분리 배송은 배송지 추가하여 주문 가능합니다.</p>";
		sHTML += "			<p class='t2 c-blue'>* 배송 비용이 추가될 수 있습니다.</p>";
		sHTML += "		</div>";
		sHTML += "	</div>";
		sHTML += "</div>";
		sHTML += "<div class='tbl_basic pr-mb1' name='div_" + uuid + "'>";
		sHTML += "	<table class='write'>";
		sHTML += "		<colgroup>";
		sHTML += "			<col class='th2'>";
		sHTML += "			<col>";
		sHTML += "		</colgroup>";
		sHTML += "		<tbody>";
		sHTML += "			<tr>";
		sHTML += "				<th>수신인 선택 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<label class='inp_radio mr20'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp1_" + uuid + "' onclick=\"setRecver('1', '" + uuid + "')\" value='1' /><span>환자</span></label>";
		sHTML += "					<label class='inp_radio mr20'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp2_" + uuid + "' onclick=\"setRecver('2', '" + uuid + "')\" value='2' /><span>한의원</span></label>";
		sHTML += "					<label class='inp_radio'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp3_" + uuid + "' onclick=\"setRecver('3', '" + uuid + "')\" value='3' checked /><span>수동입력</span></label>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>수신인 <span class='i_emp'>*</span></th>";
		sHTML += "				<td><input type='text' class='inp_txt w100p' name='recv_nm' id='recv_nm_" + uuid + "' placeholder='수신인 정보를 입력하세요' /></td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>연락처</th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_phone'>";
		sHTML += "						<input type='text' name='recv_tel1' id='recv_tel1_" + uuid + "' class='inp_txt number' maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_tel2' id='recv_tel2_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_tel3' id='recv_tel3_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>휴대폰 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_phone'>";
		sHTML += "						<input type='text' name='recv_mobile1' id='recv_mobile1_" + uuid + "' class='inp_txt number' maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_mobile2' id='recv_mobile2_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_mobile3' id='recv_mobile3_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>주소 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_addr'>";
		sHTML += "						<div class='inp'>";
		sHTML += "							<button type='button' onClick=\"OpenDaumRecv('" + uuid + "')\" class='btn-pk n blue2'><span>우편번호</span></button>";
		sHTML += "							<input type='text' name='recv_zipcode' id='recv_zipcode_" + uuid + "' onClick=\"OpenDaumRecv('" + uuid + "')\" readOnly class='inp_txt' placeholder='우편번호 검색을 클릭하세요'>";
		//sHTML += "							<button type='button' class='btn-pk n red fl-r'><span>당일배송 지역</span></button>";
		sHTML += "						</div>";
		sHTML += "						<input type='text' name='recv_address' id='recv_address_" + uuid + "' class='inp_txt w100p' onClick=\"OpenDaumRecv('" + uuid + "')\" readOnly placeholder='우편번호 검색 시 읍,면,동 자동입력'>";
		sHTML += "						<input type='text' name='recv_address2' id='recv_address2_" + uuid + "' class='inp_txt w100p' placeholder='나머지 상세 주소를 입력하세요'>";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>탕전실<br/>요청사항</th>";
		sHTML += "				<td>";
		sHTML += "					<textarea class='textare1 w100p' name='msg_make' id='msg_make_" + uuid + "' placeholder='탕전실에 전달할 요청사항을 적어주세요(200자 이내)'></textarea>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>배송기사<br/>요청사항</th>";
		sHTML += "				<td>";
		sHTML += "					<textarea class='textare1 w100p' name='msg_delivery' id='msg_delivery_" + uuid + "' placeholder='택배 배송기사에게 전달할 요청사항을 적어주세요(200자 이내)'></textarea>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "		</tbody>";
		sHTML += "	</table>";
		sHTML += "</div>";
		
		$("#address_area").html(sHTML);		
	}
	
	function setMultiRecver() {
		var uuid = getUUID();		
		
		var sHTML = "";
		sHTML += "<div class='top_tit' name='div_" + uuid + "'>";
		sHTML += "	<input type='hidden' name='uuid' value='" + uuid + "' />";
		sHTML += "	<h3 class='h_tit2 recv_num'>분리 배송</h3>";
		sHTML += "	<div class='rgh'>";
		sHTML += "		<button type='button' class='btn-pk blue2 s' onclick=\"addRecver()\"><span>배송지 추가</span></button>";
		sHTML += "		<button type='button' class='btn-pk gray2 s' onclick=\"setOneRecver()\"><span>초기화</span></button>";
		sHTML += "	</div>";
		sHTML += "</div>";
		sHTML += "<p name='div_" + uuid + "' class='c-blue mb10 fz-s1'>* 분리 배송시, 배송 비용이 추가될 수 있습니다.</p>";
		sHTML += "<div class='tbl_basic pr-mb1' name='div_" + uuid + "'>";
		sHTML += "	<table class='write'>";
		sHTML += "		<colgroup>";
		sHTML += "			<col class='th2'>";
		sHTML += "			<col>";
		sHTML += "		</colgroup>";
		sHTML += "		<tbody>";
		sHTML += "			<tr>";
		sHTML += "				<th>분리배송 <br>수량</th>";
		sHTML += "				<td>";
		sHTML += "					<div class='box_separation'>";
		//sHTML += "						<button type='button' class='btn_del' onclick=\"deleteRecver('" + uuid + "');\"><span>삭제</span></button>";
		
		<c:forEach var="i" begin="1" end="${LIST.size()}">
			sHTML += "						<div class='b'>";
			sHTML += "							<p>${LIST.get(i - 1).OPTION_NM}</p>";						
			sHTML += "							<div class='inp_bind ai-b mt10'>";
			sHTML += "								<div class='inp_num w100'>";
			sHTML += "									<input type='number' name='order_qntt' id='order_qntt_${i}_" + uuid + "' value='${LIST.get(i - 1).ORDER_QNTT}' option_nm='${LIST.get(i - 1).OPTION_NM}' promise_seq='${LIST.get(i - 1).PROMISE_SEQ}' order_qntt='${LIST.get(i - 1).ORDER_QNTT}' amount='${LIST.get(i - 1).AMOUNT}' bom_cd='${LIST.get(i - 1).BOM_CD}' onKeyUp=\"qnttCheck(this)\"/>";
			sHTML += "									<button type='button' class='bt_up' onClick=\"qnttUp('" + uuid + "', '${i}')\"><span>수량 증가</span></button>";
			sHTML += "									<button type='button' class='bt_down' onClick=\"qnttDown('" + uuid + "', '${i}')\"><span>수량 감소</span></button>";
			sHTML += "								</div>";
			
			var ORDER_QNTT = "${LIST.get(i - 1).ORDER_QNTT}";
			var AMOUNT = "${LIST.get(i - 1).AMOUNT}";
			
			ORDER_QNTT = parseInt(ORDER_QNTT);
			AMOUNT = parseInt(AMOUNT);
			
			sHTML += "								<div class='total'>";
			sHTML += "									<p class='amount'>" + NumberFormat(ORDER_QNTT * AMOUNT) + "</p>";
			sHTML += "								</div>";
			sHTML += "							</div>";
			sHTML += "						</div>";		
		</c:forEach>		
			
		//배송비
		sHTML += "						<div class='b'>";
		sHTML += "							<p>배송비</p>";						
		sHTML += "							<div class='inp_bind ai-b mt10'>";	
		sHTML += "								<div class='inp_num w100'>";
		sHTML += "								</div>";
		sHTML += "								<div class='total'>";
		sHTML += "									<input type='hidden' name='delivery_price' value='${DELIVERY_PRICE}' />";
		sHTML += "									<p class='amount'>" + NumberFormat("${DELIVERY_PRICE}") + "</p>";
		sHTML += "								</div>";
		sHTML += "							</div>";
		sHTML += "						</div>";			
		//배송비
		
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>수신인 선택 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<label class='inp_radio mr20'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp1_" + uuid + "' onclick=\"setRecver('1', '" + uuid + "')\" value='1' /><span>환자</span></label>";
		sHTML += "					<label class='inp_radio mr20'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp2_" + uuid + "' onclick=\"setRecver('2', '" + uuid + "')\" value='2' /><span>한의원</span></label>";
		sHTML += "					<label class='inp_radio'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp3_" + uuid + "' onclick=\"setRecver('3', '" + uuid + "')\" value='3' checked /><span>수동입력</span></label>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>수신인</th>";
		sHTML += "				<td><input type='text' class='inp_txt w100p' name='recv_nm' id='recv_nm_" + uuid + "' placeholder='수신인 정보를 입력하세요' /></td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>연락처</th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_phone'>";
		sHTML += "						<input type='text' name='recv_tel1' id='recv_tel1_" + uuid + "' class='inp_txt number' maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_tel2' id='recv_tel2_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_tel3' id='recv_tel3_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>휴대폰 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_phone'>";
		sHTML += "						<input type='text' name='recv_mobile1' id='recv_mobile1_" + uuid + "' class='inp_txt number' maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_mobile2' id='recv_mobile2_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_mobile3' id='recv_mobile3_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>주소 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_addr'>";
		sHTML += "						<div class='inp'>";
		sHTML += "							<button type='button' onClick=\"OpenDaumRecv('" + uuid + "')\" class='btn-pk n blue2'><span>우편번호</span></button>";
		sHTML += "							<input type='text' name='recv_zipcode' id='recv_zipcode_" + uuid + "' onClick=\"OpenDaumRecv('" + uuid + "')\" readOnly class='inp_txt' placeholder='우편번호 검색을 클릭하세요'>";
		//sHTML += "							<button type='button' class='btn-pk n red fl-r'><span>당일배송 지역</span></button>";
		sHTML += "						</div>";
		sHTML += "						<input type='text' name='recv_address' id='recv_address_" + uuid + "' class='inp_txt w100p' onClick=\"OpenDaumRecv('" + uuid + "')\" readOnly placeholder='우편번호 검색 시 읍,면,동 자동입력'>";
		sHTML += "						<input type='text' name='recv_address2' id='recv_address2_" + uuid + "' class='inp_txt w100p' placeholder='나머지 상세 주소를 입력하세요'>";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>탕전실<br/>요청사항</th>";
		sHTML += "				<td>";
		sHTML += "					<textarea class='textare1 w100p' name='msg_make' id='msg_make_" + uuid + "' placeholder='탕전실에 전달할 요청사항을 적어주세요(200자 이내)'></textarea>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>배송기사<br/>요청사항</th>";
		sHTML += "				<td>";
		sHTML += "					<textarea class='textare1 w100p' name='msg_delivery' id='msg_delivery_" + uuid + "' placeholder='택배 배송기사에게 전달할 요청사항을 적어주세요(200자 이내)'></textarea>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "		</tbody>";
		sHTML += "	</table>";
		sHTML += "</div>";

		$("#address_area").html(sHTML);		
		
		for(var i = 0; i < $(".recv_num").length; i++) {
			$(".recv_num").eq(i).html("분리 배송 " + (i + 1));
		}
	}
	
	function addRecver() {
		var uuid = getUUID();		
		
		var sHTML = "";
		sHTML += "<div class='top_tit' name='div_" + uuid + "'>";
		sHTML += "	<input type='hidden' name='uuid' value='" + uuid + "' />";
		sHTML += "	<h3 class='h_tit2 recv_num'>분리 배송</h3>";
		sHTML += "	<div class='rgh'>";
		sHTML += "		<button type='button' class='btn-pk blue2 s' onclick=\"addRecver()\"><span>배송지 추가</span></button>";
		sHTML += "		<button type='button' class='btn-pk gray2 s' onclick=\"setOneRecver()\"><span>초기화</span></button>";
		sHTML += "	</div>";
		sHTML += "</div>";
		sHTML += "<p name='div_" + uuid + "' class='c-blue mb10 fz-s1'>* 분리 배송시, 배송 비용이 추가될 수 있습니다.</p>";
		sHTML += "<div class='tbl_basic pr-mb1' name='div_" + uuid + "'>";
		sHTML += "	<table class='write'>";
		sHTML += "		<colgroup>";
		sHTML += "			<col class='th2'>";
		sHTML += "			<col>";
		sHTML += "		</colgroup>";
		sHTML += "		<tbody>";
		sHTML += "			<tr>";
		sHTML += "				<th>분리배송 <br>수량</th>";
		sHTML += "				<td>";
		sHTML += "					<div class='box_separation'>";
		sHTML += "						<button type='button' class='btn_del' onclick=\"deleteRecver('" + uuid + "');\"><span>삭제</span></button>";
		
		<c:forEach var="i" begin="1" end="${LIST.size()}">
			sHTML += "						<div class='b'>";
			sHTML += "							<p>${LIST.get(i - 1).OPTION_NM}</p>";						
			sHTML += "							<div class='inp_bind ai-b mt10'>";
			sHTML += "								<div class='inp_num w100'>";
			sHTML += "									<input type='number' name='order_qntt' id='order_qntt_${i}_" + uuid + "' value='0' promise_seq='${LIST.get(i - 1).PROMISE_SEQ}' option_nm='${LIST.get(i - 1).OPTION_NM}' order_qntt='${LIST.get(i - 1).ORDER_QNTT}' amount='${LIST.get(i - 1).AMOUNT}' bom_cd='${LIST.get(i - 1).BOM_CD}' onKeyUp=\"qnttCheck(this)\"/>";
			sHTML += "									<button type='button' class='bt_up' onClick=\"qnttUp('" + uuid + "', '${i}')\"><span>수량 증가</span></button>";
			sHTML += "									<button type='button' class='bt_down' onClick=\"qnttDown('" + uuid + "', '${i}')\"><span>수량 감소</span></button>";
			sHTML += "								</div>";
			sHTML += "								<div class='total'>";
			sHTML += "									<p class='amount'>0</p>";
			sHTML += "								</div>";
			sHTML += "							</div>";
			sHTML += "						</div>";		
		</c:forEach>		
		
		//배송비
		sHTML += "						<div class='b'>";
		sHTML += "							<p>배송비</p>";						
		sHTML += "							<div class='inp_bind ai-b mt10'>";	
		sHTML += "								<div class='inp_num w100'>";
		sHTML += "								</div>";
		sHTML += "								<div class='total'>";
		sHTML += "									<input type='hidden' name='delivery_price' value='2500' />";
		sHTML += "									<p class='amount'>" + NumberFormat("2500") + "</p>";
		sHTML += "								</div>";
		sHTML += "							</div>";
		sHTML += "						</div>";			
		//배송비
		
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>수신인 선택 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<label class='inp_radio mr20'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp1_" + uuid + "' onclick=\"setRecver('1', '" + uuid + "')\" value='1' /><span>환자</span></label>";
		sHTML += "					<label class='inp_radio mr20'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp2_" + uuid + "' onclick=\"setRecver('2', '" + uuid + "')\" value='2' /><span>한의원</span></label>";
		sHTML += "					<label class='inp_radio'><input type='radio' name='recv_tp_" + uuid + "' id='recv_tp3_" + uuid + "' onclick=\"setRecver('3', '" + uuid + "')\" value='3' checked /><span>수동입력</span></label>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>수신인 <span class='i_emp'>*</span></th>";
		sHTML += "				<td><input type='text' class='inp_txt w100p' name='recv_nm' id='recv_nm_" + uuid + "' placeholder='수신인 정보를 입력하세요' /></td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>연락처</th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_phone'>";
		sHTML += "						<input type='text' name='recv_tel1' id='recv_tel1_" + uuid + "' class='inp_txt number' maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_tel2' id='recv_tel2_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_tel3' id='recv_tel3_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>휴대폰 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_phone'>";
		sHTML += "						<input type='text' name='recv_mobile1' id='recv_mobile1_" + uuid + "' class='inp_txt number' maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_mobile2' id='recv_mobile2_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "						<span>-</span>";
		sHTML += "						<input type='text' name='recv_mobile3' id='recv_mobile3_" + uuid + "' class='inp_txt' number maxlength='4' />";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		sHTML += "			<tr>";
		sHTML += "				<th>주소 <span class='i_emp'>*</span></th>";
		sHTML += "				<td>";
		sHTML += "					<div class='inp_addr'>";
		sHTML += "						<div class='inp'>";
		sHTML += "							<button type='button' onClick=\"OpenDaumRecv('" + uuid + "')\" class='btn-pk n blue2'><span>우편번호</span></button>";
		sHTML += "							<input type='text' name='recv_zipcode' id='recv_zipcode_" + uuid + "' onClick=\"OpenDaumRecv('" + uuid + "')\" readOnly class='inp_txt' placeholder='우편번호 검색을 클릭하세요'>";
		//sHTML += "							<button type='button' class='btn-pk n red fl-r'><span>당일배송 지역</span></button>";
		sHTML += "						</div>";
		sHTML += "						<input type='text' name='recv_address' id='recv_address_" + uuid + "' class='inp_txt w100p' onClick=\"OpenDaumRecv('" + uuid + "')\" readOnly placeholder='우편번호 검색 시 읍,면,동 자동입력'>";
		sHTML += "						<input type='text' name='recv_address2' id='recv_address2_" + uuid + "' class='inp_txt w100p' placeholder='나머지 상세 주소를 입력하세요'>";
		sHTML += "					</div>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>탕전실<br/>요청사항</th>";
		sHTML += "				<td>";
		sHTML += "					<textarea class='textare1 w100p' name='msg_make' id='msg_make_" + uuid + "' placeholder='탕전실에 전달할 요청사항을 적어주세요(200자 이내)'></textarea>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "			<tr>";
		sHTML += "				<th>배송기사<br/>요청사항</th>";
		sHTML += "				<td>";
		sHTML += "					<textarea class='textare1 w100p' name='msg_delivery' id='msg_delivery_" + uuid + "' placeholder='택배 배송기사에게 전달할 요청사항을 적어주세요(200자 이내)'></textarea>";
		sHTML += "				</td>";
		sHTML += "			</tr>";
		
		sHTML += "		</tbody>";
		sHTML += "	</table>";
		sHTML += "</div>";

		$("#address_area").append(sHTML);
		
		$(".inp_num").each(function(){
			var $this = $(this);

			$this.find("button").unbind("click");
		});			
		
		for(var i = 0; i < $(".recv_num").length; i++) {
			$(".recv_num").eq(i).html("분리 배송 " + (i + 1));
		}
	}
	
	function calcAmount(obj) {
		var input = $(obj).val();
		var amount = $(obj).attr("amount");
		
		input = parseInt(input);
		amount = parseInt(amount);
		
		$(obj).closest(".inp_bind").find(".amount").html(NumberFormat(input * amount));		
	}
	
	function setSender(value) {
		if(value == "1") {
			$("#send_nm").val("${SESSION.COMPANY}");
			
			var tel = "${SESSION.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#send_tel1").val(tel_array[0]);
					$("#send_tel2").val(tel_array[1]);
					$("#send_tel3").val(tel_array[2]);
				}
				else {
					$("#send_tel1").val("");
					$("#send_tel2").val("");
					$("#send_tel3").val("");
				}
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
			
			var mobile = "${SESSION.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#send_mobile1").val(mobile_array[0]);
					$("#send_mobile2").val(mobile_array[1]);
					$("#send_mobile3").val(mobile_array[2]);
				}
				else {
					$("#send_mobile1").val("");
					$("#send_mobile2").val("");
					$("#send_mobile3").val("");
				}
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
			
			$("#send_zipcode").val("${SESSION.ZIPCODE}");
			$("#send_address").val("${SESSION.ADDRESS}");
			$("#send_address2").val("${SESSION.ADDRESS2}");
		}
		else if(value == "2") {
			$("#send_nm").val("탕전실");
			
			var tel = "${HANPURE.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#send_tel1").val(tel_array[0]);
					$("#send_tel2").val(tel_array[1]);
					$("#send_tel3").val(tel_array[2]);
				}
				else {
					$("#send_tel1").val("");
					$("#send_tel2").val("");
					$("#send_tel3").val("");
				}
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
			
			var mobile = "${HANPURE.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#send_mobile1").val(mobile_array[0]);
					$("#send_mobile2").val(mobile_array[1]);
					$("#send_mobile3").val(mobile_array[2]);
				}
				else {
					$("#send_mobile1").val("");
					$("#send_mobile2").val("");
					$("#send_mobile3").val("");
				}
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
			
			$("#send_zipcode").val("${HANPURE.ZIPCODE}");
			$("#send_address").val("${HANPURE.ADDRESS}");
			$("#send_address2").val("${HANPURE.ADDRESS2}");
		}
		else if(value == "3") {
			$("#send_nm").val("");
			
			$("#send_tel1").val("");
			$("#send_tel2").val("");
			$("#send_tel3").val("");
			
			$("#send_mobile1").val("");
			$("#send_mobile2").val("");
			$("#send_mobile3").val("");
			
			$("#send_zipcode").val("");
			$("#send_address").val("");
			$("#send_address2").val("");
		}	
	}
	
	function setRecver(value, uuid) {
		if(value == "1") {
			UUID = uuid;			
			showPopup(700, 500, "/common/pop_patient");
		}
		else if(value == "2") {
			$("#recv_nm_" + uuid).val("${SESSION.COMPANY}");
			
			var tel = "${SESSION.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#recv_tel1_" + uuid).val(tel_array[0]);
					$("#recv_tel2_" + uuid).val(tel_array[1]);
					$("#recv_tel3_" + uuid).val(tel_array[2]);
				}
				else {
					$("#recv_tel1_" + uuid).val("");
					$("#recv_tel2_" + uuid).val("");
					$("#recv_tel3_" + uuid).val("");
				}
			}
			else {
				$("#recv_tel1_" + uuid).val("");
				$("#recv_tel2_" + uuid).val("");
				$("#recv_tel3_" + uuid).val("");
			}
			
			var mobile = "${SESSION.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#recv_mobile1_" + uuid).val(mobile_array[0]);
					$("#recv_mobile2_" + uuid).val(mobile_array[1]);
					$("#recv_mobile3_" + uuid).val(mobile_array[2]);
				}
				else {
					$("#recv_mobile1_" + uuid).val("");
					$("#recv_mobile2_" + uuid).val("");
					$("#recv_mobile3_" + uuid).val("");
				}
			}
			else {
				$("#recv_mobile1_" + uuid).val("");
				$("#recv_mobile2_" + uuid).val("");
				$("#recv_mobile3_" + uuid).val("");
			}
			
			$("#recv_zipcode_" + uuid).val("${SESSION.ZIPCODE}");
			$("#recv_address_" + uuid).val("${SESSION.ADDRESS}");
			$("#recv_address2_" + uuid).val("${SESSION.ADDRESS2}");
		}
		else if(value == "3") {
			$("#recv_nm_" + uuid).val("");
			
			$("#recv_tel1_" + uuid).val("");
			$("#recv_tel2_" + uuid).val("");
			$("#recv_tel3_" + uuid).val("");
			
			$("#recv_mobile1_" + uuid).val("");
			$("#recv_mobile2_" + uuid).val("");
			$("#recv_mobile3_" + uuid).val("");
			
			$("#recv_zipcode_" + uuid).val("");
			$("#recv_address_" + uuid).val("");
			$("#recv_address2_" + uuid).val("");
		}	
	}
	
	function setPatient(name, tel, mobile, zipcode, address, address2) {
		$("#recv_nm_" + UUID).val(name);
		if(tel) {
			var tel_array = tel.split("-");
			if(tel_array.length == 3) {
				$("#recv_tel1_" + UUID).val(tel_array[0]);
				$("#recv_tel2_" + UUID).val(tel_array[1]);
				$("#recv_tel3_" + UUID).val(tel_array[2]);
			}
			else {
				$("#recv_tel1_" + UUID).val("");
				$("#recv_tel2_" + UUID).val("");
				$("#recv_tel3_" + UUID).val("");
			}
		}
		else {
			$("#recv_tel1_" + UUID).val("");
			$("#recv_tel2_" + UUID).val("");
			$("#recv_tel3_" + UUID).val("");
		}
		if(mobile) {
			var mobile_array = mobile.split("-");
			if(mobile_array.length == 3) {
				$("#recv_mobile1_" + UUID).val(mobile_array[0]);
				$("#recv_mobile2_" + UUID).val(mobile_array[1]);
				$("#recv_mobile3_" + UUID).val(mobile_array[2]);
			}
			else {
				$("#recv_mobile1_" + UUID).val("");
				$("#recv_mobile2_" + UUID).val("");
				$("#recv_mobile3_" + UUID).val("");
			}
		}
		else {
			$("#recv_mobile1_" + UUID).val("");
			$("#recv_mobile2_" + UUID).val("");
			$("#recv_mobile3_" + UUID).val("");
		}
		
		$("#recv_zipcode_" + UUID).val(zipcode);
		$("#recv_address_" + UUID).val(address);
		$("#recv_address2_" + UUID).val(address2);
	}
	
	function deleteRecver(e) {
		console.log("deleteRecver", e);
		
		$("[name='div_" + e + "']").remove();
		
		for(var i = 0; i < $(".recv_num").length; i++) {
			$(".recv_num").eq(i).html("분리 배송 " + (i + 1));
		}
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">약속처방 주문서 작성</h2>
			</header>

			<h3 class="h_tit2">상품 정보</h3>
			<div class="tbl_basic pr-mb1">
				<table class="list">
					<colgroup>
						<col class="num">
						<col>
						<col class="size2">
						<col style="width: 20%;">
					</colgroup>
					<thead>
						<tr>
							<th>번호</th>
							<th>상품명</th>
							<th>배송 요청량</th>
							<th>가격</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" begin="1" end="${LIST.size()}">
							<tr>
								<td>${i}</td>
								<td class="subject">
									<div class="prd_order">
										<div class="im"><img src="/download?filepath=${LIST.get(i - 1).FILEPATH1}" alt=""></div>
										<div class="tx">
											<p>${LIST.get(i - 1).OPTION_NM}</p>
										</div>
									</div>
								</td>
								<td>
									<mask:display type="NUMBER" value="${LIST.get(i - 1).ORDER_QNTT}" />																	
								</td>
								<td class="ta-r"><mask:display type="NUMBER" value="${LIST.get(i - 1).ORDER_QNTT * LIST.get(i - 1).AMOUNT}" /> 원</td>
							</tr>						
						</c:forEach>
						
						<tr>
							<td></td>
							<td class="subject">
									<div class="prd_order">
										<div class="im"></div>
										<div class="tx">
											<p>배송비</p>
										</div>
									</div>
								</td>
							<td></td>
							<td class="ta-r"><mask:display type="NUMBER" value="${DELIVERY_PRICE}" /> 원</td>
						</tr>			
					</tbody>
				</table>
			</div>




			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">보내는 사람</h3>
				<div class="tbl_basic pr-mb1">
					<table class="write">
						<colgroup>
							<col class="th2">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>발신지 선택</th>
								<td>
									<label class="inp_radio mr20"><input type="radio" name="send_tp" id="send_tp1" onClick="setSender('1')" value="1" checked /><span>한의원</span></label>
									<label class="inp_radio mr20"><input type="radio" name="send_tp" id="send_tp2" onClick="setSender('2')" value="2" /><span>탕전실</span></label>
									<label class="inp_radio"><input type="radio" name="send_tp" id="send_tp3" onClick="setSender('3')" value="3" /><span>수동입력</span></label>
								</td>
							</tr>
							<tr>
								<th>발신인</th>
								<td><input type="text" class="inp_txt w100p" name="send_nm" id="send_nm" placeholder="발신인 정보를 입력하세요" /></td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<div class="inp_phone">
										<input type="text" name="send_tel1" id="send_tel1" class="inp_txt number" maxlength="4" />
										<span>-</span>
										<input type="text" name="send_tel2" id="send_tel2" class="inp_txt number" maxlength="4" />
										<span>-</span>
										<input type="text" name="send_tel3" id="send_tel3" class="inp_txt number" maxlength="4" />
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰</th>
								<td>
									<div class="inp_phone">
										<input type="text" name="send_mobile1" id="send_mobile1" class="inp_txt number" maxlength="4" />
										<span>-</span>
										<input type="text" name="send_mobile2" id="send_mobile2" class="inp_txt number" maxlength="4" />
										<span>-</span>
										<input type="text" name="send_mobile3" id="send_mobile3" class="inp_txt number" maxlength="4" />
									</div>
								</td>
							</tr>
							<tr>
								<th>주소</th>
								<td>
									<div class="inp_addr">
										<div class="inp">
											<button type="button" class="btn-pk n blue2" onClick="OpenDaumSend()"><span>우편번호</span></button>
											<input type="text" class="inp_txt" name="send_zipcode" id="send_zipcode" readOnly onClick="OpenDaumSend()" placeholder="우편번호 검색을 클릭하세요">
										</div>
										<input type="text" class="inp_txt w100p" name="send_address" id="send_address" onClick="OpenDaumSend()" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력">
										<input type="text" class="inp_txt w100p" name="send_address2" id="send_address2" placeholder="나머지 상세 주소를 입력하세요">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>


				<!-- 1. 분리배송 전 -->
				<div id="address_area">
					
				</div>	
			</div>		

			<!-- 우측데이터 -->
			<div class="rgh_c">
				<div class="mbtn_ty1">
					<button type="button" id="btnPrev" class="btn-pk b white"><span>이전 단계로 이동</span></button>
					<button type="button" id="btnSave" class="btn-pk b blue"><span>주문하기</span></button>
				</div>
			</div><!--// rgh_c -->
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>