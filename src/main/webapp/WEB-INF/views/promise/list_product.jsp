<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('.btnSave').click(function() {
	    	goSave();
	    });
	});
	
	function setHash(page) {					
		var param = "page=" + page;
		param += "&LIST_STATUS=I,F";
		window.location.hash = param;
	}
	
	function optionChange(promise_seq) {
		var filepath = $("#product_option_" + promise_seq + " option:selected").attr("filepath");
		var item_qntt = $("#product_option_" + promise_seq + " option:selected").attr("item_qntt");				
		var stock_qntt = $("#stock_qntt_" + promise_seq).html();
		
		stock_qntt = getOnlyNumber(stock_qntt);			
		
		stock_qntt = parseInt(stock_qntt);
		item_qntt = parseInt(item_qntt);
		
		var box_qntt = stock_qntt / item_qntt;
		box_qntt = parseInt(box_qntt);		
		
		console.log(stock_qntt, item_qntt, box_qntt);			
		
		$("#box_" + promise_seq).html("(" + NumberFormat(box_qntt) + "ea)");	
		
		$("#img_" + promise_seq).html("<img src='/download?filepath=" + filepath + "' />");	
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/promise/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	
	            			console.log("PRODUCT_OPTION : " + response.list[i].PRODUCT_OPTION);
	            				            			
	            			var product_option = JSON.parse(response.list[i].PRODUCT_OPTION);	            			
	            			
	            			sHTML += "<li>";
	            			sHTML += "	<div class='img_prd' id='img_" + response.list[i].PROMISE_SEQ + "'>";	  
	            			sHTML += "		<img src='/download?filepath=" + product_option[0].option_image + "' alt=''>";	            				            			
	            			sHTML += "	</div>";
	            			sHTML += "	<div class='cont'>";
	            			sHTML += "		<label class='inp_checkbox'><input type='checkbox' name='chk' value='" + response.list[i].PROMISE_SEQ + "' /><span>" + response.list[i].PRODUCT_NM + "</span></label>";
	            			sHTML += "		<div class='tbl_basic'>";
	            			sHTML += "			<table class='view'>";
	            			sHTML += "				<tbody>";
	            			sHTML += "					<tr>";
	            			sHTML += "						<th>포장 단위</th>";
	            			sHTML += "						<td>";
	            			sHTML += "							<select name='product_option' item_info='" + response.list[i].PRODUCT_OPTION + "' id='product_option_" + response.list[i].PROMISE_SEQ + "' class='select1 w100p' onChange=\"optionChange('" + response.list[i].PROMISE_SEQ + "')\">";
	            			
	            			for(var k = 0; k < product_option.length; k++) {
	            				sHTML += "							<option filepath='" + product_option[k].option_image + "' item_qntt='" + product_option[k].item_qntt + "' value='" + product_option[k].bom_cd + "'>" + product_option[k].bom_nm + "</option>";
	            			}
	            			
	            			sHTML += "							</select>";
	            			sHTML += "						</td>";
	            			sHTML += "					</tr>";
							sHTML += "					<tr>";
							sHTML += "						<th>처방 단위</th>";
							sHTML += "						<td>" + response.list[i].QNTT + " " + response.list[i].UNIT + "</td>";
							sHTML += "					</tr>";
							sHTML += "					<tr>";
							sHTML += "						<th>처방량(월)</th>";
							sHTML += "						<td>" + NumberFormat(response.list[i].ORDER_QNTT) + "</td>";
							sHTML += "					</tr>";
							sHTML += "					<tr>";
							sHTML += "						<th>처방 기간</th>";
							sHTML += "						<td>" + NumberFormat(response.list[i].PERIOD) + "개월</td>";
							sHTML += "					</tr>";
							sHTML += "					<tr>";
							sHTML += "						<th>만료일자</th>";
							sHTML += "						<td>";
							sHTML += "							<div class='d-flex j-b'>";
							sHTML += response.list[i].END_DT;
						
							if(response.list[i].STATUS == "F") {
								sHTML += " <button type='button' class='btn-pk s red2' onclick='openLayerPopup('popEvent1');'><span>마감임박</span></button>"
							}
							
							sHTML += "							</div>";
							sHTML += "						</td>";
							sHTML += "					</tr>";
							sHTML += "					<tr>";
							sHTML += "						<th>발송 가능량</th>";
							
							var stock_qntt = parseInt(response.list[i].STOCK_QNTT);
							var item_qntt = parseInt(product_option[0].item_qntt);
							var box_qntt = stock_qntt / item_qntt;
							
							box_qntt = parseInt(box_qntt);							
							
							sHTML += "						<td><span id='stock_qntt_" + response.list[i].PROMISE_SEQ + "'>" + response.list[i].STOCK_QNTT + "</span> " + response.list[i].UNIT + " <span class='c-red' id='box_" + response.list[i].PROMISE_SEQ + "'>(" + box_qntt + "ea)</span></td>";
							sHTML += "					</tr>";
							sHTML += "				</tbody>";
							sHTML += "			</table>";
							sHTML += "		</div>";
							sHTML += "	</div>";
							sHTML += "</li>";
	            			
		            		startno--;
	            		}
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	 
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goSave() {
		if(ajaxRunning) return;
		
		var length = GetCheckedLength("chk");
		if(length == 0) {
			alert("하나 이상 선택해 주세요.");
			return;
		}
		
		length = $("input:checkbox[name='chk']").length;
		
		var formData = new FormData();
		
		for(var i = 0; i < length; i++) {
			if($("input:checkbox[name='chk']").eq(i).prop("checked")) {
				var promise_seq	= $("input:checkbox[name='chk']").eq(i).val();
				var bom_cd = $("select[name='product_option']").eq(i).val();
				
				console.log(promise_seq, bom_cd);		
				
				var box_qntt = $("#box_" + promise_seq).html();
				box_qntt = getOnlyNumber(box_qntt);
				box_qntt = parseInt(box_qntt);
				if(box_qntt <= 0) {
					alert("발송 가능 수량이 부족한 제품이 존재합니다.");
					return;
				}				
					
			    formData.append("LIST_PROMISE_SEQ[]", promise_seq);
			    formData.append("LIST_BOM_CD[]", bom_cd);			    
			}
		}	
		
		var url = "/promise/setBasket";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	location.href = "/promise/ready";
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });		
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">약속처방 배송요청</h2>
			</header>

			<div class="tbl_btn mb">
				<a href="javascript:" class="btn-pk blue n btnSave"><span>선택처방 모두담기</span></a>
			</div>

			<div class="lst_product2">
				<ul id="tbody">
					
				</ul>
			</div>

			<div class="tbl_btn">
				<a href="javascript:" class="btn-pk blue n btnSave"><span>선택처방 모두담기</span></a>
			</div>

			<div class="pagenation">
				
			</div>

		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>