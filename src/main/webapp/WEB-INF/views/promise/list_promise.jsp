<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	var PRODUCT_TP = "${PRODUCT_TP}";
	var CATEGORY1 = "";
	var CATEGORY2 = "";

	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	var CATEGORY = "";
	
	$(function() {		
		$('#PRODUCT_NM').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	    
	    
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#PRODUCT_NM").val("");
	    	$("#CATEGORY").val("");
	    	
	    	$("#sdate1").val("");
	    	$("#edate1").val("");
	    	
	    	$("#sdate2").val("");
	    	$("#edate2").val("");
	    	
	    	$(".period").val("");
	    	$(".status").prop("checked", false);	    	
	    	
	    	//$("#searchField").val("");
			$("#searchText").val("");
			
			$(".tab_category").removeClass("on");
			$(".tab_category").eq(0).addClass("on");
	    	setHash(1);
	    });
	});
	
	function setHash(page) {	
		var PRODUCT_NM = GetValue("PRODUCT_NM");
		//var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		var sdate1 = GetValue("sdate1");
		var edate1 = GetValue("edate1");
		var sdate2 = GetValue("sdate2");
		var edate2 = GetValue("edate2");
		
		var status = "";
		$("input:checkbox[name='status']:checked").each(function() {
			var value = $(this).val();
			if(value) {
				if(status != "") status += ",";
				status += value;
			}			
		});	
				
		var param = "page=" + page;
		param += "&PRODUCT_NM=" + PRODUCT_NM;
		//param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;	
		param += "&sdate1=" + sdate1;
		param += "&edate1=" + edate1;
		param += "&sdate2=" + sdate2;
		param += "&edate2=" + edate2;
		param += "&LIST_STATUS=" + status;
		param += "&CATEGORY1=" + CATEGORY1;
		param += "&CATEGORY2=" + CATEGORY2;	
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/promise/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	 
	            			
	            			sHTML += "<tr>";
	            			sHTML += "	<td>" + startno + "</td>";
	            			sHTML += "	<td>" + response.list[i].PRODUCT_NM + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_QNTT) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].STOCK_QNTT) + "</td>";
	            			sHTML += "	<td>" + response.list[i].PERIOD + "개월</td>";
	            			sHTML += "	<td>" + response.list[i].TURN + "/" + response.list[i].PERIOD + "</td>";
	            			sHTML += "	<td>조제기간 " + response.list[i].PERIOD_MAKE + "일<br>/(" + response.list[i].MAKE_DT + ")</td>";
	            			sHTML += "	<td>" + response.list[i].SEND_DT + "</td>";
	            			sHTML += "	<td>" + response.list[i].END_DT + "</td>";
	            			
	            			sHTML += "	<td>";	            			
	            			if(response.list[i].STATUS == "I") {
	            				sHTML += "	<button type='button' class='btn-pk s blue2 w100p'><span>진행중</span></button>";
	            			}
	            			else if(response.list[i].STATUS == "F") {
	            				sHTML += "	<button type='button' class='btn-pk s red2 w100p'><span>마감임박</span></button>";
	            			}
							if(response.list[i].STATUS == "E") {
								sHTML += "	<button type='button' class='btn-pk s gray w100p'><span>종료</span></button>";	
							}
							sHTML += "	</td>";
							
							sHTML += "	<td>";	            			
	            			if(response.list[i].STATUS == "I") {
	            				sHTML += "	<button type='button' class='btn-pk s blue2 w100p' onclick=\"showAddPromise('3', '" + response.list[i].PROMISE_SEQ + "')\"><span>사전처방 추가</span></button>";
	            			}
	            			else if(response.list[i].STATUS == "F") {
	            				sHTML += "	<button type='button' class='btn-pk s blue2 w100p' onclick=\"showAddPromise('2', '" + response.list[i].PROMISE_SEQ + "')\"><span>사전처방 연장</span></button>";
	            			}
							if(response.list[i].STATUS == "E") {
								sHTML += "	<button type='button' class='btn-pk s blue2 w100p' onclick=\"showAddPromise('1', '" + response.list[i].PROMISE_SEQ + "')\"><span>신규 등록</span></button>";	
							}
							sHTML += "	</td>";
	            			sHTML += "</tr>";
	            			
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='11'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goUpdate(recipe_seq) {
		location.href = "/recipe/update/" + recipe_seq + "/" + window.location.hash;
	}
	
	function setDate(type, period) {
		var today = getToday();
		if(period != "") {
			$("#sdate" + type).val(getPreMonth(today, parseInt(period)));
			$("#edate" + type).val(today);
		}
		else {
			$("#sdate" + type).val("");
			$("#edate" + type).val("");
		}
	}
	
	function setCategory1(obj, category) {
		$(".tab_category").removeClass("on");
		$(obj).parent().addClass("on");
		
		CATEGORY1 = category;
		CATEGORY2 = "";
		
		if(category != "" && category != "Y") {
			//2depth
			selectCategory(category);			
		}
		else {
			$("#category2").html("");
		}
		
		setHash(1);	
	}
	
	function setCategory2(obj, category) {
		$(".tab_category2").removeClass("on");
		$(obj).parent().addClass("on");
		
		CATEGORY2 = category;
		
		setHash(1);	
	}

	function selectCategory(p_code) {
		if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("PRODUCT_TP", PRODUCT_TP);
		formData.append("P_CODE", p_code);

	    var url = "/category/select";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));

	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var list = response.list;
	            	
	            	for(var i = 0; i < list.length; i++) {	  
	            		sHTML += "<li class='tab_category2'><a href='javascript:' onClick=\"setCategory2(this, '" + response.list[i].CODE + "')\"><span>" + response.list[i].NAME + "</span></a></li>";
	            	}
	            	
	            	$("#category2").html(sHTML);
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));

	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });	
	}
	
	function showAddPromise(type, promise_seq) {
		showPopup(800, 800, "/common/pop_promise_add?type=" + type + "&promise_seq=" + promise_seq);
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">사전처방 관리</h2>
			</header>

			<div class="tab ty2">
				<ul>
					<li class="tab_category on"><a href="javascript:" onClick="setCategory1(this, '')"><span>전체</span></a></li>
					<li class="tab_category"><a href="javascript:" onClick="setCategory1(this, 'Y')"><span>이벤트상품</span></a></li>
					
					<c:forEach var="i" begin="1" end="${LIST_CATEGORY1.size()}">
						<li class="tab_category"><a href="javascript:" onClick="setCategory1(this, '${LIST_CATEGORY1.get(i - 1).CODE}')"><span>${LIST_CATEGORY1.get(i - 1).NAME}</span></a></li>
					</c:forEach>
				</ul>
				<ul id="category2">
					
				</ul>
			</div>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c50">
						<p class="w80">발송가능일자</p>
						<div class="inp_cal">
							<input type="text" name="sdate1" id="sdate1" class="inp_txt calender datepicker_first date" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate1" id="edate1"  class="inp_txt calender datepicker_last date" readOnly placeholder="yyyy-mm-dd" />
						</div>
						<select class="select1 period" onChange="setDate('1', this.value)">
							<option value="">전체</option>
							<option value="-3">3개월</option>
							<option value="-6">6개월</option>
							<option value="-9">9개월</option>
							<option value="-12">12개월</option>
						</select>
					</div>
					<div class="col c50">
						<p class="w80">만료일자</p>
						<div class="inp_cal">
							<input type="text" name="sdate2" id="sdate2"  class="inp_txt calender datepicker_first date" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate2" id="edate2"  class="inp_txt calender datepicker_last date" readOnly placeholder="yyyy-mm-dd" />
						</div>
						<select class="select1 period" onChange="setDate('2', this.value)">
							<option value="">전체</option>
							<option value="-3">3개월</option>
							<option value="-6">6개월</option>
							<option value="-9">9개월</option>
							<option value="-12">12개월</option>
						</select>
					</div>
				</div>
				<div class="in">
					<div class="col c50">
						<p class="w80">처방상태</p>
						
						<div class="lst_chk">
							<!-- <label class="inp_checkbox"><input type="checkbox" name="status" class="status" value="" /><span>전체</span></label> -->
							<label class="inp_checkbox"><input type="checkbox" name="status" class="status" value="I" /><span>진행중</span></label>
							<label class="inp_checkbox"><input type="checkbox" name="status" class="status" value="F" /><span>마감임박</span></label>							
							<label class="inp_checkbox"><input type="checkbox" name="status" class="status" value="E" /><span>종료</span></label>
						</div>
					</div>
					<div class="col c50 sch">
						<p class="w80"><span>처방명</span></p>
						<input type="text" class="inp_txt wid1" name="PRODUCT_NM" id="PRODUCT_NM" placeholder="처방명을 입력하세요" />
						<input type="hidden" name="CATEGORY" id="CATEGORY" value="" />
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				<div class="rgh">
					
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num">
						<col>
						<col class="size2">
						<col class="size2">
						<col class="size2">
						<col class="size1">
						<col class="td1">
						<col class="day">
						<col class="day">
						<col class="size2">
						<col class="td1">
					</colgroup>
					<thead>
						<tr>
							<th>No</th>
							<th>처방명</th>
							<th>처방량(월)</th>
							<th>발송가능 수량</th>
							<th>처방기간</th>
							<th>회차</th>
							<th>조제완료일</th>
							<th>발송가능일자</th>
							<th>만료일자</th>
							<th>처방상태</th>
							<th>사전처방</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>