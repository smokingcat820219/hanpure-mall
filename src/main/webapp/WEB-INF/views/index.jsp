<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(function() {	 		
		$('#id').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	goLogin();
	        }
	    });	
		
		$('#pwd').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	goLogin();
	        }
	    });	
		
	    $('#btnLogin').click(function() {
	    	goLogin();
	    });
	    
	    var id = getCookie("ID");
	    if(getCookie("SAVE_ID") == "Y") {
	    	$("#id").val(id);
	    	$("#save_id").prop("checked", true);	    	
	    }
	    else {
	    	$("#id").val("");
	    	$("#save_id").prop("checked", false);
	    }		
	    
	    /*
	    navigator.clipboard.readText()
	    .then(text => {
	      console.log('Pasted content: ', text);
	    })
	    .catch(err => {
	      console.error('Failed to read clipboard contents: ', err);
	    });
	    */
	});
	
	function goLogin() {		
		if(ajaxRunning) return;
		
	    var id = GetValue("id");
	    var pwd = GetValue("pwd");  
	    
	    if(!id) {
	    	alert("아이디를 입력해 주세요.");
	    	$("#id").focus();
	    	return;
	    }  
	    if(!pwd) {
	    	alert("비밀번호를 입력해 주세요.");
	    	$("#pwd").focus();
	    	return;
	    }
	    
	    var formData = new FormData();
	    formData.append("ID", id);
	    formData.append("PWD", pwd);
	    
	    var url = "/login";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	 
	            	if(isChecked("save_id") == "Y") {
	            		setCookie("ID", id, null);	
	            		setCookie("SAVE_ID", "Y", null);
	            	}
	            	else {
	            		setCookie("ID", "", null);	
	            		setCookie("SAVE_ID", "", null);
	            	}
	            	
	            	location.reload();
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}	
</script>

<style>
	.wrap_login {display: table; width: 100%; height: 100vh; background-color: #132944;}
	.wrap_login .inner {display: table-cell; vertical-align: middle;}
	.wrap_login .box {max-width:400px; margin: 0 auto; padding: 50px; background-color: #fff;}
	.wrap_login .box .logo {margin-bottom: 40px; text-align: center;}
	.wrap_login .box div {margin-bottom: 20px;}
	.wrap_login .box p {font-weight: 700; font-size: 12px; margin-bottom: 5px;}
</style>

<body>

<div id="wrap">
	<div class="wrap_login">
		<div class="inner">
			<fieldset class="box">
				<h1 class="logo"><a href="javascript:"><img src="/images/common/logo.jpg" alt="한퓨어"></a></h1>
				
				<div class="inp">
					<div>
						<p>한의원 아이디</p>
						<input type="text" name="id" id="id" class="inp_txt w100p" placeholder="아이디를 입력하세요." />
					</div>
					<div>
						<p>비밀번호</p>
						<input type="password" name="pwd" id="pwd" class="inp_txt w100p" placeholder="비밀번호를 입력하세요." />
					</div>
					<div>
						<label class="inp_checkbox"><input type="checkbox" name="save_id" id="save_id" /><span>아이디저장</span></label>
					</div>
				</div>
				<div class="btn-bot">
					<button id="btnLogin" class="btn-pk b blue w100p"><span>로그인</span></button>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<!-- 로딩 -->
<div class="loadingbar" style="display: none;">
	<span><img src="/images/common/loading.gif" alt="로딩중"></span>
</div>


</body>
</html>