<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(function() {	 		
	    $('#btnList').click(function() {	    	
	    	location.href = "/herbs/list" + window.location.hash;
	    }); 
	});	
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">본초사전</h2>
			</header>

			<h3 class="h_stit1 f-en mb20">본초 상세 설명</h3>

			<!-- 좌측데이터 -->
			<div class="lft_c ty2">
				<div class="img_prd">
					<c:choose>
						<c:when test="${VO.FILEPATH ne null and VO.FILEPATH ne ''}">
							<img src="/download?filepath=${VO.FILEPATH}&filename=${VO.FILENAME}" alt="">		
						</c:when>
						<c:otherwise>
							<img src="/images/common/img_noimg.jpg" width="100%" height="100%" alt="사진">	
						</c:otherwise>
					</c:choose>
				</div>				
			</div><!--// lft_c -->

			<!-- 우측데이터 -->
			<div class="rgh_c ty2">
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>본초명</th>
								<td>${VO.HERBS_NM_KR}</td>
							</tr>
							<tr>
								<th>학명</th>
								<td>${VO.TOP_NM_KR}</td>
							</tr>
							<tr>
								<th>과명</th>
								<td>${VO.MID_NM_KR}</td>
							</tr>
							<tr>
								<th>별칭/이명</th>
								<td>${VO.NICK_NM_KR}</td>
							</tr>
							<tr>
								<th>성</th>
								<td>${VO.HABITUDE}</td>
							</tr>
							<tr>
								<th>미</th>
								<td>${VO.TEMPER}</td>
							</tr>
							<tr>
								<th>귀경</th>
								<td>${VO.ORGAN}</td>
							</tr>
							<tr>
								<th>중독</th>
								<td>${VO.POISON}</td>
							</tr>
							<tr>
								<th>법제</th>
								<td>${VO.PROCESS_KR}</td>
							</tr>
							<tr>
								<th>본초 설명</th>
								<td>${VO.DESC_KR}</td>
							</tr>
							<tr>
								<th>주의사항</th>
								<td>${CAUTION_KR}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div><!--// rgh_c -->

			<div class="clb btn-bot">
				<div class="lft">
					<a href="javascript:" id="btnList" class="btn-pk b blue2"><span>목록</span></a>
				</div>
			</div>

		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>