<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {	
		$('#btnExcelDownload').click(function() {
			location.href = "/pdata/excel/download?" + window.location.hash.substring(1);
	    });	
		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
			$("#searchField").val("");
			$("#searchText").val("");
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");		
		
		var param = "page=" + page;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);
		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/pdata/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {    
	            			sHTML += "<tr onClick=\"goView('" + response.list[i].PRE_CD + "')\">";
	            			sHTML += "	<td>" + startno + "</td>";
	            			sHTML += "	<td>" + response.list[i].PRE_SRC_BOOK + "</td>";
	            			sHTML += "	<td>" + response.list[i].PRE_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].PRE_MED + "</td>";
	            			sHTML += "	<td>" + response.list[i].PRE_MED_QNTT + "</td>";
	            			sHTML += "</tr>";
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='5'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goView(pre_cd) {
		location.href = "/pdata/view/" + pre_cd + "/" + window.location.hash;
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">방제사전</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c50 sch">
						<p>검색</p>
						<select name="searchField" id="searchField" class="select1 wid1">
							<option value="">전체</option>
							<option value="1">출전</option>
							<option value="2">방제명</option>
							<option value="3">약재구성</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력하세요" />
						
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				
				<div class="rgh">
					<a href="javascript:;" id="btnExcelDownload" class="btn-pk blue2 n"><span>엑셀 다운로드</span></a>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col width="80px" />
						<col width="300px" />
						<col width="200px" />												
						<col />
						<col width="100px" />
					</colgroup>
					<thead>
						<tr>
							<th>No</th>
							<th>출전</th>
							<th>방제명</th>
							<th>약재구성</th>
							<th>약미</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>