<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(function() {		    	
		$('#btnList').click(function() {	    	
	    	location.href = "/pdata/list" + window.location.hash;
	    });	  	    
	});	
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">방제사전</h2>
			</header>

			<h3 class="h_stit1 f-en mb20">상세 설명</h3>

			<!-- 우측데이터 -->
			<div class="ty2">
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>출전</th>
								<td>${VO.PRE_SRC_BOOK}</td>
							</tr>
							<tr>
								<th>방제명</th>
								<td>${VO.PRE_NM}</td>
							</tr>
							<tr>
								<th>약미</th>
								<td>${VO.PRE_MED_QNTT}</td>
							</tr>	
							<tr>
								<th>약재구성</th>
								<td>
									<table class="write mt-1">
										<colgroup>							
											<col width="150px" />
											<col />
										</colgroup>
										<thead>
											<tr>
												<th class="ta-c">번호</th>
												<th class="ta-c">약재명</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="i" begin="1" end="${LIST.size()}">
												<tr>
													<td>${i}</td>
													<td>${LIST.get(i - 1).PRE_MED}</td>
												</tr>												
											</c:forEach>	
										</tbody>
									</table>
								</td>
							</tr>	
						</tbody>
					</table>
				</div>
			</div><!--// rgh_c -->

			<div class="clb btn-bot">
				<div class="lft">
					<a href="javascript:" id="btnList" class="btn-pk b blue2"><span>목록</span></a>
				</div>
			</div>

		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>