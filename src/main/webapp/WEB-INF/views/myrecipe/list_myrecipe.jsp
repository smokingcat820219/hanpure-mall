<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }
	    
	    $('.btnWrite').click(function() {
	    	location.href = "/myrecipe/write" + window.location.hash;
	    });
	    
	    $('.btnDelete').click(function() {
			if(ajaxRunning) return;
			
			var length = GetCheckedLength("chk");
			if(length == 0) {
				alert("하나 이상 선택해 주세요.");
				return;
			}
			
			var formData = new FormData();
			
			for(var i = 0; i < length; i++) {
				var recipe_seq = $("input:checkbox[name='chk']:checked").eq(i).val();
				console.log("recipe_seq : " + recipe_seq);
				
				formData.append("LIST_RECIPE_SEQ", recipe_seq);
			}	
			
		    if(!confirm("선택하신 항목을 삭제 하시겠습니까?")) {
		    	return;
		    }
		    
		    var url = "/myrecipe/delete";
		    $.ajax({
		        type:"POST",
		        url:url,
		        data:formData,
		        cache: false,
		        processData: false,  // file전송시 필수
		        contentType: false,  // file전송시 필수
		        beforeSend: function() {
		        	ShowCSS($(".loadingbar"));
		        },
		        success:function(response) {
		        	HideCSS($(".loadingbar"));
		        	
		            if(response.result == 200) {	 
		            	goPage();
		            }
		            else {
		            	alert(response.message);
		            }
		        },
		        error:function(request, status, error) {
		        	HideCSS($(".loadingbar"));
		        	
		            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    });
	    });
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#searchField").val("");
			$("#searchText").val("");
			$("#RECIPE_TP").val("");		
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var itemPerPage = GetValue("itemPerPage");
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");		
		var RECIPE_TP = GetValue("RECIPE_TP");
		
				
		var param = "page=" + page;
		param += "&itemPerPage=" + itemPerPage;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;		
		param += "&RECIPE_TP=" + RECIPE_TP;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);
		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/myrecipe/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {    
	            			sHTML += "<tr>";
	            			sHTML += "	<td><label class='inp_checkbox'><input type='checkbox' name='chk' class='chk' value='" + response.list[i].RECIPE_SEQ + "' /><span class='d' title='전체선택'></span></label></td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + startno + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].RECIPE_TP_NM + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].RECIPE_NM + "</td>";
	            			
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">";	            			
	            			
	            			if(response.list[i].RECIPE_TP == "001") {
	            				var sInfo = "";
	            				sInfo += response.list[i].INFO1 + "첩";
	            				sInfo += "," + NumberFormat(response.list[i].INFO2) + "팩";
	            				sInfo += "," + NumberFormat(response.list[i].INFO3) + "mL";
	            				sInfo += "," + response.list[i].INFO4;	            				
	            				sInfo += "," + response.list[i].INFO5;	            				
	            				sInfo += "," + NumberFormat(response.list[i].INFO6) + "mL";
	            				
	            				if(response.list[i].INFO7 == "Y") {
	            					sInfo += ", 향미제 선택없음";	
	            				}
	            				else {
	            					sInfo += ", 향미제 선택";
	            				}
	            				
	            				if(response.list[i].INFO8) {
	            					sInfo += ", 앰플(자하거) " + NumberFormat(response.list[i].INFO8) + "mL";	
	            				}
	            				else {
	            					sInfo += ", 앰플(자하거) 선택없음";
	            				}
	            				
	            				//주수상반
	            				if(response.list[i].INFO9) {
	            					//코드
	            					sInfo += "," + response.list[i].INFO9;
	            					sInfo += "," + response.list[i].INFO10 + "mL";
	            				}
	            				else {
	            					sInfo += ", 주수상반 선택없음";
	            				}
	            				
	            				sHTML += sInfo;	    
	            			}
	            			else if(response.list[i].RECIPE_TP == "002") {
	            				var sInfo = "";
	            				sInfo += response.list[i].INFO1 + "첩";
	            				
	            				if(response.list[i].INFO2 == "Y") {
	            					sInfo += "," + "농축 있음";
	            				}
	            				else {
	            					sInfo += "," + "농축 없음";	
	            				}
	            				
	            				sInfo += "," + response.list[i].INFO3;
	            				sInfo += "," + response.list[i].INFO4;	            				
	            				sInfo += "," + response.list[i].INFO5;	            				
	            				sInfo += ", 제분손실량 " + NumberFormat(response.list[i].INFO6) + "g";
	            				sInfo += ", 제환손실양 " + NumberFormat(response.list[i].INFO7) + "g";
	            				sInfo += ", 부형제량 " + NumberFormat(response.list[i].INFO8) + "g";
	            				
	            				sHTML += sInfo;	    
	            			}
	            			else if(response.list[i].RECIPE_TP == "003") {
	            				var sInfo = "";
	            				sInfo += NumberFormat(response.list[i].INFO1) + "첩";
	            				sInfo += "," + NumberFormat(response.list[i].INFO2) + "포";
	            				
	            				sHTML += sInfo;	    
							}
	            			sHTML += "	</td>";	            			
	            				            			
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].DRUG_NM + "</td>";	            			
	            			sHTML += "</tr>";
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='6'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goUpdate(recipe_seq) {
		location.href = "/myrecipe/update/" + recipe_seq + "/" + window.location.hash;
	}
	
	function allCheck(obj) {
		if($(obj).prop("checked")) {
			$(".chk").prop("checked", true);
		}
		else {
			$(".chk").prop("checked", false);
		}
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">나의 처방</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c15">
						<p>처방 유형</p>
						<select name="RECIPE_TP" id="RECIPE_TP" class="select1">
							<option value="">전체</option>
							<c:forEach var="i" begin="1" end="${LIST_RECIPE_TP.size()}">
								<c:if test="${LIST_RECIPE_TP.get(i - 1).SUB_CD ne '004'}">
									<option value="${LIST_RECIPE_TP.get(i - 1).SUB_CD}">${LIST_RECIPE_TP.get(i - 1).SUB_NM_KR}</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
					<div class="col c75 sch">
						<select name="searchField" id="searchField" class="select1">
							<option value="">전체</option>
							<option value="1">처방명</option>
							<option value="2">약재명</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" />
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				<select name="itemPerPage" id="itemPerPage" class="select1">
					<option value="10">10개씩 보기</option>
					<option value="20">20개씩 보기</option>
					<option value="30">30개씩 보기</option>
					<option value="40">40개씩 보기</option>
					<option value="50">50개씩 보기</option>
				</select>
				<div class="rgh">
					<a href="javascript:" class="btn-pk n blue btnWrite"><span>신규등록</span></a>
					<a href="javascript:" class="btn-pk n white btnDelete"><span>삭제</span></a>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num">
						<col class="num">
						<col class="size1">
						<col class="td2">
						<col>
						<col>
					</colgroup>
					<thead>
						<tr>
							<th><label class="inp_checkbox"><input type="checkbox" onClick="allCheck(this)" /><span class="d" title="전체선택"></span></label></th>
							<th>순번</th>
							<th>처방구분</th>
							<th>처방명</th>
							<th>정보</th>
							<th>약재 구성</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="tbl_botm ta-r">
				<a href="javascript:" class="btn-pk n blue btnWrite"><span>신규등록</span></a>
				<a href="javascript:" class="btn-pk n white btnDelete"><span>삭제</span></a>
			</div>
			<div class="pagenation">
				<ul>
					
				</ul>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>