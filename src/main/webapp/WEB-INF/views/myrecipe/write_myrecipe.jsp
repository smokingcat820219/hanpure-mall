<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>	
	var CLASS = "${SESSION.CLASS}";
	var POISON = "${POISON}";
	var CONFLICT = ${CONFLICT};	
</script>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script src="/js/recipe.js"></script>

<script>	
	$(function() {
		/*
		//자동 저장 2분
		setInterval(function() {
			goSave("AUTO");
			  
		}, 1000 * 60 * 2);	//2분
		*/
	    
	    $('#btnInit').click(function() {
	    	deleteDrugAll();
	    });
	    
	    $('#btnDrugDelete').click(function() {
	    	deleteDrug();
	    });	  
	    
	    $('#btnList').click(function() {
	    	if(!confirm("등록중이던 정보는 저장되지 않습니다. 목록으로 이동 하시겠습니까?")) {
		    	return;
		    }
	    	
	    	location.href = "/myrecipe/list" + window.location.hash;;
	    });
	    
	    $('#btnSave').click(function() {
	    	goSave("SAVE");
	    });
	    
		tab(".tab_c2", 1);
	    
		var recipe_tp = GetRadioValue("recipe_tp");		
		
		//addPriceList();
		
		setRecipeTp(recipe_tp);
		
		selectStick("200");
				
		calc();
	});
	
	function goSave() {
		if(ajaxRunning) return;
				
		var recipe_tp = GetRadioValue("recipe_tp");
		var recipe_nm = GetValue("recipe_nm");
		
		if(!recipe_nm) {
			alert("처바영을 입력해 주세요.");
			$("#recipe_nm").focus();
			return;
		}
		
		var length = $("input:checkbox[name='drug_cd']").length;
		if(length == 0) {
			alert("적어도 하나 이상의 약재를 추가해 주세요.");
			return;
		}	

		var info1 = "";
		var info2 = "";
		var info3 = "";
		var info4 = "";
		var info5 = "";
		var info6 = "";
		var info7 = "";
		var info8 = "";
		var info9 = "";
		var info10 = "";
		var info10_qntt = "";
		var info11 = "";
		
		var marking_tp = "";
		var marking_text = "";
		
		var package1 = "";
		var package2 = "";
		var package3 = "";
		var package4 = "";
		var vaccum = "N";		
		
		if(recipe_tp == "001") {
			info1 = $.trim($("#form1 #info1").val());
			info2 = $.trim($("#form1 #info2").val());
			info3 = $.trim($("#form1 #info3").val());
			info4 = $.trim($("#form1 #info4").val());
			info5 = $.trim($("#form1 #info5").html());
			info6 = $.trim($("#form1 #info6").html());
			info7 = $.trim($("#form1 #info7").val());
			info8 = $.trim($("#form1 #info8").val());
			info9 = $.trim($("#form1 #info9").val());			
			info10 = $("#form1 #info10").attr("code");
			info10_qntt = $("#form1 #info10").attr("value");		
			info11 = $.trim($("#form1 #info11").val());
			
			if(!info10) info10 = "";
			if(!info10_qntt) info10_qntt = "";			
			
			info6 = getOnlyFloat(info6);
			
			if(!info11) {
				alert("탕전시간을 입력해 주세요.");
				$("#form1 #info11").focus();
				return;
			}	
			
			marking_tp = $("#form1 #btnMarking").attr("marking_tp");
			marking_text = $("#form1 #btnMarking").attr("marking_text");			
			package1 = $("#form1 #btnPackage1").attr("bom_cd");
			package2 = $("#form1 #btnPackage2").attr("bom_cd");
			package3 = $("#form1 #btnPackage3").attr("bom_cd");
			
			vaccum = $("#form1 #btnPackage2").attr("vaccum");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			if(!vaccum) vaccum = "N";			
		}
		else if(recipe_tp == "002") {
			info1 = $.trim($("#form2 #info1").val());
			info2 = $.trim($("#form2 #info2").val());
			info3 = $.trim($("#form2 #info3").val());
			info4 = $.trim($("#form2 #info4").val());
			info5 = $.trim($("#form2 #info5").val());			
			info6 = $.trim($("#form2 #info6").html());
			info7 = $.trim($("#form2 #info7").html());
			info8 = $.trim($("#form2 #info8").html());
			
			info6 = getOnlyFloat(info6);
			info7 = getOnlyFloat(info7);
			info8 = getOnlyFloat(info8);	
			
			marking_tp = $("#form2 #btnMarking").attr("marking_tp");
			marking_text = $("#form2 #btnMarking").attr("marking_text");			
			package1 = $("#form2 #btnPackage1").attr("bom_cd");
			package2 = $("#form2 #btnPackage2").attr("bom_cd");
			package3 = $("#form2 #btnPackage3").attr("bom_cd");
			package4 = $("#form2 #btnPackage4").attr("bom_cd");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			if(!package4) package4 = "";
		}
		else if(recipe_tp == "003") {
			info1 = $.trim($("#form3 #info1").val());
			info2 = $.trim($("#form3 #info2").val());
			
			marking_tp = $("#form3 #btnMarking").attr("marking_tp");
			marking_text = $("#form3 #btnMarking").attr("marking_text");			
			package1 = $("#form3 #btnPackage1").attr("bom_cd");
			package2 = $("#form3 #btnPackage2").attr("bom_cd");
			package3 = $("#form3 #btnPackage3").attr("bom_cd");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
		}	
		else if(recipe_tp == "004") {
			info1 = $.trim($("#form4 #info1").val());
			info2 = $.trim($("#form4 #info2").val());
			info3 = $.trim($("#form4 #info3").val());
			info4 = $.trim($("#form4 #info4").val());
			info5 = $.trim($("#form4 #info5").html());
			info6 = $.trim($("#form4 #info6").html());
			info7 = $.trim($("#form4 #info7").val());
			info8 = $.trim($("#form4 #info8").val());
			info9 = $.trim($("#form4 #info9").val());			
			info10 = $("#form4 #info10").attr("code");
			info10_qntt = $("#form4 #info10").attr("value");		
			info11 = $.trim($("#form4 #info11").val());
			
			if(!info10) info10 = "";
			if(!info10_qntt) info10_qntt = "";			
			
			info6 = getOnlyFloat(info6);
			
			if(!info11) {
				alert("탕전시간을 입력해 주세요.");
				$("#form4 #info11").focus();
				return;
			}	
			
			marking_tp = $("#form4 #btnMarking").attr("marking_tp");
			marking_text = $("#form4 #btnMarking").attr("marking_text");			
			package1 = $("#form4 #btnPackage1").attr("bom_cd");
			package2 = $("#form4 #btnPackage2").attr("bom_cd");
			package3 = $("#form4 #btnPackage3").attr("bom_cd");
			
			vaccum = $("#form4 #btnPackage2").attr("vaccum");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			if(!vaccum) vaccum = "N";
		}
		
	    var formData = new FormData();	   
	    formData.append("RE_HOTPOT", GetValue("re_hotpot"));
		formData.append("RECIPE_TP", recipe_tp);
		formData.append("RECIPE_NM", recipe_nm);
		formData.append("INFO1", info1);
		formData.append("INFO2", info2);
		formData.append("INFO3", info3);
		formData.append("INFO4", info4);
		formData.append("INFO5", info5);
		formData.append("INFO6", info6);
		formData.append("INFO7", info7);
		formData.append("INFO8", info8);
		formData.append("INFO9", info9);
		formData.append("INFO10", info10);
		formData.append("INFO10_QNTT", info10_qntt);
		formData.append("INFO11", info11);
		
		formData.append("MARKING_TP", marking_tp);
		formData.append("MARKING_TEXT", marking_text);
		formData.append("PACKAGE1", package1);
		formData.append("PACKAGE2", package2);
		formData.append("PACKAGE3", package3);
		formData.append("PACKAGE4", package4);
		formData.append("VACCUM", vaccum);
		
		var total_chup = getOnlyFloat($("#sum_total_chup").html());
		var total_qntt = getOnlyFloat($("#sum_total_qntt").html());
		var total_price = getOnlyNumber($("#sum_total_price").html());
		
		formData.append("TOTAL_CHUP", total_chup);
		formData.append("TOTAL_QNTT", total_qntt);
		formData.append("TOTAL_PRICE", total_price);
		
		if(recipe_tp == "002") {
			if(parseInt(total_qntt) < 600) {
				alert("제환은 600g 이상만 처방이 가능합니다.");
				return;
			}
		}
		
		var drug_names = "";		
		
		var drug_length = $("input:checkbox[name='drug_cd']").length;
		if(drug_length == 0) {
			alert("적어도 하나 이상의 약재를 추가해 주세요.");
			return;
		}
		
		for(var i = 0; i < drug_length; i++) {
			var drug_cd = $("input:checkbox[name='drug_cd']").eq(i).val();
			var drug_nm = $("input:checkbox[name='drug_cd']").eq(i).data("name");
			//var stock_qntt = $("input:checkbox[name='drug_cd']").eq(i).data("stock");
			var price = $("input:checkbox[name='drug_cd']").eq(i).data("price");
			var origin = $("input:checkbox[name='drug_cd']").eq(i).data("origin");
			
			var total_qntt = getOnlyFloat($(".total_qntt").eq(i).html());
			var total_price = getOnlyNumber($(".total_price").eq(i).html());
						
			var chup1 = $("input[name='chup1']").eq(i).val();
			var turn = $("select[name='turn']").eq(i).val();
			
			if(!chup1) {
				alert("1첩량을 입력해 주세요.");
				$("input[name='chup1']").eq(i).focus();
				return;
			}
			
			if(parseInt(chup1) <= 0) {
				alert("1첩량을 입력해 주세요.");
				$("input[name='chup1']").eq(i).focus();
				return;
			}
			
			//if(parseInt(stock_qntt) < parseInt(total_qntt)) {
			//	alert("재고량보다 주문량이 많습니다.");
			//	$("input[name='chup1']").eq(i).focus();
			//	return;
			//}
			
			formData.append("LIST_DRUG_CD", drug_cd);
			formData.append("LIST_DRUG_NM", drug_nm);
			formData.append("LIST_TURN", turn);
			formData.append("LIST_CHUP1", chup1);	
			formData.append("LIST_ORIGIN", origin);	
			formData.append("LIST_PRICE", price);
			formData.append("LIST_TOTAL_QNTT", total_qntt);
			formData.append("LIST_TOTAL_PRICE", total_price);
			
			if(drug_names != "") {
				drug_names += ",";
			}
			
			drug_names += drug_nm;
		}
		
		formData.append("DRUG_NM", drug_names);		
		
		/*
		if(!package1) {
			if(recipe_tp == "001") {
				alert("파우치 포장정보를 선택해 주세요.");
				return;
			}
			else if(recipe_tp == "002") {
				if(info3 == "004") {
					alert("내지 포장정보를 선택해 주세요.");
					return;
				}				
			}
			else if(recipe_tp == "003") {
				alert("스틱 포장정보를 선택해 주세요.");
				return;
			}
			else if(recipe_tp == "004") {
				alert("파우치 포장정보를 선택해 주세요.");
				return;
			}
		}
		
		if(!package2) {
			if(recipe_tp == "002") {
				if(info3 != "004") {
					alert("소포장 포장정보를 선택해 주세요.");	
					return;
				}				
			}
		}	
		*/
		
	    if(!confirm("입력하신 내용을 등록 하시겠습니까?")) {
	    	return;
	    }
	    
	    var url = "/myrecipe/write";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
					alert("입력하신 내용을 등록 완료 되었습니다.");
	            	
	            	location.href = "/myrecipe/list" + window.location.hash;      	
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}		
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">처방전</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>처방구분</th>
								<td colspan="3">
									<c:forEach var="i" begin="1" end="${LIST_RECIPE_TP.size()}">
										<c:if test="${LIST_RECIPE_TP.get(i - 1).SUB_CD ne '004'}">
											<label class="inp_radio mr20"><input type="radio" name="recipe_tp" value="${LIST_RECIPE_TP.get(i - 1).SUB_CD}" onClick="setRecipeTp(this.value)" <c:if test="${i == 1}">checked</c:if> /><span>${LIST_RECIPE_TP.get(i - 1).SUB_NM_KR}</span></label>
										</c:if>									
									</c:forEach>	
									<button type="button" id="btnSearchHospital" price_class="${SESSION.CLASS}" style="display:none"></button>	
								</td>
							</tr>
							<tr>
								<th>처방명 <span class="i_emp">*</span></th>
								<td colspan="3">
									<div class="inp_btn">
										<input type="text" name="recipe_nm" id="recipe_nm" class="inp_txt w100p" placeholder="처방명을 입력해주세요." />
										<button type="button" id="btnInit" class="btn-pk n white"><span>초기화</span></button>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table class="list small">
						<colgroup>
							<col class="chk" />
							<col class="num" />
							<col />
							<col class="size1" />
							<col />
							<col />
							<col />
							<col />
							<col />
						</colgroup>
						<thead>
							<tr>
								<th><label class="inp_checkbox"><input type="checkbox" onClick="allCheck(this)"><span class="d"></span></label></th>
								<th>번호</th>
								<th>약재명</th>
								<th>유형</th>
								<th>원산지</th>
								<th>가격</th>
								<th>1첩량</th>
								<th>총 용량</th>
								<th>약재비</th>
							</tr>
						</thead>
						<tbody id="tbody">
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6" class="ta-l">
									<button type="button" id="btnDrugDelete" class="btn-pk s blue"><span>선택삭제</span></button>
								</td>
								<td class="bg ta-r"><span id="sum_total_chup">0</span>g</td>
								<td class="bg ta-r"><span id="sum_total_qntt">0</span>g</td>
								<td class="bg ta-r"><strong class="c-red fz-b1"><span id="sum_total_price">0</span>원</strong></td>
							</tr>
						</tfoot>
					</table>
				</div>


				<form id="form1" <c:if test="${RECIPE_TP ne '001'}">style="display:none"</c:if>>				
					<h3 class="h_tit2">전탕 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list small">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>팩수</th>
									<th>팩 용량</th>
									<th>특수탕전</th>
									<th>탕전법</th>
									<th>탕전 물양</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="calc()">											
											<c:forEach var="i" begin="1" end="2000">
												<option value="${i}" <c:if test="${i == 20}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											<c:forEach var="i" begin="1" end="120">
												<option value="${i}" <c:if test="${i == 40}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 팩</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info3" id="info3" onChange="calc()">
											<c:forEach var="i" begin="50" end="130">
												<option value="${i}" <c:if test="${i == 120}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info4" id="info4" onChange="calc()">
											<c:forEach var="i" begin="1" end="${LIST_SPECIAL_TP.size()}">
												<option value="${LIST_SPECIAL_TP.get(i - 1).SUB_CD}" data-value='${LIST_SPECIAL_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_SPECIAL_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td><span id="info5">표준(선무후문)</span></td>
									<td><span id="info6">0</span> mL</td>
								</tr>
								<tr>
									<th>향미제(초코)</th>
									<th>앰플(자하거)</th>									
									<th>녹용별전</th>
									<th>주수상반</th>
									<th>재탕전</th>
									<th>탕전시간(분)</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info7" id="info7" onChange="calc()">
											<option value="N">선택없음</option>
											<option value="Y">선택</option>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info8" id="info8" onChange="calc()">
											<option value="">선택없음</option>
											<c:forEach var="i" begin="10" end="200" step="10">
												<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info9" id="info9" onChange="calc()">
											<option value="">선택없음</option>
											<c:forEach var="i" begin="10" end="200" step="10">
												<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
											</c:forEach>	
										</select>
									</td>
									<td>										
										<div class="btn-pk ss blue3 w100p" id="info10" onClick="showDrink()">선택 없음</div>
									</td>
									<td>
										<select class="select1 w100p" name="re_hotpot" id="re_hotpot" onChange="calc()">
											<option value="N">선택없음</option>
											<option value="Y">재탕전</option>
										</select>
									</td>
									<td><input type="text" name="info11" id="info11" class="inp_txt w100p number" value="120" maxlength="3" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형 <span class="i_emp">*</span></th>
									<th>파우치 <span class="i_emp">*</span></th>
									<th>탕약박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>						
					</div>
				</form>
				
				<form id="form2" <c:if test="${RECIPE_TP ne '002'}">style="display:none"</c:if>>		
					<h3 class="h_tit2">제환 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list small">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>농축</th>
									<th>제형</th>
									<th>부형제</th>
									<th>분말도</th>
									<th>제분손실량</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="calc()">
											<c:forEach var="i" begin="1" end="2000">
												<option value="${i}" <c:if test="${i == 20}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											<c:forEach var="i" begin="1" end="${LIST_PRESS_TP.size()}">
												<option value="${LIST_PRESS_TP.get(i - 1).SUB_CD}" data-value='${LIST_PRESS_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_PRESS_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info3" id="info3" onChange="showBox002(this.value)">
											<c:forEach var="i" begin="1" end="${LIST_CIRCLE_SIZE.size()}">
												<option value="${LIST_CIRCLE_SIZE.get(i - 1).SUB_CD}" data-value='${LIST_CIRCLE_SIZE.get(i - 1).SUB_VALUE_KR}'>${LIST_CIRCLE_SIZE.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info4" id="info4" onChange="calc()">
											<c:forEach var="i" begin="1" end="${LIST_DOUGH_TP.size()}">
												<option value="${LIST_DOUGH_TP.get(i - 1).SUB_CD}" data-value='${LIST_DOUGH_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_DOUGH_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info5" id="info5" onChange="calc()">
											<c:forEach var="i" begin="1" end="${LIST_POWDER_TP.size()}">
												<option value="${LIST_POWDER_TP.get(i - 1).SUB_CD}" data-value='${LIST_POWDER_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_POWDER_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>									
									</td>
									<td><span id="info6">0</span> g</td>
								</tr>
								<tr>
									<th>제환손실량</th>
									<th>부형제량</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
								<tr>
									<td><span id="info7">0</span> g</td>
									<td><span id="info8">0</span> g</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>내지</th>
									<th>소포장</th>
									<th>포장박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage4" onclick="showPackage('4')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package4">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>											
					</div>
				</form>
				
				
				<form id="form3" <c:if test="${RECIPE_TP ne '003'}">style="display:none"</c:if>>									
					<h3 class="h_tit2">연조엑스 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>스틱수</th>
									<td colspan="2" rowspan="2" class="ta-l">
										&nbsp;&nbsp;&nbsp;&nbsp;* 탕전비 : 1탕전 50첩 기준량  3,750g   단가 : 10,000원<br />
										&nbsp;&nbsp;&nbsp;&nbsp;(ex, 200첩 : 50첩 x 4탕 = 40,000원  )<br />
										&nbsp;&nbsp;&nbsp;&nbsp;* 스틱포장비(살균처리) : 1포당 300원<br /> 
										&nbsp;&nbsp;&nbsp;&nbsp;* 스틱, 케이스 가격별도<br />
									</td>
								</tr>								
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="selectStick(this.value); calc()">
											<c:forEach var="i" begin="100" end="400" step="100">
												<option value="${i}" <c:if test="${i == 200}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
											</c:forEach>	
										</select>
									</td>
									<td>	
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>스틱</th>
									<th>케이스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1', '010')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2', '011');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3', '003')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>										
					</div>
				</form>
				
				<form id="form4" <c:if test="${RECIPE_TP ne '004'}">style="display:none"</c:if>>				
					<h3 class="h_tit2">전탕 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list small">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>팩수</th>
									<th>팩 용량</th>
									<th>특수탕전</th>
									<th>탕전법</th>
									<th>탕전 물양</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="calc()">
											<option value="10">10 첩</option>
											<option value="20">20 첩</option>									
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											<c:forEach var="i" begin="1" end="120">
												<option value="${i}" <c:if test="${i == 40}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 팩</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info3" id="info3" onChange="calc()">
											<c:forEach var="i" begin="50" end="130">
												<option value="${i}" <c:if test="${i == 120}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info4" id="info4" onChange="calc()">
											<c:forEach var="i" begin="1" end="${LIST_SPECIAL_TP.size()}">
												<option value="${LIST_SPECIAL_TP.get(i - 1).SUB_CD}" data-value='${LIST_SPECIAL_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_SPECIAL_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td><span id="info5">표준(선무후문)</span></td>
									<td><span id="info6">0</span> mL</td>
								</tr>
								<tr>
									<th>향미제(초코)</th>
									<th>앰플(자하거)</th>									
									<th>녹용별전</th>
									<th>주수상반</th>
									<th>탕전시간(분)</th>
									<td></td>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info7" id="info7" onChange="calc()">
											<option value="N">선택없음</option>
											<option value="Y">선택</option>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info8" id="info8" onChange="calc()">
											<option value="">선택없음</option>
											<c:forEach var="i" begin="10" end="200" step="10">
												<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
											</c:forEach>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info9" id="info9" onChange="calc()">
											<option value="">선택없음</option>
											<c:forEach var="i" begin="10" end="200" step="10">
												<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
											</c:forEach>	
										</select>
									</td>
									<td>										
										<div class="btn-pk ss blue3 w100p" id="info10" onClick="showDrink()">선택 없음</div>
									</td>
									<td><input type="text" name="info11" id="info11" class="inp_txt w100p number" value="120" maxlength="3" /></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>파우치</th>
									<th>탕약박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1', '001')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2', '002');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3', '003')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>				
			</div><!--// lft_c -->

			<!-- 우측데이터 -->
			<div class="rgh_c">
				<h3 class="h_tit2">약재/처방 추가</h3>
				<div class="top_tit">
					<div class="tab ty1 tab_c2">
						<ul class="n4">						
							<c:choose>
								<c:when test="${RECIPE_TP eq '004'}">
									<li style="margin: 0 3px;"><a href="#tab2_c1"><span>약재검색</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c2"><span>첩약사전</span></a></li>
								</c:when>
								<c:otherwise>
									<li style="margin: 0 3px;"><a href="#tab2_c1"><span>약재검색</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c2"><span>나의처방</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c3"><span>이전처방</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c4"><span>방제사전</span></a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
				<div class="tabcont pr-mb1">
					<c:choose>
						<c:when test="${RECIPE_TP eq '004'}">
							<div id="tab2_c1">
								<iframe src="/common/search_drug" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c2">
								<iframe id="tab2_c2_iframe" frameborder="0"></iframe>
							</div>
						</c:when>
						<c:otherwise>
							<div id="tab2_c1">
								<iframe src="/common/search_drug" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c2">
								<iframe id="tab2_c2_iframe" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c3">
								<iframe id="tab2_c3_iframe" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c4">
								<iframe src="/common/search_pdata" frameborder="0"></iframe>
							</div>
						</c:otherwise>
					</c:choose>					
				</div>
			</div><!--// rgh_c -->
			
			<div class="botm_c">
				<div class="mbtn_ty1 mt20">
					<a href="javascript:;" id="btnSave" class="btn-pk b blue"><span>저장</span></a>
					<a href="javascript:;" id="btnList" class="btn-pk b blue2"><span>취소</span></a>
				</div>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/editor2.jsp"%>

<%@ include file="/WEB-INF/views/include/footer.jsp" %>