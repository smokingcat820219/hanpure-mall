<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script>
	$(function() {			
		$('#btnPre').click(function() {
			history.back();
	    });
		
		$('#btnSave').click(function() {
			goSave();
	    });
	});

	function OpenDaumZipEx(type) {
		new daum.Postcode({
			oncomplete: function(data) {
				$("#" + type + "_zipcode").val(data.zonecode);
				$("#" + type + "_address").val(data.roadAddress);
			}
		}).open();
	}

	function setSender(value) {
		if(value == "1") {
			$("#send_nm").val("${SESSION.COMPANY}");
			
			var tel = "${SESSION.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#send_tel1").val(tel_array[0]);
					$("#send_tel2").val(tel_array[1]);
					$("#send_tel3").val(tel_array[2]);
				}
				else {
					$("#send_tel1").val("");
					$("#send_tel2").val("");
					$("#send_tel3").val("");
				}
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
			
			var mobile = "${SESSION.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#send_mobile1").val(mobile_array[0]);
					$("#send_mobile2").val(mobile_array[1]);
					$("#send_mobile3").val(mobile_array[2]);
				}
				else {
					$("#send_mobile1").val("");
					$("#send_mobile2").val("");
					$("#send_mobile3").val("");
				}
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
			
			$("#send_zipcode").val("${SESSION.ZIPCODE}");
			$("#send_address").val("${SESSION.ADDRESS}");
			$("#send_address2").val("${SESSION.ADDRESS2}");
		}
		else if(value == "2") {
			//탕전실
			$("#send_nm").val("탕전실");
			
			var tel = "${HANPURE.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#send_tel1").val(tel_array[0]);
					$("#send_tel2").val(tel_array[1]);
					$("#send_tel3").val(tel_array[2]);
				}
				else {
					$("#send_tel1").val("");
					$("#send_tel2").val("");
					$("#send_tel3").val("");
				}
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
			
			var mobile = "${HANPURE.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#send_mobile1").val(mobile_array[0]);
					$("#send_mobile2").val(mobile_array[1]);
					$("#send_mobile3").val(mobile_array[2]);
				}
				else {
					$("#send_mobile1").val("");
					$("#send_mobile2").val("");
					$("#send_mobile3").val("");
				}
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
			
			$("#send_zipcode").val("${HANPURE.ZIPCODE}");
			$("#send_address").val("${HANPURE.ADDRESS}");
			$("#send_address2").val("${HANPURE.ADDRESS2}");
		}
		else if(value == "3") {
			//수동입력
			$("#send_nm").val("");
			$("#send_tel1").val("");
			$("#send_tel2").val("");
			$("#send_tel3").val("");
			$("#send_mobile1").val("");
			$("#send_mobile2").val("");
			$("#send_mobile3").val("");
			$("#send_zipcode").val("");
			$("#send_address").val("");
			$("#send_address2").val("");
		}	
	}
	
	function setRecver(value) {
		if(value == "1") {
			showPopup(700, 500, "/common/pop_patient");
		}
		else if(value == "2") {
			$("#recv_nm").val("${SESSION.COMPANY}");
			
			var tel = "${SESSION.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#recv_tel1").val(tel_array[0]);
					$("#recv_tel2").val(tel_array[1]);
					$("#recv_tel3").val(tel_array[2]);
				}
				else {
					$("#recv_tel1").val("");
					$("#recv_tel2").val("");
					$("#recv_tel3").val("");
				}
			}
			else {
				$("#recv_tel1").val("");
				$("#recv_tel2").val("");
				$("#recv_tel3").val("");
			}
			
			var mobile = "${SESSION.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#recv_mobile1").val(mobile_array[0]);
					$("#recv_mobile2").val(mobile_array[1]);
					$("#recv_mobile3").val(mobile_array[2]);
				}
				else {
					$("#recv_mobile1").val("");
					$("#recv_mobile2").val("");
					$("#recv_mobile3").val("");
				}
			}
			else {
				$("#recv_mobile1").val("");
				$("#recv_mobile2").val("");
				$("#recv_mobile3").val("");
			}
			
			$("#recv_zipcode").val("${SESSION.ZIPCODE}");
			$("#recv_address").val("${SESSION.ADDRESS}");
			$("#recv_address2").val("${SESSION.ADDRESS2}");
		}
		else if(value == "3") {
			$("#recv_nm").val("");
			
			$("#recv_tel1").val("");
			$("#recv_tel2").val("");
			$("#recv_tel3").val("");
			
			$("#recv_mobile1").val("");
			$("#recv_mobile2").val("");
			$("#recv_mobile3").val("");
			
			$("#recv_zipcode").val("");
			$("#recv_address").val("");
			$("#recv_address2").val("");
		}	
	}
	
	function goSave() {
		if(ajaxRunning) return;
		
		var send_tp = GetRadioValue("send_tp");
		var send_nm = GetValue("send_nm");
		var send_tel1 = GetValue("send_tel1");
		var send_tel2 = GetValue("send_tel2");
		var send_tel3 = GetValue("send_tel3");		
		var send_mobile1 = GetValue("send_mobile1");
		var send_mobile2 = GetValue("send_mobile2");
		var send_mobile3 = GetValue("send_mobile3");		
		var send_zipcode = GetValue("send_zipcode");
		var send_address = GetValue("send_address");
		var send_address2 = GetValue("send_address2");
		var recv_tp = GetRadioValue("recv_tp");
		var recv_nm = GetValue("recv_nm");
		var recv_tel1 = GetValue("recv_tel1");
		var recv_tel2 = GetValue("recv_tel2");
		var recv_tel3 = GetValue("recv_tel3");		
		var recv_mobile1 = GetValue("recv_mobile1");
		var recv_mobile2 = GetValue("recv_mobile2");
		var recv_mobile3 = GetValue("recv_mobile3");		
		var recv_zipcode = GetValue("recv_zipcode");
		var recv_address = GetValue("recv_address");
		var recv_address2 = GetValue("recv_address2");		
		var delivery_dt = GetValue("delivery_dt");
		//var msg_make = GetValue("msg_make");
		//var msg_delivery = GetValue("msg_delivery");
		
		if(!send_nm) {
	    	alert("발신인을 입력해 주세요.");
	    	$("#send_nm").focus();
	    	return;
	    }
		
		/*
		if(!send_tel1) {
	    	alert("발신인 연락처를 입력해 주세요.");
	    	$("#send_tel1").focus();
	    	return;
	    }
		
		if(!send_tel2) {
	    	alert("발신인 연락처를 입력해 주세요.");
	    	$("#send_tel2").focus();
	    	return;
	    }		
		
		if(!send_tel3) {
	    	alert("발신인 연락처를 입력해 주세요.");
	    	$("#send_tel3").focus();
	    	return;
	    }	
		*/
		
		if(!send_mobile1) {
	    	alert("발신인 휴대폰을 입력해 주세요.");
	    	$("#send_mobile1").focus();
	    	return;
	    }
		
		if(!send_mobile2) {
	    	alert("발신인 휴대폰을 입력해 주세요.");
	    	$("#send_mobile2").focus();
	    	return;
	    }
		
		if(!send_mobile3) {
	    	alert("발신인 휴대폰을 입력해 주세요.");
	    	$("#send_mobile3").focus();
	    	return;
	    }
		
		if(!send_zipcode) {
	    	alert("발신인 주소를 검색 해 주세요.");
	    	$("#send_zipcode").focus();
	    	return;
	    }
		
		if(!send_address) {
			alert("발신인 주소를 검색 해 주세요.");
	    	$("#send_address").focus();
	    	return;
	    }
		
		if(!send_address2) {
			alert("발신인 상세주소를 입력 해 주세요.");
	    	$("#send_address2").focus();
	    	return;
	    }
		
		if(!recv_nm) {
	    	alert("수신인을 입력해 주세요.");
	    	$("#recv_nm").focus();
	    	return;
	    }
		
		/*
		if(!recv_tel1) {
	    	alert("수신인 연락처를 입력해 주세요.");
	    	$("#recv_tel1").focus();
	    	return;
	    }
		
		if(!recv_tel2) {
	    	alert("수신인 연락처를 입력해 주세요.");
	    	$("#recv_tel2").focus();
	    	return;
	    }	
		
		if(!recv_tel3) {
	    	alert("수신인 연락처를 입력해 주세요.");
	    	$("#recv_tel3").focus();
	    	return;
	    }	
		*/
		
		if(!recv_mobile1) {
	    	alert("수신인 휴대폰을 입력해 주세요.");
	    	$("#recv_mobile1").focus();
	    	return;
	    }
		
		if(!recv_mobile2) {
	    	alert("수신인 휴대폰을 입력해 주세요.");
	    	$("#recv_mobile2").focus();
	    	return;
	    }
		
		if(!recv_mobile3) {
	    	alert("수신인 휴대폰을 입력해 주세요.");
	    	$("#recv_mobile3").focus();
	    	return;
	    }
		
		if(!recv_zipcode) {
	    	alert("수신인 주소를 검색 해 주세요.");
	    	$("#recv_zipcode").focus();
	    	return;
	    }
		
		if(!recv_address) {
			alert("수신인 주소를 검색 해 주세요.");
	    	$("#recv_address").focus();
	    	return;
	    }
		
		if(!recv_address2) {
			alert("수신인 상세주소를 입력 해 주세요.");
	    	$("#recv_address2").focus();
	    	return;
	    }
		
		if(!delivery_dt) {
			alert("발송요청일을 입력 해 주세요.");
	    	$("#delivery_dt").focus();
	    	return;
		}	
		
		if(delivery_dt) {
			var dayOfWeek = getDayOfWeek(delivery_dt);
			console.log("dayOfWeek", dayOfWeek);
			
			if(dayOfWeek == "일") {
				alert("주말에는 발송이 불가능합니다.");
				return;
			}
			
			if(delivery_dt == getToday()) {
				var hour = getHour();
				if(hour > 13) {
					alert("1시 이후 주문건은 당일 배송이 불가능합니다.");
					return;
				} 
			}
			else {
				var days = getDateDiff(getToday(), delivery_dt);
				console.log("days : " + days);
				if(days <= 0) {
					alert("오늘 이 후 날짜를 선택해 주세요.");
					return;	
				}	
			}
		}
		
		var send_tel = send_tel1 + "-" + send_tel2 + "-" + send_tel3;
		var send_mobile = send_mobile1 + "-" + send_mobile2 + "-" + send_mobile3;
		
		var recv_tel = recv_tel1 + "-" + recv_tel2 + "-" + recv_tel3;
		var recv_mobile = recv_mobile1 + "-" + recv_mobile2 + "-" + recv_mobile3;
		
		if(!confirm("주문 하시겠습니까?")) {
			return;
		}
		
	    var formData = new FormData();	
	    formData.append("RECIPE_SEQ", "${RECIPE.RECIPE_SEQ}");
	    formData.append("SEND_TP", send_tp);
   		formData.append("SEND_NM", send_nm);
   		formData.append("SEND_TEL", send_tel);		
   		formData.append("SEND_MOBILE", send_mobile);		
   		formData.append("SEND_ZIPCODE", send_zipcode);
   		formData.append("SEND_ADDRESS", send_address);
   		formData.append("SEND_ADDRESS2", send_address2);
   		formData.append("RECV_TP", recv_tp);
   		formData.append("RECV_NM", recv_nm);
   		formData.append("RECV_TEL", recv_tel);		
   		formData.append("RECV_MOBILE", recv_mobile);		
   		formData.append("RECV_ZIPCODE", recv_zipcode);
   		formData.append("RECV_ADDRESS", recv_address);
   		formData.append("RECV_ADDRESS2", recv_address2);		
   		formData.append("DELIVERY_DT", delivery_dt);
   		//formData.append("MSG_MAKE", msg_make);
   		//formData.append("MSG_DELIVERY", msg_delivery);
   		
   		formData.append("PRODUCT_SEQ", "${PRODUCT_SEQ}");
   		formData.append("ORDER_QNTT", "${ORDER_QNTT}");
   		
   		formData.append("DRUG_LABEL_TP", "${DRUG_LABEL_TP}");
   		formData.append("DRUG_LABEL_TEXT", "${DRUG_LABEL_TEXT}");
   		formData.append("DELIVERY_LABEL_TP", "${DELIVERY_LABEL_TP}");
   		formData.append("DELIVERY_LABEL_TEXT", "${DELIVERY_LABEL_TEXT}");
   		
	    var url = "/product/delivery";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	if(confirm("주문이 완료되었습니다.\r\n결제는 [마이페이지]에서 진행할 수 있습니다.\r\n마이페이지로 이동 하시겠습니까?")) {
            			location.href = "/order/list";
            		}
            		else {
            			location.href = "/order/list";	
            		}
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}		
	
	function setPatient(name, tel, mobile, zipcode, address, address2) {
		$("#recv_nm").val(name);
		if(tel) {
			var tel_array = tel.split("-");
			if(tel_array.length == 3) {
				$("#recv_tel1").val(tel_array[0]);
				$("#recv_tel2").val(tel_array[1]);
				$("#recv_tel3").val(tel_array[2]);
			}
			else {
				$("#recv_tel1").val("");
				$("#recv_tel2").val("");
				$("#recv_tel3").val("");
			}
		}
		else {
			$("#recv_tel1").val("");
			$("#recv_tel2").val("");
			$("#recv_tel3").val("");
		}
		if(mobile) {
			var mobile_array = mobile.split("-");
			if(mobile_array.length == 3) {
				$("#recv_mobile1").val(mobile_array[0]);
				$("#recv_mobile2").val(mobile_array[1]);
				$("#recv_mobile3").val(mobile_array[2]);
			}
			else {
				$("#recv_mobile1").val("");
				$("#recv_mobile2").val("");
				$("#recv_mobile3").val("");
			}
		}
		else {
			$("#recv_mobile1").val("");
			$("#recv_mobile2").val("");
			$("#recv_mobile3").val("");
		}
		
		$("#recv_zipcode").val(zipcode);
		$("#recv_address").val(address);
		$("#recv_address2").val(address2);
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">배송정보 입력</h2>
			</header>

			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">보내는 사람</h3>
				<div class="tbl_basic pr-mb1">
					<table class="write">
						<colgroup>
							<col class="th2">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>발신지 선택 <span class="i_emp">*</span></th>
								<td>
									<label class="inp_radio mr20"><input type="radio" name="send_tp" value="1" onClick="setSender(this.value)" checked /><span>한의원</span></label>
									<label class="inp_radio mr20"><input type="radio" name="send_tp" value="2" onClick="setSender(this.value)" /><span>탕전실</span></label>
									<label class="inp_radio"><input type="radio" name="send_tp" value="3" onClick="setSender(this.value)" /><span>수동입력</span></label>
								</td>
							</tr>
							<tr>
								<th>발신인 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt w100p" name="send_nm" id="send_nm" value="${SESSION.COMPANY}" placeholder="발신인 정보를 입력하세요" /></td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<div class="inp_phone">
										<input type="text" name="send_tel" id="send_tel1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.TEL}" />' />
										<span>-</span>
										<input type="text" name="send_tel" id="send_tel2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL2" value="${SESSION.TEL}" />' />
										<span>-</span>
										<input type="text" name="send_tel" id="send_tel3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL3" value="${SESSION.TEL}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_phone">
										<input type="text" name="send_mobile" id="send_mobile1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.MOBILE}" />' />
										<span>-</span>
										<input type="text" name="send_mobile" id="send_mobile2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.MOBILE}" />' />
										<span>-</span>
										<input type="text" name="send_mobile" id="send_mobile3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.MOBILE}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>주소 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_addr">
										<div class="inp">
											<button type="button" class="btn-pk n blue2" onClick="OpenDaumZipEx('send')"><span>우편번호</span></button>
											<input type="text" name="send_zipcode" id="send_zipcode" class="inp_txt" onClick="OpenDaumZipEx('send')" value="${SESSION.ZIPCODE}" readOnly placeholder="우편번호 검색을 클릭하세요" />
										</div>
										<input type="text" name="send_address" id="send_address" class="inp_txt w100p" onClick="OpenDaumZipEx('send')" value="${SESSION.ADDRESS}" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력" />
										<input type="text" name="send_address2" id="send_address2" class="inp_txt w100p" placeholder="나머지 상세 주소를 입력하세요" value="${SESSION.ADDRESS2}" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<h3 class="h_tit2">받는 사람</h3>
				<div class="tbl_basic pr-mb1">
					<table class="write">
						<colgroup>
							<col class="th2">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>수신인 선택 <span class="i_emp">*</span></th>
								<td>
									<label class="inp_radio mr20"><input type="radio" name="recv_tp" value="1" onClick="setRecver(this.value)" /><span>환자</span></label>
									<label class="inp_radio mr20"><input type="radio" name="recv_tp" value="2" onClick="setRecver(this.value)" checked /><span>한의원</span></label>
									<label class="inp_radio"><input type="radio" name="recv_tp" value="3" onClick="setRecver(this.value)" /><span>수동입력</span></label>
								</td>
							</tr>
							<tr>
								<th>수신인 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt w100p" name="recv_nm" id="recv_nm" value="${SESSION.COMPANY}" placeholder="수신인 정보를 입력하세요" /></td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<div class="inp_phone">
										<input type="text" name="recv_tel" id="recv_tel1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.TEL}" />' />
										<span>-</span>
										<input type="text" name="recv_tel" id="recv_tel2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.TEL}" />' />
										<span>-</span>
										<input type="text" name="recv_tel" id="recv_tel3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.TEL}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_phone">
										<input type="text" name="recv_mobile" id="recv_mobile1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.MOBILE}" />' />
										<span>-</span>
										<input type="text" name="recv_mobile" id="recv_mobile2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.MOBILE}" />' />
										<span>-</span>
										<input type="text" name="recv_mobile" id="recv_mobile3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${SESSION.MOBILE}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>주소 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_addr">
										<div class="inp">
											<button type="button" class="btn-pk n blue2" onClick="OpenDaumZipEx('recv')"><span>우편번호</span></button>
											<input type="text" name="recv_zipcode" id="recv_zipcode" class="inp_txt" onClick="OpenDaumZipEx('recv')" value="${SESSION.ZIPCODE}" readOnly placeholder="우편번호 검색을 클릭하세요" />
										</div>
										<input type="text" name="recv_address" id="recv_address" class="inp_txt w100p" onClick="OpenDaumZipEx('recv')" value="${SESSION.ADDRESS}" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력" />
										<input type="text" name="recv_address2" id="recv_address2" class="inp_txt w100p" placeholder="나머지 상세 주소를 입력하세요" value="${SESSION.ADDRESS2}" />
									</div>
								</td>
							</tr>
							<tr>
								<th>발송요청일 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt calender datepicker date" name="delivery_dt" id="delivery_dt" value="" readOnly placeholder="yyyy-mm-dd"></td>
							</tr>
						</tbody>
					</table>
					<p class="t_info mt10 c-blue">* 오후 1시 이전 주문완료 시 당일 발송이 가능하며, 오후 1시 이후 주문완료 시 익일 발송됩니다.</p>
				</div>
			</div><!--// lft_c -->

			<div class="botm_c">
				<div class="mbtn_ty1">
					<button type="button" id="btnPre" class="btn-pk b white"><span>이전 단계로 이동</span></button>
					<button type="button" id="btnSave" class="btn-pk b blue"><span>주문하기</span></button>
				</div>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->>

<%@ include file="/WEB-INF/views/include/footer.jsp" %>