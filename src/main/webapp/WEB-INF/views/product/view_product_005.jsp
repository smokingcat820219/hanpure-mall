<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>	
	$(function() {	    
	    $('#btnList').click(function() {
	    	location.href = "/product/list?PRODUCT_TP=${VO.PRODUCT_TP}" + window.location.hash;
	    });
	    
	    $('#btnSave').click(function() {
	    	goSave();
	    });	    
	   
	    var product_text = ${VO.PRODUCT_TEXT};	    
	    var sHTML = "";
		for(var i = 0; i < product_text.length; i++) {
			sHTML += "<tr>";
			sHTML += "	<th>" + product_text[i].title + "</th>";
			sHTML += "	<td>" + product_text[i].content + "</td>";
			sHTML += "</tr>";
		}
		$("#tbody1").html(sHTML);
	});		
	
	function goSave() {
		if(ajaxRunning) return;
		
		var period = GetValue("period");
		var product_seq = "${VO.PRODUCT_SEQ}";
		
		if(!confirm("사전처방을 등록 하시겠습니까?")) {
			return;
		}
		
	    var formData = new FormData();	
	    formData.append("PRODUCT_SEQ", product_seq);
	    formData.append("PERIOD", period);
   		
	    var url = "/promise/write";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	alert("사전처방 등록이 완료 되었습니다.");
	            	
	            	location.href = "/promise/list";
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}	
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<div class="clearfix pr-mb1">
				<!-- 좌측데이터 -->
				<div class="lft_c ty2">					
					<div class="owl-carousel">						
						<c:if test="${VO.FILEPATH1 ne null and VO.FILEPATH1 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH1}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH2 ne null and VO.FILEPATH2 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH2}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH3 ne null and VO.FILEPATH3 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH3}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH4 ne null and VO.FILEPATH4 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH4}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH5 ne null and VO.FILEPATH5 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH5}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH6 ne null and VO.FILEPATH6 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH6}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH7 ne null and VO.FILEPATH7 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH7}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH8 ne null and VO.FILEPATH8 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH8}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH9 ne null and VO.FILEPATH9 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH9}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH10 ne null and VO.FILEPATH10 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH10}" alt="">
								</div>
							</div>
						</c:if>					
					</div>
				</div><!--// lft_c -->
				
				<!-- 우측데이터 -->
				<div class="rgh_c ty2">
					<h3 class="h_tit1 mb20">${VO.PRODUCT_NM}</h3>
					<h3 class="h_tit2">${DEPTH2} 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="view">
							<colgroup>
								<col class="th1">
								<col>
							</colgroup>
							<tbody id="tbody1">
								
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">사전처방 등록 <button type="button" class="btn_i f-en" onclick="openLayerPopup('popInfor');"><span>!</span></button></h3>
					<div class="tbl_basic pr-mb1">
						<table class="view">
							<colgroup>
								<col class="th1">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<th>조제기간</th>
									<td><strong><mask:display type="NUMBER" value="${VO.PERIOD_MAKE}" />일/조제완료(${FINISH_DATE})</strong></td>									
								</tr>
								<tr>
									<th>처방단위</th>
									<td><strong><mask:display type="NUMBER" value="${VO.QNTT}" /></strong> ${VO.UNIT}</td>									
								</tr>
								<tr>
									<th>처방량(월)</th>
									<td><strong><mask:display type="NUMBER" value="${VO.MONTHLY_QNTT}" /></strong></td>									
								</tr>
								<tr>
									<th>처방 기간</th>
									<td>
										<select name="period" id="period" class="select1 w100p">
											<c:if test="${VO.PERIOD_ORDER ne ''}">
												<c:forEach items="${fn:split(VO.PERIOD_ORDER, ',')}" var="item">
												    <option value="${item}">${item}개월</option>
												</c:forEach>
											</c:if>
										</select>
									</td>									
								</tr>								
							</tbody>
						</table>
					</div>
				</div><!--// rgh_c -->
			</div>

			<div class="clearfix pr-mb1">
				<h3 class="h_tit2">상세이미지</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col>
						</colgroup>
						<tbody>
							<tr>
								<td><mask:display type="EDITOR" value="${VO.CONTENT1}" /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="rgh_c">
				<div class="mbtn_ty1">
					<a href="javascript:" id="btnList" class="btn-pk b blue2"><span>목록</span></a>
					<a href="javascript:" id="btnSave" class="btn-pk b blue"><span>사전처방 등록하기</span></a>
				</div>
			</div>


		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<!-- 이미지 롤링 -->
<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>
<script>
$(function(){
	var slider1 = $(".owl-carousel");
	slider1.owlCarousel({
		loop: $(".owl-carousel .item").length > 1 ? true: false,
		margin:0,
		nav:false,
		dots: $(".owl-carousel .item").length > 1 ? true: false,
		items:1,
		smartSpeed:1500,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		mouseDrag : $(".owl-carousel .item").length > 1 ? true: false,
	});

	slider1.trigger('refresh.owl.carousel');
})
</script>

    margin-left: -165.5px;
    margin-top: -463px;
    
<!-- 팝업 : 사전처장 알림 -->
<div id="popInfor" class="layerPopup" style="width:800px;">
	<div class="popup" style="height:600px">
		<div class="p_head">
			<h2 class="tit">안내</h2>
			<button type="button" class="btn_close b-close"><span>닫기</span></button>
		</div>
		<div class="p_cont">
			<div class="txt">
				<mask:display type="EDITOR" value="${VO.CONTENT2}" />
			</div>
			<div class="btn-bot">
				<a href="javascript:;" class="btn-pk n blue b-close"><span>확인</span></a>
			</div>
		</div>				
	</div>
</div>
<!--// 팝업 : 사전처장 알림 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>