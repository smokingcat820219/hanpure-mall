<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script>	
	function selectDrugLabel(value) {
		var recipe_tp = GetRadioValue("recipe_tp");
		
		if(value == "005") {
			$("#drug_label_text").attr("disabled", false); 		
		}
		else {
			$("#drug_label_text").attr("disabled", true); 		
		}		
	}
	
	function selectDeliveryLabel(value) {		
		if(value == "005") {
			$("#delivery_label_text").attr("disabled", false);			
		}
		else {
			$("#delivery_label_text").attr("disabled", true); 			
		}		
	}

	$(function() {	    
	    $('#btnList').click(function() {
	    	location.href = "/product/list?PRODUCT_TP=${VO.PRODUCT_TP}" + window.location.hash;
	    });
	    
	    $('#btnNext').click(function() {
	    	goNext();
	    });
	    
	    $(".inp_num").each(function(){
			var $this = $(this);

			$this.find("button").on("click", function(){
				var number = $this.find("input").val();

				if ($(this).hasClass("bt_up")){
					number ++;
				} else {

					if (number > 0){
						number --;
					}
					
				}

				$this.find("input").val(number);
			});
		});
	    
	    
	    var product_text = ${VO.PRODUCT_TEXT};	    
	    var sHTML = "";
	    sHTML += "<tr>";
		sHTML += "	<th>" + "판매가격" + "</th>";
		sHTML += "	<td><strong>" + NumberFormat("${VO.AMOUNT}") + "원</strong></td>";
		sHTML += "</tr>";
	    
		for(var i = 0; i < product_text.length; i++) {
			sHTML += "<tr>";
			sHTML += "	<th>" + product_text[i].title + "</th>";
			sHTML += "	<td>" + product_text[i].content + "</td>";
			sHTML += "</tr>";
		}
		$("#tbody1").html(sHTML);
	    
	    addPriceList();
	    
	  	//금액 셋팅
		$("#remark1").html("${RECIPE.REMARK1}");
		$("#remark2").html("${RECIPE.REMARK2}");
		$("#remark3").html("${RECIPE.REMARK3}");
		$("#remark4").html("${RECIPE.REMARK4}");
		$("#remark5").html("${RECIPE.REMARK5}");
		$("#remark6").html("${RECIPE.REMARK6}");
		$("#remark7").html("${RECIPE.REMARK7}");
		$("#remark8").html("${RECIPE.REMARK8}");
		$("#remark9").html("${RECIPE.REMARK9}");
		$("#remark10").html("${RECIPE.REMARK10}");
		$("#remark11").html("${RECIPE.REMARK11}");
		
		$("#price1").html(NumberFormat("${RECIPE.PRICE1}"));
		$("#price2").html(NumberFormat("${RECIPE.PRICE2}"));
		$("#price3").html(NumberFormat("${RECIPE.PRICE3}"));
		$("#price4").html(NumberFormat("${RECIPE.PRICE4}"));
		$("#price5").html(NumberFormat("${RECIPE.PRICE5}"));
		$("#price6").html(NumberFormat("${RECIPE.PRICE6}"));
		$("#price7").html(NumberFormat("${RECIPE.PRICE7}"));
		$("#price8").html(NumberFormat("${RECIPE.PRICE8}"));
		$("#price9").html(NumberFormat("${RECIPE.PRICE9}"));
		$("#price10").html(NumberFormat("${RECIPE.PRICE10}"));
		$("#price11").html(NumberFormat("${RECIPE.PRICE11}"));		
	    
		$("#amount").html(NumberFormat("${RECIPE.AMOUNT}"));
		
		if("${RECIPE.PACKAGE1}") {
			getItemInfo("1", "${RECIPE.PACKAGE1}");	
		}
		if("${RECIPE.PACKAGE2}") {
			getItemInfo("2", "${RECIPE.PACKAGE2}");	
		}
		if("${RECIPE.PACKAGE3}") {
			getItemInfo("3", "${RECIPE.PACKAGE3}");	
		}
		
		selectDrugLabel("001");
		selectDeliveryLabel("001");
	});		
	
	function getItemInfo(pkg, bom_cd) {		
		console.log("getItemInfo : " + bom_cd);
		if(!bom_cd) return; 
		
		//if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("BOM_CD", bom_cd);
		
	    var url = "/bom/selectItems";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
				
	            if(response.result == 200) {  	            	   
	            	var sHTML = "";
	            	sHTML += "	<div class='owl-carousel' id='rolling" + pkg + "'>";
	            	for(var i = 0; i < response.list.length; i++) {	
	            		sHTML += "		<div class='item'>";
	        			sHTML += "			<div class='img_prd long'>";
	        			
	        			var filepath = response.list[i].FILEPATH;
	        			var item_nm = response.list[i].ITEM_NM;
	        			
	        			if(filepath) {
	        				sHTML += "				<img src='/download?filepath=" + filepath + "' title='" + item_nm + "' />";
	        			}
	        			else {
	        				sHTML += "				<img src='/images/common/img_noimg.jpg' title='" + item_nm + "' />";
	        			}
						
						sHTML += "			</div>";
						sHTML += "		</div>";	
	            	}
	            	
	            	sHTML += "	</div>";          	
	            	
	            	$("#package" + pkg).html(sHTML);
	            	var slider = $("#package" + pkg + " #rolling" + pkg);
	        		slider.owlCarousel({
	        			loop: $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        			margin:0,
	        			nav:false,
	        			dots: $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        			items:1,
	        			smartSpeed:1500,
	        			autoplay:true,
	        			autoplayTimeout:5000,
	        			autoplayHoverPause:false,
	        			mouseDrag : $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        		});

	        		slider.trigger('refresh.owl.carousel');
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));

	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function addPriceList() {
		var sHTML = "";
		
		sHTML += "<tr>";
		sHTML += "	<th>약재비</th>";		
		sHTML += "	<td class='ta-r'><span id='remark1'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price1'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>탕전비</th>";
		sHTML += "	<td class='ta-r'><span id='remark2'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price2'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>특수탕전비</th>";
		sHTML += "	<td class='ta-r'><span id='remark3'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price3'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>향미제</th>";
		sHTML += "	<td class='ta-r'><span id='remark4'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price4'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>앰플(자하거)</th>";
		sHTML += "	<td class='ta-r'><span id='remark5'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price5'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "	<th>녹용별전</th>";
		sHTML += "	<td class='ta-r'><span id='remark6'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price6'></span>원</td>";
		sHTML += "</tr>";	
		sHTML += "<tr>";
		sHTML += "	<th>주수상반</th>";
		sHTML += "	<td class='ta-r'><span id='remark7'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price7'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>조제비</th>";
		sHTML += "	<td class='ta-r'><span id='remark8'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price8'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>포장비</th>";
		sHTML += "	<td class='ta-r'><span id='remark9'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price9'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>배송비</th>";
		sHTML += "	<td class='ta-r'><span id='remark10'>0</span></td>";
		sHTML += "	<td class='ta-r'><span id='price10'></span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th>기타</th>";
		sHTML += "	<td class='ta-r'><span id='remark11'>주문 후, 관리자 확인 시 입력되며 [마이페이지]에서 확인할 수 있습니다.</span></td>";
		sHTML += "	<td class='ta-r'><span id='price11'>0</span>원</td>";
		sHTML += "</tr>";
		sHTML += "<tr>";
		sHTML += "	<th><strong>총 합계</strong></th>";
		sHTML += "	<td colspan='2' class='ta-r'><strong class='c-red' id='amount'>0</strong>원</td>";
		sHTML += "</tr>";
		
		$("#list_price").html(sHTML);
	}
	
	function goNext() {
		var order_qntt = GetValue("order_qntt");
		if(!order_qntt) {
			alert("주문 수량을 선택해 주세요.");
			$("#order_qntt").focus();
			return;
		}
		
		if(parseInt(order_qntt) <= 0) {
			alert("주문 수량을 선택해 주세요.");
			$("#order_qntt").focus();
			return;
		}		
		
		var drug_label_tp = GetValue("drug_label_tp");
		var drug_label_text = GetValue("drug_label_text");
		var delivery_label_tp = GetValue("delivery_label_tp");
		var delivery_label_text = GetValue("delivery_label_text");
		
		var param = "PRODUCT_SEQ=${VO.PRODUCT_SEQ}";
		param += "&ORDER_QNTT=" + order_qntt;
		param += "&DRUG_LABEL_TP=" + drug_label_tp;
		param += "&DRUG_LABEL_TEXT=" + drug_label_text;
		param += "&DELIVERY_LABEL_TP=" + delivery_label_tp;
		param += "&DELIVERY_LABEL_TEXT=" + delivery_label_text;
		
		location.href = "/product/delivery?" + param + window.location.hash;		
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<div class="clearfix pr-mb1">
				<!-- 좌측데이터 -->
				<div class="lft_c ty2">					
					<div class="owl-carousel">
						<c:if test="${VO.FILEPATH1 ne null and VO.FILEPATH1 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH1}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH2 ne null and VO.FILEPATH2 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH2}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH3 ne null and VO.FILEPATH3 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH3}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH4 ne null and VO.FILEPATH4 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH4}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH5 ne null and VO.FILEPATH5 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH5}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH6 ne null and VO.FILEPATH6 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH6}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH7 ne null and VO.FILEPATH7 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH7}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH8 ne null and VO.FILEPATH8 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH8}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH9 ne null and VO.FILEPATH9 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH9}" alt="">
								</div>
							</div>
						</c:if>
						<c:if test="${VO.FILEPATH10 ne null and VO.FILEPATH10 ne ''}">
							<div class="item">
								<div class="img_prd long">
									<img src="/download?filepath=${VO.FILEPATH10}" alt="">
								</div>
							</div>
						</c:if>					
					</div>
				</div><!--// lft_c -->
				
				<!-- 우측데이터 -->
				<div class="rgh_c ty2">
					<h3 class="h_tit1 mb20">${VO.PRODUCT_NM}</h3>
					<h3 class="h_tit2">${DEPTH2} 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="view">
							<colgroup>
								<col class="th1">
								<col>
							</colgroup>
							<tbody id="tbody1">
								
							</tbody>
						</table>
						<div class="mt10">
							<p class="d-ib va-m mr10 fz-b1"><strong>${VO.PRODUCT_NM}</strong></p>
							<div class="d-ib inp_num">
								<input type="number" name="order_qntt" id="order_qntt" value="1" />
								<button type="button" class="bt_up"><span>수량 증가</span></button>
								<button type="button" class="bt_down"><span>수량 감소</span></button>
							</div>
						</div>
					</div>
				</div><!--// rgh_c -->
			</div>

			<div class="clearfix pr-mb1">
				<h3 class="h_tit2">상세이미지</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col>
						</colgroup>
						<tbody>
							<tr>
								<td><mask:display type="EDITOR" value="${VO.CONTENT1}" /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="clearfix pr-mb1">
				<h3 class="h_tit2">처방전</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th2">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>처방구분</th>
								<td>${DEPTH2}</td>
							</tr>
							<tr>
								<th>처방명</th>
								<td>${RECIPE.RECIPE_NM}</td>
							</tr>
						</tbody>
					</table>
					<table class="list">
						<colgroup>
							<col class="num">
							<col>
							<col>
							<col>
							<col>
							<col>
						</colgroup>
						<thead>
							<tr>
								<th>No</th>
								<th>약재명</th>
								<th>유형</th>
								<th>원산지</th>
								<th>1첩량</th>
								<th>총 약재량</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
								<tr>
									<td>${i}</td>
									<td>${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
									<td>
										<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">선전</c:if>
										<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">일반</c:if>
										<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">후하</c:if>									
									</td>
									<td>${LIST_RECIPE.get(i - 1).ORIGIN}</td>
									<td class="ta-r"><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).CHUP1}" /> g</td>
									<td class="ta-r"><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).TOTAL_QNTT}" /> g</td>
								</tr>							
							</c:forEach>
						</tbody>
					</table>
				</div>

				<h3 class="h_tit2">${DEPTH2} 정보</h3>
				<div class="tbl_basic">
					<table class="list">
						<colgroup>
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
						</colgroup>
						<tbody>
							<tr>
								<th>첩수</th>
								<th>팩수</th>
								<th>팩용량</th>
								<th>특수탕전</th>
								<th>탕전법</th>
								<th>탕전 물양</th>
							</tr>
							<tr>
								<td><mask:display type="NUMBER" value="${RECIPE.INFO1}" /> 첩</td>
								<td><mask:display type="NUMBER" value="${RECIPE.INFO2}" /> 팩</td>
								<td><mask:display type="NUMBER" value="${RECIPE.INFO3}" /> mL</td>
								<td>${MAP_SPECIAL_TP.get(RECIPE.INFO4).get(0)}</td>
								<td>${RECIPE.INFO5}</td>
								<td><mask:display type="NUMBER" value="${RECIPE.INFO6}" /> mL</td>								
							</tr>
							<tr>
								<th>향미제(초코)</th>
								<th>앰플(자하거)</th>									
								<th>녹용별전</th>
								<th>주수상반</th>
								<th>탕전시간</th>
								<td></td>
							</tr>
							<tr>
								<td>
									<c:choose>
										<c:when test="${RECIPE.INFO7 eq 'Y'}">
											선택
										</c:when>
										<c:otherwise>
											선택안함
										</c:otherwise>
									</c:choose>								
								</td>
								<td>
									<c:choose>
										<c:when test="${RECIPE.INFO8 ne ''}">
											<mask:display type="NUMBER" value="${RECIPE.INFO8}" /> mL
										</c:when>
										<c:otherwise>
											선택안함
										</c:otherwise>
									</c:choose>			
								</td>
								<td>
									<c:choose>
										<c:when test="${RECIPE.INFO9 ne ''}">
											<mask:display type="NUMBER" value="${RECIPE.INFO9}" /> mL
										</c:when>
										<c:otherwise>
											선택안함
										</c:otherwise>
									</c:choose>			
								</td>
								<td>
									<c:choose>
										<c:when test="${RECIPE.INFO10 ne ''}">
											${RECIPE.INFO10_NM} / <mask:display type="NUMBER" value="${RECIPE.INFO10}" /> mL
										</c:when>
										<c:otherwise>
											선택안함
										</c:otherwise>
									</c:choose>		
								</td>
								<td><mask:display type="NUMBER" value="${RECIPE.INFO11}" />분</td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="clearfix pos-r">
				<div class="lft_c">
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>파우치</th>
									<th>탕약박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t" >
										<c:choose>
											<c:when test="${RECIPE.PACKAGE1 ne ''}">
												<button type="button" class="btn-pk s blue w80p" ><span>${RECIPE.PACKAGE1_NM}</span></button>
												<div class="box_td" id="package1">
													
												</div>
											</c:when>
											<c:otherwise>
												<button type="button" class="btn-pk s blue w80p"><span>선택 없음</span></button>
												<div class="box_td">
												</div>
											</c:otherwise>										
										</c:choose>				
									</td>
									<td class="va-t">
										<c:choose>
											<c:when test="${RECIPE.PACKAGE2 ne ''}">
												<button type="button" class="btn-pk s blue w80p" ><span>${RECIPE.PACKAGE2_NM}</span></button>
												<div class="box_td" id="package2">
												
												</div>
											</c:when>
											<c:otherwise>
												<button type="button" class="btn-pk s blue w80p"><span>선택 없음</span></button>
												<div class="box_td">
												</div>
											</c:otherwise>										
										</c:choose>				
									</td>
									<td class="va-t">
										<c:choose>
											<c:when test="${RECIPE.PACKAGE3 ne ''}">
												<button type="button" class="btn-pk s blue w80p" ><span>${RECIPE.PACKAGE3_NM}</span></button>
												<div class="box_td" id="package3">
													
												</div>
											</c:when>
											<c:otherwise>
												<button type="button" class="btn-pk s blue w80p"><span>선택 없음</span></button>
												<div class="box_td">
												</div>
											</c:otherwise>										
										</c:choose>				
									</td>
								</tr>
							</tbody>
						</table>
						<table class="view small" style="margin-top: -1px;">
							<colgroup>
								<col class="th1" />
								<col class="td1" />
								<col />
								<col class="th1" />
								<col class="td1" />
								<col />
							</colgroup>
							<tbody>
								<tr>
									<th>탕약박스 <br>부착라벨</th>
									<td>
										<select name="drug_label_tp" id="drug_label_tp" class="select1 w100p" onChange="selectDrugLabel(this.value)">
											<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
												<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td>
										<input type="text" name="drug_label_text" id="drug_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" maxlength="14" />
									</td>
									<th>택배박스 <br>부착라벨</th>
									<td>
										<select name="delivery_label_tp" id="delivery_label_tp" class="select1 w100p" onChange="selectDeliveryLabel(this.value)">
											<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
												<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
											</c:forEach>
										</select>
									</td>
									<td>
										<input type="text" name="delivery_label_text" id="delivery_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" maxlength="14" />
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>		
				
				<div class="rgh_c">
					<h3 class="h_tit2">비용안내</h3>
					<div class="tbl_basic ">
						<table class="view">
							<colgroup>
								<col class="th1">
								<col>
								<col class="td1">
							</colgroup>
							<thead>
								<tr>
									<th>항목</th>
									<th>수량</th>
									<th>금액</th>
								</tr>
							</thead>
							<tbody id="list_price">
								
							</tbody>
						</table>
					</div>
				</div>				
			</div>
			
			<div class="rgh_c">
				<div class="mbtn_ty1">
					<a href="javascript:" id="btnList" class="btn-pk b blue2"><span>목록</span></a>
					<a href="javascript:" id="btnNext" class="btn-pk b blue"><span>배송지 입력하기</span></a>
				</div>
			</div>


		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<!-- 이미지 롤링 -->
<script>
$(function(){
	var slider1 = $(".owl-carousel");
	slider1.owlCarousel({
		loop: $(".owl-carousel .item").length > 1 ? true: false,
		margin:0,
		nav:false,
		dots: $(".owl-carousel .item").length > 1 ? true: false,
		items:1,
		smartSpeed:1500,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		mouseDrag : $(".owl-carousel .item").length > 1 ? true: false,
	});

	slider1.trigger('refresh.owl.carousel');
})
</script>

<%@ include file="/WEB-INF/views/include/footer.jsp" %>