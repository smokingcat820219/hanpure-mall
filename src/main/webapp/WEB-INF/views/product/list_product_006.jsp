<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	var PRODUCT_TP = "${PRODUCT_TP}";
	var CATEGORY1 = "";
	var CATEGORY2 = "";
	
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	    
	    
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {	    	
	    	$("#searchField").val("");
			$("#searchText").val("");
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {	
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
				
		var param = "page=" + page;
		param += "&PRODUCT_TP=" + PRODUCT_TP;
		param += "&CATEGORY1=" + CATEGORY1;
		param += "&CATEGORY2=" + CATEGORY2;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;	
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/product/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	
	            			sHTML += "<li>";
	            			sHTML += "	<a href='javascript:' onClick=\"goView('" + response.list[i].PRODUCT_SEQ + "')\">";
	            			sHTML += "	<div class='img_prd'>";
	            			sHTML += "		<img src='/download?filepath=" + response.list[i].FILEPATH1 + "' alt=''>";
	            			sHTML += "	</div>";
	            			sHTML += "	<div class='cont'>";
	            			sHTML += "		<p class='t1'>" + response.list[i].PRODUCT_NM + "</p>";
	            			sHTML += "		<p class='t2'>" + response.list[i].PRODUCT_DESC + "</p>";
	            			sHTML += "		<p class='t3'>" + NumberFormat(response.list[i].AMOUNT) + " 원</p>";
	            			sHTML += "	</div>";
	            			sHTML += "	</a>";
	            			sHTML += "	<div class='botm'>";
	            			sHTML += "		<a href='javascript:' onClick=\"goView('" + response.list[i].PRODUCT_SEQ + "')\" class='btn-pk blue n'><span>처방하기</span></a>";
							sHTML += "	</div>";
							sHTML += "</li>";
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='14'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goView(product_seq) {
		location.href = "/product/view/" + product_seq + "/" + window.location.hash;
	}
	
	function setCategory1(obj, category) {
		$(".tab_category").removeClass("on");
		$(obj).parent().addClass("on");
		
		CATEGORY1 = category;
		CATEGORY2 = "";
		
		if(category != "" && category != "Y") {
			//2depth
			selectCategory(category);			
		}
		else {
			$("#category2").html("");
		}
		
		setHash(1);	
	}
	
	function setCategory2(obj, category) {
		$(".tab_category2").removeClass("on");
		$(obj).parent().addClass("on");
		
		CATEGORY2 = category;
		
		setHash(1);	
	}

	function selectCategory(p_code) {
		if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("PRODUCT_TP", PRODUCT_TP);
		formData.append("P_CODE", p_code);

	    var url = "/category/select";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));

	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var list = response.list;
	            	
	            	for(var i = 0; i < list.length; i++) {	  
	            		sHTML += "<li class='tab_category2'><a href='javascript:' onClick=\"setCategory2(this, '" + response.list[i].CODE + "')\"><span>" + response.list[i].NAME + "</span></a></li>";
	            	}
	            	
	            	$("#category2").html(sHTML);
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));

	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });	
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">상용처방</h2>
			</header>

			<div class="tab ty2">
				<ul>
					<li class="tab_category on"><a href="javascript:" onClick="setCategory1(this, '')"><span>전체</span></a></li>
					<li class="tab_category"><a href="javascript:" onClick="setCategory1(this, 'Y')"><span>이벤트상품</span></a></li>
					
					<c:forEach var="i" begin="1" end="${LIST_CATEGORY1.size()}">
						<li class="tab_category"><a href="javascript:" onClick="setCategory1(this, '${LIST_CATEGORY1.get(i - 1).CODE}')"><span>${LIST_CATEGORY1.get(i - 1).NAME}</span></a></li>
					</c:forEach>
				</ul>
				<ul id="category2">
					
				</ul>
			</div>

			<div class="lst_product">
				<ul id="tbody">
					
				</ul>
			</div>


			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>