<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<div class="sidebar_y">
	<h2 class="tit f-en">한퓨어 한의원</h2>
	<aside class="menu">
		<ul>
			<li <c:if test="${DEPTH1 eq '처방하기'}">class="on"</c:if>>
				<a href="#"><span>처방하기</span></a>
				<div class="depth2">
					<ul>
						<li <c:if test="${DEPTH2 eq '탕전'}">class="on"</c:if>><a href="/recipe/write?RECIPE_TP=001">탕전</a></li>
						<li <c:if test="${DEPTH2 eq '제환'}">class="on"</c:if>><a href="/recipe/write?RECIPE_TP=002">제환</a></li>
						<li <c:if test="${DEPTH2 eq '연조엑스'}">class="on"</c:if>><a href="/recipe/write?RECIPE_TP=003">연조엑스</a></li>
						<li <c:if test="${DEPTH2 eq '첩약'}">class="on"</c:if>><a href="/recipe/write?RECIPE_TP=004">첩약</a></li>
					</ul>
				</div>
			</li>
			<li <c:if test="${DEPTH1 eq '간편처방'}">class="on"</c:if>>
				<a href="#"><span>간편처방</span></a>
				<div class="depth2">
					<ul>												
						<li <c:if test="${DEPTH2 eq '약속처방 배송요청'}">class="on"</c:if>><a href="/promise/list_product">약속처방 배송요청</a></li>
						<li <c:if test="${DEPTH2 eq '약속처방'}">class="on"</c:if>><a href="/product/list?PRODUCT_TP=005">약속처방</a></li>
						<li <c:if test="${DEPTH2 eq '사전처방관리'}">class="on"</c:if>><a href="/promise/list">사전처방관리</a></li>									
						<li <c:if test="${DEPTH2 eq '상용처방'}">class="on"</c:if>><a href="/product/list?PRODUCT_TP=006">상용처방</a></li>
					</ul>
				</div>
			</li>
			<li <c:if test="${DEPTH1 eq '처방관리'}">class="on"</c:if>>
				<a href="#"><span>처방관리</span></a>
				<div class="depth2">
					<ul>					
						<li <c:if test="${DEPTH2 eq '임시저장'}">class="on"</c:if>><a href="/recipe/list_temp">임시저장</a></li>
						<li <c:if test="${DEPTH2 eq '주문내역조회'}">class="on"</c:if>><a href="/order/list">주문내역조회</a></li>						
						<li <c:if test="${DEPTH2 eq '나의처방'}">class="on"</c:if>><a href="/myrecipe/list">나의처방</a></li>
						<li <c:if test="${DEPTH2 eq '조제지시 및 복용법'}">class="on"</c:if>><a href="/howto/list">조제지시 및 복용법</a></li>
						<li <c:if test="${DEPTH2 eq '환자정보'}">class="on"</c:if>><a href="/patient/list">환자정보</a></li>
					</ul>
				</div>
			</li>			
			<li <c:if test="${DEPTH1 eq '처방사전'}">class="on"</c:if>>
				<a href="#"><span>처방사전</span></a>
				<div class="depth2">
					<ul>
						<li <c:if test="${DEPTH2 eq '방제사전'}">class="on"</c:if>><a href="/pdata/list">방제사전</a></li>
						<li <c:if test="${DEPTH2 eq '본초사전'}">class="on"</c:if>><a href="/herbs/list">본초사전</a></li>
						<li <c:if test="${DEPTH2 eq '약재목록'}">class="on"</c:if>><a href="/drug/list">약재목록</a></li>
						<li <c:if test="${DEPTH2 eq '첩약약재정보'}">class="on"</c:if>><a href="/drug/list_insurance">첩약약재정보</a></li>
					</ul>
				</div>
			</li>
			
			
			<li <c:if test="${DEPTH1 eq '통계 및 보고서'}">class="on"</c:if>>
				<a href="#"><span>통계&amp;보고서</span></a>
				<div class="depth2">
					<ul>						
						<li <c:if test="${DEPTH2 eq '처방결산'}">class="on"</c:if>><a href="/report/list_closing">처방결산</a></li>
						<li <c:if test="${DEPTH2 eq '분석 보고서'}">class="on"</c:if>><a href="/report/list_patient">분석 보고서</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</aside>
</div><!-- sidebar -->