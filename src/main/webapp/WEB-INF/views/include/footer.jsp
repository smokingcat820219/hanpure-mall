<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<!-- 로딩 -->
<div class="loadingbar" style="display: none;">
	<span><img src="/images/common/loading.gif" alt="로딩중"></span>
</div>


<script>
	function showPopup(width, height, url) {
		$("#iframe").width(width);
		$("#iframe").height(height);
		$('#iframe').attr('src', url);
		
		var $objWidht = $('#iframe').outerWidth();
		var $objHeight = $('#iframe').outerHeight();
		var scrollTop = $(window).scrollTop();
		
		$('#iframe').css({
			"margin-left" : - (width / 2),
			"margin-top" : - (height / 2)
		});
		
		$("#iframe").fadeIn(100);
		if (!$(".popup_dim").length > 0){
			$("#iframe").before("<div class='popup_dim'></div>");
		}
	}
	
	function hidePopup() {
		$("#iframe").fadeOut(100);
		$(".popup_dim").remove();
	}
</script>

<iframe id="iframe" class="layerPopup" src="" frameborder="0" style="width: 0px;height: 0px;"></iframe>

</body>
</html>