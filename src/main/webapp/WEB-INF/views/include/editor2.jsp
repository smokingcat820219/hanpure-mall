<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>
	
<!-- include summernote css/js -->
<script src="/vendor/jquery-3.2.1.min.js"></script>
<script>
	var sn$ = jQuery.noConflict();
</script>
<link href="/vendor/summernote-lite.css" rel="stylesheet" type="text/css" media="all">
<script src="/vendor/summernote-lite.js"></script>
<script src="/vendor/summernote-ko-KR.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var bgButton = function(context) {
		var ui = sn$.summernote.ui;
		var button = ui.buttonGroup([
			ui.button({
				className: 'dropdown-toggle',
				contents: '<i class="note-icon-pencil"/>BG</i> <span class="note-icon-caret"></span>',
				container: 'body',
				tooltip: '배경색 변경',
				data: {
					toggle: 'dropdown'
				}
			}),
			ui.dropdown({
                className: 'drop-default summernote-list',
                contents: "<span style='background-color:#fff;'></span><span style='background-color:#000;'></span><span style='background-color:#dddddd;'></span><span style='background-color:#F7C6CE;'></span><span style='background-color:#FFE7CE;'></span><span style='background-color:#FFEFC6'></span><span style='background-color:#D6EFD6'></span><span style='background-color:#CEDEE7'></span><span style='background-color:#CEE7F7'></span><span style='background-color:#D6D6E7'></span><span style='background-color:#E7D6DE'></span><span style='background-color:#FFE79C'></span>",
                callback: function ($dropdown) {                    
                	$dropdown.find('span').each(function () {
                		$(this).click(function () {  
							var bgcode = $(this).css("background-color");
							sn$(".note-editable").append("<div class='note-wrap' style='background-color:"+bgcode+"'></div>").css("background-color",bgcode);
                        });
                    });
                }
            })
		]);

		return button.render();
	}

	sn$('#howtomake').summernote({
		focus: true,
		placeholder: '내용을 입력해 주세요.',
		lang: 'ko-KR',
		height: 150,
		fontsize: '14',
		minHeight: null,
		maxHeight: null, 
		focus: false,
        followingToolbar: false,
		toolbar : [
			['mybutton',['bgcolor']],
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture']],
			['view', ['fullscreen']], //codeview
		],
	    buttons: {
	      bgcolor: bgButton
	    },
	    callbacks: {
	    	onFocus: function (contents) {
	    		if(sn$('#howtomake').summernote('isEmpty')) {
	    	    	sn$('#howtomake').summernote("code", "");
	    	    }
	    	},
        	onImageUpload: function(file, editor, welEditable) {
        		sendHowToMake(file[0], editor, welEditable);
        	},
        	onPaste: function (e) {
                var clipboardData = e.originalEvent.clipboardData;
                if (clipboardData && clipboardData.items && clipboardData.items.length) {
                    var item = clipboardData.items[0];
                    if (item.kind === 'file' && item.type.indexOf('image/') !== -1) {
                        e.preventDefault();
                    }
                }
            }
      	}
  	});

	function sendHowToMake(file, editor, welEditable) {		
		var url = "/summernote/upload";
		var formData = new FormData();
        formData.append("upfile", file);
        
        $.ajax({
            type:"POST",
            url:url,
            data:formData,
            cache: false,
            processData: false,  // file전송시 필수
            contentType: false,  // file전송시 필수
            beforeSend: function() {
            },
            success:function(response) {            	
            	if(response.result == 200) {            
            		console.log("response.url : " + response.url);
            		sn$('#howtomake').summernote('insertImage', response.url, response.filename);
                }
            },
            error:function(request, status, error) {
                alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
            }
        });
	}
	
	sn$('#howtoeat').summernote({
		focus: true,
		placeholder: '내용을 입력해 주세요.',
		lang: 'ko-KR',
		height: 150,
		fontsize: '14',
		minHeight: null,
		maxHeight: null, 
		focus: false,
        followingToolbar: false,
		toolbar : [
			['mybutton',['bgcolor']],
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture']],
			['view', ['fullscreen']], //codeview
		],
	    buttons: {
	      bgcolor: bgButton
	    },
	    callbacks: {
	    	onFocus: function (contents) {
	    		if(sn$('#howtoeat').summernote('isEmpty')) {
	    	    	sn$('#howtoeat').summernote("code", "");
	    	    }
	    	},
        	onImageUpload: function(file, editor, welEditable) {
        		sendHowToEat(file[0], editor, welEditable);
        	},
        	onPaste: function (e) {
                var clipboardData = e.originalEvent.clipboardData;
                if (clipboardData && clipboardData.items && clipboardData.items.length) {
                    var item = clipboardData.items[0];
                    if (item.kind === 'file' && item.type.indexOf('image/') !== -1) {
                        e.preventDefault();
                    }
                }
            }
      	}
  	});

	function sendHowToEat(file, editor, welEditable) {		
		var url = "/summernote/upload";
		var formData = new FormData();
        formData.append("upfile", file);
        
        $.ajax({
            type:"POST",
            url:url,
            data:formData,
            cache: false,
            processData: false,  // file전송시 필수
            contentType: false,  // file전송시 필수
            beforeSend: function() {
            },
            success:function(response) {            	
            	if(response.result == 200) {            
            		console.log("response.url : " + response.url);
            		sn$('#howtoeat').summernote('insertImage', response.url, response.filename);
                }
            },
            error:function(request, status, error) {
                alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
            }
        });
	}
});
</script>
