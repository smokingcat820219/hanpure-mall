<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!doctype html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>ERROR</title>
	<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Noto+Sans+KR&display=swap');
	* {margin:0; padding:0; line-height:1.5;}
	em {font-style:normal;}
	html,body {width:100%; height:100%; font-size:16px; font-family: 'Noto Sans KR', sans-serif; font-weight:300; word-break:keep-all;}
	#wrap_error {display:table; width:100%; height:100%; text-align:center;}
	#wrap_error > .inner {display:table-cell; vertical-align:middle;}
	#wrap_error > .inner > div {display:inline-block; max-width:800px; padding:3% 5%; text-align:left;}
	#wrap_error .title {position:relative; display:block; padding-bottom:15px; margin-bottom:30px; font-weight:900; font-size:450%; color:#fc0000;}
	#wrap_error .title:after {content:""; position:absolute; left:0; bottom:0; width:100%; height:1px; background-color:#000;}
	#wrap_error .t1 {font-size:1.25em; color:#636363; margin-bottom:10px;}
	#wrap_error .t2 {font-size:1em; color:#a3a3a3;}
	#wrap_error .btns {margin-top:40px;}
	#wrap_error .btns a {text-decoration:none; display:inline-block; padding:13px 20px; min-width:180px; border:1px solid #333; color:#333; text-align:center; font-size:20px; line-height:1; font-weight:700;}
	#wrap_error .btns a:hover {background-color:#333; color:#fff;}
	</style>
</head>
<body>
	<div id="wrap_error">
		<div class="inner"><div>
			<h1 class="title"><span>E<em>rror</em> 503</span></h1>
			<div class="txt">
				<p class="t1"><strong>Service Unavailable</strong></p>
				<p class="t2">서비스를 사용 할 수 없습니다.</p>
				<div class="btns">
					<a href="javascript:">관리자에게 문의해 주세요.</a>
				</div>
			</div>
		</div></div>
	</div>
</body>
</html>