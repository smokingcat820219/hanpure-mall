<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	var TYPE = "M";
	
	$(function() {		
		$(".eat").hide();
		
		goPage();
		
		$('#btnAdd').click(function(e) {
			showPopup(1000, 800, "/howto/write?TYPE=" + TYPE);
	    });
		
		$('#btnDelete').click(function(e) {
			if(ajaxRunning) return;
			
			var length = GetCheckedLength("chk");
			if(length == 0) {
				alert("하나 이상 선택해 주세요.");
				return;
			}
			
			var formData = new FormData();
			
			for(var i = 0; i < length; i++) {
				var howto_seq	= $("input:checkbox[name='chk']:checked").eq(i).val();
				console.log("howto_seq : " + howto_seq);
				
				formData.append("LIST_HOWTO_SEQ", howto_seq);
			}	
			
		    if(!confirm("선택하신 항목을 삭제 하시겠습니까?")) {
		    	return;
		    }
		    
		    var url = "/howto/delete";
		    $.ajax({
		        type:"POST",
		        url:url,
		        data:formData,
		        cache: false,
		        processData: false,  // file전송시 필수
		        contentType: false,  // file전송시 필수
		        beforeSend: function() {
		        	ShowCSS($(".loadingbar"));
		        },
		        success:function(response) {
		        	HideCSS($(".loadingbar"));
		        	
		            if(response.result == 200) {	 
		            	goPage();
		            }
		            else {
		            	alert(response.message);
		            }
		        },
		        error:function(request, status, error) {
		        	HideCSS($(".loadingbar"));
		        	
		            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    });
	    });
	});
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("TYPE", TYPE);      
		
	    var url = "/howto/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {    	            			
	            			sHTML += "<tr>";
	            			sHTML += "	<td><label class='inp_checkbox'><input type='checkbox' name='chk' class='chk' value='" + response.list[i].HOWTO_SEQ + "' /><span class='d' title='전체선택'></span></label></td>";
	            			sHTML += "	<td>" + (i + 1) + "</td>";
	            			sHTML += "	<td class='ta-l' onClick=\"goUpdate('" + response.list[i].HOWTO_SEQ + "')\">" + response.list[i].TITLE + "</td>";
	            			sHTML += "	<td class='eat'><span class='ico_file'>" + response.list[i].FILE_QNTT + "</span></td>";
	            			sHTML += "</tr>";
	            		}
	            	}
	            	else {
	            		if(TYPE == "M") {
	            			sHTML += "<tr>";
		            		sHTML += "	<td colspan='3'>";
		            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
							sHTML += "	</td>";
							sHTML += "</tr>";	
	            		}
	            		else {
	            			sHTML += "<tr>";
		            		sHTML += "	<td colspan='4'>";
		            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
							sHTML += "	</td>";
							sHTML += "</tr>";
	            		}	            		
	            	}
	            	
	            	$("#tbody").html(sHTML); 
	            	
	            	if(TYPE == "M") {
	            		$(".eat").hide();
	            	}
	            	else {
	            		$(".eat").show();
	            	}
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goUpdate(howto_seq) {
		showPopup(1000, 800, "/howto/update/" + howto_seq + "/");
	}
	
	function allCheck(obj) {
		if($(obj).prop("checked")) {
			$(".chk").prop("checked", true);
		}
		else {
			$(".chk").prop("checked", false);
		}
	}
	
	function setTab(tab) {
		if(tab == "1") {
			TYPE = "M";
			
			$("#tab1").addClass("on");
			$("#tab2").removeClass("on");
			
			goPage(); 
		}
		else if(tab == "2") {
			TYPE = "E";
			
			$("#tab1").removeClass("on");
			$("#tab2").addClass("on");
			
			goPage(); 
		}		
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">조제지시 및 복용법</h2>
			</header>

			<div class="tab ty1 big">
				<ul class="n2">
					<li id="tab1" class="on" onClick="setTab('1')"><a href="javascript:"><span>조제지시</span></a></li>
					<li id="tab2" onClick="setTab('2')"><a href="javascript:"><span>복용법</span></a></li>
				</ul>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno"></strong>건</p>
			</div>
			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num" />
						<col class="num" />
						<col />
						<col class="name eat" />
					</colgroup>
					<thead>
						<tr>
							<th><label class="inp_checkbox"><input type="checkbox" onClick="allCheck(this)" /><span class="d" title="전체선택"></span></label></th>
							<th>순번</th>
							<th>제목</th>							
							<th class="eat">첨부파일</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="tbl_botm ta-r">
				<a href="javascript:;" class="btn-pk n blue" id="btnAdd"><span>신규등록</span></a>
				<a href="javascript:;" class="btn-pk n white" id="btnDelete"><span>삭제</span></a>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>