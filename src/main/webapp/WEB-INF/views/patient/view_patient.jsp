<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(function() {	 		
	    $('#btnList').click(function() {	    	
	    	location.href = "/patient/list" + window.location.hash;
	    });
	    
	    $('#btnUpdate').click(function() {	    	
	    	location.href = "/patient/update/" + "${VO.PATIENT_SEQ}" + "/" + window.location.hash;
	    }); 
	});	
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">환자정보</h2>
			</header>

			<h3 class="h_stit1 f-en mb20">환자정보 상세</h3>

			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">환자 정보</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>차트번호</th>
								<td>${VO.CHART_NO}</td>
							</tr>
							<tr>
								<th>사상체질</th>
								<td>${VO.PATIENT_TP_NM}</td>
							</tr>
							<tr>
								<th>초진일</th>
								<td>
									<c:if test="${LIST_RECIPE.size() > 0}">
										<mask:display type="DATE10" value="${LIST_RECIPE.get(0).CREATED_AT}" />
									</c:if>
								</td>
							</tr>
							<tr>
								<th>환자명</th>
								<td>${VO.NAME}</td>
							</tr>
							<tr>
								<th>성별</th>
								<td>${VO.SEX_NM}</td>
							</tr>
							<tr>
								<th>생년월일</th>
								<td>${VO.BIRTH}</td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>${VO.TEL}</td>
							</tr>
							<tr>
								<th>휴대폰</th>
								<td>${VO.MOBILE}</td>
							</tr>
							<tr>
								<th>우편번호</th>
								<td>${VO.ZIPCODE}</td>
							</tr>
							<tr>
								<th>주소</th>
								<td>${VO.ADDRESS}</td>
							</tr>
							<tr>
								<th>상세주소</th>
								<td>${VO.ADDRESS2}</td>
							</tr>
							<tr>
								<th>환자 메모</th>
								<td>${VO.REMARK}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div><!--// lft_c -->

			<!-- 우측데이터 -->
			<div class="rgh_c">
				<div class="top_tit">
					<h3 class="h_tit2">처방내역</h3>
					<div class="rgh">
						<a href="/recipe/write?RECIPE_TP=001&PATIENT_SEQ=${VO.PATIENT_SEQ}" class="btn-pk n blue"><span>새 처방하기</span></a>
					</div>
				</div>
				<div class="tbl_basic">
					<table class="list default cur">
						<colgroup>
							<col class="num">
							<col>
							<col class="day">
							<col class="name">
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>처방명</th>
								<th>처방일</th>
								<th>처방하기</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
								<tr>
									<td>${i}</td>
									<td class="subject">${LIST_RECIPE.get(i - 1).RECIPE_NM}</td>
									<td><mask:display type="DATE10" value="${LIST_RECIPE.get(i - 1).CREATED_AT}" /></td>
									<td><a href="/recipe/copy/${LIST_RECIPE.get(i - 1).RECIPE_SEQ}/" class="btn-pk s blue2"><span>처방하기</span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div><!--// rgh_c -->

			<div class="clb btn-bot">
				<div class="lft">
					<a href="javascript:" id="btnList" class="btn-pk b blue2"><span>목록</span></a>
					<a href="javascript:" id="btnUpdate" class="btn-pk b blue"><span>수정</span></a>
				</div>
			</div>

		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>