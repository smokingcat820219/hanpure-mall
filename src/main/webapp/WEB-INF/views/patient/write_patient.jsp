<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>

<script>
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
	    $('#btnSave').click(function() {
	    	goSave();
	    });	
	});
	
	function goSave() {
		if(ajaxRunning) return;
		
		var chart_no = GetValue("chart_no");
		var patient_tp = GetValue("patient_tp");
		var name = GetValue("name");
		var sex = GetValue("sex");
		var birth = GetValue("birth");
		var tel1 = GetValue("tel1");
		var tel2 = GetValue("tel2");
		var tel3 = GetValue("tel3");		
		var mobile1 = GetValue("mobile1");
		var mobile2 = GetValue("mobile2");
		var mobile3 = GetValue("mobile3");		
		var zipcode = GetValue("zipcode");
		var address = GetValue("address");
		var address2 = GetValue("address2");
		var remark = GetValue("remark");
				
		if(!name) {
	    	alert("환자명을 입력해 주세요.");
	    	$("#name").focus();
	    	return;
	    }
		
		if(!mobile1) {
	    	alert("휴대폰번호를 입력해 주세요.");
	    	$("#mobile1").focus();
	    	return;
	    }
		
		if(!mobile2) {
	    	alert("휴대폰번호를 입력해 주세요.");
	    	$("#mobile2").focus();
	    	return;
	    }  
		
		if(!mobile3) {
	    	alert("휴대폰번호를 입력해 주세요.");
	    	$("#mobile3").focus();
	    	return;
	    }  
		
		if(!zipcode) {
	    	alert("우편번호 검색을 해주세요.");
	    	$("#zipcode").focus();
	    	return;
	    }
		
		if(!address) {
	    	alert("우편번호 검색을 해주세요.");
	    	$("#address").focus();
	    	return;
	    }
		
		if(!address2) {
	    	alert("상세주소를 입력해 주세요.");
	    	$("#address2").focus();
	    	return;
	    }  	
		
		var tel = tel1 + "-" + tel2 + "-" + tel3;
		var mobile = mobile1 + "-" + mobile2 + "-" + mobile3;
	   	    
	    var formData = new FormData();
	    formData.append("PATIENT_TP", patient_tp);
	    formData.append("CHART_NO", chart_no);
	    formData.append("NAME", name);
	    formData.append("SEX", sex);
	    formData.append("BIRTH", birth);
	    formData.append("TEL", tel);
	    formData.append("MOBILE", mobile);
	    formData.append("ZIPCODE", zipcode);
	    formData.append("ADDRESS", address);
	    formData.append("ADDRESS2", address2);
	    formData.append("REMARK", remark);
		
	    if(!confirm("입력하신 내용을 등록 하시겠습니까?")) {
	    	return;
	    }
	    
	    var url = "/patient/write";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	 
	            	alert("입력하신 내용을 등록 완료 되었습니다.");
	            	
	            	try {
	            		parent.goPage();	
	            	}catch(e) {
	            		
	            	}	            	
	            	
	            	$(".b-close").trigger("click");
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">환자 등록</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<p class="fz1 mb10">
			*표시된 항목은 필수 입력 항목으로, 입력하지 않으면 저장되지 않습니다.
			<br>성별, 생년월일을 입력하지 않으면, 정확한 분석 보고서 제공이 어렵습니다.
		</p>
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col class="th2">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>차트번호</th>
						<td><input type="text" class="inp_txt w100p" name="chart_no" id="chart_no" placeholder="차트번호를 입력하세요. 미 입력 시, 자동으로 생성 됩니다." /></td>
					</tr>
					<tr>
						<th>사상체질</th>
						<td>
							<select name="patient_tp" id="patient_tp" class="select1 w100p">
								<c:forEach var="i" begin="1" end="${LIST_PATIENT_TP.size()}">
									<option value="${LIST_PATIENT_TP.get(i - 1).SUB_CD}">${LIST_PATIENT_TP.get(i - 1).SUB_NM_KR}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>환자명 <span class="i_emp">*</span></th>
						<td><input type="text" class="inp_txt w100p" name="name" id="name" placeholder="환자명을 입력하세요" /></td>
					</tr>
					<tr>
						<th>성별</th>
						<td>
							<select name="sex" id="sex" class="select1 w100p">
								<option value="">선택</option>
								<c:forEach var="i" begin="1" end="${LIST_SEX_TP.size()}">
									<option value="${LIST_SEX_TP.get(i - 1).SUB_CD}">${LIST_SEX_TP.get(i - 1).SUB_NM_KR}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>생년월일</th>
						<td>
							<div class="inp_days">
								<input type="text" name="birth" id="birth" class="inp_txt calender datepicker date" readOnly placeholder="yyyy-mm-dd" />
								<button type="button" class="btn-pk n blue ml20" onClick="$('#birth').val('')"><span>초기화</span></button>
							</div>
						</td>
					</tr>
					<tr>
						<th>전화번호</th>
						<td>
							<div class="inp_phone">
								<input type="text" class="inp_txt number" name="tel1" id="tel1" maxlength="4" />
								<span>-</span>
								<input type="text" class="inp_txt number" name="tel2" id="tel2" maxlength="4" />
								<span>-</span>
								<input type="text" class="inp_txt number" name="tel3" id="tel3" maxlength="4" />
							</div>
						</td>
					</tr>
					<tr>
						<th>휴대폰 <span class="i_emp">*</span></th>
						<td>
							<div class="pos-r">
								<div class="inp_phone">
									<input type="text" class="inp_txt number" name="mobile1" id="mobile1" maxlength="4" />
									<span>-</span>
									<input type="text" class="inp_txt number" name="mobile2" id="mobile2" maxlength="4" />
									<span>-</span>
									<input type="text" class="inp_txt number" name="mobile3" id="mobile3" maxlength="4" />
								</div>
								<!-- <button type="button" class="btn-pk n blue2" id="btnCheck"><span>중복확인</span></button> -->
							</div>
						</td>
					</tr>
					<tr>
						<th>주소<span class="i_emp">*</span></th>
						<td>
							<div class="inp_addr">
								<div class="inp">
									<button type="button" class="btn-pk n blue2 daum_post"><span>우편번호</span></button>
									<input type="text" class="inp_txt daum_post" name="zipcode" id="zipcode" readOnly placeholder="우편번호 검색을 클릭하세요">
								</div>
								<input type="text" class="inp_txt w100p" name="address" id="address" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력" />
								<input type="text" class="inp_txt w100p" name="address2" id="address2" placeholder="나머지 상세 주소를 입력하세요" maxlength="100" />
							</div>
						</td>
					</tr>
					<tr>
						<th>환자메모</th>
						<td><input type="text" class="inp_txt w100p" name="remark" id="remark" placeholder="환자 메모는 탕전실과 고객에게 전달되지 않습니다"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="btn-bot">
			<a href="javascript:" id="btnSave" class="btn-pk n blue"><span>등록</span></a>
			<a href="javascript:" class="btn-pk n white b-close"><span>취소</span></a>
		</div>
	</div>
</div>





<script>
$(function(){
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
})
</script>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>