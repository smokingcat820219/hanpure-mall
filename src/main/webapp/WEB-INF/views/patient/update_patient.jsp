<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script>
	$(function() {	 		
	    $('#btnList').click(function() {	    	
	    	location.href = "/patient/list" + window.location.hash;
	    });
	    
	    $('#btnSave').click(function() {
	    	goSave();
	    }); 
	});	
	
	function goSave() {
		if(ajaxRunning) return;
		
		var patient_seq = "${VO.PATIENT_SEQ}";
		var chart_no = GetValue("chart_no");
		var name = GetValue("name");
		var sex = GetValue("sex");
		var birth = GetValue("birth");
		var tel1 = GetValue("tel1");
		var tel2 = GetValue("tel2");
		var tel3 = GetValue("tel3");		
		var mobile1 = GetValue("mobile1");
		var mobile2 = GetValue("mobile2");
		var mobile3 = GetValue("mobile3");		
		var zipcode = GetValue("zipcode");
		var address = GetValue("address");
		var address2 = GetValue("address2");
		var remark = GetValue("remark");
			
		if(!chart_no) {
	    	alert("차트번호를 입력해 주세요.");
	    	$("#chart_no").focus();
	    	return;
	    }
		
		if(!name) {
	    	alert("환자명을 입력해 주세요.");
	    	$("#name").focus();
	    	return;
	    }
		
		//if(!birth) {
		//	alert("생년월일을 선택해 주세요.");
	    //	$("#birth").focus();
	    //	return;	
		//}
		
		if(!mobile1) {
	    	alert("휴대폰번호를 입력해 주세요.");
	    	$("#mobile1").focus();
	    	return;
	    }
		
		if(!mobile2) {
	    	alert("휴대폰번호를 입력해 주세요.");
	    	$("#mobile2").focus();
	    	return;
	    }  
		
		if(!mobile3) {
	    	alert("휴대폰번호를 입력해 주세요.");
	    	$("#mobile3").focus();
	    	return;
	    }  
		
		if(!zipcode) {
	    	alert("우편번호 검색을 해주세요.");
	    	$("#zipcode").focus();
	    	return;
	    }
		
		if(!address) {
	    	alert("우편번호 검색을 해주세요.");
	    	$("#address").focus();
	    	return;
	    }
		
		if(!address2) {
	    	alert("상세주소를 입력해 주세요.");
	    	$("#address2").focus();
	    	return;
	    }  	
		
		var tel = tel1 + "-" + tel2 + "-" + tel3;
		var mobile = mobile1 + "-" + mobile2 + "-" + mobile3;
	   	    
	    var formData = new FormData();
	    formData.append("PATIENT_SEQ", patient_seq);
	    formData.append("PATIENT_TP", GetValue("patient_tp"));
	    formData.append("CHART_NO", chart_no);
	    formData.append("NAME", name);
	    formData.append("SEX", sex);
	    formData.append("BIRTH", birth);
	    formData.append("TEL", tel);
	    formData.append("MOBILE", mobile);
	    formData.append("ZIPCODE", zipcode);
	    formData.append("ADDRESS", address);
	    formData.append("ADDRESS2", address2);
	    formData.append("REMARK", remark);
		
	    if(!confirm("입력하신 내용을 수정 하시겠습니까?")) {
	    	return;
	    }
	    
	    var url = "/patient/update";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	 
	            	alert("입력하신 내용을 수정 완료 되었습니다.");
	            	
	            	location.href = "/patient/view/" + "${VO.PATIENT_SEQ}" + "/" + window.location.hash;
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">환자정보</h2>
			</header>

			<h3 class="h_stit1 f-en mb20">환자정보 상세</h3>

			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">환자 정보</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>차트번호</th>
								<td><input type="text" class="inp_txt w100p" name="chart_no" id="chart_no" value="${VO.CHART_NO}" placeholder="차트번호를 입력하세요. 미 입력 시, 자동으로 생성 됩니다." /></td>
							</tr>
							<tr>
								<th>사상체질</th>
								<td>
									<select name="patient_tp" id="patient_tp" class="select1 w100p">
										<c:forEach var="i" begin="1" end="${LIST_PATIENT_TP.size()}">
											<option value="${LIST_PATIENT_TP.get(i - 1).SUB_CD}" <c:if test="${VO.PATIENT_TP eq LIST_PATIENT_TP.get(i - 1).SUB_CD}">select</c:if>>${LIST_PATIENT_TP.get(i - 1).SUB_NM_KR}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th>초진일</th>
								<td>
									<c:if test="${LIST_RECIPE.size() > 0}">
										<mask:display type="DATE10" value="${LIST_RECIPE.get(0).CREATED_AT}" />
									</c:if>
								</td>
							</tr>
							<tr>
								<th>환자명 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt w100p" name="name" id="name" value="${VO.NAME}" placeholder="환자명을 입력하세요" /></td>
							</tr>
							<tr>
								<th>성별</th>
								<td>
									<select name="sex" id="sex" class="select1 w100p">
										<option value="">선택</option>
										<c:forEach var="i" begin="1" end="${LIST_SEX_TP.size()}">
											<option value="${LIST_SEX_TP.get(i - 1).SUB_CD}" <c:if test="${VO.SEX == LIST_SEX_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_SEX_TP.get(i - 1).SUB_NM_KR}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th>생년월일</th>
								<td>
									<div class="inp_days">
										<input type="text" name="birth" id="birth" class="inp_txt calender datepicker date" value="${VO.BIRTH}" readOnly placeholder="yyyy-mm-dd" />
										<button type="button" class="btn-pk n blue ml20" onClick="$('#birth').val('')"><span>초기화</span></button>
									</div>
								</td>
							</tr>
							<tr>
								<th>전화번호</th>
								<td>
									<div class="inp_phone">
										<input type="text" class="inp_txt number" name="tel1" id="tel1" value='<mask:display type="TEL1" value="${VO.TEL}" />' maxlength="4" />
										<span>-</span>
										<input type="text" class="inp_txt number" name="tel2" id="tel2" value='<mask:display type="TEL2" value="${VO.TEL}" />' maxlength="4" />
										<span>-</span>
										<input type="text" class="inp_txt number" name="tel3" id="tel3" value='<mask:display type="TEL3" value="${VO.TEL}" />' maxlength="4" />
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰 <span class="i_emp">*</span></th>
								<td>
									<div class="pos-r">
										<div class="inp_phone">
											<input type="text" class="inp_txt number" name="mobile1" id="mobile1" value='<mask:display type="TEL1" value="${VO.MOBILE}" />' maxlength="4" />
											<span>-</span>
											<input type="text" class="inp_txt number" name="mobile2" id="mobile2" value='<mask:display type="TEL2" value="${VO.MOBILE}" />' maxlength="4" />
											<span>-</span>
											<input type="text" class="inp_txt number" name="mobile3" id="mobile3" value='<mask:display type="TEL3" value="${VO.MOBILE}" />' maxlength="4" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>주소<span class="i_emp">*</span></th>
								<td>
									<div class="inp_addr">
										<div class="inp">
										<button type="button" class="btn-pk n blue2 daum_post"><span>우편번호</span></button>
										<input type="text" class="inp_txt daum_post" name="zipcode" id="zipcode" value="${VO.ZIPCODE}" readOnly placeholder="우편번호 검색을 클릭하세요">
									</div>
									<input type="text" class="inp_txt w100p" name="address" id="address" value="${VO.ADDRESS}" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력" />
									<input type="text" class="inp_txt w100p" name="address2" id="address2" value="${VO.ADDRESS2}" placeholder="나머지 상세 주소를 입력하세요" maxlength="100" />
									</div>
								</td>
							</tr>
							<tr>
								<th>환자 메모</th>
								<td><input type="text" class="inp_txt w100p" name="remark" id="remark" value="${VO.REMARK}" placeholder="환자 메모는 탕전실과 고객에게 전달되지 않습니다"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div><!--// lft_c -->

			<!-- 우측데이터 -->
			<div class="rgh_c">
				<div class="top_tit">
					<h3 class="h_tit2">처방내역</h3>
					<div class="rgh">
						<a href="/recipe/write?RECIPE_TP=001&PATIENT_SEQ=${VO.PATIENT_SEQ}" class="btn-pk n blue"><span>새 처방하기</span></a>
					</div>
				</div>
				<div class="tbl_basic">
					<table class="list default cur">
						<colgroup>
							<col class="num">
							<col>
							<col class="day">
							<col class="name">
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>처방명</th>
								<th>처방일</th>
								<th>처방하기</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
								<tr>
									<td>${i}</td>
									<td class="subject">${LIST_RECIPE.get(i - 1).RECIPE_NM}</td>
									<td><mask:display type="DATE10" value="${LIST_RECIPE.get(i - 1).CREATED_AT}" /></td>
									<td><a href="/recipe/copy/${LIST_RECIPE.get(i - 1).RECIPE_SEQ}/" class="btn-pk s blue2"><span>처방하기</span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div><!--// rgh_c -->

			<div class="clb btn-bot">
				<div class="lft">
					<a href="javascript:" id="btnList" class="btn-pk b blue2"><span>목록</span></a>
					<a href="javascript:" id="btnSave" class="btn-pk b blue"><span>수정</span></a>
				</div>
			</div>

		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>