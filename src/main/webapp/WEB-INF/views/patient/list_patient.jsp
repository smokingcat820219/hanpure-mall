<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {	
		$('.btnAdd').click(function(e) {
			showPopup(700, 850, "/patient/write");
	    });
		
		$('.btnDelete').click(function(e) {
			if(ajaxRunning) return;
			
			var length = GetCheckedLength("chk");
			if(length == 0) {
				alert("하나 이상 선택해 주세요.");
				return;
			}
			
			var formData = new FormData();
			
			for(var i = 0; i < length; i++) {
				var patient_seq	= $("input:checkbox[name='chk']:checked").eq(i).val();
				console.log("patient_seq : " + patient_seq);
				
				formData.append("LIST_PATIENT_SEQ", patient_seq);
			}	
			
		    if(!confirm("선택하신 항목을 삭제 하시겠습니까?")) {
		    	return;
		    }
		    
		    var url = "/patient/delete";
		    $.ajax({
		        type:"POST",
		        url:url,
		        data:formData,
		        cache: false,
		        processData: false,  // file전송시 필수
		        contentType: false,  // file전송시 필수
		        beforeSend: function() {
		        	ShowCSS($(".loadingbar"));
		        },
		        success:function(response) {
		        	HideCSS($(".loadingbar"));
		        	
		            if(response.result == 200) {	 
		            	goPage();
		            }
		            else {
		            	alert(response.message);
		            }
		        },
		        error:function(request, status, error) {
		        	HideCSS($(".loadingbar"));
		        	
		            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    });
	    });
		
		$('.btnExcelDownload').click(function(e) {
			location.href = "/patient/excel/download?" + window.location.hash.substring(1);
	    });	
		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
			$("#searchField").val("");
			$("#searchText").val("");
			
			$("#SEX").val("");
			$("#BIRTH").val("");
			$("#sdate").val("");
			$("#edate").val("");
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var SEX = GetValue("SEX");
		var BIRTH = GetValue("BIRTH");
		var sdate = GetValue("sdate");
		var edate = GetValue("edate");	
		
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		var itemPerPage = GetValue("itemPerPage");
				
		var param = "page=" + page;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;
		param += "&itemPerPage=" + itemPerPage;
		
		param += "&SEX=" + SEX;
		param += "&BIRTH=" + BIRTH;
		param += "&sdate=" + sdate;
		param += "&edate=" + edate;		
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);
		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/patient/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {    	            			
	            			if(!response.list[i].SEX_NM) response.list[i].SEX_NM = "";
	            			if(!response.list[i].BIRTH) response.list[i].BIRTH = "";
	            			
	            			sHTML += "<tr>";	            			
	            			sHTML += "	<td><label class='inp_checkbox'><input type='checkbox' name='chk' class='chk' value='" + response.list[i].PATIENT_SEQ + "' /><span class='d'></span></label></td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + startno + "</td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].CHART_NO + "</td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].NAME + "</td>";	            			
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].SEX_NM + "</td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].BIRTH + "</td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].MOBILE + "</td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].PATIENT_TP_NM + "</td>";
	            			
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">";
	            			if(response.list[i].ZIPCODE) {
	            				sHTML += "(" + response.list[i].ZIPCODE + ") ";
	            			}
	            			if(response.list[i].ADDRESS) {
	            				sHTML += response.list[i].ADDRESS + " ";
	            			}
	            			if(response.list[i].ADDRESS2) {
	            				sHTML += response.list[i].ADDRESS2;
	            			}
	            			sHTML += "	</td>";
	            			sHTML += "	<td onClick=\"goView('" + response.list[i].PATIENT_SEQ + "')\">" + response.list[i].RECIPE_DT + "</td>";
	            			sHTML += "</tr>";
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='10'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goView(patient_seq) {
		location.href = "/patient/view/" + patient_seq + "/" + window.location.hash;
	}
	
	function allCheck(obj) {
		if($(obj).prop("checked")) {
			$(".chk").prop("checked", true);
		}
		else {
			$(".chk").prop("checked", false);
		}
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">환자정보</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col">
						<p>성별</p>
						<select name="SEX" id="SEX" class="select1">
							<option value="">전체</option>
							<c:forEach var="i" begin="1" end="${LIST_SEX_TP.size()}">
								<option value="${LIST_SEX_TP.get(i - 1).SUB_CD}">${LIST_SEX_TP.get(i - 1).SUB_NM_KR}</option>
							</c:forEach>
						</select>						
					</div>
					<div class="col">
						<p>생년월일</p>
						<div class="inp_cal">
							<input type="text" name="BIRTH" id="BIRTH" class="inp_txt calender datepicker date" readOnly placeholder="yyyy-mm-dd" />
						</div>
					</div>
					<div class="col">
						<p>최근처방일</p>
						<div class="inp_cal">
							<input type="text" name="sdate" id="sdate" class="inp_txt calender datepicker_first" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate" id="edate" class="inp_txt calender datepicker_last" readOnly placeholder="yyyy-mm-dd" />
						</div>
					</div>
					<div class="col c30 sch">
						<p>검색</p>
						<select name="searchField" id="searchField" class="select1 wid1">
							<option value="">전체</option>
							<option value="1">차트번호</option>
							<option value="2">환자명</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력하세요" />
						
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				<select name="itemPerPage" id="itemPerPage" class="select1">
					<option value="10">10개씩 보기</option>
					<option value="20">20개씩 보기</option>
					<option value="30">30개씩 보기</option>
					<option value="40">40개씩 보기</option>
					<option value="50">50개씩 보기</option>
				</select>
				<div class="rgh">
					<a href="javascript:;" class="btn-pk blue n btnAdd"><span>환자등록</span></a>
					<a href="javascript:;" class="btn-pk white n btnDelete"><span>삭제</span></a>
					<a href="javascript:;" class="btn-pk blue2 n btnExcelDownload"><span>엑셀 다운로드</span></a>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num" />
						<col class="num" />
						<col width="200px" />
						<col width="100px" />
						<col width="80px" />
						<col width="100px" />
						<col class="number" />
						<col width="150px" />
						<col />						
						<col class="day2" />
					</colgroup>
					<thead>
						<tr>
							<th><label class="inp_checkbox"><input type="checkbox" onClick="allCheck(this)" /><span class="d" title="전체선택"></span></label></th>
							<th>순번</th>
							<th>차트번호</th>
							<th>환자명</th>
							<th>성별</th>
							<th>생년월일</th>
							<th>휴대폰</th>
							<th>사상체질</th>
							<th>주소</th>
							<th>최근처방일</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="tbl_botm ta-r">
				<button type="button" class="btn-pk blue btnAdd"><span>환자등록</span></button>
				<button type="button" class="btn-pk white btnDelete"><span>삭제</span></button>
				<button type="button" class="btn-pk blue2 btnExcelDownload"><span>엑셀 다운로드</span></button>
			</div>
			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>