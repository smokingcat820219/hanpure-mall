<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script src="/js/Chart.min.js"></script>
<script>	
	$(function() {			
		$('#btnExcelDownload').click(function() {
	    	downloadExcel();
	    });
		
	    $('#btnSearch').click(function() {
	    	goPage();
	    });
	    
	    $('#btnInit').click(function() {
			$("#searchField").val("1");
			
			$("#sdate1").val("${TODAY1}");
			$("#edate1").val("${TODAY2}");			
			
			$("#year1").val("${YEAR}");
			$("#year2").val("${YEAR}");
			
			$("#month1").val("${MONTH}");
			$("#month2").val("${MONTH}");
			
			$(".order_tp").prop("checked", false);
			
			select_period("1");
			
			goPage();
	    });
	    
	    select_period("1");
	    goPage();
	});
	
	function downloadExcel() {		
		var order_tp = "";
		$("input:checkbox[name='order_tp']:checked").each(function() {
			var value = $(this).val();
			if(value) {
				if(order_tp != "") order_tp += ",";
				order_tp += value;
			}			
		});	
		
		var param = "";
		param += "?searchField=" + GetValue("searchField");
		param += "&sdate1=" + GetValue("sdate1");
		param += "&edate1=" + GetValue("edate1");
		param += "&year1=" + GetValue("year1");
		param += "&year2=" + GetValue("year2");
		param += "&month1=" + GetValue("month1");
		param += "&month2=" + GetValue("month2");
		param += "&LIST_ORDER_TP=" + order_tp;
		
		location.href = "/report/list_recipe1/excel/download" + param;
	}
	
	function select_period(value) {
		$(".sdate").hide();
		$("#year1").hide();
		$("#year2").hide();
		$("#month1").hide();
		$("#month2").hide();
		
		if(value == "1") {
			$(".sdate").show();
		}
		else if(value == "2") {
			$("#year1").show();
			$("#year2").show();
			
			$("#month1").show();
			$("#month2").show();
		}
		else if(value == "3") {
			$("#year1").show();
			$("#year2").show();
		}
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var formData = new FormData();
		var searchField = GetValue("searchField");
		formData.append("searchField", searchField);
		
		var order_tp = "";
		$("input:checkbox[name='order_tp']:checked").each(function() {
			var value = $(this).val();
			if(value) {
				if(order_tp != "") order_tp += ",";
				order_tp += value;
			}			
		});	
		
		formData.append("LIST_ORDER_TP", order_tp);		
		
		if(searchField == "1") {
			var sdate1 = GetValue("sdate1");
			var edate1 = GetValue("edate1");
			
			if(!sdate1) {
				alert("날짜를 선택해 주세요.");
				$("#sdate1").focus();
				return;				
			}
			if(!edate1) {
				alert("날짜를 선택해 주세요.");
				$("#edate1").focus();
				return;				
			}
			
			formData.append("sdate1", sdate1);
			formData.append("edate1", edate1);			
		}
		else if(searchField == "2") {
			var year1 = GetValue("year1");
			var month1 = GetValue("month1");
			var year2 = GetValue("year2");
			var month2 = GetValue("month2");
						
			formData.append("year1", year1);
			formData.append("year2", year2);			
			formData.append("month1", month1);
			formData.append("month2", month2);
		}
		else if(searchField == "3") {
			var year1 = GetValue("year1");
			var year2 = GetValue("year2");
						
			formData.append("year1", year1);
			formData.append("year2", year2);
		}
		else {
			return;
		}
		
	    var url = "/report/list_recipe1";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	
	            	//$("#totalno").html(NumberFormat(response.list.length));
	            	
	            	var sum1 = 0;
	            	var sum2 = 0;
	            	var sum3 = 0;
	            	var sum4 = 0;
	            	var sum5 = 0;
	            	var sum6 = 0;
	            	
	            	for(var i = 0; i < response.list.length; i++) {
	            		sHTML += "<tr>";
	            		
	            		if(searchField == "1") {
            				sHTML += "	<th>" + response.list[i].ORDER_DT + " (" + response.list[i].DAY_OF_WEEK + ")</th>";	
            			}
            			else {
            				sHTML += "	<th>" + response.list[i].ORDER_DT + "</th>";
            			}
	            				            	
		            	sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_TP_001)+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_TP_002)+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_TP_003)+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_TP_004)+ "</td>";		            	
		            	sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_TP_007)+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].ORDER_TP_006)+ "</td>";
		            	sHTML += "</tr>";	     
		            	
		            	sum1 += parseInt(response.list[i].ORDER_TP_001);
		            	sum2 += parseInt(response.list[i].ORDER_TP_002);
		            	sum3 += parseInt(response.list[i].ORDER_TP_003);
		            	sum4 += parseInt(response.list[i].ORDER_TP_004);
		            	sum5 += parseInt(response.list[i].ORDER_TP_007);
		            	sum6 += parseInt(response.list[i].ORDER_TP_006);
	            	}
	            	
	            	$("#sum1").html(NumberFormat(sum1));
	            	$("#sum2").html(NumberFormat(sum2));
	            	$("#sum3").html(NumberFormat(sum3));
	            	$("#sum4").html(NumberFormat(sum4));
	            	$("#sum5").html(NumberFormat(sum5));
	            	$("#sum6").html(NumberFormat(sum6));
	            	
	            	$("#totalno").html(NumberFormat(sum1 + sum2 + sum3 + sum4 + sum5 + sum6));
	            	
	            	$("#tbody").html(sHTML);	      
	            	
	            	//차트
	            	//차트 그리기
	            	$("#box_chart").html("<canvas id='chart1' class='in'  width='520' height='520'></canvas>");	            	
	            	
	            	var chart1 = document.getElementById("chart1").getContext('2d');
	            	var chart = new Chart(chart1, {
	            		type: 'radar',
	            		data: {
	            			labels: ['탕전', '제환', '연조엑스', '첩약', '약속', '상용'],
	            			datasets: [{
	            				label: '처방건수',
	            				data: [sum1, sum2, sum3, sum4, sum5, sum6],
	            				backgroundColor: "rgba(27,75,210,0.4)",
	            				borderColor: "#145cff",
	            				borderWidth: 2,
	            				pointRadius: 4,
	            				pointBorderColor : "#233cf9",
	            				pointBackgroundColor : "#233cf9",
	            			}],
	            			
	            		},
	            		options: {
	            			maintainAspectRatio: false,
	            			responsive: true,
	            			legend: {
	            				display: true,
	            				position: 'bottom',
	            				labels: {
	            					fontColor: '#333',
	            					fontSize: 13,
	            					padding: 20
	            				}
	            			},
	            			layout: {
	            				padding: {
	            					left: 10,
	            					right: 10,
	            					top: 10,
	            					bottom: 10
	            				}
	            			},
	            			scale: {
	            				xAxes: [{
	            					gridLines: {
	            						backgroundColor: [ 'lightgrey', 'white' ],
	            						backgroundColorRepeat: true
	            					}
	            				}],
	            				gridLines: {
	            					color: "#eef0f6",
	            					lineWidth: 1,
	            					backgroundColor: "#eee"
	            				},
	            				/* 범위 지정
	            				ticks: {
	            					//display:false,
	            					beginAtZero: true,
	            					min: 0,
	            					max: 100,
	            				},
	            				*/
	            				pointLabels: {
	            					fontSize: 14,
	            					fontColor: "#666"
	            				},
	            			},
	            		}
	            	});
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<div class="top_tit">
				<div class="tab ty2">
					<ul class="n4">
						<li><a href="/report/list_patient"><span>환자 분석</span></a></li>
						<li class="on"><a href="/report/list_recipe1"><span>처방 분석</span></a></li>
						<li><a href="/report/list_drug"><span>처방 약재 분석</span></a></li>
						<li><a href="/report/list_payment"><span>결제비용 분석</span></a></li>
					</ul>
				</div>
			</div>

			<div class="tab ty1">
				<ul class="n2">
					<li class="on"><a href="/report/list_recipe1"><span>처방 구분</span></a></li>
					<li><a href="/report/list_recipe2"><span>유입 구분</span></a></li>
				</ul>
			</div>

			<div class="tbl_box_sch etc1">
				<div class="col c50">
					<p class="w80">기간별 조회</p>
					<select name="searchField" id="searchField" class="select1" onChange=select_period(this.value)>
						<option value="1">일별</option>
						<option value="2">월별</option>
						<option value="3">연별</option>
					</select>
					<div class="inp_cal">
						<span class="sdate"><input type="text" name="sdate1" id="sdate1" class="inp_txt calender datepicker_first" value="${TODAY1}" readOnly placeholder="yyyy-mm-dd" /></span>
						<select name="year1" id="year1" class="select1">
							<c:forEach var="i" begin="${YEAR - 5}" end="${YEAR + 5}">
								<option value="${i}" <c:if test="${YEAR eq i}">selected</c:if>>${i}년</option>
							</c:forEach>
						</select>
						<select name="month1" id="month1" class="select1">
							<option value="01" <c:if test="${MONTH eq '01'}">selected</c:if>>1월</option>
							<option value="02" <c:if test="${MONTH eq '02'}">selected</c:if>>2월</option>
							<option value="03" <c:if test="${MONTH eq '03'}">selected</c:if>>3월</option>
							<option value="04" <c:if test="${MONTH eq '04'}">selected</c:if>>4월</option>
							<option value="05" <c:if test="${MONTH eq '05'}">selected</c:if>>5월</option>
							<option value="06" <c:if test="${MONTH eq '06'}">selected</c:if>>6월</option>
							<option value="07" <c:if test="${MONTH eq '07'}">selected</c:if>>7월</option>
							<option value="08" <c:if test="${MONTH eq '08'}">selected</c:if>>8월</option>
							<option value="09" <c:if test="${MONTH eq '09'}">selected</c:if>>9월</option>
							<option value="10" <c:if test="${MONTH eq '10'}">selected</c:if>>10월</option>
							<option value="11" <c:if test="${MONTH eq '11'}">selected</c:if>>11월</option>
							<option value="12" <c:if test="${MONTH eq '12'}">selected</c:if>>12월</option>								
						</select>							
						<span>~</span>
						<span class="sdate"><input type="text" name="edate1" id="edate1" class="inp_txt calender datepicker_last" value="${TODAY2}" readOnly placeholder="yyyy-mm-dd" /></span>
						<select name="year2" id="year2" class="select1">
							<c:forEach var="i" begin="${YEAR - 5}" end="${YEAR + 5}">
								<option value="${i}" <c:if test="${YEAR eq i}">selected</c:if>>${i}년</option>
							</c:forEach>
						</select>
						<select name="month2" id="month2" class="select1">
							<option value="01" <c:if test="${MONTH eq '01'}">selected</c:if>>1월</option>
							<option value="02" <c:if test="${MONTH eq '02'}">selected</c:if>>2월</option>
							<option value="03" <c:if test="${MONTH eq '03'}">selected</c:if>>3월</option>
							<option value="04" <c:if test="${MONTH eq '04'}">selected</c:if>>4월</option>
							<option value="05" <c:if test="${MONTH eq '05'}">selected</c:if>>5월</option>
							<option value="06" <c:if test="${MONTH eq '06'}">selected</c:if>>6월</option>
							<option value="07" <c:if test="${MONTH eq '07'}">selected</c:if>>7월</option>
							<option value="08" <c:if test="${MONTH eq '08'}">selected</c:if>>8월</option>
							<option value="09" <c:if test="${MONTH eq '09'}">selected</c:if>>9월</option>
							<option value="10" <c:if test="${MONTH eq '10'}">selected</c:if>>10월</option>
							<option value="11" <c:if test="${MONTH eq '11'}">selected</c:if>>11월</option>
							<option value="12" <c:if test="${MONTH eq '12'}">selected</c:if>>12월</option>	
						</select>
					</div>
				</div>
				<div class="in">
					<div class="col c80">
						<p>처방구분</p>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="" /><span>전체</span></label>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="001" /><span>탕전</span></label>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="002" /><span>제환</span></label>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="003" /><span>연조엑스</span></label>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="004" /><span>첩약</span></label>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="007" /><span>약속처방</span></label>
						<label class="inp_checkbox mr20"><input type="checkbox" name="order_tp" class="order_tp" value="006" /><span>상용처방</span></label>
					</div>
					<div class="col c10 ta-r">
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="clearfix">
				<div class="lft_c">
					<div class="tbl_top">
						<p class="t_total">
							<span class="mr20">전체주문 <strong class="c-blue" id="totalno">0</strong>건</span>
							<!-- <span>처방 구분 <strong class="c-blue">전체</strong></span> -->
						</p>
						<div class="rgh">
							<a href="javascript:;" id="btnExcelDownload" class="btn-pk blue2 n"><span>엑셀 다운로드</span></a>
						</div>
					</div>
					
					<div class="tbl_basic">
						<table class="list cur">
							<colgroup>
								<col>
								<col class="size1">
								<col class="size1">
								<col class="size1">
								<col class="size1">
								<col class="size1">
								<col class="size1">
							</colgroup>
							<thead>
								<tr>
									<th>기간별</th>
									<th>탕전</th>
									<th>제환</th>
									<th>연조엑스</th>
									<th>첩약</th>
									<th>약속</th>
									<th>상용</th>
								</tr>
							</thead>
							<tbody id="tbody">
								
							</tbody>
							<tfoot>
								<tr class="bg">
									<td>합계</td>
									<td><span id="sum1">0</span>건</td>
									<td><span id="sum2">0</span>건</td>
									<td><span id="sum3">0</span>건</td>
									<td><span id="sum4">0</span>건</td>
									<td><span id="sum5">0</span>건</td>
									<td><span id="sum6">0</span>건</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

				<div class="rgh_c">
					<div id="box_chart" class="graph">
					</div>
				</div>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>