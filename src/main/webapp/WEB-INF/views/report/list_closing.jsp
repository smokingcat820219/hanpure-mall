<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>


<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
		
		setDate(1, 1);
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('#btnExcelDownload').click(function() {
			location.href = "/report/list_closing/download?" + window.location.hash.substring(1);
	    });		
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#PAYMENT_STATUS").prop("checked", false);
	    	
	    	$("#sdate1").val("");
			$("#edate1").val("");
			
			$("#searchField").val("");
			$("#searchText").val("");
			
			$("#ORDER_TP").val("");
			
			$(".select_date").val("");
			
	    	setHash(1);
	    });
	});
	
	function setDate(type, period) {
		var today = getToday();
		if(period == "1") {
			$("#sdate" + type).val(getPreDate(today, -7));
			$("#edate" + type).val(today);
		}
		else if(period == "2") {
			$("#sdate" + type).val(getPreMonth(today, -1));
			$("#edate" + type).val(today);
		}
		else if(period == "3") {
			$("#sdate" + type).val(getPreMonth(today, -3));
			$("#edate" + type).val(today);
		}
		else if(period == "4") {
			$("#sdate" + type).val(getPreMonth(today, -6));
			$("#edate" + type).val(today);
		}
		else {
			$("#sdate" + type).val("");
			$("#edate" + type).val("");
		}
	}
	
	function setHash(page) {
		var PAYMENT_STATUS = isChecked("PAYMENT_STATUS");
		if(PAYMENT_STATUS != "Y") {
			PAYMENT_STATUS = "";
		}
		
		var sdate1 = GetValue("sdate1");
		var edate1 = GetValue("edate1");
		
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		
		var ORDER_TP = GetValue("ORDER_TP");
		
		var param = "page=" + page;
		param += "&sdate1=" + sdate1;
		param += "&edate1=" + edate1;
		
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;
		
		param += "&ORDER_TP=" + ORDER_TP;
		
		param += "&PAYMENT_STATUS=" + PAYMENT_STATUS;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);
		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/report/list_closing/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		var price1 = 0;
	            		var price2 = 0;
	            		var price3 = 0;
	            		var price4 = 0;
	            		var price5 = 0;
	            		var price6 = 0;
	            		var price7 = 0;
	            		var price8 = 0;
	            		var price9 = 0;
	            		var price10 = 0;
	            		var price11 = 0;
	            		
	            		var amount = 0;
	            		
	            		for(var i = 0; i < response.list.length; i++) {    
	            			sHTML += "<tr>";	            			
	            			sHTML += "	<td>" + (i + 1) + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORDER_TP_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORDER_STATUS_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].DOCTOR + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORDER_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].PATIENT_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORDER_DT + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE1) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE2) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE3) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE4) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE5) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE6) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE7) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE8) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE9) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE10) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRICE11) + "</td>";
	            			sHTML += "	<td>" + NumberFormat(response.list[i].AMOUNT) + "</td>";	            			
	            			sHTML += "</tr>";
	            			
	            			price1 += parseInt(response.list[i].PRICE1);
		            		price2 += parseInt(response.list[i].PRICE2);
		            		price3 += parseInt(response.list[i].PRICE3);
		            		price4 += parseInt(response.list[i].PRICE4);
		            		price5 += parseInt(response.list[i].PRICE5);
		            		price6 += parseInt(response.list[i].PRICE6);
		            		price7 += parseInt(response.list[i].PRICE7);
		            		price8 += parseInt(response.list[i].PRICE8);
		            		price9 += parseInt(response.list[i].PRICE9);
		            		price10 += parseInt(response.list[i].PRICE10);
		            		price11 += parseInt(response.list[i].PRICE11);
		            		amount += parseInt(response.list[i].AMOUNT);	            			
	            		}
	            		
	            		$("#price1").html(NumberFormat(price1));
	            		$("#price2").html(NumberFormat(price2));
	            		$("#price3").html(NumberFormat(price3));
	            		$("#price4").html(NumberFormat(price4));
	            		$("#price5").html(NumberFormat(price5));
	            		$("#price6").html(NumberFormat(price6));
	            		$("#price7").html(NumberFormat(price7));
	            		$("#price8").html(NumberFormat(price8));
	            		$("#price9").html(NumberFormat(price9));
	            		$("#price10").html(NumberFormat(price10));
	            		$("#price11").html(NumberFormat(price11));
	            		$("#amount").html(NumberFormat(amount));	            		
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='19'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
						
						$("#price1").html("");
	            		$("#price2").html("");
	            		$("#price3").html("");
	            		$("#price4").html("");
	            		$("#price5").html("");
	            		$("#price6").html("");
	            		$("#price7").html("");
	            		$("#price8").html("");
	            		$("#price9").html("");
	            		$("#price10").html("");
	            		$("#price11").html("");
	            		$("#amount").html("");	            
	            	}
	            	
	            	$("#tbody").html(sHTML);
					//$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c40">
						<p>주문 일자</p>
						<div class="inp_cal">
							<input type="text" name="sdate1" id="sdate1" class="inp_txt calender datepicker_first date" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate1" id="edate1"  class="inp_txt calender datepicker_last date" readOnly placeholder="yyyy-mm-dd" />
						</div>
						<select class="select1 select_date" onChange="setDate('1', this.value)">
							<option value="">전체</option>
							<option value="1" selected>7일</option>
							<option value="2">1개월</option>
							<option value="3">3개월</option>
							<option value="4">6개월</option>
						</select>
					</div>
					<div class="col c50 sch">
						<p>검색</p>
						<select name="ORDER_TP" id="ORDER_TP" class="select1" style="width:150px">
							<option value="">전체</option>
							<option value="001">탕전</option>
							<option value="002">제환</option>
							<option value="003">연조엑스</option>
							<option value="004">첩약</option>
						</select>
						<select name="searchField" id="searchField" class="select1">
							<option value="">전체</option>
							<option value="1">처방의</option>
							<option value="2">처방명</option>
							<option value="3">환자명</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력해 주세요." />
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
					<div class="col c10">
						<label class="inp_checkbox"><input type="checkbox" name="PAYMENT_STATUS" id="PAYMENT_STATUS" value="Y" /><span>미결제처방제외</span></label>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				<div class="rgh">
					<a href="javascript:;" id="btnExcelDownload" class="btn-pk blue2 n"><span>엑셀 다운로드</span></a>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col class="day">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
						<tr>
							<th>번호</th>
							<th>구분</th>
							<th>상태</th>
							<th>처방의</th>
							<th>처방명</th>
							<th>환자명</th>
							<th>처방일시</th>
							<th>약재비</th>
							<th>탕전비/분말비</th>
							<th>특수탕전비/농축비</th>
							<th>향미제/제형비/스틱포장비</th>
							<th>앰플/부형제</th>
							<th>녹용별전/청소비</th>
							<th>주수상반/부재료</th>
							<th>조제비/수공비/조제탕전비</th>
							<th>포장비</th>
							<th>배송비</th>
							<th>기타</th>
							<th>합계</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
					<tfoot>
						<tr class="bg">
							<td colspan="7">합계</td>
							<td id="price1"></td>
							<td id="price2"></td>
							<td id="price3"></td>
							<td id="price4"></td>
							<td id="price5"></td>
							<td id="price6"></td>
							<td id="price7"></td>
							<td id="price8"></td>
							<td id="price9"></td>
							<td id="price10"></td>
							<td id="price11"></td>
							<td><strong id="amount"></strong></td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="pagenation">
				
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>