<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>


<script>
	var SORT = "1";
	
	function setSort(sort) {
		console.log("setSort");
		
		SORT = sort;
		
		goPage();
	}
	
	$(function() {	
		$('#DRUG_NM').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	goPage();
	        }
	    });	
		
		$('#btnExcelDownload').click(function() {
	    	downloadExcel();
	    });
		
	    $('#btnSearch').click(function() {
	    	goPage();
	    });
	    
	    $('#btnInit').click(function() {
			$("#searchField").val("1");
			
			$("#sdate1").val("${TODAY1}");
			$("#edate1").val("${TODAY2}");			
			
			$("#year1").val("${YEAR}");
			$("#year2").val("${YEAR}");
			
			$("#month1").val("${MONTH}");
			$("#month2").val("${MONTH}");
			
			$("#DRUG_NM").val("");
			
			select_period("1");
			
			goPage();
	    });
	    
	    select_period("1");
	    goPage();
	});
	
	function downloadExcel() {		
		var param = "";
		param += "?searchField=" + GetValue("searchField");
		param += "&sdate1=" + GetValue("sdate1");
		param += "&edate1=" + GetValue("edate1");
		param += "&year1=" + GetValue("year1");
		param += "&year2=" + GetValue("year2");
		param += "&month1=" + GetValue("month1");
		param += "&month2=" + GetValue("month2");
		param += "&DRUG_NM=" + GetValue("DRUG_NM");
		param += "&SORT=" + SORT;
		
		console.log("param : " + param);
		
		location.href = "/report/list_drug/excel/download" + param;
	}
	
	function select_period(value) {
		$(".sdate").hide();
		$("#year1").hide();
		$("#year2").hide();
		$("#month1").hide();
		$("#month2").hide();
		
		if(value == "1") {
			$(".sdate").show();
		}
		else if(value == "2") {
			$("#year1").show();
			$("#year2").show();
			
			$("#month1").show();
			$("#month2").show();
		}
		else if(value == "3") {
			$("#year1").show();
			$("#year2").show();
		}
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var formData = new FormData();
		var searchField = GetValue("searchField");
		formData.append("searchField", searchField);
		formData.append("SORT", SORT);
		formData.append("DRUG_NM", GetValue("DRUG_NM"));
		
		if(searchField == "1") {
			var sdate1 = GetValue("sdate1");
			var edate1 = GetValue("edate1");
			
			if(!sdate1) {
				alert("날짜를 선택해 주세요.");
				$("#sdate1").focus();
				return;				
			}
			if(!edate1) {
				alert("날짜를 선택해 주세요.");
				$("#edate1").focus();
				return;				
			}
			
			formData.append("sdate1", sdate1);
			formData.append("edate1", edate1);			
		}
		else if(searchField == "2") {
			var year1 = GetValue("year1");
			var month1 = GetValue("month1");
			var year2 = GetValue("year2");
			var month2 = GetValue("month2");
						
			formData.append("year1", year1);
			formData.append("year2", year2);			
			formData.append("month1", month1);
			formData.append("month2", month2);
		}
		else if(searchField == "3") {
			var year1 = GetValue("year1");
			var year2 = GetValue("year2");
						
			formData.append("year1", year1);
			formData.append("year2", year2);
		}
		else {
			return;
		}
		
	    var url = "/report/list_drug";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	
	            	$("#totalno").html(NumberFormat(response.list.length));	            	
	            	
	            	for(var i = 0; i < response.list.length; i++) {
	            		sHTML += "<tr>";	     
	            		sHTML += "	<td>" + (i + 1) + "</td>";
	            		sHTML += "	<td>" + response.list[i].DRUG_NM + "</td>";
		            	sHTML += "	<td>" + NumberFormat(parseFloat(response.list[i].TOTAL_QNTT).toFixed(2))+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].RANK_TOTAL_QNTT)+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].TOTAL_PRICE)+ "</td>";
		            	sHTML += "	<td>" + NumberFormat(response.list[i].RANK_TOTAL_PRICE)+ "</td>";
		            	sHTML += "</tr>";	            	
	            	}
	            	
	            	$("#tbody").html(sHTML);	            	
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">${DEPTH2}</h2>
			</header>

			<div class="top_tit">
				<div class="tab ty2">
					<ul class="n4">
						<li><a href="/report/list_patient"><span>환자 분석</span></a></li>
						<li><a href="/report/list_recipe1"><span>처방 분석</span></a></li>
						<li class="on"><a href="/report/list_drug"><span>처방 약재 분석</span></a></li>
						<li><a href="/report/list_payment"><span>결제비용 분석</span></a></li>
					</ul>
				</div>
			</div>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c50">
						<p class="w80">기간별 조회</p>
						<select name="searchField" id="searchField" class="select1" onChange=select_period(this.value)>
							<option value="1">일별</option>
							<option value="2">월별</option>
							<option value="3">연별</option>
						</select>
						<div class="inp_cal">
							<span class="sdate"><input type="text" name="sdate1" id="sdate1" class="inp_txt calender datepicker_first" value="${TODAY1}" readOnly placeholder="yyyy-mm-dd" /></span>
							<select name="year1" id="year1" class="select1">
								<c:forEach var="i" begin="${YEAR - 5}" end="${YEAR + 5}">
									<option value="${i}" <c:if test="${YEAR eq i}">selected</c:if>>${i}년</option>
								</c:forEach>
							</select>
							<select name="month1" id="month1" class="select1">
								<option value="01" <c:if test="${MONTH eq '01'}">selected</c:if>>1월</option>
								<option value="02" <c:if test="${MONTH eq '02'}">selected</c:if>>2월</option>
								<option value="03" <c:if test="${MONTH eq '03'}">selected</c:if>>3월</option>
								<option value="04" <c:if test="${MONTH eq '04'}">selected</c:if>>4월</option>
								<option value="05" <c:if test="${MONTH eq '05'}">selected</c:if>>5월</option>
								<option value="06" <c:if test="${MONTH eq '06'}">selected</c:if>>6월</option>
								<option value="07" <c:if test="${MONTH eq '07'}">selected</c:if>>7월</option>
								<option value="08" <c:if test="${MONTH eq '08'}">selected</c:if>>8월</option>
								<option value="09" <c:if test="${MONTH eq '09'}">selected</c:if>>9월</option>
								<option value="10" <c:if test="${MONTH eq '10'}">selected</c:if>>10월</option>
								<option value="11" <c:if test="${MONTH eq '11'}">selected</c:if>>11월</option>
								<option value="12" <c:if test="${MONTH eq '12'}">selected</c:if>>12월</option>								
							</select>							
							<span>~</span>
							<span class="sdate"><input type="text" name="edate1" id="edate1" class="inp_txt calender datepicker_last" value="${TODAY2}" readOnly placeholder="yyyy-mm-dd" /></span>
							<select name="year2" id="year2" class="select1">
								<c:forEach var="i" begin="${YEAR - 5}" end="${YEAR + 5}">
									<option value="${i}" <c:if test="${YEAR eq i}">selected</c:if>>${i}년</option>
								</c:forEach>
							</select>
							<select name="month2" id="month2" class="select1">
								<option value="01" <c:if test="${MONTH eq '01'}">selected</c:if>>1월</option>
								<option value="02" <c:if test="${MONTH eq '02'}">selected</c:if>>2월</option>
								<option value="03" <c:if test="${MONTH eq '03'}">selected</c:if>>3월</option>
								<option value="04" <c:if test="${MONTH eq '04'}">selected</c:if>>4월</option>
								<option value="05" <c:if test="${MONTH eq '05'}">selected</c:if>>5월</option>
								<option value="06" <c:if test="${MONTH eq '06'}">selected</c:if>>6월</option>
								<option value="07" <c:if test="${MONTH eq '07'}">selected</c:if>>7월</option>
								<option value="08" <c:if test="${MONTH eq '08'}">selected</c:if>>8월</option>
								<option value="09" <c:if test="${MONTH eq '09'}">selected</c:if>>9월</option>
								<option value="10" <c:if test="${MONTH eq '10'}">selected</c:if>>10월</option>
								<option value="11" <c:if test="${MONTH eq '11'}">selected</c:if>>11월</option>
								<option value="12" <c:if test="${MONTH eq '12'}">selected</c:if>>12월</option>	
							</select>
						</div>
					</div>
					<div class="col c50 sch">
						<p>약재명</p>
						<input type="text" name="DRUG_NM" id="DRUG_NM" class="inp_txt wid1" placeholder="약재명으로 검색하세요" />
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="clearfix">
				<div class="">
					<div class="tbl_top">
						<p class="t_total">전체주문 <strong class="c-blue" id="totalno">0</strong>건</p>
						<div class="rgh">
							<a href="javascript:;" id="btnExcelDownload" class="btn-pk blue2 n"><span>엑셀 다운로드</span></a>
						</div>
					</div>

					<div class="tbl_top">
						<div class="tab">
							<ul>
								<li><a href="javascript:" onClick="setSort('1')">[가나다순]</a></li>
								<li><a href="javascript:" onClick="setSort('2')">[수량 높은 순위]</a></li>
								<li><a href="javascript:" onClick="setSort('3')">[금액 높은 순위]</a></li>
							</ul>
						</div>
					</div>
					
					<div class="tbl_basic">
						<table class="list cur">
							<colgroup>
								<col class="num" />
								<col />
								<col />
								<col />
								<col />
								<col />
							</colgroup>
							<thead>
								<tr>
									<th>순번</th>
									<th>약재명</th>
									<th>주문 수량</th>
									<th>최대수량 순위</th>
									<th>주문 금액</th>
									<th>최대금액 순위</th>
								</tr>
							</thead>
							<tbody id="tbody">
								
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>