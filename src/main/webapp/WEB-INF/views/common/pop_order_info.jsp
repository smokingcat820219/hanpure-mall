<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script>	
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
		$('#btnCopy').click(function() {
			parent.location.href = "/recipe/copy/${RECIPE.RECIPE_SEQ}/";
	    });
		
		$('#btnSave').click(function() {
			if(ajaxRunning) return;
			
			var recipe_seq = "${RECIPE.RECIPE_SEQ}";
			
		    var formData = new FormData();
		    formData.append("RECIPE_SEQ", recipe_seq);
			
		    if(!confirm("나의처방에 저장 하시겠습니까?")) {
		    	return;
		    }
		    
		    var url = "/order/toMyrecipe";
		    $.ajax({
		        type:"POST",
		        url:url,
		        data:formData,
		        cache: false,
		        processData: false,  // file전송시 필수
		        contentType: false,  // file전송시 필수
		        beforeSend: function() {
		        	ShowCSS($(".loadingbar"));
		        },
		        success:function(response) {
		        	HideCSS($(".loadingbar"));
		        	
		            if(response.result == 200) {	 
		            	alert("나의처방에 저장이 완료 되었습니다.");
		            }
		            else {
		            	alert(response.message);
		            }
		        },
		        error:function(request, status, error) {
		        	HideCSS($(".loadingbar"));
		        	
		            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    });
	    });
		
		if("${RECIPE.PACKAGE1}") {
			getItemInfo("1", "${RECIPE.PACKAGE1}");	
		}
		if("${RECIPE.PACKAGE2}") {
			getItemInfo("2", "${RECIPE.PACKAGE2}");	
		}
		if("${RECIPE.PACKAGE3}") {
			getItemInfo("3", "${RECIPE.PACKAGE3}");	
		}
		if("${RECIPE.PACKAGE4}") {
			getItemInfo("4", "${RECIPE.PACKAGE4}");	
		}		
	});
	
	function getItemInfo(pkg, bom_cd) {		
		console.log("getItemInfo : " + bom_cd);
		if(!bom_cd) return; 
		
		//if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("BOM_CD", bom_cd);
		
	    var url = "/bom/selectItems";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
				
	            if(response.result == 200) {  	            	   
	            	var sHTML = "";
	            	sHTML += "	<div class='owl-carousel' id='rolling" + pkg + "'>";
	            	for(var i = 0; i < response.list.length; i++) {	
	            		sHTML += "		<div class='item'>";
	        			sHTML += "			<div class='img_prd long'>";
	        			
	        			var filepath = response.list[i].FILEPATH;
	        			var item_nm = response.list[i].ITEM_NM;
	        			
	        			if(filepath) {
	        				sHTML += "				<img src='/download?filepath=" + filepath + "' title='" + item_nm + "' />";
	        			}
	        			else {
	        				sHTML += "				<img src='/images/common/img_noimg.jpg' title='" + item_nm + "' />";
	        			}
						
						sHTML += "			</div>";
						sHTML += "		</div>";	
	            	}
	            	
	            	sHTML += "	</div>";          	
	            	
	            	$("#package" + pkg).html(sHTML);
	            	var slider = $("#package" + pkg + " #rolling" + pkg);
	        		slider.owlCarousel({
	        			loop: $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        			margin:0,
	        			nav:false,
	        			dots: $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        			items:1,
	        			smartSpeed:1500,
	        			autoplay:true,
	        			autoplayTimeout:5000,
	        			autoplayHoverPause:false,
	        			mouseDrag : $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        		});

	        		slider.trigger('refresh.owl.carousel');
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));

	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">주문내역 확인</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
				</colgroup>
				<tbody>
					<tr>
						<th>처방구분</th>
						<td colspan="5">
							<c:if test="${RECIPE.RECIPE_TP eq '001'}">탕전</c:if>
							<c:if test="${RECIPE.RECIPE_TP eq '002'}">제환</c:if>
							<c:if test="${RECIPE.RECIPE_TP eq '003'}">연조엑스</c:if>
							<c:if test="${RECIPE.RECIPE_TP eq '004'}">첩약</c:if>							
						</td>
					</tr>
					<tr>
						<th>처방명</th>
						<td colspan="5">${RECIPE.RECIPE_NM}</td>
					</tr>
					<tr>
						<th>약재 구성</th>
						<td colspan="5" class="pd">
							<div class="" style="height:150px; overflow:scroll">
								<table class="list small">
									<colgroup>
										<col class="num" />
										<col />
										<col width="80px" />
										<col width="80px" />
										<col width="80px" />
										<col width="80px" />
									</colgroup>
									<thead>
										<tr>
											<th>번호</th>
											<th>약재명</th>
											<th>유형</th>
											<th>원산지</th>
											<th>1첩량</th>
											<th>총 용량</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
											<tr>
												<td>${i}</td>
												<td>${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
												<td>
													<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">선전</c:if>
													<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">일반</c:if>
													<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">후하</c:if>												
												</td>
												<td>${LIST_RECIPE.get(i - 1).ORIGIN}</td>
												<td><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).CHUP1}" /> g</td>
												<td><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).TOTAL_QNTT}" /> g</td>
											</tr>
										</c:forEach>										
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<th>총 약재량</th>
						<td colspan="5"><mask:display type="NUMBER" value="${RECIPE.TOTAL_QNTT}" /> g</td>
					</tr>
					
					<c:if test="${RECIPE.RECIPE_TP eq '001'}">
						<tr>
							<th>첩수</th>
							<td colspan="2">${RECIPE.INFO1} 첩</td>
							<th>팩수</th>
							<td colspan="2">${RECIPE.INFO2} 팩</td>
						</tr>
						<tr>							
							<th>팩용량</th>
							<td colspan="2">${RECIPE.INFO3} mL</td>
							<th>특수탕전</th>
							<td colspan="2">${RECIPE.INFO4}</td>							
						</tr>
						<tr>
							<th>탕전법</th>
							<td colspan="2">${RECIPE.INFO5}</td>
							<th>탕전 물양</th>
							<td colspan="2"><mask:display type="NUMBER" value="${RECIPE.INFO6}" /> mL</td>							
						</tr>
						<tr>
							<th>향미제(초코)</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO7 eq 'Y'}">선택없음</c:if>
								<c:if test="${RECIPE.INFO7 eq 'N'}">선택</c:if>
							</td>
							<th>앰플(자하거)</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO8 ne ''}">${RECIPE.INFO8}mL</c:if>
								<c:if test="${RECIPE.INFO8 eq ''}">선택없음</c:if>
							</td>							
						</tr>
						<tr>	
							<th>녹용별전</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO9 ne ''}">${RECIPE.INFO9}mL</c:if>
								<c:if test="${RECIPE.INFO9 eq ''}">선택없음</c:if>
							</td>
							<th>주수상반</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO10 ne ''}">${RECIPE.INFO10_NM}/${RECIPE.INFO10_QNTT}mL</c:if>
								<c:if test="${RECIPE.INFO10 eq ''}">선택없음</c:if>
							</td>
						</tr>
						<tr>	
							<th>재탕전</th>
							<td colspan="2">
								<c:if test="${RECIPE.RE_HOTPOT eq 'Y'}">재탕전</c:if>
								<c:if test="${RECIPE.RE_HOTPOT eq 'N'}">선택없음</c:if>
							</td>
							<th>탕전시간(분)</th>
							<td colspan="2">
								<mask:display type="NUMBER" value="${RECIPE.INFO11}" />분
							</td>
						</tr>
						<tr>
							<td rowspan="3" colspan="6" class="pd">
								<table class="">
									<thead>
										<tr>
											<th class="ta-c">파우치</th>
											<th class="ta-c">탕약박스</th>
											<th class="ta-c">배송박스</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td id="package1">
																							
											</td>
											<td id="package2">
																							
											</td>
											<td id="package3">
																							
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${RECIPE.RECIPE_TP eq '002'}">
						<tr>							
							<th>첩수</th>
							<td colspan="2">${RECIPE.INFO1} 첩</td>
							<th>농축</th>
							<td colspan="2">${RECIPE.INFO2}</td>
						</tr>
						<tr>							
							<th>제형</th>
							<td colspan="2">${RECIPE.INFO3}</td>
							<th>부형제</th>
							<td colspan="2">${RECIPE.INFO4}</td>
						</tr>
						<tr>							
							<th>분말도</th>
							<td colspan="2">${RECIPE.INFO5}</td>
							<th>제분손실량</th>
							<td colspan="2"><mask:display type="NUMBER" value="${RECIPE.INFO6}" /> g</td>							
						</tr>
						<tr>							
							<th>제환손실량</th>
							<td colspan="2"><mask:display type="NUMBER" value="${RECIPE.INFO7}" /> g</td>
							<th>부형제량</th>
							<td colspan="2"><mask:display type="NUMBER" value="${RECIPE.INFO8}" /> g</td>							
						</tr>
						<tr>
							<td rowspan="3" colspan="6" class="pd">
								<table class="">
									<thead>
										<tr>
											<th class="ta-c">내지</th>
											<th class="ta-c">소포장</th>
											<th class="ta-c">포장박스</th>
											<th class="ta-c">배송박스</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td id="package1">
																							
											</td>
											<td id="package2">
																							
											</td>
											<td id="package3">
																							
											</td>
											<td id="package4">
																							
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${RECIPE.RECIPE_TP eq '003'}">
						<tr>
							<th>첩수</th>
							<td colspan="2">${RECIPE.INFO1} 첩</td>
							<th>스틱수</th>
							<td colspan="2">${RECIPE.INFO2} 포</td>
						</tr>
						<tr>
							<td rowspan="3" colspan="6" class="pd">
								<table class="">
									<thead>
										<tr>
											<th class="ta-c">스틱</th>
											<th class="ta-c">케이스</th>
											<th class="ta-c">배송박스</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td id="package1">
																							
											</td>
											<td id="package2">
																							
											</td>
											<td id="package3">
																							
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</c:if>
					
					<c:if test="${RECIPE.RECIPE_TP eq '004'}">
						<tr>
							<th>첩수</th>
							<td colspan="2">${RECIPE.INFO1} 첩</td>
							<th>팩수</th>
							<td colspan="2">${RECIPE.INFO2} 팩</td>
						</tr>
						<tr>							
							<th>팩용량</th>
							<td colspan="2">${RECIPE.INFO3} mL</td>
							<th>특수탕전</th>
							<td colspan="2">${RECIPE.INFO4}</td>							
						</tr>
						<tr>
							<th>탕전법</th>
							<td colspan="2">${RECIPE.INFO5}</td>
							<th>탕전 물양</th>
							<td colspan="2"><mask:display type="NUMBER" value="${RECIPE.INFO6}" /> mL</td>							
						</tr>
						<tr>
							<th>향미제(초코)</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO7 eq 'Y'}">선택없음</c:if>
								<c:if test="${RECIPE.INFO7 eq 'N'}">선택</c:if>
							</td>
							<th>앰플(자하거)</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO8 ne ''}">${RECIPE.INFO8}mL</c:if>
								<c:if test="${RECIPE.INFO8 eq ''}">선택없음</c:if>
							</td>							
						</tr>
						<tr>	
							<th>녹용별전</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO9 ne ''}">${RECIPE.INFO9}mL</c:if>
								<c:if test="${RECIPE.INFO9 eq ''}">선택없음</c:if>
							</td>
							<th>주수상반</th>
							<td colspan="2">
								<c:if test="${RECIPE.INFO10 ne ''}">${RECIPE.INFO10_NM}/${RECIPE.INFO10_QNTT}mL</c:if>
								<c:if test="${RECIPE.INFO10 eq ''}">선택없음</c:if>
							</td>
						</tr>
						<tr>	
							<th>재탕전</th>
							<td colspan="2">
								<c:if test="${RECIPE.RE_HOTPOT eq 'Y'}">재탕전</c:if>
								<c:if test="${RECIPE.RE_HOTPOT eq 'N'}">선택없음</c:if>
							</td>
							<th>탕전시간(분)</th>
							<td colspan="2">
								<mask:display type="NUMBER" value="${RECIPE.INFO11}" />분
							</td>
						</tr>
						<tr>
							<td rowspan="3" colspan="6" class="pd">
								<table class="">
									<thead>
										<tr>
											<th class="ta-c">파우치</th>
											<th class="ta-c">탕약박스</th>
											<th class="ta-c">배송박스</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td id="package1">
																							
											</td>
											<td id="package2">
																							
											</td>
											<td id="package3">
																							
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<div class="btn-bot">
			<a href="javascript:" id="btnCopy" class="btn-pk n blue mr10"><span>새 처방하기</span></a>
			
			<c:if test="${RECIPE.RECIPE_TP ne '004'}">
				<a href="javascript:" id="btnSave" class="btn-pk n blue mr10"><span>나의처방에 저장</span></a>
			</c:if>
			<a href="javascript:" class="btn-pk n white b-close"><span>닫기</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>