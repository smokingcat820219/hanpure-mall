<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>


<script>	
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
	});
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">주문내역 확인</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col width="200px" />
					<col width="150px" />
					<col />
				</colgroup>
				<tbody>
					<tr>
						<th class='ta-c'>처방구분</th>
						<td colspan="2">약속처방</td>
					</tr>
						
					<c:forEach var="i" begin="1" end="${LIST.size()}">
						<tr>
							<td rowspan="3">
								<div>
									<c:if test="${LIST.get(i - 1).FILEPATH1 ne ''}">
										<img src="/download?filepath=${LIST.get(i - 1).FILEPATH1}" alt="사진" style="width:200px; height:200px" />
									</c:if>
								</div>
							</td>
							<th class='ta-c'>상품명</th>
							<td>${LIST.get(i - 1).OPTION_NM}</td>
						</tr>
						<tr>
							<th class='ta-c'>배송 요청 수량</th>
							<td><mask:display type="NUMBER" value="${LIST.get(i - 1).ORDER_QNTT}"/></td>
						</tr>
						<tr>
							<th class='ta-c'>가격</th>
							<td><mask:display type="NUMBER" value="${LIST.get(i - 1).AMOUNT}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="btn-bot">
			<a href="javascript:;" class="btn-pk n white b-close"><span>닫기</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>