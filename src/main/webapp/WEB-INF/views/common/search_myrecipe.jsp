<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>

<script>
	var json = null;
	
	$(function() {		
		$('#RECIPE_NM').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    setHash(1);
	});
	
	function setHash(page) {		
		if(ajaxRunning) return;
		
		var RECIPE_NM = GetValue("RECIPE_NM");
		
		var formData = new FormData();
		formData.append("page", page);
		formData.append("itemPerPage", "5");
		formData.append("RECIPE_NM", RECIPE_NM);		
		formData.append("RECIPE_TP", "${RECIPE_TP}");
		
		var url = "/myrecipe/selectPagePrivate";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	json = response.list;
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr onClick=\"goSelected(" + i + ")\">";	            			
	            			sHTML += "	<td>" + response.list[i].RECIPE_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].DRUG_NM + "</td>";	  
	            			sHTML += "	<td>" + NumberFormat(response.list[i].DRUG_NM.split(',').length) + "</td>";
	            			sHTML += "</tr>";	            			
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='3'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goSelected(index) {		
		console.log("goSelected", JsonToString(json[index]));
		
		var recipe_seq = json[index].RECIPE_SEQ;
		
		if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("RECIPE_SEQ", recipe_seq);
		
	    var url = "/myrecipe/selectDetail";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	
					parent.deleteDrugAll();
	            	
	            	var recipe_nm = json[index].RECIPE_NM;
	        		var info1 = json[index].INFO1;
	        		var info2 = json[index].INFO2;
	        		var info3 = json[index].INFO3;
	        		var info4 = json[index].INFO4;
	        		var info5 = json[index].INFO5;
	        		var info6 = json[index].INFO6;
	        		var info7 = json[index].INFO7;
	        		var info8 = json[index].INFO8;
	        		var info9 = json[index].INFO9;
	        		var info10 = json[index].INFO10;	        		
	        		var info10_nm = json[index].INFO10_NM;
	        		var info10_qntt = json[index].INFO10_QNTT;
	        		var info10_price = json[index].INFO10_PRICE;
	        		
	        		var info11 = json[index].INFO11;
	        		var vaccum = json[index].VACCUM;
	        		if(!vaccum) {
	        			vaccum = "N";
	        		}
	        		
	        		var marking_tp = json[index].MARKING_TP;
	        		var marking_text = json[index].MARKING_TEXT;
	            	
	        		var package1 = json[index].PACKAGE1;
	        		var package2 = json[index].PACKAGE2;
	        		var package3 = json[index].PACKAGE3;	
	        		var package4 = json[index].PACKAGE4;
	        		var package1_nm = json[index].PACKAGE1_NM;
	        		var package2_nm = json[index].PACKAGE2_NM;
	        		var package3_nm = json[index].PACKAGE3_NM;
	        		var package4_nm = json[index].PACKAGE4_NM;
	        		var package1_tp = json[index].PACKAGE1_TP;
	        		var package2_tp = json[index].PACKAGE2_TP;
	        		var package3_tp = json[index].PACKAGE3_TP;
	        		var package4_tp = json[index].PACKAGE4_TP;	    
	        			        			        		
	        		parent.setRecipeInfo(recipe_nm, info1, info2, info3, info4, info5, info6, info7, info8, info9, info10, info10_nm, info10_qntt, info10_price, info11, marking_tp, marking_text, package1, package2, package3, package4, package1_nm, package2_nm, package3_nm, package4_nm, package1_tp, package2_tp, package3_tp, package4_tp, vaccum);	        		
	        		console.log("prerecipe", recipe_nm, info1, info2, info3, info4, info5, info6, info7, info8, info9, info10, info10_nm, info10_qntt, info10_price, info11, marking_tp, marking_text, package1, package2, package3, package4, package1_nm, package2_nm, package3_nm, package4_nm, package1_tp, package2_tp, package3_tp, package4_tp, vaccum);
	        		
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  
	            			var herbs_cd = response.list[i].HERBS_CD;
	            			var drug_cd = response.list[i].DRUG_CD;
	            			var drug_nm = response.list[i].DRUG_NM;
	            			var stock_qntt = response.list[i].STOCK_QNTT;
	            			var chup1 = response.list[i].CHUP1;
	            			var turn = response.list[i].TURN;
	            			var origin = response.list[i].ORIGIN;
	            			var price_a = response.list[i].PRICE;
	            			var price_b = response.list[i].PRICE;
	            			var price_c = response.list[i].PRICE;
	            			var price_d = response.list[i].PRICE;
	            			var price_e = response.list[i].PRICE;		
	            			var absorption_rate = response.list[i].ABSORPTION_RATE;
	            			
	            			parent.setDrug(herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price_a, price_b, price_c, price_d, price_e, absorption_rate);
	            			console.log("setDrug", herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price_a, price_b, price_c, price_d, price_e, absorption_rate);
	            		}
	            	}
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div class="tbl_sch">
	<div class="h">개인방제</div>
	<div class="t">
		<button type="button" id="btnSearch" class="btn"><span>검색</span></button>
		<input type="text" name="RECIPE_NM" id="RECIPE_NM" class="inp_txt w100p" placeholder="처방명을 입력하여 검색 하세요." />
	</div>
</div>

<div class="tbl_basic">
	<table class="list cur">
		<colgroup>
			<col width="150px" />
			<col />
			<col class="size2" />
		</colgroup>
		<thead>
			<tr>
				<th>처방명</th>
				<th>약재정보</th>
				<th>약미</th>
			</tr>
		</thead>
		<tbody id="tbody">
			
		</tbody>
	</table>
</div>
<div class="pagenation">
	<ul>
		
	</ul>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>