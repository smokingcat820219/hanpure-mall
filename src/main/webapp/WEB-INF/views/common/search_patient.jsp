<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script>
	var json = null;
	
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	});
	
	function setHash(page) {	
		var searchText = GetValue("searchText");
		
		var param = "page=" + page;
		param += "&searchText=" + searchText;
		param += "&itemPerPage=5";
		
		window.location.hash = param;
		
		parent.setSearchPatientText(searchText);
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);		
		   
		   formData.append(key, value);
		}
		
	    var url = "/patient/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	json = response.list;
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr onClick=\"goSelected(" + i + ")\">";	            			
	            			sHTML += "	<td>" + response.list[i].CHART_NO + "</td>";
	            			sHTML += "	<td>" + response.list[i].NAME + "</td>";	  
	            			
							sHTML += "	<td>";
	            			
	            			if(response.list[i].SEX_NM) {
	            				sHTML += response.list[i].SEX_NM
	            			}
	            			
	            			if(response.list[i].BIRTH) {
	            				if(response.list[i].SEX_NM) {
	            					sHTML += "/";
	            				}
	            				
	            				sHTML += calcAge(response.list[i].BIRTH);	            				
	            			}
	            			
	            			sHTML += "	</td>";	            			
	            			
	            			sHTML += "	<td>" + response.list[i].RECIPE_DT + "</td>";
	            			sHTML += "</tr>";	
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='4'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goSelected(index) {		
		console.log("json[index] : " + JsonToString(json[index]));
		
		parent.setPatient(json[index].PATIENT_SEQ, json[index].NAME, json[index].CHART_NO, json[index].SEX_NM, json[index].BIRTH, json[index].RECIPE_DT, json[index].MOBILE, json[index].ZIPCODE, json[index].ADDRESS, json[index].ADDRESS2, json[index].REMARK);		
	}
</script>

<body>

<div class="tbl_sch">
	<div class="t">
		<button type="button" id="btnSearch" class="btn"><span>검색</span></button>
		<input type="text" name="searchText" id="searchText" class="inp_txt w100p" value="${searchText}" placeholder="환자명, 차트번호를 입력하여 검색하세요" />
	</div>
</div>
<div class="tbl_basic">
	<table class="list cur">
		<colgroup>
			<col />
			<col width="100px" />
			<col width="100px" />
			<col width="120px" />
		</colgroup>
		<thead>
			<tr>
				<th>차트번호</th>
				<th>환자명</th>
				<th>성별/나이</th>
				<th>최근처방일</th>
			</tr>
		</thead>
		<tbody id="tbody">
			
		</tbody>
	</table>
</div>
<div class="pagenation">
	<ul>
		
	</ul>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>