<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>

<script>
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
	    $('#btnSave').click(function() {
	    	goSave();
	    });	
	});
	
	function goSave() {
		if(ajaxRunning) return;
		
		var title = GetValue("title");
		var content = $.trim(sn$('#content').summernote('code'));
				
		if(!title) {
	    	alert("제목을 입력해 주세요.");
	    	$("#title").focus();
	    	return;
	    }
	   	    
	    var formData = new FormData();
	    formData.append("TYPE", "${TYPE}");
	    formData.append("TITLE", title);
	    formData.append("CONTENT", content);
	    
	    var length = $(".filebox_txt").length;
	    for(var i = 0; i < length; i++) {
	    	var filepath = $(".filebox_txt").eq(i).data("path");
	    	var filename = $(".filebox_txt").eq(i).data("name");
	    	
	    	formData.append("LIST_FILEPATH", filepath);
	    	formData.append("LIST_FILENAME", filename);
	    }
		
	    if(!confirm("입력하신 내용을 등록 하시겠습니까?")) {
	    	return;
	    }
	    
	    var url = "/howto/write";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	 
	            	alert("입력하신 내용을 등록 완료 되었습니다.");
	            	
	            	try {
	            		parent.goPage();	
	            	}catch(e) {
	            		
	            	}	            	
	            	
	            	$(".b-close").trigger("click");
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function uploadFile() {
		var filename = $("#upfile").val();
		if(filename) {
			var formData = new FormData();	    
			formData.append("upfile", document.getElementById('upfile').files[0]);
			var url = "/uploadFile";
		    $.ajax({
		        type:"POST",
		        url:url,
		        data:formData,
		        cache: false,
		        processData: false,  // file전송시 필수
		        contentType: false,  // file전송시 필수
		        beforeSend: function() {
		        	ShowCSS($(".loadingbar"));
		        },
		        success:function(response) {
		        	HideCSS($(".loadingbar"));
		        	
		            if(response.result == 200) {			            	
		            	$("#filename").val(response.filename);
		            	$("#filepath").val(response.filepath);
		            	
		            	var sHTML = "";
		            	
		            	sHTML += "<div class='filebox_txt' data-name='" + response.filename + "' data-path='" + response.filepath + "' style='display: inline-block;'>";
		            	sHTML += "	<em class='upload-name2'>" + response.filename + "</em>";
		            	sHTML += "	<button type='button' class='btn_file_del' onclick='deleteFile(this)'><span>삭제</span></button>";
		            	sHTML += "</div>";
		            	
		            	$(".filebox_cont").append(sHTML);
		            }
		            else {
		            	alert(response.message);
		            }
		            
		            $("#upfile").val("");
		        },
		        error:function(request, status, error) {
		        	HideCSS($(".loadingbar"));
		        	
		            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    });
		}
	}
	
	function deleteFile(obj) {		
		$(obj).parent().remove();
	}
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">
			<c:if test="${TYPE eq 'M'}">조제지시 등록</c:if>
			<c:if test="${TYPE eq 'E'}">복용법 등록</c:if>
		</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col class="th2">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>제목</th>
						<td><input type="text" name="title" id="title" class="inp_txt w100p" value="" placeholder="제목을 입력하세요(100자 이내)"></td>
					</tr>
					<tr>
						<th>내용</th>
						<td><textarea rows="20" name="content" id="content" class="textarea1 w100p"></textarea></td>
					</tr>
					
					<c:if test="${TYPE eq 'E'}">
						<tr>
							<th>첨부파일</th>
							<td>
								<div class="filebox">
									<label for="upfile">첨부파일</label>
									<input type="file" name="upfile" id="upfile" class="upload-hidden" onChange="uploadFile()" />
								</div>
								
								<div class="filebox_cont">
									
								</div>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>

		<div class="btn-bot">
			<a href="javascript:" id="btnSave" class="btn-pk n blue" onclick="openLayerPopup('popAlert2', 'block');"><span>등록</span></a>
			<a href="javascript:" class="btn-pk n white b-close" onclick="openLayerPopup('popAlert4', 'block');"><span>취소</span></a>
		</div>
	</div>
</div>

<script>
$(function(){
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
})
</script>

<%@ include file="/WEB-INF/views/include/editor.jsp" %>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>