<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script>
	var json = null;
	
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {	
		if("${TYPE}" == "M") {
			$(".eat").hide();	
		}
		
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var param = "page=" + page;
		param += "&TYPE=" + "${TYPE}";
		param += "&itemPerPage=20";
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);		
		   
		   formData.append(key, value);
		}
		
	    var url = "/howto/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	json = response.list;
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr onClick=\"goSelected(" + i + ")\">";	   
	            			sHTML += "	<td>" + (i + 1) + "</td>";
	            			sHTML += "	<td class='ta-l'>" + response.list[i].TITLE + "</td>";
	            			sHTML += "	<td class='eat'>" + response.list[i].FILE_QNTT + "</td>";	  
	            			sHTML += "	<td>" + response.list[i].CREATED_AT + "</td>";
	            			sHTML += "</tr>";	            			
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='4'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
					
					if("${TYPE}" == "M") {
						$(".eat").hide();	
					}
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goSelected(index) {		
		console.log("json[index] : " + JsonToString(json[index]));
				
		var formData = new FormData();
		formData.append("HOWTO_SEQ", json[index].HOWTO_SEQ);
		
	    var url = "/howto/selectFiles";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	
	            	var content = json[index].CONTENT;
	            	if(content) {
	            		parent.setHowTo("${TYPE}", content);	
	            	}
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			var filename = response.list[i].FILENAME;
	            			var filepath = response.list[i].FILEPATH;
	            			
	            			if("${TYPE}" == "E") {
	            				parent.setHowToFile("${TYPE}", filename, filepath);	
	            			}
	            		}
	            	}	 
	            	
	            	$(".b-close").trigger("click");
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">
			<c:if test="${TYPE eq 'M'}">조제지시 등록</c:if>
			<c:if test="${TYPE eq 'E'}">복용법 등록</c:if>
		</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="list cur">
				<colgroup>
					<col class="num">
					<col>
					<col class="num eat">
					<col class="day">
				</colgroup>
				<thead>
					<tr>
						<th>No.</th>
						<th>제목</th>
						<th class="eat">첨부파일</th>
						<th>등록일</th>
					</tr>
				</thead>
				<tbody id="tbody">
				
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
$(function(){
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
})
</script>

<%@ include file="/WEB-INF/views/include/editor.jsp" %>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>