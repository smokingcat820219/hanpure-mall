<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>


<script>	
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
	});
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">받는 사람</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="view">
				<colgroup>
					<col class="th2">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>받는 사람</th>
						<td>${ORDER.RECV_NM}</td>
					</tr>
					<tr>
						<th>연락처</th>
						<td>${ORDER.RECV_TEL}</td>
					</tr>
					<tr>
						<th>휴대폰</th>
						<td>${ORDER.RECV_MOBILE}</td>
					</tr>
					<tr>
						<th>우편번호</th>
						<td>${ORDER.RECV_ZIPCODE}</td>
					</tr>
					<tr>
						<th>주소</th>
						<td>${ORDER.RECV_ADDRESS}</td>
					</tr>
					<tr>
						<th>상세주소</th>
						<td>${ORDER.RECV_ADDRESS2}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="btn-bot">
			<a href="javascript:;" class="btn-pk n white b-close"><span>닫기</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>