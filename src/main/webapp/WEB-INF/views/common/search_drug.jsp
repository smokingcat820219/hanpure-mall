<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>

<script>
	var json = null;
	
	$(function() {		
		$('#DRUG_NM').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    setHash(1);
	});
	
	function setHash(page) {		
		if(ajaxRunning) return;
		
		var DRUG_NM = GetValue("DRUG_NM");
		var DRUG_TP = GetValue("DRUG_TP");
		
		var formData = new FormData();
		formData.append("page", page);
		formData.append("DRUG_NM", DRUG_NM);
		formData.append("DRUG_TP", DRUG_TP);
		formData.append("itemPerPage", "5");
		formData.append("INSURANCE_YN", "Y");
		
	    var url = "/stock_drug/selectPageStock";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	json = response.list;
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr onClick=\"goSelected(" + i + ")\">";
	            			
	            			sHTML += "	<td>" + response.list[i].DRUG_NM + "</td>";
	            			sHTML += "	<td>" + response.list[i].ORIGIN + "</td>";	  
	            			sHTML += "	<td>" + response.list[i].MAKER + "</td>";
	            			
	            			if("${SESSION.CLASS}" == "B") {
	            				sHTML += "	<td>" + response.list[i].PRICE_B + "</td>";
	            			}
	            			else if("${SESSION.CLASS}" == "C") {
	            				sHTML += "	<td>" + response.list[i].PRICE_C + "</td>";
	            			}
	            			else if("${SESSION.CLASS}" == "D") {
	            				sHTML += "	<td>" + response.list[i].PRICE_D + "</td>";
	            			}
	            			else if("${SESSION.CLASS}" == "E") {
	            				sHTML += "	<td>" + response.list[i].PRICE_E + "</td>";
	            			}
	            			else {
	            				sHTML += "	<td>" + response.list[i].PRICE_A + "</td>";	
	            			}
	            			
	            			sHTML += "</tr>";	            			
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='4'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goSelected(index) {		
		console.log("json[index] : " + JsonToString(json[index]));
				
		var herbs_cd = json[index].HERBS_CD;
		var drug_cd = json[index].DRUG_CD;
		var drug_nm = json[index].DRUG_NM;
		var stock_qntt = json[index].STOCK_QNTT;
		var chup1 = "0";
		var turn = "2";
		var origin = json[index].ORIGIN;
		var price_a = json[index].PRICE_A;
		var price_b = json[index].PRICE_B;
		var price_c = json[index].PRICE_C;
		var price_d = json[index].PRICE_D;
		var price_e = json[index].PRICE_E;		
		var absorption_rate = json[index].ABSORPTION_RATE;
		
		parent.setDrug(herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price_a, price_b, price_c, price_d, price_e, absorption_rate);
	}
	
	function setDrugTp(drug_tp) {
		$("#tab1").removeClass("on");
		$("#tab2").removeClass("on");
		$("#tab3").removeClass("on");
		
		if(drug_tp == "001") {
			$("#tab1").addClass("on");
		}
		else if(drug_tp == "002") {
			$("#tab2").addClass("on");
		}
		else if(drug_tp == "003") {
			$("#tab3").addClass("on");
		}
		
		$("#DRUG_TP").val(drug_tp);
		
		setHash(1);
	}
</script>

<body>

<div class="tbl_sch">
	<div class="h">약재검색</div>
	<div class="t">
		<button type="button" id="btnSearch" class="btn"><span>검색</span></button>
		<input type="text" name="DRUG_NM" id="DRUG_NM" class="inp_txt w100p" placeholder="약재명을 입력하여 검색 하세요." />
		<input type="hidden" name="DRUG_TP" id="DRUG_TP" value="001" />	
	</div>
</div>

<div class="tab ty1 tab_c2_1">
	<ul class="small">
		<li id="tab1" class="on"><a href="javascript:" onClick="setDrugTp('001')"><span>약재</span></a></li>
		<li id="tab2"><a href="javascript:" onClick="setDrugTp('002')"><span>별전</span></a></li>
		<li id="tab3"><a href="javascript:" onClick="setDrugTp('003')"><span>감미제</span></a></li>
	</ul>
</div>
<div class="tbl_basic">
	<table class="list cur">
		<colgroup>
			<col>
			<col class="size1">
			<col>
			<col class="size1">
		</colgroup>
		<thead>
			<tr>
				<th>약재명</th>
				<th>원산지</th>
				<th>제조사</th>
				<th>가격</th>
			</tr>
		</thead>
		<tbody id="tbody">
			
		</tbody>
	</table>
</div>
<div class="pagenation">
	<ul>
		
	</ul>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>