<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script>
	var json = null;
	
	$(function() {		
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
		if(!"${PKG}") {
			alert("잘못된 접근입니다.");
			
			$(".b-close").trigger("click");
		}
		
		if(!"${BOM_TP}") {
			alert("잘못된 접근입니다.");
			
			$(".b-close").trigger("click");
		}		
		
		setHash(1);
	    
	    $('#btnOK').click(function() {
	    	var selected = GetRadioValue("radio");
	    	
	    	if(selected) {	    		
	    		var vaccum = "N";
	    		if($("#vaccum").prop("checked")) {
	    			vaccum = "Y";
	    		}
	    		
	    		var bom_cd = json[selected].BOM_CD;	    		
	    		var bom_nm = json[selected].BOM_NM;
	    		var bom_tp = json[selected].BOM_TP;
	    		
	    		
	    		parent.setPackage("${PKG}", bom_cd, bom_nm, bom_tp, vaccum);	    		
	    		console.log("pop_package", "${PKG}", bom_cd, bom_nm, bom_tp, vaccum);
	    		
	    		$(".b-close").trigger("click");
	    		
	    		/*
	    		var qntt = json[selected].QNTT;
	    		var price = json[selected].PRICE;
	    		var make_price = json[selected].MAKE_PRICE;
	    		var filepath = json[selected].FILEPATH;
	    		
	    		parent.setPackage("${PKG}", item_cd, item_nm, qntt, price, make_price, filepath, vaccum);
	    		
	    		console.log("pop_package", "${PKG}", item_cd, item_nm, qntt, price, make_price, filepath, vaccum);
	    		
	    		$(".b-close").trigger("click");
	    		*/
	    	}
	    	else {
	    		alert("하나를 선택해 주세요.");
	    	}
	    });
	    
	    $('#btnCancel').click(function() {
	    	parent.setPackage("${PKG}", "", "", "");	     		
    		
    		$(".b-close").trigger("click");
	    });
	});
	
	function setHash(page) {		
		if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("page", page);
		formData.append("LIST_BOM_TP", "${BOM_TP}");
		
	    var url = "/bom/selectDatas";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {               	
	            	
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	json = response.list;
	            	
	            	$("#tbody").html("");	            	
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	
	            			var sHTML = "";
	            			
	            			sHTML += "<li>";
	            			
	            			if(response.list[i].BOM_CD == "${BOM_CD}") {
	            				sHTML += "	<label class='inp_radio mb10' style='z-index:999'><input type='radio' name='radio' value='" + i + "' checked /><span></span></label>";	
	            			}
	            			else {
	            				sHTML += "	<label class='inp_radio mb10' style='z-index:999'><input type='radio' name='radio' value='" + i + "' /><span></span></label>";	
	            			}
	            			
	            			sHTML += "	<div class='owl-carousel' id='" + response.list[i].BOM_CD + "'>";
	            			//sHTML += "		<div class='item'>";
	            			//sHTML += "			<div class='img_prd long'>";
							//sHTML += "				<img src='/download?filepath=' alt=''>";
							//sHTML += "			</div>";
							//sHTML += "		</div>";
							sHTML += "	</div>";
							
							sHTML += "	<p>" + response.list[i].BOM_NM + "</p>";
							
	            			sHTML += "</li>";
	    				
		            		startno--;
		            		
		            		$("#tbody").append(sHTML);		            		
		            		
		            		getItemInfo(response.list[i].BOM_CD);
	            		}
	            	}
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function getItemInfo(bom_cd) {		
		console.log("getItemInfo : " + bom_cd);
		//if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("BOM_CD", bom_cd);
		
	    var url = "/bom/selectItems";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {     
	            	var sHTML = "";
	            	
	            	console.log(response.list);
	            	
	            	for(var i = 0; i < response.list.length; i++) {
	            		sHTML += "		<div class='item'>";
	        			sHTML += "			<div class='img_prd long'>";
	        			
	        			var filepath = response.list[i].FILEPATH;
	        			var item_nm = response.list[i].ITEM_NM;
	        			
	        			if(filepath) {
	        				sHTML += "				<img src='/download?filepath=" + filepath + "' title='" + item_nm + "' />";
	        			}
	        			else {
	        				sHTML += "				<img src='/images/common/img_noimg.jpg' title='" + item_nm + "' />";
	        			}
						
						sHTML += "			</div>";
						sHTML += "		</div>";				
	            	}
	            	
	            	$("#" + bom_cd).html(sHTML);
	            	
	            	//var slider = $(".owl-carousel");
	            	var slider = $("#" + bom_cd);
            		slider.owlCarousel({
            			loop: $("#" + bom_cd + " .item").length > 1 ? true: false,
            			margin:0,
            			nav:false,
            			dots: $("#" + bom_cd + " .item").length > 1 ? true: false,
            			items:1,
            			smartSpeed:1500,
            			autoplay:true,
            			autoplayTimeout:5000,
            			autoplayHoverPause:false,
            			mouseDrag : $("#" + bom_cd + " .item").length > 1 ? true: false,
            		});

            		slider.trigger('refresh.owl.carousel');
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">
			<c:if test="${BOM_TP eq '001'}">파우치 선택</c:if>
			<c:if test="${BOM_TP eq '002'}">탕약박스 선택</c:if>
			<c:if test="${BOM_TP eq '003'}">배송박스 선택</c:if>
			<c:if test="${BOM_TP eq '004'}">스틱 선택</c:if>
			<c:if test="${BOM_TP eq '005'}">케이스 선택</c:if>
			<c:if test="${BOM_TP eq '006, 007, 008'}">내지 선택</c:if>
			<c:if test="${BOM_TP eq '009, 010, 012'}">소포장 선택</c:if>
			<c:if test="${BOM_TP eq '011, 012'}">소포장 선택</c:if>			
		</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<p class="fz1 c-blue mb10">
			<c:if test="${BOM_TP eq '001'}">* 파우치 선택 후, 확인 버튼을 눌러주세요.</c:if>
			<c:if test="${BOM_TP eq '002'}">* 탕약박스를 선택 해 주세요.<br/>* 진공포장을 원하시는 경우, 진공포장하기에 체크 해 주세요.</c:if>
			<c:if test="${BOM_TP eq '003'}">* 배송 박스 선택 후, 확인 버튼을 눌러주세요.</c:if>
			<c:if test="${BOM_TP eq '004'}">* 스틱 선택 후, 확인 버튼을 눌러주세요.</c:if>
			<c:if test="${BOM_TP eq '005'}">* 케이스 선택 후, 확인 버튼을 눌러주세요.</c:if>
			
			<c:if test="${BOM_TP eq '006, 007, 008'}">* 내지 선택 후, 확인 버튼을 눌러주세요.</c:if>
			<c:if test="${BOM_TP eq '009, 010, 012'}">* 소포장 선택 후, 확인 버튼을 눌러주세요.</c:if>
			<c:if test="${BOM_TP eq '011, 012'}">* 소포장 선택 후, 확인 버튼을 눌러주세요.</c:if>			
		</p>
		<div class="mb10" <c:if test="${BOM_TP ne '002'}">style="display:none"</c:if>>
			<label class="inp_checkbox"><input type="checkbox" name="vaccum" id="vaccum" <c:if test="${VACCUM eq 'Y'}">checked</c:if> /><span>진공포장 하기</span></label>
		</div>
		<div class="lst_box">
			<ul id="tbody">
				
			</ul>
		</div>

		<div class="btn-bot">
			<a href="javascript:" id="btnOK" class="btn-pk n blue"><span>확인</span></a>
			<a href="javascript:" id="btnCancel" class="btn-pk n white"><span>선택 취소</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>