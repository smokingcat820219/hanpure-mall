<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script>	
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
		var slider1 = $(".owl-carousel");
		slider1.owlCarousel({
			loop: $(".owl-carousel .item").length > 1 ? true: false,
			margin:0,
			nav:false,
			dots: $(".owl-carousel .item").length > 1 ? true: false,
			items:1,
			smartSpeed:1500,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:false,
			mouseDrag : $(".owl-carousel .item").length > 1 ? true: false,
		});

		slider1.trigger('refresh.owl.carousel');
	});
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">주문내역 확인</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col width="200px" />
					<col width="150px" />
					<col />
				</colgroup>
				<tbody>
					<tr>
						<th class="ta-c">처방구분</th>
						<td colspan="2">상용처방</td>
					</tr>	
					<tr>
						<th class="ta-c">상품명</th>
						<td colspan="2">${ORDER.ORDER_NM}</td>
					</tr>					
					<tr>
						<td rowspan="3">
							<div class="owl-carousel">
								<c:if test="${PRODUCT.FILEPATH1 ne null and PRODUCT.FILEPATH1 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH1}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH2 ne null and PRODUCT.FILEPATH2 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH2}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH3 ne null and PRODUCT.FILEPATH3 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH3}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH4 ne null and PRODUCT.FILEPATH4 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH4}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH5 ne null and PRODUCT.FILEPATH5 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH5}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH6 ne null and PRODUCT.FILEPATH6 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH6}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH7 ne null and PRODUCT.FILEPATH7 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH7}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH8 ne null and PRODUCT.FILEPATH8 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH8}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH9 ne null and PRODUCT.FILEPATH9 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH9}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>
								<c:if test="${PRODUCT.FILEPATH10 ne null and PRODUCT.FILEPATH10 ne ''}">
									<div class="item">
										<div class="img_prd long">
											<img src="/download?filepath=${PRODUCT.FILEPATH10}" style="width:200px; height:200px" />
										</div>
									</div>
								</c:if>					
							</div>
						</td>
						<th class="ta-c">금액</th>
						<td><mask:display type="NUMBER" value="${ORDER.ORDER_AMOUNT / ORDER.ORDER_QNTT}"/> 원</td>
					</tr>
					<tr>
						<th class="ta-c">배송 요청 수량</th>
						<td><mask:display type="NUMBER" value="${ORDER.ORDER_QNTT}"/></td>
					</tr>
					<tr>
						<th class="ta-c">합계</th>
						<td><mask:display type="NUMBER" value="${ORDER.ORDER_AMOUNT}"/> 원</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="btn-bot">
			<a href="javascript:;" class="btn-pk n white b-close"><span>닫기</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>