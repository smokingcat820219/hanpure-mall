<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>


<script>	
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
		 $('#btnSave').click(function() {
	    	goSave();
	    });
	});
	
	function goSave() {
		if(ajaxRunning) return;
		
		var period = GetValue("period");
		var promise_seq = "${PROMISE.PROMISE_SEQ}";
		var product_seq = "${PROMISE.PRODUCT_SEQ}";
		
		if("${TYPE}" == "1") {
			if(!confirm("사전처방을 신규등록 하시겠습니까?")) {
				return;
			}	
		}
		else if("${TYPE}" == "2") {
			if(!confirm("사전처방을 연장 하시겠습니까?")) {
				return;
			}	
		}
		else if("${TYPE}" == "3") {
			if(!confirm("사전처방을 추가 하시겠습니까?")) {
				return;
			}	
		}
		else {
			$(".b-close").trigger("click");	         
		}
		
		
	    var formData = new FormData();	
	    formData.append("PROMISE_SEQ", promise_seq);
	    formData.append("PRODUCT_SEQ", product_seq);
	    formData.append("PERIOD", period);
   		
	    var url = "/promise/add";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	if("${TYPE}" == "1") {
	            		alert("사전처방 신규등록이 완료 되었습니다.");
	        		}
	        		else if("${TYPE}" == "2") {
	        			alert("사전처방 연장이 완료 되었습니다.");	
	        		}
	        		else if("${TYPE}" == "3") {
	        			alert("사전처방 추가가 완료 되었습니다.");	
	        		}
	            	
	            	try {
	            		parent.goPage();	
	            	}catch(e) {
	            		
	            	}	            	
	            	
	            	$(".b-close").trigger("click");	            	
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}	
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<c:if test="${TYPE eq '1'}"><h2 class="tit">사전처방 신규등록</h2></c:if>
		<c:if test="${TYPE eq '2'}"><h2 class="tit">사전처방 연장</h2></c:if>
		<c:if test="${TYPE eq '3'}"><h2 class="tit">사전처방 추가</h2></c:if>
		
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<h3 class="h_tit3">사전 처방 현황</h3>
		<div class="tbl_basic pr-mb1">
			<table class="view">
				<colgroup>
					<col class="th1">
					<col>
					<col class="th1">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>상품명</th>
						<td colspan="3">${PROMISE.PRODUCT_NM}</td>
					</tr>
					<tr>
						<th>약재구성</th>
						<td colspan="3">${PROMISE.DRUG_NM}</td>
					</tr>
					<tr>
						<th>발송 가능량</th>
						<td><mask:display type="NUMBER" value="${PROMISE.STOCK_QNTT}" /></td>
						<th>만료일자</th>
						<td>
							${PROMISE.END_DT}							
							<c:if test="${PROMISE.STATUS eq 'F'}">
								<span class="btn-pk s red2 ml20"><span>마감임박</span></span>
							</c:if>
							<c:if test="${PROMISE.STATUS eq 'E'}">
								<span class="btn-pk s gray ml20"><span>종료</span></span>
							</c:if>
							<c:if test="${PROMISE.STATUS eq 'I'}">
								<span class="btn-pk s blue2 ml20"><span>진행중</span></span>
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<h3 class="h_tit3">사전처방 등록</h3>
		<div class="tbl_basic">
			<table class="view">
				<colgroup>
					<col class="th1">
					<col>
					<col class="th1">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>상품명</th>
						<td colspan="3">${PROMISE.PRODUCT_NM}</td>						
					</tr>
					<tr>
						<th>조제의뢰처</th>
						<td>㈜한퓨어</td>
						<th>한의원</th>
						<td>${SESSION.COMPANY}</td>
					</tr>
					<tr>
						<th>처방일자</th>
						<td>${TODAY}</td>
						<th>조제기간</th>
						<td>${PROMISE.PERIOD_MAKE}일/조제완료(${END_DT})</td>
					</tr>
					<tr>
						<th>사전처방량</th>
						<td>
							<mask:display type="NUMBER" value="${PROMISE.MONTHLY_QNTT}" />
						</td>
						<th>조제기간</th>
						<td>
							<select name="period" id="period" class="select1 w100p">								
								<c:forEach items="${fn:split(PROMISE.PERIOD_ORDER, ',')}" var="item">
								    <option value="${item}">${item}개월</option>
								</c:forEach>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="btn-bot">
			<a href="javascript:" class="btn-pk n white b-close"><span>닫기</span></a>
			
			<c:if test="${TYPE eq '1'}">
				<a href="javascript:" id="btnSave" class="btn-pk n blue"><span>사전처방 신규등록</span></a>
			</c:if>
			<c:if test="${TYPE eq '2'}">
				<a href="javascript:" id="btnSave" class="btn-pk n blue"><span>사전처방 연장</span></a>
			</c:if>
			<c:if test="${TYPE eq '3'}">
				<a href="javascript:" id="btnSave" class="btn-pk n blue"><span>사전처방 추가</span></a>
			</c:if>	
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>