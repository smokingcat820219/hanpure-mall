<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script>
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
		
	    $('#btnSave').click(function() {
	    	goSave();
	    });	
	    
	    setCode("${CODE}");
	});
	
	function goSave() {		
		var code = GetValue("code");
		var price = $('#code option:selected').attr('price');
		var name  = $('#code option:selected').text();
		var value = GetValue("value");
		
		console.log("code : " + code);
		console.log("name : " + name);
		console.log("value : " + value);
		console.log("price : " + price);		
		
		parent.setDrink(code, name, value, price);
		
		$(".b-close").trigger("click");
	}
	
	function setCode(code) {
		if(code != "") {
			var sHTML = "";
			
			for(var i = 10; i <= 4000; i += 10) {
				if(i == "${VALUE}") {
					sHTML += "<option value='" + i + "' selected>" + NumberFormat(i) + "mL</option>";
				}
				else {
					sHTML += "<option value='" + i + "'>" + NumberFormat(i) + "mL</option>";	
				}
			}
			
			$("#value").html(sHTML);
		}
		else {
			var sHTML = "";
			sHTML += "<option value=''>선택없음</option>";			
			$("#value").html(sHTML);
		}
	}
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">주수상반 선택</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col class="th2">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<th>주수상반 선택</th>
						<td>
							<select name="code" id="code" class="select1" onChange=setCode(this.value)>
								<option value="" price=''>선택없음</option>
								
								<c:forEach var="i" begin="1" end="${LIST_DRINK_TP.size()}">
									<option value="${LIST_DRINK_TP.get(i - 1).SUB_CD}" price='${LIST_DRINK_TP.get(i - 1).SUB_VALUE_KR}' <c:if test="${CODE eq LIST_DRINK_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_DRINK_TP.get(i - 1).SUB_NM_KR}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>용량선택</th>
						<td>
							<select name="value" id="value" class="select1">
								<option value="">선택없음</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="btn-bot">
			<a href="javascript:" id="btnSave" class="btn-pk n blue"><span>확인</span></a>
			<a href="javascript:" class="btn-pk n white b-close"><span>취소</span></a>
		</div>
	</div>
</div>





<script>
$(function(){
	$(".b-close").on("click", function(){
		parent.$(".layerPopup").fadeOut(100);
		parent.$(".popup_dim").remove();
	});
})
</script>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>