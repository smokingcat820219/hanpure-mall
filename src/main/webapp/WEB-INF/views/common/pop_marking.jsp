<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script>	
	$(function() {		
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
	    
	    $('#btnOK').click(function() {
	    	var marking_tp = GetRadioValue("marking_tp");
	    	console.log("marking_tp : " + marking_tp);
	    	
	    	if(marking_tp) {
	    		var marking_text = GetValue("marking_text_" + marking_tp);
		    	console.log("marking_text : " + marking_text);
		    	
	    		if(marking_tp == "005") {	    			
	    			if(!marking_text) {
	    				alert("출력될 텍스트를 입력해 주세요.");
	    				$("#marking_text_" + marking_tp).focus();
	    				return;
	    			}
	    		}	
	    		
	    		parent.setMarking(marking_tp, marking_text);	    		
	    		
	    		$(".b-close").trigger("click");
	    	}
	    	else {
	    		alert("하나를 선택해 주세요.");
	    	}
	    });
	});
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">마킹 유형 선택</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<p class="fz1 c-blue mb10">* 마킹 유형을 선택 후, 확인 버튼을 눌러주세요</p>
		<div class="tbl_basic">
			<table class="list">
				<thead>
					<tr>
						<c:forEach var="i" begin="1" end="${LIST_MARKING_TP.size()}">
							<th><label class="inp_radio"><input type="radio" name="marking_tp" value="${LIST_MARKING_TP.get(i - 1).SUB_CD}" data-name="${LIST_MARKING_TP.get(i - 1).SUB_NM_KR}" <c:if test="${MARKING_TP eq LIST_MARKING_TP.get(i - 1).SUB_CD}">checked</c:if> /><span>${LIST_MARKING_TP.get(i - 1).SUB_NM_KR}</span></label></th>
						</c:forEach>
					</tr>
				</thead>
				<tbody>
					<tr>
						<c:forEach var="i" begin="1" end="${LIST_MARKING_TP.size()}">
							<td>
								<c:choose>
									<c:when test="${LIST_MARKING_TP.get(i - 1).SUB_CD eq '001'}">
										<div class="box_td">
											<img src="/images/tmp_img.jpg" alt="사진">
											<p></p>
										</div>	
										<input type="hidden" class="inp_txt w100p" name="marking_text" id="marking_text_${LIST_MARKING_TP.get(i - 1).SUB_CD}" value="${LIST_MARKING_TP.get(i - 1).SUB_VALUE_KR}" />
									</c:when>
									<c:when test="${LIST_MARKING_TP.get(i - 1).SUB_CD eq '005'}">
										<input type="text" class="inp_txt w100p" name="marking_text" id="marking_text_${LIST_MARKING_TP.get(i - 1).SUB_CD}" placeholder="직접입력(14자)" maxlength="14" value='<c:if test="${MARKING_TP eq '005'}">${MARKING_TEXT}</c:if>' />		
									</c:when>									
									<c:otherwise>
										<div class="box_td">
											<img src="/images/tmp_img.jpg" alt="사진">
											<p>${LIST_MARKING_TP.get(i - 1).SUB_VALUE_KR}</p>
										</div>	
										<input type="hidden" class="inp_txt w100p" name="marking_text" id="marking_text_${LIST_MARKING_TP.get(i - 1).SUB_CD}" value="${LIST_MARKING_TP.get(i - 1).SUB_VALUE_KR}" />
									</c:otherwise>
								</c:choose>
							</td>							
						</c:forEach>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="btn-bot">
			<a href="javascript:" id="btnOK" class="btn-pk n blue"><span>확인</span></a>
			<a href="javascript:" class="btn-pk n white b-close"><span>취소</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>