<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>

<script>
	var json = null;
	
	$(function() {		
		$('#PRE_NM').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });	
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    setHash(1);
	});
	
	function setHash(page) {	
		if(ajaxRunning) return;
		
		var PRE_SRC_BOOK = GetValue("PRE_SRC_BOOK");
		var PRE_NM = GetValue("PRE_NM");
		
		var formData = new FormData();
		formData.append("page", page);
		formData.append("itemPerPage", "5");
		formData.append("PRE_SRC_BOOK", PRE_SRC_BOOK);
		formData.append("PRE_NM", PRE_NM);
		
	    var url = "/pdata/selectPage";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	json = response.list;
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  	            			
	            			sHTML += "<tr onClick=\"goSelected(" + i + ")\">";	            			
	            			sHTML += "	<td>" + response.list[i].PRE_SRC_BOOK + "</td>";
	            			sHTML += "	<td>" + response.list[i].PRE_NM + "</td>";	  
	            			sHTML += "	<td>" + NumberFormat(response.list[i].PRE_MED.split(',').length) + "</td>";
	            			sHTML += "</tr>";	            			
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='3'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goSelected(index) {		
		console.log("json[index] : " + JsonToString(json[index]));
		
		var pre_cd = json[index].PRE_CD
		
		
		if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("PRE_CD", pre_cd);
		
	    var url = "/pdata/selectDetail";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	
	            	parent.deleteDrugAll();
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {  
	            			response.list[i].TURN = "2";
	            			
	            			var herbs_cd = response.list[i].HERBS_CD;
	            			var drug_cd = response.list[i].DRUG_CD;	            			
	            			var drug_nm = response.list[i].DRUG_NM;
	            			var stock_qntt = response.list[i].STOCK_QNTT;
	            			var chup1 = "0";
	            			var turn = "2";
	            			var origin = response.list[i].ORIGIN;
	            			var price_a = response.list[i].PRICE_A;
	            			var price_b = response.list[i].PRICE_B;
	            			var price_c = response.list[i].PRICE_C;
	            			var price_d = response.list[i].PRICE_D;
	            			var price_e = response.list[i].PRICE_E;	            			
	            			var absorption_rate = response.list[i].ABSORPTION_RATE;
	            			
	            			parent.setDrug(herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price_a, price_b, price_c, price_d, price_e, absorption_rate);
	            		}
	            	}
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
</script>

<body>

<div class="tbl_sch">
	<div class="h">방제사전</div>
	<div style="margin-right:10px">
		<select class="select1" name="PRE_SRC_BOOK" id="PRE_SRC_BOOK" style="width:150px" onChange="setHash(1)">
			<option value="">출전 선택</option>			
			<c:forEach var="i" begin="1" end="${LIST.size()}">
				<option value="${LIST.get(i - 1).PRE_SRC_BOOK}">${LIST.get(i - 1).PRE_SRC_BOOK}</option>
			</c:forEach>
		</select>		
	</div>
	<div class="t">
		<button type="button" id="btnSearch" class="btn"><span>검색</span></button>			
		<input type="text" name="PRE_NM" id="PRE_NM" class="inp_txt w100p" placeholder="방제명을 입력하여 검색 하세요." />
		<input type="hidden" name="itemPerPage" id="itemPerPage" value="5" />		
	</div>
</div>

<div class="tbl_basic">
	<table class="list cur">
		<colgroup>
			<col>
			<col>
			<col class="name">
		</colgroup>
		<thead>
			<tr>
				<th>출전</th>
				<th>방제명</th>
				<th>약미</th>
			</tr>
		</thead>
		<tbody id="tbody">
			<tr>
				<td>방약합편, 하3</td>
				<td>척담탕</td>
				<td>10</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="pagenation">
	<ul>
		
	</ul>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>