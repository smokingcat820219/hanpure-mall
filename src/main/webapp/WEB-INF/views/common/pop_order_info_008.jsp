<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/pop_top.jsp" %>


<script>	
	$(function() {	
		$(".b-close").on("click", function(){
			parent.$(".layerPopup").fadeOut(100);
			parent.$(".popup_dim").remove();
		});
	});
</script>

<body>

<div class="popup pop_register">
	<div class="p_head">
		<h2 class="tit">주문내역 확인</h2>
		<button type="button" class="btn_close b-close"><span>닫기</span></button>
	</div>
	<div class="p_cont">
		<div class="tbl_basic">
			<table class="write">
				<colgroup>
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
				</colgroup>
				<tbody>
					<tr>
						<th>처방명</th>
						<td colspan="5">${VO.RECIPE_NM}</td>
					</tr>
					<tr>
						<th>약재 구성</th>
						<td colspan="5" class="pd">
							<div class="scrollY">
								<table class="list small">
									<colgroup>
										<col class="num" />
										<col />
										<col width="100px" />
										<col width="100px" />
									</colgroup>
									<thead>
										<tr>
											<th>번호</th>
											<th>약재명</th>
											<th>원산지</th>
											<th>총 용량</th>
										</tr>
									</thead>
									<tbody>
										<c:set var="total_drug">0</c:set>
										<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
											<tr>
												<td>${i}</td>
												<td>${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
												<td>${LIST_RECIPE.get(i - 1).ORIGIN}</td>
												<td><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).TOTAL_QNTT}" /> g</td>
											</tr>
										</c:forEach>										
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<th>총 약재량</th>
						<td colspan="4"><mask:display type="NUMBER" value="${VO.TOTAL_QNTT}" /> g</td>
					</tr>						
					<tr>
						<td colspan="2">
							<div>
								<c:if test="${VO.PACKAGE1_FILEPATH ne ''}">
									<img src="/download?filepath=${VO.PACKAGE1_FILEPATH}" alt="사진">
								</c:if>
							</div>
							<p class="ta-c pt5">${VO.PACKAGE1_NM}</p>
						</td>
						<td colspan="2">
							<div>
								<c:if test="${VO.PACKAGE2_FILEPATH ne ''}">
									<img src="/download?filepath=${VO.PACKAGE2_FILEPATH}" alt="사진">
								</c:if>
							</div>
							<p class="ta-c pt5">${VO.PACKAGE2_NM}</p>
						</td>
						<td colspan="2">
							<div>
								<c:if test="${VO.PACKAGE3_FILEPATH ne ''}">
									<img src="/download?filepath=${VO.PACKAGE3_FILEPATH}" alt="사진">
								</c:if>
							</div>
							<p class="ta-c pt5">${VO.PACKAGE3_NM}</p>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<c:if test="${VO.PACKAGE1_FILEPATH ne ''}">
								<p class="ta-c pt5">주문수량 : ${VO.PACKAGE1_ORDER}</p>
							</c:if>
						</td>
						<td colspan="2">
							<c:if test="${VO.PACKAGE2_FILEPATH ne ''}">
								<p class="ta-c pt5">주문수량 : ${VO.PACKAGE2_ORDER}</p>
							</c:if>
						</td>
						<td colspan="2">
							<c:if test="${VO.PACKAGE3_FILEPATH ne ''}">
								<p class="ta-c pt5">주문수량 : ${VO.PACKAGE3_ORDER}</p>
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="btn-bot">
			<a href="javascript:;" class="btn-pk n white b-close"><span>닫기</span></a>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/pop_footer.jsp" %>