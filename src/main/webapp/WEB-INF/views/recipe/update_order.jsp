<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script>
	$(function() {	
		if(!"${ORDER.SEND_TP}") {
			SetRadioValue("send_tp", "1", "checked");			
			setSender("1");
		}
		
		if(!"${ORDER.RECV_TP}") {
			SetRadioValue("recv_tp", "1", "checked");		
			setRecver("1");
		}
		
		$('#btnPre').click(function() {
			if(confirm("입력한 내용을 임시 저장 후, 이전 단계로 이동 하시겠습니까?\r\n[확인] 클륵 시, 저장 후 이전 단계로 이동합니다.\r\n[취소] 클릭 시, 저장하지 않고 이전 단계로 이동합니다.")) {
				goSave("PRE_TEMP");
			}
			else {
				goSave("PRE");
			}
	    });
		
		$('#btnTemp').click(function() {
			goSave("TEMP");
	    });
		
		$('#btnOrder').click(function() {
			goSave("SAVE");
	    });
		
		$("#send_nm").focus();
		
		if("${RECIPE.PACKAGE1}") {
			getItemInfo("1", "${RECIPE.PACKAGE1}");	
		}
		if("${RECIPE.PACKAGE2}") {
			getItemInfo("2", "${RECIPE.PACKAGE2}");	
		}
		if("${RECIPE.PACKAGE3}") {
			getItemInfo("3", "${RECIPE.PACKAGE3}");	
		}
		if("${RECIPE.PACKAGE4}") {
			getItemInfo("4", "${RECIPE.PACKAGE4}");	
		}		
	});
	
	function getItemInfo(pkg, bom_cd) {		
		console.log("getItemInfo : " + bom_cd);
		if(!bom_cd) return; 
		
		//if(ajaxRunning) return;
		
		var formData = new FormData();
		formData.append("BOM_CD", bom_cd);
		
	    var url = "/bom/selectItems";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	//ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	//HideCSS($(".loadingbar"));
				
	            if(response.result == 200) {  	            	   
	            	var sHTML = "";
	            	sHTML += "	<div class='owl-carousel' id='rolling" + pkg + "'>";
	            	for(var i = 0; i < response.list.length; i++) {	
	            		sHTML += "		<div class='item'>";
	        			sHTML += "			<div class='img_prd long'>";
	        			
	        			var filepath = response.list[i].FILEPATH;
	        			var item_nm = response.list[i].ITEM_NM;
	        			
	        			if(filepath) {
	        				sHTML += "				<img src='/download?filepath=" + filepath + "' title='" + item_nm + "' />";
	        			}
	        			else {
	        				sHTML += "				<img src='/images/common/img_noimg.jpg' title='" + item_nm + "' />";
	        			}
						
						sHTML += "			</div>";
						sHTML += "		</div>";	
	            	}
	            	
	            	sHTML += "	</div>";          	
	            	
	            	$("#package" + pkg).html(sHTML);
	            	var slider = $("#package" + pkg + " #rolling" + pkg);
	        		slider.owlCarousel({
	        			loop: $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        			margin:0,
	        			nav:false,
	        			dots: $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        			items:1,
	        			smartSpeed:1500,
	        			autoplay:true,
	        			autoplayTimeout:5000,
	        			autoplayHoverPause:false,
	        			mouseDrag : $("#package" + pkg + " #rolling" + pkg + " .item").length > 1 ? true: false,
	        		});

	        		slider.trigger('refresh.owl.carousel');
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	//HideCSS($(".loadingbar"));

	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}

	function OpenDaumZipEx(type) {
		new daum.Postcode({
			oncomplete: function(data) {
				$("#" + type + "_zipcode").val(data.zonecode);
				$("#" + type + "_address").val(data.roadAddress);
			}
		}).open();
	}

	function setSender(value) {
		if(value == "1") {
			$("#send_nm").val("${SESSION.COMPANY}");
			
			var tel = "${SESSION.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#send_tel1").val(tel_array[0]);
					$("#send_tel2").val(tel_array[1]);
					$("#send_tel3").val(tel_array[2]);
				}
				else {
					$("#send_tel1").val("");
					$("#send_tel2").val("");
					$("#send_tel3").val("");
				}
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
			
			var mobile = "${SESSION.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#send_mobile1").val(mobile_array[0]);
					$("#send_mobile2").val(mobile_array[1]);
					$("#send_mobile3").val(mobile_array[2]);
				}
				else {
					$("#send_mobile1").val("");
					$("#send_mobile2").val("");
					$("#send_mobile3").val("");
				}
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
			
			$("#send_zipcode").val("${SESSION.ZIPCODE}");
			$("#send_address").val("${SESSION.ADDRESS}");
			$("#send_address2").val("${SESSION.ADDRESS2}");
		}
		else if(value == "2") {
			//탕전실
			$("#send_nm").val("탕전실");
			
			var tel = "${HANPURE.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#send_tel1").val(tel_array[0]);
					$("#send_tel2").val(tel_array[1]);
					$("#send_tel3").val(tel_array[2]);
				}
				else {
					$("#send_tel1").val("");
					$("#send_tel2").val("");
					$("#send_tel3").val("");
				}
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
			
			var mobile = "${HANPURE.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#send_mobile1").val(mobile_array[0]);
					$("#send_mobile2").val(mobile_array[1]);
					$("#send_mobile3").val(mobile_array[2]);
				}
				else {
					$("#send_mobile1").val("");
					$("#send_mobile2").val("");
					$("#send_mobile3").val("");
				}
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
			
			$("#send_zipcode").val("${HANPURE.ZIPCODE}");
			$("#send_address").val("${HANPURE.ADDRESS}");
			$("#send_address2").val("${HANPURE.ADDRESS2}");
		}
		else if(value == "3") {
			//수동입력
			$("#send_nm").val("");
			$("#send_tel1").val("");
			$("#send_tel2").val("");
			$("#send_tel3").val("");
			$("#send_mobile1").val("");
			$("#send_mobile2").val("");
			$("#send_mobile3").val("");
			$("#send_zipcode").val("");
			$("#send_address").val("");
			$("#send_address2").val("");
		}	
	}
	
	function setRecver(value) {
		if(value == "1") {
			$("#recv_nm").val("${PATIENT.NAME}");
			
			var tel = "${PATIENT.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#recv_tel1").val(tel_array[0]);
					$("#recv_tel2").val(tel_array[1]);
					$("#recv_tel3").val(tel_array[2]);
				}
				else {
					$("#recv_tel1").val("");
					$("#recv_tel2").val("");
					$("#recv_tel3").val("");
				}
			}
			else {
				$("#recv_tel1").val("");
				$("#recv_tel2").val("");
				$("#recv_tel3").val("");
			}
			
			var mobile = "${PATIENT.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#recv_mobile1").val(mobile_array[0]);
					$("#recv_mobile2").val(mobile_array[1]);
					$("#recv_mobile3").val(mobile_array[2]);
				}
				else {
					$("#recv_mobile1").val("");
					$("#recv_mobile2").val("");
					$("#recv_mobile3").val("");
				}
			}
			else {
				$("#recv_mobile1").val("");
				$("#recv_mobile2").val("");
				$("#recv_mobile3").val("");
			}
			
			$("#recv_zipcode").val("${PATIENT.ZIPCODE}");
			$("#recv_address").val("${PATIENT.ADDRESS}");
			$("#recv_address2").val("${PATIENT.ADDRESS2}");
		}
		else if(value == "2") {
			$("#recv_nm").val("${SESSION.COMPANY}");
			
			var tel = "${SESSION.TEL}";
			if(tel) {
				var tel_array = tel.split("-");
				if(tel_array.length == 3) {
					$("#recv_tel1").val(tel_array[0]);
					$("#recv_tel2").val(tel_array[1]);
					$("#recv_tel3").val(tel_array[2]);
				}
				else {
					$("#recv_tel1").val("");
					$("#recv_tel2").val("");
					$("#recv_tel3").val("");
				}
			}
			else {
				$("#recv_tel1").val("");
				$("#recv_tel2").val("");
				$("#recv_tel3").val("");
			}
			
			var mobile = "${SESSION.MOBILE}";
			if(mobile) {
				var mobile_array = mobile.split("-");
				if(mobile_array.length == 3) {
					$("#recv_mobile1").val(mobile_array[0]);
					$("#recv_mobile2").val(mobile_array[1]);
					$("#recv_mobile3").val(mobile_array[2]);
				}
				else {
					$("#recv_mobile1").val("");
					$("#recv_mobile2").val("");
					$("#recv_mobile3").val("");
				}
			}
			else {
				$("#recv_mobile1").val("");
				$("#recv_mobile2").val("");
				$("#recv_mobile3").val("");
			}
			
			$("#recv_zipcode").val("${SESSION.ZIPCODE}");
			$("#recv_address").val("${SESSION.ADDRESS}");
			$("#recv_address2").val("${SESSION.ADDRESS2}");
		}
		else if(value == "3") {
			$("#recv_nm").val("");
			
			$("#recv_tel1").val("");
			$("#recv_tel2").val("");
			$("#recv_tel3").val("");
			
			$("#recv_mobile1").val("");
			$("#recv_mobile2").val("");
			$("#recv_mobile3").val("");
			
			$("#recv_zipcode").val("");
			$("#recv_address").val("");
			$("#recv_address2").val("");
		}	
	}
	
	function goSave(save_temp) {
		if(ajaxRunning) return;
		
		var send_tp = GetRadioValue("send_tp");
		var send_nm = GetValue("send_nm");
		var send_tel1 = GetValue("send_tel1");
		var send_tel2 = GetValue("send_tel2");
		var send_tel3 = GetValue("send_tel3");		
		var send_mobile1 = GetValue("send_mobile1");
		var send_mobile2 = GetValue("send_mobile2");
		var send_mobile3 = GetValue("send_mobile3");		
		var send_zipcode = GetValue("send_zipcode");
		var send_address = GetValue("send_address");
		var send_address2 = GetValue("send_address2");
		var recv_tp = GetRadioValue("recv_tp");
		var recv_nm = GetValue("recv_nm");
		var recv_tel1 = GetValue("recv_tel1");
		var recv_tel2 = GetValue("recv_tel2");
		var recv_tel3 = GetValue("recv_tel3");		
		var recv_mobile1 = GetValue("recv_mobile1");
		var recv_mobile2 = GetValue("recv_mobile2");
		var recv_mobile3 = GetValue("recv_mobile3");		
		var recv_zipcode = GetValue("recv_zipcode");
		var recv_address = GetValue("recv_address");
		var recv_address2 = GetValue("recv_address2");		
		var delivery_dt = GetValue("delivery_dt");
		var msg_make = GetValue("msg_make");
		var msg_delivery = GetValue("msg_delivery");
		
		if(save_temp == "SAVE") {
			if(!send_nm) {
		    	alert("발신인을 입력해 주세요.");
		    	$("#send_nm").focus();
		    	return;
		    }
			
			/*
			if(!send_tel1) {
		    	alert("발신인 연락처를 입력해 주세요.");
		    	$("#send_tel1").focus();
		    	return;
		    }
			
			if(!send_tel2) {
		    	alert("발신인 연락처를 입력해 주세요.");
		    	$("#send_tel2").focus();
		    	return;
		    }
			
			
			if(!send_tel3) {
		    	alert("발신인 연락처를 입력해 주세요.");
		    	$("#send_tel3").focus();
		    	return;
		    }	
			*/
			
			if(!send_mobile1) {
		    	alert("발신인 휴대폰을 입력해 주세요.");
		    	$("#send_mobile1").focus();
		    	return;
		    }
			
			if(!send_mobile2) {
		    	alert("발신인 휴대폰을 입력해 주세요.");
		    	$("#send_mobile2").focus();
		    	return;
		    }
			
			if(!send_mobile3) {
		    	alert("발신인 휴대폰을 입력해 주세요.");
		    	$("#send_mobile3").focus();
		    	return;
		    }
			
			if(!send_zipcode) {
		    	alert("발신인 주소를 검색 해 주세요.");
		    	$("#send_zipcode").focus();
		    	return;
		    }
			
			if(!send_address) {
				alert("발신인 주소를 검색 해 주세요.");
		    	$("#send_address").focus();
		    	return;
		    }
			
			if(!send_address2) {
				alert("발신인 상세주소를 입력 해 주세요.");
		    	$("#send_address2").focus();
		    	return;
		    }
			
			if(!recv_nm) {
		    	alert("수신인을 입력해 주세요.");
		    	$("#recv_nm").focus();
		    	return;
		    }
			
			/*
			if(!recv_tel1) {
		    	alert("수신인 연락처를 입력해 주세요.");
		    	$("#recv_tel1").focus();
		    	return;
		    }
			
			if(!recv_tel2) {
		    	alert("수신인 연락처를 입력해 주세요.");
		    	$("#recv_tel2").focus();
		    	return;
		    }	
			
			if(!recv_tel3) {
		    	alert("수신인 연락처를 입력해 주세요.");
		    	$("#recv_tel3").focus();
		    	return;
		    }	
			*/
			
			if(!recv_mobile1) {
		    	alert("수신인 휴대폰을 입력해 주세요.");
		    	$("#recv_mobile1").focus();
		    	return;
		    }
			
			if(!recv_mobile2) {
		    	alert("수신인 휴대폰을 입력해 주세요.");
		    	$("#recv_mobile2").focus();
		    	return;
		    }
			
			if(!recv_mobile3) {
		    	alert("수신인 휴대폰을 입력해 주세요.");
		    	$("#recv_mobile3").focus();
		    	return;
		    }
			
			if(!recv_zipcode) {
		    	alert("수신인 주소를 검색 해 주세요.");
		    	$("#recv_zipcode").focus();
		    	return;
		    }
			
			if(!recv_address) {
				alert("수신인 주소를 검색 해 주세요.");
		    	$("#recv_address").focus();
		    	return;
		    }
			
			if(!recv_address2) {
				alert("수신인 상세주소를 입력 해 주세요.");
		    	$("#recv_address2").focus();
		    	return;
		    }
			
			if(!delivery_dt) {
				alert("발송요청일을 입력 해 주세요.");
		    	$("#delivery_dt").focus();
		    	return;
			}	
			
			if(delivery_dt) {
				var dayOfWeek = getDayOfWeek(delivery_dt);
				console.log("dayOfWeek", dayOfWeek);
				
				if(dayOfWeek == "일") {
					alert("주말에는 발송이 불가능합니다.");
					return;
				}
				
				if(delivery_dt == getToday()) {
					var hour = getHour();
					if(hour > 13) {
						alert("1시 이후 주문건은 당일 배송이 불가능합니다.");
						return;
					} 
				}
				else {
					var days = getDateDiff(getToday(), delivery_dt);
					console.log("days : " + days);
					if(days <= 0) {
						alert("오늘 이 후 날짜를 선택해 주세요.");
						return;	
					}	
				}
			}
		}
		
		var send_tel = send_tel1 + "-" + send_tel2 + "-" + send_tel3;
		var send_mobile = send_mobile1 + "-" + send_mobile2 + "-" + send_mobile3;
		
		var recv_tel = recv_tel1 + "-" + recv_tel2 + "-" + recv_tel3;
		var recv_mobile = recv_mobile1 + "-" + recv_mobile2 + "-" + recv_mobile3;
		
		if(save_temp == "SAVE") {
			if(!confirm("주문 하시겠습니까?")) {
				return;
			}
		}
		else if(save_temp == "TEMP") {
			if(!confirm("임시저장 하시겠습니까?")) {
				return;
			}
		}
		
	    var formData = new FormData();	
	    formData.append("RECIPE_SEQ", "${RECIPE.RECIPE_SEQ}");
	    formData.append("SAVE_TEMP", save_temp);
	    formData.append("SEND_TP", send_tp);
   		formData.append("SEND_NM", send_nm);
   		formData.append("SEND_TEL", send_tel);		
   		formData.append("SEND_MOBILE", send_mobile);		
   		formData.append("SEND_ZIPCODE", send_zipcode);
   		formData.append("SEND_ADDRESS", send_address);
   		formData.append("SEND_ADDRESS2", send_address2);
   		formData.append("RECV_TP", recv_tp);
   		formData.append("RECV_NM", recv_nm);
   		formData.append("RECV_TEL", recv_tel);		
   		formData.append("RECV_MOBILE", recv_mobile);		
   		formData.append("RECV_ZIPCODE", recv_zipcode);
   		formData.append("RECV_ADDRESS", recv_address);
   		formData.append("RECV_ADDRESS2", recv_address2);		
   		formData.append("DELIVERY_DT", delivery_dt);
   		formData.append("MSG_MAKE", msg_make);
   		formData.append("MSG_DELIVERY", msg_delivery);
	    
	    var url = "/recipe/order/write";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	if(save_temp == "PRE") {
	            		location.reload();
	            	}
	            	else if(save_temp == "PRE_TEMP") {
	            		alert("임시 저장이 완료 되었습니다.\r\n이전 단계로 이동합니다.");
	            		location.reload();
	            	}
	            	else if(save_temp == "TEMP") {
	            		alert("임시 저장이 완료 되었습니다.");
	            		location.href = "/recipe/list_temp";
	            	}   
	            	else if(save_temp == "SAVE") {	            		
	            		if(confirm("주문이 완료되었습니다.\r\n결제는 [마이페이지]에서 진행할 수 있습니다.\r\n마이페이지로 이동 하시겠습니까?")) {
	            			location.href = "/order/list";
	            		}
	            		else {
	            			location.href = "/order/list";	
	            		}
	            	}   
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}		
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">배송정보 입력</h2>
			</header>

			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">보내는 사람</h3>
				<div class="tbl_basic pr-mb1">
					<table class="write">
						<colgroup>
							<col class="th2">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>발신지 선택 <span class="i_emp">*</span></th>
								<td>
									<label class="inp_radio mr20"><input type="radio" name="send_tp" value="1" onClick="setSender(this.value)" <c:if test="${ORDER.SEND_TP eq '1'}">checked</c:if> /><span>한의원</span></label>
									<label class="inp_radio mr20"><input type="radio" name="send_tp" value="2" onClick="setSender(this.value)" <c:if test="${ORDER.SEND_TP eq '2'}">checked</c:if> /><span>탕전실</span></label>
									<label class="inp_radio"><input type="radio" name="send_tp" value="3" onClick="setSender(this.value)" <c:if test="${ORDER.SEND_TP eq '3'}">checked</c:if> /><span>수동입력</span></label>
								</td>
							</tr>
							<tr>
								<th>발신인 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt w100p" name="send_nm" id="send_nm" value="${ORDER.SEND_NM}" placeholder="발신인 정보를 입력하세요" /></td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<div class="inp_phone">
										<input type="text" name="send_tel" id="send_tel1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.SEND_TEL}" />' />
										<span>-</span>
										<input type="text" name="send_tel" id="send_tel2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL2" value="${ORDER.SEND_TEL}" />' />
										<span>-</span>
										<input type="text" name="send_tel" id="send_tel3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL3" value="${ORDER.SEND_TEL}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_phone">
										<input type="text" name="send_mobile" id="send_mobile1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.SEND_MOBILE}" />' />
										<span>-</span>
										<input type="text" name="send_mobile" id="send_mobile2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.SEND_MOBILE}" />' />
										<span>-</span>
										<input type="text" name="send_mobile" id="send_mobile3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.SEND_MOBILE}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>주소 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_addr">
										<div class="inp">
											<button type="button" class="btn-pk n blue2" onClick="OpenDaumZipEx('send')"><span>우편번호</span></button>
											<input type="text" name="send_zipcode" id="send_zipcode" class="inp_txt" onClick="OpenDaumZipEx('send')" value="${ORDER.SEND_ZIPCODE}" readOnly placeholder="우편번호 검색을 클릭하세요" />
										</div>
										<input type="text" name="send_address" id="send_address" class="inp_txt w100p" onClick="OpenDaumZipEx('send')" value="${ORDER.SEND_ADDRESS}" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력" />
										<input type="text" name="send_address2" id="send_address2" class="inp_txt w100p" placeholder="나머지 상세 주소를 입력하세요" value="${ORDER.SEND_ADDRESS2}" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<h3 class="h_tit2">받는 사람</h3>
				<div class="tbl_basic pr-mb1">
					<table class="write">
						<colgroup>
							<col class="th2">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>수신인 선택 <span class="i_emp">*</span></th>
								<td>
									<label class="inp_radio mr20"><input type="radio" name="recv_tp" value="1" onClick="setRecver(this.value)" <c:if test="${ORDER.RECV_TP eq '1'}">checked</c:if> /><span>환자</span></label>
									<label class="inp_radio mr20"><input type="radio" name="recv_tp" value="2" onClick="setRecver(this.value)" <c:if test="${ORDER.RECV_TP eq '2'}">checked</c:if> /><span>한의원</span></label>
									<label class="inp_radio"><input type="radio" name="recv_tp" value="3" onClick="setRecver(this.value)" <c:if test="${ORDER.RECV_TP eq '3'}">checked</c:if> /><span>수동입력</span></label>
								</td>
							</tr>
							<tr>
								<th>수신인 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt w100p" name="recv_nm" id="recv_nm" value="${ORDER.RECV_NM}" placeholder="수신인 정보를 입력하세요" /></td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<div class="inp_phone">
										<input type="text" name="recv_tel" id="recv_tel1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.RECV_TEL}" />' />
										<span>-</span>
										<input type="text" name="recv_tel" id="recv_tel2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.RECV_TEL}" />' />
										<span>-</span>
										<input type="text" name="recv_tel" id="recv_tel3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.RECV_TEL}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>휴대폰 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_phone">
										<input type="text" name="recv_mobile" id="recv_mobile1" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.RECV_MOBILE}" />' />
										<span>-</span>
										<input type="text" name="recv_mobile" id="recv_mobile2" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.RECV_MOBILE}" />' />
										<span>-</span>
										<input type="text" name="recv_mobile" id="recv_mobile3" class="inp_txt number" maxlength="4" value='<mask:display type="TEL1" value="${ORDER.RECV_MOBILE}" />' />
									</div>
								</td>
							</tr>
							<tr>
								<th>주소 <span class="i_emp">*</span></th>
								<td>
									<div class="inp_addr">
										<div class="inp">
											<button type="button" class="btn-pk n blue2" onClick="OpenDaumZipEx('recv')"><span>우편번호</span></button>
											<input type="text" name="recv_zipcode" id="recv_zipcode" class="inp_txt" onClick="OpenDaumZipEx('recv')" value="${ORDER.RECV_ZIPCODE}" readOnly placeholder="우편번호 검색을 클릭하세요" />
										</div>
										<input type="text" name="recv_address" id="recv_address" class="inp_txt w100p" onClick="OpenDaumZipEx('recv')" value="${ORDER.RECV_ADDRESS}" readOnly placeholder="우편번호 검색 시 읍,면,동 자동입력" />
										<input type="text" name="recv_address2" id="recv_address2" class="inp_txt w100p" placeholder="나머지 상세 주소를 입력하세요" value="${ORDER.RECV_ADDRESS2}" />
									</div>
								</td>
							</tr>
							<tr>
								<th>배송기사<br>요청사항</th>
								<td><textarea rows="3" class="textare1 w100p" name="msg_delivery" id="msg_delivery" placeholder=" 택배 배송기사에게 전달할 요청사항을 적어주세요(200자 이내)">${ORDER.MSG_DELIVERY}</textarea></td>
							</tr>
							<tr>
								<th>발송요청일 <span class="i_emp">*</span></th>
								<td><input type="text" class="inp_txt calender datepicker date" name="delivery_dt" id="delivery_dt" value="${ORDER.DELIVERY_DT}" readOnly placeholder="yyyy-mm-dd" /></td>
							</tr>
						</tbody>
					</table>
					<p class="t_info mt10 c-blue">* 오후 1시 이전 주문완료 시 당일 발송이 가능하며, 오후 1시 이후 주문완료 시 익일 발송됩니다.</p>
				</div>

				<h3 class="h_tit2">배송 안내</h3>
				<div class="tbl_basic">
					<mask:display type="EDITOR" value="${HANPURE.DELIVERY_MSG}" />
				</div>
			</div><!--// lft_c -->

			<!-- 우측데이터 -->
			<div class="rgh_c">
				<h3 class="h_tit2">주문 정보</h3>
				<div class="tbl_basic ">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>처방구분</th>
								<td colspan="3">
									<c:if test="${RECIPE.RECIPE_TP eq '001'}">탕전</c:if>
									<c:if test="${RECIPE.RECIPE_TP eq '002'}">제환</c:if>
									<c:if test="${RECIPE.RECIPE_TP eq '003'}">연조엑스</c:if>
									<c:if test="${RECIPE.RECIPE_TP eq '004'}">첩약</c:if>									
								</td>
							</tr>
							<tr>
								<th>환자명</th>
								<td colspan="3">${PATIENT.NAME}</td>
							</tr>
							<tr>
								<th>연락처</th>
								<td colspan="3">${PATIENT.MOBILE}</td>
							</tr>
							<tr>
								<th>주소</th>
								<td colspan="3">(${PATIENT.ZIPCODE})${PATIENT.ADDRESS} ${PATIENT.ADDRESS2}</td>
							</tr>
							<tr>
								<th>처방명</th>
								<td colspan="3">${RECIPE.RECIPE_NM}</td>
							</tr>
							<tr>
								<th>약재 구성</th>
								<td colspan="3">
									<table class="list small cur">
										<colgroup>
											<col class="num">
											<col>
											<col>
											<col>
											<col>
											<col>
										</colgroup>
										<thead>
											<tr>
												<th>번호</th>
												<th>약재명</th>
												<th>유형</th>
												<th>원산지</th>
												<th>1첩량</th>
												<th>총 용량</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
												<tr>
													<td>${i}</td>
													<td>${LIST_RECIPE.get(i - 1).DRUG_NM}</td>
													<td>
														<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">일반</c:if>
														<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">선전</c:if>
														<c:if test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">후하</c:if>
													</td>
													<td>${LIST_RECIPE.get(i - 1).ORIGIN}</td>
													<td><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).CHUP1}" />g</td>
													<td><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).TOTAL_QNTT}" />g</td>
												</tr>
											</c:forEach>
												<tr>
													<td colspan="4">총약재량</td>
													<td colspan="2"><mask:display type="NUMBER" value="${RECIPE.TOTAL_QNTT}" />g</td>
												</tr>
										</tbody>
									</table>
								</td>
							</tr>
							
							<c:if test="${RECIPE.RECIPE_TP eq '001'}">
								<tr>
									<th>첩수</th>
									<td>${RECIPE.INFO1} 첩</td>
									<th>팩수</th>
									<td>${RECIPE.INFO2} 팩</td>									
								</tr>
								<tr>							
									<th>팩용량</th>
									<td>${RECIPE.INFO3} mL</td>	
									<th>특수탕전</th>
									<td>${MAP_SPECIAL_TP.get(RECIPE.INFO4)}</td>
								</tr>
								<tr>	
									<th>탕전법</th>
									<td>${RECIPE.INFO5}</td>
									<th>탕전 물양</th>
									<td><mask:display type="NUMBER" value="${RECIPE.INFO6}" />mL</td>
								</tr>
								<tr>	
									<th>향미제</th>
									<td>
										<c:if test="${RECIPE.INFO7 eq 'Y'}">선택없음</c:if>
										<c:if test="${RECIPE.INFO7 eq 'N'}">선택</c:if>
									</td>
									<th>앰플(자하거)</th>
									<td>
										<c:if test="${RECIPE.INFO8 ne ''}">${RECIPE.INFO8}mL</c:if>
										<c:if test="${RECIPE.INFO8 eq ''}">선택없음</c:if>
									</td>
								</tr>
								<tr>	
									<th>녹용별전</th>
									<td>
										<c:if test="${RECIPE.INFO9 ne ''}">${RECIPE.INFO9}mL</c:if>
										<c:if test="${RECIPE.INFO9 eq ''}">선택없음</c:if>
									</td>
									<th>주수상반</th>
									<td>
										<c:if test="${RECIPE.INFO10 ne ''}">${RECIPE.INFO10_NM}/${RECIPE.INFO10_QNTT}mL</c:if>
										<c:if test="${RECIPE.INFO10 eq ''}">선택없음</c:if>
									</td>
								</tr>
								<tr>	
									<th>재탕전</th>
									<td>
										<c:if test="${RECIPE.RE_HOTPOT eq 'Y'}">재탕전</c:if>
										<c:if test="${RECIPE.RE_HOTPOT eq 'N'}">선택없음</c:if>
									</td>
									<th>탕전시간(분)</th>
									<td>
										<mask:display type="NUMBER" value="${RECIPE.INFO11}" />분
									</td>
								</tr>
								<tr>
									<td rowspan="3" colspan="4" class="pd">
										<table class="">
											<thead>
												<tr>
													<th>파우치</th>
													<th>탕약박스</th>
													<th>배송박스</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td id="package1">
																									
													</td>
													<td id="package2">
																									
													</td>
													<td id="package3">
																									
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</c:if>
							
							<c:if test="${RECIPE.RECIPE_TP eq '002'}">
								<tr>
									<th>첩수</th>
									<td>${RECIPE.INFO1} 첩</td>
									<th>농축</th>
									<td>${MAP_PRESS_TP.get(RECIPE.INFO2)}</td>							
								</tr>
								<tr>
									<th>제형</th>
									<td>${MAP_CIRCLE_SIZE.get(RECIPE.INFO3)}</td>
									<th>부형제</th>
									<td>${MAP_DOUGH_TP.get(RECIPE.INFO4)}</td>
								</tr>
								<tr>
									<th>분말도</th>
									<td>${MAP_POWDER_TP.get(RECIPE.INFO5)}</td>
									<th>제분손실량</th>
									<td><mask:display type="NUMBER" value="${RECIPE.INFO6}" />g</td>
								</tr>
								<tr>
									<th>제환손실량</th>
									<td><mask:display type="NUMBER" value="${RECIPE.INFO7}" />g</td>
									<th>부형제량</th>
									<td><mask:display type="NUMBER" value="${RECIPE.INFO8}" />g</td>
								</tr>								
								<tr>
									<td rowspan="3" colspan="4" class="pd">
										<table class="">
											<thead>
												<tr>
													<th>내지</th>
													<th>소포장</th>
													<th>포장박스</th>
													<th>배송박스</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td id="package1">
																									
													</td>
													<td id="package2">
																									
													</td>
													<td id="package3">
																									
													</td>	
													<td id="package4">
																									
													</td>												
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</c:if>		
							<c:if test="${RECIPE.RECIPE_TP eq '003'}">
								<tr>
									<th>첩수</th>
									<td>${RECIPE.INFO2} 첩</td>
									<th>스틱수</th>
									<td>${RECIPE.INFO2} 포</td>
								</tr>
								<tr>
									<td rowspan="3" colspan="4" class="pd">
										<table class="">
											<thead>
												<tr>
													<th>스틱</th>
													<th>케이스</th>
													<th>배송박스</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td id="package1">
																									
													</td>
													<td id="package2">
																									
													</td>
													<td id="package3">
																									
													</td>											
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</c:if>	
							
							<c:if test="${RECIPE.RECIPE_TP eq '004'}">
								<tr>
									<th>첩수</th>
									<td>${RECIPE.INFO1} 첩</td>
									<th>팩수</th>
									<td>${RECIPE.INFO2} 팩</td>									
								</tr>
								<tr>							
									<th>팩용량</th>
									<td>${RECIPE.INFO3} mL</td>	
									<th>특수탕전</th>
									<td>${MAP_SPECIAL_TP.get(RECIPE.INFO4)}</td>
								</tr>
								<tr>	
									<th>탕전법</th>
									<td>${RECIPE.INFO5}</td>
									<th>탕전 물양</th>
									<td><mask:display type="NUMBER" value="${RECIPE.INFO6}" />mL</td>
								</tr>
								<tr>	
									<th>향미제</th>
									<td>
										<c:if test="${RECIPE.INFO7 eq 'Y'}">선택없음</c:if>
										<c:if test="${RECIPE.INFO7 eq 'N'}">선택</c:if>
									</td>
									<th>앰플(자하거)</th>
									<td>
										<c:if test="${RECIPE.INFO8 ne ''}">${RECIPE.INFO8}mL</c:if>
										<c:if test="${RECIPE.INFO8 eq ''}">선택없음</c:if>
									</td>
								</tr>
								<tr>	
									<th>녹용별전</th>
									<td>
										<c:if test="${RECIPE.INFO9 ne ''}">${RECIPE.INFO9}mL</c:if>
										<c:if test="${RECIPE.INFO9 eq ''}">선택없음</c:if>
									</td>
									<th>주수상반</th>
									<td>
										<c:if test="${RECIPE.INFO10 ne ''}">${RECIPE.INFO10_NM}/${RECIPE.INFO10_QNTT}mL</c:if>
										<c:if test="${RECIPE.INFO10 eq ''}">선택없음</c:if>
									</td>
								</tr>
								<tr>	
									<th>재탕전</th>
									<td>
										<c:if test="${RECIPE.RE_HOTPOT eq 'Y'}">재탕전</c:if>
										<c:if test="${RECIPE.RE_HOTPOT eq 'N'}">선택없음</c:if>
									</td>
									<th>탕전시간(분)</th>
									<td>
										<mask:display type="NUMBER" value="${RECIPE.INFO11}" />분
									</td>									
								</tr>
								<tr>
									<td rowspan="3" colspan="4" class="pd">
										<table class="">
											<thead>
												<tr>
													<th>파우치</th>
													<th>탕약박스</th>
													<th>배송박스</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td id="package1">
																									
													</td>
													<td id="package2">
																									
													</td>
													<td id="package3">
																									
													</td>	
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</c:if>					
						</tbody>
					</table>
				</div>
			</div><!--// rgh_c -->

			<div class="botm_c">				
				<div class="mbtn_ty1">
					<button type="button" id="btnPre" class="btn-pk b white"><span>이전 단계로 이동</span></button>
					<button type="button" id="btnTemp" class="btn-pk b blue2"><span>처방전 임시저장</span></button>
					<button type="button" id="btnOrder" class="btn-pk b blue"><span>주문하기</span></button>
				</div>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->>

<%@ include file="/WEB-INF/views/include/footer.jsp" %>