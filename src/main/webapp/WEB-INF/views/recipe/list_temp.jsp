<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	$(window).on('hashchange', function(e) {	    
		goPage();
	});
	
	$(function() {		
		$('#searchText').keyup(function(e) {
	        if(e.keyCode == 13) {
	        	setHash(1);
	        }
	    });		
	    
	    if(window.location.hash != "") {
	    	goPage();
	    }
	    else {
	    	setHash(1);
	    }
	    
	    $('.btnDelete').click(function() {
			if(ajaxRunning) return;
			
			var length = GetCheckedLength("chk");
			if(length == 0) {
				alert("하나 이상 선택해 주세요.");
				return;
			}
			
			var formData = new FormData();
			
			for(var i = 0; i < length; i++) {
				var recipe_seq = $("input:checkbox[name='chk']:checked").eq(i).val();
				console.log("recipe_seq : " + recipe_seq);
				
				formData.append("LIST_RECIPE_SEQ", recipe_seq);
			}	
			
		    if(!confirm("선택하신 항목을 삭제 하시겠습니까?")) {
		    	return;
		    }
		    
		    var url = "/recipe/delete";
		    $.ajax({
		        type:"POST",
		        url:url,
		        data:formData,
		        cache: false,
		        processData: false,  // file전송시 필수
		        contentType: false,  // file전송시 필수
		        beforeSend: function() {
		        	ShowCSS($(".loadingbar"));
		        },
		        success:function(response) {
		        	HideCSS($(".loadingbar"));
		        	
		            if(response.result == 200) {	 
		            	goPage();
		            }
		            else {
		            	alert(response.message);
		            }
		        },
		        error:function(request, status, error) {
		        	HideCSS($(".loadingbar"));
		        	
		            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        }
		    });
	    });
	    
	    $('#btnSearch').click(function() {
	    	setHash(1);
	    });
	    
	    $('#btnInit').click(function() {
	    	$("#RECIPE_TP").val("");
	    	
	    	$("#sdate1").val("");
	    	$("#edate1").val("");
	    	
	    	$("#searchField").val("");
			$("#searchText").val("");
			
	    	setHash(1);
	    });
	});
	
	function setHash(page) {		
		var itemPerPage = GetValue("itemPerPage");
		var RECIPE_TP = GetValue("RECIPE_TP");
		var searchField = GetValue("searchField");
		var searchText = GetValue("searchText");
		var sdate1 = GetValue("sdate1");
		var edate1 = GetValue("edate1");
				
		var param = "page=" + page;
		param += "&itemPerPage=" + itemPerPage;
		param += "&RECIPE_TP=" + RECIPE_TP;
		param += "&searchField=" + searchField;
		param += "&searchText=" + searchText;	
		param += "&sdate1=" + sdate1;
		param += "&edate1=" + edate1;
		
		window.location.hash = param;
	}
	
	function goPage() {		
		if(ajaxRunning) return;
		
		var param = window.location.hash.substring(1);
		var params = param.split('&');
		
		var formData = new FormData();
		for(var i = 0; i < params.length; i++) {
		   var key = params[i].split('=')[0];
		   var value = decodeURI(params[i].split('=')[1]);
		   
		   $("#" + key).val(value);
		   
		   console.log("key : " + key);
		   console.log("value : " + value);		   
		   
		   formData.append(key, value);         
		}
		
	    var url = "/recipe/selectPageTemp";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	
	            if(response.result == 200) {                	
	            	var sHTML = "";
	            	var startno = parseInt(response.startno);
	            	var totalno = parseInt(response.totalno);
	            	$("#totalno").html(NumberFormat(totalno));
	            	
	            	if(response.list.length > 0) {
	            		for(var i = 0; i < response.list.length; i++) {   
	            			sHTML += "<tr>";
	            			sHTML += "	<td><label class='inp_checkbox'><input type='checkbox' name='chk' class='chk' value='" + response.list[i].RECIPE_SEQ + "' /><span class='d'></span></label></td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + startno + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].RECIPE_TP_NM + "</td>";	            			
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].RECIPE_CD + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].DOCTOR + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].RECIPE_NM + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + response.list[i].PATIENT_NM + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + NumberFormat(response.list[i].AMOUNT) + "</td>";
	            			sHTML += "	<td onClick=\"goUpdate('" + response.list[i].RECIPE_SEQ + "')\">" + getDate10(response.list[i].UPDATED_AT) + "</td>";
	            			sHTML += "</tr>";
		            		
		            		startno--;
	            		}
	            	}
	            	else {
	            		sHTML += "<tr>";
	            		sHTML += "	<td colspan='9'>";
	            		sHTML += "		<p class='notx'>내용이 없습니다.</p>";
						sHTML += "	</td>";
						sHTML += "</tr>";
	            	}
	            	
	            	$("#tbody").html(sHTML);
					$(".pagenation").html(response.pagenation);	  
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
	
	function goUpdate(recipe_seq) {
		location.href = "/recipe/update/" + recipe_seq + "/" + window.location.hash;
	}
	
	function allCheck(obj) {
		if($(obj).prop("checked")) {
			$(".chk").prop("checked", true);
		}
		else {
			$(".chk").prop("checked", false);
		}
	}
	
	function setDate(type, period) {
		var today = getToday();
		if(period == "1") {
			$("#sdate" + type).val(getPreMonth(today, -3));
			$("#edate" + type).val(today);
		}
		else if(period == "2") {
			$("#sdate" + type).val(getPreMonth(today, -6));
			$("#edate" + type).val(today);
		}
		else if(period == "3") {
			$("#sdate" + type).val(getPreMonth(today, -9));
			$("#edate" + type).val(today);
		}
		else if(period == "4") {
			$("#sdate" + type).val(getPreMonth(today, -12));
			$("#edate" + type).val(today);
		}
		else {
			$("#sdate" + type).val("");
			$("#edate" + type).val("");
		}
	}
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">임시저장</h2>
			</header>

			<div class="tbl_box_sch">
				<div class="in">
					<div class="col c10">
						<p>구분</p>
						<select name="RECIPE_TP" id="RECIPE_TP" class="select1">
							<option value="">전체</option>
							
							<c:forEach var="i" begin="1" end="${LIST_RECIPE_TP.size()}">
								<option value="${LIST_RECIPE_TP.get(i - 1).SUB_CD}">${LIST_RECIPE_TP.get(i - 1).SUB_NM_KR}</option>
							</c:forEach>	
						</select>
					</div>
					<div class="col c45">
						<p>등록일자</p>
						<div class="inp_cal">
							<input type="text" name="sdate1" id="sdate1" class="inp_txt calender datepicker_first date" readOnly placeholder="yyyy-mm-dd" />
							<span>~</span>
							<input type="text" name="edate1" id="edate1" class="inp_txt calender datepicker_last date" readOnly placeholder="yyyy-mm-dd" />
						</div>
						<select class="select1" onChange="setDate('1', this.value)">
							<option value="">전체</option>
							<option value="1">3개월</option>
							<option value="2">6개월</option>
							<option value="3">9개월</option>
							<option value="4">12개월</option>
						</select>
					</div>
					<div class="col c45 sch">
						<select name="searchField" id="searchField" class="select1">
							<option value="">전체</option>
							<option value="1">처방의</option>
							<option value="2">처방명</option>
							<option value="3">환자명</option>
						</select>
						<input type="text" name="searchText" id="searchText" class="inp_txt wid1" placeholder="검색어를 입력해주세요." />
						<button type="button" id="btnSearch" class="btn-pk n blue"><span class="i-aft i_sch">검색</span></button>
						<button type="button" id="btnInit" class="btn-pk n blue2"><span>초기화</span></button>
					</div>
				</div>
			</div>

			<div class="tbl_top">
				<p class="t_total">총 <strong class="c-blue" id="totalno">0</strong>건</p>
				<select name="itemPerPage" id="itemPerPage" class="select1">
					<option value="10">10개씩 보기</option>
					<option value="20">20개씩 보기</option>
					<option value="30">30개씩 보기</option>
					<option value="40">40개씩 보기</option>
					<option value="0">전체보기</option>
				</select>
				<div class="rgh">
					<button type="button" class="btn-pk s white btnDelete"><span>삭제</span></button>
				</div>
			</div>

			<div class="tbl_basic">
				<table class="list cur">
					<colgroup>
						<col class="num">
						<col class="num">
						<col class="name">
						<col class="code">
						<col class="name">
						<col>
						<col class="name">
						<col>
						<col class="day">
					</colgroup>
					<thead>
						<tr>
							<th><label class="inp_checkbox"><input type="checkbox" onClick="allCheck(this)"><span class="d" title="전체선택"></span></label></th>
							<th>순번</th>
							<th>구분</th>
							<th>처방코드</th>
							<th>처방의</th>
							<th>처방명</th>
							<th>환자명</th>
							<th>주문금액</th>
							<th>등록일자</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
					</tbody>
				</table>
			</div>
			<div class="tbl_botm ta-r">
				<button type="button" class="btn-pk white btnDelete"><span>삭제</span></button>
			</div>
			<div class="pagenation">
				<ul>
					
				</ul>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/footer.jsp" %>