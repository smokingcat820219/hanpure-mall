<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/views/include/top.jsp" %>

<script>
	var RECIPE_SEQ = "";
	
	var HANPURE = ${HANPURE};
	HANPURE.PRICE_HOTPOT = 0;
	HANPURE.PRICE_MAKE = 0;	
	
	var CLASS = "${SESSION.CLASS}";
	var POISON = "${POISON}";
	var CONFLICT = ${CONFLICT};	
	
	var POP_WEIGHT = 0;
	var POP_QNTT = 0;
	
	if(CLASS == "B") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_B;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_B;
	}
	else if(CLASS == "C") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_C;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_C;
	}
	else if(CLASS == "D") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_D;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_D;
	}
	else if(CLASS == "E") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_E;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_E;
	}
	else {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_A;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_A;
	}
</script>

<link href="/css/owl.carousel.min.css" rel="stylesheet">
<script src="/js/owl.carousel.min.js"></script>

<script src="/js/recipe_order.js"></script>
<script src="/js/patient.js"></script>
<script src="/js/howto.js"></script>

<script>	
	$(function() {
		/*
		//자동 저장 2분
		setInterval(function() {
			goSave("AUTO");
			  
		}, 1000 * 60 * 2);	//2분
		*/
	    
	    $('#btnInit').click(function() {
	    	deleteDrugAll();
	    });
	    
	    $('#btnDrugDelete').click(function() {
	    	deleteDrug();
	    });	  
	    
	    $('#btnTemp').click(function() {
	    	goSave("TEMP");
	    });
	    
	    $('#btnSave').click(function() {
	    	goSave("SAVE");
	    });
	    	    
	    <c:if test="${PATIENT ne null}">
	    	tab(".tab_c1", 1);
	    </c:if>
	    <c:if test="${PATIENT eq null}">
	    	tab(".tab_c1", 2);
	    </c:if>
	    
		tab(".tab_c2", 1);
	    
		var recipe_tp = GetRadioValue("recipe_tp");		
		
		addPriceList();
		
		setRecipeTp(recipe_tp);
		
		//selectStick("200");
				
		//calc();		
		
		<c:if test="${RECIPE.RECIPE_TP eq '001'}">
			$("#remark1").html("${RECIPE.REMARK1}");
			$("#remark2").html("${RECIPE.REMARK2}");
			$("#remark3").html("${RECIPE.REMARK3}");
			$("#remark4").html("${RECIPE.REMARK4}");
			$("#remark5").html("${RECIPE.REMARK5}");
			$("#remark6").html("${RECIPE.REMARK6}");
			$("#remark7").html("${RECIPE.REMARK7}");
			$("#remark8").html("${RECIPE.REMARK8}");
			$("#remark9").html("${RECIPE.REMARK9}");
			$("#remark10").html("${RECIPE.REMARK10}");
			$("#remark11").html("${RECIPE.REMARK11}");
			
			$("#price1").html(NumberFormat("${RECIPE.PRICE1}"));
			$("#price2").html(NumberFormat("${RECIPE.PRICE2}"));
			$("#price3").html(NumberFormat("${RECIPE.PRICE3}"));
			$("#price4").html(NumberFormat("${RECIPE.PRICE4}"));
			$("#price5").html(NumberFormat("${RECIPE.PRICE5}"));
			$("#price6").html(NumberFormat("${RECIPE.PRICE6}"));
			$("#price7").html(NumberFormat("${RECIPE.PRICE7}"));
			$("#price8").html(NumberFormat("${RECIPE.PRICE8}"));
			$("#price9").html(NumberFormat("${RECIPE.PRICE9}"));
			$("#price10").html(NumberFormat("${RECIPE.PRICE10}"));
			$("#price11").html(NumberFormat("${RECIPE.PRICE11}"));	
			
			<c:if test="${RECIPE.PACKAGE1 ne null and RECIPE.PACKAGE1 ne ''}">
				$("#form1 #btnPackage1").attr("bom_cd", "${RECIPE.PACKAGE1}");
				$("#form1 #btnPackage1").attr("bom_nm", "${RECIPE.PACKAGE1_NM}");	
				$("#form1 #btnPackage1").attr("bom_tp", "${RECIPE.PACKAGE1_TP}");
				$("#form1 #btnPackage1").html("<span>" + "${RECIPE.PACKAGE1_NM}" + "</span>");
				
				getItemInfo("1", "${RECIPE.PACKAGE1}");
			</c:if>
			
			<c:if test="${RECIPE.PACKAGE2 ne null and RECIPE.PACKAGE2 ne ''}">
				$("#form1 #btnPackage2").attr("bom_cd", "${RECIPE.PACKAGE2}");
				$("#form1 #btnPackage2").attr("bom_nm", "${RECIPE.PACKAGE2_NM}");	
				$("#form1 #btnPackage2").attr("bom_tp", "${RECIPE.PACKAGE2_TP}");
				$("#form1 #btnPackage2").html("<span>" + "${RECIPE.PACKAGE2_NM}" + "</span>");
				
				getItemInfo("2", "${RECIPE.PACKAGE2}");
			</c:if>
		
			<c:if test="${RECIPE.PACKAGE3 ne null and RECIPE.PACKAGE3 ne ''}">
				$("#form1 #btnPackage3").attr("bom_cd", "${RECIPE.PACKAGE3}");
				$("#form1 #btnPackage3").attr("bom_nm", "${RECIPE.PACKAGE3_NM}");		
				$("#form1 #btnPackage3").attr("bom_tp", "${RECIPE.PACKAGE3_TP}");
				$("#form1 #btnPackage3").html("<span>" + "${RECIPE.PACKAGE3_NM}" + "</span>");
				
				getItemInfo("3", "${RECIPE.PACKAGE3}");
			</c:if>
		</c:if>
		<c:if test="${RECIPE.RECIPE_TP eq '002'}">
			$("#remark1").html("${RECIPE.REMARK1}");
			$("#remark2").html("${RECIPE.REMARK2}");
			$("#remark3").html("${RECIPE.REMARK3}");
			$("#remark4").html("${RECIPE.REMARK4}");
			$("#remark5").html("${RECIPE.REMARK5}");
			$("#remark6").html("${RECIPE.REMARK6}");
			$("#remark7").html("${RECIPE.REMARK7}");
			$("#remark8").html("${RECIPE.REMARK8}");
			$("#remark9").html("${RECIPE.REMARK9}");
			$("#remark10").html("${RECIPE.REMARK10}");
			$("#remark11").html("${RECIPE.REMARK11}");
			
			$("#price1").html(NumberFormat("${RECIPE.PRICE1}"));
			$("#price2").html(NumberFormat("${RECIPE.PRICE2}"));
			$("#price3").html(NumberFormat("${RECIPE.PRICE3}"));
			$("#price4").html(NumberFormat("${RECIPE.PRICE4}"));
			$("#price5").html(NumberFormat("${RECIPE.PRICE5}"));
			$("#price6").html(NumberFormat("${RECIPE.PRICE6}"));
			$("#price7").html(NumberFormat("${RECIPE.PRICE7}"));
			$("#price8").html(NumberFormat("${RECIPE.PRICE8}"));
			$("#price9").html(NumberFormat("${RECIPE.PRICE9}"));
			$("#price10").html(NumberFormat("${RECIPE.PRICE10}"));
			$("#price11").html(NumberFormat("${RECIPE.PRICE11}"));
			
			<c:if test="${RECIPE.PACKAGE1 ne null and RECIPE.PACKAGE1 ne ''}">
				$("#form2 #btnPackage1").attr("bom_cd", "${RECIPE.PACKAGE1}");
				$("#form2 #btnPackage1").attr("bom_nm", "${RECIPE.PACKAGE1_NM}");	
				$("#form2 #btnPackage1").attr("bom_tp", "${RECIPE.PACKAGE1_TP}");
				$("#form2 #btnPackage1").html("<span>" + "${RECIPE.PACKAGE1_NM}" + "</span>");
				
				getItemInfo("1", "${RECIPE.PACKAGE1}");
			</c:if>
			
			<c:if test="${RECIPE.PACKAGE2 ne null and RECIPE.PACKAGE2 ne ''}">
				$("#form2 #btnPackage2").attr("bom_cd", "${RECIPE.PACKAGE2}");
				$("#form2 #btnPackage2").attr("bom_nm", "${RECIPE.PACKAGE2_NM}");		
				$("#form2 #btnPackage2").attr("bom_tp", "${RECIPE.PACKAGE2_TP}");
				$("#form2 #btnPackage2").html("<span>" + "${RECIPE.PACKAGE2_NM}" + "</span>");
				
				getItemInfo("2", "${RECIPE.PACKAGE2}");
			</c:if>
		
			<c:if test="${RECIPE.PACKAGE3 ne null and RECIPE.PACKAGE3 ne ''}">
				$("#form2 #btnPackage3").attr("bom_cd", "${RECIPE.PACKAGE3}");
				$("#form2 #btnPackage3").attr("bom_nm", "${RECIPE.PACKAGE3_NM}");
				$("#form2 #btnPackage3").attr("bom_tp", "${RECIPE.PACKAGE3_TP}");
				$("#form2 #btnPackage3").html("<span>" + "${RECIPE.PACKAGE3_NM}" + "</span>");
				
				getItemInfo("3", "${RECIPE.PACKAGE3}");
			</c:if>
			
			<c:if test="${RECIPE.PACKAGE4 ne null and RECIPE.PACKAGE4 ne ''}">
				$("#form2 #btnPackage4").attr("bom_cd", "${RECIPE.PACKAGE4}");
				$("#form2 #btnPackage4").attr("bom_nm", "${RECIPE.PACKAGE4_NM}");	
				$("#form2 #btnPackage4").attr("bom_tp", "${RECIPE.PACKAGE4_TP}");
				$("#form2 #btnPackage4").html("<span>" + "${RECIPE.PACKAGE4_NM}" + "</span>");
				
				getItemInfo("4", "${RECIPE.PACKAGE4}");
			</c:if>
		</c:if>
		<c:if test="${RECIPE.RECIPE_TP eq '003'}">
			$("#remark1").html("${RECIPE.REMARK1}");
			$("#remark2").html("${RECIPE.REMARK2}");
			$("#remark3").html("${RECIPE.REMARK3}");
			$("#remark4").html("${RECIPE.REMARK4}");
			$("#remark5").html("${RECIPE.REMARK5}");
			$("#remark6").html("${RECIPE.REMARK6}");
			$("#remark7").html("${RECIPE.REMARK7}");
			$("#remark8").html("${RECIPE.REMARK8}");
			$("#remark9").html("${RECIPE.REMARK9}");
			$("#remark10").html("${RECIPE.REMARK10}");
			$("#remark11").html("${RECIPE.REMARK11}");
			
			$("#price1").html(NumberFormat("${RECIPE.PRICE1}"));
			$("#price2").html(NumberFormat("${RECIPE.PRICE2}"));
			$("#price3").html(NumberFormat("${RECIPE.PRICE3}"));
			$("#price4").html(NumberFormat("${RECIPE.PRICE4}"));
			$("#price5").html(NumberFormat("${RECIPE.PRICE5}"));
			$("#price6").html(NumberFormat("${RECIPE.PRICE6}"));
			$("#price7").html(NumberFormat("${RECIPE.PRICE7}"));
			$("#price8").html(NumberFormat("${RECIPE.PRICE8}"));
			$("#price9").html(NumberFormat("${RECIPE.PRICE9}"));
			$("#price10").html(NumberFormat("${RECIPE.PRICE10}"));
			$("#price11").html(NumberFormat("${RECIPE.PRICE11}"));
			
			<c:if test="${RECIPE.PACKAGE1 ne null and RECIPE.PACKAGE1 ne ''}">
				$("#form3 #btnPackage1").attr("bom_cd", "${RECIPE.PACKAGE1}");
				$("#form3 #btnPackage1").attr("bom_nm", "${RECIPE.PACKAGE1_NM}");	
				$("#form3 #btnPackage1").attr("bom_tp", "${RECIPE.PACKAGE1_TP}");
				$("#form3 #btnPackage1").html("<span>" + "${RECIPE.PACKAGE1_NM}" + "</span>");
				
				getItemInfo("1", "${RECIPE.PACKAGE1}");
			</c:if>
			
			<c:if test="${RECIPE.PACKAGE2 ne null and RECIPE.PACKAGE2 ne ''}">
				$("#form3 #btnPackage2").attr("bom_cd", "${RECIPE.PACKAGE2}");
				$("#form3 #btnPackage2").attr("bom_nm", "${RECIPE.PACKAGE2_NM}");
				$("#form3 #btnPackage2").attr("bom_tp", "${RECIPE.PACKAGE2_TP}");
				$("#form3 #btnPackage2").html("<span>" + "${RECIPE.PACKAGE2_NM}" + "</span>");
				
				getItemInfo("2", "${RECIPE.PACKAGE2}");
			</c:if>
		
			<c:if test="${RECIPE.PACKAGE3 ne null and RECIPE.PACKAGE3 ne ''}">
				$("#form3 #btnPackage3").attr("bom_cd", "${RECIPE.PACKAGE3}");
				$("#form3 #btnPackage3").attr("bom_nm", "${RECIPE.PACKAGE3_NM}");	
				$("#form3 #btnPackage3").attr("bom_tp", "${RECIPE.PACKAGE3_TP}");
				$("#form3 #btnPackage3").html("<span>" + "${RECIPE.PACKAGE3_NM}" + "</span>");
				
				getItemInfo("3", "${RECIPE.PACKAGE3}");
			</c:if>
			
			selectStick("${RECIPE.INFO1}");
			$("#form3 #info2").val("${RECIPE.INFO2}");
		</c:if>
		<c:if test="${RECIPE.RECIPE_TP eq '004'}">
			$("#remark1").html("${RECIPE.REMARK1}");
			$("#remark2").html("${RECIPE.REMARK2}");
			$("#remark3").html("${RECIPE.REMARK3}");
			$("#remark4").html("${RECIPE.REMARK4}");
			$("#remark5").html("${RECIPE.REMARK5}");
			$("#remark6").html("${RECIPE.REMARK6}");
			$("#remark7").html("${RECIPE.REMARK7}");
			$("#remark8").html("${RECIPE.REMARK8}");
			$("#remark9").html("${RECIPE.REMARK9}");
			$("#remark10").html("${RECIPE.REMARK10}");
			$("#remark11").html("${RECIPE.REMARK11}");
			
			$("#price1").html(NumberFormat("${RECIPE.PRICE1}"));
			$("#price2").html(NumberFormat("${RECIPE.PRICE2}"));
			$("#price3").html(NumberFormat("${RECIPE.PRICE3}"));
			$("#price4").html(NumberFormat("${RECIPE.PRICE4}"));
			$("#price5").html(NumberFormat("${RECIPE.PRICE5}"));
			$("#price6").html(NumberFormat("${RECIPE.PRICE6}"));
			$("#price7").html(NumberFormat("${RECIPE.PRICE7}"));
			$("#price8").html(NumberFormat("${RECIPE.PRICE8}"));
			$("#price9").html(NumberFormat("${RECIPE.PRICE9}"));
			$("#price10").html(NumberFormat("${RECIPE.PRICE10}"));
			$("#price11").html(NumberFormat("${RECIPE.PRICE11}"));
			
			<c:if test="${RECIPE.PACKAGE1 ne null and RECIPE.PACKAGE1 ne ''}">
				$("#form4 #btnPackage1").attr("bom_cd", "${RECIPE.PACKAGE1}");
				$("#form4 #btnPackage1").attr("bom_nm", "${RECIPE.PACKAGE1_NM}");
				$("#form4 #btnPackage1").attr("bom_tp", "${RECIPE.PACKAGE1_TP}");
				$("#form4 #btnPackage1").html("<span>" + "${RECIPE.PACKAGE1_NM}" + "</span>");
				
				getItemInfo("1", "${RECIPE.PACKAGE1}");
			</c:if>
			
			<c:if test="${RECIPE.PACKAGE2 ne null and RECIPE.PACKAGE2 ne ''}">
				$("#form4 #btnPackage2").attr("bom_cd", "${RECIPE.PACKAGE2}");
				$("#form4 #btnPackage2").attr("bom_nm", "${RECIPE.PACKAGE2_NM}");	
				$("#form4 #btnPackage2").attr("bom_tp", "${RECIPE.PACKAGE2_TP}");
				$("#form4 #btnPackage2").html("<span>" + "${RECIPE.PACKAGE2_NM}" + "</span>");
				
				getItemInfo("2", "${RECIPE.PACKAGE2}");
			</c:if>
		
			<c:if test="${RECIPE.PACKAGE3 ne null and RECIPE.PACKAGE3 ne ''}">
				$("#form4 #btnPackage3").attr("bom_cd", "${RECIPE.PACKAGE3}");
				$("#form4 #btnPackage3").attr("bom_nm", "${RECIPE.PACKAGE3_NM}");	
				$("#form4 #btnPackage3").attr("bom_tp", "${RECIPE.PACKAGE3_TP}");
				$("#form4 #btnPackage3").html("<span>" + "${RECIPE.PACKAGE3_NM}" + "</span>");
				
				getItemInfo("3", "${RECIPE.PACKAGE3}");
			</c:if>
		</c:if>
				
		setMarking("${RECIPE.MARKING_TP}", "${RECIPE.MARKING_TEXT}");
				
		$('.chup1').keyup(function(e) {
			console.log("chup1");
			
			InpuOnlyFloat(this);
			
			calc();
		});	
		
		$('.chup1').click(function(e) {
			$(this).select();
		});	
	});
	
	function goSave(save_temp) {
		if(ajaxRunning) return;
		
		var doctor = GetValue("doctor");
		var patient_seq = GetValue("patient_seq");
		var remark = GetValue("remark");
		
		var recipe_tp = GetRadioValue("recipe_tp");
		var inflow = GetValue("inflow");		
		var recipe_nm = GetValue("recipe_nm");
		
		var howtomake = $.trim(sn$('#howtomake').summernote('code'));
		var howtoeat = $.trim(sn$('#howtoeat').summernote('code'));
		
		if(save_temp == "SAVE") {
			if(!doctor) {
		    	alert("처방의를 선택해 주세요.");
		    	$("#doctor").focus();
		    	return;
		    }	
			if(!patient_seq) {
		    	alert("환자를 선택해 주세요.");
		    	$("#patient_seq").focus();
		    	return;
		    }	
			if(!recipe_nm) {
		    	alert("처방명을 입력해 주세요.");
		    	$("#recipe_nm").focus();
		    	return;
		    }		
			
			var length = $("input:checkbox[name='drug_cd']").length;
			if(length == 0) {
				alert("적어도 하나 이상의 약재를 추가해 주세요.");
				return;
			}	
		}

		var info1 = "";
		var info2 = "";
		var info3 = "";
		var info4 = "";
		var info5 = "";
		var info6 = "";
		var info7 = "";
		var info8 = "";
		var info9 = "";
		var info10 = "";
		var info10_qntt = "";
		var info11 = "";
		
		var marking_tp = "";
		var marking_text = "";
		
		var package1 = "";
		var package2 = "";
		var package3 = "";
		var package4 = "";
		var vaccum = "N";
		
		var remark1 = "";
		var remark2 = "";
		var remark3 = "";
		var remark4 = "";
		var remark5 = "";
		var remark6 = "";
		var remark7 = "";
		var remark8 = "";
		var remark9 = "";
		var remark10 = "";
		var remark11 = "";
				
		var price1 = "";
		var price2 = "";
		var price3 = "";
		var price4 = "";
		var price5 = "";
		var price6 = "";
		var price7 = "";
		var price8 = "";
		var price9 = "";
		var price10 = "";
		var price11 = "";
		
		var drug_label_tp = "";	
		var drug_label_text = "";	
		var delivery_label_tp = "";	
		var delivery_label_text = "";	
		
		if(recipe_tp == "001") {
			info1 = $.trim($("#form1 #info1").val());
			info2 = $.trim($("#form1 #info2").val());
			info3 = $.trim($("#form1 #info3").val());
			info4 = $.trim($("#form1 #info4").val());
			info5 = $.trim($("#form1 #info5").html());
			info6 = $.trim($("#form1 #info6").html());
			info7 = $.trim($("#form1 #info7").val());
			info8 = $.trim($("#form1 #info8").val());
			info9 = $.trim($("#form1 #info9").val());			
			info10 = $("#form1 #info10").attr("code");
			info10_qntt = $("#form1 #info10").attr("value");		
			info11 = $.trim($("#form1 #info11").val());
			
			if(!info10) info10 = "";
			if(!info10_qntt) info10_qntt = "";			
			
			info6 = getOnlyFloat(info6);
			
			if(!info11) {
				alert("탕전시간을 입력해 주세요.");
				$("#form1 #info11").focus();
				return;
			}	
			
			marking_tp = $("#form1 #btnMarking").attr("marking_tp");
			marking_text = $("#form1 #btnMarking").attr("marking_text");			
			package1 = $("#form1 #btnPackage1").attr("bom_cd");
			package2 = $("#form1 #btnPackage2").attr("bom_cd");
			package3 = $("#form1 #btnPackage3").attr("bom_cd");
			
			vaccum = $("#form1 #btnPackage2").attr("vaccum");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			if(!vaccum) vaccum = "N";
			
			remark1 = $("#remark1").html();
			remark2 = $("#remark2").html();
			remark3 = $("#remark3").html();
			remark4 = $("#remark4").html();
			remark5 = $("#remark5").html();
			remark6 = $("#remark6").html();
			remark7 = $("#remark7").html();
			remark8 = $("#remark8").html();
			remark9 = $("#remark9").html();
			remark10 = $("#remark10").html();
			remark11 = $("#remark11").html();
			
			price1 = $("#price1").html();
			price2 = $("#price2").html();
			price3 = $("#price3").html();
			price4 = $("#price4").html();
			price5 = $("#price5").html();
			price6 = $("#price6").html();
			price7 = $("#price7").html();
			price8 = $("#price8").html();
			price9 = $("#price9").html();
			price10 = $("#price10").html();
			price11 = $("#price11").html();
			
			drug_label_tp = $.trim($("#form1 #drug_label_tp").val());
			drug_label_text = $.trim($("#form1 #drug_label_text").val());
			delivery_label_tp = $.trim($("#form1 #delivery_label_tp").val());
			delivery_label_text = $.trim($("#form1 #delivery_label_text").val());
		}
		else if(recipe_tp == "002") {
			info1 = $.trim($("#form2 #info1").val());
			info2 = $.trim($("#form2 #info2").val());
			info3 = $.trim($("#form2 #info3").val());
			info4 = $.trim($("#form2 #info4").val());
			info5 = $.trim($("#form2 #info5").val());			
			info6 = $.trim($("#form2 #info6").html());
			info7 = $.trim($("#form2 #info7").html());
			info8 = $.trim($("#form2 #info8").html());
			
			info6 = getOnlyFloat(info6);
			info7 = getOnlyFloat(info7);
			info8 = getOnlyFloat(info8);	
			
			marking_tp = $("#form2 #btnMarking").attr("marking_tp");
			marking_text = $("#form2 #btnMarking").attr("marking_text");			
			package1 = $("#form2 #btnPackage1").attr("bom_cd");
			package2 = $("#form2 #btnPackage2").attr("bom_cd");
			package3 = $("#form2 #btnPackage3").attr("bom_cd");
			package4 = $("#form2 #btnPackage4").attr("bom_cd");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			if(!package4) package4 = "";
			
			remark1 = $("#remark1").html();
			remark2 = $("#remark2").html();
			remark3 = $("#remark3").html();
			remark4 = $("#remark4").html();
			remark5 = $("#remark5").html();
			remark6 = $("#remark6").html();
			remark7 = $("#remark7").html();
			remark8 = $("#remark8").html();
			remark9 = $("#remark9").html();
			remark10 = $("#remark10").html();
			remark11 = $("#remark11").html();
			
			price1 = $("#price1").html();
			price2 = $("#price2").html();
			price3 = $("#price3").html();
			price4 = $("#price4").html();
			price5 = $("#price5").html();
			price6 = $("#price6").html();			
			price7 = $("#price7").html();
			price8 = $("#price8").html();
			price9 = $("#price9").html();
			price10 = $("#price10").html();
			price11 = $("#price11").html();
			
			drug_label_tp = $.trim($("#form2 #drug_label_tp").val());
			drug_label_text = $.trim($("#form2 #drug_label_text").val());
			delivery_label_tp = $.trim($("#form2 #delivery_label_tp").val());
			delivery_label_text = $.trim($("#form2 #delivery_label_text").val());
		}
		else if(recipe_tp == "003") {
			info1 = $.trim($("#form3 #info1").val());
			info2 = $.trim($("#form3 #info2").val());
			
			marking_tp = $("#form3 #btnMarking").attr("marking_tp");
			marking_text = $("#form3 #btnMarking").attr("marking_text");			
			package1 = $("#form3 #btnPackage1").attr("bom_cd");
			package2 = $("#form3 #btnPackage2").attr("bom_cd");
			package3 = $("#form3 #btnPackage3").attr("bom_cd");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			
			remark1 = $("#remark1").html();
			remark2 = $("#remark2").html();
			remark3 = $("#remark3").html();
			remark4 = $("#remark4").html();
			remark5 = $("#remark5").html();
			remark6 = $("#remark6").html();
			remark7 = $("#remark7").html();
			remark8 = $("#remark8").html();
			remark9 = $("#remark9").html();
			remark10 = $("#remark10").html();
			remark11 = $("#remark11").html();
			
			price1 = $("#price1").html();
			price2 = $("#price2").html();
			price3 = $("#price3").html();
			price4 = $("#price4").html();
			price5 = $("#price5").html();
			price6 = $("#price6").html();
			price7 = $("#price7").html();
			price8 = $("#price8").html();
			price9 = $("#price9").html();
			price10 = $("#price10").html();
			price11 = $("#price11").html();
			
			drug_label_tp = $.trim($("#form3 #drug_label_tp").val());
			drug_label_text = $.trim($("#form3 #drug_label_text").val());
			delivery_label_tp = $.trim($("#form3 #delivery_label_tp").val());
			delivery_label_text = $.trim($("#form3 #delivery_label_text").val());	
		}	
		else if(recipe_tp == "004") {
			info1 = $.trim($("#form4 #info1").val());
			info2 = $.trim($("#form4 #info2").val());
			info3 = $.trim($("#form4 #info3").val());
			info4 = $.trim($("#form4 #info4").val());
			info5 = $.trim($("#form4 #info5").html());
			info6 = $.trim($("#form4 #info6").html());
			info7 = $.trim($("#form4 #info7").val());
			info8 = $.trim($("#form4 #info8").val());
			info9 = $.trim($("#form4 #info9").val());			
			info10 = $("#form4 #info10").attr("code");
			info10_qntt = $("#form4 #info10").attr("value");		
			info11 = $.trim($("#form4 #info11").val());
			
			if(!info10) info10 = "";
			if(!info10_qntt) info10_qntt = "";			
			
			info6 = getOnlyFloat(info6);
			
			if(!info11) {
				alert("탕전시간을 입력해 주세요.");
				$("#form4 #info11").focus();
				return;
			}	
			
			marking_tp = $("#form4 #btnMarking").attr("marking_tp");
			marking_text = $("#form4 #btnMarking").attr("marking_text");			
			package1 = $("#form4 #btnPackage1").attr("bom_cd");
			package2 = $("#form4 #btnPackage2").attr("bom_cd");
			package3 = $("#form4 #btnPackage3").attr("bom_cd");
			
			vaccum = $("#form4 #btnPackage2").attr("vaccum");
			
			if(!marking_tp) marking_tp = "";
			if(!marking_text) marking_text = "";
			if(!package1) package1 = "";
			if(!package2) package2 = "";
			if(!package3) package3 = "";
			if(!vaccum) vaccum = "N";
			
			remark1 = $("#remark1").html();
			remark2 = $("#remark2").html();
			remark3 = $("#remark3").html();
			remark4 = $("#remark4").html();
			remark5 = $("#remark5").html();
			remark6 = $("#remark6").html();
			remark7 = $("#remark7").html();
			remark8 = $("#remark8").html();
			remark9 = $("#remark9").html();
			remark10 = $("#remark10").html();
			remark11 = $("#remark11").html();
			
			price1 = $("#price1").html();
			price2 = $("#price2").html();
			price3 = $("#price3").html();
			price4 = $("#price4").html();
			price5 = $("#price5").html();
			price6 = $("#price6").html();
			price7 = $("#price7").html();
			price8 = $("#price8").html();
			price9 = $("#price9").html();
			price10 = $("#price10").html();
			price11 = $("#price11").html();
			
			drug_label_tp = $.trim($("#form4 #drug_label_tp").val());
			drug_label_text = $.trim($("#form4 #drug_label_text").val());
			delivery_label_tp = $.trim($("#form4 #delivery_label_tp").val());
			delivery_label_text = $.trim($("#form4 #delivery_label_text").val());
		}
		
		if(!remark1) remark1 = "";
		if(!remark2) remark2 = "";
		if(!remark3) remark3 = "";
		if(!remark4) remark4 = "";
		if(!remark5) remark5 = "";
		if(!remark6) remark6 = "";
		if(!remark7) remark7 = "";
		if(!remark8) remark8 = "";
		if(!remark9) remark9 = "";
		if(!remark10) remark10 = "";
		if(!remark11) remark11 = "";
		
		if(!price1) price1 = "";
		if(!price2) price2 = "";
		if(!price3) price3 = "";
		if(!price4) price4 = "";
		if(!price5) price5 = "";
		if(!price6) price6 = "";
		if(!price7) price7 = "";
		if(!price8) price8 = "";
		if(!price9) price9 = "";
		if(!price10) price10 = "";
		if(!price11) price11 = "";	
	   	    
		if(drug_label_tp != "005") {
			drug_label_text = "";	
		}
		
		if(delivery_label_tp != "005") {
			delivery_label_text = "";	
		}
		
	    var formData = new FormData();	    
	    formData.append("SAVE_TEMP", save_temp);
	    formData.append("RE_HOTPOT", GetValue("re_hotpot"));
	    formData.append("RECIPE_SEQ", RECIPE_SEQ);    
	    formData.append("DOCTOR", doctor);
	    formData.append("PATIENT_SEQ", patient_seq);	
	    formData.append("REMARK", remark);
		formData.append("RECIPE_TP", recipe_tp);
		formData.append("INFLOW", inflow);
		formData.append("RECIPE_NM", recipe_nm);
		formData.append("INFO1", info1);
		formData.append("INFO2", info2);
		formData.append("INFO3", info3);
		formData.append("INFO4", info4);
		formData.append("INFO5", info5);
		formData.append("INFO6", info6);
		formData.append("INFO7", info7);
		formData.append("INFO8", info8);
		formData.append("INFO9", info9);
		formData.append("INFO10", info10);
		formData.append("INFO10_QNTT", info10_qntt);
		formData.append("INFO11", info11);
		
		formData.append("MARKING_TP", marking_tp);
		formData.append("MARKING_TEXT", marking_text);
		formData.append("PACKAGE1", package1);
		formData.append("PACKAGE2", package2);
		formData.append("PACKAGE3", package3);
		formData.append("PACKAGE4", package4);
		formData.append("VACCUM", vaccum);
		
		formData.append("REMARK1", remark1);
		formData.append("REMARK2", remark2);
		formData.append("REMARK3", remark3);
		formData.append("REMARK4", remark4);
		formData.append("REMARK5", remark5);
		formData.append("REMARK6", remark6);
		formData.append("REMARK7", remark7);
		formData.append("REMARK8", remark8);
		formData.append("REMARK9", remark9);
		formData.append("REMARK10", remark10);
		formData.append("REMARK11", remark11);
		
		formData.append("PRICE1", price1);
		formData.append("PRICE2", price2);
		formData.append("PRICE3", price3);
		formData.append("PRICE4", price4);
		formData.append("PRICE5", price5);
		formData.append("PRICE6", price6);
		formData.append("PRICE7", price7);
		formData.append("PRICE8", price8);
		formData.append("PRICE9", price9);
		formData.append("PRICE10", price10);	
		formData.append("PRICE11", price11);
		
		formData.append("DRUG_LABEL_TP", drug_label_tp);
		formData.append("DRUG_LABEL_TEXT", drug_label_text);
		formData.append("DELIVERY_LABEL_TP", delivery_label_tp);
		formData.append("DELIVERY_LABEL_TEXT", delivery_label_text);
		
		formData.append("HOWTOMAKE", howtomake);
		formData.append("HOWTOEAT", howtoeat);
		
		length = $(".make_file").length;
		for(var i = 0; i < length; i++) {
			formData.append("LIST_HOWTOMAKE_FILENAME", $(".make_file").eq(i).data("name"));
			formData.append("LIST_HOWTOMAKE_FILEPATH", $(".make_file").eq(i).data("path"));
		}
		
		length = $(".eat_file").length;		
		for(var i = 0; i < length; i++) {
			formData.append("LIST_HOWTOEAT_FILENAME", $(".eat_file").eq(i).data("name"));
			formData.append("LIST_HOWTOEAT_FILEPATH", $(".eat_file").eq(i).data("path"));
		}	
		
		var total_chup = getOnlyFloat($("#sum_total_chup").html());
		var total_qntt = getOnlyFloat($("#sum_total_qntt").html());
		var total_price = getOnlyNumber($("#sum_total_price").html());
		var amount = getOnlyNumber($("#amount").html());		
		
		formData.append("TOTAL_CHUP", total_chup);
		formData.append("TOTAL_QNTT", total_qntt);
		formData.append("TOTAL_PRICE", total_price);
		formData.append("AMOUNT", amount);
		
		if(recipe_tp == "002") {
			if(parseInt(total_qntt) < 600) {
				alert("제환은 600g 이상만 처방이 가능합니다.");
				return;
			}
		}
		
		var drug_names = "";		
		
		var drug_length = $("input:checkbox[name='drug_cd']").length;
		if(drug_length == 0) {
			alert("적어도 하나 이상의 약재를 추가해 주세요.");
			return;
		}
		
		for(var i = 0; i < drug_length; i++) {
			var drug_cd = $("input:checkbox[name='drug_cd']").eq(i).val();
			var drug_nm = $("input:checkbox[name='drug_cd']").eq(i).data("name");
			var stock_qntt = $("input:checkbox[name='drug_cd']").eq(i).data("stock");
			var price = $("input:checkbox[name='drug_cd']").eq(i).data("price");
			var origin = $("input:checkbox[name='drug_cd']").eq(i).data("origin");
			
			var total_qntt = getOnlyFloat($(".total_qntt").eq(i).html());
			var total_price = getOnlyNumber($(".total_price").eq(i).html());
						
			var chup1 = $("input[name='chup1']").eq(i).val();
			var turn = $("select[name='turn']").eq(i).val();
			
			if(!chup1) {
				alert("1첩량을 입력해 주세요.");
				$("input[name='chup1']").eq(i).focus();
				return;
			}
			
			if(parseInt(chup1) <= 0) {
				alert("1첩량을 입력해 주세요.");
				$("input[name='chup1']").eq(i).focus();
				return;
			}
			
			if(parseInt(stock_qntt) < parseInt(total_qntt)) {
				alert("재고량보다 주문량이 많습니다.");
				$("input[name='chup1']").eq(i).focus();
				return;
			}
			
			formData.append("LIST_DRUG_CD", drug_cd);
			formData.append("LIST_DRUG_NM", drug_nm);
			formData.append("LIST_TURN", turn);
			formData.append("LIST_CHUP1", chup1);	
			formData.append("LIST_ORIGIN", origin);	
			formData.append("LIST_PRICE", price);
			formData.append("LIST_TOTAL_QNTT", total_qntt);
			formData.append("LIST_TOTAL_PRICE", total_price);
			
			if(drug_names != "") {
				drug_names += ",";
			}
			
			drug_names += drug_nm;
		}
		
		formData.append("DRUG_NM", drug_names);
		
		if(save_temp == "SAVE") {
			if(!marking_tp) {
				alert("마킹유형을 선택해 주세요.");
				return;
			}
		}
		
		if(save_temp == "SAVE") {
			if(!package1) {
				if(recipe_tp == "001") {
					alert("파우치 포장정보를 선택해 주세요.");
					return;
				}
				else if(recipe_tp == "002") {
					if(info3 == "004") {
						alert("내지 포장정보를 선택해 주세요.");
						return;
					}				
				}
				else if(recipe_tp == "003") {
					alert("스틱 포장정보를 선택해 주세요.");
					return;
				}
				else if(recipe_tp == "004") {
					alert("파우치 포장정보를 선택해 주세요.");
					return;
				}
			}
			
			if(!package2) {
				if(recipe_tp == "002") {
					if(info3 != "004") {
						alert("소포장 포장정보를 선택해 주세요.");	
						return;
					}				
				}
			}		
			
		    if(!confirm("입력하신 내용을 등록 하시겠습니까?")) {
		    	return;
		    }
		}
	    
	    var url = "/recipe/write";
	    
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {	
	            	RECIPE_SEQ = response.message;
	            	
	            	if(save_temp == "TEMP") {
	            		alert("임시 저장 되었습니다.");
	            		location.href = "/recipe/list_temp" + window.location.hash;
	            	}
	            	else if(save_temp == "SAVE") {		
	            		alert("배송지 입력으로 이동합니다.");
	            		location.href = "/recipe/update/" + RECIPE_SEQ + "/" + window.location.hash;	
	            	}	            	
	            }
	            else {
	            	alert(response.message);
	            }
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}	
</script>

<body>
<div id="wrap">

<!-- 컨텐츠 시작 -->
<%@ include file="/WEB-INF/views/include/path.jsp" %>

<div id="container_y" class="container_y">
	<div class="inr-c">
		<%@ include file="/WEB-INF/views/include/sidebar.jsp" %>

		<section class="contents_y">
			<header class="hd_tit1">
				<h2 class="h_tit1 f-en">처방하기</h2>
			</header>

			<!-- 좌측데이터 -->
			<div class="lft_c">
				<h3 class="h_tit2">처방정보</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col class="td1">
							<col class="th1">
							<col class="td1">
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>날짜</th>
								<td class="ta-c">${RECIPE.CREATED_AT}</td>
								<th>한의원명</th>
								<td class="ta-c">${SESSION.COMPANY}</td>
								<th>처방의 <span class="i_emp">*</span></th>
								<td class="ta-c">
									<select name="doctor" id="doctor" class="select1 w100p">
										<option value="">선택하세요</option>
										<c:forEach var="i" begin="1" end="${LIST_DOCTORS.size()}">
											<option value="${LIST_DOCTORS.get(i - 1)}" <c:if test="${RECIPE.DOCTOR eq LIST_DOCTORS.get(i - 1)}">selected</c:if>>${LIST_DOCTORS.get(i - 1)}</option>
										</c:forEach>
									</select>
									
									<button type="button" id="btnSearchHospital" price_class="${SESSION.CLASS}" style="display:none"></button>									
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<h3 class="h_tit2">환자요약</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view ty2">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>환자명</th>
								<td>
									<input type="text" name="name" id="name" readOnly class="inp_txt w100p" value="${PATIENT.NAME}" placeholder="우측 [환자 정보]에서 환자를 검색하여 선택 해 주세요, 신규 환자인 경우 [등록]버튼으로 추가 해 주세요." />
									<input type="hidden" name="patient_seq" id="patient_seq" value="${PATIENT.PATIENT_SEQ}" />										
								</td>
							</tr>
							<tr>
								<th>처방메모</th>
								<td><textarea name="remark" id="remark" class="textarea w100p" placeholder="환자 특이사항 또는 기타사항을 작성 해 주세요. (처방메모는 탕전실에 전달되지 않습니다.)">${RECIPE.REMARK}</textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
			
				<h3 class="h_tit2">처방전</h3>
				<div class="tbl_basic pr-mb1">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>처방구분</th>
								<td><p>${RECIPE.RECIPE_TP_NM}</p>
									<c:forEach var="entry" items="${MAP_RECIPE_TP}">
										<input type="radio" name="recipe_tp" value="${entry.key}" <c:if test="${entry.key == RECIPE.RECIPE_TP}">checked</c:if> style="display:none" />
									</c:forEach>
								</td>
								<th>유입 구분</th>
								<td>
									<select name="inflow" id="inflow" class="select1 w100p">
										<c:forEach var="i" begin="1" end="${LIST_INFLOW_TP.size()}">
											<option value="${LIST_INFLOW_TP.get(i - 1).SUB_CD}" <c:if test="${LIST_INFLOW_TP.get(i - 1).SUB_CD eq VO.INFLOW}">selected</c:if>>${LIST_INFLOW_TP.get(i - 1).SUB_NM_KR}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th>처방명 <span class="i_emp">*</span></th>
								<td colspan="3">
									<div class="inp_btn">
										<input type="text" name="recipe_nm" id="recipe_nm" class="inp_txt w100p" value="${RECIPE.RECIPE_NM}" placeholder="처방명을 입력해주세요." />
										<button type="button" id="btnInit" class="btn-pk n white"><span>초기화</span></button>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table class="list small">
						<colgroup>
							<col class="chk" />
							<col class="num" />
							<col />
							<col class="size1" />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
						</colgroup>
						<thead>
							<tr>
								<th><label class="inp_checkbox"><input type="checkbox" onClick="allCheck(this)"><span class="d"></span></label></th>
								<th>번호</th>
								<th>약재명</th>
								<th>유형</th>
								<th>원산지</th>
								<th>가격</th>
								<th>재고수량</th>
								<th>1첩량</th>
								<th>총 용량</th>
								<th>약재비</th>
							</tr>
						</thead>
						<tbody id="tbody">
							<c:forEach var="i" begin="1" end="${LIST_RECIPE.size()}">
								<tr>
									<td><label class='inp_checkbox'><input type='checkbox' class='chk' name='drug_cd' value='${LIST_RECIPE.get(i - 1).DRUG_CD}' 
									data-herbs='${LIST_RECIPE.get(i - 1).HERBS_CD}'
									data-name='${LIST_RECIPE.get(i - 1).DRUG_NM}' 
									data-rate='${LIST_RECIPE.get(i - 1).ABSORPTION_RATE}' 
									data-price='${LIST_RECIPE.get(i - 1).PRICE}' 
									data-origin='${LIST_RECIPE.get(i - 1).ORIGIN}'
									data-stock='${LIST_RECIPE.get(i - 1).STOCK_QNTT}' /><span class='d'></span></label></td>
									<td class='no'></td>
									<td>${LIST_RECIPE.get(i - 1).DRUG_NM}<span class='poison'></span>
										<c:if test="${fn:indexOf(POISON,LIST_RECIPE.get(i - 1).HERBS_CD) > -1}">
											<button type="button" class="btn_cir1 ty2" onclick="toastMsg('독성 약재가 있습니다','ty2')"><span>독</span></button>
										</c:if>
									</td>
									<td>
										<select name='turn' class='select1 w100p'>
											<option value='1' <c:if test="${LIST_RECIPE.get(i - 1).TURN eq '1'}">selected</c:if>>선전</option>
											<option value='2' <c:if test="${LIST_RECIPE.get(i - 1).TURN eq '2'}">selected</c:if>>일반</option>
											<option value='3' <c:if test="${LIST_RECIPE.get(i - 1).TURN eq '3'}">selected</c:if>>후하</option>
										</select>
									</td>
									<td>${LIST_RECIPE.get(i - 1).ORIGIN}</td>
									<td class='ta-r price'><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).PRICE}" />원</td>
									<td class='ta-r'><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).STOCK_QNTT}" />g</td>
									<td class='ta-r'><div class='fx'><input type='text' name='chup1' class='inp_txt chup1' style="width:80%" value='${LIST_RECIPE.get(i - 1).CHUP1}'>g</div></td>
									<td class='ta-r total_qntt'><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).TOTAL_QNTT}" /></td>
									<td class='ta-r total_price'><mask:display type="NUMBER" value="${LIST_RECIPE.get(i - 1).TOTAL_PRICE}" /></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7" class="ta-l">
									<button type="button" id="btnDrugDelete" class="btn-pk s blue"><span>선택삭제</span></button>
								</td>
								<td class="bg ta-r"><span id="sum_total_chup"><mask:display type="NUMBER" value="${RECIPE.TOTAL_CHUP}" /></span>g</td>
								<td class="bg ta-r"><span id="sum_total_qntt"><mask:display type="NUMBER" value="${RECIPE.TOTAL_QNTT}" /></span>g</td>
								<td class="bg ta-r"><strong class="c-red fz-b1"><span id="sum_total_price"><mask:display type="NUMBER" value="${RECIPE.TOTAL_PRICE}" /></span>원</strong></td>
							</tr>
						</tfoot>
					</table>
				</div>

				<form id="form1" <c:if test="${RECIPE.RECIPE_TP ne '001'}">style="display:none"</c:if>>				
					<h3 class="h_tit2">전탕 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list small">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>팩수</th>
									<th>팩 용량</th>
									<th>특수탕전</th>
									<th>탕전법</th>
									<th>탕전 물양</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="calc()">											
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<c:forEach var="i" begin="1" end="2000">
														<option value="${i}" <c:if test="${i == RECIPE.INFO1}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
													</c:forEach>	
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="2000">
														<option value="${i}" <c:if test="${i == 20}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
													</c:forEach>	
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<c:forEach var="i" begin="1" end="120">
														<option value="${i}" <c:if test="${i == RECIPE.INFO2}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 팩</option>
													</c:forEach>	
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="120">
														<option value="${i}" <c:if test="${i == 40}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 팩</option>
													</c:forEach>	
												</c:otherwise>
											</c:choose>											
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info3" id="info3" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<c:forEach var="i" begin="50" end="130">
														<option value="${i}" <c:if test="${i == RECIPE.INFO3}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="50" end="130">
														<option value="${i}" <c:if test="${i == 120}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>	
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info4" id="info4" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<c:forEach var="i" begin="1" end="${LIST_SPECIAL_TP.size()}">
														<option value="${LIST_SPECIAL_TP.get(i - 1).SUB_CD}" data-value='${LIST_SPECIAL_TP.get(i - 1).SUB_VALUE_KR}' <c:if test="${LIST_SPECIAL_TP.get(i - 1).SUB_CD eq RECIPE.INFO4}">selected</c:if>>${LIST_SPECIAL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_SPECIAL_TP.size()}">
														<option value="${LIST_SPECIAL_TP.get(i - 1).SUB_CD}" data-value='${LIST_SPECIAL_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_SPECIAL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td><span id="info5">표준(선무후문)</span></td>
									<td><span id="info6"><mask:display type="NUMBER" value="${RECIPE.INFO6}" /></span> mL</td>
								</tr>
								<tr>
									<th>향미제(초코)</th>
									<th>앰플(자하거)</th>									
									<th>녹용별전</th>
									<th>주수상반</th>
									<th>재탕전</th>
									<th>탕전시간(분)</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info7" id="info7">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<option value="N" <c:if test="${RECIPE.INFO7 eq 'N'}">selected</c:if>>선택없음</option>
													<option value="Y" <c:if test="${RECIPE.INFO7 eq 'Y'}">selected</c:if>>선택</option>
												</c:when>
												<c:otherwise>
													<option value="N">선택없음</option>
													<option value="Y">선택</option>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info8" id="info8">
											<option value="">선택없음</option>	
											
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<c:forEach var="i" begin="10" end="200" step="10">
														<option value="${i}" <c:if test="${i == RECIPE.INFO8}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="10" end="200" step="10">
														<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info9" id="info9">
											<option value="">선택없음</option>	
											
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">
													<c:forEach var="i" begin="10" end="200" step="10">
														<option value="${i}" <c:if test="${i == RECIPE.INFO9}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="10" end="200" step="10">
														<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>											
										<c:choose>
											<c:when test="${RECIPE.RECIPE_TP eq '001'}">											
												<div class="btn-pk ss blue3 w100p" id="info10" code='${RECIPE.INFO10}' name='${RECIPE.INFO10_NM}' value='${RECIPE.INFO10_QNTT}' price='${RECIPE.INFO10_PRICE}' onClick="showDrink()"><c:if test="${RECIPE.INFO10 ne ''}">${RECIPE.INFO10_NM}/${RECIPE.INFO10_QNTT}mL</c:if><c:if test="${RECIPE.INFO10 eq ''}">선택없음</c:if></div>
											</c:when>
											<c:otherwise>
												<div class="btn-pk ss blue3 w100p" id="info10" onClick="showDrink()">선택없음</div>
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<select class="select1 w100p" name="re_hotpot" id="re_hotpot" onChange="calc()">
											<option value="N" <c:if test="${RECIPE.RE_HOTPOT eq 'N'}">selected</c:if>>선택없음</option>
											<option value="Y" <c:if test="${RECIPE.RE_HOTPOT eq 'Y'}">selected</c:if>>재탕전</option>
										</select>
									</td>
									<td><input type="text" name="info11" id="info11" class="inp_txt w100p number" value="${RECIPE.INFO11}" maxlength="3" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>파우치</th>
									<th>탕약박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="view small" style="margin-top: -1px;">
							<colgroup>
								<col class="th1">
								<col class="td1">
								<col>
								<col class="th1">
								<col class="td1">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<th>탕약박스 <br>부착라벨</th>
									<td>
										<select name="drug_label_tp" id="drug_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DRUG_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>												
										</select>
									</td>
									<td>
										<input type="text" name="drug_label_text" id="drug_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" value='<c:if test="${RECIPE.RECIPE_TP eq '001' or RECIPE.RECIPE_TP eq '004'}">${RECIPE.DRUG_LABEL_TEXT}</c:if>' maxlength="14" />
									</td>
									<th>택배박스 <br>부착라벨</th>
									<td>
										<select name="delivery_label_tp" id="delivery_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '001'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DELIVERY_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<input type="text" name="delivery_label_text" id="delivery_label_text" class="inp_txt w100p" value='<c:if test="${RECIPE.RECIPE_TP eq '001' or RECIPE.RECIPE_TP eq '004'}">${RECIPE.DELIVERY_LABEL_TEXT}</c:if>' maxlength="8" />
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
				
				<form id="form2" <c:if test="${RECIPE.RECIPE_TP ne '002'}">style="display:none"</c:if>>		
					<h3 class="h_tit2">제환 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list small">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>농축</th>
									<th>제형</th>
									<th>부형제</th>
									<th>분말도</th>
									<th>제분손실량</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="calc()">										
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">						
													<c:forEach var="i" begin="1" end="2000">
														<option value="${i}" <c:if test="${i == RECIPE.INFO1}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
													</c:forEach>	
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="2000">
														<option value="${i}" <c:if test="${i == 20}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
													</c:forEach>	
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">	
													<c:forEach var="i" begin="1" end="${LIST_PRESS_TP.size()}">
														<option value="${LIST_PRESS_TP.get(i - 1).SUB_CD}" data-value='${LIST_PRESS_TP.get(i - 1).SUB_VALUE_KR}' <c:if test="${RECIPE.INFO2 eq LIST_PRESS_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_PRESS_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>	
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_PRESS_TP.size()}">
														<option value="${LIST_PRESS_TP.get(i - 1).SUB_CD}" data-value='${LIST_PRESS_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_PRESS_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info3" id="info3" onChange="showBox002(this.value)">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">						
													<c:forEach var="i" begin="1" end="${LIST_CIRCLE_SIZE.size()}">
														<option value="${LIST_CIRCLE_SIZE.get(i - 1).SUB_CD}" data-value='${LIST_CIRCLE_SIZE.get(i - 1).SUB_VALUE_KR}' <c:if test="${RECIPE.INFO3 eq LIST_CIRCLE_SIZE.get(i - 1).SUB_CD}">selected</c:if>>${LIST_CIRCLE_SIZE.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_CIRCLE_SIZE.size()}">
														<option value="${LIST_CIRCLE_SIZE.get(i - 1).SUB_CD}" data-value='${LIST_CIRCLE_SIZE.get(i - 1).SUB_VALUE_KR}'>${LIST_CIRCLE_SIZE.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info4" id="info4" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">						
													<c:forEach var="i" begin="1" end="${LIST_DOUGH_TP.size()}">
														<option value="${LIST_DOUGH_TP.get(i - 1).SUB_CD}" data-value='${LIST_DOUGH_TP.get(i - 1).SUB_VALUE_KR}' <c:if test="${RECIPE.INFO4 eq LIST_DOUGH_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_DOUGH_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_DOUGH_TP.size()}">
														<option value="${LIST_DOUGH_TP.get(i - 1).SUB_CD}" data-value='${LIST_DOUGH_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_DOUGH_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>	
										<select class="select1 w100p" name="info5" id="info5" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">	
													<c:forEach var="i" begin="1" end="${LIST_POWDER_TP.size()}">
														<option value="${LIST_POWDER_TP.get(i - 1).SUB_CD}" data-value='${LIST_POWDER_TP.get(i - 1).SUB_VALUE_KR}' <c:if test="${RECIPE.INFO5 eq LIST_POWDER_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_POWDER_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>	
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_POWDER_TP.size()}">
														<option value="${LIST_POWDER_TP.get(i - 1).SUB_CD}" data-value='${LIST_POWDER_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_POWDER_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>						
									</td>
									<td><span id="info6"><mask:display type="NUMBER" value="${RECIPE.INFO6}" /></span> g</td>
								</tr>
								<tr>
									<th>제환손실량</th>
									<th>부형제량</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
								<tr>
									<td><span id="info7"><mask:display type="NUMBER" value="${RECIPE.INFO7}" /></span> g</td>
									<td><span id="info8"><mask:display type="NUMBER" value="${RECIPE.INFO8}" /></span> g</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>내지</th>
									<th>소포장</th>
									<th>포장박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage4" onclick="showPackage('4')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package4">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>	
						<table class="view small" style="margin-top: -1px;">
							<colgroup>
								<col class="th1">
								<col class="td1">
								<col>
								<col class="th1">
								<col class="td1">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<th>탕약박스 <br>부착라벨</th>
									<td>
										<select name="drug_label_tp" id="drug_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DRUG_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<input type="text" name="drug_label_text" id="drug_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" value='<c:if test="${RECIPE.RECIPE_TP eq '002'}">${RECIPE.DRUG_LABEL_TEXT}</c:if>' maxlength="14" />
									</td>
									<th>택배박스 <br>부착라벨</th>
									<td>
										<select name="delivery_label_tp" id="delivery_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '002'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DELIVERY_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<input type="text" name="delivery_label_text" id="delivery_label_text" class="inp_txt w100p" value='<c:if test="${RECIPE.RECIPE_TP eq '002'}">${RECIPE.DELIVERY_LABEL_TEXT}</c:if>' maxlength="8" />
									</td>
								</tr>
							</tbody>
						</table>				
					</div>
				</form>
				
				
				<form id="form3" <c:if test="${RECIPE.RECIPE_TP ne '003'}">style="display:none"</c:if>>									
					<h3 class="h_tit2">연조엑스 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>스틱수</th>
									<td colspan="2" rowspan="2" class="ta-l">
										&nbsp;&nbsp;&nbsp;&nbsp;* 탕전비 : 1탕전 50첩 기준량  3,750g   단가 : 10,000원<br />
										&nbsp;&nbsp;&nbsp;&nbsp;(ex, 200첩 : 50첩 x 4탕 = 40,000원  )<br />
										&nbsp;&nbsp;&nbsp;&nbsp;* 스틱포장비(살균처리) : 1포당 300원<br /> 
										&nbsp;&nbsp;&nbsp;&nbsp;* 스틱, 케이스 가격별도<br />
									</td>
								</tr>								
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="selectStick(this.value); calc()">
											<c:forEach var="i" begin="100" end="400" step="100">
												<option value="${i}" <c:if test="${RECIPE.INFO1 == i}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 첩</option>
											</c:forEach>	
										</select>
									</td>
									<td>	
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>스틱</th>
									<th>케이스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="view small" style="margin-top: -1px;">
							<colgroup>
								<col class="th1">
								<col class="td1">
								<col>
								<col class="th1">
								<col class="td1">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<th>탕약박스 <br>부착라벨</th>
									<td>
										<select name="drug_label_tp" id="drug_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '003'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DRUG_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<input type="text" name="drug_label_text" id="drug_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" value='<c:if test="${RECIPE.RECIPE_TP eq '003'}">${RECIPE.DRUG_LABEL_TEXT}</c:if>' maxlength="14" />
									</td>
									<th>택배박스 <br>부착라벨</th>
									<td>
										<select name="delivery_label_tp" id="delivery_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '003'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DELIVERY_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<input type="text" name="delivery_label_text" id="delivery_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" value='<c:if test="${RECIPE.RECIPE_TP eq '003'}">${RECIPE.DELIVERY_LABEL_TEXT}</c:if>' maxlength="14" />
									</td>
								</tr>
							</tbody>
						</table>					
					</div>
				</form>
				
				<form id="form4" <c:if test="${RECIPE.RECIPE_TP ne '004'}">style="display:none"</c:if>>				
					<h3 class="h_tit2">전탕 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list small">
							<tbody>
								<tr>
									<th>첩수</th>
									<th>팩수</th>
									<th>팩 용량</th>
									<th>특수탕전</th>
									<th>탕전법</th>
									<th>탕전 물양</th>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info1" id="info1" onChange="calc()">											
											<c:choose>
												
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<option value="10" <c:if test="${10 == RECIPE.INFO1}">selected</c:if>>10 첩</option>
													<option value="20" <c:if test="${10 == RECIPE.INFO1}">selected</c:if>>20 첩</option>	
												</c:when>
												<c:otherwise>
													<option value="10">10 첩</option>
													<option value="20">20 첩</option>	
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info2" id="info2" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<c:forEach var="i" begin="1" end="120">
														<option value="${i}" <c:if test="${i == RECIPE.INFO2}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 팩</option>
													</c:forEach>	
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="120">
														<option value="${i}" <c:if test="${i == 40}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> 팩</option>
													</c:forEach>	
												</c:otherwise>
											</c:choose>											
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info3" id="info3" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<c:forEach var="i" begin="50" end="130">
														<option value="${i}" <c:if test="${i == RECIPE.INFO3}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="50" end="130">
														<option value="${i}" <c:if test="${i == 120}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>	
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info4" id="info4" onChange="calc()">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<c:forEach var="i" begin="1" end="${LIST_SPECIAL_TP.size()}">
														<option value="${LIST_SPECIAL_TP.get(i - 1).SUB_CD}" data-value='${LIST_SPECIAL_TP.get(i - 1).SUB_VALUE_KR}' <c:if test="${LIST_SPECIAL_TP.get(i - 1).SUB_CD eq RECIPE.INFO4}">selected</c:if>>${LIST_SPECIAL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_SPECIAL_TP.size()}">
														<option value="${LIST_SPECIAL_TP.get(i - 1).SUB_CD}" data-value='${LIST_SPECIAL_TP.get(i - 1).SUB_VALUE_KR}'>${LIST_SPECIAL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td><span id="info5">표준(선무후문)</span></td>
									<td><span id="info6"><mask:display type="NUMBER" value="${RECIPE.INFO6}" /></span> mL</td>
								</tr>
								<tr>
									<th>향미제(초코)</th>
									<th>앰플(자하거)</th>									
									<th>녹용별전</th>
									<th>주수상반</th>
									<th>탕전시간(분)</th>
									<td></td>
								</tr>
								<tr>
									<td>
										<select class="select1 w100p" name="info7" id="info7">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<option value="N" <c:if test="${RECIPE.INFO7 eq 'N'}">selected</c:if>>선택없음</option>
													<option value="Y" <c:if test="${RECIPE.INFO7 eq 'Y'}">selected</c:if>>선택</option>
												</c:when>
												<c:otherwise>
													<option value="N">선택없음</option>
													<option value="Y">선택</option>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info8" id="info8">
											<option value="">선택없음</option>	
											
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<c:forEach var="i" begin="10" end="20" step="10">
														<option value="${i}" <c:if test="${i == RECIPE.INFO8}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="10" end="20" step="10">
														<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>
										<select class="select1 w100p" name="info9" id="info9">
											<option value="">선택없음</option>	
											
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">
													<c:forEach var="i" begin="10" end="200" step="10">
														<option value="${i}" <c:if test="${i == RECIPE.INFO9}">selected</c:if>><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="10" end="200" step="10">
														<option value="${i}"><mask:display type="NUMBER" value="${i}" /> mL</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</select>
									</td>
									<td>											
										<c:choose>
											<c:when test="${RECIPE.RECIPE_TP eq '004'}">											
												<div class="btn-pk ss blue3 w100p" id="info10" code='${RECIPE.INFO10}' name='${RECIPE.INFO10_NM}' value='${RECIPE.INFO10_QNTT}' price='${RECIPE.INFO10_PRICE}' onClick="showDrink()"><c:if test="${RECIPE.INFO10 ne ''}">${RECIPE.INFO10_NM}/${RECIPE.INFO10_QNTT}mL</c:if><c:if test="${RECIPE.INFO10 eq ''}">선택없음</c:if></div>
											</c:when>
											<c:otherwise>
												<div class="btn-pk ss blue3 w100p" id="info10" onClick="showDrink()">선택없음</div>
											</c:otherwise>
										</c:choose>
									</td>
									<td><input type="text" name="info11" id="info11" class="inp_txt w100p number" value="${RECIPE.INFO11}" maxlength="3" /></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<h3 class="h_tit2">포장 정보</h3>
					<div class="tbl_basic pr-mb1">
						<table class="list">
							<tbody>
								<tr>
									<th>마킹 유형</th>
									<th>파우치</th>
									<th>탕약박스</th>
									<th>배송박스</th>
								</tr>
								<tr>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnMarking" onclick="showMarking();"><span>선택하세요</span></button>
										<div class="box_td" id="img_marking">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage1" onclick="showPackage('1')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package1">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage2" onclick="showPackage('2');"><span>선택하세요</span></button>
										<div class="box_td" id="img_package2">
											
										</div>
									</td>
									<td class="va-t">
										<button type="button" class="btn-pk s blue w80p" id="btnPackage3" onclick="showPackage('3')"><span>선택하세요</span></button>
										<div class="box_td" id="img_package3">
											
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="view small" style="margin-top: -1px;">
							<colgroup>
								<col class="th1">
								<col class="td1">
								<col>
								<col class="th1">
								<col class="td1">
								<col>
							</colgroup>
							<tbody>
								<tr>
									<th>탕약박스 <br>부착라벨</th>
									<td>
										<select name="drug_label_tp" id="drug_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DRUG_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>												
										</select>
									</td>
									<td>
										<input type="text" name="drug_label_text" id="drug_label_text" class="inp_txt w100p" placeholder="직접입력(14자)" value='<c:if test="${RECIPE.RECIPE_TP eq '001' or RECIPE.RECIPE_TP eq '004'}">${RECIPE.DRUG_LABEL_TEXT}</c:if>' maxlength="14" />
									</td>
									<th>택배박스 <br>부착라벨</th>
									<td>
										<select name="delivery_label_tp" id="delivery_label_tp" class="select1 w100p">
											<c:choose>
												<c:when test="${RECIPE.RECIPE_TP eq '004'}">										
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}" <c:if test="${RECIPE.DELIVERY_LABEL_TP eq LIST_LABEL_TP.get(i - 1).SUB_CD}">selected</c:if>>${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<c:forEach var="i" begin="1" end="${LIST_LABEL_TP.size()}">
														<option value="${LIST_LABEL_TP.get(i - 1).SUB_CD}">${LIST_LABEL_TP.get(i - 1).SUB_NM_KR}</option>
													</c:forEach>
												</c:otherwise>
											</c:choose>	
										</select>
									</td>
									<td>
										<input type="text" name="delivery_label_text" id="delivery_label_text" class="inp_txt w100p" value='<c:if test="${RECIPE.RECIPE_TP eq '001' or RECIPE.RECIPE_TP eq '004'}">${RECIPE.DELIVERY_LABEL_TEXT}</c:if>' maxlength="14" />
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
				
				<h3 class="h_tit2">조제지시 및 요청사항</h3>
				<div class="tbl_basic">
					<table class="view small">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>조제지시<br/>및<br/>요청사항</th>
								<td>
									<div class="mb5">
										<button type="button" class="btn-pk n white w100" onclick="showPopup('800', '800', '/common/pop_list_howto?TYPE=M')">
											<span>불러오기</span>
										</button>
										<button type="button" class="btn-pk n white w100" onClick="sn$('#howtomake').summernote('code', '')">
											<span>내용삭제</span>
										</button>
									</div> <textarea rows="5" name="howtomake" id="howtomake" class="textarea w100p" placeholder="조제지시를 작성 해 주세요 (200자 이내)">${RECIPE.HOWTOMAKE}</textarea>
									<!-- 
									<div class="filebox mt5">
										<label for="upfile1">파일첨부</label> <input type="file" name="upfile1" id="upfile1" class="upload-hidden" onChange="uploadFile1()" />
									</div>
									<div class="filebox_cont" id="howtomake_file">
									
									</div>
									 -->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<h3 class="h_tit2 mt10">복용법</h3>
				<div class="tbl_basic">
					<table class="view small">
						<colgroup>
							<col class="th1">
							<col>
						</colgroup>
						<tbody>
							<tr>
								<th>복용법</th>
								<td>
									<div class="mb5">
										<button type="button" class="btn-pk n white w100" onclick="showPopup('800', '800', '/common/pop_list_howto?TYPE=E')">
											<span>불러오기</span>
										</button>
										<button type="button" class="btn-pk n white w100" onClick="sn$('#howtoeat').summernote('code', '')">
											<span>내용삭제</span>
										</button>
									</div> <textarea rows="5" name="howtoeat" id="howtoeat" class="textarea w100p" placeholder="복용법을 작성 해 주세요 (200자 이내)">${RECIPE.HOWTOEAT}</textarea>
									<div class="filebox mt5">
										<label for="upfile2">파일첨부</label> <input type="file" name="upfile2" id="upfile2" class="upload-hidden" onChange="uploadFile2()" />
									</div>
									<div class="filebox_cont" id="howtoeat_file">
										<c:forEach var="i" begin="1" end="${LIST_ATTACH_EAT.size()}">
											<div class='filebox_txt' data-name='${LIST_ATTACH_EAT.get(i - 1).FILENAME}' data-path='${LIST_ATTACH_EAT.get(i - 1).FILEPATH}' style='display: inline-block;'>
							            		<em class='upload-name2'><a href="/download?filepath=${LIST_ATTACH_EAT.get(i - 1).FILEPATH}&filename=${LIST_ATTACH_EAT.get(i - 1).FILENAME}">${LIST_ATTACH_EAT.get(i - 1).FILENAME}</a></em>
							            		<button type='button' class='btn_file_del' onclick='deleteFile(this)'><span>삭제</span></button>
							            	</div>
										</c:forEach>	
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div><!--// lft_c -->

			<!-- 우측데이터 -->
			<div class="rgh_c">
				<h3 class="h_tit2">환자 정보</h3>
				<div class="top_tit">
					<div class="tab ty1 tab_c1">
						<ul class="n2">
							<li><a id="patient_tab1" href="#tab1_c1"><span>선택한 환자정보</span></a></li>
							<li><a id="patient_tab2" href="#tab1_c2"><span>환자 검색</span></a></li>
						</ul>
					</div>
					<div class="rgh">
						<button type="button" id="btnAddPatient" class="btn-pk s blue">
							<span>등록</span>
						</button>
					</div>
				</div>
				<div class="tabcont pr-mb1">
					<div id="tab1_c1">
						<div class="tbl_sch">
							<div class="t">
								<button type="button" class="btn">
									<span>검색</span>
								</button>
								<input type="text" name="searchPatient" id="searchPatient" class="inp_txt w100p" placeholder="환자명, 차트번호를 입력하여 검색하세요" />
							</div>
						</div>
						<div class="tbl_basic">
							<table class="view small">
								<colgroup>
									<col width="100px" />
									<col width="120px" />
									<col width="100px" />
									<col />
								</colgroup>
								<tbody>
									<tr>
										<th>환자명</th>
										<td id="patient_name"><c:if test="${PATIENT ne null}">${PATIENT.NAME}</c:if></td>
										<th>차트번호</th>
										<td id="patient_chart_no"><c:if test="${PATIENT ne null}">${PATIENT.CHART_NO}</c:if></td>
									</tr>
									<tr>
										<th>성별/나이</th>
										<td id="patient_sex"><c:if test="${PATIENT ne null}">${PATIENT.SEX_NM}</c:if></td>
										<th>최근처방일</th>
										<td id="patient_recipe_dt"><c:if test="${PATIENT ne null}">${PATIENT.RECIPE_DT}</c:if></td>
									</tr>
									<tr>
										<th>연락처</th>
										<td colspan="3" id="patient_mobile"><c:if test="${PATIENT ne null}">${PATIENT.MOBILE}</c:if></td>
									</tr>
									<tr>
										<th>주소</th>
										<td colspan="3" id="patient_address"><c:if test="${PATIENT ne null}">(${PATIENT.ZIPCODE})${PATIENT.ADDRESS} ${PATIENT.ADDRESS2}</c:if></td>
									</tr>
									<tr>
										<th>환자메모</th>
										<td colspan="3" id="patient_remark"><c:if test="${PATIENT ne null}">${PATIENT.REMARK}</c:if></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div id="tab1_c2">
						<iframe id="search_patient" src="/common/search_patient" style="height: 375px" frameborder="0"></iframe>
					</div>
				</div>
				
				<h3 class="h_tit2">약재/처방 추가</h3>
				<div class="top_tit">
					<div class="tab ty1 tab_c2">
						<ul class="n4">						
							<c:choose>
								<c:when test="${RECIPE_TP eq '004'}">
									<li style="margin: 0 3px;"><a href="#tab2_c1"><span>약재검색</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c2"><span>첩약사전</span></a></li>
								</c:when>
								<c:otherwise>
									<li style="margin: 0 3px;"><a href="#tab2_c1"><span>약재검색</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c2"><span>나의처방</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c3"><span>이전처방</span></a></li>
									<li style="margin: 0 3px;"><a href="#tab2_c4"><span>방제사전</span></a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
				<div class="tabcont pr-mb1">
					<c:choose>
						<c:when test="${RECIPE_TP eq '004'}">
							<div id="tab2_c1">
								<iframe src="/common/search_drug" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c2">
								<iframe id="tab2_c2_iframe" frameborder="0"></iframe>
							</div>
						</c:when>
						<c:otherwise>
							<div id="tab2_c1">
								<iframe src="/common/search_drug" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c2">
								<iframe id="tab2_c2_iframe" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c3">
								<iframe id="tab2_c3_iframe" frameborder="0"></iframe>
							</div>
		
							<div id="tab2_c4">
								<iframe src="/common/search_pdata" frameborder="0"></iframe>
							</div>
						</c:otherwise>
					</c:choose>					
				</div>
				
				<h3 class="h_tit2">비용안내</h3>
				<div class="tbl_basic"  style="margin-bottom: 70px;">
					<table class="view">
						<colgroup>
							<col class="th1">
							<col>
							<col class="td1">
						</colgroup>
						<thead>
							<tr>
								<th>항목</th>
								<th>수량</th>
								<th>금액</th>
							</tr>
						</thead>
						<tbody id="list_price">
												
						</tbody>
					</table>
				</div>
			</div><!--// rgh_c -->
			
			<div class="botm_c">
				<div class="mbtn_ty1 mt20">
					<a href="javascript:" id="btnTemp" class="btn-pk b blue2"><span>처방전 임시저장</span></a> 
					<a href="javascript:" id="btnSave" class="btn-pk b blue"><span>다음단계(배송지 정보 입력)</span></a>
				</div>
			</div>
		</section><!-- contents -->
	</div>
</div>
<!--// 컨텐츠 시작 -->

<%@ include file="/WEB-INF/views/include/editor2.jsp"%>

<%@ include file="/WEB-INF/views/include/footer.jsp" %>