package framework.smokingcat.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * <h1>XSSFilter</h1>
 * 파라미터의 일부 문자를 변환한다.
 * <p>
 * <b>Note:</b>
 *
 * @author Jungmin Oh
 * @since 2018-08-17
 */
public class XSSFilter implements Filter {
	@SuppressWarnings("unused")
	private FilterConfig filterConfig = null;

	private final static String[] targetSpaceData = {};
	
	@Override
	public void destroy() {
		this.filterConfig = null;
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
		
		/*
		String url = ((HttpServletRequest) request).getRequestURI().toString();
		if(url.indexOf(this.excludePatterns) > -1) {				
			chain.doFilter(new XSSRequestWrapperPass((HttpServletRequest) request), response);
		}
		else {
			chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
		}
		*/
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	
	/**
	 * 파라미터의 문자중 특정 문자를 치환한다.
	 * 
	 * @param String data
	 * @return String data
	 */
	public static String xssFilter(String data) {
		if (data == null || data.trim().length() <= 0) {
			return "";
		}

		int nTotalCount = targetSpaceData.length;
		for (int nCount = 0; nCount < nTotalCount; nCount++) {
			data = data.replaceAll("(?i)" + targetSpaceData[nCount], "");
		}
		
		data = xssEscape(data);
		
		

		return data;
	}

	/**
	 * 문자열중 일부 특수기호를 치환한다.
	 * 
	 * @param str
	 * @return escapedStr.toString()
	 */
	public static String xssEscape(String str) {
		StringBuffer escapedStr = new StringBuffer();
		char[] ch = str.toCharArray();
		int charSize = ch.length;

		for (int i = 0; i < charSize; i++) {
			if (ch[i] == '<') {
				escapedStr.append("&lt;");
			} else if (ch[i] == '>') {
				escapedStr.append("&gt;");
			} else if (ch[i] == '"') {
				escapedStr.append("&quot;");
			} else if (ch[i] == '&') {
				escapedStr.append("&amp;");
			} else if (ch[i] == '#') {
				escapedStr.append("&#35;");
			} else if (ch[i] == '(') {
				escapedStr.append("&#40;");
			} else if (ch[i] == ')') {
				escapedStr.append("&#41;");
			} else if (ch[i] == '\'') {
				escapedStr.append("&#39;");
			} else {
				escapedStr.append(ch[i]);
			}
		}
		return escapedStr.toString();
	}
}
