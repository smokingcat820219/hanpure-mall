package framework.smokingcat.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * <h1>XSSRequestWrapper</h1>
 * 파라미터의 일부 문자를 변환한다.
 * <p>
 * <b>Note:</b>
 *
 * @author Jungmin Oh
 * @since 2018-08-17
 */
public class XSSRequestWrapper extends HttpServletRequestWrapper {
	public XSSRequestWrapper(final HttpServletRequest request) {
		super(request);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 파라미터의 문자중 특정 문자를 치환한다.
	 * 
	 * @param String parameter
	 * @return String[] encodedValues
	 */
	@Override
	public String[] getParameterValues(final String parameter) {
		// TODO Auto-generated method stub
		String[] values = super.getParameterValues(parameter);
		if (values == null) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++) {
			encodedValues[i] = XSSFilter.xssFilter(values[i]);
		}

		return encodedValues;
	}

	/**
	 * 파라미터의 문자중 특정 문자를 치환한다.
	 * 
	 * @param String name
	 * @return String value
	 */
	@Override
	public String getParameter(final String name) {
		// TODO Auto-generated method stub
		String value = super.getHeader(name);
		return XSSFilter.xssFilter(value);
	}
}