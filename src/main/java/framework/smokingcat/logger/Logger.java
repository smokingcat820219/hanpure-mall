package framework.smokingcat.logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class Logger {

	public static void loggingIdleTime(long startTime) {
		long endTime = System.currentTimeMillis();
		long idleTime = endTime - startTime;

		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.debug(log + "loggingIdleTime [" + CurrentTime.getInstance(idleTime) + "]");
	}
	
	public static String getClassName() {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		return element.getClassName();
	}
	
	public static void info() {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		String message = element.getMethodName() + " execute ...";
		logger.debug(log + message);
	}

	public static void info(String message) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.info(log + message);
	}

	public static void debug(String message) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.debug(log + message);
	}

	public static void debug(String key, Object value) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.debug(log + key + " [" + value + "]");
	}

	public static void debug(Exception ex) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.debug(log + ex.getMessage());

		ex.printStackTrace();
	}

	public static void debug(Object obj) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		if (obj == null) {
			log = log + "null";
		} else {
			log = log + obj.toString();
		}
		logger.debug(log);
	}

	public static void error(String message) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.error(log + message);
	}

	public static void error(Exception ex) {
		Exception e = new Exception();
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - ";
		logger.error(log + ex.getMessage());

		ex.printStackTrace();
	}

	public static void param(Exception e, String message) {
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = element.getClassName() + ":" + element.getLineNumber() + " - <param> ";
		logger.debug(log + message);
	}

	public static void sql(Exception e, String message) {
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = "\r\n--------------------------------------------------------------------------------\r\n";
		log += message.replace("\n", "\r\n");
		logger.debug(log);
	}

	public static void out(Exception e, String message) {
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		logger.debug(message);
	}

	public static void result(Exception e, Object result, long executeTime) {
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = "[query execute time - " + CurrentTime.getInstance(executeTime) + "]\r\n";
		log += result;
		log += "\r\n--------------------------------------------------------------------------------\r\n";
		logger.debug(log);
	}

	public static void result(Exception e, int result, long executeTime) {
		StackTraceElement element = e.getStackTrace()[1];
		Log logger = LogFactory.getLog(element.getClassName());
		String log = "[" + result + " row query execute time - " + CurrentTime.getInstance(executeTime) + "]";
		log += "\r\n--------------------------------------------------------------------------------\r\n";
		logger.debug(log);
	}
}
