package framework.smokingcat.paging;

import javax.servlet.http.HttpServletRequest;

import framework.smokingcat.util.AgentUtils;
import framework.smokingcat.util.CommonUtil;

/**
 * <h1>페이징</h1>
 * 페이징의 기본 설정 및 이전, 다음 처리
 * <p>
 * <b>Note:</b>
 *
 * @author Jungmin Oh
 * @since 2018-08-17
 */
public class Pagenation {
	/*
	private String os;
	private String browser;
	private String reg_ip;
	private String upd_ip;	
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getReg_ip() {
		return reg_ip;
	}

	public void setReg_ip(String reg_ip) {
		this.reg_ip = reg_ip;
	}

	public String getUpd_ip() {
		return upd_ip;
	}

	public void setUpd_ip(String upd_ip) {
		this.upd_ip = upd_ip;
	}
	
	public void setRequest(HttpServletRequest request) {		
		this.setOs(AgentUtils.getUserAgent(request).getOperatingSystem().getName());;
		this.setBrowser(AgentUtils.getUserAgent(request).getBrowser().getName());
		this.setReg_ip(CommonUtil.getUserIP(request));
		this.setUpd_ip(CommonUtil.getUserIP(request));
	}
	*/
	
	/**
	 * 한 페이지에 출력할 게시글 수
	 */
	private int itemPerPage = 10;
	
	/**
	 * 다음, 이전 버튼 클릭시 뛰어넘을 페이지 수
	 */
	private int nextPage = 10;
	
	/**
	 *  페이지 변수
	 */
	private int page = 1;
	
	/**
	 * 총 게시글 수
	 */
	private int totalItemCount;
	
	private int currentItem;

	private String searchField;
	private String searchText;
	
	/**
	 * 현재 위치한 페이지를 구함
	 * 
	 * @return int
	 */
	public int getCurrentPage() {
		int page = this.page;
		if (page < 1) {
			page = 1;
		}
		int pageCount = getPageCount();
		if (page > pageCount) {
			page = pageCount;
		}
		return page;
	}

	/**
	 * 전체 페이지 수를 구함
	 * 
	 * @return int
	 */
	public int getPageCount() {
		return (totalItemCount - 1) / itemPerPage + 1;
	}

	/**
	 * 페이지의 시작 위치를 구함
	 * 
	 * @return int
	 */
	public int getPageBegin() {
		return ((getCurrentPage() - 1) / nextPage) * nextPage + 1;
	}
	
	/**
	 * 페이지의 끝 위치를 구함 
	 * 
	 * @return int
	 */
	public int getPageEnd() {
		int pageCount = getPageCount();
		int num = getPageBegin() + nextPage - 1;
		return Math.min(pageCount, num);
	}
	
	/**
	 * 전체 카운트
	 * 
	 * @return int
	 */
	public int getTotalItemCount() {
		return totalItemCount;
	}

	/**
	 * 전체 카운트 설정
	 * 
	 * @param int
	 */
	public void setTotalItemCount(int totalItemCount) {
		this.totalItemCount = totalItemCount;
	}

	/**
	 * 페이지당 갯수
	 * 
	 * @return int
	 */
	public int getItemPerPage() {
		return itemPerPage;
	}

	/**
	 * 페이지당 갯수 설정
	 * 
	 * @param int
	 */
	public void setItemPerPage(int itemPerPage) {
		this.itemPerPage = itemPerPage;
	}

	/**
	 * 다음 페이지
	 * 
	 * @return int
	 */
	public int getNextPage() {
		return nextPage;
	}
	
	/**
	 * 다음 페이지 설정
	 * 
	 * @param int
	 */
	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * 현재 게시글 위치 (mysql limit에 사용)
	 * 
	 * @return int
	 */
	public int getCurrentItem() {
		this.currentItem = (page - 1) * itemPerPage; 
		return currentItem;
	}
	
	
	public void setCurrentItem(int currentItem) {
		this.currentItem = currentItem;
	}

	/**
	 * 현재 게시글 위치 (mysql limit에 사용)
	 * 
	 * @return int
	 */
	public int getCurrentItemEnd() {
		return (page - 1) * itemPerPage + itemPerPage;
	}

	/**
	 * 다음으로 점프하는 페이지
	 * 
	 * @return int
	 */
	public int getJumpNextPage() {
		return Math.min(getPageCount(), page + nextPage);
	}

	/**
	 * 이전으로 점프하는 페이지
	 * 
	 * @return int
	 */
	public int getJumpPrevPage() {
		return Math.max(1, page - nextPage);
	}
	
	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	
	private String sdate;
	private String edate;

	public String getSdate() {
		return sdate;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}
}
