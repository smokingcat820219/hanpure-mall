package framework.smokingcat.interceptor;


import java.lang.reflect.Field;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
 
@Intercepts({ @Signature(type = StatementHandler.class, method = "update", args = { Statement.class }),
	@Signature(type = StatementHandler.class, method = "query", args = { Statement.class, ResultHandler.class }) })

	public class QueryInterceptor implements Interceptor {
	
	@Override
	public Object intercept(Invocation invocation) throws Throwable {		
		try {
			StatementHandler handler = (StatementHandler)invocation.getTarget();          
	        BoundSql boundSql = handler.getBoundSql();  
	                  
	        String sql = boundSql.getSql();  
	          
	        Object param = handler.getParameterHandler().getParameterObject();  
	        
	        if(param == null) {  
	            sql = sql.replaceFirst("\\?", "''");  
	        }
	        else{                
	            if(param instanceof Integer || param instanceof Long || param instanceof Float || param instanceof Double) {
	            	sql = sql.replaceFirst("\\?", param.toString());  
	            }
	            else if(param instanceof String) {  
	            	String regex = "\\?";
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(sql);
                    sql = matcher.replaceFirst(matcher.quoteReplacement("'" + param + "'"));
	            }
	            else if(param instanceof Map) {
	            	List<ParameterMapping> paramMapping = boundSql.getParameterMappings();      
	              
	            	for(ParameterMapping mapping : paramMapping) {  
	            		String propValue = mapping.getProperty();   
	            		Object value = ((Map) param).get(propValue);  
	            		if(value instanceof String) {
	            			String regex = "\\?";
	                        Pattern pattern = Pattern.compile(regex);
	                        Matcher matcher = pattern.matcher(sql);
	                        sql = matcher.replaceFirst(matcher.quoteReplacement("'" + value + "'"));
	            		}
	            		else{  
	            			sql = sql.replaceFirst("\\?", value.toString());  
	            		}  
	            	}  
	            }
	            else {	            	 
	            	List<ParameterMapping> paramMapping = boundSql.getParameterMappings();	            	
	              
	            	Class<? extends Object> paramClass = param.getClass();  
	            	
	            	String prePropValue = "";
	            	int array_index = 0;
	            	
		            for(ParameterMapping mapping : paramMapping){  
		                String propValue = mapping.getProperty();    
		                
		                try {
		                	int nIndex = -1;
		                	Field field = null;
			                if(propValue.equals("currentItem") || propValue.equals("itemPerPage") || propValue.equals("searchField") || propValue.equals("searchText")) {	//부모 클래스
			                	field = paramClass.getSuperclass().getDeclaredField(propValue);
			                }
			                else {			                	
			                	if(propValue.startsWith("__frch_")) {
			                		String foreach_str = "__frch_";
			                		propValue = propValue.replaceAll(foreach_str, "");
			                		
			                		int nLast = propValue.lastIndexOf("_");
			                		if(nLast > -1) {
			                			nIndex = Integer.parseInt(propValue.substring(nLast + 1));			                			
			                			propValue = "list_" + propValue.substring(0, nLast);
			                			propValue = propValue.toLowerCase();
			                		}
			                		
			                		try {
			                			field = paramClass.getDeclaredField(propValue);
			                		}catch(Exception e1) {
			                			//e1.printStackTrace();
			                			propValue = propValue.toUpperCase();
			                			field = paramClass.getDeclaredField(propValue);
			                		}	
			                		
			                		if(prePropValue.equals(propValue)) {
			                			array_index++;
			                		}
			                		else {
			                			prePropValue = propValue;
			                			array_index = 0;
			                		}
			                	}
			                	else {
			                		field = paramClass.getDeclaredField(propValue);
			                	}			                	
			                }
			                
			                field.setAccessible(true);                 
			                Class<?> javaType = mapping.getJavaType();
			                
			                if(String.class == javaType) {  
			                	if(nIndex == -1) {			                		
			                		String regex = "\\?";
			                        Pattern pattern = Pattern.compile(regex);
			                        Matcher matcher = pattern.matcher(sql);
			                        sql = matcher.replaceFirst(matcher.quoteReplacement("'" + field.get(param).toString() + "'"));
			                	}
			                	else {
			                		String temp = String.valueOf(field.get(param));
			                		temp = temp.substring(1, temp.length() - 1);
			                		String[] sParams = temp.split(",");
			                		
			                		String regex = "\\?";
			                        Pattern pattern = Pattern.compile(regex);
			                        Matcher matcher = pattern.matcher(sql);
			                        sql = matcher.replaceFirst(matcher.quoteReplacement("'" + sParams[array_index].trim() + "'"));
			                	} 
			                }
			                else{  				                	
			                	String regex = "\\?";
		                        Pattern pattern = Pattern.compile(regex);
		                        Matcher matcher = pattern.matcher(sql);
		                        sql = matcher.replaceFirst(matcher.quoteReplacement("'" + field.get(param).toString() + "'"));
			                }				                
		                }catch(Exception ee) {
		                	ee.printStackTrace();
		                }                 
		            }  
	            }  
	        }  
	           
	        System.out.println("=====================================================================");  
	        System.out.println("### SQL : " + sql);  
	        System.out.println("====================================================================="); 
		}catch(Exception e) {
			e.printStackTrace();
			
			try {
				Object[] args = invocation.getArgs();
				
				String excuteQuery = (args[0] + "").replaceAll("(\\r|\\n|\\r\\n)+", "\n");
				
				String query = "";
				{
					StringTokenizer token = new StringTokenizer(excuteQuery, "\n");
					while (token.hasMoreTokens()) {
						String tmp = token.nextToken();
						//if (!CommonUtil.isNull(tmp.trim())) {
							query += tmp + "\n";
						//}
					}
				}		
				
				queryLogging(query, "SELECT");
				queryLogging(query, "INSERT");
				queryLogging(query, "UPDATE");
				queryLogging(query, "DELETE");
			}catch(Exception ee) {
				
			}
		}		 
        
		return invocation.proceed();
	}
	
	private void queryLogging(String... args) {
		if (args[0].indexOf(": " + args[1]) >= 0) {
			String[] _outQuery = args[0].split(": " + args[1]);
			System.out.println("\n\t\t" + args[1] + " " + _outQuery[1]);
		}
	}
	
	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}
	
	@Override
	public void setProperties(Properties properties) {
	}
}

