package framework.smokingcat.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import framework.smokingcat.logger.CurrentTime;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.ApplicationContextHolder;
import kr.co.hanpure.common.CommonCode;
import kr.co.hanpure.database.log.LogService;
import kr.co.hanpure.database.user.UserVO;

public class Interceptor extends HandlerInterceptorAdapter {	
	private static final Logger logger = LoggerFactory.getLogger(Interceptor.class);
		
	private long startTime;	
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		startTime = System.currentTimeMillis();

		logger.debug("==========================================================================================");
		logger.debug("Request URI \t:  " + request.getRequestURI() + "\t" + CommonUtil.getDateTime());		
		String UserIP = CommonUtil.getUserIP(request);		
		logger.debug("UserIP \t:  " + UserIP);		
		logger.debug("");		
		
		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		long endTime = System.currentTimeMillis();
		long executeTime = endTime - startTime;
		logger.debug("");
		logger.debug(request.getRequestURI() + " execute time [" + CurrentTime.getInstance(executeTime) + "]");
		logger.debug("==========================================================================================\n");

		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);	

		super.postHandle(request, response, handler, modelAndView);
	}
}
