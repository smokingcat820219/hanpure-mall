package framework.smokingcat.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import framework.smokingcat.filter.XSSFilter;
import framework.smokingcat.interceptor.Interceptor;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new Interceptor())
		.excludePathPatterns("/images/**", "/css/**", "/fonts/**", "/js/**", "/vendor/**");
	}
	
	@Bean
	public FilterRegistrationBean<XSSFilter> filterRegistrationBean() {
		FilterRegistrationBean<XSSFilter> filterRegistration = new FilterRegistrationBean<>();
		filterRegistration.setFilter(new XSSFilter());
		filterRegistration.setOrder(1);
		filterRegistration.addUrlPatterns("/*");
		return filterRegistration;
	}
}
