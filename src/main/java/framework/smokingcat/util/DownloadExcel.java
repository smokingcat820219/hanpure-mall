package framework.smokingcat.util;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

public class DownloadExcel extends AbstractXlsxStreamingView  {
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub		
		String fileName = (String)model.get("fileName");
		fileName = new String(fileName.getBytes("KSC5601"), "8859_1");
		
		Map<String, String> columns = (Map<String, String>)model.get("columns");		
		List<Map<String, String>> list_map = (List<Map<String, String>>)model.get("list_map");
		
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".xlsx\"");
        
        Sheet sheet = workbook.createSheet("Sheet1");
        sheet.setDefaultColumnWidth(30);
        
        Row header = sheet.createRow(0);
        
        CellStyle style = workbook.createCellStyle();
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setWrapText(true);
		
        int columnCount = 0;
		for( String key : columns.keySet() ){
            System.out.println( String.format("키 : %s, 값 : %s", key, columns.get(key)) );
            
            Cell cell = header.createCell(columnCount);
            cell.setCellValue(key);
			cell.setCellStyle(style);
					
            columnCount++;
        }	
		
        int rowCount = 1;
        for (int i = 0; i < list_map.size(); i++) {
        	Row body = sheet.createRow(rowCount++);
			
			columnCount = 0;
			for(String key : columns.keySet() ) {	            
				Cell cell = body.createCell(columnCount);
				cell.setCellValue(list_map.get(i).get(key));
				cell.setCellStyle(style);
				
	            columnCount++;
	        }	
		}
	}
}
