package framework.smokingcat.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

/**
 * <h1>파일 다운로드</h1>
 * 파일 다운로드 클래스
 * <p>
 * <b>Note:</b>로컬 경로의 이미지를 img 태그에 출력 가능하도록 만든 클래스
 *
 * @author Jungmin Oh
 * @since 2018-08-17
 */
public class DownloadView extends AbstractView {
	public DownloadView() {
		this.setContentType("applicaiton/download;charset=utf-8");
	}
	
	@Override
	protected void renderMergedOutputModel(final Map<String, Object> model, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		File file = (File) model.get("downloadFile");
		String fileName = (String) model.get("fileName");
		
		System.out.println("DownloadView fileName : " + fileName);
		
				
		response.setContentType(this.getContentType());
		response.setContentLength((int) file.length());

		fileName = java.net.URLEncoder.encode(fileName, "UTF-8");
		fileName = fileName.replaceAll("\\+", "%20");
		
		response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");

		OutputStream out = response.getOutputStream();
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);
			FileCopyUtils.copy(fis, out);
		} catch (Exception e) {
			System.out.println("파일이 존재하지 않습니다.");
			//e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
		out.flush();
	}
}
