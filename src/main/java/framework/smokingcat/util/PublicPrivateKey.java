package framework.smokingcat.util;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.HashMap;

import javax.crypto.Cipher;

public class PublicPrivateKey {
	static final int KEY_SIZE = 512;

    public static void main(String[] args) {
    	PublicPrivateKey publicPrivateKey = new PublicPrivateKey();
    	
    	/*
    	//키쌍 생성
    	HashMap<String, String> rsaKeyPair = publicPrivateKey.createKeypairAsString();
    	String publicKey = rsaKeyPair.get("publicKey");
        String privateKey = rsaKeyPair.get("privateKey");
    	System.out.println("만들어진 공개키:" + publicKey);
        System.out.println("만들어진 개인키:" + privateKey);
        */
            	
    	
    	
    	/*
    	String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALan1/Q5+DtJAtKKYB6BfFIGYRWcVOHwDCfSUL28WnZHp/LnJnHOAaExAB3jxohP65ikAa6dyIoXvyMevhACqTkCAwEAAQ==";
        String privateKey = "MIIBVwIBADANBgkqhkiG9w0BAQEFAASCAUEwggE9AgEAAkEAtqfX9Dn4O0kC0opgHoF8UgZhFZxU4fAMJ9JQvbxadken8ucmcc4BoTEAHePGiE/rmKQBrp3Iihe/Ix6+EAKpOQIDAQABAkEAoHTp/R4/trT4ycPNlH7jm5sHBlkDKEgcJY9YZpVkUqo5yj5oWXw7dgiIYdvgtCQwh5Q9heLo+0o5093ZIqzeAQIhAPHqdckHNm8htpRyeoz44OV05pn73qaFzTJ0++JtHb/JAiEAwUoprJECF0gFKWeN/usZ14EvshtvghJK6wckEB8otfECIQDTM16hoypi24wLDebwLFgr+2NYDAzWVPpQktFVXO4cAQIhAL27mdrN4LqPtJlnECh6CLzVD8eKTNPmNuor8Udt/p1RAiEA8S7a77GHKOcdMzQ9z60zGfmiBkyYrClnMFkz0sE3Mfs=";
        
        //String s20220317 = publicPrivateKey.getDateToMilliseconds("2023-03-17");
        //String s20220318 = publicPrivateKey.getDateToMilliseconds("2023-03-18");
        
        String s20220317 = "smokingcat";
        String s20220318 = "link2us";
        
        
        
        System.out.println("s20220317 : " + s20220317);
        System.out.println("s20220318 : " + s20220318);
        
        String encrypted20220317 = publicPrivateKey.encode(s20220317, publicKey);
        String encrypted20220318 = publicPrivateKey.encode(s20220318, publicKey);   
        
        System.out.println("encrypted20220317 : " + encrypted20220317);
        System.out.println("encrypted20220318 : " + encrypted20220318);
        
        s20220317 = publicPrivateKey.decode(encrypted20220317, privateKey);
        s20220318 = publicPrivateKey.decode(encrypted20220318, privateKey);  
        
        System.out.println("s20220317 : " + s20220317);
        System.out.println("s20220318 : " + s20220318);
        */
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	/*
        String privateKey = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEA3gxytPDJSUwx2VLS 7A1lqRV6VjeY+MKauvQaAbjMIWZ4NHkLt4zZKbwdU8sTRpGagisu+Xr4K4yknMLQ gLH4YwIDAQABAkEAjvnm9llA0sCMLIpnI/WwawJYZqGgDzWHWjAJeuhaEHpt6bY7 77S4siGiMP01DcPl6lDHyU8CV6GQML2vsqypGQIhAP45lqAVJU4EEdsvUk7iSgjM SHmUmodUS6S40so+rZBNAiEA35lYsei2Db3PjZ/Dx3MLs21A13y5sKy3WhXOD8F9 g28CIQCeazaApQytE/4ojtGElKC74Mjc8oxk/lNw3zeDjhFMFQIgWwl5Vj40WVXt unDQ6lTJ78zMuK+cfvVjVXdJRZr2+lsCIBO3fNmPM78YLNKAJg0uiyk1QxyXxXsS H2uCKeDfwLP2";
        String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAN4McrTwyUlMMdlS0uwNZakVelY3mPjC mrr0GgG4zCFmeDR5C7eM2Sm8HVPLE0aRmoIrLvl6+CuMpJzC0ICx+GMCAwEAAQ==";
        
        privateKey = privateKey.replaceAll(" ", "");
        publicKey = publicKey.replaceAll(" ", "");
        
        String plain_text = "smokingcat";
        System.out.println("plain_text : " + plain_text);
        
        String encrypted = publicPrivateKey.encode(plain_text, publicKey);
        System.out.println("encrypted : " + encrypted);
        
        String decoded = publicPrivateKey.decode(encrypted, privateKey);
        
        System.out.println("decoded : " + decoded);
        */
    	
    	String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALan1/Q5+DtJAtKKYB6BfFIGYRWcVOHwDCfSUL28WnZHp/LnJnHOAaExAB3jxohP65ikAa6dyIoXvyMevhACqTkCAwEAAQ==";
        String privateKey = "MIIBVwIBADANBgkqhkiG9w0BAQEFAASCAUEwggE9AgEAAkEAtqfX9Dn4O0kC0opgHoF8UgZhFZxU4fAMJ9JQvbxadken8ucmcc4BoTEAHePGiE/rmKQBrp3Iihe/Ix6+EAKpOQIDAQABAkEAoHTp/R4/trT4ycPNlH7jm5sHBlkDKEgcJY9YZpVkUqo5yj5oWXw7dgiIYdvgtCQwh5Q9heLo+0o5093ZIqzeAQIhAPHqdckHNm8htpRyeoz44OV05pn73qaFzTJ0++JtHb/JAiEAwUoprJECF0gFKWeN/usZ14EvshtvghJK6wckEB8otfECIQDTM16hoypi24wLDebwLFgr+2NYDAzWVPpQktFVXO4cAQIhAL27mdrN4LqPtJlnECh6CLzVD8eKTNPmNuor8Udt/p1RAiEA8S7a77GHKOcdMzQ9z60zGfmiBkyYrClnMFkz0sE3Mfs=";
        
        String encrypted = "r+Kty07ZL/aFcB/260OkuOq15x4+50HaApxqYy8uMvw8Cc8pUXoT+MJEr4W7gLokKUYDTj5gxV9Xj3WevS5ihA==";
        
        String decoded = publicPrivateKey.decode(encrypted, privateKey);
        
        System.out.println("decoded : " + decoded);
    }

    /**
     * 키페어 생성
     */
    public HashMap<String, String> createKeypairAsString() {
        HashMap<String, String> stringKeypair = new HashMap<>();
        try {
            SecureRandom secureRandom = new SecureRandom();
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(KEY_SIZE, secureRandom);
            KeyPair keyPair = keyPairGenerator.genKeyPair();

            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            String stringPublicKey = Base64.getEncoder().encodeToString(publicKey.getEncoded());
            String stringPrivateKey = Base64.getEncoder().encodeToString(privateKey.getEncoded());

            stringKeypair.put("publicKey", stringPublicKey);
            stringKeypair.put("privateKey", stringPrivateKey);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringKeypair;
    }

    /**
     * 암호화
     */
    public String encode(String plainData, String stringPublicKey) {
        String encryptedData = null;
        try {
            //평문으로 전달받은 공개키를 공개키객체로 만드는 과정
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] bytePublicKey = Base64.getDecoder().decode(stringPublicKey.getBytes());
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(bytePublicKey);
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            //만들어진 공개키객체를 기반으로 암호화모드로 설정하는 과정
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            //평문을 암호화하는 과정
            byte[] byteEncryptedData = cipher.doFinal(plainData.getBytes());
            encryptedData = Base64.getEncoder().encodeToString(byteEncryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedData;
    }

    /**
     * 복호화
     */
    public String decode(String encryptedData, String stringPrivateKey) {
        String decryptedData = null;
        try {
            //평문으로 전달받은 개인키를 개인키객체로 만드는 과정
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] bytePrivateKey = Base64.getDecoder().decode(stringPrivateKey.getBytes());
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(bytePrivateKey);
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            //만들어진 개인키객체를 기반으로 암호화모드로 설정하는 과정
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            //암호문을 평문화하는 과정
            byte[] byteEncryptedData = Base64.getDecoder().decode(encryptedData.getBytes());
            byte[] byteDecryptedData = cipher.doFinal(byteEncryptedData);
            decryptedData = new String(byteDecryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedData;
    }
    
    public String getDateToMilliseconds(String endDate) {
        try {
            endDate = endDate + " 23:59:59";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            java.util.Date date = simpleDateFormat.parse(endDate);

            return String.valueOf(date.getTime());
        }catch(Exception e) {
            e.printStackTrace();
        }
        
        return "";
    }
}

