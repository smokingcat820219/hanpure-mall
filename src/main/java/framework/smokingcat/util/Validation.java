package framework.smokingcat.util;

import java.util.regex.Pattern;

public class Validation {
	public static boolean checkBizId(String bizid) {		
		boolean bCheck = false;
		try {
			String regex = "[0-9]{3,3}-[0-9]{2,2}-[0-9]{5,5}$";
			bCheck = bizid.matches(regex);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return bCheck;
	}

	public static boolean checkIP(String ip) {		
		boolean bCheck = false;
		try {
			String regex = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$";
			bCheck = ip.matches(regex);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return bCheck;
	}

	public static boolean checkMobile(String mobile) {		
		boolean bCheck = false;
		try {
			mobile = mobile.replaceAll("-", "");
			
			//String regex = "(010|011|016|017|018|019)-[0-9]{3,4}-[0-9]{4,4}$";
			String regex = "(010|011|016|017|018|019)[0-9]{3,4}[0-9]{4,4}$";
			bCheck = mobile.matches(regex);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return bCheck;
	}

	public static boolean checkTel(String tel) {
		boolean bCheck = false;
		try {
			tel = tel.replaceAll("-", "");
			
			//String regex = "(02|051|053|032|062|042|052|044|031|033|043|041|063|061|054|055|064|070)-[0-9]{3,4}-[0-9]{4,4}$";
			String regex = "(02|051|053|032|062|042|052|044|031|033|043|041|063|061|054|055|064|070)[0-9]{3,4}[0-9]{4,4}$";
			bCheck = tel.matches(regex);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return bCheck;
	}
	
	public static boolean checkDate(String sValue, String sDivision) {
		try {
			sValue = sValue.replaceAll(sDivision, "");
			
			if(sValue.length() != 8) {
				return false;
			}
			
			String sYear = sValue.substring(0,  4);
			String sMonth = sValue.substring(4,  6);
			String sDate = sValue.substring(6,  8);
			
			int nYear = Integer.parseInt(sYear);
			int nMonth = Integer.parseInt(sMonth);
			int nDate = Integer.parseInt(sDate);
			
			if(nYear < 1900 || nYear > 2999) {
				return false;
			}
			if(nMonth < 0 || nMonth > 12) {
				return false;
			}
			if(nDate < 0 || nDate > 31) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static boolean checkEmail(String email) {
		int atCheck = 0;
		for(int i = 0; i < email.length(); i++) {
			if(email.charAt(i) == '@') {
				atCheck++;
			}
		}
		
		if(atCheck != 1) {
			return false;
		}
		
		boolean bCheck = false;
		try {
			String regex = "[0-9a-zA-Z][-_.0-9a-zA-Z]+@[-_0-9a-zA-Z]+(\\.[-_0-9a-zA-Z]+){1,2}$";	
			bCheck = email.matches(regex);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return bCheck;
	}
	
	public static boolean  checkPwd(String pwd, int min_length, int max_length) {			
		String PATTERN1 = ".*[a-z].*";
		String PATTERN2 = ".*[A-Z].*";
		String PATTERN3 = ".*[0-9].*";
		String PATTERN4 = ".*[~!@#$%^&*()_+-.,;:\\/|<>\"'`].*";

		if(pwd == null || pwd.length() < min_length || pwd.length() > max_length) {
			return false;
		} 
		else {
			int nCount = 0;

			if(Pattern.matches(PATTERN1, pwd)) {
				nCount++;
			}
			//if(Pattern.matches(PATTERN2, pwd)) {
			//	nCount++;
			//}
			if(Pattern.matches(PATTERN3, pwd)) {
				nCount++;
			}
			//if(Pattern.matches(PATTERN4, pwd)) {
			//	nCount++;
			//}
			

			if(nCount < 2) {
				return false;
			} 
			else {
				return true;
			}
		}
	}
}
