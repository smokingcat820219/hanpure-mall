package framework.smokingcat.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

public class CommonUtil {
	public interface RETURN {
		public static final int SUCCESS = 200;
		public static final int FAIL = 201;
		public static final int LOGIN = 202;
	}

	public static String setDefaultBlank(String value) {
		if (value == null || "".equals(value.trim())) {
			return "";
		} else {
			return value;
		}
	}

	public static boolean isNull(String value) {
		if (value == null || "".equals(value.trim())) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNull(String value, String[] values) {
		if (value == null || "".equals(value.trim())) {
			return true;
		}

		for (int i = 0; i < values.length; i++) {
			if (values[i].equals(value)) {
				return false;
			}
		}

		return true;
	}

	public static String SHA_256(String str) {
		String SHA = "";
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-256");
			sh.update(str.getBytes());
			byte byteData[] = sh.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			SHA = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			SHA = null;
		}
		return SHA;
	}

	public static void ShowValues(Object obj) {
		try {
			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value = field.get(obj);

				// if (value != null && !value.equals("")) {
				// Logger.debug(field.getName() + "=" + value);
				System.out.println(field.getName() + "=" + value);
				// }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JSONObject ObjectToJSONObject(Object obj) {
		try {
			JSONObject data = new JSONObject();

			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value = field.get(obj);

				if (value != null) {
					data.put(field.getName(), RemoveFilter(value.toString()));
				}
			}

			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Map<String, String> ObjectToMap(Object obj) {
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value = field.get(obj);

				if (value != null) {
					map.put(field.getName(), RemoveFilter(value.toString()));
				}
			}

			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return map;
		}
	}

	public static <T> JSONArray ListToJSONArray(List<T> list) {
		try {
			JSONArray json_array = new JSONArray();

			for (int i = 0; i < list.size(); i++) {
				Object obj = list.get(i);

				JSONObject data = new JSONObject();

				for (Field field : obj.getClass().getDeclaredFields()) {
					field.setAccessible(true);
					Object value = field.get(obj);

					if (value != null) {
						data.put(field.getName(), RemoveFilter(value.toString()));
					}
				}
				json_array.add(data);
			}
			return json_array;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String returnAjax(int result, String message) {
		JSONObject data = new JSONObject();
		data.put("result", result);
		data.put("message", message);

		return data.toString();
	}

	public static void MakeFolder(String path) {
		try {
			File dir = new File(path);

			if (!dir.exists()) {
				dir.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void DeleteFile(String parent, String name) {
		try {
			if (name != null && !name.equals("")) {
				File file = new File(parent, name);
				if (file.isFile()) {
					if (file.exists()) {
						file.delete();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getOrientation(BufferedInputStream is) {
		int orientation = 1;
		try {
			com.drew.metadata.Metadata metadata = com.drew.imaging.ImageMetadataReader.readMetadata(is);
			com.drew.metadata.exif.ExifIFD0Directory directory = metadata
					.getDirectory(com.drew.metadata.exif.ExifIFD0Directory.class);
			orientation = directory.getInt(com.drew.metadata.exif.ExifIFD0Directory.TAG_ORIENTATION);
		} catch (Exception e) {
			// e.printStackTrace();

			return -1;
		}

		return orientation;
	}

	public static BufferedImage rotateImageForMobile(InputStream is, int orientation) {
		try {
			BufferedImage bi = ImageIO.read(is);
			if (orientation == 6) { // 정위치
				return rotateImage(bi, 90);
			} else if (orientation == 1) { // 왼쪽으로 눞였을때
				return bi;
			} else if (orientation == 3) {// 오른쪽으로 눞였을때
				return rotateImage(bi, 180);
			} else if (orientation == 8) {// 180도
				return rotateImage(bi, 270);
			} else {
				return bi;
			}
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	public static BufferedImage rotateImage(BufferedImage orgImage, int radians) {
		BufferedImage newImage;
		if (radians == 90 || radians == 270) {
			newImage = new BufferedImage(orgImage.getHeight(), orgImage.getWidth(), orgImage.getType());
		} else if (radians == 180) {
			newImage = new BufferedImage(orgImage.getWidth(), orgImage.getHeight(), orgImage.getType());
		} else {
			return orgImage;
		}

		Graphics2D graphics = (Graphics2D) newImage.getGraphics();
		graphics.rotate(Math.toRadians(radians), newImage.getWidth() / 2, newImage.getHeight() / 2);
		graphics.translate((newImage.getWidth() - orgImage.getWidth()) / 2,
				(newImage.getHeight() - orgImage.getHeight()) / 2);
		graphics.drawImage(orgImage, 0, 0, orgImage.getWidth(), orgImage.getHeight(), null);

		return newImage;
	}

	public static boolean isEnableFile(MultipartFile upfile) {
		if (upfile == null || upfile.isEmpty()) {
			return false;
		} else {
			String filename = upfile.getOriginalFilename();
			String fileExt = filename.substring(filename.lastIndexOf("."));
			fileExt = fileExt.toLowerCase();

			if (fileExt.indexOf("exe") > -1) {
				return false;
			} else if (fileExt.indexOf("html") > -1) {
				return false;
			} else if (fileExt.indexOf("jsp") > -1) {
				return false;
			} else if (fileExt.indexOf("js") > -1) {
				return false;
			} else if (fileExt.indexOf("bat") > -1) {
				return false;
			} else if (fileExt.indexOf("sh") > -1) {
				return false;
			}
		}

		return true;
	}

	public static String[] CopyFile(MultipartFile upfile, String savePath) {
		String dateFolderName = getTodayFolder(savePath);

		String[] attach = new String[3];
		attach[0] = "";
		attach[1] = "";
		attach[2] = "";

		try {
			if (!upfile.isEmpty()) {
				attach[0] = upfile.getOriginalFilename();
				String fileExt = attach[0].substring(attach[0].lastIndexOf("."));
				attach[1] = dateFolderName + "/" + System.currentTimeMillis()
						+ UUID.randomUUID().toString().replaceAll("-", "") + fileExt;
				attach[2] = String.valueOf(upfile.getSize());

				File newFile = new File(savePath + File.separator + attach[1]);
				// FileUtils.writeByteArrayToFile(newFile, upfile.getBytes());

				if (isImagefile(attach[0])) {
					try {
						int orientation = 1;
						byte[] bytes = upfile.getBytes();
						orientation = getOrientation(new BufferedInputStream(new ByteArrayInputStream(bytes)));

						if (orientation == -1) {
							FileUtils.writeByteArrayToFile(newFile, upfile.getBytes());
						} else {
							BufferedImage buffredImage = rotateImageForMobile(
									new BufferedInputStream(new ByteArrayInputStream(bytes)), orientation);
							ImageIO.write(buffredImage, fileExt.substring(1), newFile);
						}
					} catch (Exception ee) {
						ee.printStackTrace();

						FileUtils.writeByteArrayToFile(newFile, upfile.getBytes());
					}
				} else {
					FileUtils.writeByteArrayToFile(newFile, upfile.getBytes());
				}

				return attach;
			}
		} catch (Exception e) {
			e.printStackTrace();

			return attach;
		}

		return attach;
	}

	public static boolean isImagefile(String filename) {
		filename = filename.toLowerCase();

		boolean bImageFile = false;

		if (filename.indexOf(".jpg") > -1) {
			bImageFile = true;
		} else if (filename.indexOf(".jpeg") > -1) {
			bImageFile = true;
		} else if (filename.indexOf(".png") > -1) {
			bImageFile = true;
		} else if (filename.indexOf(".bmp") > -1) {
			bImageFile = true;
		} else if (filename.indexOf(".gif") > -1) {
			bImageFile = true;
		}

		return bImageFile;
	}

	/*
	 * public static boolean CheckBizID(String bizID) { bizID =
	 * bizID.replaceAll("-", "");
	 * 
	 * int hap = 0; int temp = 0; int check[] = { 1, 3, 7, 1, 3, 7, 1, 3, 5 };
	 * 
	 * if (bizID.length() != 10) { return false; }
	 * 
	 * for (int i = 0; i < 9; i++) { if (bizID.charAt(i) < '0' ||
	 * bizID.charAt(i) > '9') { return false; } else {
	 * 
	 * }
	 * 
	 * hap = hap + Character.getNumericValue(bizID.charAt(i)) * check[temp];
	 * temp++; }
	 * 
	 * hap += Character.getNumericValue(bizID.charAt(8)) * 5 / 10;
	 * 
	 * if ((10 - hap % 10) % 10 == Character.getNumericValue(bizID.charAt(9))) {
	 * return true; } else { return false; } }
	 */

	public static ModelAndView Go404() {
		ModelAndView mav = new ModelAndView("/error/error404");
		return mav;
	}

	public static ModelAndView GoErrorPage(String message) {
		ModelAndView mav = new ModelAndView("/error/throws");
		mav.addObject("message", message);
		return mav;
	}

	public static ModelAndView GoErrorPage(Exception e, String controllerName, String funcName) {
		ModelAndView mav = new ModelAndView("/error/throws");
		mav.addObject("message", e.getMessage());
		return mav;
	}

	public static void SendMail(String senderName, List<String> list_recv, String title, String content,
			String MAIL_SMTP_HOST, String MAIL_SMTP_PORT, final String MAIL_USER_ID, final String MAIL_PASSWORD)
					throws SendFailedException {

		try {
			if(MAIL_SMTP_PORT.equals("25")) {				
				HtmlEmail email = new HtmlEmail();
				email.setCharset("UTF-8");
				String authuser = MAIL_USER_ID;
				String authpwd = MAIL_PASSWORD;
				
				authuser = authuser.substring(0, authuser.indexOf("@"));				
				email.setSmtpPort(Integer.parseInt(MAIL_SMTP_PORT));
				email.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
				email.setDebug(true); // true if you want to debug
				email.setHostName(MAIL_SMTP_HOST);
				
				email.setFrom(MAIL_USER_ID, senderName);
				email.setSubject(title);
							
				email.setHtmlMsg(content);
				email.addTo(list_recv.get(0), list_recv.get(0));
				email.send();
			}
			else {
				HtmlEmail email = new HtmlEmail();
				email.setCharset("UTF-8");
				String authuser = MAIL_USER_ID;
				String authpwd = MAIL_PASSWORD;
				
				email.setSmtpPort(Integer.parseInt(MAIL_SMTP_PORT));
				email.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
				email.setDebug(true); // true if you want to debug
				email.setHostName(MAIL_SMTP_HOST);

				email.getMailSession().getProperties().put("mail.smtp.auth", "true");
				email.getMailSession().getProperties().put("mail.smtp.socketFactory.class",   "javax.net.ssl.SSLSocketFactory");
				email.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");
				
				email.getMailSession().getProperties().put("mail.debug", "true");
				email.getMailSession().getProperties().put("mail.smtp.port", MAIL_SMTP_PORT);
				email.getMailSession().getProperties().put("mail.smtp.socketFactory.port", MAIL_SMTP_PORT);
				
				email.getMailSession().getProperties().put("mail.smtp.socketFactory.fallback", "false");			
				email.setFrom(MAIL_USER_ID, senderName);
				email.setSubject(title);
							
				email.setHtmlMsg(content);
				email.addTo(list_recv.get(0), list_recv.get(0));
				email.send();
			}			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void SendMailWithFiles(String senderName, List<String> list_recv, String title, String content,
			List<File> list_file, String MAIL_SMTP_HOST, String MAIL_SMTP_PORT, final String MAIL_USER_ID,
			final String MAIL_PASSWORD) throws SendFailedException {

		try {
			// content += "<br/><br/><font color='red'
			// style='font-size:18px;'><b>※ 본 메일은 발신 전송 메일입니다.</b></font>";

			HtmlEmail email = new HtmlEmail();
			email.setCharset("UTF-8");
			String authuser = MAIL_USER_ID;
			String authpwd = MAIL_PASSWORD;

			email.setSmtpPort(Integer.parseInt(MAIL_SMTP_PORT));
			email.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
			email.setDebug(true); // true if you want to debug
			email.setHostName(MAIL_SMTP_HOST);

			email.getMailSession().getProperties().put("mail.smtp.auth", "true");
			email.getMailSession().getProperties().put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
			email.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");

			email.getMailSession().getProperties().put("mail.debug", "false");
			email.getMailSession().getProperties().put("mail.smtp.port", MAIL_SMTP_PORT);
			email.getMailSession().getProperties().put("mail.smtp.socketFactory.port", MAIL_SMTP_PORT);

			email.getMailSession().getProperties().put("mail.smtp.socketFactory.fallback", "false");
			email.setFrom(MAIL_USER_ID, senderName);
			email.setSubject(title);

			for (int i = 0; i < list_file.size(); i++) {
				EmailAttachment attachment = new EmailAttachment();
				attachment.setPath(list_file.get(i).getAbsolutePath());
				attachment.setDisposition(EmailAttachment.ATTACHMENT);
				attachment.setDescription(list_file.get(i).getName());
				attachment.setName(MimeUtility.encodeText(list_file.get(i).getName(), "euc-kr", "B")); // 파일의
																										// 이름을
																										// 지정

				email.attach(attachment);
			}

			email.setHtmlMsg(content);

			for (int i = 0; i < list_recv.size(); i++) {
				email.addTo(list_recv.get(i), list_recv.get(i));
			}

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String RemoveFilter(String text) {
		text = text.replaceAll("&lt;", "<");
		text = text.replaceAll("&gt;", ">");
		text = text.replaceAll("&quot;", "\"");
		text = text.replaceAll("&amp;", "&");
		text = text.replaceAll("&#40;", "(");
		text = text.replaceAll("&#41;", ")");
		text = text.replaceAll("&#39;", "\'");

		return text;

	}

	public static class SMTPAuthenticator extends javax.mail.Authenticator {
		private String MAIL_USER_ID;
		private String MAIL_PASSWORD;

		public SMTPAuthenticator(String id, String pwd) {
			this.MAIL_USER_ID = id;
			this.MAIL_PASSWORD = pwd;
		}

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(MAIL_USER_ID, MAIL_PASSWORD);
		}
	}

	public static JSONObject StringToJSONObject(String json) {
		try {
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject) parser.parse(json);
			return obj;
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}
	
	public static JSONArray StringToJSONArray(String json) {
		try {
			JSONParser parser = new JSONParser();
			JSONArray obj = (JSONArray) parser.parse(json);
			return obj;
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	public static String getUserIP(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getConfirmNum() {
		String code[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		int nPwdLength = 6;
		String rtnStr = "";
		for (int i = 0; i < nPwdLength; i++) {
			int rand = (int) (Math.random() * code.length);
			rtnStr += code[rand];
		}
		return rtnStr;
	}

	public static String getTempPwd(int nPwdLength) {
		String code[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i",
				"j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
		String tmpPwd = "";
		for (int i = 0; i < nPwdLength; i++) {
			int rand = (int) (Math.random() * code.length);
			tmpPwd += code[rand];
		}
		return tmpPwd;
	}

	public static String getTempAddress(int nPwdLength) {
		String tmpPwd = "";
		{
			String code[] = { "m", "n" };
			for (int i = 0; i < 1; i++) {
				int rand = (int) (Math.random() * code.length);
				tmpPwd += code[rand];
			}
		}
		String code[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i",
				"j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D",
				"E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
				"Z" };
		for (int i = 0; i < nPwdLength - 1; i++) {
			int rand = (int) (Math.random() * code.length);
			tmpPwd += code[rand];
		}
		return tmpPwd;
	}

	public static String getYesterday(String today) {
		try {
			String[] dates = today.split("-");
			System.out.println(dates.length);

			int year = Integer.parseInt(dates[0]);
			int month = Integer.parseInt(dates[1]);
			int date = Integer.parseInt(dates[2]);

			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH, date - 1);

			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH) + 1;
			date = cal.get(Calendar.DAY_OF_MONTH);

			String tomorrow = String.format("%04d-%02d-%02d", year, month, date);
			return tomorrow;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getTomorrow(String today) {
		try {
			String[] dates = today.split("-");
			System.out.println(dates.length);

			int year = Integer.parseInt(dates[0]);
			int month = Integer.parseInt(dates[1]);
			int date = Integer.parseInt(dates[2]);

			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH, date + 1);

			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH) + 1;
			date = cal.get(Calendar.DAY_OF_MONTH);

			String tomorrow = String.format("%04d-%02d-%02d", year, month, date);
			return tomorrow;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static long StrDateToLong(String today) {
		try {
			String[] dates = today.split("-");

			int year = Integer.parseInt(dates[0]);
			int month = Integer.parseInt(dates[1]);
			int date = Integer.parseInt(dates[2]);

			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH, date);

			return cal.getTimeInMillis();
		} catch (Exception e) {
			e.printStackTrace();
			return 0L;
		}
	}

	public static String getTodayFolder(String parent) {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyyyMMdd");

		String folderName = sdf.format(new Date());

		File dir = new File(parent, folderName);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		return folderName;
	}

	public static String setNextMonth(String today, int period) {
		try {
			String[] dates = today.split("-");

			int year = Integer.parseInt(dates[0]);
			int month = Integer.parseInt(dates[1]);
			int date = Integer.parseInt(dates[2]);

			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1 + period);
			cal.set(Calendar.DAY_OF_MONTH, date);

			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH) + 1;
			date = cal.get(Calendar.DAY_OF_MONTH);

			String tomorrow = String.format("%04d-%02d-%02d", year, month, date);
			return tomorrow;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static int getDateDiff(String sDate1, String sDate2) {
		try {
			Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
			Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate2);

			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();

			cal1.setTime(date1);
			cal2.setTime(date2);

			long diffSec = (cal2.getTimeInMillis() - cal1.getTimeInMillis()) / 1000;
			long diffDays = diffSec / (24 * 60 * 60);

			return (int) diffDays;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static String setNextDay(String today, int days) {
		try {
			String[] dates = today.split("-");

			int year = Integer.parseInt(dates[0]);
			int month = Integer.parseInt(dates[1]);
			int date = Integer.parseInt(dates[2]);

			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH, date + days);

			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH) + 1;
			date = cal.get(Calendar.DAY_OF_MONTH);

			String tomorrow = String.format("%04d-%02d-%02d", year, month, date);
			return tomorrow;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String setNextTime(int times) {
		try {
			Calendar cal = Calendar.getInstance();
			int nHour = cal.get(Calendar.HOUR_OF_DAY);

			cal.set(Calendar.HOUR_OF_DAY, nHour + times);

			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int date = cal.get(Calendar.DAY_OF_MONTH);

			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int minute = cal.get(Calendar.MINUTE);
			int second = cal.get(Calendar.SECOND);

			String tomorrow = String.format("%04d년 %02d월 %02d일 %02d:%02d", year, month, date, hour, second);
			return tomorrow;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getToday() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyyy-MM-dd");

		return sdf.format(new Date());
	}

	public static String getTodayKR() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyyy년 MM월 dd일 (HH:mm)");

		return sdf.format(new Date());
	}

	public static String getTodayEx() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyyyMMdd");

		return sdf.format(new Date());
	}

	public static String getYear() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyyy");

		return sdf.format(new Date());
	}

	public static String getMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("MM");

		return sdf.format(new Date());
	}

	public static String getDate() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("dd");

		return sdf.format(new Date());
	}

	public static String getHour() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("HH");

		return sdf.format(new Date());
	}

	public static String getMinute() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("mm");

		return sdf.format(new Date());
	}

	public static String getSecond() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("ss");

		return sdf.format(new Date());
	}

	public static String getDateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyyy-MM-dd HH:mm:ss");

		return sdf.format(new Date());
	}
	
	public static String getDateTimeEx() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyMMddHHmmssSSS");

		return sdf.format(new Date());
	}

	public static String getMethodName() {
		StackTraceElement[] elements = null;

		try {
			throw new Exception("getMethodName");
		} catch (Exception e) {
			elements = e.getStackTrace();
		}

		String methodName = ((StackTraceElement) elements[1]).getMethodName();

		return methodName;
	}

	/*
	 * public static String GetMailTemplete(HttpServletRequest request, String
	 * filename, String cert) { String sReturn = "";
	 * 
	 * try { String protocol = request.getScheme(); String port =
	 * String.valueOf(request.getLocalPort());
	 * 
	 * String host = "";
	 * 
	 * String URL = request.getRequestURI(); host = protocol + "://" + URL + ":"
	 * + port;
	 * 
	 * String filepath = request.getRealPath("/") + File.separator + "WEB-INF" +
	 * File.separator + "views" + File.separator + "mail" + File.separator +
	 * filename;
	 * 
	 * File file = new File(filepath); if(!file.exists()) return sReturn;
	 * 
	 * BufferedReader in = new BufferedReader(new FileReader(filepath)); String
	 * sTmp;
	 * 
	 * while((sTmp = in.readLine()) != null) { sReturn += sTmp; } in.close();
	 * 
	 * sReturn = sReturn.replaceAll("\\{LINK\\}", host + "/" + "common/cert/" +
	 * cert + "/"); sReturn = sReturn.replaceAll("\\{CERT\\}", cert); sReturn =
	 * sReturn.replaceAll("\\{HOST\\}", host);
	 * 
	 * Logger.debug("sReturn=" + sReturn);
	 * 
	 * return sReturn; }catch(Exception e) { e.printStackTrace(); return
	 * sReturn; } }
	 */

	public static String GetMailTemplete(HttpServletRequest request, String filepath) {
		String sReturn = "";

		try {
			File file = new File(filepath);
			if (!file.exists())
				return sReturn;

			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filepath), "UTF8"));
			String sTmp;

			while ((sTmp = in.readLine()) != null) {
				sReturn += sTmp;
			}
			in.close();

			return sReturn;
		} catch (Exception e) {
			e.printStackTrace();
			return sReturn;
		}
	}

	public static double[] AddressToGeocode(String CLIENTID, String CLIENTSECRET, String ADDRESS) {
		double[] dbRet = { 0, 0 };
		try {
			StringBuilder URL = new StringBuilder("https://naveropenapi.apigw.ntruss.com/map-geocode/v2/geocode");
			URL.append("?query=" + URLEncoder.encode(ADDRESS, "UTF-8"));

			CloseableHttpClient httpclient = HttpClients.createDefault();

			HttpGet httpGet = new HttpGet(URL.toString());
			httpGet.setHeader("Content-Type", "application/json");
			httpGet.setHeader("X-NCP-APIGW-API-KEY-ID", CLIENTID);
			httpGet.setHeader("X-NCP-APIGW-API-KEY", CLIENTSECRET);

			// UTF-8은 한글
			CloseableHttpResponse response = httpclient.execute(httpGet);

			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);

					JSONParser parser = new JSONParser();

					JSONObject root = (JSONObject) parser.parse(result);

					String status = (String) root.get("status");
					if (status.equals("OK")) {
						JSONArray addresses = (JSONArray) root.get("addresses");
						if (addresses.size() > 0) {
							JSONObject address = (JSONObject) addresses.get(0);
							String x = (String) address.get("x");
							String y = (String) address.get("y");

							dbRet[0] = Double.parseDouble(y);
							dbRet[1] = Double.parseDouble(x);
						}
					} else {
						// 에러
						String errorMessage = (String) root.get("errorMessage");
						System.out.println("errorMessage : " + errorMessage);
					}
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dbRet;
	}

	public static String GeocodeToAddress(String CLIENTID, String CLIENTSECRET, String lat, String lon) {
		String sRet = "";

		try {
			StringBuilder URL = new StringBuilder("https://naveropenapi.apigw.ntruss.com/map-reversegeocode/v2/gc");
			URL.append("?request=coordsToaddr");
			URL.append("&coords=" + lon + "," + lat);
			URL.append("&sourcecrs=epsg:4326&output=json&orders=addr,admcode,roadaddr");

			CloseableHttpClient httpclient = HttpClients.createDefault();

			HttpGet httpGet = new HttpGet(URL.toString());
			httpGet.setHeader("Content-Type", "application/json");
			httpGet.setHeader("X-NCP-APIGW-API-KEY-ID", CLIENTID);
			httpGet.setHeader("X-NCP-APIGW-API-KEY", CLIENTSECRET);

			// UTF-8은 한글
			CloseableHttpResponse response = httpclient.execute(httpGet);

			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);

					System.out.println("result : " + result);

					JSONParser parser = new JSONParser();

					JSONObject root = (JSONObject) parser.parse(result);

					JSONObject status = (JSONObject) root.get("status");
					Long code = (Long) status.get("code");
					if (code == 0) {

						JSONArray results = (JSONArray) root.get("results");
						if (results.size() > 0) {
							JSONObject address = (JSONObject) results.get(0);

							JSONObject region = (JSONObject) address.get("region");

							JSONObject area1 = (JSONObject) region.get("area1");
							JSONObject area2 = (JSONObject) region.get("area2");
							JSONObject area3 = (JSONObject) region.get("area3");

							sRet += (String) area1.get("name");
							sRet += " " + (String) area2.get("name");

							JSONObject land = (JSONObject) address.get("land");

							if (!CommonUtil.isNull((String) land.get("name"))) {
								sRet += " " + (String) land.get("name");
								sRet += " " + (String) land.get("number1");

								if (!CommonUtil.isNull((String) land.get("number2"))) {
									sRet += "-" + (String) land.get("number2");
								}
							} else {
								sRet += " " + (String) area3.get("name");

								sRet += " " + (String) land.get("number1");

								if (!CommonUtil.isNull((String) land.get("number2"))) {
									sRet += "-" + (String) land.get("number2");
								}
							}
						}
					} else {
						// 에러
						String errorMessage = (String) root.get("errorMessage");
						System.out.println("errorMessage : " + errorMessage);
					}
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sRet;
	}

	public static void SendPush(List<String> list, String title, String content) {
		try {
			RestTemplate restTemplate = new RestTemplate();

			org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
			Charset utf8 = Charset.forName("UTF-8");
			org.springframework.http.MediaType mediaType = new org.springframework.http.MediaType("application", "json",
					utf8);
			headers.setContentType(mediaType);

			// 서버키
			headers.add("Authorization",
					"key=AAAA8s14PwE:APA91bGB8VqsyiRCkAzInz0P4FukFW5FpHrz27x-bEGUtdDfAzBWoUXXdyls-lBKF76E2PZ30nHH8cro5IZ_BVXcFGPe7yzU-76G3dwjjFRReaRHTfCyd2pOtjYCmPF4Q-MmsZcZZzpn");

			JSONObject fcm = new JSONObject();

			// 사용자 토큰
			JSONArray tokens = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				tokens.add(list.get(i));
			}

			JSONObject data = new JSONObject();
			data.put("title", title);
			data.put("body", content);

			fcm.put("registration_ids", tokens);
			// fcm.put("notification", noti);
			fcm.put("data", data);

			org.springframework.http.HttpEntity request;
			request = new org.springframework.http.HttpEntity(fcm.toString(), headers);

			org.springframework.http.ResponseEntity<String> result = restTemplate.exchange(
					"https://fcm.googleapis.com/fcm/send", org.springframework.http.HttpMethod.POST, request,
					String.class);

			System.out.println("fcm.toString()=" + fcm.toString());
			System.out.println("result.getBody()=" + result.getBody());
		} catch (Exception e) {

		}
	}

	public static String[] navigation(String CLIENTID, String CLIENTSECRET, String sStart, String sGoal) {
		String[] sRet = { "0", "0", "0", "0" };
		try {
			// String sStart = "127.0563926,37.5201717,name=유로보";
			// String sGoal = "128.3657167,36.119239,name=은성";
			StringBuilder URL = new StringBuilder("https://naveropenapi.apigw.ntruss.com/map-direction/v1/driving");
			URL.append("?start=" + URLEncoder.encode(sStart, "UTF-8"));
			URL.append("&goal=" + URLEncoder.encode(sGoal, "UTF-8"));
			URL.append("&option=traoptimal");

			CloseableHttpClient httpclient = HttpClients.createDefault();

			HttpGet httpGet = new HttpGet(URL.toString());
			httpGet.setHeader("Content-Type", "application/json");
			httpGet.setHeader("X-NCP-APIGW-API-KEY-ID", CLIENTID);
			httpGet.setHeader("X-NCP-APIGW-API-KEY", CLIENTSECRET);

			// UTF-8은 한글
			CloseableHttpResponse response = httpclient.execute(httpGet);

			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);

					System.out.println(result);

					JSONParser parser = new JSONParser();

					JSONObject root = (JSONObject) parser.parse(result);

					long code = (Long) root.get("code");
					if (code == 0) {
						JSONObject route = (JSONObject) root.get("route");

						JSONArray traoptimal = (JSONArray) route.get("traoptimal");
						if (traoptimal.size() > 0) {
							JSONObject item = (JSONObject) traoptimal.get(0);

							JSONObject summary = (JSONObject) item.get("summary");

							long distance = (Long) summary.get("distance");
							long duration = (Long) summary.get("duration") / 1000L;
							long tollFare = (Long) summary.get("tollFare");
							long fuelPrice = (Long) summary.get("fuelPrice");

							sRet[0] = String.valueOf(distance);
							sRet[1] = String.valueOf(secondsToTime((int) duration));
							sRet[2] = String.valueOf(tollFare);
							sRet[3] = String.valueOf(fuelPrice);
						}
					} else {
						// 에러
						String message = (String) root.get("message");
						System.out.println("message : " + message);
					}
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sRet;
	}

	public static String secondsToTime(int seconds) {
		// int numberOfDays = seconds / 86400;
		int numberOfHours = (seconds % 86400) / 3600;
		int numberOfMinutes = ((seconds % 86400) % 3600) / 60;
		int numberOfSeconds = ((seconds % 86400) % 3600) % 60;

		return String.format("%02d:%02d:%02d", numberOfHours, numberOfMinutes, numberOfSeconds);

	}

	public static String NumberFormat(String number) {
		try {
			if(number.indexOf(".") > -1) {
				DecimalFormat df = new DecimalFormat("#,###,###.##");
				return df.format(Float.parseFloat(number));
			}
			else {
				DecimalFormat df = new DecimalFormat("#,###,###");
				return df.format(Integer.parseInt(number));
			}
			
			
		} catch (Exception e) {
			return "";
		}
	}

	public static String NumberFormat(int number) {
		try {
			DecimalFormat df = new DecimalFormat("#,###,###");
			return df.format(number);
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String NumberFormat(float number) {
		try {
			DecimalFormat df = new DecimalFormat("#,###,###.##");
			return df.format(number);
		} catch (Exception e) {
			return "";
		}
	}

	public static List<String> GetBankList() {
		List<String> list = new ArrayList<String>();
		list.add("신한");
		list.add("제주");
		list.add("국민");
		list.add("농협");
		list.add("우리");
		list.add("KEB하나");
		list.add("외환");
		list.add("우체국");
		list.add("기업");
		list.add("SC");
		list.add("BNP파리바");
		list.add("수협");
		list.add("산업");
		list.add("도이치");
		list.add("저축은행");
		list.add("신협");
		list.add("지역농.축협");
		list.add("새마을");
		list.add("경남");
		list.add("전북");
		list.add("광주");
		list.add("부산");
		list.add("대구");
		list.add("HSBC");
		list.add("JP모간");
		list.add("BOA");
		list.add("산림조합");
		list.add("중국공상");
		list.add("케이뱅크");
		list.add("카카오뱅크");
		list.add("한국씨티");
		list.add("중국건설");
		list.add("국세");
		return list;
	}

	public static List<String> GetSalayTypeList() {
		List<String> list = new ArrayList<String>();
		list.add("시급");
		list.add("일급");
		list.add("주급");
		list.add("월급");
		list.add("건당");
		list.add("TC");
		list.add("기타");
		return list;
	}

	// wideshot
	public static String SendSMS(String sejongApiKey, String userKey, String sender, String recver, String content,
			String advertisementYn) {
		// String URL = "https://api-dev.wideshot.co.kr/api/v1/message/sms";
		// //개발서버
		String URL = "https://api.wideshot.co.kr/api/v1/message/sms"; // 운영서버

		try {
			/*
			 * HTTP
			 */
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			/*
			 * HTTPS
			 */
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpPost httpPost = new HttpPost(URL);
			httpPost.setHeader("sejongApiKey", sejongApiKey);
			// httpPost.setHeader("User-Agent", "Mozilla/5.0");
			// httpPost.setHeader("User-Agent", "en-US,en;q=0.5");

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("callback", sender));
			nvps.add(new BasicNameValuePair("contents", content));
			nvps.add(new BasicNameValuePair("receiverTelNo", recver));
			nvps.add(new BasicNameValuePair("userKey", userKey));
			nvps.add(new BasicNameValuePair("advertisementYn", advertisementYn));

			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			CloseableHttpResponse response = httpclient.execute(httpPost);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String sResult = EntityUtils.toString(response.getEntity());
				HttpEntity entity = response.getEntity();
				EntityUtils.consume(entity);

				return sResult;
			} else {
				String sResult = EntityUtils.toString(response.getEntity());

				JSONObject json = new JSONObject();
				json.put("code", "999");
				json.put("message", sResult);

				return json.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();

			JSONObject json = new JSONObject();
			json.put("code", "999");
			json.put("message", e.getMessage());

			return json.toString();
		}
	}

	// wideshot
	public static String SendMMS(String sejongApiKey, String userKey, String sender, String recver, String content,
			String advertisementYn) {
		// String URL = "https://api-dev.wideshot.co.kr/api/v1/message/sms";
		// //개발서버
		String URL = "https://api.wideshot.co.kr/api/v1/message/mms"; // 운영서버

		try {
			/*
			 * HTTP
			 */
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			/*
			 * HTTPS
			 */
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpPost httpPost = new HttpPost(URL);
			httpPost.setHeader("sejongApiKey", sejongApiKey);
			// httpPost.setHeader("User-Agent", "Mozilla/5.0");
			// httpPost.setHeader("User-Agent", "en-US,en;q=0.5");

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("callback", sender));
			nvps.add(new BasicNameValuePair("contents", content));
			nvps.add(new BasicNameValuePair("receiverTelNo", recver));
			nvps.add(new BasicNameValuePair("userKey", userKey));
			nvps.add(new BasicNameValuePair("advertisementYn", advertisementYn));

			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			CloseableHttpResponse response = httpclient.execute(httpPost);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String sResult = EntityUtils.toString(response.getEntity());
				HttpEntity entity = response.getEntity();
				EntityUtils.consume(entity);

				return sResult;
			} else {
				String sResult = EntityUtils.toString(response.getEntity());

				JSONObject json = new JSONObject();
				json.put("code", "999");
				json.put("message", sResult);

				return json.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();

			JSONObject json = new JSONObject();
			json.put("code", "999");
			json.put("message", e.getMessage());

			return json.toString();
		}
	}

	public static String SendMMS(String sejongApiKey, String userKey, String sender, String recver, String content,
			String advertisementYn, String title, File imageFile1) {
		// String URL = "https://api-dev.wideshot.co.kr/api/v1/message/sms";
		// //개발서버
		String URL = "https://api.wideshot.co.kr/api/v1/message/mms"; // 운영서버

		try {
			/*
			 * HTTP
			 */
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			/*
			 * HTTPS
			 */
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpPost httpPost = new HttpPost(URL);
			httpPost.setHeader("sejongApiKey", sejongApiKey);

			/*
			 * List <NameValuePair> nvps = new ArrayList<NameValuePair>();
			 * nvps.add(new BasicNameValuePair("callback", sender));
			 * nvps.add(new BasicNameValuePair("contents", content));
			 * nvps.add(new BasicNameValuePair("receiverTelNo", recver));
			 * nvps.add(new BasicNameValuePair("userKey", userKey));
			 * nvps.add(new BasicNameValuePair("advertisementYn",
			 * advertisementYn)); nvps.add(new BasicNameValuePair("title",
			 * title));
			 * 
			 * httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			 */

			MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
			multipartEntity.setCharset(Charset.forName("utf-8"));
			multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			multipartEntity.addPart("callback", new StringBody(sender, ContentType.APPLICATION_JSON));
			multipartEntity.addPart("contents", new StringBody(content, ContentType.APPLICATION_JSON));
			multipartEntity.addPart("receiverTelNo", new StringBody(recver, ContentType.APPLICATION_JSON));
			multipartEntity.addPart("userKey", new StringBody(userKey, ContentType.APPLICATION_JSON));
			multipartEntity.addPart("advertisementYn", new StringBody(advertisementYn, ContentType.APPLICATION_JSON));
			multipartEntity.addPart("title", new StringBody(title, ContentType.APPLICATION_JSON));
			multipartEntity.addBinaryBody("imageFile1", imageFile1);

			httpPost.setEntity(multipartEntity.build());

			CloseableHttpResponse response = httpclient.execute(httpPost);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String sResult = EntityUtils.toString(response.getEntity());
				HttpEntity entity = response.getEntity();
				EntityUtils.consume(entity);

				return sResult;
			} else {
				String sResult = EntityUtils.toString(response.getEntity());

				JSONObject json = new JSONObject();
				json.put("code", "999");
				json.put("message", sResult);

				return json.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();

			JSONObject json = new JSONObject();
			json.put("code", "999");
			json.put("message", e.getMessage());

			return json.toString();
		}
	}

	// wideshot
	public static String ResultSMS(String sejongApiKey, String sendCode) {
		// String URL = "https://api-dev.wideshot.co.kr/api/v1/message/result";
		// //개발서버
		String URL = "https://api.wideshot.co.kr/api/v1/message/result?sendCode=" + sendCode; // 운영서버

		try {
			/*
			 * HTTP
			 */
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			/*
			 * HTTPS
			 */
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpGet httpGet = new HttpGet(URL);
			httpGet.setHeader("sejongApiKey", sejongApiKey);

			CloseableHttpResponse response = httpclient.execute(httpGet);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String sResult = EntityUtils.toString(response.getEntity());
				HttpEntity entity = response.getEntity();
				EntityUtils.consume(entity);

				return sResult;
			} else {
				String sResult = EntityUtils.toString(response.getEntity());

				JSONObject json = new JSONObject();
				json.put("code", "999");
				json.put("message", sResult);

				return json.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();

			JSONObject json = new JSONObject();
			json.put("code", "999");
			json.put("message", e.getMessage());

			return json.toString();
		}
	}
	//

	public static String getRandomKey() {
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdf.applyPattern("yyMMdd");

		String sKey = sdf.format(new Date());
		sKey += "_" + UUID.randomUUID().toString().replaceAll("-", "");

		return sKey;
	}

	public static String subStringBytes(String str, int byteLength, int sizePerLetter) {
		if (str.getBytes().length <= byteLength) {
			return str;
		}

		int retLength = 0;
		int tempSize = 0;
		int asc;
		if (str == null || "".equals(str) || "null".equals(str)) {
			str = "";
		}

		int length = str.length();

		for (int i = 1; i <= length; i++) {
			asc = (int) str.charAt(i - 1);
			if (asc > 127) {
				if (byteLength >= tempSize + sizePerLetter) {
					tempSize += sizePerLetter;
					retLength++;
				}
			} else {
				if (byteLength > tempSize) {
					tempSize++;
					retLength++;
				}
			}
		}

		return str.substring(0, retLength) + "...";
	}

	public static String getDomain(HttpServletRequest request) {
		String sScheme = request.getScheme();
		String sServerName = request.getServerName();
		String sServerPort = String.valueOf(request.getServerPort());

		String sDomain = String.format("%s://%s:%s", sScheme, sServerName, sServerPort);
		if (sServerPort.equals("80")) {
			sDomain = String.format("%s://%s", sScheme, sServerName);
		}

		return sDomain;
	}

	public static boolean isFloat(String sNumber) {
		int nTextSize = sNumber.length();

		int nCheck = 0;
		for (int i = 0; i < nTextSize; i++) {
			if (sNumber.charAt(i) == '1' || sNumber.charAt(i) == '2' || sNumber.charAt(i) == '3'
					|| sNumber.charAt(i) == '4' || sNumber.charAt(i) == '5' || sNumber.charAt(i) == '6'
					|| sNumber.charAt(i) == '7' || sNumber.charAt(i) == '8' || sNumber.charAt(i) == '9'
					|| sNumber.charAt(i) == '0'|| sNumber.charAt(i) == '.') {
				nCheck++;
			}
		}

		if (nTextSize == nCheck) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNumber(String sNumber) {
		int nTextSize = sNumber.length();

		int nCheck = 0;
		for (int i = 0; i < nTextSize; i++) {
			if (sNumber.charAt(i) == '1' || sNumber.charAt(i) == '2' || sNumber.charAt(i) == '3'
					|| sNumber.charAt(i) == '4' || sNumber.charAt(i) == '5' || sNumber.charAt(i) == '6'
					|| sNumber.charAt(i) == '7' || sNumber.charAt(i) == '8' || sNumber.charAt(i) == '9'
					|| sNumber.charAt(i) == '0') {
				nCheck++;
			}
		}

		if (nTextSize == nCheck) {
			return true;
		} else {
			return false;
		}
	}

	public static int onlyNumber(String sNumber) {
		int nTextSize = sNumber.length();
		String sTemp = "";
		int nCheck = 0;
		for (int i = 0; i < nTextSize; i++) {
			if (sNumber.charAt(i) == '1' || sNumber.charAt(i) == '2' || sNumber.charAt(i) == '3'
					|| sNumber.charAt(i) == '4' || sNumber.charAt(i) == '5' || sNumber.charAt(i) == '6'
					|| sNumber.charAt(i) == '7' || sNumber.charAt(i) == '8' || sNumber.charAt(i) == '9'
					|| sNumber.charAt(i) == '0') {
				sTemp += sNumber.charAt(i);
			}
		}

		return CommonUtil.parseInt(sTemp);
	}

	public static boolean isNumber(String sNumber, int nLength) {
		int nTextSize = sNumber.length();
		if (nLength != nTextSize) {
			return false;
		}

		int nCheck = 0;
		for (int i = 0; i < nTextSize; i++) {
			if (sNumber.charAt(i) == '1' || sNumber.charAt(i) == '2' || sNumber.charAt(i) == '3'
					|| sNumber.charAt(i) == '4' || sNumber.charAt(i) == '5' || sNumber.charAt(i) == '6'
					|| sNumber.charAt(i) == '7' || sNumber.charAt(i) == '8' || sNumber.charAt(i) == '9'
					|| sNumber.charAt(i) == '0') {
				nCheck++;
			}
		}

		if (nTextSize == nCheck) {
			return true;
		} else {
			return false;
		}
	}

	public static String defaultString(String str1, String str2) {
		if (isNull(str1) || str1.equals("null")) {
			return str2;
		} else {
			return String.valueOf(str1);
		}
	}

	public static int parseInt(String str) {
		int nInt = 0;

		try {
			nInt = Integer.parseInt(str);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return nInt;
	}
	
	public static float parseFloat(String str) {
		float fFloat = 0;

		try {
			fFloat = Float.parseFloat(str);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return fFloat;
	}

	public static HttpServletRequest getCurrentRequest() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		HttpServletRequest servletRequest = sra.getRequest();
		return servletRequest;
	}

	public static String xssEscape(String str) {
		StringBuffer escapedStr = new StringBuffer();
		char[] ch = str.toCharArray();
		int charSize = ch.length;

		for (int i = 0; i < charSize; i++) {
			if (ch[i] == '<') {
				escapedStr.append("&lt;");
			} else if (ch[i] == '>') {
				escapedStr.append("&gt;");
			} else if (ch[i] == '"') {
				escapedStr.append("&quot;");
			} else if (ch[i] == '&') {
				escapedStr.append("&amp;");
			} else if (ch[i] == '#') {
				escapedStr.append("&#35;");
			} else if (ch[i] == '(') {
				escapedStr.append("&#40;");
			} else if (ch[i] == ')') {
				escapedStr.append("&#41;");
			} else if (ch[i] == '\'') {
				escapedStr.append("&#39;");
			} else {
				escapedStr.append(ch[i]);
			}
		}
		return escapedStr.toString();
	}

	public static String RemoveTag(String text) {
		int nStart = -1;
		int nEnd = -1;

		nStart = text.indexOf("<");

		while (nStart > -1) {
			nEnd = text.indexOf(">", nStart + 1);
			if (nEnd > -1) {
				String temp1 = text.substring(0, nStart);
				String temp2 = text.substring(nEnd + 1);

				text = temp1 + temp2;
				nStart = text.indexOf("<");
			} else {
				break;
			}
		}

		return text.trim();
	}

	public static String encodingPwd(String pwd) {
		BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder(-1);
		pwd = bcryptPasswordEncoder.encode(pwd);

		return pwd;
	}

	public static boolean matchingPwd(String pwd, String encodedPassword) {
		BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder(-1);
		if (bcryptPasswordEncoder.matches(pwd, encodedPassword)) {
			return true;
		} else {
			return false;
		}
	}

	public static void testCharSet(String text) {
		String[] charset = { "euc-kr", "ksc5601", "x-windows-949", "iso-8859-1", "utf-8", "8859_1" };

		for (int i = 0; i < charset.length; i++) {
			for (int j = 0; j < charset.length; j++) {
				try {
					String temp = new String(text.getBytes(charset[i]), charset[j]);
					System.out.println(String.format("%s %s =>%s", charset[i], charset[j], temp));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
	}
	
	public static String getLastDate(String date) {
		String[] dates = date.split("-");
		
		Calendar cal = Calendar.getInstance();

		cal.set(CommonUtil.parseInt(dates[0]), CommonUtil.parseInt(dates[1]) - 1, CommonUtil.parseInt(dates[2]));

		return String.format("%s-%s-%02d", dates[0], dates[1], cal.getActualMaximum(Calendar.DAY_OF_MONTH));
	}	
}
