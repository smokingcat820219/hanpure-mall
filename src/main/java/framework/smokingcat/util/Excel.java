package framework.smokingcat.util;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * <h1>Excel library</h1>
 * 엑셀 파일 저장시 컨트롤 가능하도록 만든 클래스
 * <p>
 * <b>Note:</b>
 *
 * @author Jungmin Oh
 * @since 2018-08-17
 */
public class Excel {
	public Workbook workbook;
	public Sheet worksheet;
	public Row row;
	//public Palette palette;
	public Font font;
	public CellStyle headerStyle;
	public CellStyle bodyStyle;
	public int rowIdx = 0;
	public float height = 20;
	public int columnCount;
	public Cell[] cell;

	public Excel(final Workbook workbook) {
		this.workbook = workbook;
		
		createSheet();
	}

	public void createSheet() {
		this.worksheet = this.workbook.createSheet("Sheet1");

		this.font = this.worksheet.getWorkbook().createFont();
		this.font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		this.headerStyle = this.worksheet.getWorkbook().createCellStyle();
		this.headerStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		this.headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		this.headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		this.headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		this.headerStyle.setWrapText(true);
		this.headerStyle.setFont(this.font);
		this.headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		this.headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		this.headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		this.headerStyle.setBorderBottom(CellStyle.BORDER_THIN);

		this.bodyStyle = this.worksheet.getWorkbook().createCellStyle();
		this.bodyStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		this.bodyStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		this.bodyStyle.setAlignment(CellStyle.ALIGN_CENTER);
		this.bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		this.bodyStyle.setWrapText(true);
		this.bodyStyle.setFont(this.font);
		this.bodyStyle.setBorderLeft(CellStyle.BORDER_THIN);
		this.bodyStyle.setBorderTop(CellStyle.BORDER_THIN);
		this.bodyStyle.setBorderRight(CellStyle.BORDER_THIN);
		this.bodyStyle.setBorderBottom(CellStyle.BORDER_THIN);
	}

	public void SetColumnCount(final int columnCount) {
		this.columnCount = columnCount;
		this.cell = new Cell[columnCount];

		for (int i = 0; i < columnCount; i++) {
			this.worksheet.setColumnWidth(i, 5000);
		}
	}

	public void addHeader() {
		this.row = this.worksheet.createRow(this.rowIdx);
		this.row.setHeightInPoints(this.height);

		for (int i = 0; i < this.columnCount; i++) {
			this.cell[i] = this.row.createCell(i);
			this.cell[i].setCellStyle(this.headerStyle);
		}

		this.rowIdx++;
	}
	
	public void addHeader(int count) {
		this.row = this.worksheet.createRow(this.rowIdx);
		this.row.setHeightInPoints(this.height);

		for (int i = 0; i < count; i++) {
			this.cell[i] = this.row.createCell(i);
			this.cell[i].setCellStyle(this.headerStyle);
		}

		this.rowIdx++;
	}

	public void setBodyStyle(int idx) {
		this.cell[idx].setCellStyle(this.bodyStyle);
	}
	
	public void addBlankRow() {
		this.row = this.worksheet.createRow(this.rowIdx);
		this.row.setHeightInPoints(this.height);

		for (int i = 0; i < this.columnCount; i++) {
			this.cell[i] = this.row.createCell(i);
		}

		this.rowIdx++;
	}
	
	public void addRow() {
		this.row = this.worksheet.createRow(this.rowIdx);
		this.row.setHeightInPoints(this.height);

		for (int i = 0; i < this.columnCount; i++) {
			this.cell[i] = this.row.createCell(i);
			this.cell[i].setCellStyle(this.bodyStyle);
		}

		this.rowIdx++;
	}
	
	public void addRow(int count) {
		this.row = this.worksheet.createRow(this.rowIdx);
		this.row.setHeightInPoints(this.height);

		for (int i = 0; i < count; i++) {
			this.cell[i] = this.row.createCell(i);
			this.cell[i].setCellStyle(this.bodyStyle);
		}

		this.rowIdx++;
	}

	public void setText(final String[] text) {
		for (int i = 0; i < text.length; i++) {
			this.cell[i].setCellValue(text[i]);
		}
	}

	public void setValue(final int col, final String value) {
		this.cell[col].setCellValue(value);
	}

	public void setValue(final int col, final double value) {
		this.cell[col].setCellValue(value);
	}

	public void save(final HttpServletResponse response, String excelName) {
		try {
			excelName = URLEncoder.encode(excelName, "UTF-8");
			excelName = excelName.replaceAll("[+]", " ");
		} catch (Exception e) {
			e.printStackTrace();
		}

		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "ATTachment; Filename=" + excelName + ".xlsx");
	}

	public void cellSpan(final String[] span) {
		for (int i = 0; i < span.length; i++) {
			this.worksheet.addMergedRegion(CellRangeAddress.valueOf(span[i]));
		}
	}

	public void colSpan(final int from, final int to) {
		this.worksheet.addMergedRegion(new CellRangeAddress(this.rowIdx - 1, this.rowIdx - 1, from, to));
	}

	public void rowSpan(final int current) {
		this.worksheet.addMergedRegion(new CellRangeAddress(this.rowIdx - 1, this.rowIdx, current, current));
	}
	
	public void addMergedRegion(int row1, int row2, int col1, int col2) {
		this.worksheet.addMergedRegion(new CellRangeAddress(row1, row2, col1, col2));
	}
}