package framework.smokingcat.taglib;

import java.io.IOException;
import java.text.DecimalFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import framework.smokingcat.util.CommonUtil;

public class TagLibDisplay extends TagSupport {
	private static final String BIZID = "BIZID";
	private static final String BIRTH = "BIRTH";
	private static final String TEL = "TEL";
	private static final String MOBILE = "MOBILE";	
	private static final String BR = "BR";
	private static final String ENG_DATE = "ENG_DATE";
	private static final String JUMIN = "JUMIN";
	private static final String EDITOR = "EDITOR";
	private static final String NUMBER = "NUMBER";
	private static final String INTEGER = "INTEGER";
	private static final String SEX = "SEX";
	private static final String AGE = "AGE";
	private static final String EMAIL1 = "EMAIL1";
	private static final String EMAIL2 = "EMAIL2";
	private static final String MANWON = "MANWON";
	private static final String NULL = "NULL";
	private static final String DATE10 = "DATE10";
	private static final String DATE19 = "DATE19";
	private static final String TEL1 = "TEL1";
	private static final String TEL2 = "TEL2";
	private static final String TEL3 = "TEL3";
	
	private static final long serialVersionUID = 6993049271016443497L;
	private String type;
	private String value;

	@Override
	public int doStartTag() throws JspException {
		try {
			JspWriter out = this.pageContext.getOut();

			String returnValue = this.setMasking(this.type, this.value);
			out.print(returnValue);
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return SKIP_BODY;
	}

	public String setMasking(String type,  String strValue) {
		String returnValue = "";

		if (type.equals(BIZID)) {
			return this.getBizID(strValue);
		} 
		else if (type.equals(BIRTH)) {
			return this.getBirth(strValue);
		}
		else if(type.equals(TEL)) {
			return this.getTel(strValue);
		} 
		else if(type.equals(MOBILE)) {
			return this.getMobile(strValue);
		} 		
		else if(type.equals(BR)) {
			return this.getBR(strValue);
		} 
		else if(type.equals(ENG_DATE)) {
			return this.getEngDate(strValue);
		} 	
		else if(type.equals(JUMIN)) {
			return this.getJumin(strValue);
		} 	
		else if (type.equals(EDITOR)) {
			return this.getEditor(strValue);
		} 			
		else if (type.equals(NUMBER)) {
			return this.getNumber(strValue);
		}
		else if (type.equals(INTEGER)) {
			return this.getInteger(strValue);
		} 
		else if (type.equals(SEX)) {
			return this.getSex(strValue);
		} 
		else if (type.equals(AGE)) {
			return this.getAge(strValue);
		}
		else if (type.equals(EMAIL1)) {
			return this.getEmail1(strValue);
		}
		else if (type.equals(EMAIL2)) {
			return this.getEmail2(strValue);
		}
		else if (type.equals(TEL1)) {
			return this.getTel1(strValue);
		}
		else if (type.equals(TEL2)) {
			return this.getTel2(strValue);
		}
		else if (type.equals(TEL3)) {
			return this.getTel3(strValue);
		}
		else if (type.equals(MANWON)) {
			return this.getManwon(strValue);
		}
		else if (type.equals(NULL)) {
			return this.getNull(strValue);
		}
		else if (type.equals(DATE10)) {
			return this.getDate10(strValue);
		}
		else if (type.equals(DATE19)) {
			return this.getDate19(strValue);
		}
		
		
		
		
		return returnValue;
	}
	
	public String getEmail1(String email) {
		try {
			String[] emails = email.split("@");
			return emails[0];
		}catch(Exception e) {
			//e.printStackTrace();
			return "";
		}
	}
	
	public String getEmail2(String email) {
		try {
			String[] emails = email.split("@");
			return emails[1];
		}catch(Exception e) {
			//e.printStackTrace();
			return "";
		}
	}
	
	public String getTel1(String tel) {
		try {
			tel = getTel(tel);
			
			String[] tels = tel.split("-");
			return tels[0];
		}catch(Exception e) {
			//e.printStackTrace();
			return "";
		}
	}
	
	public String getTel2(String tel) {
		try {
			tel = getTel(tel);
			
			String[] tels = tel.split("-");
			return tels[1];
		}catch(Exception e) {
			//e.printStackTrace();
			return "";
		}
	}
	
	public String getTel3(String tel) {
		try {
			tel = getTel(tel);
			
			String[] tels = tel.split("-");
			return tels[2];
		}catch(Exception e) {
			//e.printStackTrace();
			return "";
		}
	}
	
	public String getAge(String birth) {
		try {
			String year = birth.substring(0, 4);
			return String.valueOf(Integer.parseInt(CommonUtil.getYear()) - Integer.parseInt(year) + 1);
		}catch(Exception e) {
			return "";
		}
	}
	
	public String getSex(String sex) {
		if(sex.equals("M")) {
			return "남";
		}
		else if(sex.equals("F")) {
			return "여";
		}
		else if(sex.equals("A")) {
			return "성별무관";
		}
		else {
			return "";
		}
	}
	
	public String getBizID(String businessNum) {
		String returnBusinessNum = "";
		
		if(businessNum.length() == 9) {
			businessNum = businessNum.replaceAll("-",  "");

			try {
				String temp1 = businessNum.substring(0, 2);
				String temp2 = businessNum.substring(2, 5);
				String temp3 = businessNum.substring(5, 9);

				returnBusinessNum = temp1 + "-" + temp2 + "-" + temp3;
			} catch (Exception e) {
				//e.printStackTrace();
			}
			
			return returnBusinessNum;
		}
		else if(businessNum.length() == 10) {
			businessNum = businessNum.replaceAll("-",  "");

			try {
				String temp1 = businessNum.substring(0, 3);
				String temp2 = businessNum.substring(3, 5);
				String temp3 = businessNum.substring(5, 10);

				returnBusinessNum = temp1 + "-" + temp2 + "-" + temp3;
			} catch (Exception e) {
				//e.printStackTrace();
			}
			
			return returnBusinessNum;
		}
		else {			
			return businessNum;
		}
	}
	
	public String getBirth(String birth) {
		String returnBirth = "";
		
		if(birth.length() < 8) {
			return birth;
		}
		else {
			birth = birth.replaceAll("-",  "");

			try {
				String temp1 = birth.substring(0, 4);
				String temp2 = birth.substring(4, 6);
				String temp3 = birth.substring(6, 8);

				returnBirth = temp1 + "-" + temp2 + "-" + temp3;
			} catch (Exception e) {
				//e.printStackTrace();
			}
			
			return returnBirth;
		}
	}

	public String getTel(String tel) {
		try {
			if(tel == null || tel.equals("")) {
				return "";
			}
			
			tel = tel.replaceAll(" ", "");
			tel = tel.replaceAll("-", "");			
			tel = tel.replaceAll("\\)", "");
			tel = tel.replaceAll("\\(", "");
			tel = tel.replaceAll("\\+82", "0");
			
			if(tel.length() < 9) {
				if(tel.length() > 4) {
					String tel1 = "02";
					String tel2 = tel.substring(0, 4);
					String tel3 = tel.substring(tel.length() - 4, tel.length());
					
					return tel1 + "-" + tel2 + "-" + tel3;
				}
				
				return tel;
			}
			
			String front = tel.substring(0,  2);
			String front2 = tel.substring(0,  3);
			
			if(front.equals("02")) {
				String tel1 = tel.substring(0, 2);
				String tel2 = tel.substring(2, tel.length() - 4);
				String tel3 = tel.substring(tel.length() - 4, tel.length());		
		
				return tel1 + "-" + tel2 + "-" + tel3;
			}
			else if(front2.equals("051") ||
					front2.equals("053") ||
					front2.equals("032") ||
					front2.equals("062") ||
					front2.equals("042") ||
					front2.equals("052") ||
					front2.equals("044") ||
					front2.equals("031") ||
					front2.equals("033") ||
					front2.equals("043") ||
					front2.equals("041") ||
					front2.equals("063") ||
					front2.equals("061") ||
					front2.equals("054") ||
					front2.equals("055") ||
					front2.equals("064") ||
					front2.equals("070") ||
					front2.equals("010") || 
					front2.equals("011") || 
					front2.equals("016") ||
					front2.equals("017") ||
					front2.equals("018") ||
					front2.equals("019")) {
				
				String tel1 = tel.substring(0, 3);
				String tel2 = tel.substring(3, tel.length() - 4);
				String tel3 = tel.substring(tel.length() - 4, tel.length());		
		
				return tel1 + "-" + tel2 + "-" + tel3;
			}
			else {
				return getMobile(tel);
			}
		}catch(Exception e) {
			return tel;
		}
	}
	
	public String getMobile(String mobile) {
		try {
			if(mobile == null || mobile.equals("")) {
				return "";
			}
			
			mobile = mobile.replaceAll("-", "");
			
			if(mobile.length() < 10) {
				return mobile;
			}
			
			String mobile1 = mobile.substring(0, 3);
			String mobile2 = mobile.substring(3, mobile.length() - 4);
			String mobile3 = mobile.substring(mobile.length() - 4, mobile.length());		
	
			return mobile1 + "-" + mobile2 + "-" + mobile3;
		}catch(Exception e) {
			return mobile;
		}
	}
	
	public String getBR(String strValue) {
		try {	
			strValue = strValue.replaceAll("\r\n", "<br/>");
			strValue = strValue.replaceAll("\n", "<br/>");
		} catch (Exception e) {
			//e.printStackTrace();
		}

		return strValue;
	}
	
	public String getEngDate(String strValue) {
		if(CommonUtil.isNull(strValue)) return "";
		
		String year = strValue.substring(0, 4);
		String month = strValue.substring(5, 7);
		String day = strValue.substring(8, 10);
				
		if(month.equals("01")) {
			month = "Jan";
		} else if(month.equals("02")) {
			month = "Feb";
		} else if(month.equals("03")) {
			month = "Mar";
		} else if(month.equals("04")) {
			month = "Apr";
		} else if(month.equals("05")) {
			month = "May";
		} else if(month.equals("06")) {
			month = "Jun";
		} else if(month.equals("07")) {
			month = "Jul";
		} else if(month.equals("08")) {
			month = "Aug";
		} else if(month.equals("09")) {
			month = "Sep";
		} else if(month.equals("10")) {
			month = "Oct";
		} else if(month.equals("11")) {
			month = "Nov";
		} else if(month.equals("12")) {
			month = "Dec";
		}			
		
		return month + " " + day + "," + year;
	}
	
	public String getJumin(String jumin) {
		String returnJumin = "";
		
		if(jumin.length() < 13) {
			return jumin;
		}
		else {
			jumin = jumin.replaceAll("-",  "");

			try {
				String temp1 = jumin.substring(0, 6);
				String temp2 = jumin.substring(6, 13);

				returnJumin = temp1 + "-" + temp2;
			} catch (Exception e) {
				//e.printStackTrace();
			}
			
			return returnJumin;
		}
	}
	
	public String getEditor(final String smart) {
		String returnValue = "";
		returnValue = smart;

		returnValue = returnValue.replaceAll("&amp;", "&");
		returnValue = returnValue.replaceAll("&lt;", "<");
		returnValue = returnValue.replaceAll("&gt;", ">");
		returnValue = returnValue.replaceAll("&quot;", "\"");		
		returnValue = returnValue.replaceAll("&#35;", "#");
		returnValue = returnValue.replaceAll("&#40;", "(");
		returnValue = returnValue.replaceAll("&#41;", ")");		
		returnValue = returnValue.replaceAll("&nbsp;", " ");	
		
		return returnValue;
	}	
	
	public String getNumber(final String number) {
		String returnNumber = "0";
		
		if(number == null || number.equals("")) {
			return returnNumber;
		}		
				
		try {
			int idx = number.indexOf(".");
			
			if(idx > -1) {
				String[] numbers = number.split("\\.");				
				
				DecimalFormat df = new DecimalFormat("#,##0");				
				
				if(numbers[1].equals(".00")) {
					returnNumber = df.format(Integer.parseInt(numbers[0]));
					return returnNumber;
				}
				else if(numbers[1].equals(".0")) {
					returnNumber = df.format(Integer.parseInt(numbers[0]));
					return returnNumber;
				}
				else {
					if(numbers[1].length() > 2) {
						returnNumber = df.format(Integer.parseInt(numbers[0])) + "." + numbers[1].substring(0, 2);
						return returnNumber;
					}
					else {
						returnNumber = df.format(Integer.parseInt(numbers[0])) + "." + numbers[1];
						return returnNumber;
					}
				}
			}
			else {
				DecimalFormat df = new DecimalFormat("#,##0");
				returnNumber = df.format(Integer.parseInt(number));
				return returnNumber;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			
			return "0";
		}	
	}
	
	public String getInteger(final String number) {
		String returnNumber = "0";
		
		if(number == null || number.equals("")) {
			return returnNumber;
		}		
				
		try {
			String str = number;
			int idx = str.indexOf(".");
			
			if(idx > -1) {
				str = str.substring(0,  idx);
				
				DecimalFormat df = new DecimalFormat("#,##0");
				
				if(number.substring(idx).equals(".0")) {
					returnNumber = str;
					return returnNumber;
				}
				else {
					returnNumber = str + number.substring(idx, idx + 3);
					return returnNumber;
				}
			}
			else {
				returnNumber = str;
				return returnNumber;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			
			return "0";
		}		
	}
	
	public String getManwon(final String number) {
		String returnNumber = "0만";
		
		if(number == null || number.equals("")) {
			return returnNumber;
		}		
				
		try {			
			float fNumber = Float.parseFloat(number);
			fNumber = fNumber / 10000.0f;
			
			String str = String.format("%.1f", fNumber) + "만";
			str = str.replaceAll("\\.0", "");
			return str;
		} catch (Exception e) {
			//e.printStackTrace();
			
			return returnNumber;
		}
	}
	
	public String getNull(final String str) {
		if(CommonUtil.isNull(str)) {
			return "-";
		}
		else {
			return str;
		}
	}
	public String getDate10(final String date) {
		if(date.length() > 10) {
			return date.substring(0, 10);
		}
		else {
			return date;
		}
	}
	public String getDate19(final String date) {
		if(date.length() > 19) {
			return date.substring(0, 19);
		}
		else {
			return date;
		}
	}
	
	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(final String value) {
		this.value = value;
	}	
}
