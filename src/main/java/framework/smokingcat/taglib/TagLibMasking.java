package framework.smokingcat.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class TagLibMasking extends TagSupport {
	private static final String ID = "ID";
	private static final String NAME = "NAME";
	private static final String EMAIL = "EMAIL";
	
	/**
	 *
	 */
	private static final long serialVersionUID = 6993049271016443497L;
	private String type;
	private String value;

	@Override
	public int doStartTag() throws JspException {
		try {
			JspWriter out = this.pageContext.getOut();

			String returnValue = this.setMasking(this.type, this.value);
			out.print(returnValue);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	public String setMasking(final String type, final String strValue) {
		String returnValue = "";

		if (type.equals(ID)) {
			return this.getId(strValue);
		} 
		else if (type.equals(NAME)) {
			return this.getName(strValue);
		} 
		else if (type.equals(EMAIL)) {
			return this.getEmail(strValue);
		}
		else {
			returnValue = "";
		}

		return returnValue;
	}

	public String getId(final String id) {
		String returnValue = "";

		if (id.length() > 2) {
			returnValue = id.substring(0, 2);

			for (int i = 0; i < id.length() - 2; i++) {
				returnValue += "*";
			}
		} else {
			try {
				returnValue = id.substring(0, 1);

				for (int i = 1; i < id.length(); i++) {
					returnValue += "*";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return returnValue;
	}

	public String getName(final String name) {
		String returnValue = "";

		if(name.length() > 4) {
			returnValue += name.substring(0, name.length() - 3) + "***";
		}
		else {
			for (int i = 0; i < name.length(); i++) {
				if (i % 2 == 0) {
					returnValue += name.substring(i, i + 1);
				} else {
					returnValue += "*";
				}
			}
		}		

		return returnValue;
	}	

	public String getEmail(final String email) {
		String returnValue = "";
		int idx = email.indexOf("@");

		if (idx > -1) {
			String address = email.substring(0, idx);
			String domain = email.substring(idx, email.length());
			
			for (int i = 0; i < address.length(); i++) {				
				if(i < 2) {
					returnValue += address.substring(i, i + 1);
				}
				else {
					returnValue += "*";
				}
			}

			returnValue += domain;
		} else {
			for (int i = 0; i < email.length(); i++) {
				if (i % 2 == 0) {
					returnValue += email.substring(i, i + 1);
				} else {
					returnValue += "*";
				}
			}
		}

		return returnValue;
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(final String value) {
		this.value = value;
	}	
}
