package kr.co.hanpure.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import framework.smokingcat.logger.CurrentTime;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.ApplicationContextHolder;
import kr.co.hanpure.common.CommonCode;
import kr.co.hanpure.database.log.LogService;
import kr.co.hanpure.database.user.UserVO;

public class InterceptorEx extends HandlerInterceptorAdapter {	
	private static final Logger logger = LoggerFactory.getLogger(InterceptorEx.class);
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		/*
		UserVO SESSION = (UserVO)request.getSession().getAttribute(CommonCode.USER_SESSION_KEY);
		if(SESSION != null) {			
			//if(request.getMethod().equals("GET")) {
				String url = request.getRequestURI().toString();
				LogService logService = ApplicationContextHolder.getContext().getBean(LogService.class);				
				logService.insertLog(url, request, SESSION);
			//}			
		}
		*/
		
		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
}
