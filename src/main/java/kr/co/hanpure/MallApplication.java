package kr.co.hanpure;

import javax.servlet.http.HttpSessionListener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

import kr.co.hanpure.config.SessionListener;

@EnableAsync
@SpringBootApplication
@ComponentScan({"kr.co.hanpure", "framework.smokingcat"})
public class MallApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(MallApplication.class, args);
	}
	
	@Bean
	public HttpSessionListener httpSessionListener(){
		return new SessionListener();
	}
}
