package kr.co.hanpure.database.bom;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface BomMapper {	
	void insertData(BomVO vo);
	void updateData(BomVO vo);
	
	BomVO selectData(BomVO vo);
	List<BomVO> selectDatas(BomVO vo);
	Integer selectDataCount(BomVO vo);
	List<BomVO> selectDataList(BomVO vo);
}
