package kr.co.hanpure.database.bom;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("BomVO")
public class BomVO extends Pagenation {
	private String BOM_SEQ;
	private String BOM_CD;
	private String BOM_NM;	
	private String BOM_TP;
	private String ECOUNT_CD;
	private String BOM_CNT;
	private String SET_YN;
	private String USE_YN;
	private String DEL_YN;	
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	///////
	private String BOM_TP_NM;
	private String FILEPATH;
	private List<String> LIST_BOM_TP = new ArrayList<String>();
	
	public String getBOM_SEQ() {
		return BOM_SEQ;
	}
	public void setBOM_SEQ(String bOM_SEQ) {
		BOM_SEQ = bOM_SEQ;
	}
	public String getBOM_CD() {
		return BOM_CD;
	}
	public void setBOM_CD(String bOM_CD) {
		BOM_CD = bOM_CD;
	}
	public String getBOM_NM() {
		return BOM_NM;
	}
	public void setBOM_NM(String bOM_NM) {
		BOM_NM = bOM_NM;
	}
	public String getBOM_TP() {
		return BOM_TP;
	}
	public void setBOM_TP(String bOM_TP) {
		BOM_TP = bOM_TP;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getBOM_TP_NM() {
		return BOM_TP_NM;
	}
	public void setBOM_TP_NM(String bOM_TP_NM) {
		BOM_TP_NM = bOM_TP_NM;
	}
	public String getECOUNT_CD() {
		return ECOUNT_CD;
	}
	public void setECOUNT_CD(String eCOUNT_CD) {
		ECOUNT_CD = eCOUNT_CD;
	}
	public String getBOM_CNT() {
		return BOM_CNT;
	}
	public void setBOM_CNT(String bOM_CNT) {
		BOM_CNT = bOM_CNT;
	}
	public String getSET_YN() {
		return SET_YN;
	}
	public void setSET_YN(String sET_YN) {
		SET_YN = sET_YN;
	}
	public String getFILEPATH() {
		return FILEPATH;
	}
	public void setFILEPATH(String fILEPATH) {
		FILEPATH = fILEPATH;
	}
	public List<String> getLIST_BOM_TP() {
		return LIST_BOM_TP;
	}
	public void setLIST_BOM_TP(List<String> lIST_BOM_TP) {
		LIST_BOM_TP = lIST_BOM_TP;
	}	
}
