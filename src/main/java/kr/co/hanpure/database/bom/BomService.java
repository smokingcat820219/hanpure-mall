package kr.co.hanpure.database.bom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class BomService {
	@Autowired
	private BomMapper mapper;
	
	public void insertData(BomVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(BomVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public BomVO selectData(BomVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<BomVO> selectDatas(BomVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(BomVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<BomVO> selectDataList(BomVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


