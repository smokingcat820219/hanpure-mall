package kr.co.hanpure.database.item;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ItemMapper {	
	void insertData(ItemVO vo);
	void updateData(ItemVO vo);
	ItemVO selectDataOne(ItemVO vo);
	ItemVO selectData(ItemVO vo);
	List<ItemVO> selectDatas(ItemVO vo);
	Integer selectDataCount(ItemVO vo);
	List<ItemVO> selectDataList(ItemVO vo);
}

