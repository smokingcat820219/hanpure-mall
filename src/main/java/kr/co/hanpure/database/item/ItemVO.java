package kr.co.hanpure.database.item;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ItemVO")
public class ItemVO extends Pagenation {
	private String ITEM_SEQ;
	private String ITEM_CD;
	private String ITEM_NM;
	private String ORIGIN;
	private String MAKER;
	private String SPEC;
	private String ECOUNT1_CD;
	private String ECOUNT2_CD;
	private String HOSPITAL_ID;	
	private String FILENAME;
	private String FILEPATH;
	private String REMARK;	
	private String WEIGHT;
	private String PRICE_A;
	private String PRICE_B;
	private String MAKE_PRICE;
	private String USE_YN;
	private String DEL_YN;	
	private String ITEM_TP;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	///
	private List<String> LIST_ITEM_CD = new ArrayList<String>();
	
	private String HOSPITAL_NM;
	private String ITEM_STATUS_NM;
	private String WH_CD;
	private String MIN_QNTT;	
	private String ITEM_STATUS;
	private String ECOUNT_CD;
	private String ITEM_TP_NM;
	
	
	public String getITEM_SEQ() {
		return ITEM_SEQ;
	}
	public void setITEM_SEQ(String iTEM_SEQ) {
		ITEM_SEQ = iTEM_SEQ;
	}
	public String getITEM_CD() {
		return ITEM_CD;
	}
	public void setITEM_CD(String iTEM_CD) {
		ITEM_CD = iTEM_CD;
	}
	public String getITEM_NM() {
		return ITEM_NM;
	}
	public void setITEM_NM(String iTEM_NM) {
		ITEM_NM = iTEM_NM;
	}
	public String getORIGIN() {
		return ORIGIN;
	}
	public void setORIGIN(String oRIGIN) {
		ORIGIN = oRIGIN;
	}
	public String getMAKER() {
		return MAKER;
	}
	public void setMAKER(String mAKER) {
		MAKER = mAKER;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getFILENAME() {
		return FILENAME;
	}
	public void setFILENAME(String fILENAME) {
		FILENAME = fILENAME;
	}
	public String getFILEPATH() {
		return FILEPATH;
	}
	public void setFILEPATH(String fILEPATH) {
		FILEPATH = fILEPATH;
	}
	public String getREMARK() {
		return REMARK;
	}
	public void setREMARK(String rEMARK) {
		REMARK = rEMARK;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getHOSPITAL_NM() {
		return HOSPITAL_NM;
	}
	public void setHOSPITAL_NM(String hOSPITAL_NM) {
		HOSPITAL_NM = hOSPITAL_NM;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getMIN_QNTT() {
		return MIN_QNTT;
	}
	public void setMIN_QNTT(String mIN_QNTT) {
		MIN_QNTT = mIN_QNTT;
	}
	public String getITEM_STATUS_NM() {
		return ITEM_STATUS_NM;
	}
	public void setITEM_STATUS_NM(String iTEM_STATUS_NM) {
		ITEM_STATUS_NM = iTEM_STATUS_NM;
	}
	public String getITEM_STATUS() {
		return ITEM_STATUS;
	}
	public void setITEM_STATUS(String iTEM_STATUS) {
		ITEM_STATUS = iTEM_STATUS;
	}
	public String getWH_CD() {
		return WH_CD;
	}
	public void setWH_CD(String wH_CD) {
		WH_CD = wH_CD;
	}	
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getPRICE_A() {
		return PRICE_A;
	}
	public void setPRICE_A(String pRICE_A) {
		PRICE_A = pRICE_A;
	}
	public String getPRICE_B() {
		return PRICE_B;
	}
	public void setPRICE_B(String pRICE_B) {
		PRICE_B = pRICE_B;
	}
	public String getMAKE_PRICE() {
		return MAKE_PRICE;
	}
	public void setMAKE_PRICE(String mAKE_PRICE) {
		MAKE_PRICE = mAKE_PRICE;
	}
	public String getECOUNT1_CD() {
		return ECOUNT1_CD;
	}
	public void setECOUNT1_CD(String eCOUNT1_CD) {
		ECOUNT1_CD = eCOUNT1_CD;
	}
	public String getECOUNT2_CD() {
		return ECOUNT2_CD;
	}
	public void setECOUNT2_CD(String eCOUNT2_CD) {
		ECOUNT2_CD = eCOUNT2_CD;
	}
	public List<String> getLIST_ITEM_CD() {
		return LIST_ITEM_CD;
	}
	public void setLIST_ITEM_CD(List<String> lIST_ITEM_CD) {
		LIST_ITEM_CD = lIST_ITEM_CD;
	}	
	public String getITEM_TP() {
		return ITEM_TP;
	}
	public void setITEM_TP(String iTEM_TP) {
		ITEM_TP = iTEM_TP;
	}
	public String getECOUNT_CD() {
		return ECOUNT_CD;
	}
	public void setECOUNT_CD(String eCOUNT_CD) {
		ECOUNT_CD = eCOUNT_CD;
	}
	public String getITEM_TP_NM() {
		return ITEM_TP_NM;
	}
	public void setITEM_TP_NM(String iTEM_TP_NM) {
		ITEM_TP_NM = iTEM_TP_NM;
	}
	
}
