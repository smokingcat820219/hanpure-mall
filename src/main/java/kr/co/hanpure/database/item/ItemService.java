package kr.co.hanpure.database.item;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ItemService {
	@Autowired
	private ItemMapper mapper;
	
	public void insertData(ItemVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(ItemVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public ItemVO selectDataOne(ItemVO vo) {
		Logger.info();
		return this.mapper.selectDataOne(vo);
	}
	
	public ItemVO selectData(ItemVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<ItemVO> selectDatas(ItemVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(ItemVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<ItemVO> selectDataList(ItemVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


