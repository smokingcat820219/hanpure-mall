package kr.co.hanpure.database.recipe_attach;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class RecipeAttachService {
	@Autowired
	private RecipeAttachMapper mapper;
	
	public void insertData(RecipeAttachVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteDataAll(String RECIPE_SEQ) {
		Logger.info();
		this.mapper.deleteDataAll(RECIPE_SEQ);
	}
	
	public RecipeAttachVO selectData(RecipeAttachVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<RecipeAttachVO> selectDatas(RecipeAttachVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
}


