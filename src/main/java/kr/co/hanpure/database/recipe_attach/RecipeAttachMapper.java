package kr.co.hanpure.database.recipe_attach;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface RecipeAttachMapper {	
	void insertData(RecipeAttachVO vo);
	void deleteDataAll(String RECIPE_SEQ);
	RecipeAttachVO selectData(RecipeAttachVO vo);
	List<RecipeAttachVO> selectDatas(RecipeAttachVO vo);
}

