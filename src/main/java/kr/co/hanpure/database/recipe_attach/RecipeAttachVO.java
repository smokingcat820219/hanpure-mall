package kr.co.hanpure.database.recipe_attach;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("RecipeAttachVO")
public class RecipeAttachVO extends Pagenation {
	private String RECIPE_ATTACH_SEQ;
	private String RECIPE_SEQ;
	private String DIVISION;
	private String FILEPATH;
	private String FILENAME;
	
	private List<String> LIST_HOWTOMAKE_FILENAME = new ArrayList<String>();
	private List<String> LIST_HOWTOMAKE_FILEPATH = new ArrayList<String>();
	private List<String> LIST_HOWTOEAT_FILENAME = new ArrayList<String>();
	private List<String> LIST_HOWTOEAT_FILEPATH = new ArrayList<String>();
	
	public String getRECIPE_ATTACH_SEQ() {
		return RECIPE_ATTACH_SEQ;
	}
	public void setRECIPE_ATTACH_SEQ(String rECIPE_ATTACH_SEQ) {
		RECIPE_ATTACH_SEQ = rECIPE_ATTACH_SEQ;
	}
	public String getRECIPE_SEQ() {
		return RECIPE_SEQ;
	}
	public void setRECIPE_SEQ(String rECIPE_SEQ) {
		RECIPE_SEQ = rECIPE_SEQ;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getFILEPATH() {
		return FILEPATH;
	}
	public void setFILEPATH(String fILEPATH) {
		FILEPATH = fILEPATH;
	}
	public String getFILENAME() {
		return FILENAME;
	}
	public void setFILENAME(String fILENAME) {
		FILENAME = fILENAME;
	}
	public List<String> getLIST_HOWTOMAKE_FILENAME() {
		return LIST_HOWTOMAKE_FILENAME;
	}
	public void setLIST_HOWTOMAKE_FILENAME(List<String> lIST_HOWTOMAKE_FILENAME) {
		LIST_HOWTOMAKE_FILENAME = lIST_HOWTOMAKE_FILENAME;
	}
	public List<String> getLIST_HOWTOEAT_FILENAME() {
		return LIST_HOWTOEAT_FILENAME;
	}
	public void setLIST_HOWTOEAT_FILENAME(List<String> lIST_HOWTOEAT_FILENAME) {
		LIST_HOWTOEAT_FILENAME = lIST_HOWTOEAT_FILENAME;
	}
	public List<String> getLIST_HOWTOMAKE_FILEPATH() {
		return LIST_HOWTOMAKE_FILEPATH;
	}
	public void setLIST_HOWTOMAKE_FILEPATH(List<String> lIST_HOWTOMAKE_FILEPATH) {
		LIST_HOWTOMAKE_FILEPATH = lIST_HOWTOMAKE_FILEPATH;
	}
	public List<String> getLIST_HOWTOEAT_FILEPATH() {
		return LIST_HOWTOEAT_FILEPATH;
	}
	public void setLIST_HOWTOEAT_FILEPATH(List<String> lIST_HOWTOEAT_FILEPATH) {
		LIST_HOWTOEAT_FILEPATH = lIST_HOWTOEAT_FILEPATH;
	}	
}
