package kr.co.hanpure.database.recipein;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class RecipeInService {
	@Autowired
	private RecipeInMapper mapper;
	
	public void insertData(RecipeInVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteDataAll(String RECIPE_SEQ) {
		Logger.info();
		this.mapper.deleteDataAll(RECIPE_SEQ);
	}
	
	public List<RecipeInVO> selectDatas(RecipeInVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
}


