package kr.co.hanpure.database.recipein;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface RecipeInMapper {	
	void insertData(RecipeInVO vo);
	void deleteDataAll(String RECIPE_SEQ);
	List<RecipeInVO> selectDatas(RecipeInVO vo);
}
