package kr.co.hanpure.database.recipein;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("RecipeInVO")
public class RecipeInVO extends Pagenation {
	private String RECIPE_SEQ;
	private String DRUG_CD;
	private String DRUG_NM;
	private String TURN;
	private String ORIGIN;
	private String PRICE;
	private String CHUP1;
	private String TOTAL_QNTT;
	private String TOTAL_PRICE;
	private String SORT;	
	
	private List<String> LIST_DRUG_CD = new ArrayList<String>();
	private List<String> LIST_DRUG_NM = new ArrayList<String>();
	private List<String> LIST_TURN = new ArrayList<String>();
	private List<String> LIST_ORIGIN = new ArrayList<String>();
	private List<String> LIST_PRICE = new ArrayList<String>();
	private List<String> LIST_CHUP1 = new ArrayList<String>();
	private List<String> LIST_TOTAL_QNTT = new ArrayList<String>();
	private List<String> LIST_TOTAL_PRICE = new ArrayList<String>();
	
	//
	private String HERBS_CD;
	private String ABSORPTION_RATE;
	private String STOCK_QNTT;
	
	public String getRECIPE_SEQ() {
		return RECIPE_SEQ;
	}
	public void setRECIPE_SEQ(String rECIPE_SEQ) {
		RECIPE_SEQ = rECIPE_SEQ;
	}
	public String getDRUG_CD() {
		return DRUG_CD;
	}
	public void setDRUG_CD(String dRUG_CD) {
		DRUG_CD = dRUG_CD;
	}
	public String getDRUG_NM() {
		return DRUG_NM;
	}
	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}
	public String getTURN() {
		return TURN;
	}
	public void setTURN(String tURN) {
		TURN = tURN;
	}
	public String getORIGIN() {
		return ORIGIN;
	}
	public void setORIGIN(String oRIGIN) {
		ORIGIN = oRIGIN;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getCHUP1() {
		return CHUP1;
	}
	public void setCHUP1(String cHUP1) {
		CHUP1 = cHUP1;
	}
	public String getTOTAL_QNTT() {
		return TOTAL_QNTT;
	}
	public void setTOTAL_QNTT(String tOTAL_QNTT) {
		TOTAL_QNTT = tOTAL_QNTT;
	}
	public String getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}
	public void setTOTAL_PRICE(String tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public List<String> getLIST_DRUG_CD() {
		return LIST_DRUG_CD;
	}
	public void setLIST_DRUG_CD(List<String> lIST_DRUG_CD) {
		LIST_DRUG_CD = lIST_DRUG_CD;
	}
	public List<String> getLIST_DRUG_NM() {
		return LIST_DRUG_NM;
	}
	public void setLIST_DRUG_NM(List<String> lIST_DRUG_NM) {
		LIST_DRUG_NM = lIST_DRUG_NM;
	}
	public List<String> getLIST_TURN() {
		return LIST_TURN;
	}
	public void setLIST_TURN(List<String> lIST_TURN) {
		LIST_TURN = lIST_TURN;
	}
	public List<String> getLIST_ORIGIN() {
		return LIST_ORIGIN;
	}
	public void setLIST_ORIGIN(List<String> lIST_ORIGIN) {
		LIST_ORIGIN = lIST_ORIGIN;
	}
	public List<String> getLIST_PRICE() {
		return LIST_PRICE;
	}
	public void setLIST_PRICE(List<String> lIST_PRICE) {
		LIST_PRICE = lIST_PRICE;
	}
	public List<String> getLIST_CHUP1() {
		return LIST_CHUP1;
	}
	public void setLIST_CHUP1(List<String> lIST_CHUP1) {
		LIST_CHUP1 = lIST_CHUP1;
	}
	public List<String> getLIST_TOTAL_QNTT() {
		return LIST_TOTAL_QNTT;
	}
	public void setLIST_TOTAL_QNTT(List<String> lIST_TOTAL_QNTT) {
		LIST_TOTAL_QNTT = lIST_TOTAL_QNTT;
	}
	public List<String> getLIST_TOTAL_PRICE() {
		return LIST_TOTAL_PRICE;
	}
	public void setLIST_TOTAL_PRICE(List<String> lIST_TOTAL_PRICE) {
		LIST_TOTAL_PRICE = lIST_TOTAL_PRICE;
	}
	public String getHERBS_CD() {
		return HERBS_CD;
	}
	public void setHERBS_CD(String hERBS_CD) {
		HERBS_CD = hERBS_CD;
	}
	public String getABSORPTION_RATE() {
		return ABSORPTION_RATE;
	}
	public void setABSORPTION_RATE(String aBSORPTION_RATE) {
		ABSORPTION_RATE = aBSORPTION_RATE;
	}
	public String getSTOCK_QNTT() {
		return STOCK_QNTT;
	}
	public void setSTOCK_QNTT(String sTOCK_QNTT) {
		STOCK_QNTT = sTOCK_QNTT;
	}	
}
