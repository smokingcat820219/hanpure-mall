package kr.co.hanpure.database.report;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ReportMapper {	
	List<ReportVO> selectDatasByPatient(ReportVO vo);
	List<ReportVO> selectDatasByRecipe1(ReportVO vo);
	List<ReportVO> selectDatasByRecipe2(ReportVO vo);	
	List<ReportVO> selectDatasByDrug(ReportVO vo);
	List<ReportVO> selectDatasByPayment(ReportVO vo);
}
