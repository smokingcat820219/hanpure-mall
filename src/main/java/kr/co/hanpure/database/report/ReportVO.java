package kr.co.hanpure.database.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ReportVO")
public class ReportVO extends Pagenation {
	private String ORDER_PATH_TOTAL;
	private String ORDER_PATH_001;
	private String ORDER_PATH_002;
	private String ORDER_PATH_003;
	private String ORDER_STATUS_CANCEL;
	
	private String ORDER_DT;
	private String DAY_OF_WEEK;
	private String DRUG_CD;
	private String DRUG_NM;
	private String TOTAL_QNTT;
	private String TOTAL_PRICE;
	private String RANK_TOTAL_QNTT;
	private String RANK_TOTAL_PRICE;
	
	private String MAKE_QNTT;
	
	private String SUM_TOTAL_QNTT;
	private String SUM_MAKE_QNTT;
	
	private String DRUG_CNT;
	private String MAKE_CNT;
	
	private String DIFF;
	private String ORDER_CNT;
	
	private String HOSPITAL_ID;
	private String PAYMENT_STATUS_Y_CNT;
	private String PAYMENT_STATUS_Y_PRICE;
	private String PAYMENT_STATUS_N_CNT;
	private String PAYMENT_STATUS_N_PRICE;
	
	private String SORT;
	
	private String ORDER_TP_001;
	private String ORDER_TP_002;
	private String ORDER_TP_003;
	private String ORDER_TP_004;
	private String ORDER_TP_005;
	private String ORDER_TP_006;	
	private String ORDER_TP_007;
	private String ORDER_TP_008;
	
	private String INFLOW_001;
	private String INFLOW_002;
	private String INFLOW_003;
	private String INFLOW_004;
	private String INFLOW_005;
	private String INFLOW_006;
	
	private List<String> LIST_SEX = new ArrayList<String>();
	private List<String> LIST_PAYMENT_STATUS = new ArrayList<String>();
	private List<String> LIST_ORDER_TP = new ArrayList<String>();
	private List<String> LIST_INFLOW = new ArrayList<String>();	
		
	private String searchField;
	private String sdate1;
	private String edate1;
	private String year1;
	private String month1;
	private String year2;
	private String month2;	
	private String sex;
	private String AGE;
	private String LOCAL;
	
	public String getORDER_PATH_001() {
		return ORDER_PATH_001;
	}
	public void setORDER_PATH_001(String oRDER_PATH_001) {
		ORDER_PATH_001 = oRDER_PATH_001;
	}
	public String getORDER_PATH_002() {
		return ORDER_PATH_002;
	}
	public void setORDER_PATH_002(String oRDER_PATH_002) {
		ORDER_PATH_002 = oRDER_PATH_002;
	}
	public String getORDER_PATH_003() {
		return ORDER_PATH_003;
	}
	public void setORDER_PATH_003(String oRDER_PATH_003) {
		ORDER_PATH_003 = oRDER_PATH_003;
	}
	public String getORDER_DT() {
		return ORDER_DT;
	}
	public void setORDER_DT(String oRDER_DT) {
		ORDER_DT = oRDER_DT;
	}
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getSdate1() {
		return sdate1;
	}
	public void setSdate1(String sdate1) {
		this.sdate1 = sdate1;
	}
	public String getEdate1() {
		return edate1;
	}
	public void setEdate1(String edate1) {
		this.edate1 = edate1;
	}
	public String getYear1() {
		return year1;
	}
	public void setYear1(String year1) {
		this.year1 = year1;
	}
	public String getMonth1() {
		return month1;
	}
	public void setMonth1(String month1) {
		this.month1 = month1;
	}
	public String getYear2() {
		return year2;
	}
	public void setYear2(String year2) {
		this.year2 = year2;
	}
	public String getMonth2() {
		return month2;
	}
	public void setMonth2(String month2) {
		this.month2 = month2;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}	
	public String getAGE() {
		return AGE;
	}
	public void setAGE(String aGE) {
		AGE = aGE;
	}
	public String getLOCAL() {
		return LOCAL;
	}
	public void setLOCAL(String lOCAL) {
		LOCAL = lOCAL;
	}
	public String getORDER_PATH_TOTAL() {
		return ORDER_PATH_TOTAL;
	}
	public void setORDER_PATH_TOTAL(String oRDER_PATH_TOTAL) {
		ORDER_PATH_TOTAL = oRDER_PATH_TOTAL;
	}
	public String getORDER_STATUS_CANCEL() {
		return ORDER_STATUS_CANCEL;
	}
	public void setORDER_STATUS_CANCEL(String oRDER_STATUS_CANCEL) {
		ORDER_STATUS_CANCEL = oRDER_STATUS_CANCEL;
	}
	public String getDAY_OF_WEEK() {
		return DAY_OF_WEEK;
	}
	public void setDAY_OF_WEEK(String dAY_OF_WEEK) {
		DAY_OF_WEEK = dAY_OF_WEEK;
	}
	public List<String> getLIST_SEX() {
		return LIST_SEX;
	}
	public void setLIST_SEX(List<String> lIST_SEX) {
		LIST_SEX = lIST_SEX;
	}
	public String getDRUG_CD() {
		return DRUG_CD;
	}
	public void setDRUG_CD(String dRUG_CD) {
		DRUG_CD = dRUG_CD;
	}
	public String getDRUG_NM() {
		return DRUG_NM;
	}
	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}
	public String getTOTAL_QNTT() {
		return TOTAL_QNTT;
	}
	public void setTOTAL_QNTT(String tOTAL_QNTT) {
		TOTAL_QNTT = tOTAL_QNTT;
	}
	public String getMAKE_QNTT() {
		return MAKE_QNTT;
	}
	public void setMAKE_QNTT(String mAKE_QNTT) {
		MAKE_QNTT = mAKE_QNTT;
	}
	public String getSUM_TOTAL_QNTT() {
		return SUM_TOTAL_QNTT;
	}
	public void setSUM_TOTAL_QNTT(String sUM_TOTAL_QNTT) {
		SUM_TOTAL_QNTT = sUM_TOTAL_QNTT;
	}
	public String getSUM_MAKE_QNTT() {
		return SUM_MAKE_QNTT;
	}
	public void setSUM_MAKE_QNTT(String sUM_MAKE_QNTT) {
		SUM_MAKE_QNTT = sUM_MAKE_QNTT;
	}
	public String getDRUG_CNT() {
		return DRUG_CNT;
	}
	public void setDRUG_CNT(String dRUG_CNT) {
		DRUG_CNT = dRUG_CNT;
	}
	public String getMAKE_CNT() {
		return MAKE_CNT;
	}
	public void setMAKE_CNT(String mAKE_CNT) {
		MAKE_CNT = mAKE_CNT;
	}
	public String getDIFF() {
		return DIFF;
	}
	public void setDIFF(String dIFF) {
		DIFF = dIFF;
	}
	public String getORDER_CNT() {
		return ORDER_CNT;
	}
	public void setORDER_CNT(String oRDER_CNT) {
		ORDER_CNT = oRDER_CNT;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}	
	public String getPAYMENT_STATUS_Y_CNT() {
		return PAYMENT_STATUS_Y_CNT;
	}
	public void setPAYMENT_STATUS_Y_CNT(String pAYMENT_STATUS_Y_CNT) {
		PAYMENT_STATUS_Y_CNT = pAYMENT_STATUS_Y_CNT;
	}
	public String getPAYMENT_STATUS_Y_PRICE() {
		return PAYMENT_STATUS_Y_PRICE;
	}
	public void setPAYMENT_STATUS_Y_PRICE(String pAYMENT_STATUS_Y_PRICE) {
		PAYMENT_STATUS_Y_PRICE = pAYMENT_STATUS_Y_PRICE;
	}
	public String getPAYMENT_STATUS_N_CNT() {
		return PAYMENT_STATUS_N_CNT;
	}
	public void setPAYMENT_STATUS_N_CNT(String pAYMENT_STATUS_N_CNT) {
		PAYMENT_STATUS_N_CNT = pAYMENT_STATUS_N_CNT;
	}
	public String getPAYMENT_STATUS_N_PRICE() {
		return PAYMENT_STATUS_N_PRICE;
	}
	public void setPAYMENT_STATUS_N_PRICE(String pAYMENT_STATUS_N_PRICE) {
		PAYMENT_STATUS_N_PRICE = pAYMENT_STATUS_N_PRICE;
	}
	public List<String> getLIST_PAYMENT_STATUS() {
		return LIST_PAYMENT_STATUS;
	}
	public void setLIST_PAYMENT_STATUS(List<String> lIST_PAYMENT_STATUS) {
		LIST_PAYMENT_STATUS = lIST_PAYMENT_STATUS;
	}
	public String getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}
	public void setTOTAL_PRICE(String tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}
	public String getRANK_TOTAL_QNTT() {
		return RANK_TOTAL_QNTT;
	}
	public void setRANK_TOTAL_QNTT(String rANK_TOTAL_QNTT) {
		RANK_TOTAL_QNTT = rANK_TOTAL_QNTT;
	}
	public String getRANK_TOTAL_PRICE() {
		return RANK_TOTAL_PRICE;
	}
	public void setRANK_TOTAL_PRICE(String rANK_TOTAL_PRICE) {
		RANK_TOTAL_PRICE = rANK_TOTAL_PRICE;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public List<String> getLIST_ORDER_TP() {
		return LIST_ORDER_TP;
	}
	public void setLIST_ORDER_TP(List<String> lIST_ORDER_TP) {
		LIST_ORDER_TP = lIST_ORDER_TP;
	}
	public String getORDER_TP_001() {
		return ORDER_TP_001;
	}
	public void setORDER_TP_001(String oRDER_TP_001) {
		ORDER_TP_001 = oRDER_TP_001;
	}
	public String getORDER_TP_002() {
		return ORDER_TP_002;
	}
	public void setORDER_TP_002(String oRDER_TP_002) {
		ORDER_TP_002 = oRDER_TP_002;
	}
	public String getORDER_TP_003() {
		return ORDER_TP_003;
	}
	public void setORDER_TP_003(String oRDER_TP_003) {
		ORDER_TP_003 = oRDER_TP_003;
	}
	public String getORDER_TP_004() {
		return ORDER_TP_004;
	}
	public void setORDER_TP_004(String oRDER_TP_004) {
		ORDER_TP_004 = oRDER_TP_004;
	}
	public String getORDER_TP_005() {
		return ORDER_TP_005;
	}
	public void setORDER_TP_005(String oRDER_TP_005) {
		ORDER_TP_005 = oRDER_TP_005;
	}
	public String getORDER_TP_007() {
		return ORDER_TP_007;
	}
	public void setORDER_TP_007(String oRDER_TP_007) {
		ORDER_TP_007 = oRDER_TP_007;
	}
	public String getINFLOW_001() {
		return INFLOW_001;
	}
	public void setINFLOW_001(String iNFLOW_001) {
		INFLOW_001 = iNFLOW_001;
	}
	public String getINFLOW_002() {
		return INFLOW_002;
	}
	public void setINFLOW_002(String iNFLOW_002) {
		INFLOW_002 = iNFLOW_002;
	}
	public String getINFLOW_003() {
		return INFLOW_003;
	}
	public void setINFLOW_003(String iNFLOW_003) {
		INFLOW_003 = iNFLOW_003;
	}
	public String getINFLOW_004() {
		return INFLOW_004;
	}
	public void setINFLOW_004(String iNFLOW_004) {
		INFLOW_004 = iNFLOW_004;
	}
	public String getINFLOW_005() {
		return INFLOW_005;
	}
	public void setINFLOW_005(String iNFLOW_005) {
		INFLOW_005 = iNFLOW_005;
	}
	public String getINFLOW_006() {
		return INFLOW_006;
	}
	public void setINFLOW_006(String iNFLOW_006) {
		INFLOW_006 = iNFLOW_006;
	}
	public List<String> getLIST_INFLOW() {
		return LIST_INFLOW;
	}
	public void setLIST_INFLOW(List<String> lIST_INFLOW) {
		LIST_INFLOW = lIST_INFLOW;
	}
	public String getORDER_TP_006() {
		return ORDER_TP_006;
	}
	public void setORDER_TP_006(String oRDER_TP_006) {
		ORDER_TP_006 = oRDER_TP_006;
	}
	public String getORDER_TP_008() {
		return ORDER_TP_008;
	}
	public void setORDER_TP_008(String oRDER_TP_008) {
		ORDER_TP_008 = oRDER_TP_008;
	}		
}
