package kr.co.hanpure.database.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ReportService {
	@Autowired
	private ReportMapper mapper;
	
	public List<ReportVO> selectDatasByPatient(ReportVO vo) {
		Logger.info();
		return this.mapper.selectDatasByPatient(vo);
	}
	
	public List<ReportVO> selectDatasByRecipe1(ReportVO vo) {
		Logger.info();
		return this.mapper.selectDatasByRecipe1(vo);
	}
	
	public List<ReportVO> selectDatasByRecipe2(ReportVO vo) {
		Logger.info();
		return this.mapper.selectDatasByRecipe2(vo);
	}
	
	public List<ReportVO> selectDatasByDrug(ReportVO vo) {
		Logger.info();
		return this.mapper.selectDatasByDrug(vo);
	}
	
	public List<ReportVO> selectDatasByPayment(ReportVO vo) {
		Logger.info();
		return this.mapper.selectDatasByPayment(vo);
	}
}


