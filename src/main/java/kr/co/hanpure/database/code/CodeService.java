package kr.co.hanpure.database.code;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class CodeService {
	@Autowired
	private CodeMapper mapper;
	
	public void truncate() {
		Logger.info();
		this.mapper.truncate();
	}
	
	public void insertData(CodeVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(CodeVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(CodeVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public CodeVO selectData(CodeVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public Integer selectDataCount(CodeVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<CodeVO> selectDataList(CodeVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
}


