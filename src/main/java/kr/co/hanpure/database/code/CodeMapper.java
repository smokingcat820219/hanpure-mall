package kr.co.hanpure.database.code;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CodeMapper {	
	void truncate();
	
	void insertData(CodeVO vo);
	void updateData(CodeVO vo);
	void deleteData(CodeVO vo);
	CodeVO selectData(CodeVO vo);
	Integer selectDataCount(CodeVO vo);
	List<CodeVO> selectDataList(CodeVO vo);
}
