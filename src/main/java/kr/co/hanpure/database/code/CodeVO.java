package kr.co.hanpure.database.code;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("CodeVO")
public class CodeVO extends Pagenation {
	private String GROUP_CD;
	private String GROUP_NM_KR;
	private String GROUP_NM_CN;
	private String REMARK_KR;
	private String REMARK_CN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	private String SUB_QNTT;	
	
	private List<String> LIST_SUB_CD = new ArrayList<String>();
	private List<String> LIST_SUB_NM_KR = new ArrayList<String>();
	private List<String> LIST_SUB_NM_CN = new ArrayList<String>();
	private List<String> LIST_SUB_VALUE_KR = new ArrayList<String>();
	private List<String> LIST_SUB_VALUE_CN = new ArrayList<String>();
	
	public String getGROUP_CD() {
		return GROUP_CD;
	}
	public void setGROUP_CD(String gROUP_CD) {
		GROUP_CD = gROUP_CD;
	}
	public String getGROUP_NM_KR() {
		return GROUP_NM_KR;
	}
	public void setGROUP_NM_KR(String gROUP_NM_KR) {
		GROUP_NM_KR = gROUP_NM_KR;
	}
	public String getGROUP_NM_CN() {
		return GROUP_NM_CN;
	}
	public void setGROUP_NM_CN(String gROUP_NM_CN) {
		GROUP_NM_CN = gROUP_NM_CN;
	}
	public String getREMARK_KR() {
		return REMARK_KR;
	}
	public void setREMARK_KR(String rEMARK_KR) {
		REMARK_KR = rEMARK_KR;
	}
	public String getREMARK_CN() {
		return REMARK_CN;
	}
	public void setREMARK_CN(String rEMARK_CN) {
		REMARK_CN = rEMARK_CN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getSUB_QNTT() {
		return SUB_QNTT;
	}
	public void setSUB_QNTT(String sUB_QNTT) {
		SUB_QNTT = sUB_QNTT;
	}
	public List<String> getLIST_SUB_CD() {
		return LIST_SUB_CD;
	}
	public void setLIST_SUB_CD(List<String> lIST_SUB_CD) {
		LIST_SUB_CD = lIST_SUB_CD;
	}
	public List<String> getLIST_SUB_NM_KR() {
		return LIST_SUB_NM_KR;
	}
	public void setLIST_SUB_NM_KR(List<String> lIST_SUB_NM_KR) {
		LIST_SUB_NM_KR = lIST_SUB_NM_KR;
	}
	public List<String> getLIST_SUB_NM_CN() {
		return LIST_SUB_NM_CN;
	}
	public void setLIST_SUB_NM_CN(List<String> lIST_SUB_NM_CN) {
		LIST_SUB_NM_CN = lIST_SUB_NM_CN;
	}
	public List<String> getLIST_SUB_VALUE_KR() {
		return LIST_SUB_VALUE_KR;
	}
	public void setLIST_SUB_VALUE_KR(List<String> lIST_SUB_VALUE_KR) {
		LIST_SUB_VALUE_KR = lIST_SUB_VALUE_KR;
	}
	public List<String> getLIST_SUB_VALUE_CN() {
		return LIST_SUB_VALUE_CN;
	}
	public void setLIST_SUB_VALUE_CN(List<String> lIST_SUB_VALUE_CN) {
		LIST_SUB_VALUE_CN = lIST_SUB_VALUE_CN;
	}	
}

