package kr.co.hanpure.database.drug;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class DrugService {
	@Autowired
	private DrugMapper mapper;
	
	public void insertData(DrugVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(DrugVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public DrugVO selectData(DrugVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<DrugVO> selectDatas(DrugVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(DrugVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<DrugVO> selectDataList(DrugVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public Integer selectInsuranceCount(DrugVO vo) {
		Logger.info();
		return this.mapper.selectInsuranceCount(vo);
	}
	
	public List<DrugVO> selectInsuranceList(DrugVO vo) {
		Logger.info();
		return this.mapper.selectInsuranceList(vo);
	}
}


