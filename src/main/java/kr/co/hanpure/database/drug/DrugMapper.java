package kr.co.hanpure.database.drug;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface DrugMapper {	
	void insertData(DrugVO vo);
	void updateData(DrugVO vo);
	DrugVO selectData(DrugVO vo);
	List<DrugVO> selectDatas(DrugVO vo);
	Integer selectDataCount(DrugVO vo);
	List<DrugVO> selectDataList(DrugVO vo);
	
	Integer selectInsuranceCount(DrugVO vo);
	List<DrugVO> selectInsuranceList(DrugVO vo);
	
}
