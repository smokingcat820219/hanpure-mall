package kr.co.hanpure.database.drug;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("DrugVO")
public class DrugVO extends Pagenation {
	private String DRUG_CD;
	private String DRUG_NM;
	private String DRUG_NM2;
	private String HERBS_CD;
	private String INSURANCE_YN;
	private String INSURANCE_CODE1;
	private String INSURANCE_CODE2;
	private String INSURANCE_CODE3;
	private String HOSPITAL_ID;
	private String DRUG_TP;
	private String ABSORPTION_CD;
	private String ABSORPTION_EX;
	private String ABSORPTION_RATE;
	private String ORIGIN;
	private String MAKER;
	private String RELATION_WORD;
	private String PROCESS;
	private String NICK_NM;
	private String CONTENT;
	private String REMARK1;
	private String REMARK2;
	private String REMARK3;
	private String SPEC;
	private String KOREA_YN;
	private String ROAST_YN;
	private String PRICE_A;
	private String PRICE_B;
	private String PRICE_C;
	private String PRICE_D;
	private String PRICE_E;
	private String ECOUNT;	
	private String USE_YN;
	private String DEL_YN;
	
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	
	/////////////
	private String WH_CD;
	private String WH_NM;
	private String STOCK_QNTT;
	private String WORKPLACE_TP;
	private String RAW_MIN_QNTT;
	private String RAW_DRUG_STATUS;
	private String MIN_QNTT;
	private String DRUG_STATUS;
	private String KEYWORD;
	private String DRUG_STATUS_NM;
	private String RAW_DRUG_STATUS_NM;
	
	private String DIV_CD;
	private String HERBS_NM;
	private String DRUG_TP_NM;
	private String HOSPITAL_NM;
	
	
	private String HABITUDE;
	private String TEMPER;
	private String ORGAN;
	private String POISON;   
	private String PRICE;
	private String QNTT;
	
	private String searchField;
	private String searchText;
	
	private String searchField2;
	private String searchText2;
	
	private String sdate1;
	private String edate1;
	
	private String year;
	private String month;
	
	private List<String> LIST_DRUG_CD = new ArrayList<String>();
	private List<String> LIST_INSURANCE_YN = new ArrayList<String>();
	
	public String getDRUG_CD() {
		return DRUG_CD;
	}
	public void setDRUG_CD(String dRUG_CD) {
		DRUG_CD = dRUG_CD;
	}
	public String getDRUG_NM() {
		return DRUG_NM;
	}
	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}
	public String getHERBS_CD() {
		return HERBS_CD;
	}
	public void setHERBS_CD(String hERBS_CD) {
		HERBS_CD = hERBS_CD;
	}
	public String getINSURANCE_YN() {
		return INSURANCE_YN;
	}
	public void setINSURANCE_YN(String iNSURANCE_YN) {
		INSURANCE_YN = iNSURANCE_YN;
	}
	public String getINSURANCE_CODE1() {
		return INSURANCE_CODE1;
	}
	public void setINSURANCE_CODE1(String iNSURANCE_CODE1) {
		INSURANCE_CODE1 = iNSURANCE_CODE1;
	}
	public String getINSURANCE_CODE2() {
		return INSURANCE_CODE2;
	}
	public void setINSURANCE_CODE2(String iNSURANCE_CODE2) {
		INSURANCE_CODE2 = iNSURANCE_CODE2;
	}
	public String getINSURANCE_CODE3() {
		return INSURANCE_CODE3;
	}
	public void setINSURANCE_CODE3(String iNSURANCE_CODE3) {
		INSURANCE_CODE3 = iNSURANCE_CODE3;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getDRUG_TP() {
		return DRUG_TP;
	}
	public void setDRUG_TP(String dRUG_TP) {
		DRUG_TP = dRUG_TP;
	}
	public String getABSORPTION_CD() {
		return ABSORPTION_CD;
	}
	public void setABSORPTION_CD(String aBSORPTION_CD) {
		ABSORPTION_CD = aBSORPTION_CD;
	}
	public String getABSORPTION_EX() {
		return ABSORPTION_EX;
	}
	public void setABSORPTION_EX(String aBSORPTION_EX) {
		ABSORPTION_EX = aBSORPTION_EX;
	}
	public String getABSORPTION_RATE() {
		return ABSORPTION_RATE;
	}
	public void setABSORPTION_RATE(String aBSORPTION_RATE) {
		ABSORPTION_RATE = aBSORPTION_RATE;
	}
	public String getORIGIN() {
		return ORIGIN;
	}
	public void setORIGIN(String oRIGIN) {
		ORIGIN = oRIGIN;
	}
	public String getMAKER() {
		return MAKER;
	}
	public void setMAKER(String mAKER) {
		MAKER = mAKER;
	}
	public String getRELATION_WORD() {
		return RELATION_WORD;
	}
	public void setRELATION_WORD(String rELATION_WORD) {
		RELATION_WORD = rELATION_WORD;
	}
	public String getPROCESS() {
		return PROCESS;
	}
	public void setPROCESS(String pROCESS) {
		PROCESS = pROCESS;
	}
	public String getNICK_NM() {
		return NICK_NM;
	}
	public void setNICK_NM(String nICK_NM) {
		NICK_NM = nICK_NM;
	}
	public String getCONTENT() {
		return CONTENT;
	}
	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}
	public String getREMARK1() {
		return REMARK1;
	}
	public void setREMARK1(String rEMARK1) {
		REMARK1 = rEMARK1;
	}
	public String getREMARK2() {
		return REMARK2;
	}
	public void setREMARK2(String rEMARK2) {
		REMARK2 = rEMARK2;
	}
	public String getREMARK3() {
		return REMARK3;
	}
	public void setREMARK3(String rEMARK3) {
		REMARK3 = rEMARK3;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getSearchField2() {
		return searchField2;
	}
	public void setSearchField2(String searchField2) {
		this.searchField2 = searchField2;
	}
	public String getSearchText2() {
		return searchText2;
	}
	public void setSearchText2(String searchText2) {
		this.searchText2 = searchText2;
	}
	public String getDIV_CD() {
		return DIV_CD;
	}
	public void setDIV_CD(String dIV_CD) {
		DIV_CD = dIV_CD;
	}
	public String getHERBS_NM() {
		return HERBS_NM;
	}
	public void setHERBS_NM(String hERBS_NM) {
		HERBS_NM = hERBS_NM;
	}
	public String getDRUG_TP_NM() {
		return DRUG_TP_NM;
	}
	public void setDRUG_TP_NM(String dRUG_TP_NM) {
		DRUG_TP_NM = dRUG_TP_NM;
	}
	public String getHOSPITAL_NM() {
		return HOSPITAL_NM;
	}
	public void setHOSPITAL_NM(String hOSPITAL_NM) {
		HOSPITAL_NM = hOSPITAL_NM;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getPRICE_A() {
		return PRICE_A;
	}
	public void setPRICE_A(String pRICE_A) {
		PRICE_A = pRICE_A;
	}
	public String getPRICE_B() {
		return PRICE_B;
	}
	public void setPRICE_B(String pRICE_B) {
		PRICE_B = pRICE_B;
	}
	public String getPRICE_C() {
		return PRICE_C;
	}
	public void setPRICE_C(String pRICE_C) {
		PRICE_C = pRICE_C;
	}
	public String getPRICE_D() {
		return PRICE_D;
	}
	public void setPRICE_D(String pRICE_D) {
		PRICE_D = pRICE_D;
	}
	public String getPRICE_E() {
		return PRICE_E;
	}
	public void setPRICE_E(String pRICE_E) {
		PRICE_E = pRICE_E;
	}
	public String getRAW_MIN_QNTT() {
		return RAW_MIN_QNTT;
	}
	public void setRAW_MIN_QNTT(String rAW_MIN_QNTT) {
		RAW_MIN_QNTT = rAW_MIN_QNTT;
	}
	public String getMIN_QNTT() {
		return MIN_QNTT;
	}
	public void setMIN_QNTT(String mIN_QNTT) {
		MIN_QNTT = mIN_QNTT;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getECOUNT() {
		return ECOUNT;
	}
	public void setECOUNT(String eCOUNT) {
		ECOUNT = eCOUNT;
	}
	public String getDRUG_STATUS() {
		return DRUG_STATUS;
	}
	public void setDRUG_STATUS(String dRUG_STATUS) {
		DRUG_STATUS = dRUG_STATUS;
	}
	public String getWORKPLACE_TP() {
		return WORKPLACE_TP;
	}
	public void setWORKPLACE_TP(String wORKPLACE_TP) {
		WORKPLACE_TP = wORKPLACE_TP;
	}
	public String getSTOCK_QNTT() {
		return STOCK_QNTT;
	}
	public void setSTOCK_QNTT(String sTOCK_QNTT) {
		STOCK_QNTT = sTOCK_QNTT;
	}
	public String getWH_CD() {
		return WH_CD;
	}
	public void setWH_CD(String wH_CD) {
		WH_CD = wH_CD;
	}
	public String getWH_NM() {
		return WH_NM;
	}
	public void setWH_NM(String wH_NM) {
		WH_NM = wH_NM;
	}
	public String getDRUG_STATUS_NM() {
		return DRUG_STATUS_NM;
	}
	public void setDRUG_STATUS_NM(String dRUG_STATUS_NM) {
		DRUG_STATUS_NM = dRUG_STATUS_NM;
	}
	public List<String> getLIST_DRUG_CD() {
		return LIST_DRUG_CD;
	}
	public void setLIST_DRUG_CD(List<String> lIST_DRUG_CD) {
		LIST_DRUG_CD = lIST_DRUG_CD;
	}
	public String getKOREA_YN() {
		return KOREA_YN;
	}
	public void setKOREA_YN(String kOREA_YN) {
		KOREA_YN = kOREA_YN;
	}
	public String getROAST_YN() {
		return ROAST_YN;
	}
	public void setROAST_YN(String rOAST_YN) {
		ROAST_YN = rOAST_YN;
	}
	public String getRAW_DRUG_STATUS() {
		return RAW_DRUG_STATUS;
	}
	public void setRAW_DRUG_STATUS(String rAW_DRUG_STATUS) {
		RAW_DRUG_STATUS = rAW_DRUG_STATUS;
	}
	public String getKEYWORD() {
		return KEYWORD;
	}
	public void setKEYWORD(String kEYWORD) {
		KEYWORD = kEYWORD;
	}
	public String getRAW_DRUG_STATUS_NM() {
		return RAW_DRUG_STATUS_NM;
	}
	public void setRAW_DRUG_STATUS_NM(String rAW_DRUG_STATUS_NM) {
		RAW_DRUG_STATUS_NM = rAW_DRUG_STATUS_NM;
	}
	public String getDRUG_NM2() {
		return DRUG_NM2;
	}
	public void setDRUG_NM2(String dRUG_NM2) {
		DRUG_NM2 = dRUG_NM2;
	}
	public List<String> getLIST_INSURANCE_YN() {
		return LIST_INSURANCE_YN;
	}
	public void setLIST_INSURANCE_YN(List<String> lIST_INSURANCE_YN) {
		LIST_INSURANCE_YN = lIST_INSURANCE_YN;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getHABITUDE() {
		return HABITUDE;
	}
	public void setHABITUDE(String hABITUDE) {
		HABITUDE = hABITUDE;
	}
	public String getTEMPER() {
		return TEMPER;
	}
	public void setTEMPER(String tEMPER) {
		TEMPER = tEMPER;
	}
	public String getORGAN() {
		return ORGAN;
	}
	public void setORGAN(String oRGAN) {
		ORGAN = oRGAN;
	}
	public String getPOISON() {
		return POISON;
	}
	public void setPOISON(String pOISON) {
		POISON = pOISON;
	}
	public String getSdate1() {
		return sdate1;
	}
	public void setSdate1(String sdate1) {
		this.sdate1 = sdate1;
	}
	public String getEdate1() {
		return edate1;
	}
	public void setEdate1(String edate1) {
		this.edate1 = edate1;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getQNTT() {
		return QNTT;
	}
	public void setQNTT(String qNTT) {
		QNTT = qNTT;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
}
