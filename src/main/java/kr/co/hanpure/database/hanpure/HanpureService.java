package kr.co.hanpure.database.hanpure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class HanpureService {
	@Autowired
	private HanpureMapper mapper;
	
	public void insertData(HanpureVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(HanpureVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public HanpureVO selectData(HanpureVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
}


