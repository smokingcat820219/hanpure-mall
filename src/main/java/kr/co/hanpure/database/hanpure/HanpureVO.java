package kr.co.hanpure.database.hanpure;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("HanpureVO")
public class HanpureVO extends Pagenation {
	private String PRICE_HOTPOT_A;
	private String PRICE_HOTPOT_B;
	private String PRICE_HOTPOT_C;
	private String PRICE_HOTPOT_D;
	private String PRICE_HOTPOT_E;
	
	private String PRICE_AMPLE;
	private String PRICE_DEER;
	private String PRICE_MAKE_A;
	private String PRICE_MAKE_B;
	private String PRICE_MAKE_C;
	private String PRICE_MAKE_D;
	private String PRICE_MAKE_E;
	
	private String PRICE_HOTPOT_MAKE;
	private String PRICE_PACKAGE;
	private String PRICE_DELIVERY_SEOUL;
	private String PRICE_DELIVERY_ETC;
	private String PRICE_DELIVERY_PO;	
	private String TEL;
	private String MOBILE;
	private String ZIPCODE;
	private String ADDRESS;
	private String ADDRESS2;
	private String DELIVERY_MSG;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	public String getPRICE_HOTPOT_A() {
		return PRICE_HOTPOT_A;
	}
	public void setPRICE_HOTPOT_A(String pRICE_HOTPOT_A) {
		PRICE_HOTPOT_A = pRICE_HOTPOT_A;
	}
	public String getPRICE_HOTPOT_B() {
		return PRICE_HOTPOT_B;
	}
	public void setPRICE_HOTPOT_B(String pRICE_HOTPOT_B) {
		PRICE_HOTPOT_B = pRICE_HOTPOT_B;
	}
	public String getPRICE_HOTPOT_C() {
		return PRICE_HOTPOT_C;
	}
	public void setPRICE_HOTPOT_C(String pRICE_HOTPOT_C) {
		PRICE_HOTPOT_C = pRICE_HOTPOT_C;
	}
	public String getPRICE_HOTPOT_D() {
		return PRICE_HOTPOT_D;
	}
	public void setPRICE_HOTPOT_D(String pRICE_HOTPOT_D) {
		PRICE_HOTPOT_D = pRICE_HOTPOT_D;
	}
	public String getPRICE_HOTPOT_E() {
		return PRICE_HOTPOT_E;
	}
	public void setPRICE_HOTPOT_E(String pRICE_HOTPOT_E) {
		PRICE_HOTPOT_E = pRICE_HOTPOT_E;
	}
	public String getPRICE_AMPLE() {
		return PRICE_AMPLE;
	}
	public void setPRICE_AMPLE(String pRICE_AMPLE) {
		PRICE_AMPLE = pRICE_AMPLE;
	}
	public String getPRICE_MAKE_A() {
		return PRICE_MAKE_A;
	}
	public void setPRICE_MAKE_A(String pRICE_MAKE_A) {
		PRICE_MAKE_A = pRICE_MAKE_A;
	}
	public String getPRICE_MAKE_B() {
		return PRICE_MAKE_B;
	}
	public void setPRICE_MAKE_B(String pRICE_MAKE_B) {
		PRICE_MAKE_B = pRICE_MAKE_B;
	}
	public String getPRICE_MAKE_C() {
		return PRICE_MAKE_C;
	}
	public void setPRICE_MAKE_C(String pRICE_MAKE_C) {
		PRICE_MAKE_C = pRICE_MAKE_C;
	}
	public String getPRICE_MAKE_D() {
		return PRICE_MAKE_D;
	}
	public void setPRICE_MAKE_D(String pRICE_MAKE_D) {
		PRICE_MAKE_D = pRICE_MAKE_D;
	}
	public String getPRICE_MAKE_E() {
		return PRICE_MAKE_E;
	}
	public void setPRICE_MAKE_E(String pRICE_MAKE_E) {
		PRICE_MAKE_E = pRICE_MAKE_E;
	}
	public String getPRICE_HOTPOT_MAKE() {
		return PRICE_HOTPOT_MAKE;
	}
	public void setPRICE_HOTPOT_MAKE(String pRICE_HOTPOT_MAKE) {
		PRICE_HOTPOT_MAKE = pRICE_HOTPOT_MAKE;
	}
	public String getPRICE_PACKAGE() {
		return PRICE_PACKAGE;
	}
	public void setPRICE_PACKAGE(String pRICE_PACKAGE) {
		PRICE_PACKAGE = pRICE_PACKAGE;
	}
	public String getPRICE_DELIVERY_SEOUL() {
		return PRICE_DELIVERY_SEOUL;
	}
	public void setPRICE_DELIVERY_SEOUL(String pRICE_DELIVERY_SEOUL) {
		PRICE_DELIVERY_SEOUL = pRICE_DELIVERY_SEOUL;
	}
	public String getPRICE_DELIVERY_ETC() {
		return PRICE_DELIVERY_ETC;
	}
	public void setPRICE_DELIVERY_ETC(String pRICE_DELIVERY_ETC) {
		PRICE_DELIVERY_ETC = pRICE_DELIVERY_ETC;
	}
	public String getPRICE_DELIVERY_PO() {
		return PRICE_DELIVERY_PO;
	}
	public void setPRICE_DELIVERY_PO(String pRICE_DELIVERY_PO) {
		PRICE_DELIVERY_PO = pRICE_DELIVERY_PO;
	}
	public String getTEL() {
		return TEL;
	}
	public void setTEL(String tEL) {
		TEL = tEL;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getZIPCODE() {
		return ZIPCODE;
	}
	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getADDRESS2() {
		return ADDRESS2;
	}
	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}
	public String getDELIVERY_MSG() {
		return DELIVERY_MSG;
	}
	public void setDELIVERY_MSG(String dELIVERY_MSG) {
		DELIVERY_MSG = dELIVERY_MSG;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getPRICE_DEER() {
		return PRICE_DEER;
	}
	public void setPRICE_DEER(String pRICE_DEER) {
		PRICE_DEER = pRICE_DEER;
	}		
}
