package kr.co.hanpure.database.hanpure;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface HanpureMapper {	
	void insertData(HanpureVO vo);
	void updateData(HanpureVO vo);
	HanpureVO selectData(HanpureVO vo);
}

