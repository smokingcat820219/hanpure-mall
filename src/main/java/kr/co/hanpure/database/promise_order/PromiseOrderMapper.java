package kr.co.hanpure.database.promise_order;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PromiseOrderMapper {		
	void insertData(PromiseOrderVO vo);
	List<PromiseOrderVO> selectDatas(PromiseOrderVO vo);
}
