package kr.co.hanpure.database.promise_order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class PromiseOrderService {
	@Autowired
	private PromiseOrderMapper mapper;
	
	public void insertData(PromiseOrderVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public List<PromiseOrderVO> selectDatas(PromiseOrderVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}	
}


