package kr.co.hanpure.database.promise_order;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("PromiseOrderVO")
public class PromiseOrderVO extends Pagenation {
	private String PROMISE_ORDER_SEQ;
	private String ORDER_SEQ;
	private String HOSPITAL_ID;
	private String DOCTOR;
	private String PRODUCT_NM;
	private String OPTION_NM;
	private String BOX_QNTT;
	private String PROMISE_SEQ;
	private String PRODUCT_SEQ;
	private String BOM_CD;
	private String PRICE;
	private String ORDER_QNTT;
	private String AMOUNT;	
	private String PRODUCT_OPTION;
	private String ECOUNT_WH_CD;
	
	private List<String> LIST_PROMISE_SEQ = new ArrayList<String>();
	private List<String> LIST_BOM_CD = new ArrayList<String>();
	private List<String> LIST_ORDER_QNTT = new ArrayList<String>();
	
	private String ITEM_NM1;
	private String ITEM_NM2;
	private String ITEM_NM3;
	private String ITEM_NM4;
	private String ITEM_NM5;
	private String ITEM_NM6;
	private String ITEM_NM7;
	private String ITEM_NM8;
	private String ITEM_NM9;
	private String ITEM_NM10;
	
	private String FILEPATH1;
	private String FILEPATH2;
	private String FILEPATH3;
	private String FILEPATH4;
	private String FILEPATH5;
	private String FILEPATH6;
	private String FILEPATH7;
	private String FILEPATH8;
	private String FILEPATH9;
	private String FILEPATH10;	
	
	public String getORDER_SEQ() {
		return ORDER_SEQ;
	}
	public void setORDER_SEQ(String oRDER_SEQ) {
		ORDER_SEQ = oRDER_SEQ;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getDOCTOR() {
		return DOCTOR;
	}
	public void setDOCTOR(String dOCTOR) {
		DOCTOR = dOCTOR;
	}
	public String getPRODUCT_NM() {
		return PRODUCT_NM;
	}
	public void setPRODUCT_NM(String pRODUCT_NM) {
		PRODUCT_NM = pRODUCT_NM;
	}
	public String getOPTION_NM() {
		return OPTION_NM;
	}
	public void setOPTION_NM(String oPTION_NM) {
		OPTION_NM = oPTION_NM;
	}
	public String getBOX_QNTT() {
		return BOX_QNTT;
	}
	public void setBOX_QNTT(String bOX_QNTT) {
		BOX_QNTT = bOX_QNTT;
	}
	public String getPROMISE_SEQ() {
		return PROMISE_SEQ;
	}
	public void setPROMISE_SEQ(String pROMISE_SEQ) {
		PROMISE_SEQ = pROMISE_SEQ;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getORDER_QNTT() {
		return ORDER_QNTT;
	}
	public void setORDER_QNTT(String oRDER_QNTT) {
		ORDER_QNTT = oRDER_QNTT;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public List<String> getLIST_PROMISE_SEQ() {
		return LIST_PROMISE_SEQ;
	}
	public void setLIST_PROMISE_SEQ(List<String> lIST_PROMISE_SEQ) {
		LIST_PROMISE_SEQ = lIST_PROMISE_SEQ;
	}
	public List<String> getLIST_ORDER_QNTT() {
		return LIST_ORDER_QNTT;
	}
	public void setLIST_ORDER_QNTT(List<String> lIST_ORDER_QNTT) {
		LIST_ORDER_QNTT = lIST_ORDER_QNTT;
	}
	public String getFILEPATH1() {
		return FILEPATH1;
	}
	public void setFILEPATH1(String fILEPATH1) {
		FILEPATH1 = fILEPATH1;
	}
	public String getBOM_CD() {
		return BOM_CD;
	}
	public void setBOM_CD(String bOM_CD) {
		BOM_CD = bOM_CD;
	}
	public List<String> getLIST_BOM_CD() {
		return LIST_BOM_CD;
	}
	public void setLIST_BOM_CD(List<String> lIST_BOM_CD) {
		LIST_BOM_CD = lIST_BOM_CD;
	}
	public String getPROMISE_ORDER_SEQ() {
		return PROMISE_ORDER_SEQ;
	}
	public void setPROMISE_ORDER_SEQ(String pROMISE_ORDER_SEQ) {
		PROMISE_ORDER_SEQ = pROMISE_ORDER_SEQ;
	}
	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}
	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}
	public String getPRODUCT_OPTION() {
		return PRODUCT_OPTION;
	}
	public void setPRODUCT_OPTION(String pRODUCT_OPTION) {
		PRODUCT_OPTION = pRODUCT_OPTION;
	}
	public String getFILEPATH2() {
		return FILEPATH2;
	}
	public void setFILEPATH2(String fILEPATH2) {
		FILEPATH2 = fILEPATH2;
	}
	public String getFILEPATH3() {
		return FILEPATH3;
	}
	public void setFILEPATH3(String fILEPATH3) {
		FILEPATH3 = fILEPATH3;
	}
	public String getFILEPATH4() {
		return FILEPATH4;
	}
	public void setFILEPATH4(String fILEPATH4) {
		FILEPATH4 = fILEPATH4;
	}
	public String getFILEPATH5() {
		return FILEPATH5;
	}
	public void setFILEPATH5(String fILEPATH5) {
		FILEPATH5 = fILEPATH5;
	}
	public String getFILEPATH6() {
		return FILEPATH6;
	}
	public void setFILEPATH6(String fILEPATH6) {
		FILEPATH6 = fILEPATH6;
	}
	public String getFILEPATH7() {
		return FILEPATH7;
	}
	public void setFILEPATH7(String fILEPATH7) {
		FILEPATH7 = fILEPATH7;
	}
	public String getFILEPATH8() {
		return FILEPATH8;
	}
	public void setFILEPATH8(String fILEPATH8) {
		FILEPATH8 = fILEPATH8;
	}
	public String getFILEPATH9() {
		return FILEPATH9;
	}
	public void setFILEPATH9(String fILEPATH9) {
		FILEPATH9 = fILEPATH9;
	}
	public String getFILEPATH10() {
		return FILEPATH10;
	}
	public void setFILEPATH10(String fILEPATH10) {
		FILEPATH10 = fILEPATH10;
	}
	public String getECOUNT_WH_CD() {
		return ECOUNT_WH_CD;
	}
	public void setECOUNT_WH_CD(String eCOUNT_WH_CD) {
		ECOUNT_WH_CD = eCOUNT_WH_CD;
	}
	public String getITEM_NM1() {
		return ITEM_NM1;
	}
	public void setITEM_NM1(String iTEM_NM1) {
		ITEM_NM1 = iTEM_NM1;
	}
	public String getITEM_NM2() {
		return ITEM_NM2;
	}
	public void setITEM_NM2(String iTEM_NM2) {
		ITEM_NM2 = iTEM_NM2;
	}
	public String getITEM_NM3() {
		return ITEM_NM3;
	}
	public void setITEM_NM3(String iTEM_NM3) {
		ITEM_NM3 = iTEM_NM3;
	}
	public String getITEM_NM4() {
		return ITEM_NM4;
	}
	public void setITEM_NM4(String iTEM_NM4) {
		ITEM_NM4 = iTEM_NM4;
	}
	public String getITEM_NM5() {
		return ITEM_NM5;
	}
	public void setITEM_NM5(String iTEM_NM5) {
		ITEM_NM5 = iTEM_NM5;
	}
	public String getITEM_NM6() {
		return ITEM_NM6;
	}
	public void setITEM_NM6(String iTEM_NM6) {
		ITEM_NM6 = iTEM_NM6;
	}
	public String getITEM_NM7() {
		return ITEM_NM7;
	}
	public void setITEM_NM7(String iTEM_NM7) {
		ITEM_NM7 = iTEM_NM7;
	}
	public String getITEM_NM8() {
		return ITEM_NM8;
	}
	public void setITEM_NM8(String iTEM_NM8) {
		ITEM_NM8 = iTEM_NM8;
	}
	public String getITEM_NM9() {
		return ITEM_NM9;
	}
	public void setITEM_NM9(String iTEM_NM9) {
		ITEM_NM9 = iTEM_NM9;
	}
	public String getITEM_NM10() {
		return ITEM_NM10;
	}
	public void setITEM_NM10(String iTEM_NM10) {
		ITEM_NM10 = iTEM_NM10;
	}
}

