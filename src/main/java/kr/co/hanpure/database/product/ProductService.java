package kr.co.hanpure.database.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ProductService {
	@Autowired
	private ProductMapper mapper;
	
	public void insertData(ProductVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(ProductVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public ProductVO selectData(ProductVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<ProductVO> selectDatas(ProductVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(ProductVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<ProductVO> selectDataList(ProductVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


