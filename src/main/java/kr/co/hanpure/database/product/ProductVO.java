package kr.co.hanpure.database.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ProductVO")
public class ProductVO extends Pagenation {
	private String PRODUCT_SEQ;
	private String RECIPE_SEQ;
	private String PRODUCT_TP;
	private String PRODUCT_NM;
	private String PRODUCT_DESC;
	private String CATEGORY1;
	private String CATEGORY2;	
	private String EVENT_YN;	
	private String PRODUCT_CD;
	private String ITEM_CD;
	private String SORT;
	private String USE_YN;
	private String QNTT;
	private String UNIT;
	private String CONTENT1;
	private String CONTENT2;
	private String PERIOD_ORDER;
	private String PERIOD_MAKE;
	private String MONTHLY_QNTT;
	private String FILEPATH1;
	private String FILEPATH2;
	private String FILEPATH3;
	private String FILEPATH4;
	private String FILEPATH5;
	private String FILEPATH6;
	private String FILEPATH7;
	private String FILEPATH8;
	private String FILEPATH9;
	private String FILEPATH10;	
	private String PRODUCT_TEXT; 
	private String PRODUCT_OPTION; 
	private String DEL_YN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	
	/////////
	private List<String> LIST_USE_YN = new ArrayList<String>();
	private List<String> LIST_FILEPATH = new ArrayList<String>();
	private List<String> LIST_PRODUCT_SEQ = new ArrayList<String>();
	private List<String> LIST_PRODUCT_NM = new ArrayList<String>();
	private List<String> LIST_SORT = new ArrayList<String>();	
	
	private String CATEGORY1_NM;
	private String CATEGORY2_NM;	
	private String AMOUNT;
	
	private String ITEM_NM;
	
	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}
	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}
	public String getRECIPE_SEQ() {
		return RECIPE_SEQ;
	}
	public void setRECIPE_SEQ(String rECIPE_SEQ) {
		RECIPE_SEQ = rECIPE_SEQ;
	}
	public String getPRODUCT_TP() {
		return PRODUCT_TP;
	}
	public void setPRODUCT_TP(String pRODUCT_TP) {
		PRODUCT_TP = pRODUCT_TP;
	}
	public String getPRODUCT_NM() {
		return PRODUCT_NM;
	}
	public void setPRODUCT_NM(String pRODUCT_NM) {
		PRODUCT_NM = pRODUCT_NM;
	}
	public String getPRODUCT_DESC() {
		return PRODUCT_DESC;
	}
	public void setPRODUCT_DESC(String pRODUCT_DESC) {
		PRODUCT_DESC = pRODUCT_DESC;
	}
	public String getCATEGORY1() {
		return CATEGORY1;
	}
	public void setCATEGORY1(String cATEGORY1) {
		CATEGORY1 = cATEGORY1;
	}
	public String getCATEGORY2() {
		return CATEGORY2;
	}
	public void setCATEGORY2(String cATEGORY2) {
		CATEGORY2 = cATEGORY2;
	}
	public String getEVENT_YN() {
		return EVENT_YN;
	}
	public void setEVENT_YN(String eVENT_YN) {
		EVENT_YN = eVENT_YN;
	}
	public String getPRODUCT_CD() {
		return PRODUCT_CD;
	}
	public void setPRODUCT_CD(String pRODUCT_CD) {
		PRODUCT_CD = pRODUCT_CD;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getQNTT() {
		return QNTT;
	}
	public void setQNTT(String qNTT) {
		QNTT = qNTT;
	}
	public String getUNIT() {
		return UNIT;
	}
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	public String getCONTENT1() {
		return CONTENT1;
	}
	public void setCONTENT1(String cONTENT1) {
		CONTENT1 = cONTENT1;
	}
	public String getCONTENT2() {
		return CONTENT2;
	}
	public void setCONTENT2(String cONTENT2) {
		CONTENT2 = cONTENT2;
	}
	public String getPERIOD_ORDER() {
		return PERIOD_ORDER;
	}
	public void setPERIOD_ORDER(String pERIOD_ORDER) {
		PERIOD_ORDER = pERIOD_ORDER;
	}
	public String getPERIOD_MAKE() {
		return PERIOD_MAKE;
	}
	public void setPERIOD_MAKE(String pERIOD_MAKE) {
		PERIOD_MAKE = pERIOD_MAKE;
	}
	public String getMONTHLY_QNTT() {
		return MONTHLY_QNTT;
	}
	public void setMONTHLY_QNTT(String mONTHLY_QNTT) {
		MONTHLY_QNTT = mONTHLY_QNTT;
	}
	public String getFILEPATH1() {
		return FILEPATH1;
	}
	public void setFILEPATH1(String fILEPATH1) {
		FILEPATH1 = fILEPATH1;
	}
	public String getFILEPATH2() {
		return FILEPATH2;
	}
	public void setFILEPATH2(String fILEPATH2) {
		FILEPATH2 = fILEPATH2;
	}
	public String getFILEPATH3() {
		return FILEPATH3;
	}
	public void setFILEPATH3(String fILEPATH3) {
		FILEPATH3 = fILEPATH3;
	}
	public String getFILEPATH4() {
		return FILEPATH4;
	}
	public void setFILEPATH4(String fILEPATH4) {
		FILEPATH4 = fILEPATH4;
	}
	public String getFILEPATH5() {
		return FILEPATH5;
	}
	public void setFILEPATH5(String fILEPATH5) {
		FILEPATH5 = fILEPATH5;
	}
	public String getFILEPATH6() {
		return FILEPATH6;
	}
	public void setFILEPATH6(String fILEPATH6) {
		FILEPATH6 = fILEPATH6;
	}
	public String getFILEPATH7() {
		return FILEPATH7;
	}
	public void setFILEPATH7(String fILEPATH7) {
		FILEPATH7 = fILEPATH7;
	}
	public String getFILEPATH8() {
		return FILEPATH8;
	}
	public void setFILEPATH8(String fILEPATH8) {
		FILEPATH8 = fILEPATH8;
	}
	public String getFILEPATH9() {
		return FILEPATH9;
	}
	public void setFILEPATH9(String fILEPATH9) {
		FILEPATH9 = fILEPATH9;
	}
	public String getFILEPATH10() {
		return FILEPATH10;
	}
	public void setFILEPATH10(String fILEPATH10) {
		FILEPATH10 = fILEPATH10;
	}
	public String getPRODUCT_TEXT() {
		return PRODUCT_TEXT;
	}
	public void setPRODUCT_TEXT(String pRODUCT_TEXT) {
		PRODUCT_TEXT = pRODUCT_TEXT;
	}
	public String getPRODUCT_OPTION() {
		return PRODUCT_OPTION;
	}
	public void setPRODUCT_OPTION(String pRODUCT_OPTION) {
		PRODUCT_OPTION = pRODUCT_OPTION;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public List<String> getLIST_USE_YN() {
		return LIST_USE_YN;
	}
	public void setLIST_USE_YN(List<String> lIST_USE_YN) {
		LIST_USE_YN = lIST_USE_YN;
	}
	public List<String> getLIST_FILEPATH() {
		return LIST_FILEPATH;
	}
	public void setLIST_FILEPATH(List<String> lIST_FILEPATH) {
		LIST_FILEPATH = lIST_FILEPATH;
	}
	public List<String> getLIST_PRODUCT_SEQ() {
		return LIST_PRODUCT_SEQ;
	}
	public void setLIST_PRODUCT_SEQ(List<String> lIST_PRODUCT_SEQ) {
		LIST_PRODUCT_SEQ = lIST_PRODUCT_SEQ;
	}
	public List<String> getLIST_PRODUCT_NM() {
		return LIST_PRODUCT_NM;
	}
	public void setLIST_PRODUCT_NM(List<String> lIST_PRODUCT_NM) {
		LIST_PRODUCT_NM = lIST_PRODUCT_NM;
	}
	public List<String> getLIST_SORT() {
		return LIST_SORT;
	}
	public void setLIST_SORT(List<String> lIST_SORT) {
		LIST_SORT = lIST_SORT;
	}
	public String getITEM_CD() {
		return ITEM_CD;
	}
	public void setITEM_CD(String iTEM_CD) {
		ITEM_CD = iTEM_CD;
	}
	public String getITEM_NM() {
		return ITEM_NM;
	}
	public void setITEM_NM(String iTEM_NM) {
		ITEM_NM = iTEM_NM;
	}
	public String getCATEGORY1_NM() {
		return CATEGORY1_NM;
	}
	public void setCATEGORY1_NM(String cATEGORY1_NM) {
		CATEGORY1_NM = cATEGORY1_NM;
	}
	public String getCATEGORY2_NM() {
		return CATEGORY2_NM;
	}
	public void setCATEGORY2_NM(String cATEGORY2_NM) {
		CATEGORY2_NM = cATEGORY2_NM;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}	
}
