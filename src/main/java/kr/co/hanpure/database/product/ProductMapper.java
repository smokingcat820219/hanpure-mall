package kr.co.hanpure.database.product;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ProductMapper {	
	void insertData(ProductVO vo);
	void updateData(ProductVO vo);
	ProductVO selectData(ProductVO vo);
	List<ProductVO> selectDatas(ProductVO vo);
	Integer selectDataCount(ProductVO vo);
	List<ProductVO> selectDataList(ProductVO vo);
}

