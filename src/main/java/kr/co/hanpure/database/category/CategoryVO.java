package kr.co.hanpure.database.category;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("CategoryVO")
public class CategoryVO extends Pagenation {
	private String PRODUCT_TP;
	private String P_CODE;
	private String CODE;
	private String NAME;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	public String getPRODUCT_TP() {
		return PRODUCT_TP;
	}
	public void setPRODUCT_TP(String pRODUCT_TP) {
		PRODUCT_TP = pRODUCT_TP;
	}
	public String getP_CODE() {
		return P_CODE;
	}
	public void setP_CODE(String p_CODE) {
		P_CODE = p_CODE;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}	
}

