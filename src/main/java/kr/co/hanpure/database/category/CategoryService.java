package kr.co.hanpure.database.category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class CategoryService {
	@Autowired
	private CategoryMapper mapper;
	
	public void insertData(CategoryVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteData(CategoryVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public CategoryVO selectData(CategoryVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}	
	
	public List<CategoryVO> selectDatas(CategoryVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}		
	
	public Map<String, String> selectDatasMap(CategoryVO vo) {
		Logger.info();
		
		Map<String, String> map = new HashMap<String, String>();
		List<CategoryVO> list = this.mapper.selectDatas(vo);
		for(int i = 0; i < list.size(); i++) {
			map.put(list.get(i).getCODE(), list.get(i).getNAME());
		}
		
		return map;
	}	
}


