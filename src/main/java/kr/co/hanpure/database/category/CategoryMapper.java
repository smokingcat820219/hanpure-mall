package kr.co.hanpure.database.category;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CategoryMapper {	
	void truncate();
	
	void insertData(CategoryVO vo);
	void deleteData(CategoryVO vo);
	CategoryVO selectData(CategoryVO vo);
	List<CategoryVO> selectDatas(CategoryVO vo);
}
