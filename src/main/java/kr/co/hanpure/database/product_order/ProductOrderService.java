package kr.co.hanpure.database.product_order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ProductOrderService {
	@Autowired
	private ProductOrderMapper mapper;
	
	public void insertData(ProductOrderVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public ProductOrderVO selectData(ProductOrderVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<ProductOrderVO> selectDatas(ProductOrderVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
}


