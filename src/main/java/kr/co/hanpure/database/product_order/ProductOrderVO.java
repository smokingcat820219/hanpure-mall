package kr.co.hanpure.database.product_order;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ProductOrderVO")
public class ProductOrderVO extends Pagenation {
	private String ORDER_SEQ;
	private String HOSPITAL_ID;
	private String DOCTOR;
	private String PRODUCT_NM;
	private String OPTION_NM;
	private String PRODUCT_SEQ;
	private String SORT;
	private String PRICE;
	private String ORDER_QNTT;
	private String AMOUNT;
	
	private String DRUG_LABEL_TP;
	private String DRUG_LABEL_TEXT;
	private String DELIVERY_LABEL_TP;
	private String DELIVERY_LABEL_TEXT;
	
	private String FILEPATH1;
	private String FILEPATH2;
	private String FILEPATH3;
	private String FILEPATH4;
	private String FILEPATH5;
	private String FILEPATH6;
	private String FILEPATH7;
	private String FILEPATH8;
	private String FILEPATH9;
	private String FILEPATH10;	
	
	public String getORDER_SEQ() {
		return ORDER_SEQ;
	}
	public void setORDER_SEQ(String oRDER_SEQ) {
		ORDER_SEQ = oRDER_SEQ;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getDOCTOR() {
		return DOCTOR;
	}
	public void setDOCTOR(String dOCTOR) {
		DOCTOR = dOCTOR;
	}
	public String getPRODUCT_NM() {
		return PRODUCT_NM;
	}
	public void setPRODUCT_NM(String pRODUCT_NM) {
		PRODUCT_NM = pRODUCT_NM;
	}
	public String getOPTION_NM() {
		return OPTION_NM;
	}
	public void setOPTION_NM(String oPTION_NM) {
		OPTION_NM = oPTION_NM;
	}
	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}
	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getORDER_QNTT() {
		return ORDER_QNTT;
	}
	public void setORDER_QNTT(String oRDER_QNTT) {
		ORDER_QNTT = oRDER_QNTT;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getDRUG_LABEL_TP() {
		return DRUG_LABEL_TP;
	}
	public void setDRUG_LABEL_TP(String dRUG_LABEL_TP) {
		DRUG_LABEL_TP = dRUG_LABEL_TP;
	}
	public String getDRUG_LABEL_TEXT() {
		return DRUG_LABEL_TEXT;
	}
	public void setDRUG_LABEL_TEXT(String dRUG_LABEL_TEXT) {
		DRUG_LABEL_TEXT = dRUG_LABEL_TEXT;
	}
	public String getDELIVERY_LABEL_TP() {
		return DELIVERY_LABEL_TP;
	}
	public void setDELIVERY_LABEL_TP(String dELIVERY_LABEL_TP) {
		DELIVERY_LABEL_TP = dELIVERY_LABEL_TP;
	}
	public String getDELIVERY_LABEL_TEXT() {
		return DELIVERY_LABEL_TEXT;
	}
	public void setDELIVERY_LABEL_TEXT(String dELIVERY_LABEL_TEXT) {
		DELIVERY_LABEL_TEXT = dELIVERY_LABEL_TEXT;
	}
	public String getFILEPATH1() {
		return FILEPATH1;
	}
	public void setFILEPATH1(String fILEPATH1) {
		FILEPATH1 = fILEPATH1;
	}
	public String getFILEPATH2() {
		return FILEPATH2;
	}
	public void setFILEPATH2(String fILEPATH2) {
		FILEPATH2 = fILEPATH2;
	}
	public String getFILEPATH3() {
		return FILEPATH3;
	}
	public void setFILEPATH3(String fILEPATH3) {
		FILEPATH3 = fILEPATH3;
	}
	public String getFILEPATH4() {
		return FILEPATH4;
	}
	public void setFILEPATH4(String fILEPATH4) {
		FILEPATH4 = fILEPATH4;
	}
	public String getFILEPATH5() {
		return FILEPATH5;
	}
	public void setFILEPATH5(String fILEPATH5) {
		FILEPATH5 = fILEPATH5;
	}
	public String getFILEPATH6() {
		return FILEPATH6;
	}
	public void setFILEPATH6(String fILEPATH6) {
		FILEPATH6 = fILEPATH6;
	}
	public String getFILEPATH7() {
		return FILEPATH7;
	}
	public void setFILEPATH7(String fILEPATH7) {
		FILEPATH7 = fILEPATH7;
	}
	public String getFILEPATH8() {
		return FILEPATH8;
	}
	public void setFILEPATH8(String fILEPATH8) {
		FILEPATH8 = fILEPATH8;
	}
	public String getFILEPATH9() {
		return FILEPATH9;
	}
	public void setFILEPATH9(String fILEPATH9) {
		FILEPATH9 = fILEPATH9;
	}
	public String getFILEPATH10() {
		return FILEPATH10;
	}
	public void setFILEPATH10(String fILEPATH10) {
		FILEPATH10 = fILEPATH10;
	}	
}

