package kr.co.hanpure.database.product_order;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ProductOrderMapper {		
	void insertData(ProductOrderVO vo);
	ProductOrderVO selectData(ProductOrderVO vo);
	List<ProductOrderVO> selectDatas(ProductOrderVO vo);
}
