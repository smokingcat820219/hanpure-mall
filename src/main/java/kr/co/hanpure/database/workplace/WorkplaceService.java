package kr.co.hanpure.database.workplace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class WorkplaceService {
	@Autowired
	private WorkplaceMapper mapper;
	
	public void insertData(WorkplaceVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(WorkplaceVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void updateWhCd(WorkplaceVO vo) {
		Logger.info();
		this.mapper.updateWhCd(vo);
	}
	
	public void deleteData(WorkplaceVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public String selectWhCd() {
		Logger.info();
		return this.mapper.selectWhCd();
	}
	
	public WorkplaceVO selectData(WorkplaceVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<WorkplaceVO> selectDatas(WorkplaceVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(WorkplaceVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<WorkplaceVO> selectDataList(WorkplaceVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


