package kr.co.hanpure.database.workplace;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface WorkplaceMapper {	
	void insertData(WorkplaceVO vo);
	void updateData(WorkplaceVO vo);
	void updateWhCd(WorkplaceVO vo);
	void deleteData(WorkplaceVO vo);
	String selectWhCd();
	WorkplaceVO selectData(WorkplaceVO vo);
	List<WorkplaceVO> selectDatas(WorkplaceVO vo);
	Integer selectDataCount(WorkplaceVO vo);
	List<WorkplaceVO> selectDataList(WorkplaceVO vo);
}
