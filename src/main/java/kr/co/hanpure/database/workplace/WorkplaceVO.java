package kr.co.hanpure.database.workplace;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("WorkplaceVO")
public class WorkplaceVO extends Pagenation {
	private String WORKPLACE_CD;
	private String WORKPLACE_NM;
	private String WORKPLACE_TP;
	private String WH_CD;
	private String WH_NM;
	private String BIZ_ID;
	private String ZIPCODE;
	private String ADDRESS;
	private String ADDRESS2;
	private String WH_CD_YN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	private String WORKPLACE_TP_NM;

	public String getWORKPLACE_CD() {
		return WORKPLACE_CD;
	}

	public void setWORKPLACE_CD(String wORKPLACE_CD) {
		WORKPLACE_CD = wORKPLACE_CD;
	}

	public String getWORKPLACE_NM() {
		return WORKPLACE_NM;
	}

	public void setWORKPLACE_NM(String wORKPLACE_NM) {
		WORKPLACE_NM = wORKPLACE_NM;
	}

	public String getWORKPLACE_TP() {
		return WORKPLACE_TP;
	}

	public void setWORKPLACE_TP(String wORKPLACE_TP) {
		WORKPLACE_TP = wORKPLACE_TP;
	}

	public String getWH_CD() {
		return WH_CD;
	}

	public void setWH_CD(String wH_CD) {
		WH_CD = wH_CD;
	}

	public String getWH_NM() {
		return WH_NM;
	}

	public void setWH_NM(String wH_NM) {
		WH_NM = wH_NM;
	}

	public String getBIZ_ID() {
		return BIZ_ID;
	}

	public void setBIZ_ID(String bIZ_ID) {
		BIZ_ID = bIZ_ID;
	}

	public String getZIPCODE() {
		return ZIPCODE;
	}

	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getADDRESS2() {
		return ADDRESS2;
	}

	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}

	public String getCREATED_AT() {
		return CREATED_AT;
	}

	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}

	public String getCREATED_BY() {
		return CREATED_BY;
	}

	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}

	public String getUPDATED_AT() {
		return UPDATED_AT;
	}

	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}

	public String getUPDATED_BY() {
		return UPDATED_BY;
	}

	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}

	public String getWORKPLACE_TP_NM() {
		return WORKPLACE_TP_NM;
	}

	public void setWORKPLACE_TP_NM(String wORKPLACE_TP_NM) {
		WORKPLACE_TP_NM = wORKPLACE_TP_NM;
	}

	public String getWH_CD_YN() {
		return WH_CD_YN;
	}

	public void setWH_CD_YN(String wH_CD_YN) {
		WH_CD_YN = wH_CD_YN;
	}	
}
