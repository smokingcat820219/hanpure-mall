package kr.co.hanpure.database.recipe;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface RecipeMapper {	
	void insertData(RecipeVO vo);
	void updateData(RecipeVO vo);
	RecipeVO selectData(RecipeVO vo);
	RecipeVO selectDataOne(RecipeVO vo);
	List<RecipeVO> selectDatas(RecipeVO vo);
	
	Integer selectDataCount(RecipeVO vo);
	List<RecipeVO> selectDataList(RecipeVO vo);
}
