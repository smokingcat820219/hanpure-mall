package kr.co.hanpure.database.recipe;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("RecipeVO")
public class RecipeVO extends Pagenation {
	private String RECIPE_SEQ;
	private String ORDER_SEQ;
	private String RECIPE_CD;
	private String HOSPITAL_ID;
	private String DOCTOR;
	private String PATIENT_SEQ;
	private String REMARK;
	private String RECIPE_TP;
	private String INFLOW;
	private String RECIPE_NM;
	private String DRUG_NM;
	private String INFO1;
	private String INFO2;
	private String INFO3;
	private String INFO4;
	private String INFO5;
	private String INFO6;
	private String INFO7;
	private String INFO8;
	private String INFO9;
	private String INFO10;
	private String INFO10_QNTT;
	private String INFO11;
	
	private String MARKING_TP;
	private String MARKING_TEXT;
	private String PACKAGE1;
	private String PACKAGE2;
	private String PACKAGE3;
	private String PACKAGE4;
	private String PACKAGE1_ORDER;
	private String PACKAGE2_ORDER;
	private String PACKAGE3_ORDER;
	private String PACKAGE4_ORDER;
	private String VACCUM;
	private String REMARK1;
	private String REMARK2;
	private String REMARK3;
	private String REMARK4;
	private String REMARK5;
	private String REMARK6;
	private String REMARK7;
	private String REMARK8;
	private String REMARK9;
	private String REMARK10;
	private String REMARK11;
	private String PRICE1;
	private String PRICE2;
	private String PRICE3;
	private String PRICE4;
	private String PRICE5;
	private String PRICE6;
	private String PRICE7;
	private String PRICE8;
	private String PRICE9;
	private String PRICE10;	
	private String PRICE11;
	private String TOTAL_CHUP;
	private String TOTAL_QNTT;
	private String TOTAL_PRICE;
	private String AMOUNT;
	private String HOWTOMAKE;
	private String HOWTOEAT;
	private String DRUG_LABEL_TP;
	private String DRUG_LABEL_TEXT;
	private String DELIVERY_LABEL_TP;
	private String DELIVERY_LABEL_TEXT;
	private String SAVE_TEMP;
	private String PRIVATE_YN;
	private String MEMO;
	private String RE_HOTPOT;
	private String DEL_YN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	private String RECIPE_CNT;
	
	//추가항목
	private String RECIPE_TP_NM;
	private String INFO10_NM;
	private String INFO10_PRICE;
	private String PACKAGE1_NM;
	private String PACKAGE2_NM;
	private String PACKAGE3_NM;
	private String PACKAGE4_NM;
	private String PACKAGE1_PRICE;
	private String PACKAGE2_PRICE;
	private String PACKAGE3_PRICE;
	private String PACKAGE4_PRICE;
	private String PACKAGE1_MAKE_PRICE;
	private String PACKAGE2_MAKE_PRICE;
	private String PACKAGE3_MAKE_PRICE;
	private String PACKAGE4_MAKE_PRICE;
	private String PACKAGE1_FILEPATH;
	private String PACKAGE2_FILEPATH;
	private String PACKAGE3_FILEPATH;
	private String PACKAGE4_FILEPATH;
	private String PACKAGE1_QNTT;
	private String PACKAGE2_QNTT;
	private String PACKAGE3_QNTT;
	private String PACKAGE4_QNTT;
	private String PACKAGE1_TP;
	private String PACKAGE2_TP;
	private String PACKAGE3_TP;
	private String PACKAGE4_TP;
	
	private String PATIENT_NM;
	private String sdate1;
	private String edate1;
	
	private String ETC1;
	private String ETC2;
	private String ETC3;
	
	private List<String> LIST_RECIPE_SEQ = new ArrayList<String>();
	
	public String getRECIPE_SEQ() {
		return RECIPE_SEQ;
	}
	public void setRECIPE_SEQ(String rECIPE_SEQ) {
		RECIPE_SEQ = rECIPE_SEQ;
	}
	public String getORDER_SEQ() {
		return ORDER_SEQ;
	}
	public void setORDER_SEQ(String oRDER_SEQ) {
		ORDER_SEQ = oRDER_SEQ;
	}
	public String getRECIPE_CD() {
		return RECIPE_CD;
	}
	public void setRECIPE_CD(String rECIPE_CD) {
		RECIPE_CD = rECIPE_CD;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getDOCTOR() {
		return DOCTOR;
	}
	public void setDOCTOR(String dOCTOR) {
		DOCTOR = dOCTOR;
	}
	public String getPATIENT_SEQ() {
		return PATIENT_SEQ;
	}
	public void setPATIENT_SEQ(String pATIENT_SEQ) {
		PATIENT_SEQ = pATIENT_SEQ;
	}
	public String getREMARK() {
		return REMARK;
	}
	public void setREMARK(String rEMARK) {
		REMARK = rEMARK;
	}
	public String getRECIPE_TP() {
		return RECIPE_TP;
	}
	public void setRECIPE_TP(String rECIPE_TP) {
		RECIPE_TP = rECIPE_TP;
	}
	public String getINFLOW() {
		return INFLOW;
	}
	public void setINFLOW(String iNFLOW) {
		INFLOW = iNFLOW;
	}
	public String getRECIPE_NM() {
		return RECIPE_NM;
	}
	public void setRECIPE_NM(String rECIPE_NM) {
		RECIPE_NM = rECIPE_NM;
	}
	public String getDRUG_NM() {
		return DRUG_NM;
	}
	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}
	public String getINFO1() {
		return INFO1;
	}
	public void setINFO1(String iNFO1) {
		INFO1 = iNFO1;
	}
	public String getINFO2() {
		return INFO2;
	}
	public void setINFO2(String iNFO2) {
		INFO2 = iNFO2;
	}
	public String getINFO3() {
		return INFO3;
	}
	public void setINFO3(String iNFO3) {
		INFO3 = iNFO3;
	}
	public String getINFO4() {
		return INFO4;
	}
	public void setINFO4(String iNFO4) {
		INFO4 = iNFO4;
	}
	public String getINFO5() {
		return INFO5;
	}
	public void setINFO5(String iNFO5) {
		INFO5 = iNFO5;
	}
	public String getINFO6() {
		return INFO6;
	}
	public void setINFO6(String iNFO6) {
		INFO6 = iNFO6;
	}
	public String getINFO7() {
		return INFO7;
	}
	public void setINFO7(String iNFO7) {
		INFO7 = iNFO7;
	}
	public String getINFO8() {
		return INFO8;
	}
	public void setINFO8(String iNFO8) {
		INFO8 = iNFO8;
	}
	public String getINFO9() {
		return INFO9;
	}
	public void setINFO9(String iNFO9) {
		INFO9 = iNFO9;
	}
	public String getINFO10() {
		return INFO10;
	}
	public void setINFO10(String iNFO10) {
		INFO10 = iNFO10;
	}
	public String getMARKING_TP() {
		return MARKING_TP;
	}
	public void setMARKING_TP(String mARKING_TP) {
		MARKING_TP = mARKING_TP;
	}
	public String getMARKING_TEXT() {
		return MARKING_TEXT;
	}
	public void setMARKING_TEXT(String mARKING_TEXT) {
		MARKING_TEXT = mARKING_TEXT;
	}
	public String getPACKAGE1() {
		return PACKAGE1;
	}
	public void setPACKAGE1(String pACKAGE1) {
		PACKAGE1 = pACKAGE1;
	}
	public String getPACKAGE2() {
		return PACKAGE2;
	}
	public void setPACKAGE2(String pACKAGE2) {
		PACKAGE2 = pACKAGE2;
	}
	public String getPACKAGE3() {
		return PACKAGE3;
	}
	public void setPACKAGE3(String pACKAGE3) {
		PACKAGE3 = pACKAGE3;
	}
	public String getPACKAGE1_QNTT() {
		return PACKAGE1_QNTT;
	}
	public void setPACKAGE1_QNTT(String pACKAGE1_QNTT) {
		PACKAGE1_QNTT = pACKAGE1_QNTT;
	}
	public String getPACKAGE2_QNTT() {
		return PACKAGE2_QNTT;
	}
	public void setPACKAGE2_QNTT(String pACKAGE2_QNTT) {
		PACKAGE2_QNTT = pACKAGE2_QNTT;
	}
	public String getPACKAGE3_QNTT() {
		return PACKAGE3_QNTT;
	}
	public void setPACKAGE3_QNTT(String pACKAGE3_QNTT) {
		PACKAGE3_QNTT = pACKAGE3_QNTT;
	}
	public String getVACCUM() {
		return VACCUM;
	}
	public void setVACCUM(String vACCUM) {
		VACCUM = vACCUM;
	}
	public String getREMARK1() {
		return REMARK1;
	}
	public void setREMARK1(String rEMARK1) {
		REMARK1 = rEMARK1;
	}
	public String getREMARK2() {
		return REMARK2;
	}
	public void setREMARK2(String rEMARK2) {
		REMARK2 = rEMARK2;
	}
	public String getREMARK3() {
		return REMARK3;
	}
	public void setREMARK3(String rEMARK3) {
		REMARK3 = rEMARK3;
	}
	public String getREMARK4() {
		return REMARK4;
	}
	public void setREMARK4(String rEMARK4) {
		REMARK4 = rEMARK4;
	}
	public String getREMARK5() {
		return REMARK5;
	}
	public void setREMARK5(String rEMARK5) {
		REMARK5 = rEMARK5;
	}
	public String getREMARK6() {
		return REMARK6;
	}
	public void setREMARK6(String rEMARK6) {
		REMARK6 = rEMARK6;
	}
	public String getREMARK7() {
		return REMARK7;
	}
	public void setREMARK7(String rEMARK7) {
		REMARK7 = rEMARK7;
	}
	public String getREMARK8() {
		return REMARK8;
	}
	public void setREMARK8(String rEMARK8) {
		REMARK8 = rEMARK8;
	}
	public String getREMARK9() {
		return REMARK9;
	}
	public void setREMARK9(String rEMARK9) {
		REMARK9 = rEMARK9;
	}
	public String getREMARK10() {
		return REMARK10;
	}
	public void setREMARK10(String rEMARK10) {
		REMARK10 = rEMARK10;
	}
	public String getPRICE1() {
		return PRICE1;
	}
	public void setPRICE1(String pRICE1) {
		PRICE1 = pRICE1;
	}
	public String getPRICE2() {
		return PRICE2;
	}
	public void setPRICE2(String pRICE2) {
		PRICE2 = pRICE2;
	}
	public String getPRICE3() {
		return PRICE3;
	}
	public void setPRICE3(String pRICE3) {
		PRICE3 = pRICE3;
	}
	public String getPRICE4() {
		return PRICE4;
	}
	public void setPRICE4(String pRICE4) {
		PRICE4 = pRICE4;
	}
	public String getPRICE5() {
		return PRICE5;
	}
	public void setPRICE5(String pRICE5) {
		PRICE5 = pRICE5;
	}
	public String getPRICE6() {
		return PRICE6;
	}
	public void setPRICE6(String pRICE6) {
		PRICE6 = pRICE6;
	}
	public String getPRICE7() {
		return PRICE7;
	}
	public void setPRICE7(String pRICE7) {
		PRICE7 = pRICE7;
	}
	public String getPRICE8() {
		return PRICE8;
	}
	public void setPRICE8(String pRICE8) {
		PRICE8 = pRICE8;
	}
	public String getPRICE9() {
		return PRICE9;
	}
	public void setPRICE9(String pRICE9) {
		PRICE9 = pRICE9;
	}
	public String getPRICE10() {
		return PRICE10;
	}
	public void setPRICE10(String pRICE10) {
		PRICE10 = pRICE10;
	}	
	public String getTOTAL_QNTT() {
		return TOTAL_QNTT;
	}
	public void setTOTAL_QNTT(String tOTAL_QNTT) {
		TOTAL_QNTT = tOTAL_QNTT;
	}
	public String getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}
	public void setTOTAL_PRICE(String tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}
	public String getHOWTOMAKE() {
		return HOWTOMAKE;
	}
	public void setHOWTOMAKE(String hOWTOMAKE) {
		HOWTOMAKE = hOWTOMAKE;
	}
	public String getHOWTOEAT() {
		return HOWTOEAT;
	}
	public void setHOWTOEAT(String hOWTOEAT) {
		HOWTOEAT = hOWTOEAT;
	}
	public String getDRUG_LABEL_TP() {
		return DRUG_LABEL_TP;
	}
	public void setDRUG_LABEL_TP(String dRUG_LABEL_TP) {
		DRUG_LABEL_TP = dRUG_LABEL_TP;
	}
	public String getDRUG_LABEL_TEXT() {
		return DRUG_LABEL_TEXT;
	}
	public void setDRUG_LABEL_TEXT(String dRUG_LABEL_TEXT) {
		DRUG_LABEL_TEXT = dRUG_LABEL_TEXT;
	}
	public String getDELIVERY_LABEL_TP() {
		return DELIVERY_LABEL_TP;
	}
	public void setDELIVERY_LABEL_TP(String dELIVERY_LABEL_TP) {
		DELIVERY_LABEL_TP = dELIVERY_LABEL_TP;
	}
	public String getDELIVERY_LABEL_TEXT() {
		return DELIVERY_LABEL_TEXT;
	}
	public void setDELIVERY_LABEL_TEXT(String dELIVERY_LABEL_TEXT) {
		DELIVERY_LABEL_TEXT = dELIVERY_LABEL_TEXT;
	}
	public String getSAVE_TEMP() {
		return SAVE_TEMP;
	}
	public void setSAVE_TEMP(String sAVE_TEMP) {
		SAVE_TEMP = sAVE_TEMP;
	}
	public String getPRIVATE_YN() {
		return PRIVATE_YN;
	}
	public void setPRIVATE_YN(String pRIVATE_YN) {
		PRIVATE_YN = pRIVATE_YN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getRECIPE_TP_NM() {
		return RECIPE_TP_NM;
	}
	public void setRECIPE_TP_NM(String rECIPE_TP_NM) {
		RECIPE_TP_NM = rECIPE_TP_NM;
	}	
	public String getPACKAGE1_NM() {
		return PACKAGE1_NM;
	}
	public void setPACKAGE1_NM(String pACKAGE1_NM) {
		PACKAGE1_NM = pACKAGE1_NM;
	}
	public String getPACKAGE2_NM() {
		return PACKAGE2_NM;
	}
	public void setPACKAGE2_NM(String pACKAGE2_NM) {
		PACKAGE2_NM = pACKAGE2_NM;
	}
	public String getPACKAGE3_NM() {
		return PACKAGE3_NM;
	}
	public void setPACKAGE3_NM(String pACKAGE3_NM) {
		PACKAGE3_NM = pACKAGE3_NM;
	}
	public String getPACKAGE1_PRICE() {
		return PACKAGE1_PRICE;
	}
	public void setPACKAGE1_PRICE(String pACKAGE1_PRICE) {
		PACKAGE1_PRICE = pACKAGE1_PRICE;
	}
	public String getPACKAGE2_PRICE() {
		return PACKAGE2_PRICE;
	}
	public void setPACKAGE2_PRICE(String pACKAGE2_PRICE) {
		PACKAGE2_PRICE = pACKAGE2_PRICE;
	}
	public String getPACKAGE3_PRICE() {
		return PACKAGE3_PRICE;
	}
	public void setPACKAGE3_PRICE(String pACKAGE3_PRICE) {
		PACKAGE3_PRICE = pACKAGE3_PRICE;
	}
	public String getPACKAGE1_MAKE_PRICE() {
		return PACKAGE1_MAKE_PRICE;
	}
	public void setPACKAGE1_MAKE_PRICE(String pACKAGE1_MAKE_PRICE) {
		PACKAGE1_MAKE_PRICE = pACKAGE1_MAKE_PRICE;
	}
	public String getPACKAGE2_MAKE_PRICE() {
		return PACKAGE2_MAKE_PRICE;
	}
	public void setPACKAGE2_MAKE_PRICE(String pACKAGE2_MAKE_PRICE) {
		PACKAGE2_MAKE_PRICE = pACKAGE2_MAKE_PRICE;
	}
	public String getPACKAGE3_MAKE_PRICE() {
		return PACKAGE3_MAKE_PRICE;
	}
	public void setPACKAGE3_MAKE_PRICE(String pACKAGE3_MAKE_PRICE) {
		PACKAGE3_MAKE_PRICE = pACKAGE3_MAKE_PRICE;
	}
	public String getPACKAGE1_FILEPATH() {
		return PACKAGE1_FILEPATH;
	}
	public void setPACKAGE1_FILEPATH(String pACKAGE1_FILEPATH) {
		PACKAGE1_FILEPATH = pACKAGE1_FILEPATH;
	}
	public String getPACKAGE2_FILEPATH() {
		return PACKAGE2_FILEPATH;
	}
	public void setPACKAGE2_FILEPATH(String pACKAGE2_FILEPATH) {
		PACKAGE2_FILEPATH = pACKAGE2_FILEPATH;
	}
	public String getPACKAGE3_FILEPATH() {
		return PACKAGE3_FILEPATH;
	}
	public void setPACKAGE3_FILEPATH(String pACKAGE3_FILEPATH) {
		PACKAGE3_FILEPATH = pACKAGE3_FILEPATH;
	}
	public String getPACKAGE1_ORDER() {
		return PACKAGE1_ORDER;
	}
	public void setPACKAGE1_ORDER(String pACKAGE1_ORDER) {
		PACKAGE1_ORDER = pACKAGE1_ORDER;
	}
	public String getPACKAGE2_ORDER() {
		return PACKAGE2_ORDER;
	}
	public void setPACKAGE2_ORDER(String pACKAGE2_ORDER) {
		PACKAGE2_ORDER = pACKAGE2_ORDER;
	}
	public String getPACKAGE3_ORDER() {
		return PACKAGE3_ORDER;
	}
	public void setPACKAGE3_ORDER(String pACKAGE3_ORDER) {
		PACKAGE3_ORDER = pACKAGE3_ORDER;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getINFO10_QNTT() {
		return INFO10_QNTT;
	}
	public void setINFO10_QNTT(String iNFO10_QNTT) {
		INFO10_QNTT = iNFO10_QNTT;
	}
	public String getINFO11() {
		return INFO11;
	}
	public void setINFO11(String iNFO11) {
		INFO11 = iNFO11;
	}
	public String getINFO10_NM() {
		return INFO10_NM;
	}
	public void setINFO10_NM(String iNFO10_NM) {
		INFO10_NM = iNFO10_NM;
	}
	public String getINFO10_PRICE() {
		return INFO10_PRICE;
	}
	public void setINFO10_PRICE(String iNFO10_PRICE) {
		INFO10_PRICE = iNFO10_PRICE;
	}
	public String getREMARK11() {
		return REMARK11;
	}
	public void setREMARK11(String rEMARK11) {
		REMARK11 = rEMARK11;
	}
	public String getPRICE11() {
		return PRICE11;
	}
	public void setPRICE11(String pRICE11) {
		PRICE11 = pRICE11;
	}
	public String getTOTAL_CHUP() {
		return TOTAL_CHUP;
	}
	public void setTOTAL_CHUP(String tOTAL_CHUP) {
		TOTAL_CHUP = tOTAL_CHUP;
	}
	public String getPACKAGE4() {
		return PACKAGE4;
	}
	public void setPACKAGE4(String pACKAGE4) {
		PACKAGE4 = pACKAGE4;
	}
	public String getPACKAGE4_ORDER() {
		return PACKAGE4_ORDER;
	}
	public void setPACKAGE4_ORDER(String pACKAGE4_ORDER) {
		PACKAGE4_ORDER = pACKAGE4_ORDER;
	}
	public String getPACKAGE4_NM() {
		return PACKAGE4_NM;
	}
	public void setPACKAGE4_NM(String pACKAGE4_NM) {
		PACKAGE4_NM = pACKAGE4_NM;
	}
	public String getPACKAGE4_PRICE() {
		return PACKAGE4_PRICE;
	}
	public void setPACKAGE4_PRICE(String pACKAGE4_PRICE) {
		PACKAGE4_PRICE = pACKAGE4_PRICE;
	}
	public String getPACKAGE4_MAKE_PRICE() {
		return PACKAGE4_MAKE_PRICE;
	}
	public void setPACKAGE4_MAKE_PRICE(String pACKAGE4_MAKE_PRICE) {
		PACKAGE4_MAKE_PRICE = pACKAGE4_MAKE_PRICE;
	}
	public String getPACKAGE4_FILEPATH() {
		return PACKAGE4_FILEPATH;
	}
	public void setPACKAGE4_FILEPATH(String pACKAGE4_FILEPATH) {
		PACKAGE4_FILEPATH = pACKAGE4_FILEPATH;
	}
	public String getPACKAGE4_QNTT() {
		return PACKAGE4_QNTT;
	}
	public void setPACKAGE4_QNTT(String pACKAGE4_QNTT) {
		PACKAGE4_QNTT = pACKAGE4_QNTT;
	}
	public List<String> getLIST_RECIPE_SEQ() {
		return LIST_RECIPE_SEQ;
	}
	public void setLIST_RECIPE_SEQ(List<String> lIST_RECIPE_SEQ) {
		LIST_RECIPE_SEQ = lIST_RECIPE_SEQ;
	}
	public String getPATIENT_NM() {
		return PATIENT_NM;
	}
	public void setPATIENT_NM(String pATIENT_NM) {
		PATIENT_NM = pATIENT_NM;
	}
	public String getSdate1() {
		return sdate1;
	}
	public void setSdate1(String sdate1) {
		this.sdate1 = sdate1;
	}
	public String getEdate1() {
		return edate1;
	}
	public void setEdate1(String edate1) {
		this.edate1 = edate1;
	}
	public String getPACKAGE1_TP() {
		return PACKAGE1_TP;
	}
	public void setPACKAGE1_TP(String pACKAGE1_TP) {
		PACKAGE1_TP = pACKAGE1_TP;
	}
	public String getPACKAGE2_TP() {
		return PACKAGE2_TP;
	}
	public void setPACKAGE2_TP(String pACKAGE2_TP) {
		PACKAGE2_TP = pACKAGE2_TP;
	}
	public String getPACKAGE3_TP() {
		return PACKAGE3_TP;
	}
	public void setPACKAGE3_TP(String pACKAGE3_TP) {
		PACKAGE3_TP = pACKAGE3_TP;
	}
	public String getPACKAGE4_TP() {
		return PACKAGE4_TP;
	}
	public void setPACKAGE4_TP(String pACKAGE4_TP) {
		PACKAGE4_TP = pACKAGE4_TP;
	}
	public String getRE_HOTPOT() {
		return RE_HOTPOT;
	}
	public void setRE_HOTPOT(String rE_HOTPOT) {
		RE_HOTPOT = rE_HOTPOT;
	}
	public String getETC1() {
		return ETC1;
	}
	public void setETC1(String eTC1) {
		ETC1 = eTC1;
	}
	public String getETC2() {
		return ETC2;
	}
	public void setETC2(String eTC2) {
		ETC2 = eTC2;
	}
	public String getETC3() {
		return ETC3;
	}
	public void setETC3(String eTC3) {
		ETC3 = eTC3;
	}
	public String getRECIPE_CNT() {
		return RECIPE_CNT;
	}
	public void setRECIPE_CNT(String rECIPE_CNT) {
		RECIPE_CNT = rECIPE_CNT;
	}
}
