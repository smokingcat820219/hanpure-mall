package kr.co.hanpure.database.recipe;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class RecipeService {
	@Autowired
	private RecipeMapper mapper;
	
	public void insertData(RecipeVO vo) {
		Logger.info();
		
		if(vo.getPRICE1() != null) vo.setPRICE1(vo.getPRICE1().replaceAll(",", ""));
		if(vo.getPRICE2() != null) vo.setPRICE2(vo.getPRICE2().replaceAll(",", ""));
		if(vo.getPRICE3() != null) vo.setPRICE3(vo.getPRICE3().replaceAll(",", ""));
		if(vo.getPRICE4() != null) vo.setPRICE4(vo.getPRICE4().replaceAll(",", ""));
		if(vo.getPRICE5() != null) vo.setPRICE5(vo.getPRICE5().replaceAll(",", ""));
		if(vo.getPRICE6() != null) vo.setPRICE6(vo.getPRICE6().replaceAll(",", ""));
		if(vo.getPRICE7() != null) vo.setPRICE7(vo.getPRICE7().replaceAll(",", ""));
		if(vo.getPRICE8() != null) vo.setPRICE8(vo.getPRICE8().replaceAll(",", ""));
		if(vo.getPRICE9() != null) vo.setPRICE9(vo.getPRICE9().replaceAll(",", ""));
		if(vo.getPRICE10() != null) vo.setPRICE10(vo.getPRICE10().replaceAll(",", ""));
		if(vo.getPRICE11() != null) vo.setPRICE11(vo.getPRICE11().replaceAll(",", ""));
		
		this.mapper.insertData(vo);
	}
	
	public void updateData(RecipeVO vo) {
		Logger.info();
		
		if(vo.getPRICE1() != null) vo.setPRICE1(vo.getPRICE1().replaceAll(",", ""));
		if(vo.getPRICE2() != null) vo.setPRICE2(vo.getPRICE2().replaceAll(",", ""));
		if(vo.getPRICE3() != null) vo.setPRICE3(vo.getPRICE3().replaceAll(",", ""));
		if(vo.getPRICE4() != null) vo.setPRICE4(vo.getPRICE4().replaceAll(",", ""));
		if(vo.getPRICE5() != null) vo.setPRICE5(vo.getPRICE5().replaceAll(",", ""));
		if(vo.getPRICE6() != null) vo.setPRICE6(vo.getPRICE6().replaceAll(",", ""));
		if(vo.getPRICE7() != null) vo.setPRICE7(vo.getPRICE7().replaceAll(",", ""));
		if(vo.getPRICE8() != null) vo.setPRICE8(vo.getPRICE8().replaceAll(",", ""));
		if(vo.getPRICE9() != null) vo.setPRICE9(vo.getPRICE9().replaceAll(",", ""));
		if(vo.getPRICE10() != null) vo.setPRICE10(vo.getPRICE10().replaceAll(",", ""));
		if(vo.getPRICE11() != null) vo.setPRICE11(vo.getPRICE11().replaceAll(",", ""));
		
		this.mapper.updateData(vo);
	}
	
	public RecipeVO selectData(RecipeVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public RecipeVO selectDataOne(RecipeVO vo) {
		Logger.info();
		return this.mapper.selectDataOne(vo);
	}
	
	public List<RecipeVO> selectDatas(RecipeVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(RecipeVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<RecipeVO> selectDataList(RecipeVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


