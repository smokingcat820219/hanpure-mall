package kr.co.hanpure.database.promise;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("PromiseVO")
public class PromiseVO extends Pagenation {
	private String PROMISE_SEQ;
	private String HOSPITAL_ID;
	private String PRODUCT_SEQ;
	private String ORDER_QNTT;
	private String STOCK_QNTT;
	private String PERIOD;
	private String TURN;
	private String MAKE_DT;
	private String SEND_DT;
	private String END_DT;
	private String STATUS;
	private String APPROVAL_STATUS;
	private String APPROVAL_DT;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;	
	
	private String ORDER_SEQ;
	private String RECIPE_SEQ;
	
	private String PERIOD_MAKE;
	private String PRODUCT_NM;
	private String RECIPE_NM;
	private String QNTT;
	private String UNIT;
	
	private String BOM_CD;
	private String OPTION_NM;
	private String OPTION_QNTT;
	private String PACKAGE_QNTT;
	private String BOX_QNTT;
	private String AMOUNT;
	private String ECOUNT_WH_CD;
	private String FILEPATH1;
	
	private List<String> LIST_PROMISE_SEQ = new ArrayList<String>();
	private List<String> LIST_BOM_CD = new ArrayList<String>();
	private List<String> LIST_ORDER_QNTT = new ArrayList<String>();
	private List<String> LIST_STATUS = new ArrayList<String>();	
	
	private String sdate1;
	private String edate1;
	
	private String sdate2;
	private String edate2;
		
	private String DOCTOR;
	private String CATEGORY1;
	private String CATEGORY2;
	private String DRUG_NM;
	
	private String PERIOD_ORDER;
	private String MONTHLY_QNTT;
	private String PRODUCT_OPTION;

	public String getPROMISE_SEQ() {
		return PROMISE_SEQ;
	}

	public void setPROMISE_SEQ(String pROMISE_SEQ) {
		PROMISE_SEQ = pROMISE_SEQ;
	}

	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}

	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}

	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}

	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}

	public String getORDER_QNTT() {
		return ORDER_QNTT;
	}

	public void setORDER_QNTT(String oRDER_QNTT) {
		ORDER_QNTT = oRDER_QNTT;
	}

	public String getSTOCK_QNTT() {
		return STOCK_QNTT;
	}

	public void setSTOCK_QNTT(String sTOCK_QNTT) {
		STOCK_QNTT = sTOCK_QNTT;
	}

	public String getPERIOD() {
		return PERIOD;
	}

	public void setPERIOD(String pERIOD) {
		PERIOD = pERIOD;
	}

	public String getTURN() {
		return TURN;
	}

	public void setTURN(String tURN) {
		TURN = tURN;
	}

	public String getMAKE_DT() {
		return MAKE_DT;
	}

	public void setMAKE_DT(String mAKE_DT) {
		MAKE_DT = mAKE_DT;
	}

	public String getSEND_DT() {
		return SEND_DT;
	}

	public void setSEND_DT(String sEND_DT) {
		SEND_DT = sEND_DT;
	}

	public String getEND_DT() {
		return END_DT;
	}

	public void setEND_DT(String eND_DT) {
		END_DT = eND_DT;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getAPPROVAL_STATUS() {
		return APPROVAL_STATUS;
	}

	public void setAPPROVAL_STATUS(String aPPROVAL_STATUS) {
		APPROVAL_STATUS = aPPROVAL_STATUS;
	}

	public String getAPPROVAL_DT() {
		return APPROVAL_DT;
	}

	public void setAPPROVAL_DT(String aPPROVAL_DT) {
		APPROVAL_DT = aPPROVAL_DT;
	}

	public String getCREATED_AT() {
		return CREATED_AT;
	}

	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}

	public String getCREATED_BY() {
		return CREATED_BY;
	}

	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}

	public String getUPDATED_AT() {
		return UPDATED_AT;
	}

	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}

	public String getUPDATED_BY() {
		return UPDATED_BY;
	}

	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}

	public String getPERIOD_MAKE() {
		return PERIOD_MAKE;
	}

	public void setPERIOD_MAKE(String pERIOD_MAKE) {
		PERIOD_MAKE = pERIOD_MAKE;
	}

	public String getPRODUCT_NM() {
		return PRODUCT_NM;
	}

	public void setPRODUCT_NM(String pRODUCT_NM) {
		PRODUCT_NM = pRODUCT_NM;
	}

	public String getRECIPE_NM() {
		return RECIPE_NM;
	}

	public void setRECIPE_NM(String rECIPE_NM) {
		RECIPE_NM = rECIPE_NM;
	}

	public String getQNTT() {
		return QNTT;
	}

	public void setQNTT(String qNTT) {
		QNTT = qNTT;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public String getBOM_CD() {
		return BOM_CD;
	}

	public void setBOM_CD(String bOM_CD) {
		BOM_CD = bOM_CD;
	}

	public String getOPTION_NM() {
		return OPTION_NM;
	}

	public void setOPTION_NM(String oPTION_NM) {
		OPTION_NM = oPTION_NM;
	}

	public String getOPTION_QNTT() {
		return OPTION_QNTT;
	}

	public void setOPTION_QNTT(String oPTION_QNTT) {
		OPTION_QNTT = oPTION_QNTT;
	}

	public String getPACKAGE_QNTT() {
		return PACKAGE_QNTT;
	}

	public void setPACKAGE_QNTT(String pACKAGE_QNTT) {
		PACKAGE_QNTT = pACKAGE_QNTT;
	}

	public String getBOX_QNTT() {
		return BOX_QNTT;
	}

	public void setBOX_QNTT(String bOX_QNTT) {
		BOX_QNTT = bOX_QNTT;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getFILEPATH1() {
		return FILEPATH1;
	}

	public void setFILEPATH1(String fILEPATH1) {
		FILEPATH1 = fILEPATH1;
	}

	public List<String> getLIST_PROMISE_SEQ() {
		return LIST_PROMISE_SEQ;
	}

	public void setLIST_PROMISE_SEQ(List<String> lIST_PROMISE_SEQ) {
		LIST_PROMISE_SEQ = lIST_PROMISE_SEQ;
	}

	public List<String> getLIST_BOM_CD() {
		return LIST_BOM_CD;
	}

	public void setLIST_BOM_CD(List<String> lIST_BOM_CD) {
		LIST_BOM_CD = lIST_BOM_CD;
	}

	public List<String> getLIST_ORDER_QNTT() {
		return LIST_ORDER_QNTT;
	}

	public void setLIST_ORDER_QNTT(List<String> lIST_ORDER_QNTT) {
		LIST_ORDER_QNTT = lIST_ORDER_QNTT;
	}

	public String getDOCTOR() {
		return DOCTOR;
	}

	public void setDOCTOR(String dOCTOR) {
		DOCTOR = dOCTOR;
	}

	public List<String> getLIST_STATUS() {
		return LIST_STATUS;
	}

	public void setLIST_STATUS(List<String> lIST_STATUS) {
		LIST_STATUS = lIST_STATUS;
	}

	public String getSdate1() {
		return sdate1;
	}

	public void setSdate1(String sdate1) {
		this.sdate1 = sdate1;
	}

	public String getEdate1() {
		return edate1;
	}

	public void setEdate1(String edate1) {
		this.edate1 = edate1;
	}

	public String getSdate2() {
		return sdate2;
	}

	public void setSdate2(String sdate2) {
		this.sdate2 = sdate2;
	}

	public String getEdate2() {
		return edate2;
	}

	public void setEdate2(String edate2) {
		this.edate2 = edate2;
	}

	public String getCATEGORY1() {
		return CATEGORY1;
	}

	public void setCATEGORY1(String cATEGORY1) {
		CATEGORY1 = cATEGORY1;
	}

	public String getCATEGORY2() {
		return CATEGORY2;
	}

	public void setCATEGORY2(String cATEGORY2) {
		CATEGORY2 = cATEGORY2;
	}

	public String getDRUG_NM() {
		return DRUG_NM;
	}

	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}

	public String getPERIOD_ORDER() {
		return PERIOD_ORDER;
	}

	public void setPERIOD_ORDER(String pERIOD_ORDER) {
		PERIOD_ORDER = pERIOD_ORDER;
	}

	public String getMONTHLY_QNTT() {
		return MONTHLY_QNTT;
	}

	public void setMONTHLY_QNTT(String mONTHLY_QNTT) {
		MONTHLY_QNTT = mONTHLY_QNTT;
	}

	public String getORDER_SEQ() {
		return ORDER_SEQ;
	}

	public void setORDER_SEQ(String oRDER_SEQ) {
		ORDER_SEQ = oRDER_SEQ;
	}

	public String getRECIPE_SEQ() {
		return RECIPE_SEQ;
	}

	public void setRECIPE_SEQ(String rECIPE_SEQ) {
		RECIPE_SEQ = rECIPE_SEQ;
	}

	public String getPRODUCT_OPTION() {
		return PRODUCT_OPTION;
	}

	public void setPRODUCT_OPTION(String pRODUCT_OPTION) {
		PRODUCT_OPTION = pRODUCT_OPTION;
	}

	public String getECOUNT_WH_CD() {
		return ECOUNT_WH_CD;
	}

	public void setECOUNT_WH_CD(String eCOUNT_WH_CD) {
		ECOUNT_WH_CD = eCOUNT_WH_CD;
	}
	
}
