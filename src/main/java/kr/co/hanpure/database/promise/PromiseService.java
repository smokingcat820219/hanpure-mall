package kr.co.hanpure.database.promise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class PromiseService {
	@Autowired
	private PromiseMapper mapper;
	
	public void insertData(PromiseVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(PromiseVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(PromiseVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public PromiseVO selectData(PromiseVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public Integer selectDataCount(PromiseVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<PromiseVO> selectDataList(PromiseVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


