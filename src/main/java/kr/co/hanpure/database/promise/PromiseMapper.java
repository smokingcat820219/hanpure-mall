package kr.co.hanpure.database.promise;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PromiseMapper {	
	void insertData(PromiseVO vo);
	void updateData(PromiseVO vo);
	void deleteData(PromiseVO vo);
	PromiseVO selectData(PromiseVO vo);
	Integer selectDataCount(PromiseVO vo);
	List<PromiseVO> selectDataList(PromiseVO vo);
}
