package kr.co.hanpure.database.insurance;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface InsuranceMapper {	
	Integer selectDataCount(InsuranceVO vo);
	List<InsuranceVO> selectDataList(InsuranceVO vo);
	
	List<InsuranceVO> selectDatas(InsuranceVO vo);
}

