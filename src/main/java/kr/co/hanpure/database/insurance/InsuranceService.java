package kr.co.hanpure.database.insurance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class InsuranceService {
	@Autowired
	private InsuranceMapper mapper;	
	public Integer selectDataCount(InsuranceVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<InsuranceVO> selectDataList(InsuranceVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public List<InsuranceVO> selectDatas(InsuranceVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
}


