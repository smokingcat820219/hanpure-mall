package kr.co.hanpure.database.insurance;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("InsuranceVO")
public class InsuranceVO extends Pagenation {
	private String INSURANCE_SEQ;
	private String CODE;
	private String NAME;
	private String DRUG_CD;
	private String DRUG_NM;
	private String DRUG_QNTT;
	private String DIVISION;
	private String DISEASE_NM;
	private String CREATED_AT;
	private String CREATED_BY;
	
	private String DRUG_CNT;

	public String getINSURANCE_SEQ() {
		return INSURANCE_SEQ;
	}

	public void setINSURANCE_SEQ(String iNSURANCE_SEQ) {
		INSURANCE_SEQ = iNSURANCE_SEQ;
	}

	public String getCODE() {
		return CODE;
	}

	public void setCODE(String cODE) {
		CODE = cODE;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getDRUG_CD() {
		return DRUG_CD;
	}

	public void setDRUG_CD(String dRUG_CD) {
		DRUG_CD = dRUG_CD;
	}

	public String getDRUG_NM() {
		return DRUG_NM;
	}

	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}

	public String getDRUG_QNTT() {
		return DRUG_QNTT;
	}

	public void setDRUG_QNTT(String dRUG_QNTT) {
		DRUG_QNTT = dRUG_QNTT;
	}

	public String getDIVISION() {
		return DIVISION;
	}

	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}

	public String getDISEASE_NM() {
		return DISEASE_NM;
	}

	public void setDISEASE_NM(String dISEASE_NM) {
		DISEASE_NM = dISEASE_NM;
	}

	public String getCREATED_AT() {
		return CREATED_AT;
	}

	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}

	public String getCREATED_BY() {
		return CREATED_BY;
	}

	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}

	public String getDRUG_CNT() {
		return DRUG_CNT;
	}

	public void setDRUG_CNT(String dRUG_CNT) {
		DRUG_CNT = dRUG_CNT;
	}
}
