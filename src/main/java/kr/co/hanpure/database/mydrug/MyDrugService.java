package kr.co.hanpure.database.mydrug;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class MyDrugService {
	@Autowired
	private MyDrugMapper mapper;
	
	public void insertData(MyDrugVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(MyDrugVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public MyDrugVO selectData(MyDrugVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
}


