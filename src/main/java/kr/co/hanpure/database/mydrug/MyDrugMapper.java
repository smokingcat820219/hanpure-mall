package kr.co.hanpure.database.mydrug;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface MyDrugMapper {	
	void insertData(MyDrugVO vo);
	void updateData(MyDrugVO vo);
	
	MyDrugVO selectData(MyDrugVO vo);
}
