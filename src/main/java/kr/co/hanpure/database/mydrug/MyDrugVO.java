package kr.co.hanpure.database.mydrug;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("MyDrugVO")
public class MyDrugVO extends Pagenation {
	private String WH_CD;
	private String DRUG_CD;
	private String RAW_MIN_QNTT;
	private String RAW_DRUG_STATUS;
	private String MIN_QNTT;
	private String DRUG_STATUS;
	private String KEYWORD;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;	
	
	public String getWH_CD() {
		return WH_CD;
	}
	public void setWH_CD(String wH_CD) {
		WH_CD = wH_CD;
	}
	public String getDRUG_CD() {
		return DRUG_CD;
	}
	public void setDRUG_CD(String dRUG_CD) {
		DRUG_CD = dRUG_CD;
	}
	public String getRAW_MIN_QNTT() {
		return RAW_MIN_QNTT;
	}
	public void setRAW_MIN_QNTT(String rAW_MIN_QNTT) {
		RAW_MIN_QNTT = rAW_MIN_QNTT;
	}
	public String getRAW_DRUG_STATUS() {
		return RAW_DRUG_STATUS;
	}
	public void setRAW_DRUG_STATUS(String rAW_DRUG_STATUS) {
		RAW_DRUG_STATUS = rAW_DRUG_STATUS;
	}
	public String getMIN_QNTT() {
		return MIN_QNTT;
	}
	public void setMIN_QNTT(String mIN_QNTT) {
		MIN_QNTT = mIN_QNTT;
	}
	public String getDRUG_STATUS() {
		return DRUG_STATUS;
	}
	public void setDRUG_STATUS(String dRUG_STATUS) {
		DRUG_STATUS = dRUG_STATUS;
	}
	public String getKEYWORD() {
		return KEYWORD;
	}
	public void setKEYWORD(String kEYWORD) {
		KEYWORD = kEYWORD;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
}
