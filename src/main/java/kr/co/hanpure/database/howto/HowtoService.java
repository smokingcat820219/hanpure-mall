package kr.co.hanpure.database.howto;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class HowtoService {
	@Autowired
	private HowtoMapper mapper;
	
	public void insertData(HowtoVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(HowtoVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(HowtoVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public HowtoVO selectData(HowtoVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<HowtoVO> selectDatas(HowtoVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(HowtoVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<HowtoVO> selectDataList(HowtoVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


