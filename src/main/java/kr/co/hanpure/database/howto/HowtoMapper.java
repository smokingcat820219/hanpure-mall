package kr.co.hanpure.database.howto;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface HowtoMapper {	
	void insertData(HowtoVO vo);
	void updateData(HowtoVO vo);
	void deleteData(HowtoVO vo);
	HowtoVO selectData(HowtoVO vo);
	List<HowtoVO> selectDatas(HowtoVO vo);
	Integer selectDataCount(HowtoVO vo);
	List<HowtoVO> selectDataList(HowtoVO vo);
}

