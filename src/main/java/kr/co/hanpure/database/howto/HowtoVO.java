package kr.co.hanpure.database.howto;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("HowtoVO")
public class HowtoVO extends Pagenation {
	private String HOWTO_SEQ;
	private String TYPE;
	private String HOSPITAL_ID;
	private String TITLE;
	private String CONTENT;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	private String FILE_QNTT;
	
	private List<String> LIST_HOWTO_SEQ = new ArrayList<String>();
	private List<String> LIST_FILEPATH = new ArrayList<String>();
	private List<String> LIST_FILENAME = new ArrayList<String>();
	
	public String getHOWTO_SEQ() {
		return HOWTO_SEQ;
	}
	public void setHOWTO_SEQ(String hOWTO_SEQ) {
		HOWTO_SEQ = hOWTO_SEQ;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}	
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getCONTENT() {
		return CONTENT;
	}
	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getFILE_QNTT() {
		return FILE_QNTT;
	}
	public void setFILE_QNTT(String fILE_QNTT) {
		FILE_QNTT = fILE_QNTT;
	}
	public List<String> getLIST_HOWTO_SEQ() {
		return LIST_HOWTO_SEQ;
	}
	public void setLIST_HOWTO_SEQ(List<String> lIST_HOWTO_SEQ) {
		LIST_HOWTO_SEQ = lIST_HOWTO_SEQ;
	}
	public List<String> getLIST_FILEPATH() {
		return LIST_FILEPATH;
	}
	public void setLIST_FILEPATH(List<String> lIST_FILEPATH) {
		LIST_FILEPATH = lIST_FILEPATH;
	}
	public List<String> getLIST_FILENAME() {
		return LIST_FILENAME;
	}
	public void setLIST_FILENAME(List<String> lIST_FILENAME) {
		LIST_FILENAME = lIST_FILENAME;
	}	
}
