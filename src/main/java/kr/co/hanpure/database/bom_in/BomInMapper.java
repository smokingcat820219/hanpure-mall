package kr.co.hanpure.database.bom_in;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface BomInMapper {	
	void insertData(BomInVO vo);
	void deleteData(BomInVO vo);
	void deleteDataAll(String BOM_CD);
	BomInVO selectData(BomInVO vo);
	List<BomInVO> selectDatas(BomInVO vo);
	Integer selectDataCount(BomInVO vo);
	List<BomInVO> selectDataList(BomInVO vo);
}
