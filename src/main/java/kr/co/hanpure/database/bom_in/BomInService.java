package kr.co.hanpure.database.bom_in;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class BomInService {
	@Autowired
	private BomInMapper mapper;
	
	public void insertData(BomInVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteData(BomInVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public void deleteDataAll(String BOM_CD) {
		Logger.info();
		this.mapper.deleteDataAll(BOM_CD);
	}
	
	public BomInVO selectData(BomInVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<BomInVO> selectDatas(BomInVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(BomInVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<BomInVO> selectDataList(BomInVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public List<BomInVO> selectItems(List<BomInVO> list, BomInVO vo) {
		Logger.info();		

		List<BomInVO> list_detail = selectDatas(vo);
		
		for(int i = 0; i < list_detail.size(); i++) {
			BomInVO bomInDB = list_detail.get(i);
			if(bomInDB.getITEM_TP().equals("I")) {
				list.add(bomInDB);
			}
			else {				
				BomInVO bomInVO = new BomInVO();
				bomInVO.setBOM_CD(bomInDB.getITEM_CD());
				list = selectItems(list, bomInVO);
			}
		}
		
		return list;
	}
}


