package kr.co.hanpure.database.bom_in;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("BomInVO")
public class BomInVO extends Pagenation {
	private String BOM_IN_SEQ;
	private String BOM_CD;
	private String BOM_NM;
	private String ITEM_TP;
	private String ITEM_CD;
	private String ITEM_NM;
	private String QNTT;	
		
	////
	private String ECOUNT_CD;
	private String ITEM_TP_NM;
	private String FILEPATH;	
	
	private String SPEC;
	private String WEIGHT;
	private String PRICE_A;
	private String PRICE_B;
	private String MAKE_PRICE;	
	
	private List<String> LIST_ITEM_TP = new ArrayList<String>();
	private List<String> LIST_ITEM_CD = new ArrayList<String>();
	private List<String> LIST_QNTT = new ArrayList<String>();
	
	public String getBOM_IN_SEQ() {
		return BOM_IN_SEQ;
	}
	public void setBOM_IN_SEQ(String bOM_IN_SEQ) {
		BOM_IN_SEQ = bOM_IN_SEQ;
	}
	public String getBOM_CD() {
		return BOM_CD;
	}
	public void setBOM_CD(String bOM_CD) {
		BOM_CD = bOM_CD;
	}
	public String getITEM_TP() {
		return ITEM_TP;
	}
	public void setITEM_TP(String iTEM_TP) {
		ITEM_TP = iTEM_TP;
	}
	public String getITEM_CD() {
		return ITEM_CD;
	}
	public void setITEM_CD(String iTEM_CD) {
		ITEM_CD = iTEM_CD;
	}
	public String getITEM_NM() {
		return ITEM_NM;
	}
	public void setITEM_NM(String iTEM_NM) {
		ITEM_NM = iTEM_NM;
	}
	public String getQNTT() {
		return QNTT;
	}
	public void setQNTT(String qNTT) {
		QNTT = qNTT;
	}
	public String getECOUNT_CD() {
		return ECOUNT_CD;
	}
	public void setECOUNT_CD(String eCOUNT_CD) {
		ECOUNT_CD = eCOUNT_CD;
	}
	public List<String> getLIST_ITEM_TP() {
		return LIST_ITEM_TP;
	}
	public void setLIST_ITEM_TP(List<String> lIST_ITEM_TP) {
		LIST_ITEM_TP = lIST_ITEM_TP;
	}
	public List<String> getLIST_ITEM_CD() {
		return LIST_ITEM_CD;
	}
	public void setLIST_ITEM_CD(List<String> lIST_ITEM_CD) {
		LIST_ITEM_CD = lIST_ITEM_CD;
	}
	public List<String> getLIST_QNTT() {
		return LIST_QNTT;
	}
	public void setLIST_QNTT(List<String> lIST_QNTT) {
		LIST_QNTT = lIST_QNTT;
	}
	public String getITEM_TP_NM() {
		return ITEM_TP_NM;
	}
	public void setITEM_TP_NM(String iTEM_TP_NM) {
		ITEM_TP_NM = iTEM_TP_NM;
	}
	public String getFILEPATH() {
		return FILEPATH;
	}
	public void setFILEPATH(String fILEPATH) {
		FILEPATH = fILEPATH;
	}
	public String getWEIGHT() {
		return WEIGHT;
	}
	public void setWEIGHT(String wEIGHT) {
		WEIGHT = wEIGHT;
	}
	public String getPRICE_A() {
		return PRICE_A;
	}
	public void setPRICE_A(String pRICE_A) {
		PRICE_A = pRICE_A;
	}
	public String getPRICE_B() {
		return PRICE_B;
	}
	public void setPRICE_B(String pRICE_B) {
		PRICE_B = pRICE_B;
	}
	public String getMAKE_PRICE() {
		return MAKE_PRICE;
	}
	public void setMAKE_PRICE(String mAKE_PRICE) {
		MAKE_PRICE = mAKE_PRICE;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getBOM_NM() {
		return BOM_NM;
	}
	public void setBOM_NM(String bOM_NM) {
		BOM_NM = bOM_NM;
	}		
}
