package kr.co.hanpure.database.div;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface DivMapper {	
	void insertData(DivVO vo);
	void updateData(DivVO vo);
	void deleteData(DivVO vo);
	DivVO selectData(DivVO vo);
	List<DivVO> selectDatas(DivVO vo);
	Integer selectDataCount(DivVO vo);
	List<DivVO> selectDataList(DivVO vo);
	List<DivVO> selectDepth1();
	List<DivVO> selectDepth2(DivVO vo);
}
