package kr.co.hanpure.database.div;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("DivVO")
public class DivVO extends Pagenation {
	private String DIV_CD;
	private String DIV1_CD;
	private String DIV1_KR;
	private String DIV1_CN;
	private String DIV2_CD;
	private String DIV2_KR;
	private String DIV2_CN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	public String getDIV_CD() {
		return DIV_CD;
	}
	public void setDIV_CD(String dIV_CD) {
		DIV_CD = dIV_CD;
	}
	public String getDIV1_CD() {
		return DIV1_CD;
	}
	public void setDIV1_CD(String dIV1_CD) {
		DIV1_CD = dIV1_CD;
	}
	public String getDIV1_KR() {
		return DIV1_KR;
	}
	public void setDIV1_KR(String dIV1_KR) {
		DIV1_KR = dIV1_KR;
	}
	public String getDIV1_CN() {
		return DIV1_CN;
	}
	public void setDIV1_CN(String dIV1_CN) {
		DIV1_CN = dIV1_CN;
	}
	public String getDIV2_CD() {
		return DIV2_CD;
	}
	public void setDIV2_CD(String dIV2_CD) {
		DIV2_CD = dIV2_CD;
	}
	public String getDIV2_KR() {
		return DIV2_KR;
	}
	public void setDIV2_KR(String dIV2_KR) {
		DIV2_KR = dIV2_KR;
	}
	public String getDIV2_CN() {
		return DIV2_CN;
	}
	public void setDIV2_CN(String dIV2_CN) {
		DIV2_CN = dIV2_CN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
}
