package kr.co.hanpure.database.div;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class DivService {
	@Autowired
	private DivMapper mapper;
	
	public void insertData(DivVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(DivVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(DivVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public DivVO selectData(DivVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<DivVO> selectDatas(DivVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(DivVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<DivVO> selectDataList(DivVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public List<DivVO> selectDepth1() {
		Logger.info();
		return this.mapper.selectDepth1();
	}
	
	public List<DivVO> selectDepth2(DivVO vo) {
		Logger.info();
		return this.mapper.selectDepth2(vo);
	}
}


