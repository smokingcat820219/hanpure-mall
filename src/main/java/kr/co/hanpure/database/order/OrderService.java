package kr.co.hanpure.database.order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;

@Service
public class OrderService {
	@Autowired
	private OrderMapper mapper;
	
	public void insertData(OrderVO vo) {
		Logger.info();
		
		if(!CommonUtil.isNull(vo.getORDER_AMOUNT())) {
			vo.setORDER_AMOUNT(vo.getORDER_AMOUNT().replaceAll(",", ""));
		}
		
		if(CommonUtil.isNull(vo.getPOP())) {
			vo.setPOP("000");
		}		
		
		this.mapper.insertData(vo);
	}
	
	public void updateData(OrderVO vo) {
		Logger.info();
		
		if(!CommonUtil.isNull(vo.getORDER_AMOUNT())) {
			vo.setORDER_AMOUNT(vo.getORDER_AMOUNT().replaceAll(",", ""));
		}
		
		if(CommonUtil.isNull(vo.getPOP())) {
			vo.setPOP("000");
		}		
		
		this.mapper.updateData(vo);
	}
	
	public void deleteData(OrderVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public OrderVO selectData(OrderVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<OrderVO> selectDatas(OrderVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(OrderVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<OrderVO> selectDataList(OrderVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public List<OrderVO> selectStat(OrderVO vo) {
		Logger.info();
		return this.mapper.selectStat(vo);
	}
}


