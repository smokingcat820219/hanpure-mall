package kr.co.hanpure.database.order;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface OrderMapper {	
	void insertData(OrderVO vo);
	void updateData(OrderVO vo);
	void deleteData(OrderVO vo);
	OrderVO selectData(OrderVO vo);
	List<OrderVO> selectDatas(OrderVO vo);
	Integer selectDataCount(OrderVO vo);
	List<OrderVO> selectDataList(OrderVO vo);
	
	List<OrderVO> selectStat(OrderVO vo);
}

