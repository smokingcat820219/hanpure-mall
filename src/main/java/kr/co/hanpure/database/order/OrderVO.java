package kr.co.hanpure.database.order;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("OrderVO")
public class OrderVO extends Pagenation {
	private String ORDER_SEQ;
	private String ORDER_CD;
	private String HOSPITAL_ID;
	private String DOCTOR;
	private String ORDER_TP;
	private String RECIPE_TP;
	private String ORDER_PATH;
	private String ORDER_NM;
	private String ORDER_AMOUNT;
	private String DELIVERY_PRICE;
	private String ORDER_DT;
	private String PAYMENT_STATUS;
	private String PAYMENT_METHOD;
	private String PAYMENT_DT;
	private String ORDER_STATUS;
	private String DELIVERY_DT;
	private String SEND_TP;
	private String SEND_NM;
	private String SEND_TEL;
	private String SEND_MOBILE;
	private String SEND_ZIPCODE;
	private String SEND_ADDRESS;
	private String SEND_ADDRESS2;
	private String RECV_TP;
	private String RECV_NM;
	private String RECV_TEL;
	private String RECV_MOBILE;
	private String RECV_ZIPCODE;
	private String RECV_ADDRESS;
	private String RECV_ADDRESS2;
	private String LOCAL_CD;
	private String DELIVERY_NM;
	private String DELIVERY_CD;
	private String MSG_MAKE;
	private String MSG_DELIVERY;
	private String SAVE_TEMP;
	private String PRODUCT_SEQ;
	private String ORDER_QNTT;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;	
	
	
	private String POP;
	private String POP_STATUS;
	private String POP_STEP;
	
	private String ORDER_TP_NM;
	private String PATIENT_SEQ;
	private String PATIENT_NM;
	
	private List<String> LIST_RECV_TP = new ArrayList<String>();
	private List<String> LIST_RECV_NM = new ArrayList<String>();
	private List<String> LIST_RECV_TEL = new ArrayList<String>();
	private List<String> LIST_RECV_MOBILE = new ArrayList<String>();
	private List<String> LIST_RECV_ZIPCODE = new ArrayList<String>();
	private List<String> LIST_RECV_ADDRESS = new ArrayList<String>();
	private List<String> LIST_RECV_ADDRESS2 = new ArrayList<String>();
	private List<String> LIST_DELIVERY_DT = new ArrayList<String>();
	private List<String> LIST_DELIVERY_PRICE = new ArrayList<String>();
	
	private List<String> LIST_ORDER_STATUS = new ArrayList<String>();
	private List<String> LIST_PAYMENT_STATUS = new ArrayList<String>();	
	
	private List<String> LIST_MSG_MAKE = new ArrayList<String>();
	private List<String> LIST_MSG_DELIVERY = new ArrayList<String>();
	
	
	private String sdate1;
	private String edate1;
	
	private String sdate2;
	private String edate2;
	
	
	private String PRICE1;
	private String PRICE2;
	private String PRICE3;
	private String PRICE4;
	private String PRICE5;
	private String PRICE6;
	private String PRICE7;
	private String PRICE8;
	private String PRICE9;
	private String PRICE10;
	private String PRICE11;
	private String AMOUNT;
		
	private String PATIENT_SEX;
	private String PATIENT_BIRTH;
	private String PATIENT_TEL;
	private String PATIENT_MOBILE;
	private String PATIENT_ZIPCODE;
	private String PATIENT_ADDRESS;
	private String PATIENT_ADDRESS2;
	private String PATIENT_REMARK;
	
	private String HOSPITAL_NM;	
	private String HOSPITAL_TEL;
	private String HOSPITAL_MOBILE;
	private String HOSPITAL_ZIPCODE;
	private String HOSPITAL_ADDRESS;
	private String HOSPITAL_ADDRESS2;    
	
	private String RECIPE_TP_NM;
	private String ORDER_STATUS_NM;
	private String CHUP;
	private String PACK;	
	
	private String ORDER_PATH_NM;
	
	private String DRUG_LABEL_TP;
	private String DRUG_LABEL_TEXT;
	private String DELIVERY_LABEL_TP;
	private String DELIVERY_LABEL_TEXT;
	
	public String getORDER_SEQ() {
		return ORDER_SEQ;
	}
	public void setORDER_SEQ(String oRDER_SEQ) {
		ORDER_SEQ = oRDER_SEQ;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getORDER_TP() {
		return ORDER_TP;
	}
	public void setORDER_TP(String oRDER_TP) {
		ORDER_TP = oRDER_TP;
	}
	public String getORDER_PATH() {
		return ORDER_PATH;
	}
	public void setORDER_PATH(String oRDER_PATH) {
		ORDER_PATH = oRDER_PATH;
	}
	public String getORDER_NM() {
		return ORDER_NM;
	}
	public void setORDER_NM(String oRDER_NM) {
		ORDER_NM = oRDER_NM;
	}
	public String getORDER_AMOUNT() {
		return ORDER_AMOUNT;
	}
	public void setORDER_AMOUNT(String oRDER_AMOUNT) {
		ORDER_AMOUNT = oRDER_AMOUNT;
	}
	public String getORDER_DT() {
		return ORDER_DT;
	}
	public void setORDER_DT(String oRDER_DT) {
		ORDER_DT = oRDER_DT;
	}
	public String getPAYMENT_STATUS() {
		return PAYMENT_STATUS;
	}
	public void setPAYMENT_STATUS(String pAYMENT_STATUS) {
		PAYMENT_STATUS = pAYMENT_STATUS;
	}	
	public String getPAYMENT_METHOD() {
		return PAYMENT_METHOD;
	}
	public void setPAYMENT_METHOD(String pAYMENT_METHOD) {
		PAYMENT_METHOD = pAYMENT_METHOD;
	}
	public String getPAYMENT_DT() {
		return PAYMENT_DT;
	}
	public void setPAYMENT_DT(String pAYMENT_DT) {
		PAYMENT_DT = pAYMENT_DT;
	}
	public String getORDER_STATUS() {
		return ORDER_STATUS;
	}
	public void setORDER_STATUS(String oRDER_STATUS) {
		ORDER_STATUS = oRDER_STATUS;
	}
	public String getDELIVERY_DT() {
		return DELIVERY_DT;
	}
	public void setDELIVERY_DT(String dELIVERY_DT) {
		DELIVERY_DT = dELIVERY_DT;
	}
	public String getSEND_TP() {
		return SEND_TP;
	}
	public void setSEND_TP(String sEND_TP) {
		SEND_TP = sEND_TP;
	}
	public String getSEND_NM() {
		return SEND_NM;
	}
	public void setSEND_NM(String sEND_NM) {
		SEND_NM = sEND_NM;
	}
	public String getSEND_TEL() {
		return SEND_TEL;
	}
	public void setSEND_TEL(String sEND_TEL) {
		SEND_TEL = sEND_TEL;
	}
	public String getSEND_MOBILE() {
		return SEND_MOBILE;
	}
	public void setSEND_MOBILE(String sEND_MOBILE) {
		SEND_MOBILE = sEND_MOBILE;
	}
	public String getSEND_ZIPCODE() {
		return SEND_ZIPCODE;
	}
	public void setSEND_ZIPCODE(String sEND_ZIPCODE) {
		SEND_ZIPCODE = sEND_ZIPCODE;
	}
	public String getSEND_ADDRESS() {
		return SEND_ADDRESS;
	}
	public void setSEND_ADDRESS(String sEND_ADDRESS) {
		SEND_ADDRESS = sEND_ADDRESS;
	}
	public String getSEND_ADDRESS2() {
		return SEND_ADDRESS2;
	}
	public void setSEND_ADDRESS2(String sEND_ADDRESS2) {
		SEND_ADDRESS2 = sEND_ADDRESS2;
	}
	public String getRECV_TP() {
		return RECV_TP;
	}
	public void setRECV_TP(String rECV_TP) {
		RECV_TP = rECV_TP;
	}
	public String getRECV_NM() {
		return RECV_NM;
	}
	public void setRECV_NM(String rECV_NM) {
		RECV_NM = rECV_NM;
	}
	public String getRECV_TEL() {
		return RECV_TEL;
	}
	public void setRECV_TEL(String rECV_TEL) {
		RECV_TEL = rECV_TEL;
	}
	public String getRECV_MOBILE() {
		return RECV_MOBILE;
	}
	public void setRECV_MOBILE(String rECV_MOBILE) {
		RECV_MOBILE = rECV_MOBILE;
	}
	public String getRECV_ZIPCODE() {
		return RECV_ZIPCODE;
	}
	public void setRECV_ZIPCODE(String rECV_ZIPCODE) {
		RECV_ZIPCODE = rECV_ZIPCODE;
	}
	public String getRECV_ADDRESS() {
		return RECV_ADDRESS;
	}
	public void setRECV_ADDRESS(String rECV_ADDRESS) {
		RECV_ADDRESS = rECV_ADDRESS;
	}
	public String getRECV_ADDRESS2() {
		return RECV_ADDRESS2;
	}
	public void setRECV_ADDRESS2(String rECV_ADDRESS2) {
		RECV_ADDRESS2 = rECV_ADDRESS2;
	}
	public String getLOCAL_CD() {
		return LOCAL_CD;
	}
	public void setLOCAL_CD(String lOCAL_CD) {
		LOCAL_CD = lOCAL_CD;
	}
	public String getDELIVERY_NM() {
		return DELIVERY_NM;
	}
	public void setDELIVERY_NM(String dELIVERY_NM) {
		DELIVERY_NM = dELIVERY_NM;
	}
	public String getDELIVERY_CD() {
		return DELIVERY_CD;
	}
	public void setDELIVERY_CD(String dELIVERY_CD) {
		DELIVERY_CD = dELIVERY_CD;
	}
	public String getSAVE_TEMP() {
		return SAVE_TEMP;
	}
	public void setSAVE_TEMP(String sAVE_TEMP) {
		SAVE_TEMP = sAVE_TEMP;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	
	public String getORDER_TP_NM() {
		return ORDER_TP_NM;
	}
	public void setORDER_TP_NM(String oRDER_TP_NM) {
		ORDER_TP_NM = oRDER_TP_NM;
	}
	public String getDOCTOR() {
		return DOCTOR;
	}
	public void setDOCTOR(String dOCTOR) {
		DOCTOR = dOCTOR;
	}
	public String getPATIENT_SEQ() {
		return PATIENT_SEQ;
	}
	public void setPATIENT_SEQ(String pATIENT_SEQ) {
		PATIENT_SEQ = pATIENT_SEQ;
	}
	public String getPATIENT_NM() {
		return PATIENT_NM;
	}
	public void setPATIENT_NM(String pATIENT_NM) {
		PATIENT_NM = pATIENT_NM;
	}
	public String getMSG_MAKE() {
		return MSG_MAKE;
	}
	public void setMSG_MAKE(String mSG_MAKE) {
		MSG_MAKE = mSG_MAKE;
	}
	public String getMSG_DELIVERY() {
		return MSG_DELIVERY;
	}
	public void setMSG_DELIVERY(String mSG_DELIVERY) {
		MSG_DELIVERY = mSG_DELIVERY;
	}	
	public List<String> getLIST_RECV_TP() {
		return LIST_RECV_TP;
	}
	public void setLIST_RECV_TP(List<String> lIST_RECV_TP) {
		LIST_RECV_TP = lIST_RECV_TP;
	}
	public List<String> getLIST_RECV_NM() {
		return LIST_RECV_NM;
	}
	public void setLIST_RECV_NM(List<String> lIST_RECV_NM) {
		LIST_RECV_NM = lIST_RECV_NM;
	}
	public List<String> getLIST_RECV_TEL() {
		return LIST_RECV_TEL;
	}
	public void setLIST_RECV_TEL(List<String> lIST_RECV_TEL) {
		LIST_RECV_TEL = lIST_RECV_TEL;
	}
	public List<String> getLIST_RECV_MOBILE() {
		return LIST_RECV_MOBILE;
	}
	public void setLIST_RECV_MOBILE(List<String> lIST_RECV_MOBILE) {
		LIST_RECV_MOBILE = lIST_RECV_MOBILE;
	}
	public List<String> getLIST_RECV_ZIPCODE() {
		return LIST_RECV_ZIPCODE;
	}
	public void setLIST_RECV_ZIPCODE(List<String> lIST_RECV_ZIPCODE) {
		LIST_RECV_ZIPCODE = lIST_RECV_ZIPCODE;
	}
	public List<String> getLIST_RECV_ADDRESS() {
		return LIST_RECV_ADDRESS;
	}
	public void setLIST_RECV_ADDRESS(List<String> lIST_RECV_ADDRESS) {
		LIST_RECV_ADDRESS = lIST_RECV_ADDRESS;
	}
	public List<String> getLIST_RECV_ADDRESS2() {
		return LIST_RECV_ADDRESS2;
	}
	public void setLIST_RECV_ADDRESS2(List<String> lIST_RECV_ADDRESS2) {
		LIST_RECV_ADDRESS2 = lIST_RECV_ADDRESS2;
	}
	public List<String> getLIST_DELIVERY_DT() {
		return LIST_DELIVERY_DT;
	}
	public void setLIST_DELIVERY_DT(List<String> lIST_DELIVERY_DT) {
		LIST_DELIVERY_DT = lIST_DELIVERY_DT;
	}
	public String getORDER_CD() {
		return ORDER_CD;
	}
	public void setORDER_CD(String oRDER_CD) {
		ORDER_CD = oRDER_CD;
	}
	public List<String> getLIST_ORDER_STATUS() {
		return LIST_ORDER_STATUS;
	}
	public void setLIST_ORDER_STATUS(List<String> lIST_ORDER_STATUS) {
		LIST_ORDER_STATUS = lIST_ORDER_STATUS;
	}
	public List<String> getLIST_PAYMENT_STATUS() {
		return LIST_PAYMENT_STATUS;
	}
	public void setLIST_PAYMENT_STATUS(List<String> lIST_PAYMENT_STATUS) {
		LIST_PAYMENT_STATUS = lIST_PAYMENT_STATUS;
	}
	public String getSdate1() {
		return sdate1;
	}
	public void setSdate1(String sdate1) {
		this.sdate1 = sdate1;
	}
	public String getEdate1() {
		return edate1;
	}
	public void setEdate1(String edate1) {
		this.edate1 = edate1;
	}
	public String getSdate2() {
		return sdate2;
	}
	public void setSdate2(String sdate2) {
		this.sdate2 = sdate2;
	}
	public String getEdate2() {
		return edate2;
	}
	public void setEdate2(String edate2) {
		this.edate2 = edate2;
	}
	public String getORDER_STATUS_NM() {
		return ORDER_STATUS_NM;
	}
	public void setORDER_STATUS_NM(String oRDER_STATUS_NM) {
		ORDER_STATUS_NM = oRDER_STATUS_NM;
	}
	public String getPRICE1() {
		return PRICE1;
	}
	public void setPRICE1(String pRICE1) {
		PRICE1 = pRICE1;
	}
	public String getPRICE2() {
		return PRICE2;
	}
	public void setPRICE2(String pRICE2) {
		PRICE2 = pRICE2;
	}
	public String getPRICE3() {
		return PRICE3;
	}
	public void setPRICE3(String pRICE3) {
		PRICE3 = pRICE3;
	}
	public String getPRICE4() {
		return PRICE4;
	}
	public void setPRICE4(String pRICE4) {
		PRICE4 = pRICE4;
	}
	public String getPRICE5() {
		return PRICE5;
	}
	public void setPRICE5(String pRICE5) {
		PRICE5 = pRICE5;
	}
	public String getPRICE6() {
		return PRICE6;
	}
	public void setPRICE6(String pRICE6) {
		PRICE6 = pRICE6;
	}
	public String getPRICE7() {
		return PRICE7;
	}
	public void setPRICE7(String pRICE7) {
		PRICE7 = pRICE7;
	}
	public String getPRICE8() {
		return PRICE8;
	}
	public void setPRICE8(String pRICE8) {
		PRICE8 = pRICE8;
	}
	public String getPRICE9() {
		return PRICE9;
	}
	public void setPRICE9(String pRICE9) {
		PRICE9 = pRICE9;
	}
	public String getPRICE10() {
		return PRICE10;
	}
	public void setPRICE10(String pRICE10) {
		PRICE10 = pRICE10;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getPATIENT_SEX() {
		return PATIENT_SEX;
	}
	public void setPATIENT_SEX(String pATIENT_SEX) {
		PATIENT_SEX = pATIENT_SEX;
	}
	public String getPATIENT_BIRTH() {
		return PATIENT_BIRTH;
	}
	public void setPATIENT_BIRTH(String pATIENT_BIRTH) {
		PATIENT_BIRTH = pATIENT_BIRTH;
	}
	public String getPATIENT_TEL() {
		return PATIENT_TEL;
	}
	public void setPATIENT_TEL(String pATIENT_TEL) {
		PATIENT_TEL = pATIENT_TEL;
	}
	public String getPATIENT_MOBILE() {
		return PATIENT_MOBILE;
	}
	public void setPATIENT_MOBILE(String pATIENT_MOBILE) {
		PATIENT_MOBILE = pATIENT_MOBILE;
	}
	public String getPATIENT_ZIPCODE() {
		return PATIENT_ZIPCODE;
	}
	public void setPATIENT_ZIPCODE(String pATIENT_ZIPCODE) {
		PATIENT_ZIPCODE = pATIENT_ZIPCODE;
	}
	public String getPATIENT_ADDRESS() {
		return PATIENT_ADDRESS;
	}
	public void setPATIENT_ADDRESS(String pATIENT_ADDRESS) {
		PATIENT_ADDRESS = pATIENT_ADDRESS;
	}
	public String getPATIENT_ADDRESS2() {
		return PATIENT_ADDRESS2;
	}
	public void setPATIENT_ADDRESS2(String pATIENT_ADDRESS2) {
		PATIENT_ADDRESS2 = pATIENT_ADDRESS2;
	}
	public String getPATIENT_REMARK() {
		return PATIENT_REMARK;
	}
	public void setPATIENT_REMARK(String pATIENT_REMARK) {
		PATIENT_REMARK = pATIENT_REMARK;
	}
	public String getHOSPITAL_NM() {
		return HOSPITAL_NM;
	}
	public void setHOSPITAL_NM(String hOSPITAL_NM) {
		HOSPITAL_NM = hOSPITAL_NM;
	}
	public String getHOSPITAL_TEL() {
		return HOSPITAL_TEL;
	}
	public void setHOSPITAL_TEL(String hOSPITAL_TEL) {
		HOSPITAL_TEL = hOSPITAL_TEL;
	}
	public String getHOSPITAL_MOBILE() {
		return HOSPITAL_MOBILE;
	}
	public void setHOSPITAL_MOBILE(String hOSPITAL_MOBILE) {
		HOSPITAL_MOBILE = hOSPITAL_MOBILE;
	}
	public String getHOSPITAL_ZIPCODE() {
		return HOSPITAL_ZIPCODE;
	}
	public void setHOSPITAL_ZIPCODE(String hOSPITAL_ZIPCODE) {
		HOSPITAL_ZIPCODE = hOSPITAL_ZIPCODE;
	}
	public String getHOSPITAL_ADDRESS() {
		return HOSPITAL_ADDRESS;
	}
	public void setHOSPITAL_ADDRESS(String hOSPITAL_ADDRESS) {
		HOSPITAL_ADDRESS = hOSPITAL_ADDRESS;
	}
	public String getHOSPITAL_ADDRESS2() {
		return HOSPITAL_ADDRESS2;
	}
	public void setHOSPITAL_ADDRESS2(String hOSPITAL_ADDRESS2) {
		HOSPITAL_ADDRESS2 = hOSPITAL_ADDRESS2;
	}
	public String getRECIPE_TP_NM() {
		return RECIPE_TP_NM;
	}
	public void setRECIPE_TP_NM(String rECIPE_TP_NM) {
		RECIPE_TP_NM = rECIPE_TP_NM;
	}
	public String getCHUP() {
		return CHUP;
	}
	public void setCHUP(String cHUP) {
		CHUP = cHUP;
	}
	public String getPACK() {
		return PACK;
	}
	public void setPACK(String pACK) {
		PACK = pACK;
	}
	public String getORDER_PATH_NM() {
		return ORDER_PATH_NM;
	}
	public void setORDER_PATH_NM(String oRDER_PATH_NM) {
		ORDER_PATH_NM = oRDER_PATH_NM;
	}
	public String getRECIPE_TP() {
		return RECIPE_TP;
	}
	public void setRECIPE_TP(String rECIPE_TP) {
		RECIPE_TP = rECIPE_TP;
	}
	public String getPOP() {
		return POP;
	}
	public void setPOP(String pOP) {
		POP = pOP;
	}
	public String getPOP_STATUS() {
		return POP_STATUS;
	}
	public void setPOP_STATUS(String pOP_STATUS) {
		POP_STATUS = pOP_STATUS;
	}
	public String getPOP_STEP() {
		return POP_STEP;
	}
	public void setPOP_STEP(String pOP_STEP) {
		POP_STEP = pOP_STEP;
	}
	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}
	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}
	public String getORDER_QNTT() {
		return ORDER_QNTT;
	}
	public void setORDER_QNTT(String oRDER_QNTT) {
		ORDER_QNTT = oRDER_QNTT;
	}
	public String getDRUG_LABEL_TP() {
		return DRUG_LABEL_TP;
	}
	public void setDRUG_LABEL_TP(String dRUG_LABEL_TP) {
		DRUG_LABEL_TP = dRUG_LABEL_TP;
	}
	public String getDRUG_LABEL_TEXT() {
		return DRUG_LABEL_TEXT;
	}
	public void setDRUG_LABEL_TEXT(String dRUG_LABEL_TEXT) {
		DRUG_LABEL_TEXT = dRUG_LABEL_TEXT;
	}
	public String getDELIVERY_LABEL_TP() {
		return DELIVERY_LABEL_TP;
	}
	public void setDELIVERY_LABEL_TP(String dELIVERY_LABEL_TP) {
		DELIVERY_LABEL_TP = dELIVERY_LABEL_TP;
	}
	public String getDELIVERY_LABEL_TEXT() {
		return DELIVERY_LABEL_TEXT;
	}
	public void setDELIVERY_LABEL_TEXT(String dELIVERY_LABEL_TEXT) {
		DELIVERY_LABEL_TEXT = dELIVERY_LABEL_TEXT;
	}
	public String getPRICE11() {
		return PRICE11;
	}
	public void setPRICE11(String pRICE11) {
		PRICE11 = pRICE11;
	}	
	public List<String> getLIST_MSG_MAKE() {
		return LIST_MSG_MAKE;
	}
	public void setLIST_MSG_MAKE(List<String> lIST_MSG_MAKE) {
		LIST_MSG_MAKE = lIST_MSG_MAKE;
	}
	public List<String> getLIST_MSG_DELIVERY() {
		return LIST_MSG_DELIVERY;
	}
	public void setLIST_MSG_DELIVERY(List<String> lIST_MSG_DELIVERY) {
		LIST_MSG_DELIVERY = lIST_MSG_DELIVERY;
	}
	public String getDELIVERY_PRICE() {
		return DELIVERY_PRICE;
	}
	public void setDELIVERY_PRICE(String dELIVERY_PRICE) {
		DELIVERY_PRICE = dELIVERY_PRICE;
	}
	public List<String> getLIST_DELIVERY_PRICE() {
		return LIST_DELIVERY_PRICE;
	}
	public void setLIST_DELIVERY_PRICE(List<String> lIST_DELIVERY_PRICE) {
		LIST_DELIVERY_PRICE = lIST_DELIVERY_PRICE;
	}	
}
