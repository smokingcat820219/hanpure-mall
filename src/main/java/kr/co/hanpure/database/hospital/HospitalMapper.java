package kr.co.hanpure.database.hospital;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface HospitalMapper {	
	void insertData(HospitalVO vo);
	void updateData(HospitalVO vo);
	HospitalVO selectData(HospitalVO vo);
	Integer selectDataCount(HospitalVO vo);
	List<HospitalVO> selectDataList(HospitalVO vo);	
}
