package kr.co.hanpure.database.hospital;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;
import kr.co.hanpure.common.CommonCode;

@Service
public class HospitalService {
	@Autowired
	private HospitalMapper mapper;
	
	public HospitalVO GetLoginSession(HttpSession session) {
		Logger.info();
		
		HospitalVO SESSION = (HospitalVO)session.getAttribute(CommonCode.USER_SESSION_KEY);
		if(SESSION == null) {
			return null;
		}
		else {			
			return SESSION;
		} 
	}
	
	public void insertData(HospitalVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(HospitalVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public HospitalVO selectData(HospitalVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public Integer selectDataCount(HospitalVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<HospitalVO> selectDataList(HospitalVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


