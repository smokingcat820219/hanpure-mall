package kr.co.hanpure.database.conflict;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ConflictService {
	@Autowired
	private ConflictMapper mapper;
	
	public void insertData(ConflictVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(ConflictVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(ConflictVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public ConflictVO selectData(ConflictVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<ConflictVO> selectDatas(ConflictVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(ConflictVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<ConflictVO> selectDataList(ConflictVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


