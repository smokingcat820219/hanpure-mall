package kr.co.hanpure.database.conflict;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ConflictMapper {	
	void insertData(ConflictVO vo);
	void updateData(ConflictVO vo);
	void deleteData(ConflictVO vo);
	ConflictVO selectData(ConflictVO vo);
	List<ConflictVO> selectDatas(ConflictVO vo);
	Integer selectDataCount(ConflictVO vo);
	List<ConflictVO> selectDataList(ConflictVO vo);
}
