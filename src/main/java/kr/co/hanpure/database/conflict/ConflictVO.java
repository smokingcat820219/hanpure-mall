package kr.co.hanpure.database.conflict;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ConflictVO")
public class ConflictVO extends Pagenation {
	private String CONFLICT_CD;
	private String GROUP_CD;
	private String HERBS_CD;
	private String HERBS_NM;
	private String CAUTION_MSG_KR;
	private String CAUTION_MSG_CN;
	private String CAUTION_CONTENT_KR;
	private String CAUTION_CONTENT_CN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	public String getCONFLICT_CD() {
		return CONFLICT_CD;
	}
	public void setCONFLICT_CD(String cONFLICT_CD) {
		CONFLICT_CD = cONFLICT_CD;
	}
	public String getGROUP_CD() {
		return GROUP_CD;
	}
	public void setGROUP_CD(String gROUP_CD) {
		GROUP_CD = gROUP_CD;
	}
	public String getHERBS_CD() {
		return HERBS_CD;
	}
	public void setHERBS_CD(String hERBS_CD) {
		HERBS_CD = hERBS_CD;
	}
	public String getCAUTION_MSG_KR() {
		return CAUTION_MSG_KR;
	}
	public void setCAUTION_MSG_KR(String cAUTION_MSG_KR) {
		CAUTION_MSG_KR = cAUTION_MSG_KR;
	}
	public String getCAUTION_MSG_CN() {
		return CAUTION_MSG_CN;
	}
	public void setCAUTION_MSG_CN(String cAUTION_MSG_CN) {
		CAUTION_MSG_CN = cAUTION_MSG_CN;
	}
	public String getCAUTION_CONTENT_KR() {
		return CAUTION_CONTENT_KR;
	}
	public void setCAUTION_CONTENT_KR(String cAUTION_CONTENT_KR) {
		CAUTION_CONTENT_KR = cAUTION_CONTENT_KR;
	}
	public String getCAUTION_CONTENT_CN() {
		return CAUTION_CONTENT_CN;
	}
	public void setCAUTION_CONTENT_CN(String cAUTION_CONTENT_CN) {
		CAUTION_CONTENT_CN = cAUTION_CONTENT_CN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getHERBS_NM() {
		return HERBS_NM;
	}
	public void setHERBS_NM(String hERBS_NM) {
		HERBS_NM = hERBS_NM;
	}	
}
