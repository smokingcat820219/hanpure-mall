package kr.co.hanpure.database.stock_drug;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.mydrug.MyDrugMapper;
import kr.co.hanpure.database.mydrug.MyDrugVO;

@Service
public class StockDrugService {
	@Autowired
	private StockDrugMapper mapper;
	
	@Autowired
	private MyDrugMapper myDrugMapper;
	
	public void insertData(StockDrugVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(StockDrugVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(StockDrugVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public StockDrugVO selectData(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<StockDrugVO> selectDatas(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public StockDrugVO selectDataKorea(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectDataKorea(vo);
	}
	
	public StockDrugVO selectDataElse(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectDataElse(vo);
	}
	
	public StockDrugVO selectDataStock(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectDataStock(vo);
	}
	
	public Integer selectDataCount(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<StockDrugVO> selectDataList(StockDrugVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public void updateDrugStatus() {
		Logger.info();
		
		try {
    		StockDrugVO stockDrugVO = new StockDrugVO();
    		stockDrugVO.setDRUG_STATUS("001");
    		List<StockDrugVO> list = mapper.selectDatas(stockDrugVO);
    		
    		int nSize = list.size();
    		for(int i = 0; i < nSize; i++) {
    			StockDrugVO stockDrugDB = list.get(i);
    			
    			//적정수량보다 재고가 적을 경우
    			if(CommonUtil.parseFloat(stockDrugDB.getSTOCK_QNTT()) < CommonUtil.parseFloat(stockDrugDB.getMIN_QNTT())) {
    				//상태 변경 005
    				MyDrugVO myDrugVO = new MyDrugVO();
    				myDrugVO.setWH_CD(stockDrugDB.getWH_CD());
    				myDrugVO.setDRUG_CD(stockDrugDB.getDRUG_CD());
    				myDrugVO.setDRUG_STATUS("005");
    				myDrugVO.setUPDATED_BY("system");    				
    				myDrugMapper.updateData(myDrugVO);
    			}
    		}
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
		
	}
}


