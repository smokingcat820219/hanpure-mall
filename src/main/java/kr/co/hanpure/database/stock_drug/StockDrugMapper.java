package kr.co.hanpure.database.stock_drug;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface StockDrugMapper {	
	void insertData(StockDrugVO vo);
	void updateData(StockDrugVO vo);
	void deleteData(StockDrugVO vo);
	StockDrugVO selectData(StockDrugVO vo);
	StockDrugVO selectDataKorea(StockDrugVO vo);
	StockDrugVO selectDataElse(StockDrugVO vo);
	List<StockDrugVO> selectDatas(StockDrugVO vo);
	StockDrugVO selectDataStock(StockDrugVO vo);
	Integer selectDataCount(StockDrugVO vo);
	List<StockDrugVO> selectDataList(StockDrugVO vo);
}
