package kr.co.hanpure.database.stock_drug;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("StockDrugVO")
public class StockDrugVO extends Pagenation {
	private String WH_CD;
	private String DRUG_CD;
	private String STOCK_QNTT;
	private String IN_DT;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;	
	
	private String HERBS_CD;
	private String DRUG_NM;
	private String DRUG_TP;
	private String PRICE_A;
	private String PRICE_B;
	private String PRICE_C;
	private String PRICE_D;
	private String PRICE_E;
	private String ORIGIN;
	private String MAKER; 
	private String ABSORPTION_RATE;
	private String HERBS_NM;
	
	
	private String CHUP1;
	private String TURN;
	private String WORKPLACE_TP;
	private String CUST_SEQ;
	private String CUST_NM;
	private String MIN_QNTT;
	private String DRUG_STATUS;	
	private String SPEC;
	private String DRUG_STATUS_NM;
	private String USE_YN;
	private String sdate1;
	private String edate1;
	private String INSURANCE_YN;
	private String KOREA_YN;
	
	private List<String> LIST_INSURANCE_YN = new ArrayList<String>();
	
	public String getHERBS_NM() {
		return HERBS_NM;
	}
	public void setHERBS_NM(String hERBS_NM) {
		HERBS_NM = hERBS_NM;
	}
	public String getDRUG_CD() {
		return DRUG_CD;
	}
	public void setDRUG_CD(String dRUG_CD) {
		DRUG_CD = dRUG_CD;
	}
	public String getSTOCK_QNTT() {
		return STOCK_QNTT;
	}
	public void setSTOCK_QNTT(String sTOCK_QNTT) {
		STOCK_QNTT = sTOCK_QNTT;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getHERBS_CD() {
		return HERBS_CD;
	}
	public void setHERBS_CD(String hERBS_CD) {
		HERBS_CD = hERBS_CD;
	}
	public String getDRUG_NM() {
		return DRUG_NM;
	}
	public void setDRUG_NM(String dRUG_NM) {
		DRUG_NM = dRUG_NM;
	}
	public String getDRUG_TP() {
		return DRUG_TP;
	}
	public void setDRUG_TP(String dRUG_TP) {
		DRUG_TP = dRUG_TP;
	}
	public String getPRICE_A() {
		return PRICE_A;
	}
	public void setPRICE_A(String pRICE_A) {
		PRICE_A = pRICE_A;
	}
	public String getPRICE_B() {
		return PRICE_B;
	}
	public void setPRICE_B(String pRICE_B) {
		PRICE_B = pRICE_B;
	}
	public String getPRICE_C() {
		return PRICE_C;
	}
	public void setPRICE_C(String pRICE_C) {
		PRICE_C = pRICE_C;
	}
	public String getPRICE_D() {
		return PRICE_D;
	}
	public void setPRICE_D(String pRICE_D) {
		PRICE_D = pRICE_D;
	}
	public String getPRICE_E() {
		return PRICE_E;
	}
	public void setPRICE_E(String pRICE_E) {
		PRICE_E = pRICE_E;
	}
	public String getORIGIN() {
		return ORIGIN;
	}
	public void setORIGIN(String oRIGIN) {
		ORIGIN = oRIGIN;
	}
	public String getMAKER() {
		return MAKER;
	}
	public void setMAKER(String mAKER) {
		MAKER = mAKER;
	}
	public String getABSORPTION_RATE() {
		return ABSORPTION_RATE;
	}
	public void setABSORPTION_RATE(String aBSORPTION_RATE) {
		ABSORPTION_RATE = aBSORPTION_RATE;
	}
	public String getCHUP1() {
		return CHUP1;
	}
	public void setCHUP1(String cHUP1) {
		CHUP1 = cHUP1;
	}
	public String getTURN() {
		return TURN;
	}
	public void setTURN(String tURN) {
		TURN = tURN;
	}
	public String getWH_CD() {
		return WH_CD;
	}
	public void setWH_CD(String wH_CD) {
		WH_CD = wH_CD;
	}	
	public String getIN_DT() {
		return IN_DT;
	}
	public void setIN_DT(String iN_DT) {
		IN_DT = iN_DT;
	}
	public String getWORKPLACE_TP() {
		return WORKPLACE_TP;
	}
	public void setWORKPLACE_TP(String wORKPLACE_TP) {
		WORKPLACE_TP = wORKPLACE_TP;
	}
	public String getCUST_SEQ() {
		return CUST_SEQ;
	}
	public void setCUST_SEQ(String cUST_SEQ) {
		CUST_SEQ = cUST_SEQ;
	}
	public String getCUST_NM() {
		return CUST_NM;
	}
	public void setCUST_NM(String cUST_NM) {
		CUST_NM = cUST_NM;
	}
	public String getMIN_QNTT() {
		return MIN_QNTT;
	}
	public void setMIN_QNTT(String mIN_QNTT) {
		MIN_QNTT = mIN_QNTT;
	}
	public String getDRUG_STATUS() {
		return DRUG_STATUS;
	}
	public void setDRUG_STATUS(String dRUG_STATUS) {
		DRUG_STATUS = dRUG_STATUS;
	}
	public String getSPEC() {
		return SPEC;
	}
	public void setSPEC(String sPEC) {
		SPEC = sPEC;
	}
	public String getDRUG_STATUS_NM() {
		return DRUG_STATUS_NM;
	}
	public void setDRUG_STATUS_NM(String dRUG_STATUS_NM) {
		DRUG_STATUS_NM = dRUG_STATUS_NM;
	}
	public String getSdate1() {
		return sdate1;
	}
	public void setSdate1(String sdate1) {
		this.sdate1 = sdate1;
	}
	public String getEdate1() {
		return edate1;
	}
	public void setEdate1(String edate1) {
		this.edate1 = edate1;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getINSURANCE_YN() {
		return INSURANCE_YN;
	}
	public void setINSURANCE_YN(String iNSURANCE_YN) {
		INSURANCE_YN = iNSURANCE_YN;
	}
	public String getKOREA_YN() {
		return KOREA_YN;
	}
	public void setKOREA_YN(String kOREA_YN) {
		KOREA_YN = kOREA_YN;
	}
	public List<String> getLIST_INSURANCE_YN() {
		return LIST_INSURANCE_YN;
	}
	public void setLIST_INSURANCE_YN(List<String> lIST_INSURANCE_YN) {
		LIST_INSURANCE_YN = lIST_INSURANCE_YN;
	}		
}
