package kr.co.hanpure.database.log;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface LogMapper {	
	void insertData(LogVO vo);
	Integer selectDataCount(LogVO vo);
	List<LogVO> selectDataList(LogVO vo);
}
