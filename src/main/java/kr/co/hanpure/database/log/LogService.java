package kr.co.hanpure.database.log;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.user.UserVO;

@Service
public class LogService {
	@Autowired
	private LogMapper mapper;
	
	public void insertLog(String division, HttpServletRequest request, UserVO SESSION) {
		Logger.info();
		
		String ip = CommonUtil.getUserIP(request);
		
		LogVO logVO = new LogVO();
		logVO.setDIVISION(division);
		logVO.setUSER_CD(SESSION.getUSER_CD());
		logVO.setUSER_NM(SESSION.getUSER_NM());
		logVO.setIP(ip);
		logVO.setCREATED_BY(SESSION.getUSER_CD());
		
		this.mapper.insertData(logVO);
	}
	
	public Integer selectDataCount(LogVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<LogVO> selectDataList(LogVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


