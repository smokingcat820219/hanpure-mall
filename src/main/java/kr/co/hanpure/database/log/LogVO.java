package kr.co.hanpure.database.log;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("LogVO")
public class LogVO extends Pagenation {
	private String LOG_SEQ;
	private String DIVISION;
	private String USER_CD;
	private String USER_NM;
	private String IP;
	private String CREATED_AT;
	private String CREATED_BY;
	
	public String getLOG_SEQ() {
		return LOG_SEQ;
	}
	public void setLOG_SEQ(String lOG_SEQ) {
		LOG_SEQ = lOG_SEQ;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getUSER_CD() {
		return USER_CD;
	}
	public void setUSER_CD(String uSER_CD) {
		USER_CD = uSER_CD;
	}
	public String getUSER_NM() {
		return USER_NM;
	}
	public void setUSER_NM(String uSER_NM) {
		USER_NM = uSER_NM;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
}
