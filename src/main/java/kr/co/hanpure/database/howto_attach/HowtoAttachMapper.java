package kr.co.hanpure.database.howto_attach;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface HowtoAttachMapper {	
	void insertData(HowtoAttachVO vo);
	void deleteDataAll(HowtoAttachVO vo);
	HowtoAttachVO selectData(HowtoAttachVO vo);
	List<HowtoAttachVO> selectDatas(HowtoAttachVO vo);
}

