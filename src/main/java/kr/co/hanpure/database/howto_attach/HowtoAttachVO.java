package kr.co.hanpure.database.howto_attach;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("HowtoAttachVO")
public class HowtoAttachVO extends Pagenation {
	private String HOWTO_ATTACH_SEQ;
	private String HOWTO_SEQ;
	private String FILEPATH;
	private String FILENAME;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	public String getHOWTO_ATTACH_SEQ() {
		return HOWTO_ATTACH_SEQ;
	}
	public void setHOWTO_ATTACH_SEQ(String hOWTO_ATTACH_SEQ) {
		HOWTO_ATTACH_SEQ = hOWTO_ATTACH_SEQ;
	}
	public String getHOWTO_SEQ() {
		return HOWTO_SEQ;
	}
	public void setHOWTO_SEQ(String hOWTO_SEQ) {
		HOWTO_SEQ = hOWTO_SEQ;
	}
	public String getFILEPATH() {
		return FILEPATH;
	}
	public void setFILEPATH(String fILEPATH) {
		FILEPATH = fILEPATH;
	}
	public String getFILENAME() {
		return FILENAME;
	}
	public void setFILENAME(String fILENAME) {
		FILENAME = fILENAME;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	
	
}
