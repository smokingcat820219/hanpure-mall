package kr.co.hanpure.database.howto_attach;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class HowtoAttachService {
	@Autowired
	private HowtoAttachMapper mapper;
	
	public void insertData(HowtoAttachVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteDataAll(HowtoAttachVO vo) {
		Logger.info();
		this.mapper.deleteDataAll(vo);
	}
	
	public HowtoAttachVO selectData(HowtoAttachVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<HowtoAttachVO> selectDatas(HowtoAttachVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
}


