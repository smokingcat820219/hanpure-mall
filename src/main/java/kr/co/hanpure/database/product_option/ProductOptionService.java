package kr.co.hanpure.database.product_option;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ProductOptionService {
	@Autowired
	private ProductOptionMapper mapper;
	
	public void insertData(ProductOptionVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteDataAll(ProductOptionVO vo) {
		Logger.info();
		this.mapper.deleteDataAll(vo);
	}
	
	public List<ProductOptionVO> selectDatas(ProductOptionVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
}


