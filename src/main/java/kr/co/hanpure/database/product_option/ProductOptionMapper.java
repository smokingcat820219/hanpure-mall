package kr.co.hanpure.database.product_option;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ProductOptionMapper {	
	void insertData(ProductOptionVO vo);
	void deleteDataAll(ProductOptionVO vo);
	List<ProductOptionVO> selectDatas(ProductOptionVO vo);
}

