package kr.co.hanpure.database.product_option;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ProductOptionVO")
public class ProductOptionVO extends Pagenation {
	private String PRODUCT_SEQ;
	private String SORT;
	private String OPTION_NM;
	private String OPTION_QNTT;
	private String PACKAGE_QNTT;
	private String BOX_QNTT;
	private String AMOUNT;
	private String MARKING_TP;
	private String MARKING_TEXT;
	private String PACKAGE1;
	private String PACKAGE2;
	private String PACKAGE3;
	private String VACCUM;
	private String FILEPATH1;
	private String FILEPATH2;
	private String FILEPATH3;
	private String FILEPATH4;
	private String FILEPATH5;
	private String FILEPATH6;
	private String FILEPATH7;
	private String FILEPATH8;
	private String FILEPATH9;
	private String FILEPATH10;	
	
	private List<String> LIST_OPTION_NM = new ArrayList<>();
	private List<String> LIST_OPTION_QNTT = new ArrayList<>();
	private List<String> LIST_PACKAGE_QNTT = new ArrayList<>();
	private List<String> LIST_BOX_QNTT = new ArrayList<>();
	private List<String> LIST_AMOUNT = new ArrayList<>();
	private List<String> LIST_MARKING_TP = new ArrayList<>();
	private List<String> LIST_MARKING_TEXT = new ArrayList<>();
	private List<String> LIST_PACKAGE1 = new ArrayList<>();
	private List<String> LIST_PACKAGE2 = new ArrayList<>();
	private List<String> LIST_PACKAGE3 = new ArrayList<>();
	private List<String> LIST_VACCUM = new ArrayList<>();
	private List<String> LIST_FILEPATH = new ArrayList<>();	
	
	private String PACKAGE1_NM;	
	private String PACKAGE2_NM;
	private String PACKAGE3_NM;	
	
	private String PACKAGE1_FILEPATH;
	private String PACKAGE2_FILEPATH;
	private String PACKAGE3_FILEPATH;	
	
	private String OPTION_TP;
	
	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}
	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}
	public String getOPTION_NM() {
		return OPTION_NM;
	}
	public void setOPTION_NM(String oPTION_NM) {
		OPTION_NM = oPTION_NM;
	}
	public String getOPTION_QNTT() {
		return OPTION_QNTT;
	}
	public void setOPTION_QNTT(String oPTION_QNTT) {
		OPTION_QNTT = oPTION_QNTT;
	}
	public String getPACKAGE_QNTT() {
		return PACKAGE_QNTT;
	}
	public void setPACKAGE_QNTT(String pACKAGE_QNTT) {
		PACKAGE_QNTT = pACKAGE_QNTT;
	}
	public String getBOX_QNTT() {
		return BOX_QNTT;
	}
	public void setBOX_QNTT(String bOX_QNTT) {
		BOX_QNTT = bOX_QNTT;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getPACKAGE1() {
		return PACKAGE1;
	}
	public void setPACKAGE1(String pACKAGE1) {
		PACKAGE1 = pACKAGE1;
	}
	public String getPACKAGE2() {
		return PACKAGE2;
	}
	public void setPACKAGE2(String pACKAGE2) {
		PACKAGE2 = pACKAGE2;
	}
	public String getPACKAGE3() {
		return PACKAGE3;
	}
	public void setPACKAGE3(String pACKAGE3) {
		PACKAGE3 = pACKAGE3;
	}
	public String getVACCUM() {
		return VACCUM;
	}
	public void setVACCUM(String vACCUM) {
		VACCUM = vACCUM;
	}
	public String getFILEPATH1() {
		return FILEPATH1;
	}
	public void setFILEPATH1(String fILEPATH1) {
		FILEPATH1 = fILEPATH1;
	}
	public String getFILEPATH2() {
		return FILEPATH2;
	}
	public void setFILEPATH2(String fILEPATH2) {
		FILEPATH2 = fILEPATH2;
	}
	public String getFILEPATH3() {
		return FILEPATH3;
	}
	public void setFILEPATH3(String fILEPATH3) {
		FILEPATH3 = fILEPATH3;
	}
	public String getFILEPATH4() {
		return FILEPATH4;
	}
	public void setFILEPATH4(String fILEPATH4) {
		FILEPATH4 = fILEPATH4;
	}
	public String getFILEPATH5() {
		return FILEPATH5;
	}
	public void setFILEPATH5(String fILEPATH5) {
		FILEPATH5 = fILEPATH5;
	}
	public String getFILEPATH6() {
		return FILEPATH6;
	}
	public void setFILEPATH6(String fILEPATH6) {
		FILEPATH6 = fILEPATH6;
	}
	public String getFILEPATH7() {
		return FILEPATH7;
	}
	public void setFILEPATH7(String fILEPATH7) {
		FILEPATH7 = fILEPATH7;
	}
	public String getFILEPATH8() {
		return FILEPATH8;
	}
	public void setFILEPATH8(String fILEPATH8) {
		FILEPATH8 = fILEPATH8;
	}
	public String getFILEPATH9() {
		return FILEPATH9;
	}
	public void setFILEPATH9(String fILEPATH9) {
		FILEPATH9 = fILEPATH9;
	}
	public String getFILEPATH10() {
		return FILEPATH10;
	}
	public void setFILEPATH10(String fILEPATH10) {
		FILEPATH10 = fILEPATH10;
	}
	public List<String> getLIST_OPTION_NM() {
		return LIST_OPTION_NM;
	}
	public void setLIST_OPTION_NM(List<String> lIST_OPTION_NM) {
		LIST_OPTION_NM = lIST_OPTION_NM;
	}
	public List<String> getLIST_OPTION_QNTT() {
		return LIST_OPTION_QNTT;
	}
	public void setLIST_OPTION_QNTT(List<String> lIST_OPTION_QNTT) {
		LIST_OPTION_QNTT = lIST_OPTION_QNTT;
	}
	public List<String> getLIST_PACKAGE_QNTT() {
		return LIST_PACKAGE_QNTT;
	}
	public void setLIST_PACKAGE_QNTT(List<String> lIST_PACKAGE_QNTT) {
		LIST_PACKAGE_QNTT = lIST_PACKAGE_QNTT;
	}
	public List<String> getLIST_BOX_QNTT() {
		return LIST_BOX_QNTT;
	}
	public void setLIST_BOX_QNTT(List<String> lIST_BOX_QNTT) {
		LIST_BOX_QNTT = lIST_BOX_QNTT;
	}
	public List<String> getLIST_AMOUNT() {
		return LIST_AMOUNT;
	}
	public void setLIST_AMOUNT(List<String> lIST_AMOUNT) {
		LIST_AMOUNT = lIST_AMOUNT;
	}
	public List<String> getLIST_PACKAGE1() {
		return LIST_PACKAGE1;
	}
	public void setLIST_PACKAGE1(List<String> lIST_PACKAGE1) {
		LIST_PACKAGE1 = lIST_PACKAGE1;
	}
	public List<String> getLIST_PACKAGE2() {
		return LIST_PACKAGE2;
	}
	public void setLIST_PACKAGE2(List<String> lIST_PACKAGE2) {
		LIST_PACKAGE2 = lIST_PACKAGE2;
	}
	public List<String> getLIST_PACKAGE3() {
		return LIST_PACKAGE3;
	}
	public void setLIST_PACKAGE3(List<String> lIST_PACKAGE3) {
		LIST_PACKAGE3 = lIST_PACKAGE3;
	}
	public List<String> getLIST_VACCUM() {
		return LIST_VACCUM;
	}
	public void setLIST_VACCUM(List<String> lIST_VACCUM) {
		LIST_VACCUM = lIST_VACCUM;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public List<String> getLIST_FILEPATH() {
		return LIST_FILEPATH;
	}
	public void setLIST_FILEPATH(List<String> lIST_FILEPATH) {
		LIST_FILEPATH = lIST_FILEPATH;
	}
	public String getPACKAGE1_NM() {
		return PACKAGE1_NM;
	}
	public void setPACKAGE1_NM(String pACKAGE1_NM) {
		PACKAGE1_NM = pACKAGE1_NM;
	}
	public String getPACKAGE2_NM() {
		return PACKAGE2_NM;
	}
	public void setPACKAGE2_NM(String pACKAGE2_NM) {
		PACKAGE2_NM = pACKAGE2_NM;
	}
	public String getPACKAGE3_NM() {
		return PACKAGE3_NM;
	}
	public void setPACKAGE3_NM(String pACKAGE3_NM) {
		PACKAGE3_NM = pACKAGE3_NM;
	}
	public String getPACKAGE1_FILEPATH() {
		return PACKAGE1_FILEPATH;
	}
	public void setPACKAGE1_FILEPATH(String pACKAGE1_FILEPATH) {
		PACKAGE1_FILEPATH = pACKAGE1_FILEPATH;
	}
	public String getPACKAGE2_FILEPATH() {
		return PACKAGE2_FILEPATH;
	}
	public void setPACKAGE2_FILEPATH(String pACKAGE2_FILEPATH) {
		PACKAGE2_FILEPATH = pACKAGE2_FILEPATH;
	}
	public String getPACKAGE3_FILEPATH() {
		return PACKAGE3_FILEPATH;
	}
	public void setPACKAGE3_FILEPATH(String pACKAGE3_FILEPATH) {
		PACKAGE3_FILEPATH = pACKAGE3_FILEPATH;
	}
	public String getOPTION_TP() {
		return OPTION_TP;
	}
	public void setOPTION_TP(String oPTION_TP) {
		OPTION_TP = oPTION_TP;
	}
	public String getMARKING_TP() {
		return MARKING_TP;
	}
	public void setMARKING_TP(String mARKING_TP) {
		MARKING_TP = mARKING_TP;
	}
	public String getMARKING_TEXT() {
		return MARKING_TEXT;
	}
	public void setMARKING_TEXT(String mARKING_TEXT) {
		MARKING_TEXT = mARKING_TEXT;
	}
	public List<String> getLIST_MARKING_TP() {
		return LIST_MARKING_TP;
	}
	public void setLIST_MARKING_TP(List<String> lIST_MARKING_TP) {
		LIST_MARKING_TP = lIST_MARKING_TP;
	}
	public List<String> getLIST_MARKING_TEXT() {
		return LIST_MARKING_TEXT;
	}
	public void setLIST_MARKING_TEXT(List<String> lIST_MARKING_TEXT) {
		LIST_MARKING_TEXT = lIST_MARKING_TEXT;
	}		
}
