package kr.co.hanpure.database.pdata;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("PdataVO")
public class PdataVO extends Pagenation {
	private String PRE_CD;
	private String PRE_NM;
	private String PRE_SRC_BOOK;
	private String PRE_SRC_BOOK_DT;
	private String PRE_MED;
	private String PRE_MED_QNTT;
	private String CREATED_AT;
	private String CREATED_BY;
	
	public String getPRE_CD() {
		return PRE_CD;
	}
	public void setPRE_CD(String pRE_CD) {
		PRE_CD = pRE_CD;
	}
	public String getPRE_NM() {
		return PRE_NM;
	}
	public void setPRE_NM(String pRE_NM) {
		PRE_NM = pRE_NM;
	}
	public String getPRE_SRC_BOOK() {
		return PRE_SRC_BOOK;
	}
	public void setPRE_SRC_BOOK(String pRE_SRC_BOOK) {
		PRE_SRC_BOOK = pRE_SRC_BOOK;
	}
	public String getPRE_SRC_BOOK_DT() {
		return PRE_SRC_BOOK_DT;
	}
	public void setPRE_SRC_BOOK_DT(String pRE_SRC_BOOK_DT) {
		PRE_SRC_BOOK_DT = pRE_SRC_BOOK_DT;
	}
	public String getPRE_MED() {
		return PRE_MED;
	}
	public void setPRE_MED(String pRE_MED) {
		PRE_MED = pRE_MED;
	}
	public String getPRE_MED_QNTT() {
		return PRE_MED_QNTT;
	}
	public void setPRE_MED_QNTT(String pRE_MED_QNTT) {
		PRE_MED_QNTT = pRE_MED_QNTT;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
}
