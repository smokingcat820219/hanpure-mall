package kr.co.hanpure.database.pdata;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PdataMapper {	
	void insertData(PdataVO vo);
	void updateData(PdataVO vo);
	PdataVO selectData(PdataVO vo);
	Integer selectDataCount(PdataVO vo);
	List<PdataVO> selectDataList(PdataVO vo);
	void insertDataDetail(PdataVO vo);
	PdataVO selectDataSub(PdataVO vo);
	List<PdataVO> selectDatasSub(PdataVO vo);
	
	List<PdataVO> selectDatasByBook();
}

