package kr.co.hanpure.database.pdata;

import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;

@Service
public class PdataService {
	@Autowired
	private PdataMapper mapper;
	
	public void insertData(PdataVO vo) {
		Logger.info();
		
		if(!CommonUtil.isNull(vo.getPRE_NM())) {
			vo.setPRE_NM(CommonUtil.xssEscape(vo.getPRE_NM()));
		}
		if(!CommonUtil.isNull(vo.getPRE_SRC_BOOK())) {
			vo.setPRE_SRC_BOOK(CommonUtil.xssEscape(vo.getPRE_SRC_BOOK()));
		}
		if(!CommonUtil.isNull(vo.getPRE_MED())) {
			vo.setPRE_MED(CommonUtil.xssEscape(vo.getPRE_MED()));
		}
		
		this.mapper.insertData(vo);
	}
	
	public void updateData(PdataVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public PdataVO selectData(PdataVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public Integer selectDataCount(PdataVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<PdataVO> selectDataList(PdataVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
	
	public void insertDataDetail(PdataVO vo) {
		Logger.info();
		
		if(!CommonUtil.isNull(vo.getPRE_MED())) {
			vo.setPRE_MED(CommonUtil.xssEscape(vo.getPRE_MED()));
		}
		
		this.mapper.insertDataDetail(vo);
	}
	
	public PdataVO selectDataSub(PdataVO vo) {
		Logger.info();
		return this.mapper.selectDataSub(vo);
	}
	
	public List<PdataVO> selectDatasSub(PdataVO vo) {
		Logger.info();
		return this.mapper.selectDatasSub(vo);
	}
	
	
	public List<PdataVO> selectDatasByBook() {
		Logger.info();
		return this.mapper.selectDatasByBook();
	}
	
	
	
	
	
	
	
	//공공데이터 포탈
	private static String ENCODING_KEY = "yi2tE%2FqBxYuBwnVrNQZgKxohnNyYGlZoM%2F1AUC0Ey5ElksxqB2rllupZaQAERtfaYI3V%2BvFHnMcrDJr9CKU1ow%3D%3D";
	private static String DECODING_KEY = "yi2tE/qBxYuBwnVrNQZgKxohnNyYGlZoM/1AUC0Ey5ElksxqB2rllupZaQAERtfaYI3V+vFHnMcrDJr9CKU1ow==";
	
	public void getPreFieldSearch(String searchText) {
		try {
			StringBuilder URL = new StringBuilder("http://apis.data.go.kr/1430000/PreInfoService/getPreFieldSearch");
			URL.append("?pn=" + URLEncoder.encode(searchText, "UTF-8"));
			URL.append("&ServiceKey=" + ENCODING_KEY);			
			
			System.out.println("URL : " + URL.toString());

			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpGet httpGet = new HttpGet(URL.toString());

			// UTF-8은 한글
			CloseableHttpResponse response = httpclient.execute(httpGet);

			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);
					
					System.out.println("result : " + result);
					
					org.json.JSONObject json = org.json.XML.toJSONObject(result);
					String jsonStr = json.toString();
					System.out.println(jsonStr);	
					
					mainParse(jsonStr);
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void mainParse(String text) {
		JSONObject json = CommonUtil.StringToJSONObject(text);
		JSONObject response = (JSONObject)json.get("response");
		JSONObject body = (JSONObject)response.get("body");
		JSONObject items = (JSONObject)body.get("items");
		JSONArray json_array = (JSONArray)items.get("item");
		
		if(json_array == null) {
			return;
		}
		
		for(int i = 0; i < json_array.size(); i++) {
			JSONObject obj = (JSONObject)json_array.get(i);
			String PRE_CD = (String)obj.get("preCd");
			String PRE_NM = (String)obj.get("preNm");
			String PRE_MED = (String)obj.get("preMed");
			String PRE_SRC_BOOK = (String)obj.get("preSrcBook");
			String PRE_SRC_BOOK_DT = (String)obj.get("preSrcBookDt");
			String PRE_MED_QNTT = "";
			
			String[] Alphabet = {"A", "B", "C", "D", "E", "F", "G"};
			
			for(int k = 0; k < Alphabet.length; k++) {
				PRE_NM = PRE_NM.replaceAll(Alphabet[k], "");
				PRE_MED = PRE_MED.replaceAll(Alphabet[k], "");
				PRE_SRC_BOOK = PRE_SRC_BOOK.replaceAll(Alphabet[k], "");
				PRE_SRC_BOOK_DT = PRE_SRC_BOOK_DT.replaceAll(Alphabet[k], "");
			}
			
			PdataVO pdataDB = new PdataVO();
			pdataDB.setPRE_CD(PRE_CD);
			pdataDB = mapper.selectData(pdataDB);
			if(pdataDB == null) {
				if(!CommonUtil.isNull(PRE_MED)) {
					//인서트
					String[] PRE_MEDS = PRE_MED.split(",");
					
					pdataDB = new PdataVO();
					pdataDB.setPRE_CD(PRE_CD);
					pdataDB.setPRE_NM(PRE_NM);
					pdataDB.setPRE_MED(PRE_MED);
					pdataDB.setPRE_MED_QNTT(String.valueOf(PRE_MEDS.length));					
					pdataDB.setPRE_SRC_BOOK(PRE_SRC_BOOK);
					pdataDB.setPRE_SRC_BOOK_DT(PRE_SRC_BOOK_DT);
					pdataDB.setCREATED_BY("system");
					
					insertData(pdataDB);
					
					
					for(int k = 0; k < PRE_MEDS.length; k++) {
						PdataVO pdataVO = new PdataVO();
						pdataVO.setPRE_CD(PRE_CD);
						pdataVO.setPRE_MED(PRE_MEDS[k].replaceAll(" ", ""));
						
						insertDataDetail(pdataVO);
					}
				}
			}				
		}
	}
	
	/*
	@Async
	public void getPreDetailInfo(String pre_cd) {
		try {
			StringBuilder URL = new StringBuilder("http://apis.data.go.kr/1430000/PreInfoService/getPreDetailInfo");
			URL.append("?preCd=" + URLEncoder.encode(pre_cd, "UTF-8"));
			URL.append("&ServiceKey=" + ENCODING_KEY);
			
			System.out.println("URL : " + URL.toString());

			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpGet httpGet = new HttpGet(URL.toString());

			// UTF-8은 한글
			CloseableHttpResponse response = httpclient.execute(httpGet);

			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);
					
					System.out.println("result : " + result);
					
					org.json.JSONObject json = org.json.XML.toJSONObject(result);
					String jsonStr = json.toString();
					System.out.println(jsonStr);	
					
					subParse(jsonStr);
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	public void subParse(String text) {
		JSONObject json = CommonUtil.StringToJSONObject(text);
		JSONObject response = (JSONObject)json.get("response");
		JSONObject body = (JSONObject)response.get("body");
		JSONObject item = (JSONObject)body.get("item");		
		
		String PRE_CD = (String)item.get("preCd");
		String PRE_SRC_BOOK = (String)item.get("preSrcBook");
		String PRE_MED = (String)item.get("preMed");
		
		String[] Alphabet = {"A", "B", "C", "D", "E", "F", "G"};
		
		for(int k = 0; k < Alphabet.length; k++) {
			PRE_MED = PRE_MED.replaceAll(Alphabet[k], "");
			PRE_SRC_BOOK = PRE_SRC_BOOK.replaceAll(Alphabet[k], "");
			PRE_MED = PRE_MED.replaceAll(Alphabet[k], "");
		}
		
		PRE_MED = PRE_MED.replaceAll(" ", "");		
		
		/*
		PdataVO pdataVO = new PdataVO();
		pdataVO.setPRE_CD(PRE_CD);
		pdataVO = mapper.selectData(pdataVO);
		if(pdataVO != null) {
			//출전 업데이트					
			pdataVO.setPRE_SRC_BOOK(PRE_SRC_BOOK);			
			mapper.updateData(pdataVO);
		}
		*/			
		
		String[] ARRAY = PRE_MED.split(",");
		String[] PRE_MEDS = new String[ARRAY.length];
		String[] PRE_MED_QNTTS = new String[ARRAY.length];
		
		for(int i = 0; i < ARRAY.length; i++) {
			int nDiv = ARRAY[i].lastIndexOf("(");
			
			System.out.println("nDiv : " + nDiv);
			
			PRE_MEDS[i] = ARRAY[i].substring(0, nDiv);
			PRE_MED_QNTTS[i] = ARRAY[i].substring(nDiv, ARRAY[i].length());
			
			PRE_MED_QNTTS[i] = PRE_MED_QNTTS[i].replaceAll("g", "");
			PRE_MED_QNTTS[i] = PRE_MED_QNTTS[i].replaceAll("\\(", "");
			PRE_MED_QNTTS[i] = PRE_MED_QNTTS[i].replaceAll("\\)", "");
			
			System.out.println("PRE_MEDS[i] : " + PRE_MEDS[i]);
			System.out.println("PRE_MED_QNTTS[i] : " + PRE_MED_QNTTS[i]);
			
			PdataVO pdataDB = new PdataVO();
			pdataDB.setPRE_CD(PRE_CD);
			pdataDB.setPRE_MED(PRE_MEDS[i]);
			pdataDB = mapper.selectDataSub(pdataDB);
			if(pdataDB == null) {
				//인서트
				pdataDB = new PdataVO();
				pdataDB.setPRE_CD(PRE_CD);
				pdataDB.setPRE_MED(PRE_MEDS[i]);
				pdataDB.setPRE_MED_QNTT(PRE_MED_QNTTS[i]);
				pdataDB.setCREATED_BY("system");
				
				mapper.insertDataDetail(pdataDB);
			}							
		}		
	}
}


