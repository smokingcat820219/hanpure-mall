package kr.co.hanpure.database.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper {	
	void insertData(UserVO vo);
	void updateData(UserVO vo);
	void deleteData(UserVO vo);
	UserVO selectData(UserVO vo);
	List<UserVO> selectDatas(UserVO vo);
	Integer selectDataCount(UserVO vo);
	List<UserVO> selectDataList(UserVO vo);
	
}
