package kr.co.hanpure.database.user;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("UserVO")
public class UserVO extends Pagenation {
	private String USER_ID;
	private String USER_CD;
	private String WORKPLACE_TP;
	private String WORKPLACE_CD;
	private String USER_NM;
	private String TEL;
	private String PWD;
	private String AUTH;
	private String WORK;
	private String ZIPCODE;
	private String ADDRESS;
	private String ADDRESS2;
	private String EMAIL;
	private String STATUS;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	private String WORKPLACE_TP_NM;
	private String AUTH_NM;
	private String WORK_NM;
	private String STATUS_NM;
	
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getUSER_CD() {
		return USER_CD;
	}
	public void setUSER_CD(String uSER_CD) {
		USER_CD = uSER_CD;
	}
	public String getWORKPLACE_TP() {
		return WORKPLACE_TP;
	}
	public void setWORKPLACE_TP(String wORKPLACE_TP) {
		WORKPLACE_TP = wORKPLACE_TP;
	}	
	public String getWORKPLACE_CD() {
		return WORKPLACE_CD;
	}
	public void setWORKPLACE_CD(String wORKPLACE_CD) {
		WORKPLACE_CD = wORKPLACE_CD;
	}
	public String getUSER_NM() {
		return USER_NM;
	}
	public void setUSER_NM(String uSER_NM) {
		USER_NM = uSER_NM;
	}
	public String getTEL() {
		return TEL;
	}
	public void setTEL(String tEL) {
		TEL = tEL;
	}
	public String getPWD() {
		return PWD;
	}
	public void setPWD(String pWD) {
		PWD = pWD;
	}
	public String getAUTH() {
		return AUTH;
	}
	public void setAUTH(String aUTH) {
		AUTH = aUTH;
	}
	public String getWORK() {
		return WORK;
	}
	public void setWORK(String wORK) {
		WORK = wORK;
	}	
	public String getZIPCODE() {
		return ZIPCODE;
	}
	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getADDRESS2() {
		return ADDRESS2;
	}
	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getWORKPLACE_TP_NM() {
		return WORKPLACE_TP_NM;
	}
	public void setWORKPLACE_TP_NM(String wORKPLACE_TP_NM) {
		WORKPLACE_TP_NM = wORKPLACE_TP_NM;
	}
	public String getAUTH_NM() {
		return AUTH_NM;
	}
	public void setAUTH_NM(String aUTH_NM) {
		AUTH_NM = aUTH_NM;
	}
	public String getWORK_NM() {
		return WORK_NM;
	}
	public void setWORK_NM(String wORK_NM) {
		WORK_NM = wORK_NM;
	}
	public String getSTATUS_NM() {
		return STATUS_NM;
	}
	public void setSTATUS_NM(String sTATUS_NM) {
		STATUS_NM = sTATUS_NM;
	}	
	
}
