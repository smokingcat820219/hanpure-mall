package kr.co.hanpure.database.user;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;
import kr.co.hanpure.common.CommonCode;

@Service
public class UserService {
	@Autowired
	private UserMapper mapper;
	
	public UserVO GetLoginSession(HttpSession session) {
		Logger.info();
		
		UserVO SESSION = (UserVO)session.getAttribute(CommonCode.USER_SESSION_KEY);
		if(SESSION == null) {
			return null;
		}
		else {
			UserVO userDB = new UserVO();
			userDB.setUSER_ID(SESSION.getUSER_ID());
			
			SESSION = this.selectData(userDB);
			
			return SESSION;
		} 
	}
	
	public void insertData(UserVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(UserVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(UserVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public UserVO selectData(UserVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<UserVO> selectDatas(UserVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(UserVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<UserVO> selectDataList(UserVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


