package kr.co.hanpure.database.product_text;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class ProductTextService {
	@Autowired
	private ProductTextMapper mapper;
	
	public void insertData(ProductTextVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void deleteDataAll(ProductTextVO vo) {
		Logger.info();
		this.mapper.deleteDataAll(vo);
	}
	
	public List<ProductTextVO> selectDatas(ProductTextVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
}


