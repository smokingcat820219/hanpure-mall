package kr.co.hanpure.database.product_text;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("ProductTextVO")
public class ProductTextVO extends Pagenation {
	private String PRODUCT_SEQ;
	private String TITLE;
	private String CONTENT;
	private String SORT;
	
	private List<String> LIST_TITLE = new ArrayList<>();
	private List<String> LIST_CONTENT = new ArrayList<>();
	
	public String getPRODUCT_SEQ() {
		return PRODUCT_SEQ;
	}
	public void setPRODUCT_SEQ(String pRODUCT_SEQ) {
		PRODUCT_SEQ = pRODUCT_SEQ;
	}
	public String getTITLE() {
		return TITLE;
	}
	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}
	public String getCONTENT() {
		return CONTENT;
	}
	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public List<String> getLIST_TITLE() {
		return LIST_TITLE;
	}
	public void setLIST_TITLE(List<String> lIST_TITLE) {
		LIST_TITLE = lIST_TITLE;
	}
	public List<String> getLIST_CONTENT() {
		return LIST_CONTENT;
	}
	public void setLIST_CONTENT(List<String> lIST_CONTENT) {
		LIST_CONTENT = lIST_CONTENT;
	}
}
