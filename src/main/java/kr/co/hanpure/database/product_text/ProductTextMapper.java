package kr.co.hanpure.database.product_text;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ProductTextMapper {	
	void insertData(ProductTextVO vo);
	void deleteDataAll(ProductTextVO vo);
	List<ProductTextVO> selectDatas(ProductTextVO vo);
}

