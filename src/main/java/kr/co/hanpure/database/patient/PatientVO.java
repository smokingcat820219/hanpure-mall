package kr.co.hanpure.database.patient;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("PatientVO")
public class PatientVO extends Pagenation {
	private String PATIENT_SEQ;
	private String HOSPITAL_ID;
	private String PATIENT_TP;
	private String CHART_NO;
	private String NAME;
	private String SEX;
	private String BIRTH;
	private String TEL;
	private String MOBILE;
	private String ZIPCODE;
	private String ADDRESS;
	private String ADDRESS2;	
	private String REMARK;
	private String RECIPE_DT;
	private String DEL_YN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	private String SEX_NM;
	private String PATIENT_TP_NM;	
	
	private List<String> LIST_PATIENT_SEQ = new ArrayList<String>();
	
	public String getPATIENT_SEQ() {
		return PATIENT_SEQ;
	}
	public void setPATIENT_SEQ(String pATIENT_SEQ) {
		PATIENT_SEQ = pATIENT_SEQ;
	}
	public String getHOSPITAL_ID() {
		return HOSPITAL_ID;
	}
	public void setHOSPITAL_ID(String hOSPITAL_ID) {
		HOSPITAL_ID = hOSPITAL_ID;
	}
	public String getCHART_NO() {
		return CHART_NO;
	}
	public void setCHART_NO(String cHART_NO) {
		CHART_NO = cHART_NO;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public String getBIRTH() {
		return BIRTH;
	}
	public void setBIRTH(String bIRTH) {
		BIRTH = bIRTH;
	}
	public String getTEL() {
		return TEL;
	}
	public void setTEL(String tEL) {
		TEL = tEL;
	}
	public String getMOBILE() {
		return MOBILE;
	}
	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}
	public String getZIPCODE() {
		return ZIPCODE;
	}
	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getADDRESS2() {
		return ADDRESS2;
	}
	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}
	public String getREMARK() {
		return REMARK;
	}
	public void setREMARK(String rEMARK) {
		REMARK = rEMARK;
	}
	public String getRECIPE_DT() {
		return RECIPE_DT;
	}
	public void setRECIPE_DT(String rECIPE_DT) {
		RECIPE_DT = rECIPE_DT;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getSEX_NM() {
		return SEX_NM;
	}
	public void setSEX_NM(String sEX_NM) {
		SEX_NM = sEX_NM;
	}
	public List<String> getLIST_PATIENT_SEQ() {
		return LIST_PATIENT_SEQ;
	}
	public void setLIST_PATIENT_SEQ(List<String> lIST_PATIENT_SEQ) {
		LIST_PATIENT_SEQ = lIST_PATIENT_SEQ;
	}
	public String getPATIENT_TP() {
		return PATIENT_TP;
	}
	public void setPATIENT_TP(String pATIENT_TP) {
		PATIENT_TP = pATIENT_TP;
	}
	public String getPATIENT_TP_NM() {
		return PATIENT_TP_NM;
	}
	public void setPATIENT_TP_NM(String pATIENT_TP_NM) {
		PATIENT_TP_NM = pATIENT_TP_NM;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}		
}
