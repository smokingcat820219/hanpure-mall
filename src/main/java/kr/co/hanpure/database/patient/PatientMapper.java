package kr.co.hanpure.database.patient;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PatientMapper {	
	void insertData(PatientVO vo);
	void updateData(PatientVO vo);
	PatientVO selectData(PatientVO vo);
	List<PatientVO> selectDatas(PatientVO vo);
	Integer selectDataCount(PatientVO vo);
	List<PatientVO> selectDataList(PatientVO vo);
}
