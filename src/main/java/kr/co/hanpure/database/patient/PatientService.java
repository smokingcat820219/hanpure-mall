package kr.co.hanpure.database.patient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class PatientService {
	@Autowired
	private PatientMapper mapper;
	
	public void insertData(PatientVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(PatientVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public PatientVO selectData(PatientVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<PatientVO> selectDatas(PatientVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(PatientVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<PatientVO> selectDataList(PatientVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


