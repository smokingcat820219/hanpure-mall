package kr.co.hanpure.database.code_sub;

import java.util.ArrayList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class CodeSubService {
	@Autowired
	private CodeSubMapper mapper;
	
	public void insertData(CodeSubVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(CodeSubVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(CodeSubVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public CodeSubVO selectData(CodeSubVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<CodeSubVO> selectDatas(CodeSubVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Map<String, String> selectDatasMap(CodeSubVO vo) {
		Logger.info();
		
		Map<String, String> map = new HashMap<String, String>();
		
		List<CodeSubVO> list = this.mapper.selectDatas(vo);
		for(int i = 0; i < list.size(); i++) {
			map.put(list.get(i).getSUB_CD(), list.get(i).getSUB_NM_KR());
		}
		
		return map;
	}
	
	public Map<String, List<String>> selectDatasMapList(CodeSubVO vo) {
		Logger.info();
		
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		
		List<CodeSubVO> list = this.mapper.selectDatas(vo);
		for(int i = 0; i < list.size(); i++) {
			List<String> list_sub = new ArrayList<String>();
			list_sub.add(list.get(i).getSUB_NM_KR());
			list_sub.add(list.get(i).getSUB_VALUE_KR());
			
			map.put(list.get(i).getSUB_CD(), list_sub);
		}
		
		return map;
	}
}


