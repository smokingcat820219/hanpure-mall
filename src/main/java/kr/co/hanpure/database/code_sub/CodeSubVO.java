package kr.co.hanpure.database.code_sub;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("CodeSubVO")
public class CodeSubVO extends Pagenation {
	private String SUB_CD;
	private String SUB_NM_KR;
	private String SUB_NM_CN;
	private String SUB_VALUE_KR;
	private String SUB_VALUE_CN;
	private String GROUP_CD;
	private String SORT;
	private String USE_YN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	public String getSUB_CD() {
		return SUB_CD;
	}
	public void setSUB_CD(String sUB_CD) {
		SUB_CD = sUB_CD;
	}
	public String getSUB_NM_KR() {
		return SUB_NM_KR;
	}
	public void setSUB_NM_KR(String sUB_NM_KR) {
		SUB_NM_KR = sUB_NM_KR;
	}
	public String getSUB_NM_CN() {
		return SUB_NM_CN;
	}
	public void setSUB_NM_CN(String sUB_NM_CN) {
		SUB_NM_CN = sUB_NM_CN;
	}
	public String getSUB_VALUE_KR() {
		return SUB_VALUE_KR;
	}
	public void setSUB_VALUE_KR(String sUB_VALUE_KR) {
		SUB_VALUE_KR = sUB_VALUE_KR;
	}
	public String getSUB_VALUE_CN() {
		return SUB_VALUE_CN;
	}
	public void setSUB_VALUE_CN(String sUB_VALUE_CN) {
		SUB_VALUE_CN = sUB_VALUE_CN;
	}
	public String getGROUP_CD() {
		return GROUP_CD;
	}
	public void setGROUP_CD(String gROUP_CD) {
		GROUP_CD = gROUP_CD;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getSORT() {
		return SORT;
	}
	public void setSORT(String sORT) {
		SORT = sORT;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}		
}
