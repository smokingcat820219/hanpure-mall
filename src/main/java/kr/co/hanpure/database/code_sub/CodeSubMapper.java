package kr.co.hanpure.database.code_sub;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CodeSubMapper {	
	void insertData(CodeSubVO vo);
	void updateData(CodeSubVO vo);
	void deleteData(CodeSubVO vo);
	CodeSubVO selectData(CodeSubVO vo);
	List<CodeSubVO> selectDatas(CodeSubVO vo);
}
