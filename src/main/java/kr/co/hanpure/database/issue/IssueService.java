package kr.co.hanpure.database.issue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class IssueService {
	@Autowired
	private IssueMapper mapper;
	
	public String createCode(String division) {
		Logger.info();
		String newCode = mapper.getNewCode(division);
		
		IssueVO issueVO = new IssueVO();
		issueVO.setCODE(newCode);
		this.mapper.insertData(issueVO);
		
		return newCode;
	}
	
	public String createHerbsCode() {
		Logger.info();
		String newCode = mapper.getHerbsCode();
		
		IssueVO issueVO = new IssueVO();
		issueVO.setCODE(newCode);
		this.mapper.insertData(issueVO);
		
		return newCode;
	}
	
	public String createDrugCode(String code) {
		Logger.info();
		String newCode = mapper.getDrugCode(code);
		
		IssueVO issueVO = new IssueVO();
		issueVO.setCODE(newCode);
		this.mapper.insertData(issueVO);
		
		return newCode;
	}
	
	public String createDrugBoxCode() {
		Logger.info();
		String newCode = mapper.getDrugBoxCode();
		
		IssueVO issueVO = new IssueVO();
		issueVO.setCODE(newCode);
		this.mapper.insertData(issueVO);
		
		return newCode;
	}
	
	public String createHotPotCode() {
		Logger.info();
		String newCode = mapper.getHotPotCode();
		
		IssueVO issueVO = new IssueVO();
		issueVO.setCODE(newCode);
		this.mapper.insertData(issueVO);
		
		return newCode;
	}
}
