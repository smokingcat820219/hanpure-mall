package kr.co.hanpure.database.issue;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface IssueMapper {	
	void insertData(IssueVO vo);
	String getNewCode(String division);
	String getHerbsCode();
	String getDrugCode(String code);
	String getDrugBoxCode();
	String getHotPotCode();
}
