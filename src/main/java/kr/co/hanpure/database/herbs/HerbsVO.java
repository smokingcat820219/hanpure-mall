package kr.co.hanpure.database.herbs;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import framework.smokingcat.paging.Pagenation;

@Alias("HerbsVO")
public class HerbsVO extends Pagenation {
	private String HERBS_CD;
	private String DIV1_CD;
	private String DIV2_CD;
	private String HABITUDE;
	private String TEMPER;
	private String ORGAN;
	private String POISON;
	private String HERBS_NM_KR;
	private String HERBS_NM_CN;
	private String NICK_NM_KR;
	private String NICK_NM_CN;
	private String TOP_NM_KR;
	private String TOP_NM_CN;
	private String MID_NM_KR;
	private String MID_NM_CN;
	private String DESC_KR;
	private String DESC_CN;
	private String CAUTION_KR;
	private String CAUTION_CN;
	private String PROCESS_KR;
	private String PROCESS_CN;
	private String EFFECT_KR;
	private String EFFECT_CN;
	private String FILENAME;
	private String FILEPATH;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	public String getHERBS_CD() {
		return HERBS_CD;
	}
	public void setHERBS_CD(String hERBS_CD) {
		HERBS_CD = hERBS_CD;
	}
	public String getDIV1_CD() {
		return DIV1_CD;
	}
	public void setDIV1_CD(String dIV1_CD) {
		DIV1_CD = dIV1_CD;
	}
	public String getDIV2_CD() {
		return DIV2_CD;
	}
	public void setDIV2_CD(String dIV2_CD) {
		DIV2_CD = dIV2_CD;
	}
	public String getHABITUDE() {
		return HABITUDE;
	}
	public void setHABITUDE(String hABITUDE) {
		HABITUDE = hABITUDE;
	}
	public String getTEMPER() {
		return TEMPER;
	}
	public void setTEMPER(String tEMPER) {
		TEMPER = tEMPER;
	}
	public String getORGAN() {
		return ORGAN;
	}
	public void setORGAN(String oRGAN) {
		ORGAN = oRGAN;
	}
	public String getPOISON() {
		return POISON;
	}
	public void setPOISON(String pOISON) {
		POISON = pOISON;
	}
	public String getHERBS_NM_KR() {
		return HERBS_NM_KR;
	}
	public void setHERBS_NM_KR(String hERBS_NM_KR) {
		HERBS_NM_KR = hERBS_NM_KR;
	}
	public String getHERBS_NM_CN() {
		return HERBS_NM_CN;
	}
	public void setHERBS_NM_CN(String hERBS_NM_CN) {
		HERBS_NM_CN = hERBS_NM_CN;
	}
	public String getNICK_NM_KR() {
		return NICK_NM_KR;
	}
	public void setNICK_NM_KR(String nICK_NM_KR) {
		NICK_NM_KR = nICK_NM_KR;
	}
	public String getNICK_NM_CN() {
		return NICK_NM_CN;
	}
	public void setNICK_NM_CN(String nICK_NM_CN) {
		NICK_NM_CN = nICK_NM_CN;
	}
	public String getTOP_NM_KR() {
		return TOP_NM_KR;
	}
	public void setTOP_NM_KR(String tOP_NM_KR) {
		TOP_NM_KR = tOP_NM_KR;
	}
	public String getTOP_NM_CN() {
		return TOP_NM_CN;
	}
	public void setTOP_NM_CN(String tOP_NM_CN) {
		TOP_NM_CN = tOP_NM_CN;
	}
	public String getMID_NM_KR() {
		return MID_NM_KR;
	}
	public void setMID_NM_KR(String mID_NM_KR) {
		MID_NM_KR = mID_NM_KR;
	}
	public String getMID_NM_CN() {
		return MID_NM_CN;
	}
	public void setMID_NM_CN(String mID_NM_CN) {
		MID_NM_CN = mID_NM_CN;
	}
	public String getDESC_KR() {
		return DESC_KR;
	}
	public void setDESC_KR(String dESC_KR) {
		DESC_KR = dESC_KR;
	}
	public String getDESC_CN() {
		return DESC_CN;
	}
	public void setDESC_CN(String dESC_CN) {
		DESC_CN = dESC_CN;
	}
	public String getCAUTION_KR() {
		return CAUTION_KR;
	}
	public void setCAUTION_KR(String cAUTION_KR) {
		CAUTION_KR = cAUTION_KR;
	}
	public String getCAUTION_CN() {
		return CAUTION_CN;
	}
	public void setCAUTION_CN(String cAUTION_CN) {
		CAUTION_CN = cAUTION_CN;
	}
	public String getPROCESS_KR() {
		return PROCESS_KR;
	}
	public void setPROCESS_KR(String pROCESS_KR) {
		PROCESS_KR = pROCESS_KR;
	}
	public String getPROCESS_CN() {
		return PROCESS_CN;
	}
	public void setPROCESS_CN(String pROCESS_CN) {
		PROCESS_CN = pROCESS_CN;
	}
	public String getEFFECT_KR() {
		return EFFECT_KR;
	}
	public void setEFFECT_KR(String eFFECT_KR) {
		EFFECT_KR = eFFECT_KR;
	}
	public String getEFFECT_CN() {
		return EFFECT_CN;
	}
	public void setEFFECT_CN(String eFFECT_CN) {
		EFFECT_CN = eFFECT_CN;
	}
	public String getFILENAME() {
		return FILENAME;
	}
	public void setFILENAME(String fILENAME) {
		FILENAME = fILENAME;
	}
	public String getFILEPATH() {
		return FILEPATH;
	}
	public void setFILEPATH(String fILEPATH) {
		FILEPATH = fILEPATH;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}	
}
