package kr.co.hanpure.database.herbs;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface HerbsMapper {	
	void insertData(HerbsVO vo);
	void updateData(HerbsVO vo);
	void deleteData(HerbsVO vo);
	HerbsVO selectData(HerbsVO vo);
	List<HerbsVO> selectDatas(HerbsVO vo);
	Integer selectDataCount(HerbsVO vo);
	List<HerbsVO> selectDataList(HerbsVO vo);
}
