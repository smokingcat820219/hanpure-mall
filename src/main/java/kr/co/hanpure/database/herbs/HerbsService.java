package kr.co.hanpure.database.herbs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class HerbsService {
	@Autowired
	private HerbsMapper mapper;
	
	public void insertData(HerbsVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(HerbsVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(HerbsVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public HerbsVO selectData(HerbsVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<HerbsVO> selectDatas(HerbsVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(HerbsVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<HerbsVO> selectDataList(HerbsVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


