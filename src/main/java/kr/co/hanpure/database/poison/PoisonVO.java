package kr.co.hanpure.database.poison;

import org.apache.ibatis.type.Alias;

import framework.smokingcat.paging.Pagenation;

@Alias("PoisonVO")
public class PoisonVO extends Pagenation {
	private String POISON_CD;
	private String HERBS_CD;	
	private String USE_KR;
	private String USE_CN;
	private String CAUTION_CONTENT_KR;
	private String CAUTION_CONTENT_CN;
	private String CREATED_AT;
	private String CREATED_BY;
	private String UPDATED_AT;
	private String UPDATED_BY;
	
	private String HERBS_NM;
	private String POISON;
	public String getPOISON_CD() {
		return POISON_CD;
	}
	public void setPOISON_CD(String pOISON_CD) {
		POISON_CD = pOISON_CD;
	}
	public String getHERBS_CD() {
		return HERBS_CD;
	}
	public void setHERBS_CD(String hERBS_CD) {
		HERBS_CD = hERBS_CD;
	}
	public String getUSE_KR() {
		return USE_KR;
	}
	public void setUSE_KR(String uSE_KR) {
		USE_KR = uSE_KR;
	}
	public String getUSE_CN() {
		return USE_CN;
	}
	public void setUSE_CN(String uSE_CN) {
		USE_CN = uSE_CN;
	}
	public String getCAUTION_CONTENT_KR() {
		return CAUTION_CONTENT_KR;
	}
	public void setCAUTION_CONTENT_KR(String cAUTION_CONTENT_KR) {
		CAUTION_CONTENT_KR = cAUTION_CONTENT_KR;
	}
	public String getCAUTION_CONTENT_CN() {
		return CAUTION_CONTENT_CN;
	}
	public void setCAUTION_CONTENT_CN(String cAUTION_CONTENT_CN) {
		CAUTION_CONTENT_CN = cAUTION_CONTENT_CN;
	}
	public String getCREATED_AT() {
		return CREATED_AT;
	}
	public void setCREATED_AT(String cREATED_AT) {
		CREATED_AT = cREATED_AT;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getUPDATED_AT() {
		return UPDATED_AT;
	}
	public void setUPDATED_AT(String uPDATED_AT) {
		UPDATED_AT = uPDATED_AT;
	}
	public String getUPDATED_BY() {
		return UPDATED_BY;
	}
	public void setUPDATED_BY(String uPDATED_BY) {
		UPDATED_BY = uPDATED_BY;
	}
	public String getHERBS_NM() {
		return HERBS_NM;
	}
	public void setHERBS_NM(String hERBS_NM) {
		HERBS_NM = hERBS_NM;
	}
	public String getPOISON() {
		return POISON;
	}
	public void setPOISON(String pOISON) {
		POISON = pOISON;
	}
}
