package kr.co.hanpure.database.poison;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class PoisonService {
	@Autowired
	private PoisonMapper mapper;
	
	public void insertData(PoisonVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(PoisonVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(PoisonVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public PoisonVO selectData(PoisonVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public List<PoisonVO> selectDatas(PoisonVO vo) {
		Logger.info();
		return this.mapper.selectDatas(vo);
	}
	
	public Integer selectDataCount(PoisonVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<PoisonVO> selectDataList(PoisonVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


