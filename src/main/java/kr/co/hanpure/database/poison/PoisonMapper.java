package kr.co.hanpure.database.poison;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PoisonMapper {	
	void insertData(PoisonVO vo);
	void updateData(PoisonVO vo);
	void deleteData(PoisonVO vo);
	PoisonVO selectData(PoisonVO vo);
	List<PoisonVO> selectDatas(PoisonVO vo);
	Integer selectDataCount(PoisonVO vo);
	List<PoisonVO> selectDataList(PoisonVO vo);
}
