package kr.co.hanpure.database.cust;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.smokingcat.logger.Logger;

@Service
public class CustService {
	@Autowired
	private CustMapper mapper;
	
	public void insertData(CustVO vo) {
		Logger.info();
		this.mapper.insertData(vo);
	}
	
	public void updateData(CustVO vo) {
		Logger.info();
		this.mapper.updateData(vo);
	}
	
	public void deleteData(CustVO vo) {
		Logger.info();
		this.mapper.deleteData(vo);
	}
	
	public CustVO selectData(CustVO vo) {
		Logger.info();
		return this.mapper.selectData(vo);
	}
	
	public Integer selectDataCount(CustVO vo) {
		Logger.info();
		return this.mapper.selectDataCount(vo);
	}
	
	public List<CustVO> selectDataList(CustVO vo) {
		Logger.info();
		return this.mapper.selectDataList(vo);
	}
}


