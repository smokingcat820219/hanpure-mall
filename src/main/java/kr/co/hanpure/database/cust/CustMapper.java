package kr.co.hanpure.database.cust;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CustMapper {	
	void insertData(CustVO vo);
	void updateData(CustVO vo);
	void deleteData(CustVO vo);
	CustVO selectData(CustVO vo);
	Integer selectDataCount(CustVO vo);
	List<CustVO> selectDataList(CustVO vo);
}

