package kr.co.hanpure.database3.mall_recipe_in;

import org.apache.ibatis.annotations.Mapper;

import kr.co.hanpure.database.recipein.RecipeInVO;


@Mapper
public interface MallRecipeInMapper {	
	void insertData(RecipeInVO vo);
	void deleteDataAll(String RECIPE_SEQ);
}
