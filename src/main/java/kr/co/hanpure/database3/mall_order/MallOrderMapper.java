package kr.co.hanpure.database3.mall_order;

import org.apache.ibatis.annotations.Mapper;

import kr.co.hanpure.database.order.OrderVO;


@Mapper
public interface MallOrderMapper {	
	void insertData(OrderVO vo);
	void updateData(OrderVO vo);
	OrderVO selectData(OrderVO vo);
}

