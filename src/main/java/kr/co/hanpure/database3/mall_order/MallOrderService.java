package kr.co.hanpure.database3.mall_order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import kr.co.hanpure.database.order.OrderMapper;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.database.recipe.RecipeMapper;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.database.recipein.RecipeInMapper;
import kr.co.hanpure.database.recipein.RecipeInVO;
import kr.co.hanpure.database3.mall_recipe.MallRecipeMapper;
import kr.co.hanpure.database3.mall_recipe_in.MallRecipeInMapper;

@Service
public class MallOrderService {
	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private RecipeMapper recipeMapper;
	
	@Autowired
	private RecipeInMapper recipeInMapper;
	
	
	@Autowired
	private MallOrderMapper mapper;
	
	@Autowired
	private MallRecipeMapper mallRecipeMapper;
	
	@Autowired
	private MallRecipeInMapper mallRecipeInMapper;
	
		
	@Async
	public void syncMall(String ORDER_SEQ) {
		OrderVO orderDB = new OrderVO();
		orderDB.setORDER_SEQ(ORDER_SEQ);
		orderDB = orderMapper.selectData(orderDB);
		if(orderDB != null) {
			if(mapper.selectData(orderDB) == null) {
				//insert
				mapper.insertData(orderDB);
			}
			else {
				//update
				mapper.updateData(orderDB);
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setORDER_SEQ(ORDER_SEQ);
			
			recipeDB = recipeMapper.selectData(recipeDB);
			if(recipeDB != null) {
				if(mallRecipeMapper.selectData(recipeDB) == null) {
					//insert
					mallRecipeMapper.insertData(recipeDB);
				}
				else {
					//update
					mallRecipeMapper.updateData(recipeDB);
				}
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				
				List<RecipeInVO> list_recipe = recipeInMapper.selectDatas(recipeInVO);
				
				mallRecipeInMapper.deleteDataAll(recipeDB.getRECIPE_SEQ());
				
				for(int i = 0; i < list_recipe.size(); i++) {
					mallRecipeInMapper.insertData(list_recipe.get(i));
				}
			}
		}
	}
}


