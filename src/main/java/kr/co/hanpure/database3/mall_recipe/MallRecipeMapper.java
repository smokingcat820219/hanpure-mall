package kr.co.hanpure.database3.mall_recipe;

import org.apache.ibatis.annotations.Mapper;

import kr.co.hanpure.database.recipe.RecipeVO;


@Mapper
public interface MallRecipeMapper {	
	void insertData(RecipeVO vo);
	void updateData(RecipeVO vo);
	RecipeVO selectData(RecipeVO vo);
}
