package kr.co.hanpure.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.insurance.InsuranceService;
import kr.co.hanpure.database.insurance.InsuranceVO;
import kr.co.hanpure.database.stock_drug.StockDrugService;
import kr.co.hanpure.database.stock_drug.StockDrugVO;
import kr.co.hanpure.database.workplace.WorkplaceService;

@Controller
public class InsuranceController {	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private InsuranceService insuranceService;	
	
	@Autowired
	private StockDrugService stockDrugService;
	
	@Autowired
	private WorkplaceService workplaceService;	
	
	private String DEPTH1 = "";
	private String DEPTH2 = "";
	
	@ResponseBody
	@RequestMapping(value = "/insurance/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, InsuranceVO param) {		
		Logger.info();	
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			}
			
			int nTotalItemCount = insuranceService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<InsuranceVO> list = insuranceService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/insurance/selectDetail", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectDetail(HttpServletRequest request, HttpSession session, HttpServletResponse response, InsuranceVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");	
			}
			
			String WH_CD = workplaceService.selectWhCd();
			if(CommonUtil.isNull(WH_CD)) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "원외탕전창고로 지정된 창고가 존재하지 않습니다.");
			
			InsuranceVO insuranceDB = new InsuranceVO();
			insuranceDB.setCODE(param.getCODE());
						
			List<StockDrugVO> list_drug = new ArrayList<StockDrugVO>();
			
			List<InsuranceVO> list = insuranceService.selectDatas(insuranceDB);
			for(int i = 0; i < list.size(); i++) {				
				String drug_nm = list.get(i).getDRUG_NM();
				String drug_qntt = list.get(i).getDRUG_QNTT();
				
				int nFind = drug_nm.indexOf("&#40;");
				if(nFind > -1) {
					drug_nm = drug_nm.substring(0, nFind);
				}
				
				StockDrugVO stockDrugDB = new StockDrugVO();
				stockDrugDB.setWH_CD(WH_CD);
				stockDrugDB.setDRUG_NM(drug_nm);
				stockDrugDB.getLIST_INSURANCE_YN().add("2");
				stockDrugDB.getLIST_INSURANCE_YN().add("3");
				
				stockDrugDB = stockDrugService.selectDataKorea(stockDrugDB);
				if(stockDrugDB == null) {
					stockDrugDB = new StockDrugVO();
					stockDrugDB.setWH_CD(WH_CD);
					stockDrugDB.setDRUG_NM(drug_nm);
					stockDrugDB.getLIST_INSURANCE_YN().add("2");
					stockDrugDB.getLIST_INSURANCE_YN().add("3");
					
					stockDrugDB = stockDrugService.selectDataElse(stockDrugDB);
					if(stockDrugDB != null) {
						//추가
						stockDrugDB.setCHUP1(drug_qntt);
						list_drug.add(stockDrugDB);
					}
				}
				else {
					//추가
					stockDrugDB.setCHUP1(drug_qntt);
					list_drug.add(stockDrugDB);
				}
			}		
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list_drug));	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
}
