package kr.co.hanpure.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.conflict.ConflictService;
import kr.co.hanpure.database.conflict.ConflictVO;
import kr.co.hanpure.database.hanpure.HanpureService;
import kr.co.hanpure.database.hanpure.HanpureVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.issue.IssueService;
import kr.co.hanpure.database.order.OrderService;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.database.patient.PatientService;
import kr.co.hanpure.database.patient.PatientVO;
import kr.co.hanpure.database.poison.PoisonService;
import kr.co.hanpure.database.poison.PoisonVO;
import kr.co.hanpure.database.recipe.RecipeService;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.database.recipe_attach.RecipeAttachService;
import kr.co.hanpure.database.recipe_attach.RecipeAttachVO;
import kr.co.hanpure.database.recipein.RecipeInService;
import kr.co.hanpure.database.recipein.RecipeInVO;
import kr.co.hanpure.database3.mall_order.MallOrderService;

@Controller
public class RecipeController {	
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private PoisonService poisonService;
	
	@Autowired
	private ConflictService conflictService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private HanpureService hanpureService;
	
	@Autowired
	private RecipeAttachService recipeAttachService;
	
	@Autowired
	private IssueService issueService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private PatientService patientService;	
	
	@Autowired
	private RecipeInService recipeInService;	
	
	@Autowired
	private MallOrderService mallOrderService;
	
	private String DEPTH1 = "처방하기";	
	
	@RequestMapping(value = "/recipe/write", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView write(HttpServletRequest request, HttpSession session, HttpServletResponse response, String RECIPE_TP, String PATIENT_SEQ) {		
		Logger.info();		
		
		if(CommonUtil.isNull(RECIPE_TP, new String[] {"001", "002", "003", "004"})) {
			RECIPE_TP = "001";
		}
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			List<String> list_doctors = new ArrayList<String>();
			String DIRECTOR = SESSION.getDIRECTOR();
			String DOCTOR = SESSION.getDOCTOR();
			
			if(!CommonUtil.isNull(DIRECTOR) && !DIRECTOR.equals("null")) {
				list_doctors.add(DIRECTOR);
			}
			
			String[] doctors = DOCTOR.split("\\|");
			for(int i = 0; i < doctors.length; i++) {
				if(!CommonUtil.isNull(doctors[i])) {
					list_doctors.add(doctors[i]);
				}
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//처방구분
			codeSubDB.setGROUP_CD("RECIPE_TP");			
			Map<String, String> map_recipe_tp = codeSubService.selectDatasMap(codeSubDB);
			
			//특수탕전
			codeSubDB.setGROUP_CD("SPECIAL_TP");			
			List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
			
			//제형
			codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
			List<CodeSubVO> list_circle_size = codeSubService.selectDatas(codeSubDB);
			
			//부형제
			codeSubDB.setGROUP_CD("DOUGH_TP");			
			List<CodeSubVO> list_dough_tp = codeSubService.selectDatas(codeSubDB);
						
			//유입구분
			codeSubDB.setGROUP_CD("INFLOW_TP");			
			List<CodeSubVO> list_inflow_tp = codeSubService.selectDatas(codeSubDB);	
			
			//분말도
			codeSubDB.setGROUP_CD("POWDER_TP");			
			List<CodeSubVO> list_powder_tp = codeSubService.selectDatas(codeSubDB);
			
			//농축
			codeSubDB.setGROUP_CD("PRESS_TP");			
			List<CodeSubVO> list_press_tp = codeSubService.selectDatas(codeSubDB);
			
			//라벨구분
			codeSubDB.setGROUP_CD("LABEL_TP");			
			List<CodeSubVO> list_label_tp = codeSubService.selectDatas(codeSubDB);			
						
			//독성분
			String POISON = "";
			List<PoisonVO> list_poison = poisonService.selectDatas(new PoisonVO());
			for(int i = 0; i < list_poison.size(); i++) {
				if(!POISON.equals("")) {
					POISON += ",";
				}
				
				POISON += list_poison.get(i).getHERBS_CD();
			}
			
			
			List<ConflictVO> list_conflict = conflictService.selectDatas(new ConflictVO());		

			ModelAndView mav = new ModelAndView("/recipe/write_recipe");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", map_recipe_tp.get(RECIPE_TP));
			mav.addObject("RECIPE_TP", RECIPE_TP);
			mav.addObject("TODAY", CommonUtil.getToday());
			mav.addObject("SESSION", SESSION);
			mav.addObject("LIST_DOCTORS", list_doctors);
			mav.addObject("MAP_RECIPE_TP", map_recipe_tp);
			mav.addObject("LIST_SPECIAL_TP", list_special_tp);
			mav.addObject("LIST_CIRCLE_SIZE", list_circle_size);
			mav.addObject("LIST_DOUGH_TP", list_dough_tp);
			mav.addObject("LIST_INFLOW_TP", list_inflow_tp);
			mav.addObject("LIST_POWDER_TP", list_powder_tp);
			mav.addObject("LIST_PRESS_TP", list_press_tp);
			mav.addObject("LIST_LABEL_TP", list_label_tp);
			mav.addObject("HANPURE", CommonUtil.ObjectToJSONObject(hanpureService.selectData(new HanpureVO())));
			mav.addObject("POISON", POISON);
			mav.addObject("CONFLICT", CommonUtil.ListToJSONArray(list_conflict));
			
			if(!CommonUtil.isNull(PATIENT_SEQ)) {
				PatientVO patientDB = new PatientVO();
				patientDB.setPATIENT_SEQ(PATIENT_SEQ);
				patientDB = patientService.selectData(patientDB);
				
				mav.addObject("PATIENT", patientDB);
			}

			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/recipe/write", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO recipe, RecipeInVO recipeIn, RecipeAttachVO recipeAttach) {		
		Logger.info();	
		
		CommonUtil.ShowValues(recipe);
		
		if(CommonUtil.isNull(recipe.getSAVE_TEMP(), new String[] {"AUTO", "TEMP", "SAVE"})) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");
		if(CommonUtil.isNull(recipe.getRE_HOTPOT(), new String[] {"Y", "N"})) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 002");
		if(CommonUtil.isNull(recipe.getRECIPE_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");
		
		if(CommonUtil.isNull(recipe.getTOTAL_CHUP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 004");
		if(CommonUtil.isNull(recipe.getTOTAL_QNTT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");	
		if(CommonUtil.isNull(recipe.getTOTAL_PRICE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");
		if(CommonUtil.isNull(recipe.getAMOUNT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
		
		if(!CommonUtil.isFloat(recipe.getTOTAL_CHUP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
		if(!CommonUtil.isFloat(recipe.getTOTAL_QNTT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");	
		if(!CommonUtil.isFloat(recipe.getTOTAL_PRICE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 010");
		if(!CommonUtil.isFloat(recipe.getAMOUNT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 011");		
		
		if(recipe.getSAVE_TEMP().equals("SAVE")) {
			if(CommonUtil.isNull(recipe.getINFLOW())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");
			if(CommonUtil.isNull(recipe.getDOCTOR())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 013");
			if(CommonUtil.isNull(recipe.getPATIENT_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 014");
			if(CommonUtil.isNull(recipe.getRECIPE_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 015");
			if(CommonUtil.isNull(recipe.getDRUG_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 0160");
					
			if(CommonUtil.isNull(recipe.getINFO1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 017");
			if(CommonUtil.isNull(recipe.getINFO2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 018");
			
			if(CommonUtil.isNull(recipe.getMARKING_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 019");
			if(CommonUtil.isNull(recipe.getMARKING_TEXT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 020");		
			//if(CommonUtil.isNull(recipe.getPACKAGE1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			if(recipeIn.getLIST_DRUG_CD() == null || recipeIn.getLIST_DRUG_CD().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 021");
			if(recipeIn.getLIST_DRUG_NM() == null || recipeIn.getLIST_DRUG_NM().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 022");
			if(recipeIn.getLIST_ORIGIN() == null || recipeIn.getLIST_ORIGIN().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 023");
			if(recipeIn.getLIST_TURN() == null || recipeIn.getLIST_TURN().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 024");
			if(recipeIn.getLIST_PRICE() == null || recipeIn.getLIST_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 025");		
			if(recipeIn.getLIST_CHUP1() == null || recipeIn.getLIST_CHUP1().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 026");
			if(recipeIn.getLIST_TOTAL_QNTT() == null || recipeIn.getLIST_TOTAL_QNTT().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 027");
			if(recipeIn.getLIST_TOTAL_PRICE() == null || recipeIn.getLIST_TOTAL_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 028");
			
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_DRUG_NM().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 029");
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_ORIGIN().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 030");
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TURN().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 031");
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 032");
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_CHUP1().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 033");
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TOTAL_QNTT().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 034");
			if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TOTAL_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 035");
		}
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}
			
			if(CommonUtil.isNull(recipe.getRECIPE_SEQ())) {
				System.out.println("### insert");
				
				//추가
				recipe.setHOSPITAL_ID(SESSION.getID());
				
				if(recipe.getSAVE_TEMP().equals("SAVE")) {
					recipe.setSAVE_TEMP("2");
				}
				else {
					recipe.setSAVE_TEMP("1");
				}				
				
				String RECIPE_CD = issueService.createCode("R");
				recipe.setRECIPE_CD(RECIPE_CD);	
				
				recipe.setCREATED_BY(SESSION.getID());
				recipe.setUPDATED_BY(SESSION.getID());
				
				recipe.setRECIPE_CNT(String.valueOf(recipeIn.getLIST_DRUG_CD().size()));
				
				recipe.setREMARK10("");
				recipe.setREMARK11("");
				
				recipe.setPRICE10("0");
				recipe.setPRICE11("0");
				
				recipeService.insertData(recipe);
				
				String RECIPE_SEQ = recipe.getRECIPE_SEQ();
				
				for(int i = 0; i < recipeIn.getLIST_DRUG_CD().size(); i++) {
					RecipeInVO recipeInVO = new RecipeInVO();
					recipeInVO.setSORT(String.valueOf(i + 1));
					recipeInVO.setRECIPE_SEQ(RECIPE_SEQ);
					recipeInVO.setDRUG_CD(recipeIn.getLIST_DRUG_CD().get(i));
					recipeInVO.setDRUG_NM(recipeIn.getLIST_DRUG_NM().get(i));				
					recipeInVO.setORIGIN(recipeIn.getLIST_ORIGIN().get(i));
					recipeInVO.setPRICE(recipeIn.getLIST_PRICE().get(i));
					recipeInVO.setCHUP1(recipeIn.getLIST_CHUP1().get(i));
					recipeInVO.setTURN(recipeIn.getLIST_TURN().get(i));
					recipeInVO.setTOTAL_QNTT(recipeIn.getLIST_TOTAL_QNTT().get(i));
					recipeInVO.setTOTAL_PRICE(recipeIn.getLIST_TOTAL_PRICE().get(i));
					
					recipeInService.insertData(recipeInVO);
				}			
				
				for(int i = 0; i < recipeAttach.getLIST_HOWTOMAKE_FILENAME().size(); i++) {
					RecipeAttachVO recipeAttachVO = new RecipeAttachVO();
					recipeAttachVO.setRECIPE_SEQ(RECIPE_SEQ);
					recipeAttachVO.setDIVISION("M");
					recipeAttachVO.setFILENAME(recipeAttach.getLIST_HOWTOMAKE_FILENAME().get(i));
					recipeAttachVO.setFILEPATH(recipeAttach.getLIST_HOWTOMAKE_FILEPATH().get(i));
					
					recipeAttachService.insertData(recipeAttachVO);
				}
				
				for(int i = 0; i < recipeAttach.getLIST_HOWTOEAT_FILENAME().size(); i++) {
					RecipeAttachVO recipeAttachVO = new RecipeAttachVO();
					recipeAttachVO.setRECIPE_SEQ(RECIPE_SEQ);
					recipeAttachVO.setDIVISION("E");
					recipeAttachVO.setFILENAME(recipeAttach.getLIST_HOWTOEAT_FILENAME().get(i));
					recipeAttachVO.setFILEPATH(recipeAttach.getLIST_HOWTOEAT_FILEPATH().get(i));
					
					recipeAttachService.insertData(recipeAttachVO);
				}
				
				return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, RECIPE_SEQ);
			}
			else {
				System.out.println("### update");
				
				//업데이트
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setRECIPE_SEQ(recipe.getRECIPE_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB != null) {
					if(recipe.getSAVE_TEMP().equals("SAVE")) {
						recipeDB.setSAVE_TEMP("2");
					}
					else {
						recipeDB.setSAVE_TEMP("1");
					}
					
					recipeDB.setDOCTOR(recipe.getDOCTOR());
					recipeDB.setPATIENT_SEQ(recipe.getPATIENT_SEQ());
					recipeDB.setREMARK(recipe.getREMARK());
					recipeDB.setINFLOW(recipe.getINFLOW());
					recipeDB.setRECIPE_NM(recipe.getRECIPE_NM());
					recipeDB.setINFO1(recipe.getINFO1());
					recipeDB.setINFO2(recipe.getINFO2());
					recipeDB.setINFO3(recipe.getINFO3());
					recipeDB.setINFO4(recipe.getINFO4());
					recipeDB.setINFO5(recipe.getINFO5());
					recipeDB.setINFO6(recipe.getINFO6());
					recipeDB.setINFO7(recipe.getINFO7());
					recipeDB.setINFO8(recipe.getINFO8());
					recipeDB.setINFO9(recipe.getINFO9());
					recipeDB.setINFO10(recipe.getINFO10());
					recipeDB.setINFO10_QNTT(recipe.getINFO10_QNTT());
					recipeDB.setINFO11(recipe.getINFO11());
					
					recipeDB.setREMARK1(recipe.getREMARK1());
					recipeDB.setREMARK2(recipe.getREMARK2());
					recipeDB.setREMARK3(recipe.getREMARK3());
					recipeDB.setREMARK4(recipe.getREMARK4());
					recipeDB.setREMARK5(recipe.getREMARK5());
					recipeDB.setREMARK6(recipe.getREMARK6());
					recipeDB.setREMARK7(recipe.getREMARK7());
					recipeDB.setREMARK8(recipe.getREMARK8());
					recipeDB.setREMARK9(recipe.getREMARK9());
					//recipeDB.setREMARK10(recipe.getREMARK10());
					//recipeDB.setREMARK11(recipe.getREMARK11());
					
					recipeDB.setPRICE1(recipe.getPRICE1());
					recipeDB.setPRICE2(recipe.getPRICE2());
					recipeDB.setPRICE3(recipe.getPRICE3());
					recipeDB.setPRICE4(recipe.getPRICE4());
					recipeDB.setPRICE5(recipe.getPRICE5());
					recipeDB.setPRICE6(recipe.getPRICE6());
					recipeDB.setPRICE7(recipe.getPRICE7());
					recipeDB.setPRICE8(recipe.getPRICE8());
					recipeDB.setPRICE9(recipe.getPRICE9());
					//recipeDB.setPRICE10(recipe.getPRICE10());
					//recipeDB.setPRICE11(recipe.getPRICE11());
					
					recipeDB.setTOTAL_CHUP(recipe.getTOTAL_CHUP());
					recipeDB.setTOTAL_QNTT(recipe.getTOTAL_QNTT());
					recipeDB.setTOTAL_PRICE(recipe.getTOTAL_PRICE());
					recipeDB.setAMOUNT(recipe.getAMOUNT());						
					
					recipeDB.setDRUG_LABEL_TP(recipe.getDRUG_LABEL_TP());
					recipeDB.setDRUG_LABEL_TEXT(recipe.getDRUG_LABEL_TEXT());
					recipeDB.setDELIVERY_LABEL_TP(recipe.getDELIVERY_LABEL_TP());
					recipeDB.setDELIVERY_LABEL_TEXT(recipe.getDELIVERY_LABEL_TEXT());
					
					recipeDB.setMARKING_TP(recipe.getMARKING_TP());
					recipeDB.setMARKING_TEXT(recipe.getMARKING_TEXT());
					recipeDB.setPACKAGE1(recipe.getPACKAGE1());
					recipeDB.setPACKAGE2(recipe.getPACKAGE2());
					recipeDB.setPACKAGE3(recipe.getPACKAGE3());
					recipeDB.setPACKAGE4(recipe.getPACKAGE4());
					
					recipeDB.setVACCUM(recipe.getVACCUM());
					recipeDB.setRE_HOTPOT(recipe.getRE_HOTPOT());
					
					recipeDB.setHOWTOMAKE(recipe.getHOWTOMAKE());
					recipeDB.setHOWTOEAT(recipe.getHOWTOEAT());
					
					recipeDB.setUPDATED_BY(SESSION.getID());
					
					recipeDB.setRECIPE_CNT(String.valueOf(recipeIn.getLIST_DRUG_CD().size()));
					
					recipeService.updateData(recipeDB);
					
					String RECIPE_SEQ = recipeDB.getRECIPE_SEQ();
					
					recipeInService.deleteDataAll(RECIPE_SEQ);
					
					for(int i = 0; i < recipeIn.getLIST_DRUG_CD().size(); i++) {
						RecipeInVO recipeInVO = new RecipeInVO();
						recipeInVO.setSORT(String.valueOf(i + 1));
						recipeInVO.setRECIPE_SEQ(RECIPE_SEQ);
						recipeInVO.setDRUG_CD(recipeIn.getLIST_DRUG_CD().get(i));
						recipeInVO.setDRUG_NM(recipeIn.getLIST_DRUG_NM().get(i));				
						recipeInVO.setORIGIN(recipeIn.getLIST_ORIGIN().get(i));
						recipeInVO.setPRICE(recipeIn.getLIST_PRICE().get(i));
						recipeInVO.setCHUP1(recipeIn.getLIST_CHUP1().get(i));
						recipeInVO.setTURN(recipeIn.getLIST_TURN().get(i));
						recipeInVO.setTOTAL_QNTT(recipeIn.getLIST_TOTAL_QNTT().get(i));
						recipeInVO.setTOTAL_PRICE(recipeIn.getLIST_TOTAL_PRICE().get(i));
						
						recipeInService.insertData(recipeInVO);
					}				
					
					recipeAttachService.deleteDataAll(RECIPE_SEQ);
					
					for(int i = 0; i < recipeAttach.getLIST_HOWTOMAKE_FILENAME().size(); i++) {
						RecipeAttachVO recipeAttachVO = new RecipeAttachVO();
						recipeAttachVO.setRECIPE_SEQ(RECIPE_SEQ);
						recipeAttachVO.setDIVISION("M");
						recipeAttachVO.setFILENAME(recipeAttach.getLIST_HOWTOMAKE_FILENAME().get(i));
						recipeAttachVO.setFILEPATH(recipeAttach.getLIST_HOWTOMAKE_FILEPATH().get(i));
						
						recipeAttachService.insertData(recipeAttachVO);
					}
					
					for(int i = 0; i < recipeAttach.getLIST_HOWTOEAT_FILENAME().size(); i++) {
						RecipeAttachVO recipeAttachVO = new RecipeAttachVO();
						recipeAttachVO.setRECIPE_SEQ(RECIPE_SEQ);
						recipeAttachVO.setDIVISION("E");
						recipeAttachVO.setFILENAME(recipeAttach.getLIST_HOWTOEAT_FILENAME().get(i));
						recipeAttachVO.setFILEPATH(recipeAttach.getLIST_HOWTOEAT_FILEPATH().get(i));
						
						recipeAttachService.insertData(recipeAttachVO);
					}
					
					return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, RECIPE_SEQ);
				}
				else {
					return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 036");
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/recipe/list_temp", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list_temp(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//처방구분
			codeSubDB.setGROUP_CD("RECIPE_TP");			
			List<CodeSubVO> list_recipe_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/recipe/list_temp");
			mav.addObject("DEPTH1", "처방관리");
			mav.addObject("DEPTH2", "임시저장");			
			mav.addObject("LIST_RECIPE_TP", list_recipe_tp);
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/recipe/selectPageTemp", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPageTemp(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}			
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setPRIVATE_YN("N");
			
			param.setSAVE_TEMP("Y");
			
			int nTotalItemCount = recipeService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			
			if(param.getItemPerPage() == 0) {
				param.setItemPerPage(nTotalItemCount);
			}
			
			List<RecipeVO> list = recipeService.selectDataList(param);					
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/recipe/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}			
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setPRIVATE_YN("N");
			param.setSAVE_TEMP("N");
			
			int nTotalItemCount = recipeService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<RecipeVO> list = recipeService.selectDataList(param);			
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/recipe/update/{RECIPE_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView update(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("RECIPE_SEQ") String RECIPE_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(RECIPE_SEQ);
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) {
				return CommonUtil.Go404();
			}
			
			if(recipeDB.getSAVE_TEMP().equals("1")) {	//주문정보				
				List<String> list_doctors = new ArrayList<String>();
				String DIRECTOR = SESSION.getDIRECTOR();
				String DOCTOR = SESSION.getDOCTOR();
				
				if(!CommonUtil.isNull(DIRECTOR) && !DIRECTOR.equals("null")) {
					list_doctors.add(DIRECTOR);
				}
				
				String[] doctors = DOCTOR.split("\\|");
				for(int i = 0; i < doctors.length; i++) {
					if(!CommonUtil.isNull(doctors[i])) {
						list_doctors.add(doctors[i]);
					}
				}
				
				CodeSubVO codeSubDB = new CodeSubVO();
				//처방구분
				codeSubDB.setGROUP_CD("RECIPE_TP");			
				Map<String, String> map_recipe_tp = codeSubService.selectDatasMap(codeSubDB);
				
				//유입구분
				codeSubDB.setGROUP_CD("INFLOW_TP");			
				List<CodeSubVO> list_inflow_tp = codeSubService.selectDatas(codeSubDB);	
				
				//라벨구분
				codeSubDB.setGROUP_CD("LABEL_TP");			
				List<CodeSubVO> list_label_tp = codeSubService.selectDatas(codeSubDB);
				
				//독성분
				String POISON = "";
				List<PoisonVO> list_poison = poisonService.selectDatas(new PoisonVO());
				for(int i = 0; i < list_poison.size(); i++) {
					if(!POISON.equals("")) {
						POISON += ",";
					}
					
					POISON += list_poison.get(i).getHERBS_CD();
				}
				
				
				List<ConflictVO> list_conflict = conflictService.selectDatas(new ConflictVO());		
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				RecipeAttachVO recipeAttachVO = new RecipeAttachVO();
				recipeAttachVO.setRECIPE_SEQ(RECIPE_SEQ);
				
				recipeAttachVO.setDIVISION("M");				
				List<RecipeAttachVO> list_attach_make = recipeAttachService.selectDatas(recipeAttachVO);
				
				recipeAttachVO.setDIVISION("E");				
				List<RecipeAttachVO> list_attach_eat = recipeAttachService.selectDatas(recipeAttachVO);
				
				PatientVO patientDB = new PatientVO();
				patientDB.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());
				patientDB = patientService.selectData(patientDB);				
				
				ModelAndView mav = new ModelAndView("/recipe/update_recipe");
				mav.addObject("DEPTH1", "처방관리");
				mav.addObject("DEPTH2", "임시저장");
				mav.addObject("RECIPE_TP", recipeDB.getRECIPE_TP());
				mav.addObject("SESSION", SESSION);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("PATIENT", patientDB);
				mav.addObject("LIST_RECIPE", list_recipe);				
				mav.addObject("LIST_DOCTORS", list_doctors);
				mav.addObject("MAP_RECIPE_TP", map_recipe_tp);
				mav.addObject("LIST_INFLOW_TP", list_inflow_tp);				
				mav.addObject("LIST_LABEL_TP", list_label_tp);				
				mav.addObject("LIST_ATTACH_MAKE", list_attach_make);
				mav.addObject("LIST_ATTACH_EAT", list_attach_eat);
				mav.addObject("HANPURE", CommonUtil.ObjectToJSONObject(hanpureService.selectData(new HanpureVO())));
				mav.addObject("POISON", POISON);
				mav.addObject("CONFLICT", CommonUtil.ListToJSONArray(list_conflict));
				
				if(recipeDB.getRECIPE_TP().equals("001")) {
					//특수탕전
					codeSubDB.setGROUP_CD("SPECIAL_TP");			
					List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
					
					//주수상반
					codeSubDB.setGROUP_CD("DRINK_TP");			
					Map<String, String> map_drink = codeSubService.selectDatasMap(codeSubDB);
					
					mav.addObject("LIST_SPECIAL_TP", list_special_tp);
					mav.addObject("MAP_DRINK", map_drink);
				}
				else if(recipeDB.getRECIPE_TP().equals("002")) {
					//제형
					codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
					List<CodeSubVO> list_circle_size = codeSubService.selectDatas(codeSubDB);
					
					//부형제
					codeSubDB.setGROUP_CD("DOUGH_TP");			
					List<CodeSubVO> list_dough_tp = codeSubService.selectDatas(codeSubDB);
					
					//분말도
					codeSubDB.setGROUP_CD("POWDER_TP");			
					List<CodeSubVO> list_powder_tp = codeSubService.selectDatas(codeSubDB);
					
					//농축
					codeSubDB.setGROUP_CD("PRESS_TP");			
					List<CodeSubVO> list_press_tp = codeSubService.selectDatas(codeSubDB);
					
					mav.addObject("LIST_CIRCLE_SIZE", list_circle_size);
					mav.addObject("LIST_DOUGH_TP", list_dough_tp);
					mav.addObject("LIST_POWDER_TP", list_powder_tp);
					mav.addObject("LIST_PRESS_TP", list_press_tp);					
				}
				else if(recipeDB.getRECIPE_TP().equals("003")) {
					
				}
				else if(recipeDB.getRECIPE_TP().equals("004")) {
					//특수탕전
					codeSubDB.setGROUP_CD("SPECIAL_TP");			
					List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
					
					//주수상반
					codeSubDB.setGROUP_CD("DRINK_TP");			
					Map<String, String> map_drink = codeSubService.selectDatasMap(codeSubDB);
					
					mav.addObject("LIST_SPECIAL_TP", list_special_tp);
					mav.addObject("MAP_DRINK", map_drink);
				}
				
				
			   	return mav;
			}
			else if(recipeDB.getSAVE_TEMP().equals("2")) {	//배송정보
				OrderVO orderDB = new OrderVO();
				orderDB.setORDER_SEQ(recipeDB.getORDER_SEQ());
				orderDB = orderService.selectData(orderDB);
				if(orderDB == null) {
					orderDB = new OrderVO();
					orderDB.setMSG_MAKE("");
				}		
				
				String MSG_MAKE = orderDB.getMSG_MAKE();
				MSG_MAKE = MSG_MAKE.replaceAll("\\[진공포장\\] ", "");
				
				if(recipeDB.getVACCUM().equals("Y")) {
					MSG_MAKE = "[진공포장]" + " " + MSG_MAKE;
				}
				orderDB.setMSG_MAKE(MSG_MAKE);
				
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				PatientVO patientDB = new PatientVO();
				patientDB.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());
				patientDB = patientService.selectData(patientDB);
				
				ModelAndView mav = new ModelAndView("/recipe/update_order");
				mav.addObject("DEPTH1", "처방관리");
				mav.addObject("DEPTH2", "임시저장");
				mav.addObject("SESSION", SESSION);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("ORDER", orderDB);
				mav.addObject("PATIENT", patientDB);
				mav.addObject("HANPURE", hanpureService.selectData(new HanpureVO()));				
				
				if(recipeDB.getRECIPE_TP().equals("001") || recipeDB.getRECIPE_TP().equals("004")) {
					//특수탕전
					CodeSubVO codeSubDB = new CodeSubVO();
					codeSubDB.setGROUP_CD("SPECIAL_TP");			
					Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
					
					mav.addObject("MAP_SPECIAL_TP", map_special_tp);
				}
				else if(recipeDB.getRECIPE_TP().equals("002")) {					
					//농축
					CodeSubVO codeSubDB = new CodeSubVO();
					codeSubDB.setGROUP_CD("PRESS_TP");			
					Map<String, String> map_press_tp = codeSubService.selectDatasMap(codeSubDB);
					
					//제형
					codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
					Map<String, String> map_circle_size = codeSubService.selectDatasMap(codeSubDB);
					
					//부형제
					codeSubDB.setGROUP_CD("DOUGH_TP");			
					Map<String, String> map_dough_tp = codeSubService.selectDatasMap(codeSubDB);
					
					//분말도
					codeSubDB.setGROUP_CD("POWDER_TP");			
					Map<String, String> map_powder_tp = codeSubService.selectDatasMap(codeSubDB);
					
					mav.addObject("MAP_PRESS_TP", map_press_tp);
					mav.addObject("MAP_CIRCLE_SIZE", map_circle_size);
					mav.addObject("MAP_DOUGH_TP", map_dough_tp);
					mav.addObject("MAP_POWDER_TP", map_powder_tp);
				}
				else if(recipeDB.getRECIPE_TP().equals("003")) {
					//연조엑스
					CodeSubVO codeSubDB = new CodeSubVO();
					codeSubDB.setGROUP_CD("SOFT_EXTRACT_TP");			
					Map<String, String> map_soft_extract_tp = codeSubService.selectDatasMap(codeSubDB);	
					
					mav.addObject("MAP_SOFT_EXTRACT_TP", map_soft_extract_tp);
				}
				
			   	return mav;
			}
			else {
				return CommonUtil.Go404();
			}
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/recipe/copy/{RECIPE_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView copy(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("RECIPE_SEQ") String RECIPE_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(RECIPE_SEQ);
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) {
				return CommonUtil.Go404();
			}			
					
			List<String> list_doctors = new ArrayList<String>();
			String DIRECTOR = SESSION.getDIRECTOR();
			String DOCTOR = SESSION.getDOCTOR();
			
			if(!CommonUtil.isNull(DIRECTOR) && !DIRECTOR.equals("null")) {
				list_doctors.add(DIRECTOR);
			}
			
			String[] doctors = DOCTOR.split("\\|");
			for(int i = 0; i < doctors.length; i++) {
				if(!CommonUtil.isNull(doctors[i])) {
					list_doctors.add(doctors[i]);
				}
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//처방구분
			codeSubDB.setGROUP_CD("RECIPE_TP");			
			Map<String, String> map_recipe_tp = codeSubService.selectDatasMap(codeSubDB);
			
			//유입구분
			codeSubDB.setGROUP_CD("INFLOW_TP");			
			List<CodeSubVO> list_inflow_tp = codeSubService.selectDatas(codeSubDB);	
			
			//라벨구분
			codeSubDB.setGROUP_CD("LABEL_TP");			
			List<CodeSubVO> list_label_tp = codeSubService.selectDatas(codeSubDB);
			
			//독성분
			String POISON = "";
			List<PoisonVO> list_poison = poisonService.selectDatas(new PoisonVO());
			for(int i = 0; i < list_poison.size(); i++) {
				if(!POISON.equals("")) {
					POISON += ",";
				}
				
				POISON += list_poison.get(i).getHERBS_CD();
			}
			
			
			List<ConflictVO> list_conflict = conflictService.selectDatas(new ConflictVO());		
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
			
			RecipeAttachVO recipeAttachVO = new RecipeAttachVO();
			recipeAttachVO.setRECIPE_SEQ(RECIPE_SEQ);
			
			recipeAttachVO.setDIVISION("M");				
			List<RecipeAttachVO> list_attach_make = recipeAttachService.selectDatas(recipeAttachVO);
			
			recipeAttachVO.setDIVISION("E");				
			List<RecipeAttachVO> list_attach_eat = recipeAttachService.selectDatas(recipeAttachVO);
			
			PatientVO patientDB = new PatientVO();
			patientDB.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());
			patientDB = patientService.selectData(patientDB);				
			
			ModelAndView mav = new ModelAndView("/recipe/copy_recipe");
			mav.addObject("DEPTH1", "처방관리");
			mav.addObject("DEPTH2", "임시저장");
			mav.addObject("RECIPE_TP", recipeDB.getRECIPE_TP());
			mav.addObject("SESSION", SESSION);
			mav.addObject("RECIPE", recipeDB);
			mav.addObject("PATIENT", patientDB);
			mav.addObject("LIST_RECIPE", list_recipe);				
			mav.addObject("LIST_DOCTORS", list_doctors);
			mav.addObject("MAP_RECIPE_TP", map_recipe_tp);
			mav.addObject("LIST_INFLOW_TP", list_inflow_tp);				
			mav.addObject("LIST_LABEL_TP", list_label_tp);				
			mav.addObject("LIST_ATTACH_MAKE", list_attach_make);
			mav.addObject("LIST_ATTACH_EAT", list_attach_eat);
			mav.addObject("HANPURE", CommonUtil.ObjectToJSONObject(hanpureService.selectData(new HanpureVO())));
			mav.addObject("POISON", POISON);
			mav.addObject("CONFLICT", CommonUtil.ListToJSONArray(list_conflict));
			
			if(recipeDB.getRECIPE_TP().equals("001")) {
				//특수탕전
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
				
				//주수상반
				codeSubDB.setGROUP_CD("DRINK_TP");			
				Map<String, String> map_drink = codeSubService.selectDatasMap(codeSubDB);
				
				mav.addObject("LIST_SPECIAL_TP", list_special_tp);
				mav.addObject("MAP_DRINK", map_drink);
			}
			else if(recipeDB.getRECIPE_TP().equals("002")) {
				//제형
				codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
				List<CodeSubVO> list_circle_size = codeSubService.selectDatas(codeSubDB);
				
				//부형제
				codeSubDB.setGROUP_CD("DOUGH_TP");			
				List<CodeSubVO> list_dough_tp = codeSubService.selectDatas(codeSubDB);
				
				//분말도
				codeSubDB.setGROUP_CD("POWDER_TP");			
				List<CodeSubVO> list_powder_tp = codeSubService.selectDatas(codeSubDB);
				
				//농축
				codeSubDB.setGROUP_CD("PRESS_TP");			
				List<CodeSubVO> list_press_tp = codeSubService.selectDatas(codeSubDB);
				
				mav.addObject("LIST_CIRCLE_SIZE", list_circle_size);
				mav.addObject("LIST_DOUGH_TP", list_dough_tp);
				mav.addObject("LIST_POWDER_TP", list_powder_tp);
				mav.addObject("LIST_PRESS_TP", list_press_tp);					
			}
			else if(recipeDB.getRECIPE_TP().equals("003")) {
				
			}
			else if(recipeDB.getRECIPE_TP().equals("004")) {
				//특수탕전
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
				
				//주수상반
				codeSubDB.setGROUP_CD("DRINK_TP");			
				Map<String, String> map_drink = codeSubService.selectDatasMap(codeSubDB);
				
				mav.addObject("LIST_SPECIAL_TP", list_special_tp);
				mav.addObject("MAP_DRINK", map_drink);
			}
			
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/recipe/order/write", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO order, RecipeVO recipe) {		
		Logger.info();	
		
		if(CommonUtil.isNull(order.getSAVE_TEMP(), new String[] {"PRE", "PRE_TEMP", "TEMP", "SAVE"})) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");
		if(CommonUtil.isNull(recipe.getRECIPE_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");
		
		String SAVE_TEMP = order.getSAVE_TEMP();
		
		if(order.getSAVE_TEMP().equals("SAVE")) {
			if(CommonUtil.isNull(order.getSEND_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 004");		
			if(CommonUtil.isNull(order.getSEND_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");
			if(CommonUtil.isNull(order.getSEND_TEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");
			if(CommonUtil.isNull(order.getSEND_MOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
			if(CommonUtil.isNull(order.getSEND_ZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
			if(CommonUtil.isNull(order.getSEND_ADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");
			if(CommonUtil.isNull(order.getSEND_ADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 010");
			
			if(CommonUtil.isNull(order.getRECV_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 011");
			if(CommonUtil.isNull(order.getRECV_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");
			if(CommonUtil.isNull(order.getRECV_TEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 013");
			if(CommonUtil.isNull(order.getRECV_MOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 014");
			if(CommonUtil.isNull(order.getRECV_ZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 015");
			if(CommonUtil.isNull(order.getRECV_ADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 016");
			if(CommonUtil.isNull(order.getRECV_ADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 017");
			
			if(CommonUtil.isNull(order.getDELIVERY_DT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 018");
		}
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(recipe.getRECIPE_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null || !recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.  Error 019");		
			
			HanpureVO hanpure = hanpureService.selectData(new HanpureVO());
			if(order.getRECV_ADDRESS().startsWith("서울")) {
				recipeDB.setREMARK10("서울 : " + CommonUtil.NumberFormat(hanpure.getPRICE_DELIVERY_SEOUL()) + "원");
				recipeDB.setPRICE10(hanpure.getPRICE_DELIVERY_SEOUL());
			}
			else {
				recipeDB.setREMARK10("서울 외 지역 : " + CommonUtil.NumberFormat(hanpure.getPRICE_DELIVERY_ETC()) + "원");
				recipeDB.setPRICE10(hanpure.getPRICE_DELIVERY_ETC());
			}
			
			if(SAVE_TEMP.equals("PRE")) {
				recipeDB.setSAVE_TEMP("1");
				recipeDB.setUPDATED_BY(SESSION.getID());
				
				recipeService.updateData(recipeDB);
				
				return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
			}
			else {				
				OrderVO orderDB = new OrderVO();
				orderDB.setORDER_SEQ(recipeDB.getORDER_SEQ());
				orderDB = orderService.selectData(orderDB);
				if(orderDB == null) {
					//추가
					if(SAVE_TEMP.equals("SAVE")) {
						order.setSAVE_TEMP("N");
					}
					else {
						order.setSAVE_TEMP("Y");
					}
					
					String ORDER_CD = issueService.createCode("O");
					
					order.setORDER_CD(ORDER_CD);
					order.setDOCTOR(recipeDB.getDOCTOR());
					order.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());					
					order.setORDER_TP(recipeDB.getRECIPE_TP());
					order.setRECIPE_TP(recipeDB.getRECIPE_TP());
					order.setORDER_PATH("001");
					order.setHOSPITAL_ID(SESSION.getID());	
					order.setORDER_DT(CommonUtil.getDateTime());
					order.setORDER_NM(recipeDB.getRECIPE_NM());
					order.setORDER_AMOUNT(recipeDB.getAMOUNT());					
					order.setCREATED_BY(SESSION.getID());
					order.setUPDATED_BY(SESSION.getID());				
					
					order.setDELIVERY_NM("한의사랑");
					orderService.insertData(order);
					
					String ORDER_SEQ = order.getORDER_SEQ();
					
					if(SAVE_TEMP.equals("PRE_TEMP")) {
						recipeDB.setORDER_SEQ(ORDER_SEQ);
						recipeDB.setSAVE_TEMP("1");
						recipeDB.setUPDATED_BY(SESSION.getID());
						recipeService.updateData(recipeDB);
					}
					else if(SAVE_TEMP.equals("TEMP")) {
						recipeDB.setORDER_SEQ(ORDER_SEQ);
						recipeDB.setSAVE_TEMP("2");
						recipeDB.setUPDATED_BY(SESSION.getID());
						recipeService.updateData(recipeDB);
					}
					else if(SAVE_TEMP.equals("SAVE")) {
						recipeDB.setORDER_SEQ(ORDER_SEQ);
						recipeDB.setSAVE_TEMP("3");
						recipeDB.setUPDATED_BY(SESSION.getID());
						recipeService.updateData(recipeDB);
						
						PatientVO patientDB = new PatientVO();
						patientDB.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());
						patientDB = patientService.selectData(patientDB);
						if(patientDB != null) {
							patientDB.setRECIPE_DT(CommonUtil.getToday());
							patientDB.setCREATED_BY(SESSION.getID());
							patientService.updateData(patientDB);
						}						
					}
					
					if(SAVE_TEMP.equals("SAVE")) {
						mallOrderService.syncMall(order.getORDER_SEQ());
					}
					
					return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, ORDER_SEQ);
				}
				else {
					//업데이트
					if(SAVE_TEMP.equals("SAVE")) {
						orderDB.setSAVE_TEMP("N");
						
						recipeDB.setSAVE_TEMP("3");
						recipeDB.setUPDATED_BY(SESSION.getID());
						recipeService.updateData(recipeDB);
						
						PatientVO patientDB = new PatientVO();
						patientDB.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());
						patientDB = patientService.selectData(patientDB);
						if(patientDB != null) {
							patientDB.setRECIPE_DT(CommonUtil.getToday());
							patientDB.setCREATED_BY(SESSION.getID());
							patientService.updateData(patientDB);
						}
					}
					else {
						orderDB.setSAVE_TEMP("Y");
					}
					
					orderDB.setDOCTOR(recipeDB.getDOCTOR());
					orderDB.setPATIENT_SEQ(recipeDB.getPATIENT_SEQ());
					orderDB.setRECIPE_TP(recipeDB.getRECIPE_TP());
					
					orderDB.setORDER_DT(CommonUtil.getDateTime());
					orderDB.setORDER_NM(recipeDB.getRECIPE_NM());
					orderDB.setORDER_AMOUNT(recipeDB.getAMOUNT());
					
					orderDB.setSEND_TP(order.getSEND_TP());		
					orderDB.setSEND_NM(order.getSEND_NM());
					orderDB.setSEND_TEL(order.getSEND_TEL());
					orderDB.setSEND_MOBILE(order.getSEND_MOBILE());
					orderDB.setSEND_ZIPCODE(order.getSEND_ZIPCODE());
					orderDB.setSEND_ADDRESS(order.getSEND_ADDRESS());
					orderDB.setSEND_ADDRESS2(order.getSEND_ADDRESS2());
					orderDB.setRECV_TP(order.getRECV_TP());
					orderDB.setRECV_NM(order.getRECV_NM());
					orderDB.setRECV_TEL(order.getRECV_TEL());
					orderDB.setRECV_MOBILE(order.getRECV_MOBILE());
					orderDB.setRECV_ZIPCODE(order.getRECV_ZIPCODE());
					orderDB.setRECV_ADDRESS(order.getRECV_ADDRESS());
					orderDB.setRECV_ADDRESS2(order.getRECV_ADDRESS2());					
					orderDB.setDELIVERY_DT(order.getDELIVERY_DT());					
					orderDB.setMSG_MAKE(order.getMSG_MAKE());
					orderDB.setMSG_DELIVERY(order.getMSG_DELIVERY());					
					orderDB.setUPDATED_BY(SESSION.getID());		
					
					orderService.updateData(orderDB);
					
					String ORDER_SEQ = orderDB.getORDER_SEQ();
					
					if(SAVE_TEMP.equals("PRE_TEMP")) {
						recipeDB.setORDER_SEQ(ORDER_SEQ);
						recipeDB.setSAVE_TEMP("1");
						recipeDB.setUPDATED_BY(SESSION.getID());
						recipeService.updateData(recipeDB);
					}
					
					if(SAVE_TEMP.equals("SAVE")) {
						mallOrderService.syncMall(orderDB.getORDER_SEQ());
					}
					
					return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, ORDER_SEQ);
				}
			}			
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/recipe/delete", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String delete(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		if(param.getLIST_RECIPE_SEQ() == null || param.getLIST_RECIPE_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}	
			
			for(int i = 0; i < param.getLIST_RECIPE_SEQ().size(); i++) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setRECIPE_SEQ(param.getLIST_RECIPE_SEQ().get(i));
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB != null && recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) {
					if(!recipeDB.getSAVE_TEMP().equals("3")) {	//대기 1, 2일 경우 삭제
						recipeDB.setDEL_YN("Y");
						recipeDB.setUPDATED_BY(SESSION.getID());
						
						recipeService.updateData(recipeDB);
					}
				}				
			}
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");		
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/recipe/selectDetail", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectDetail(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getRECIPE_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");			
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(param.getRECIPE_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null || !recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());			
			List<RecipeInVO> list = recipeInService.selectDatas(recipeInVO);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
}
