package kr.co.hanpure.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.pdata.PdataService;
import kr.co.hanpure.database.pdata.PdataVO;
import kr.co.hanpure.database.stock_drug.StockDrugService;
import kr.co.hanpure.database.stock_drug.StockDrugVO;
import kr.co.hanpure.database.workplace.WorkplaceService;
import kr.co.hanpure.excel.ExcelPdataList;

@Controller
public class PdataController {		
	@Autowired
	private PdataService pdataService;
	
	@Autowired
	private StockDrugService stockDrugService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private WorkplaceService workplaceService;
	
	private String DEPTH1 = "처방사전";
	private String DEPTH2 = "방제사전";
	
	@RequestMapping(value = "/pdata/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/pdata/list_pdata");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/pdata/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, PdataVO param) {		
		Logger.info();	
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");	
			}
			
			if(param.getPRE_NM() != null && !param.getPRE_NM().equals("")) {
				pdataService.getPreFieldSearch(param.getPRE_NM());
			}	
			
			if(param.getSearchText() != null && !param.getSearchText().equals("")) {
				pdataService.getPreFieldSearch(param.getSearchText());
			}	
			
			if(!CommonUtil.isNull(param.getPRE_SRC_BOOK())) {
				String PRE_SRC_BOOK = param.getPRE_SRC_BOOK();
				PRE_SRC_BOOK = new TagLibDisplay().getEditor(PRE_SRC_BOOK);
				param.setPRE_SRC_BOOK(PRE_SRC_BOOK);
			}	
			
			int nTotalItemCount = pdataService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<PdataVO> list = pdataService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@ResponseBody
	@RequestMapping(value = "/pdata/selectDetail", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectDetail(HttpServletRequest request, HttpSession session, HttpServletResponse response, PdataVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getPRE_CD())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");	
			}
			
			String WH_CD = workplaceService.selectWhCd();
			if(CommonUtil.isNull(WH_CD)) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "원외탕전창고로 지정된 창고가 존재하지 않습니다.");
			
			PdataVO pdataDB = new PdataVO();
			pdataDB.setPRE_CD(param.getPRE_CD());
						
			List<StockDrugVO> list_drug = new ArrayList<StockDrugVO>();
			
			List<PdataVO> list = pdataService.selectDatasSub(pdataDB);
			for(int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i).getPRE_MED());
				
				String drug_nm = list.get(i).getPRE_MED();
				
				int nFind = drug_nm.indexOf("&#40;");
				if(nFind > -1) {
					drug_nm = drug_nm.substring(0, nFind);
				}
				
				StockDrugVO stockDrugDB = new StockDrugVO();
				stockDrugDB.setWH_CD(WH_CD);
				stockDrugDB.setDRUG_NM(drug_nm);
				stockDrugDB.getLIST_INSURANCE_YN().add("1");
				stockDrugDB.getLIST_INSURANCE_YN().add("3");
				stockDrugDB = stockDrugService.selectDataKorea(stockDrugDB);
				if(stockDrugDB == null) {
					stockDrugDB = new StockDrugVO();
					stockDrugDB.setWH_CD(WH_CD);
					stockDrugDB.setDRUG_NM(drug_nm);
					stockDrugDB.getLIST_INSURANCE_YN().add("1");
					stockDrugDB.getLIST_INSURANCE_YN().add("3");
					stockDrugDB = stockDrugService.selectDataElse(stockDrugDB);
					if(stockDrugDB != null) {
						//추가
						list_drug.add(stockDrugDB);
					}
				}
				else {
					//추가
					list_drug.add(stockDrugDB);
				}
			}		
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list_drug));	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@RequestMapping(value = "/pdata/view/{PRE_CD}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("PRE_CD") String PRE_CD) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			PdataVO pdataDB = new PdataVO();
			pdataDB.setPRE_CD(PRE_CD);
			pdataDB = pdataService.selectData(pdataDB);
			if(pdataDB == null) return CommonUtil.Go404();
			
			List<PdataVO> list = pdataService.selectDatasSub(pdataDB);
						
			ModelAndView mav = new ModelAndView("/pdata/view_pdata");
			//mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("VO", pdataDB);
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/pdata/excel/download", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, PdataVO param) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			param.setPage(1);
			
			int nTotalItemCount = pdataService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			param.setItemPerPage(nTotalItemCount);
			List<PdataVO> list = pdataService.selectDataList(param);
			
			ModelAndView mav = new ModelAndView(new ExcelPdataList());
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}
