package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.stock_drug.StockDrugService;
import kr.co.hanpure.database.stock_drug.StockDrugVO;
import kr.co.hanpure.database.workplace.WorkplaceService;

@Controller
public class StockDrugController {
	@Autowired
	private StockDrugService stockDrugService;	
	
	@Autowired
	private WorkplaceService workplaceService;
	
	@Autowired
	private HospitalService hospitalService;
	
	private String DEPTH1 = "";	
	private String DEPTH2 = "";	
	
	@ResponseBody
	@RequestMapping(value = "/stock_drug/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, StockDrugVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}
			
			int nTotalItemCount = stockDrugService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<StockDrugVO> list = stockDrugService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/stock_drug/selectPageStock", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPageStock(HttpServletRequest request, HttpSession session, HttpServletResponse response, StockDrugVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}
			
			String WH_CD = workplaceService.selectWhCd();
			if(CommonUtil.isNull(WH_CD)) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "원외탕전창고로 지정된 창고가 존재하지 않습니다.");
						
			param.setWH_CD(WH_CD);
			param.setUSE_YN("Y");
			param.setSTOCK_QNTT("999");
			
			int nTotalItemCount = stockDrugService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<StockDrugVO> list = stockDrugService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
}
