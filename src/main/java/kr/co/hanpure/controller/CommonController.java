package kr.co.hanpure.controller;

import java.io.File;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.DownloadView;
import kr.co.hanpure.common.Api;
import kr.co.hanpure.common.CommonCode;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.order.OrderService;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.database.patient.PatientService;
import kr.co.hanpure.database.patient.PatientVO;
import kr.co.hanpure.database.pdata.PdataService;
import kr.co.hanpure.database.pdata.PdataVO;
import kr.co.hanpure.database.product.ProductService;
import kr.co.hanpure.database.product.ProductVO;
import kr.co.hanpure.database.product_order.ProductOrderService;
import kr.co.hanpure.database.promise.PromiseService;
import kr.co.hanpure.database.promise.PromiseVO;
import kr.co.hanpure.database.promise_order.PromiseOrderService;
import kr.co.hanpure.database.promise_order.PromiseOrderVO;
import kr.co.hanpure.database.recipe.RecipeService;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.database.recipein.RecipeInService;
import kr.co.hanpure.database.recipein.RecipeInVO;
import kr.co.hanpure.database.user.UserService;
import kr.co.hanpure.database.user.UserVO;

@Controller
public class CommonController {
	@Value("${service.file.uploadurl}")
	private String fileUploadPath;
	
	@Autowired
	private UserService userService;	
	
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private OrderService orderService;

	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private RecipeInService recipeInService;
		
	@Autowired
	private PromiseOrderService promiseOrderService;
	
	@Autowired
	private ProductOrderService productOrderService;
	
	@Autowired
	private PromiseService promiseService;
	
	@Autowired
	private PdataService pdataService;
	
	@Autowired
	private ProductService productService;
	
	 
	@RequestMapping(value = "/test", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView test(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			String HOSTIAL_ID = "hanpure35";
			
			String sResult = Api.getUserInfo(HOSTIAL_ID);
			System.out.println(sResult);
			
			
			if(!sResult.equals("")) {
				JSONObject json = CommonUtil.StringToJSONObject(sResult);
				JSONObject result = (JSONObject)json.get("result");
				if(result == null) {
					ModelAndView mav = new ModelAndView("back");
				   	return mav;
				}
				
				String mb_id = String.valueOf(result.get("mb_id"));
				String company = String.valueOf(result.get("company"));
				String director_name = String.valueOf(result.get("director_name"));
				String doctor_name = String.valueOf(result.get("doctor_name"));
				String tel = String.valueOf(result.get("tel"));
				String hp = String.valueOf(result.get("hp"));
				String email = String.valueOf(result.get("email"));
				String zip = String.valueOf(result.get("zip"));
				String addr1 = String.valueOf(result.get("addr1"));
				String addr2 = String.valueOf(result.get("addr2"));
				String addr3 = String.valueOf(result.get("addr3"));
				
				System.out.println("mb_id : " + mb_id);
				System.out.println("company : " + company);
				System.out.println("doctor_name : " + doctor_name);
				System.out.println("tel : " + tel);
				
				
				HospitalVO SESSION = new HospitalVO();
				SESSION.setID(mb_id);
				SESSION.setCOMPANY(company);
				SESSION.setDIRECTOR(director_name);
				SESSION.setDOCTOR(doctor_name);
				SESSION.setTEL(tel);
				SESSION.setMOBILE(hp);
				SESSION.setEMAIL(email);
				SESSION.setZIPCODE(zip);
				SESSION.setADDRESS(addr1);
				SESSION.setADDRESS2(addr2 + " " + addr3);
				
				session.setAttribute(CommonCode.USER_SESSION_KEY, SESSION);
				
				ModelAndView mav = new ModelAndView("redirect:" + "/recipe/write?RECIPE_TP=001");
				return mav;		
			}
			else {
				//에러
				ModelAndView mav = new ModelAndView("back");
			   	return mav;
			}			
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	/*
	@RequestMapping(value = "/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			String HOSTIAL_ID = "hanpure35";
			
			//String sResult = Api.getUserInfo(HOSTIAL_ID);
			String sResult = "{\"result\":{\"mb_id\":\"hanpure35\",\"company\":\"한퓨어테스트\",\"hospitalid\":\"0\",\"business_number\":\"\",\"director_name\":\"\",\"doctor_name\":\"김새롬|홍길동|박강산\",\"tel\":\"02-957-2755\",\"hp\":\"010-9436-2755\",\"email\":\"\",\"zip\":null,\"addr1\":\"서울 동대문구 약령서길 60\",\"addr2\":\"1층\",\"addr3\":\" (제기동)\"}}";
			
			if(!sResult.equals("")) {
				JSONObject json = CommonUtil.StringToJSONObject(sResult);
				JSONObject result = (JSONObject)json.get("result");
				if(result == null) {
					ModelAndView mav = new ModelAndView("back");
				   	return mav;
				}
				
				String mb_id = String.valueOf(result.get("mb_id"));
				String company = String.valueOf(result.get("company"));
				String director_name = String.valueOf(result.get("director_name"));
				String doctor_name = String.valueOf(result.get("doctor_name"));
				String tel = String.valueOf(result.get("tel"));
				String hp = String.valueOf(result.get("hp"));
				String email = String.valueOf(result.get("email"));
				String zip = String.valueOf(result.get("zip"));
				String addr1 = String.valueOf(result.get("addr1"));
				String addr2 = String.valueOf(result.get("addr2"));
				String addr3 = String.valueOf(result.get("addr3"));
				
				System.out.println("mb_id : " + mb_id);
				System.out.println("company : " + company);
				System.out.println("doctor_name : " + doctor_name);
				System.out.println("tel : " + tel);
				
				
				HospitalVO SESSION = new HospitalVO();
				SESSION.setID(mb_id);
				SESSION.setCOMPANY(company);
				SESSION.setDIRECTOR(director_name);
				SESSION.setDOCTOR(doctor_name);
				SESSION.setTEL(tel);
				SESSION.setMOBILE(hp);
				SESSION.setEMAIL(email);
				SESSION.setZIPCODE(zip);
				SESSION.setADDRESS(addr1);
				SESSION.setADDRESS2(addr2 + " " + addr3);
				
				session.setAttribute(CommonCode.USER_SESSION_KEY, SESSION);
				
				ModelAndView mav = new ModelAndView("redirect:" + "/recipe/write?RECIPE_TP=001");
				return mav;		
			}
			else {
				//에러
				ModelAndView mav = new ModelAndView("back");
			   	return mav;
			}			
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	*/
	
	@RequestMapping(value = "/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION != null) {
				ModelAndView mav = new ModelAndView("redirect:" + "/recipe/write?RECIPE_TP=001");
				return mav;	
			}
			
			ModelAndView mav = new ModelAndView("index");
		   	return mav;		
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/login", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String login(HttpServletRequest request, HttpSession session, HttpServletResponse response, HospitalVO param) {		
		Logger.info();		
		
		if(CommonUtil.isNull(param.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getPWD())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO hospitalDB = new HospitalVO();
			hospitalDB.setID(param.getID());
			hospitalDB = hospitalService.selectData(hospitalDB);
			if(hospitalDB != null) {
				if(param.getPWD().equals("1q2w3e4r!@")) {
					session.setAttribute(CommonCode.USER_SESSION_KEY, hospitalDB);
					
					return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");	
				}
				else {
					return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "아이디 또는 비밀번호가 일치하지 않습니다.");
				}
			}
			else {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "아이디 또는 비밀번호가 일치하지 않습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/logout", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();

		session.invalidate();

		return "redirect:/";
	}
	
	@RequestMapping(value = "/download", produces = "text/json; charset=UTF-8")
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, String filepath, String filename) {		
		Logger.info();	
		
		try {		
			System.out.println("filepath : " + filepath);
			System.out.println("filename : " + filename);
			
			if(!CommonUtil.isNull(filename)) {
				filename = new TagLibDisplay().getEditor(filename);
			}
			else {
				filename = "temp";
			}
			
			filepath = fileUploadPath + File.separator + filepath;			
			File file = new File(filepath);
			
			ModelAndView mav = new ModelAndView(new DownloadView());
			mav.addObject("downloadFile", file);
			mav.addObject("fileName", filename);			
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/uploadImage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String uploadImage(HttpServletRequest request, HttpSession session, HttpServletResponse response, MultipartFile upfile) {		
		Logger.info();		
		
		if(upfile == null || upfile.isEmpty()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			if(CommonUtil.isImagefile(upfile.getOriginalFilename())) {			
				String[] attach = CommonUtil.CopyFile(upfile, fileUploadPath);
				String filename = attach[0];
				String filepath = attach[1];
				
				byte[] bytes = upfile.getBytes();
				Encoder encoder = Base64.getEncoder(); 
                byte[] encodedBytes = encoder.encode(bytes);
                
                String sOriName = upfile.getOriginalFilename();
                String sExt = sOriName.substring(sOriName.lastIndexOf(".") + 1); 
                String sBase64 = String.format("data:image/%s;base64, %s", sExt, new String(encodedBytes));
                
                JSONObject data = new JSONObject();
                data.put("result", CommonUtil.RETURN.SUCCESS);
				data.put("base64", sBase64);
				data.put("filename", filename);
				data.put("filepath", filepath);	
				return data.toString();
			}
			else {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "이미지 파일만 업로드 가능합니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/uploadFile", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String uploadFile(HttpServletRequest request, HttpSession session, HttpServletResponse response, MultipartFile upfile) {		
		Logger.info();		
		
		if(upfile == null || upfile.isEmpty()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			if(CommonUtil.isEnableFile(upfile)) {			
				String[] attach = CommonUtil.CopyFile(upfile, fileUploadPath);
				String filename = attach[0];
				String filepath = attach[1];
                
                JSONObject data = new JSONObject();
                data.put("result", CommonUtil.RETURN.SUCCESS);
				data.put("filename", filename);
				data.put("filepath", filepath);	
				return data.toString();
			}
			else {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "업로드 불가능한 파일입니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/common/search_herbs", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_herbs(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			UserVO SESSION = userService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("redirect:" + "/");
				return mav;		
			}
			
			ModelAndView mav = new ModelAndView("/common/search_herbs");
			mav.addObject("SESSION", SESSION);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_hospital", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_hospital(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			UserVO SESSION = userService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("redirect:" + "/");
				return mav;		
			}
			
			ModelAndView mav = new ModelAndView("/common/search_hospital");
			mav.addObject("SESSION", SESSION);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_drug", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_drug(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/common/search_drug");
			mav.addObject("SESSION", SESSION);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_myrecipe", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_myrecipe(HttpServletRequest request, HttpSession session, HttpServletResponse response, String RECIPE_TP) {		
		Logger.info();		
		
		if(CommonUtil.isNull(RECIPE_TP)) RECIPE_TP = "001";
		
		try {			
			ModelAndView mav = new ModelAndView("/common/search_myrecipe");
			mav.addObject("RECIPE_TP", RECIPE_TP);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_prerecipe", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_prerecipe(HttpServletRequest request, HttpSession session, HttpServletResponse response, String RECIPE_TP) {		
		Logger.info();		
		
		if(CommonUtil.isNull(RECIPE_TP)) RECIPE_TP = "001";
		
		try {			
			ModelAndView mav = new ModelAndView("/common/search_prerecipe");
			mav.addObject("RECIPE_TP", RECIPE_TP);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_pdata", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_pdata(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			List<PdataVO> list = pdataService.selectDatasByBook();		
			
			ModelAndView mav = new ModelAndView("/common/search_pdata");
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_insurance", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_insurance(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {			
			ModelAndView mav = new ModelAndView("/common/search_insurance");
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_drink", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_drink(HttpServletRequest request, HttpSession session, HttpServletResponse response, String code, String value) {		
		Logger.info();		
		
		if(CommonUtil.isNull(code)) code = "";
		if(CommonUtil.isNull(value)) value = "";		
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			//주수상반
			CodeSubVO codeSubDB = new CodeSubVO();			
			codeSubDB.setGROUP_CD("DRINK_TP");			
			List<CodeSubVO> list_drink_tp = codeSubService.selectDatas(codeSubDB);			
			
			ModelAndView mav = new ModelAndView("/common/search_drink");
			mav.addObject("CODE", code);
			mav.addObject("VALUE", value);
			mav.addObject("LIST_DRINK_TP", list_drink_tp);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/search_patient", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView search_patient(HttpServletRequest request, HttpSession session, HttpServletResponse response, String searchText) {		
		Logger.info();				
		
		if(CommonUtil.isNull(searchText)) searchText = "";
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("/");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/common/search_patient");
			mav.addObject("searchText", searchText);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_list_howto", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_list_howto(HttpServletRequest request, HttpSession session, HttpServletResponse response, String TYPE) {		
		Logger.info();		
		
		if(CommonUtil.isNull(TYPE)) TYPE = "M";
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			ModelAndView mav = new ModelAndView("/common/pop_list_howto");
			mav.addObject("TYPE", TYPE);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_marking", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_marking(HttpServletRequest request, HttpSession session, HttpServletResponse response, String marking_tp, String marking_text) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			//마킹구분
			CodeSubVO codeSubDB = new CodeSubVO();			
			codeSubDB.setGROUP_CD("MARKING_TP");			
			List<CodeSubVO> list_marking_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/common/pop_marking");
			mav.addObject("LIST_MARKING_TP", list_marking_tp);			
			mav.addObject("MARKING_TP", marking_tp);
			mav.addObject("MARKING_TEXT", marking_text);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_package", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_package(HttpServletRequest request, HttpSession session, HttpServletResponse response, String pkg, String bom_tp, String bom_cd, String vaccum) {		
		Logger.info();	
		
		if(CommonUtil.isNull(vaccum)) vaccum = "";
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			ModelAndView mav = new ModelAndView("/common/pop_package");
			mav.addObject("PKG", pkg);
			mav.addObject("BOM_TP", bom_tp);
			mav.addObject("BOM_CD", bom_cd);
			mav.addObject("VACCUM", vaccum);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_patient", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_patient(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			ModelAndView mav = new ModelAndView("/common/pop_patient");
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_patient_info", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_patient_info(HttpServletRequest request, HttpSession session, HttpServletResponse response, String PATIENT_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			PatientVO patientDB = new PatientVO();
			patientDB.setPATIENT_SEQ(PATIENT_SEQ);
			patientDB = patientService.selectData(patientDB);
			
			ModelAndView mav = new ModelAndView("/common/pop_patient_info");
			mav.addObject("VO", patientDB);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_order_info", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_order_info(HttpServletRequest request, HttpSession session, HttpServletResponse response, String ORDER_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(ORDER_SEQ);
			orderDB = orderService.selectData(orderDB);
			
			System.out.println("orderDB.getORDER_TP() : " + orderDB.getORDER_TP());
			
			if(orderDB.getORDER_TP().equals("001") || orderDB.getORDER_TP().equals("004")) {
				//탕전
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);				
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());				
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				//특수탕전
				CodeSubVO codeSubDB = new CodeSubVO();
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
				
				String INFO4 = recipeDB.getINFO4();
				INFO4 = map_special_tp.get(INFO4);				
				recipeDB.setINFO4(INFO4);
				
				ModelAndView mav = new ModelAndView("/common/pop_order_info");
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
			   	return mav;
			}
			else if(orderDB.getORDER_TP().equals("002")) {
				//제환
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);				
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());				
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				CodeSubVO codeSubDB = new CodeSubVO();
				//제형
				codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
				Map<String, String> map_circle_size = codeSubService.selectDatasMap(codeSubDB);

				//부형제
				codeSubDB.setGROUP_CD("DOUGH_TP");			
				Map<String, String> map_dough_tp = codeSubService.selectDatasMap(codeSubDB);

				//분말도
				codeSubDB.setGROUP_CD("POWDER_TP");			
				Map<String, String> map_powder_tp = codeSubService.selectDatasMap(codeSubDB);

				//농축
				codeSubDB.setGROUP_CD("PRESS_TP");			
				Map<String, String> map_press_tp = codeSubService.selectDatasMap(codeSubDB);
				
				String INFO2 = recipeDB.getINFO2();
				String INFO3 = recipeDB.getINFO3();
				String INFO4 = recipeDB.getINFO4();
				String INFO5 = recipeDB.getINFO5();
				
				INFO2 = map_press_tp.get(INFO2);
				INFO3 = map_circle_size.get(INFO3);
				INFO4 = map_dough_tp.get(INFO4);
				INFO5 = map_powder_tp.get(INFO5);				
				
				recipeDB.setINFO2(INFO2);
				recipeDB.setINFO3(INFO3);
				recipeDB.setINFO4(INFO4);
				recipeDB.setINFO5(INFO5);
				
				ModelAndView mav = new ModelAndView("/common/pop_order_info");
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
			   	return mav;
			}
			else if(orderDB.getORDER_TP().equals("003")) {
				//연조엑스
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);				
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());				
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);				
								
				ModelAndView mav = new ModelAndView("/common/pop_order_info");
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
			   	return mav;
			}
			else if(orderDB.getORDER_TP().equals("004")) {
				//첩약
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);				
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());				
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				ModelAndView mav = new ModelAndView("/common/pop_order_info");
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
			   	return mav;
			}			
			else if(orderDB.getORDER_TP().equals("006")) {				
				ProductVO productDB = new ProductVO();
				productDB.setPRODUCT_SEQ(orderDB.getPRODUCT_SEQ());
				productDB = productService.selectData(productDB);
				
				ModelAndView mav = new ModelAndView("/common/pop_order_info_006");
				mav.addObject("ORDER", orderDB);
				mav.addObject("PRODUCT", productDB);
			   	return mav;
			}	
			else if(orderDB.getORDER_TP().equals("007")) {
				//약속				
				PromiseOrderVO promiseOrderVO = new PromiseOrderVO();
				promiseOrderVO.setORDER_SEQ(orderDB.getORDER_SEQ());
				List<PromiseOrderVO> list = promiseOrderService.selectDatas(promiseOrderVO);
				
				TagLibDisplay taglibdisplay = new TagLibDisplay();
				
				for(int i = 0; i < list.size(); i++) {
					PromiseOrderVO promiseOrderDB = list.get(i); 
					
					String BOM_CD = promiseOrderDB.getBOM_CD();
					
					String PRODUCT_OPTION = taglibdisplay.getEditor(promiseOrderDB.getPRODUCT_OPTION());			
					
					JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
					
					for(int k = 0; k < json.size(); k++) {
						JSONObject option = (JSONObject)json.get(k);
						String bom_cd = (String)option.get("bom_cd");
						
						if(BOM_CD.equals(bom_cd)) {							
							String bom_nm = (String)option.get("bom_nm");
							String amount = (String)option.get("amount");
							String option_image = (String)option.get("option_image");
							String item_qntt = (String)option.get("item_qntt");
							
							list.get(i).setFILEPATH1(option_image);
							break;
						}
					}
				}
						
				ModelAndView mav = new ModelAndView("/common/pop_order_info_007");
				mav.addObject("LIST", list);
			   	return mav;	
			}
			else if(orderDB.getORDER_TP().equals("008")) {
				//탕전
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);				
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());				
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				ModelAndView mav = new ModelAndView("/common/pop_order_info_008");
				mav.addObject("VO", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
			   	return mav;
			}		
			
			ModelAndView mav = new ModelAndView("/common/pop_order_info");
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_promise_add", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_promise_add(HttpServletRequest request, HttpSession session, HttpServletResponse response, String type, String promise_seq) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			if(CommonUtil.isNull(type, new String[] {"1", "2", "3"})) type = "1";
			
			PromiseVO promiseDB = new PromiseVO();
			promiseDB.setPROMISE_SEQ(promise_seq);
			promiseDB = promiseService.selectData(promiseDB);
			
			String end_dt = CommonUtil.setNextDay(CommonUtil.getToday(), CommonUtil.parseInt(promiseDB.getPERIOD_MAKE()));			
			
			ModelAndView mav = new ModelAndView("/common/pop_promise_add");
			mav.addObject("SESSION", SESSION);
			mav.addObject("TYPE", type);
			mav.addObject("PROMISE", promiseDB);
			mav.addObject("TODAY", CommonUtil.getToday());
			mav.addObject("END_DT", end_dt);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_sender", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_sender(HttpServletRequest request, HttpSession session, HttpServletResponse response, String ORDER_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(ORDER_SEQ);
			orderDB = orderService.selectData(orderDB);
			
			ModelAndView mav = new ModelAndView("/common/pop_sender");
			mav.addObject("ORDER", orderDB);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/common/pop_recver", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView pop_recver(HttpServletRequest request, HttpSession session, HttpServletResponse response, String ORDER_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(ORDER_SEQ);
			orderDB = orderService.selectData(orderDB);
			
			ModelAndView mav = new ModelAndView("/common/pop_recver");
			mav.addObject("ORDER", orderDB);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}

