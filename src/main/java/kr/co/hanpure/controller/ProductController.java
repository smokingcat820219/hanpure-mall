package kr.co.hanpure.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.category.CategoryService;
import kr.co.hanpure.database.category.CategoryVO;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.conflict.ConflictService;
import kr.co.hanpure.database.hanpure.HanpureService;
import kr.co.hanpure.database.hanpure.HanpureVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.issue.IssueService;
import kr.co.hanpure.database.order.OrderService;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.database.poison.PoisonService;
import kr.co.hanpure.database.product.ProductService;
import kr.co.hanpure.database.product.ProductVO;
import kr.co.hanpure.database.product_option.ProductOptionService;
import kr.co.hanpure.database.product_order.ProductOrderService;
import kr.co.hanpure.database.product_text.ProductTextService;
import kr.co.hanpure.database.recipe.RecipeService;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.database.recipein.RecipeInService;
import kr.co.hanpure.database.recipein.RecipeInVO;
import kr.co.hanpure.database.user.UserService;
import kr.co.hanpure.database3.mall_order.MallOrderService;

/**
 * 독성알람 관리
 * 
 */
@Controller
public class ProductController {
	@Autowired
	private ConflictService conflictService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private IssueService issueService; 
	
	@Autowired
	private HanpureService hanpureService;
	
	@Autowired
	private CodeSubService codeSubService;
		
	@Autowired
	private PoisonService poisonService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private ProductTextService productTextService;
	
	@Autowired
	private ProductOptionService productOptionService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private OrderService orderService;	
	
	@Autowired
	private ProductOrderService productOrderService;
	
	@Autowired
	private RecipeInService recipeInService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private MallOrderService mallOrderService;
	
	private String DEPTH1 = "간편처방";
	private String DEPTH2 = "상용처방";	
	
	@RequestMapping(value = "/product/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response, String PRODUCT_TP) {		
		Logger.info();		
		
		if(CommonUtil.isNull(PRODUCT_TP, new String[] {"005", "006"})) PRODUCT_TP = "005";
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("redirect:" + "/");
				return mav;		
			}
			
			CategoryVO categoryVO = new CategoryVO();
			categoryVO.setPRODUCT_TP(PRODUCT_TP);
			categoryVO.setP_CODE("");
			List<CategoryVO> list_category1 = categoryService.selectDatas(categoryVO);	
			
			ModelAndView mav = new ModelAndView("/product/list_product_" + PRODUCT_TP);
			mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			
			if(PRODUCT_TP.equals("005")) {
				mav.addObject("DEPTH2", "약속처방");
			}
			else {
				mav.addObject("DEPTH2", "상용처방");
			}
			
			mav.addObject("PRODUCT_TP", PRODUCT_TP);
			mav.addObject("LIST_CATEGORY1", list_category1);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/product/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, ProductVO param) {		
		Logger.info();	
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.LOGIN, "로그인 후 이용해 주세요.");	
			}
			
			int nTotalItemCount = productService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<ProductVO> list = productService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/product/view/{PRODUCT_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("PRODUCT_SEQ") String PRODUCT_SEQ) {		
		Logger.info();	
		
		try {		
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ProductVO productDB = new ProductVO();
			productDB.setPRODUCT_SEQ(PRODUCT_SEQ);
			productDB = productService.selectData(productDB);
			if(productDB == null) return CommonUtil.Go404();	
			
			TagLibDisplay taglibdisplay = new TagLibDisplay();
			
			productDB.setPRODUCT_TEXT(taglibdisplay.getEditor(productDB.getPRODUCT_TEXT()));
			productDB.setPRODUCT_OPTION(taglibdisplay.getEditor(productDB.getPRODUCT_OPTION()));
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(productDB.getRECIPE_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());			
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
			
			ModelAndView mav = new ModelAndView("/product/view_product_" + productDB.getPRODUCT_TP());
			mav.addObject("DEPTH1", DEPTH1);
			
			if(productDB.getPRODUCT_TP().equals("005")) {
				mav.addObject("DEPTH2", "약속처방");
				
				String finish_date = CommonUtil.setNextDay(CommonUtil.getToday(), CommonUtil.parseInt(productDB.getPERIOD_MAKE()));
				finish_date = finish_date.replaceAll("-",  ".");
				
				mav.addObject("FINISH_DATE", finish_date);
			}
			else {				
				mav.addObject("DEPTH2", "상용처방");
				
				CodeSubVO codeSubDB = new CodeSubVO();

				//라벨구분
				codeSubDB.setGROUP_CD("LABEL_TP");			
				List<CodeSubVO> list_label_tp = codeSubService.selectDatas(codeSubDB);
				
				//특수탕전
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				Map<String, List<String>> map_special_tp = codeSubService.selectDatasMapList(codeSubDB);
				
				mav.addObject("LIST_LABEL_TP", list_label_tp);
				mav.addObject("MAP_SPECIAL_TP", map_special_tp);	
			}
			
			mav.addObject("VO", productDB);		
			mav.addObject("RECIPE", recipeDB);
			mav.addObject("LIST_RECIPE", list_recipe);
			mav.addObject("PRICE", hanpureService.selectData(new HanpureVO()));
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}	
	
	@RequestMapping(value = "/product/delivery", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView delivery(HttpServletRequest request, HttpSession session, HttpServletResponse response, String PRODUCT_SEQ, String ORDER_QNTT,
			String DRUG_LABEL_TP, String DRUG_LABEL_TEXT, String DELIVERY_LABEL_TP, String DELIVERY_LABEL_TEXT) {		
		Logger.info();	
		
		if(CommonUtil.isNull(PRODUCT_SEQ)) return CommonUtil.Go404();		
		if(CommonUtil.isNull(ORDER_QNTT)) return CommonUtil.Go404();
		
		if(CommonUtil.isNull(DRUG_LABEL_TP)) DRUG_LABEL_TP = "001";
		if(CommonUtil.isNull(DRUG_LABEL_TEXT)) DRUG_LABEL_TEXT = "";
		if(CommonUtil.isNull(DELIVERY_LABEL_TP)) DELIVERY_LABEL_TP = "001";
		if(CommonUtil.isNull(DELIVERY_LABEL_TEXT)) DELIVERY_LABEL_TEXT = "";
		
		
		try {		
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/product/write_delivery");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", "상용처방");			
			mav.addObject("SESSION", SESSION);	
			mav.addObject("PRODUCT_SEQ", PRODUCT_SEQ);
			mav.addObject("ORDER_QNTT", ORDER_QNTT);
			
			mav.addObject("DRUG_LABEL_TP", DRUG_LABEL_TP);
			mav.addObject("DRUG_LABEL_TEXT", DRUG_LABEL_TEXT);
			mav.addObject("DELIVERY_LABEL_TP", DELIVERY_LABEL_TP);
			mav.addObject("DELIVERY_LABEL_TEXT", DELIVERY_LABEL_TEXT);
			
			mav.addObject("HANPURE", hanpureService.selectData(new HanpureVO()));	
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}	
	
	
	@ResponseBody
	@RequestMapping(value = "/product/delivery", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String delivery(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getPRODUCT_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");		
		if(CommonUtil.isNull(param.getORDER_QNTT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 002");
		
		if(!CommonUtil.isNumber(param.getPRODUCT_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");		
		
		//상품 SEQ
		//수량
		//라벨 4종
		
		if(CommonUtil.isNull(param.getSEND_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");		
		if(CommonUtil.isNull(param.getSEND_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");
		if(CommonUtil.isNull(param.getSEND_TEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
		if(CommonUtil.isNull(param.getSEND_MOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
		if(CommonUtil.isNull(param.getSEND_ZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");
		if(CommonUtil.isNull(param.getSEND_ADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 010");
		if(CommonUtil.isNull(param.getSEND_ADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 011");
		
		if(CommonUtil.isNull(param.getRECV_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");
		if(CommonUtil.isNull(param.getRECV_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 013");
		if(CommonUtil.isNull(param.getRECV_TEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 014");
		if(CommonUtil.isNull(param.getRECV_MOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 015");
		if(CommonUtil.isNull(param.getRECV_ZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 016");
		if(CommonUtil.isNull(param.getRECV_ADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 017");
		if(CommonUtil.isNull(param.getRECV_ADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 018");
		
		if(CommonUtil.isNull(param.getDELIVERY_DT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 019");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 020");		
			}
			
			//상품확인
			ProductVO productDB = new ProductVO();
			productDB.setPRODUCT_SEQ(param.getPRODUCT_SEQ());
			productDB = productService.selectData(productDB);
			if(productDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 021");	
			
			//레시피 정보
			String RECIPE_SEQ = productDB.getRECIPE_SEQ();
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(RECIPE_SEQ);
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 022");	
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(RECIPE_SEQ);
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);						
			
			String ORDER_CD = issueService.createCode("O");
			param.setORDER_CD(ORDER_CD);
			
			param.setPRODUCT_SEQ(param.getPRODUCT_SEQ());
			param.setORDER_QNTT(param.getORDER_QNTT());
			
			param.setSAVE_TEMP("N");
			param.setORDER_TP(productDB.getPRODUCT_TP());
			param.setRECIPE_TP("001");		//탕전
			param.setORDER_PATH("001");		//주문하기
			param.setHOSPITAL_ID(SESSION.getID());	
			param.setORDER_DT(CommonUtil.getDateTime());
			param.setORDER_NM(productDB.getPRODUCT_NM());	
			
			String ORDER_QNTT = param.getORDER_QNTT();			
			int nAmount = CommonUtil.parseInt(ORDER_QNTT) * CommonUtil.parseInt(productDB.getAMOUNT());
			
			param.setORDER_AMOUNT(String.valueOf(nAmount));			
			param.setCREATED_BY(SESSION.getID());
			param.setUPDATED_BY(SESSION.getID());				
			
			param.setDELIVERY_NM("한의사랑");
			orderService.insertData(param);
			
			String ORDER_SEQ = param.getORDER_SEQ();			
			
			recipeDB.setORDER_SEQ(ORDER_SEQ);
			recipeDB.setDRUG_LABEL_TP(param.getDRUG_LABEL_TP());
			recipeDB.setDRUG_LABEL_TEXT(param.getDRUG_LABEL_TEXT());
			recipeDB.setDELIVERY_LABEL_TP(param.getDELIVERY_LABEL_TP());
			recipeDB.setDELIVERY_LABEL_TEXT(param.getDELIVERY_LABEL_TEXT());
			recipeDB.setCREATED_BY(SESSION.getID());
			recipeDB.setUPDATED_BY(SESSION.getID());	
			recipeService.insertData(recipeDB);
			
			RECIPE_SEQ = recipeDB.getRECIPE_SEQ();
			
			for(int i = 0; i < list_recipe.size(); i++) {
				RecipeInVO recipeInDB = list_recipe.get(i);
				recipeInDB.setRECIPE_SEQ(RECIPE_SEQ);
				recipeInService.insertData(recipeInDB);
			}			
			
			mallOrderService.syncMall(ORDER_SEQ);
			
			/*
			param2.setORDER_SEQ(ORDER_SEQ);
			param2.setHOSPITAL_ID(SESSION.getID());
			param2.setPRODUCT_NM(productDB.getPRODUCT_NM());
			param2.setPRICE(productDB.getAMOUNT());
			param2.setORDER_QNTT(ORDER_QNTT);
			param2.setAMOUNT(String.valueOf(nAmount));
			
			productOrderService.insertData(param2);
			*/			
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
}
