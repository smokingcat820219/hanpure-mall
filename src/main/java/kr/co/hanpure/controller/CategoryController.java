package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.category.CategoryService;
import kr.co.hanpure.database.category.CategoryVO;

@Controller
public class CategoryController {		
	@Autowired
	private CategoryService categoryService;
		
	@ResponseBody
	@RequestMapping(value = "/category/select", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String select(HttpServletRequest request, HttpSession session, HttpServletResponse response, CategoryVO param) {		
		Logger.info();		
		
		if(CommonUtil.isNull(param.getPRODUCT_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		//if(CommonUtil.isNull(param.getP_CODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {			
			List<CategoryVO> list = categoryService.selectDatas(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));

			return data.toString();	
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());	
		}			
	}
}
