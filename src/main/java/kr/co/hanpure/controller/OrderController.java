package kr.co.hanpure.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.bom_in.BomInService;
import kr.co.hanpure.database.bom_in.BomInVO;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.issue.IssueService;
import kr.co.hanpure.database.order.OrderService;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.database.product_option.ProductOptionService;
import kr.co.hanpure.database.product_order.ProductOrderService;
import kr.co.hanpure.database.promise.PromiseService;
import kr.co.hanpure.database.promise_order.PromiseOrderService;
import kr.co.hanpure.database.promise_order.PromiseOrderVO;
import kr.co.hanpure.database.recipe.RecipeService;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.database.recipein.RecipeInService;
import kr.co.hanpure.database.recipein.RecipeInVO;
import kr.co.hanpure.database3.mall_order.MallOrderService;
import kr.co.hanpure.excel.ExcelOrderList;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;

@Controller
public class OrderController {	
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private HospitalService hospitalService;	
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private IssueService issueService;
	
	@Autowired
	private RecipeInService recipeInService;	
	
	@Autowired
	private PromiseOrderService promiseOrderService;
	
	@Autowired
	private ProductOrderService productOrderService;
	
	@Autowired
	private ProductOptionService productOptionService;
	
	@Autowired
	private PromiseService promiseService;
	
	@Autowired
	private BomInService bomInService;	
	
	@Autowired
	private MallOrderService mallOrderService;	
	
	private String DEPTH1 = "처방관리";
	private String DEPTH2 = "주문내역조회";
	
	@RequestMapping(value = "/order/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list_order(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//처방구분
			codeSubDB.setGROUP_CD("ORDER_TP");			
			List<CodeSubVO> list_order_tp = codeSubService.selectDatas(codeSubDB);
			
			//주문상태
			codeSubDB.setGROUP_CD("ORDER_STATUS_TP");			
			List<CodeSubVO> list_order_status_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/order/list_order");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);			
			mav.addObject("LIST_ORDER_TP", list_order_tp);
			mav.addObject("LIST_ORDER_STATUS_TP", list_order_status_tp);
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/order/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPageTemp(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}			
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			int nTotalItemCount = orderService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<OrderVO> list = orderService.selectDataList(param);			
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/order/excel/download", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setPage(1);
			
			int nTotalItemCount = orderService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			param.setItemPerPage(nTotalItemCount);
			List<OrderVO> list = orderService.selectDataList(param);
			
			ModelAndView mav = new ModelAndView(new ExcelOrderList());
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/order/toMyrecipe", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String toMyrecipe(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getRECIPE_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}		
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(param.getRECIPE_SEQ());
			recipeDB = recipeService.selectDataOne(recipeDB);
			if(recipeDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
			
			String RECIPE_CD = issueService.createCode("R");
			
			recipeDB.setRECIPE_CD(RECIPE_CD);
			recipeDB.setPATIENT_SEQ("0");
			recipeDB.setDOCTOR("");
			recipeDB.setINFLOW("");
			recipeDB.setORDER_SEQ("0");
			recipeDB.setPRIVATE_YN("Y");
			recipeDB.setSAVE_TEMP("0");
			
			recipeDB.setREMARK1("");
			recipeDB.setREMARK2("");
			recipeDB.setREMARK3("");
			recipeDB.setREMARK4("");
			recipeDB.setREMARK5("");
			recipeDB.setREMARK6("");
			recipeDB.setREMARK7("");
			recipeDB.setREMARK8("");
			recipeDB.setREMARK9("");
			recipeDB.setREMARK10("");
			recipeDB.setREMARK11("");
			
			recipeDB.setPRICE1("0");
			recipeDB.setPRICE2("0");
			recipeDB.setPRICE3("0");
			recipeDB.setPRICE4("0");
			recipeDB.setPRICE5("0");
			recipeDB.setPRICE6("0");
			recipeDB.setPRICE7("0");
			recipeDB.setPRICE8("0");
			recipeDB.setPRICE9("0");
			recipeDB.setPRICE10("0");
			recipeDB.setPRICE11("0");
			
			recipeDB.setHOWTOMAKE(null);
			recipeDB.setHOWTOEAT(null);
			
			recipeDB.setDRUG_LABEL_TP("");
			recipeDB.setDRUG_LABEL_TEXT("");
			recipeDB.setDELIVERY_LABEL_TP("");
			recipeDB.setDELIVERY_LABEL_TEXT("");
			
			recipeDB.setCREATED_BY(SESSION.getID());
			recipeDB.setUPDATED_BY(SESSION.getID());
			recipeService.insertData(recipeDB);
			
			String RECIPE_SEQ = recipeDB.getRECIPE_SEQ();
			for(int i = 0; i < list_recipe.size(); i++) {
				RecipeInVO recipeInDB = list_recipe.get(i);
				recipeInDB.setRECIPE_SEQ(RECIPE_SEQ);
				
				recipeInService.insertData(recipeInDB);
			}
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/order/insurance_print", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView insurance_print(HttpServletRequest request, HttpSession session, HttpServletResponse response, String ORDER_SEQ) {		
		Logger.info();		
		
		try {				
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(ORDER_SEQ);
			orderDB = orderService.selectData(orderDB);
			if(orderDB == null) return CommonUtil.Go404();
			if(!orderDB.getHOSPITAL_ID().equals(orderDB.getHOSPITAL_ID())) return CommonUtil.Go404();
			
			if(!orderDB.getORDER_TP().equals("004")) {
				return CommonUtil.Go404();
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) return CommonUtil.Go404();
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
			
			int nDrugCnt = list_recipe.size();
			
			/*
			int nEnd = 10 - list_recipe.size();
			for(int i = 0; i < nEnd; i++) {
				RecipeInVO recipeIn = new RecipeInVO();
				recipeIn.setDRUG_NM(" ");
				recipeIn.setTOTAL_QNTT("");
				recipeIn.setORIGIN("");
				
				list_recipe.add(recipeIn);
			}
			*/
			
			CodeSubVO codeSubDB = new CodeSubVO();
			codeSubDB.setUSE_YN("Y");
			
			//특수탕전
			codeSubDB.setGROUP_CD("SPECIAL_TP");			
			Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/order/insurance_print");
			mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("ORDER", orderDB);
			mav.addObject("RECIPE", recipeDB);
			mav.addObject("LIST_RECIPE", list_recipe);
			mav.addObject("DRUG_CNT", nDrugCnt);
			mav.addObject("MAP_SPECIAL_TP", map_special_tp);			
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/order/insurance_print2", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView insurance_print2(HttpServletRequest request, HttpSession session, HttpServletResponse response, String ORDER_SEQ) {		
		Logger.info();		
		
		try {				
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(ORDER_SEQ);
			orderDB = orderService.selectData(orderDB);
			if(orderDB == null) return CommonUtil.Go404();
			if(!orderDB.getHOSPITAL_ID().equals(orderDB.getHOSPITAL_ID())) return CommonUtil.Go404();
			
			if(!orderDB.getORDER_TP().equals("004")) {
				return CommonUtil.Go404();
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) return CommonUtil.Go404();
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
			
			int nDrugCnt = list_recipe.size();
			
			/*
			int nEnd = 10 - list_recipe.size();
			for(int i = 0; i < nEnd; i++) {
				RecipeInVO recipeIn = new RecipeInVO();
				recipeIn.setDRUG_NM("");
				recipeIn.setTOTAL_QNTT("");
				recipeIn.setORIGIN("");
				
				list_recipe.add(recipeIn);
			}
			*/
			
			CodeSubVO codeSubDB = new CodeSubVO();
			codeSubDB.setUSE_YN("Y");
			
			//특수탕전
			codeSubDB.setGROUP_CD("SPECIAL_TP");			
			Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/order/insurance_print2");
			mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("ORDER", orderDB);
			mav.addObject("RECIPE", recipeDB);
			mav.addObject("LIST_RECIPE", list_recipe);
			mav.addObject("DRUG_CNT", nDrugCnt);
			mav.addObject("MAP_SPECIAL_TP", map_special_tp);
			
			try {
                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
                barcode.setDrawingText(false);
                barcode.setBarHeight(40);
                barcode.setBarWidth(1);

                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "jpg", baos);
                byte[] bytes = baos.toByteArray();

                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
            }catch(Exception e) {
                e.printStackTrace();
            }
			
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/order/print", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView print(HttpServletRequest request, HttpSession session, HttpServletResponse response, String ORDER_SEQ) {		
		Logger.info();		
		
		try {				
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}	
			
			TagLibDisplay tagLibDisplay = new TagLibDisplay();
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(ORDER_SEQ);
			orderDB = orderService.selectData(orderDB);
			if(orderDB == null) return CommonUtil.Go404();
			if(!orderDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.Go404();
			
			if(orderDB.getORDER_TP().equals("001")) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB == null) return CommonUtil.Go404();				
				
				String howtoeat = recipeDB.getHOWTOEAT();
				if(!CommonUtil.isNull(howtoeat)) {
					howtoeat = tagLibDisplay.getEditor(howtoeat);
					howtoeat = CommonUtil.RemoveTag(howtoeat);					
					howtoeat = howtoeat.replaceAll(" ", "");
					recipeDB.setHOWTOEAT(howtoeat);
				}			
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				int nDrugCnt = list_recipe.size();
				
				int nEnd = 39 - list_recipe.size();
				for(int i = 0; i < nEnd; i++) {
					RecipeInVO recipeIn = new RecipeInVO();
					recipeIn.setDRUG_NM("");
					recipeIn.setTOTAL_QNTT("");
					
					list_recipe.add(recipeIn);
				}
				
				CodeSubVO codeSubDB = new CodeSubVO();
				codeSubDB.setUSE_YN("Y");
				
				//특수탕전
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
				
				ModelAndView mav = new ModelAndView("/order/print_001");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("DRUG_CNT", nDrugCnt);
				mav.addObject("MAP_SPECIAL_TP", map_special_tp);
				
				{
					String bom_cd = recipeDB.getPACKAGE1();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM1", list);
						
						System.out.println("LIST_BOM1 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE2();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM2", list);
						
						System.out.println("LIST_BOM2 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE3();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM3", list);
						
						System.out.println("LIST_BOM3 : " + list.size());
					}
				}
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
				
				return mav;
			}
			else if(orderDB.getORDER_TP().equals("002")) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB == null) return CommonUtil.Go404();
				
				String howtoeat = recipeDB.getHOWTOEAT();
				if(!CommonUtil.isNull(howtoeat)) {
					howtoeat = tagLibDisplay.getEditor(howtoeat);
					howtoeat = CommonUtil.RemoveTag(howtoeat);					
					howtoeat = howtoeat.replaceAll(" ", "");
					recipeDB.setHOWTOEAT(howtoeat);
				}	
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
								
				int nDrugCnt = list_recipe.size();
				
				int nEnd = 39 - list_recipe.size();
				for(int i = 0; i < nEnd; i++) {
					RecipeInVO recipeIn = new RecipeInVO();
					recipeIn.setDRUG_NM("");
					recipeIn.setTOTAL_QNTT("");
					
					list_recipe.add(recipeIn);
				}
								
				CodeSubVO codeSubDB = new CodeSubVO();
				codeSubDB.setUSE_YN("Y");
				
				//제형
				codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
				Map<String, String> map_circle_size = codeSubService.selectDatasMap(codeSubDB);
				
				//부형제
				codeSubDB.setGROUP_CD("DOUGH_TP");			
				Map<String, String> map_dough_tp = codeSubService.selectDatasMap(codeSubDB);
				
				//분말도
				codeSubDB.setGROUP_CD("POWDER_TP");			
				Map<String, String> map_powder_tp = codeSubService.selectDatasMap(codeSubDB);
				
				//농축
				codeSubDB.setGROUP_CD("PRESS_TP");			
				Map<String, String> map_press_tp = codeSubService.selectDatasMap(codeSubDB);
				
				ModelAndView mav = new ModelAndView("/order/print_002");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("DRUG_CNT", nDrugCnt);
				mav.addObject("MAP_CIRCLE_SIZE", map_circle_size);
				mav.addObject("MAP_DOUGH_TP", map_dough_tp);
				mav.addObject("MAP_POWDER_TP", map_powder_tp);
				mav.addObject("MAP_PRESS_TP", map_press_tp);				
				
				{
					String bom_cd = recipeDB.getPACKAGE1();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM1", list);
						
						System.out.println("LIST_BOM1 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE2();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM2", list);
						
						System.out.println("LIST_BOM2 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE3();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM3", list);
						
						System.out.println("LIST_BOM3 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE4();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM4", list);
						
						System.out.println("LIST_BOM4 : " + list.size());
					}
				}
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
				
				return mav;
			}
			else if(orderDB.getORDER_TP().equals("003")) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB == null) return CommonUtil.Go404();
				
				String howtoeat = recipeDB.getHOWTOEAT();
				if(!CommonUtil.isNull(howtoeat)) {
					howtoeat = tagLibDisplay.getEditor(howtoeat);
					howtoeat = CommonUtil.RemoveTag(howtoeat);					
					howtoeat = howtoeat.replaceAll(" ", "");
					recipeDB.setHOWTOEAT(howtoeat);
				}	
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				int nDrugCnt = list_recipe.size();
				
				int nEnd = 39 - list_recipe.size();
				for(int i = 0; i < nEnd; i++) {
					RecipeInVO recipeIn = new RecipeInVO();
					recipeIn.setDRUG_NM("");
					recipeIn.setTOTAL_QNTT("");
					
					list_recipe.add(recipeIn);
				}
				
				ModelAndView mav = new ModelAndView("/order/print_003");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("DRUG_CNT", nDrugCnt);
				
				{
					String bom_cd = recipeDB.getPACKAGE1();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM1", list);
						
						System.out.println("LIST_BOM1 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE2();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM2", list);
						
						System.out.println("LIST_BOM2 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE3();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM3", list);
						
						System.out.println("LIST_BOM3 : " + list.size());
					}
				}
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
				
				return mav;
			}
			else if(orderDB.getORDER_TP().equals("004")) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB == null) return CommonUtil.Go404();
				
				String howtoeat = recipeDB.getHOWTOEAT();
				if(!CommonUtil.isNull(howtoeat)) {
					howtoeat = tagLibDisplay.getEditor(howtoeat);
					howtoeat = CommonUtil.RemoveTag(howtoeat);					
					howtoeat = howtoeat.replaceAll(" ", "");
					recipeDB.setHOWTOEAT(howtoeat);
				}	
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				int nDrugCnt = list_recipe.size();
				
				int nEnd = 39 - list_recipe.size();
				for(int i = 0; i < nEnd; i++) {
					RecipeInVO recipeIn = new RecipeInVO();
					recipeIn.setDRUG_NM("");
					recipeIn.setTOTAL_QNTT("");
					
					list_recipe.add(recipeIn);
				}
				
				CodeSubVO codeSubDB = new CodeSubVO();
				codeSubDB.setUSE_YN("Y");
				
				//특수탕전
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
				
				ModelAndView mav = new ModelAndView("/order/print_004");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("DRUG_CNT", nDrugCnt);
				mav.addObject("MAP_SPECIAL_TP", map_special_tp);
				
				{
					String bom_cd = recipeDB.getPACKAGE1();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM1", list);
						
						System.out.println("LIST_BOM1 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE2();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM2", list);
						
						System.out.println("LIST_BOM2 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE3();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM3", list);
						
						System.out.println("LIST_BOM3 : " + list.size());
					}
				}
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
				
				return mav;
			}			
			else if(orderDB.getORDER_TP().equals("006")) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB == null) return CommonUtil.Go404();
				
				String howtoeat = recipeDB.getHOWTOEAT();
				if(!CommonUtil.isNull(howtoeat)) {
					howtoeat = tagLibDisplay.getEditor(howtoeat);
					howtoeat = CommonUtil.RemoveTag(howtoeat);					
					howtoeat = howtoeat.replaceAll(" ", "");
					recipeDB.setHOWTOEAT(howtoeat);
				}	
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				int nDrugCnt = list_recipe.size();
				
				int nEnd = 39 - list_recipe.size();
				for(int i = 0; i < nEnd; i++) {
					RecipeInVO recipeIn = new RecipeInVO();
					recipeIn.setDRUG_NM("");
					recipeIn.setTOTAL_QNTT("");
					
					list_recipe.add(recipeIn);
				}
				
				CodeSubVO codeSubDB = new CodeSubVO();
				codeSubDB.setUSE_YN("Y");
				
				//특수탕전
				codeSubDB.setGROUP_CD("SPECIAL_TP");			
				Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
				
				ModelAndView mav = new ModelAndView("/order/print_006");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("DRUG_CNT", nDrugCnt);
				mav.addObject("MAP_SPECIAL_TP", map_special_tp);
				
				{
					String bom_cd = recipeDB.getPACKAGE1();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM1", list);
						
						System.out.println("LIST_BOM1 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE2();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM2", list);
						
						System.out.println("LIST_BOM2 : " + list.size());
					}
				}
				
				{
					String bom_cd = recipeDB.getPACKAGE3();
					if(!CommonUtil.isNull(bom_cd)) {
						List<BomInVO> list = new ArrayList<BomInVO>();
						
						BomInVO bomInVO = new BomInVO();
						bomInVO.setBOM_CD(bom_cd);
						
						list = bomInService.selectItems(list, bomInVO);
						
						mav.addObject("LIST_BOM3", list);
						
						System.out.println("LIST_BOM3 : " + list.size());
					}
				}
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
				
				return mav;				
			}
			else if(orderDB.getORDER_TP().equals("007")) {
				//약속				
				PromiseOrderVO promiseOrderVO = new PromiseOrderVO();
				promiseOrderVO.setORDER_SEQ(orderDB.getORDER_SEQ());
				List<PromiseOrderVO> list_product = promiseOrderService.selectDatas(promiseOrderVO);
				
				//TagLibDisplay taglibdisplay = new TagLibDisplay();
				
				for(int i = 0; i < list_product.size(); i++) {
					PromiseOrderVO promiseOrderDB = list_product.get(i); 
					
					String BOM_CD = promiseOrderDB.getBOM_CD();
					
					{
						String bom_cd = BOM_CD;
						if(!CommonUtil.isNull(bom_cd)) {
							List<BomInVO> list = new ArrayList<BomInVO>();
							
							BomInVO bomInVO = new BomInVO();
							bomInVO.setBOM_CD(bom_cd);
							
							list = bomInService.selectItems(list, bomInVO);		
							
							list_product.get(i).setFILEPATH1("");
							list_product.get(i).setFILEPATH2("");
							list_product.get(i).setFILEPATH3("");
							list_product.get(i).setFILEPATH4("");
							list_product.get(i).setFILEPATH5("");
							list_product.get(i).setFILEPATH6("");
							list_product.get(i).setFILEPATH7("");
							list_product.get(i).setFILEPATH8("");
							list_product.get(i).setFILEPATH9("");
							list_product.get(i).setFILEPATH10("");
							
							int nIdx = 0;
							for(int z = 0; z < list.size(); z++) {
								if(!CommonUtil.isNull(list.get(z).getFILEPATH())) {
									if(nIdx == 0) {
										list_product.get(i).setFILEPATH1(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM1(list.get(z).getITEM_NM());
									}
									else if(nIdx == 1) {
										list_product.get(i).setFILEPATH2(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM2(list.get(z).getITEM_NM());
									}
									else if(nIdx == 2) {
										list_product.get(i).setFILEPATH3(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM3(list.get(z).getITEM_NM());
									}
									else if(nIdx == 3) {
										list_product.get(i).setFILEPATH4(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM4(list.get(z).getITEM_NM());
									}
									else if(nIdx == 4) {
										list_product.get(i).setFILEPATH5(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM5(list.get(z).getITEM_NM());
									}
									else if(nIdx == 5) {
										list_product.get(i).setFILEPATH6(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM6(list.get(z).getITEM_NM());
									}
									else if(nIdx == 6) {
										list_product.get(i).setFILEPATH7(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM7(list.get(z).getITEM_NM());
									}
									else if(nIdx == 7) {
										list_product.get(i).setFILEPATH8(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM8(list.get(z).getITEM_NM());
									}
									else if(nIdx == 8) {
										list_product.get(i).setFILEPATH9(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM9(list.get(z).getITEM_NM());
									}
									else if(nIdx == 9) {
										list_product.get(i).setFILEPATH10(list.get(z).getFILEPATH());
										list_product.get(i).setITEM_NM10(list.get(z).getITEM_NM());
									}
									
									nIdx++;
								}
							}
						}
					}
				}
				
				int nDrugCnt = 39;
				
				ModelAndView mav = new ModelAndView("/order/print_007");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("LIST_PRODUCT", list_product);
				mav.addObject("DRUG_CNT", nDrugCnt);
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }					
					
				return mav;
			}
			else if(orderDB.getORDER_TP().equals("008")) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setORDER_SEQ(orderDB.getORDER_SEQ());
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB == null) return CommonUtil.Go404();
				
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());
				List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
				
				int nDrugCnt = list_recipe.size();
				
				int nEnd = 39 - list_recipe.size();
				for(int i = 0; i < nEnd; i++) {
					RecipeInVO recipeIn = new RecipeInVO();
					recipeIn.setDRUG_NM("");
					recipeIn.setTOTAL_QNTT("");
					
					list_recipe.add(recipeIn);
				}
				
				ModelAndView mav = new ModelAndView("/order/print_008");
				mav.addObject("SESSION", SESSION);
				mav.addObject("DEPTH1", DEPTH1);
				mav.addObject("DEPTH2", DEPTH2);
				mav.addObject("ORDER", orderDB);
				mav.addObject("RECIPE", recipeDB);
				mav.addObject("LIST_RECIPE", list_recipe);
				mav.addObject("DRUG_CNT", nDrugCnt);
				
				try {
	                Barcode barcode = BarcodeFactory.createCode128(orderDB.getORDER_CD());
	                barcode.setDrawingText(false);
	                barcode.setBarHeight(40);
	                barcode.setBarWidth(1);

	                BufferedImage image =  BarcodeImageHandler.getImage(barcode);

	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                ImageIO.write(image, "jpg", baos);
	                byte[] bytes = baos.toByteArray();

	                String sBarcode = Base64.getEncoder().encodeToString(bytes);                
	                mav.addObject("BARCODE", "data:image/jpeg;base64," + sBarcode);
	            }catch(Exception e) {
	                e.printStackTrace();
	            }
				
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/order/print");
			mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/order/cancel", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String cancel(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getORDER_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}		
			
			OrderVO orderDB = new OrderVO();
			orderDB.setORDER_SEQ(param.getORDER_SEQ());
			orderDB = orderService.selectData(orderDB);
			if(orderDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			if(!orderDB.getORDER_STATUS().equals("N")) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "미승인 상태에서만 취소가 가능합니다.");		
			}
			
			orderDB.setORDER_STATUS("C");
			orderDB.setUPDATED_BY(SESSION.getID());
			orderService.updateData(orderDB);
			
			mallOrderService.syncMall(orderDB.getORDER_SEQ());
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
}
