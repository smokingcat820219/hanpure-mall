package kr.co.hanpure.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.category.CategoryService;
import kr.co.hanpure.database.category.CategoryVO;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.hanpure.HanpureService;
import kr.co.hanpure.database.hanpure.HanpureVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.issue.IssueService;
import kr.co.hanpure.database.order.OrderService;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.database.product.ProductService;
import kr.co.hanpure.database.product.ProductVO;
import kr.co.hanpure.database.product_option.ProductOptionService;
import kr.co.hanpure.database.product_option.ProductOptionVO;
import kr.co.hanpure.database.promise.PromiseService;
import kr.co.hanpure.database.promise.PromiseVO;
import kr.co.hanpure.database.promise_order.PromiseOrderService;
import kr.co.hanpure.database.promise_order.PromiseOrderVO;
import kr.co.hanpure.database3.mall_order.MallOrderService;

@Controller
public class PromiseController {			
	@Autowired
	private HanpureService hanpureService;
		
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private PromiseService promiseService;	
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductOptionService productOptionService;
	
	@Autowired
	private PromiseOrderService promiseOrderService;
	
	@Autowired
	private IssueService issueService;
	
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private MallOrderService mallOrderService;
	
	
	private String DEPTH1 = "간편처방";
	private String DEPTH2 = "약속처방";
	
	@RequestMapping(value = "/promise/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			String PRODUCT_TP = "005";
			
			CategoryVO categoryVO = new CategoryVO();
			categoryVO.setPRODUCT_TP(PRODUCT_TP);
			categoryVO.setP_CODE("");
			List<CategoryVO> list_category1 = categoryService.selectDatas(categoryVO);		
			
			ModelAndView mav = new ModelAndView("/promise/list_promise");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", "사전처방관리");
			mav.addObject("PRODUCT_TP", PRODUCT_TP);
			mav.addObject("LIST_CATEGORY1", list_category1);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/promise/write", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, HttpServletResponse response, PromiseVO param) {		
		Logger.info();	
				
		if(CommonUtil.isNull(param.getPRODUCT_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		if(CommonUtil.isNull(param.getPERIOD())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			//상품확인
			ProductVO productDB = new ProductVO();
			productDB.setPRODUCT_SEQ(param.getPRODUCT_SEQ());
			productDB = productService.selectData(productDB);
			if(productDB == null || !productDB.getPRODUCT_TP().equals("005")) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
						
			String make_dt = CommonUtil.setNextDay(CommonUtil.getToday(), CommonUtil.parseInt(productDB.getPERIOD_MAKE()));
			
			param.setMAKE_DT(make_dt);
			param.setSEND_DT(make_dt);
			
			param.setTURN("1");
			param.setSTATUS("I");
			
			String end_dt = CommonUtil.setNextMonth(CommonUtil.getToday(), CommonUtil.parseInt(param.getPERIOD()));
			end_dt = CommonUtil.getLastDate(end_dt);			
			
			param.setEND_DT(end_dt);
			
			param.setORDER_QNTT(productDB.getMONTHLY_QNTT());
			param.setSTOCK_QNTT(productDB.getMONTHLY_QNTT());			
			
			param.setHOSPITAL_ID(SESSION.getID());	
			param.setCREATED_BY(SESSION.getID());
			param.setUPDATED_BY(SESSION.getID());				
			
			promiseService.insertData(param);
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/promise/add", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String add(HttpServletRequest request, HttpSession session, HttpServletResponse response, PromiseVO param) {		
		Logger.info();	
				
		
		if(CommonUtil.isNull(param.getPRODUCT_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		if(CommonUtil.isNull(param.getPERIOD())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
		
			if(!CommonUtil.isNull(param.getPROMISE_SEQ())) {
				//기존 사전처방 종료
				PromiseVO promiseDB = new PromiseVO();
				promiseDB.setPROMISE_SEQ(param.getPROMISE_SEQ());
				promiseDB = promiseService.selectData(promiseDB);
				if(!promiseDB.getSTATUS().equals("E")) {
					promiseDB.setSTATUS("E");
					promiseDB.setAPPROVAL_STATUS("E");
					promiseDB.setUPDATED_BY(SESSION.getID());
					promiseService.updateData(promiseDB);
				}
			}
			
			//상품확인
			ProductVO productDB = new ProductVO();
			productDB.setPRODUCT_SEQ(param.getPRODUCT_SEQ());
			productDB = productService.selectData(productDB);
			if(productDB == null || !productDB.getPRODUCT_TP().equals("005")) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
						
			String make_dt = CommonUtil.setNextDay(CommonUtil.getToday(), CommonUtil.parseInt(productDB.getPERIOD_MAKE()));
			
			param.setMAKE_DT(make_dt);
			param.setSEND_DT(make_dt);
			
			param.setTURN("1");
			param.setSTATUS("I");
			
			String end_dt = CommonUtil.setNextMonth(CommonUtil.getToday(), CommonUtil.parseInt(param.getPERIOD()));
			end_dt = CommonUtil.getLastDate(end_dt);			
			
			param.setEND_DT(end_dt);
			
			param.setORDER_QNTT(productDB.getMONTHLY_QNTT());
			param.setSTOCK_QNTT(productDB.getMONTHLY_QNTT());			
			
			param.setHOSPITAL_ID(SESSION.getID());	
			param.setCREATED_BY(SESSION.getID());
			param.setUPDATED_BY(SESSION.getID());				
			
			promiseService.insertData(param);
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/promise/list_product", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list_product(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/promise/list_product");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", "약속처방 배송요청");
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/promise/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, PromiseVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			int nTotalItemCount = promiseService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<PromiseVO> list = promiseService.selectDataList(param);
			
			TagLibDisplay taglibdisplay = new TagLibDisplay();
			
			for(int i = 0; i < list.size(); i++) {
				String PRODUCT_OPTION = list.get(i).getPRODUCT_OPTION();				
				PRODUCT_OPTION = taglibdisplay.getEditor(PRODUCT_OPTION);				
				list.get(i).setPRODUCT_OPTION(PRODUCT_OPTION);
			}
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@ResponseBody
	@RequestMapping(value = "/promise/getProductOptions", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String getOptions(HttpServletRequest request, HttpSession session, HttpServletResponse response, PromiseVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getPROMISE_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			PromiseVO promiseDB = new PromiseVO();
			promiseDB.setPROMISE_SEQ(param.getPROMISE_SEQ());
			promiseDB = promiseService.selectData(promiseDB);
			if(promiseDB == null || !promiseDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			
			ProductOptionVO productOptionVO = new ProductOptionVO();
			productOptionVO.setPRODUCT_SEQ(promiseDB.getPRODUCT_SEQ());			
			List<ProductOptionVO> list = productOptionService.selectDatas(productOptionVO);			
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@ResponseBody
	@RequestMapping(value = "/promise/setBasket", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String setBasket(HttpServletRequest request, HttpSession session, HttpServletResponse response, PromiseVO param) {		
		Logger.info();	
		
		CommonUtil.ShowValues(param);
		
		if(param.getLIST_PROMISE_SEQ() == null || param.getLIST_PROMISE_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getLIST_BOM_CD() == null || param.getLIST_BOM_CD().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getLIST_PROMISE_SEQ().size() != param.getLIST_BOM_CD().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			session.setAttribute("LIST_PROMISE_SEQ", param.getLIST_PROMISE_SEQ());
			session.setAttribute("LIST_BOM_CD", param.getLIST_BOM_CD());
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@ResponseBody
	@RequestMapping(value = "/promise/setOrderQntt", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String setOrderQntt(HttpServletRequest request, HttpSession session, HttpServletResponse response, PromiseVO param) {		
		Logger.info();
		
		if(CommonUtil.isNull(param.getDOCTOR())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getLIST_PROMISE_SEQ() == null || param.getLIST_PROMISE_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getLIST_BOM_CD() == null || param.getLIST_BOM_CD().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getLIST_ORDER_QNTT() == null || param.getLIST_ORDER_QNTT().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		if(param.getLIST_PROMISE_SEQ().size() != param.getLIST_BOM_CD().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getLIST_PROMISE_SEQ().size() != param.getLIST_ORDER_QNTT().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			session.setAttribute("DOCTOR", param.getDOCTOR());
			session.setAttribute("LIST_PROMISE_SEQ", param.getLIST_PROMISE_SEQ());
			session.setAttribute("LIST_BOM_CD", param.getLIST_BOM_CD());
			session.setAttribute("LIST_ORDER_QNTT", param.getLIST_ORDER_QNTT());
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@RequestMapping(value = "/promise/ready", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView ready(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		List<String> LIST_PROMISE_SEQ = (List<String>)session.getAttribute("LIST_PROMISE_SEQ");
		List<String> LIST_BOM_CD = (List<String>)session.getAttribute("LIST_BOM_CD");
		
		if(LIST_PROMISE_SEQ == null || LIST_PROMISE_SEQ.size() == 0) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		if(LIST_BOM_CD == null || LIST_BOM_CD.size() == 0) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		if(LIST_PROMISE_SEQ.size() != LIST_BOM_CD.size()) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			List<String> list_doctors = new ArrayList<String>();
			String DIRECTOR = SESSION.getDIRECTOR();
			String DOCTOR = SESSION.getDOCTOR();
			
			if(!CommonUtil.isNull(DIRECTOR) && !DIRECTOR.equals("null")) {
				list_doctors.add(DIRECTOR);
			}
			
			String[] doctors = DOCTOR.split("\\|");
			for(int i = 0; i < doctors.length; i++) {
				if(!CommonUtil.isNull(doctors[i])) {
					list_doctors.add(doctors[i]);
				}
			}
			
			TagLibDisplay taglibdisplay = new TagLibDisplay();
			
			
			List<PromiseVO> list = new ArrayList<PromiseVO>();
			for(int i = 0; i < LIST_PROMISE_SEQ.size(); i++) {
				PromiseVO promiseDB = new PromiseVO();
				promiseDB.setPROMISE_SEQ(LIST_PROMISE_SEQ.get(i));				
				promiseDB = promiseService.selectData(promiseDB); 
				if(promiseDB != null) {
					String BOM_CD = LIST_BOM_CD.get(i);
					
					int nStockQntt = CommonUtil.parseInt(promiseDB.getSTOCK_QNTT());
					int nItemQntt = 0;
					int nBoxQntt = 0;
					
					String PRODUCT_OPTION = taglibdisplay.getEditor(promiseDB.getPRODUCT_OPTION());					
					
					JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
					
					for(int k = 0; k < json.size(); k++) {
						JSONObject option = (JSONObject)json.get(k);
						String bom_cd = (String)option.get("bom_cd");
						
						if(BOM_CD.equals(bom_cd)) {							
							String bom_nm = (String)option.get("bom_nm");
							String amount = (String)option.get("amount");
							String option_image = (String)option.get("option_image");
							String item_qntt = (String)option.get("item_qntt");
							
							promiseDB.setOPTION_NM(bom_nm);
							promiseDB.setAMOUNT(amount);
							promiseDB.setFILEPATH1(option_image);
							
							nItemQntt = CommonUtil.parseInt(item_qntt);
							break;
						}
					}
					
					if(nStockQntt != 0 && nItemQntt != 0) {
						nBoxQntt = nStockQntt / nItemQntt;
					}
										
					promiseDB.setORDER_QNTT(String.valueOf(nBoxQntt));
					promiseDB.setBOM_CD(BOM_CD);
					
					list.add(promiseDB);
				}				
			}
						
			ModelAndView mav = new ModelAndView("/promise/write_ready");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", "약속처방 배송요청");
			mav.addObject("TODAY", CommonUtil.getToday());
			mav.addObject("SESSION", SESSION);
			mav.addObject("LIST_DOCTORS", list_doctors);			
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	
	
	@RequestMapping(value = "/promise/order", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView order(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		String DOCTOR = (String)session.getAttribute("DOCTOR");
		List<String> LIST_PROMISE_SEQ = (List<String>)session.getAttribute("LIST_PROMISE_SEQ");
		List<String> LIST_BOM_CD = (List<String>)session.getAttribute("LIST_BOM_CD");
		List<String> LIST_ORDER_QNTT = (List<String>)session.getAttribute("LIST_ORDER_QNTT");
		
		if(LIST_PROMISE_SEQ == null || LIST_PROMISE_SEQ.size() == 0) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		if(LIST_BOM_CD == null || LIST_BOM_CD.size() == 0) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		if(LIST_ORDER_QNTT == null || LIST_ORDER_QNTT.size() == 0) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		if(LIST_PROMISE_SEQ.size() != LIST_BOM_CD.size()) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		if(LIST_PROMISE_SEQ.size() != LIST_ORDER_QNTT.size()) {
			session.setAttribute("LIST_PROMISE_SEQ", null);
			session.setAttribute("LIST_BOM_CD", null);
			
			ModelAndView mav = new ModelAndView("redirect:" + "/promise/list_product");
			return mav;		
		}
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			TagLibDisplay taglibdisplay = new TagLibDisplay();
			
			List<PromiseVO> list = new ArrayList<PromiseVO>();
			for(int i = 0; i < LIST_PROMISE_SEQ.size(); i++) {
				PromiseVO promiseDB = new PromiseVO();
				promiseDB.setPROMISE_SEQ(LIST_PROMISE_SEQ.get(i));
				promiseDB = promiseService.selectData(promiseDB); 
				if(promiseDB != null) {
					String BOM_CD = LIST_BOM_CD.get(i);
					
					promiseDB.setORDER_QNTT(LIST_ORDER_QNTT.get(i));
					
					String PRODUCT_OPTION = taglibdisplay.getEditor(promiseDB.getPRODUCT_OPTION());					
					
					JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
					
					for(int k = 0; k < json.size(); k++) {
						JSONObject option = (JSONObject)json.get(k);
						String bom_cd = (String)option.get("bom_cd");
						
						if(BOM_CD.equals(bom_cd)) {							
							String bom_nm = (String)option.get("bom_nm");
							String amount = (String)option.get("amount");
							String option_image = (String)option.get("option_image");
							String item_qntt = (String)option.get("item_qntt");
							
							promiseDB.setOPTION_NM(bom_nm);
							promiseDB.setAMOUNT(amount);
							promiseDB.setFILEPATH1(option_image);
							break;
						}
					}
					
					promiseDB.setBOM_CD(BOM_CD);
					
					list.add(promiseDB);
				}				
			}
			
			int nTotalAmount = 0;
			int nDeliveryPrice = 2500;
			
			for(int i = 0; i < list.size(); i++) {
				nTotalAmount += CommonUtil.parseInt(list.get(i).getORDER_QNTT()) * CommonUtil.parseInt(list.get(i).getAMOUNT());
			}
			
			if(nTotalAmount >= 70000) {
				nDeliveryPrice = 0;
			}
						
			ModelAndView mav = new ModelAndView("/promise/write_order");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", "약속처방 배송요청");
			mav.addObject("TODAY", CommonUtil.getToday());
			mav.addObject("SESSION", SESSION);
			mav.addObject("DOCTOR", DOCTOR);			
			mav.addObject("LIST", list);
			mav.addObject("TOTAL_AMOUNT", nTotalAmount);
			mav.addObject("DELIVERY_PRICE", nDeliveryPrice);
			mav.addObject("HANPURE", hanpureService.selectData(new HanpureVO()));
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/promise/order", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String order(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param, PromiseOrderVO param2) {		
		Logger.info();	
		
		CommonUtil.ShowValues(param);
		CommonUtil.ShowValues(param2);
		
		if(param2.getLIST_PROMISE_SEQ() == null || param2.getLIST_PROMISE_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");
		if(param2.getLIST_BOM_CD() == null || param2.getLIST_BOM_CD().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 002");
		if(param2.getLIST_ORDER_QNTT() == null || param2.getLIST_ORDER_QNTT().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");
		
		for(int i = 0; i < param2.getLIST_ORDER_QNTT().size(); i++) {
			//if(CommonUtil.parseInt(param2.getLIST_ORDER_QNTT().get(i)) <= 0) {
			//	return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "분리배송 수량은 0보다 커야 합니다.");
			//}
		}
		
		if(CommonUtil.isNull(param.getSEND_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");		
		if(CommonUtil.isNull(param.getSEND_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");
		if(CommonUtil.isNull(param.getSEND_TEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
		if(CommonUtil.isNull(param.getSEND_MOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
		if(CommonUtil.isNull(param.getSEND_ZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");
		if(CommonUtil.isNull(param.getSEND_ADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 0010");
		if(CommonUtil.isNull(param.getSEND_ADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 0011");
		
		if(param.getLIST_RECV_TP() == null || param.getLIST_RECV_TP().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");
		if(param.getLIST_RECV_NM() == null || param.getLIST_RECV_NM().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 013");
		if(param.getLIST_RECV_TEL() == null || param.getLIST_RECV_TEL().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 014");
		if(param.getLIST_RECV_MOBILE() == null || param.getLIST_RECV_MOBILE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 015");
		if(param.getLIST_RECV_ZIPCODE() == null || param.getLIST_RECV_ZIPCODE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 016");
		if(param.getLIST_RECV_ADDRESS() == null || param.getLIST_RECV_ADDRESS().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 017");
		if(param.getLIST_RECV_ADDRESS2() == null || param.getLIST_RECV_ADDRESS2().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 018");
		if(param.getLIST_MSG_MAKE() == null || param.getLIST_MSG_MAKE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 019");
		if(param.getLIST_MSG_DELIVERY() == null || param.getLIST_MSG_DELIVERY().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 020");		
		if(param.getLIST_DELIVERY_PRICE() == null || param.getLIST_DELIVERY_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 021");
		
		if(param.getLIST_RECV_TP().size() != param.getLIST_RECV_NM().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 022");
		if(param.getLIST_RECV_TP().size() != param.getLIST_RECV_TEL().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 023");
		if(param.getLIST_RECV_TP().size() != param.getLIST_RECV_MOBILE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 024");
		if(param.getLIST_RECV_TP().size() != param.getLIST_RECV_ZIPCODE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 025");
		if(param.getLIST_RECV_TP().size() != param.getLIST_RECV_ADDRESS().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 026");
		if(param.getLIST_RECV_TP().size() != param.getLIST_RECV_ADDRESS2().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 027");
		if(param.getLIST_RECV_TP().size() != param.getLIST_MSG_MAKE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 028");		
		if(param.getLIST_RECV_TP().size() != param.getLIST_MSG_DELIVERY().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 029");
		if(param.getLIST_RECV_TP().size() != param.getLIST_DELIVERY_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 030");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.LOGIN, "로그인 후 이용해 주세요.");		
			}
			
			TagLibDisplay taglibdisplay = new TagLibDisplay();
			
			//단일
			if(param.getLIST_RECV_TP().size() == 1) {
				int nTotalOrderQntt = 0;
				
				String ORDER_NM = "";
				int nTotalOrderAmount = 0;	
				
				int nDeliveryPrice = CommonUtil.parseInt(param.getLIST_DELIVERY_PRICE().get(0));
				System.out.println("nDeliveryPrice : " + nDeliveryPrice);
				
				nTotalOrderAmount += nDeliveryPrice;
				
				for(int i = 0; i < param2.getLIST_PROMISE_SEQ().size(); i++) {
					PromiseVO promiseDB = new PromiseVO();
					promiseDB.setPROMISE_SEQ(param2.getLIST_PROMISE_SEQ().get(i));
					
					promiseDB = promiseService.selectData(promiseDB);
					if(promiseDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
					
					String BOM_CD = param2.getLIST_BOM_CD().get(i);
										
					String PRODUCT_OPTION = taglibdisplay.getEditor(promiseDB.getPRODUCT_OPTION());			
					
					JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
					
					for(int k = 0; k < json.size(); k++) {
						JSONObject option = (JSONObject)json.get(k);
						String bom_cd = (String)option.get("bom_cd");
						
						if(BOM_CD.equals(bom_cd)) {							
							String bom_nm = (String)option.get("bom_nm");
							String amount = (String)option.get("amount");
							String option_image = (String)option.get("option_image");
							String item_qntt = (String)option.get("item_qntt");
							
							promiseDB.setBOM_CD(BOM_CD);
							promiseDB.setAMOUNT(amount);
							
							if(i == 0) {		
								ORDER_NM = bom_nm;
							}
							
							break;
						}
					}
						
					
					int nOrderQntt = CommonUtil.parseInt(param2.getLIST_ORDER_QNTT().get(i));	
					
					int nAmount = CommonUtil.parseInt(promiseDB.getAMOUNT());
					
					nTotalOrderAmount += nOrderQntt * nAmount;
					
					nTotalOrderQntt += nOrderQntt;
				}
				
				if(param2.getLIST_PROMISE_SEQ().size() != 1) {
					ORDER_NM += " 외 1";
				}
				
				//단일				
				String ORDER_CD = issueService.createCode("O");
				param.setORDER_CD(ORDER_CD);	
				
				param.setORDER_TP("007");	//약속처방 배송
				param.setORDER_PATH("001");
				param.setHOSPITAL_ID(SESSION.getID());	
				param.setORDER_DT(CommonUtil.getDateTime());
				param.setORDER_NM(ORDER_NM);
				param.setORDER_QNTT(String.valueOf(nTotalOrderQntt));
				param.setORDER_AMOUNT(String.valueOf(nTotalOrderAmount));
				param.setCREATED_BY(SESSION.getID());
				param.setUPDATED_BY(SESSION.getID());	
				
				param.setRECV_TP(param.getLIST_RECV_TP().get(0));
				param.setRECV_NM(param.getLIST_RECV_NM().get(0));
				param.setRECV_TEL(param.getLIST_RECV_TEL().get(0));
				param.setRECV_MOBILE(param.getLIST_RECV_MOBILE().get(0));
				param.setRECV_ZIPCODE(param.getLIST_RECV_ZIPCODE().get(0));
				param.setRECV_ADDRESS(param.getLIST_RECV_ADDRESS().get(0));
				param.setRECV_ADDRESS2(param.getLIST_RECV_ADDRESS2().get(0));
				param.setMSG_MAKE(param.getLIST_MSG_MAKE().get(0));
				param.setMSG_DELIVERY(param.getLIST_MSG_DELIVERY().get(0));
				
				param.setDELIVERY_NM("CJ택배");
				param.setDELIVERY_PRICE(String.valueOf(nDeliveryPrice));
				orderService.insertData(param);				
				
				String ORDER_SEQ = param.getORDER_SEQ();
				
				mallOrderService.syncMall(ORDER_SEQ);
				
				for(int i = 0; i < param2.getLIST_PROMISE_SEQ().size(); i++) {
					PromiseVO promiseDB = new PromiseVO();
					promiseDB.setPROMISE_SEQ(param2.getLIST_PROMISE_SEQ().get(i));
					
					promiseDB = promiseService.selectData(promiseDB);
					if(promiseDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");					
					
					String BOM_CD = param2.getLIST_BOM_CD().get(i);
					
					PromiseOrderVO promiseOrderVO = new PromiseOrderVO();
					promiseOrderVO.setORDER_SEQ(ORDER_SEQ);
					promiseOrderVO.setHOSPITAL_ID(SESSION.getID());							
					
					String PRODUCT_OPTION = taglibdisplay.getEditor(promiseDB.getPRODUCT_OPTION());			
					
					JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
					
					for(int k = 0; k < json.size(); k++) {
						JSONObject option = (JSONObject)json.get(k);
						String bom_cd = (String)option.get("bom_cd");
						
						if(BOM_CD.equals(bom_cd)) {		
							String ecount_wh_cd = (String)option.get("ecount_wh_cd");
							String bom_nm = (String)option.get("bom_nm");
							String amount = (String)option.get("amount");
							String option_image = (String)option.get("option_image");
							String item_qntt = (String)option.get("item_qntt");
							
							promiseDB.setECOUNT_WH_CD(ecount_wh_cd);
							promiseDB.setOPTION_NM(bom_nm);
							promiseDB.setBOM_CD(BOM_CD);
							promiseDB.setAMOUNT(amount);
							promiseDB.setBOX_QNTT(item_qntt);
							break;
						}
					}					
					
					promiseOrderVO.setECOUNT_WH_CD(promiseDB.getECOUNT_WH_CD());
					promiseOrderVO.setPRODUCT_NM(promiseDB.getPRODUCT_NM());
					promiseOrderVO.setOPTION_NM(promiseDB.getOPTION_NM());
					promiseOrderVO.setPROMISE_SEQ(param2.getLIST_PROMISE_SEQ().get(i));
					promiseOrderVO.setBOM_CD(param2.getLIST_BOM_CD().get(i));
					promiseOrderVO.setPRICE(promiseDB.getAMOUNT());
					
					int nAmount = CommonUtil.parseInt(promiseDB.getAMOUNT());
					int nQntt = CommonUtil.parseInt(param2.getLIST_ORDER_QNTT().get(i));
					
					promiseOrderVO.setORDER_QNTT(String.valueOf(nQntt));
					promiseOrderVO.setAMOUNT(String.valueOf(nAmount * nQntt));	
					
					promiseOrderService.insertData(promiseOrderVO);
										
					int nStockQntt = CommonUtil.parseInt(promiseDB.getSTOCK_QNTT());	
					int nBoxQntt = CommonUtil.parseInt(promiseDB.getBOX_QNTT());
					
					int nUsedQntt = nQntt * nBoxQntt;
				
					nStockQntt = nStockQntt - nUsedQntt;
					
					//구매 수량 차감
					promiseDB.setSTOCK_QNTT(String.valueOf(nStockQntt));
					promiseDB.setUPDATED_BY(SESSION.getID());
					promiseService.updateData(promiseDB);
				}
			}
			else {
				//분리배송
				for(int i = 0; i < param.getLIST_RECV_TP().size(); i++) {
					List<String> LIST_PROMISE_SEQ = (List<String>)session.getAttribute("LIST_PROMISE_SEQ");
					int ITEM_SIZE = LIST_PROMISE_SEQ.size();
					
					System.out.println("ITEM_SIZE : " + ITEM_SIZE);
					
					String ORDER_NM = "";
					int nTotalOrderAmount = 0;				
					int nEtc = 0;
					
					int nTotalOrderQntt = 0;		
					
					int nDeliveryPrice = CommonUtil.parseInt(param.getLIST_DELIVERY_PRICE().get(i));
					System.out.println("nDeliveryPrice : " + nDeliveryPrice);
					
					nTotalOrderAmount += nDeliveryPrice;					
					
					for(int k = 0; k < ITEM_SIZE; k++) {
						PromiseVO promiseDB = new PromiseVO();
						promiseDB.setPROMISE_SEQ(param2.getLIST_PROMISE_SEQ().get(k));
						
						promiseDB = promiseService.selectData(promiseDB);
						if(promiseDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");	
						
						
						String BOM_CD = param2.getLIST_BOM_CD().get(k);
						
						String PRODUCT_OPTION = taglibdisplay.getEditor(promiseDB.getPRODUCT_OPTION());			
						
						JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
						
						for(int z = 0; z < json.size(); z++) {
							JSONObject option = (JSONObject)json.get(z);
							String bom_cd = (String)option.get("bom_cd");
							
							if(BOM_CD.equals(bom_cd)) {			
								String ecount_wh_cd = (String)option.get("ecount_wh_cd");
								String bom_nm = (String)option.get("bom_nm");
								String amount = (String)option.get("amount");
								String option_image = (String)option.get("option_image");
								String item_qntt = (String)option.get("item_qntt");
								
								promiseDB.setECOUNT_WH_CD(ecount_wh_cd);
								promiseDB.setOPTION_NM(bom_nm);
								promiseDB.setBOM_CD(BOM_CD);
								promiseDB.setAMOUNT(amount);
								
								if(i == 0) {		
									ORDER_NM = bom_nm;
								}
								
								break;
							}
						}						
						
						int nOrderQntt = CommonUtil.parseInt(param2.getLIST_ORDER_QNTT().get(k));					
						int nAmount = CommonUtil.parseInt(promiseDB.getAMOUNT());
						
						
						System.out.println("nOrderQntt : " + nOrderQntt);
						System.out.println("nAmount : " + nAmount);						
						
						nTotalOrderAmount += nOrderQntt * nAmount;						
						
						if(nOrderQntt > 0) {
							nEtc++;
						}	
						
						if(ORDER_NM.equals("") && nOrderQntt > 0) {
							ORDER_NM = promiseDB.getOPTION_NM();
						}	
						
						nTotalOrderQntt += nOrderQntt;
					}
					
					if(nEtc > 1) {
						ORDER_NM += " 외 " + (nEtc - 1);
					}
					
					System.out.println("ORDER_NM : " + ORDER_NM);
					System.out.println("nTotalOrderAmount : " + nTotalOrderAmount);
					
					String ORDER_CD = issueService.createCode("O");
					
					OrderVO orderVO = new OrderVO();					
					orderVO.setORDER_CD(ORDER_CD);	
					orderVO.setORDER_TP("007");		//약속처방 배송
					orderVO.setORDER_PATH("001");
					orderVO.setHOSPITAL_ID(SESSION.getID());	
					orderVO.setORDER_DT(CommonUtil.getDateTime());
					orderVO.setORDER_NM(ORDER_NM);
					orderVO.setORDER_QNTT(String.valueOf(nTotalOrderQntt));
					orderVO.setORDER_AMOUNT(String.valueOf(nTotalOrderAmount));
					orderVO.setCREATED_BY(SESSION.getID());
					orderVO.setUPDATED_BY(SESSION.getID());	
					
					orderVO.setDOCTOR(param.getDOCTOR());
					orderVO.setSEND_TP(param.getSEND_TP());
					orderVO.setSEND_NM(param.getSEND_NM());
					orderVO.setSEND_TEL(param.getSEND_TEL());
					orderVO.setSEND_MOBILE(param.getSEND_MOBILE());
					orderVO.setSEND_ZIPCODE(param.getSEND_ZIPCODE());
					orderVO.setSEND_ADDRESS(param.getSEND_ADDRESS());
					orderVO.setSEND_ADDRESS2(param.getSEND_ADDRESS2());
					
					orderVO.setRECV_TP(param.getLIST_RECV_TP().get(i));
					orderVO.setRECV_NM(param.getLIST_RECV_NM().get(i));
					orderVO.setRECV_TEL(param.getLIST_RECV_TEL().get(i));
					orderVO.setRECV_MOBILE(param.getLIST_RECV_MOBILE().get(i));
					orderVO.setRECV_ZIPCODE(param.getLIST_RECV_ZIPCODE().get(i));
					orderVO.setRECV_ADDRESS(param.getLIST_RECV_ADDRESS().get(i));
					orderVO.setRECV_ADDRESS2(param.getLIST_RECV_ADDRESS2().get(i));
					orderVO.setMSG_MAKE(param.getLIST_MSG_MAKE().get(i));
					orderVO.setMSG_DELIVERY(param.getLIST_MSG_DELIVERY().get(i));
					
					orderVO.setDELIVERY_NM("CJ택배");
					orderVO.setDELIVERY_PRICE(String.valueOf(nDeliveryPrice));
					
					orderService.insertData(orderVO);
					
					String ORDER_SEQ = orderVO.getORDER_SEQ();
					
					mallOrderService.syncMall(ORDER_SEQ);
					
					for(int k = 0; k < ITEM_SIZE; k++) {
						PromiseVO promiseDB = new PromiseVO();
						promiseDB.setPROMISE_SEQ(param2.getLIST_PROMISE_SEQ().get(k));
						
						promiseDB = promiseService.selectData(promiseDB);
						if(promiseDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");					
						
						String BOM_CD = param2.getLIST_BOM_CD().get(k);
						
						PromiseOrderVO promiseOrderVO = new PromiseOrderVO();
						promiseOrderVO.setORDER_SEQ(ORDER_SEQ);
						promiseOrderVO.setHOSPITAL_ID(SESSION.getID());
						
						String PRODUCT_OPTION = taglibdisplay.getEditor(promiseDB.getPRODUCT_OPTION());			
						
						JSONArray json = CommonUtil.StringToJSONArray(PRODUCT_OPTION);
						
						for(int z = 0; z < json.size(); z++) {
							JSONObject option = (JSONObject)json.get(z);
							String bom_cd = (String)option.get("bom_cd");
							
							if(BOM_CD.equals(bom_cd)) {		
								String ecount_wh_cd = (String)option.get("ecount_wh_cd");
								String bom_nm = (String)option.get("bom_nm");
								String amount = (String)option.get("amount");
								String option_image = (String)option.get("option_image");
								String item_qntt = (String)option.get("item_qntt");
								
								promiseDB.setECOUNT_WH_CD(ecount_wh_cd);
								promiseDB.setOPTION_NM(bom_nm);
								promiseDB.setBOM_CD(BOM_CD);
								promiseDB.setAMOUNT(amount);
								promiseDB.setBOX_QNTT(item_qntt);
								break;
							}
						}							
						
						promiseOrderVO.setECOUNT_WH_CD(promiseDB.getECOUNT_WH_CD());
						promiseOrderVO.setPRODUCT_NM(promiseDB.getPRODUCT_NM());
						promiseOrderVO.setOPTION_NM(promiseDB.getOPTION_NM());						
						promiseOrderVO.setBOX_QNTT(promiseDB.getBOX_QNTT());
						promiseOrderVO.setPROMISE_SEQ(param2.getLIST_PROMISE_SEQ().get(k));
						promiseOrderVO.setBOM_CD(param2.getLIST_BOM_CD().get(k));
						promiseOrderVO.setPRICE(promiseDB.getAMOUNT());
						
						int nAmount = CommonUtil.parseInt(promiseDB.getAMOUNT());
						int nQntt = CommonUtil.parseInt(param2.getLIST_ORDER_QNTT().get(k));
						
						if(nQntt > 0) {
							promiseOrderVO.setORDER_QNTT(String.valueOf(nQntt));
							promiseOrderVO.setAMOUNT(String.valueOf(nAmount * nQntt));	
							
							promiseOrderService.insertData(promiseOrderVO);
												
							int nStockQntt = CommonUtil.parseInt(promiseDB.getSTOCK_QNTT());	
							int nBoxQntt = CommonUtil.parseInt(promiseDB.getBOX_QNTT());
							
							int nUsedQntt = nQntt * nBoxQntt;
						
							nStockQntt = nStockQntt - nUsedQntt;
							
							//구매 수량 차감
							promiseDB.setSTOCK_QNTT(String.valueOf(nStockQntt));
							promiseDB.setUPDATED_BY(SESSION.getID());
							promiseService.updateData(promiseDB);
						}
						
						k--;
						ITEM_SIZE--;
						
						param2.getLIST_PROMISE_SEQ().remove(0);
						param2.getLIST_BOM_CD().remove(0);
						param2.getLIST_ORDER_QNTT().remove(0);						
					}					
				}
			}
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
}
