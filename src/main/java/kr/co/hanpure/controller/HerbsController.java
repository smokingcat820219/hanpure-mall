package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.div.DivService;
import kr.co.hanpure.database.div.DivVO;
import kr.co.hanpure.database.herbs.HerbsService;
import kr.co.hanpure.database.herbs.HerbsVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;

@Controller
public class HerbsController {		
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private DivService divService;
	
	@Autowired
	private HerbsService herbsService;
	
	@Autowired
	private HospitalService hospitalService;
	
	private String DEPTH1 = "처방사전";
	private String DEPTH2 = "본초사전";
		
	@RequestMapping(value = "/herbs/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//성
			codeSubDB.setGROUP_CD("HABITUDE_TP");			
			List<CodeSubVO> list_habitude_tp = codeSubService.selectDatas(codeSubDB);
			
			//미
			codeSubDB.setGROUP_CD("TEMPER_TP");			
			List<CodeSubVO> list_temper_tp = codeSubService.selectDatas(codeSubDB);
			
			//귀경
			codeSubDB.setGROUP_CD("ORGAN_TP");			
			List<CodeSubVO> list_organ_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/herbs/list_herbs");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_HABITUDE_TP", list_habitude_tp);	
			mav.addObject("LIST_TEMPER_TP", list_temper_tp);
			mav.addObject("LIST_ORGAN_TP", list_organ_tp);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/herbs/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, HerbsVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			int nTotalItemCount = herbsService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<HerbsVO> list = herbsService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/herbs/view/{HERBS_CD}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("HERBS_CD") String HERBS_CD) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			HerbsVO herbsDB = new HerbsVO();
			herbsDB.setHERBS_CD(HERBS_CD);
			herbsDB = herbsService.selectData(herbsDB);
			if(herbsDB == null) return CommonUtil.Go404();			
			
			ModelAndView mav = new ModelAndView("/herbs/view_herbs");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("VO", herbsDB);
			
			//분류1
			List<DivVO> list_div = divService.selectDepth1();
			mav.addObject("LIST_DIV", list_div);
			
			//분류1이 있을 경우
			if(!CommonUtil.isNull(herbsDB.getDIV1_CD())) {
				DivVO divDB = new DivVO();
				divDB.setDIV1_CD(herbsDB.getDIV1_CD());
				
				List<DivVO> list_div2 = divService.selectDepth2(divDB);
				mav.addObject("LIST_DIV2", list_div2);
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			
			//성
			codeSubDB.setGROUP_CD("HABITUDE_TP");			
			List<CodeSubVO> list_habitude_tp = codeSubService.selectDatas(codeSubDB);	
			mav.addObject("LIST_HABITUDE_TP", list_habitude_tp);	
			
			//미
			codeSubDB.setGROUP_CD("TEMPER_TP");			
			List<CodeSubVO> list_temper_tp = codeSubService.selectDatas(codeSubDB);	
			mav.addObject("LIST_TEMPER_TP", list_temper_tp);	
			
			//귀경
			codeSubDB.setGROUP_CD("ORGAN_TP");			
			List<CodeSubVO> list_organ_tp = codeSubService.selectDatas(codeSubDB);	
			mav.addObject("LIST_ORGAN_TP", list_organ_tp);
			
			//독/중독
			codeSubDB.setGROUP_CD("POISON_TP");			
			List<CodeSubVO> list_poison_tp = codeSubService.selectDatas(codeSubDB);				
			mav.addObject("LIST_POISON_TP", list_poison_tp);			
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}
