package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.drug.DrugVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.order.OrderService;
import kr.co.hanpure.database.order.OrderVO;
import kr.co.hanpure.excel.ExcelClosing;
import kr.co.hanpure.excel.ExcelInsurance;


@Controller
public class ReportClosingController {	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private OrderService orderService;
	
	private String DEPTH1 = "통계 및 보고서";
	private String DEPTH2 = "처방결산";
	
	@RequestMapping(value = "/report/list_closing", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/report/list_closing");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/report/list_closing/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			List<OrderVO> list = orderService.selectStat(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));
			data.put("totalno", list.size());			
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@RequestMapping(value = "/report/list_closing/download", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, OrderVO param) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			List<OrderVO> list = orderService.selectStat(param);
			
			ModelAndView mav = new ModelAndView(new ExcelClosing());
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}
