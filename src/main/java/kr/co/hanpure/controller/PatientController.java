package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.patient.PatientService;
import kr.co.hanpure.database.patient.PatientVO;
import kr.co.hanpure.database.recipe.RecipeService;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.excel.ExcelPatient;

@Controller
public class PatientController {	
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private RecipeService recipeService;
	
	private String DEPTH1 = "처방관리";
	private String DEPTH2 = "환자정보"; 
		
	@RequestMapping(value = "/patient/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//성별
			codeSubDB.setGROUP_CD("SEX_TP");			
			List<CodeSubVO> list_sex_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/patient/list_patient");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_SEX_TP", list_sex_tp);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/patient/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, PatientVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			int nTotalItemCount = patientService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<PatientVO> list = patientService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/patient/write", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView write(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//성별
			codeSubDB.setGROUP_CD("SEX_TP");			
			List<CodeSubVO> list_sex_tp = codeSubService.selectDatas(codeSubDB);
			
			//사상체질
			codeSubDB.setGROUP_CD("PATIENT_TP");			
			List<CodeSubVO> list_patient_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/patient/write_patient");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_SEX_TP", list_sex_tp);
			mav.addObject("LIST_PATIENT_TP", list_patient_tp);
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/patient/write", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, HttpServletResponse response, PatientVO param) {		
		Logger.info();	
		
		//if(CommonUtil.isNull(param.getCHART_NO())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		if(CommonUtil.isNull(param.getNAME())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		//if(CommonUtil.isNull(param.getSEX())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		//if(CommonUtil.isNull(param.getBIRTH())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		//if(CommonUtil.isNull(param.getTEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getMOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		//if(CommonUtil.isNull(param.getREMARK())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");			
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			}
			
			if(!CommonUtil.isNull(param.getCHART_NO())) {
				PatientVO patientDB = new PatientVO();
				patientDB.setHOSPITAL_ID(SESSION.getID());
				patientDB.setCHART_NO(param.getCHART_NO());
				if(patientService.selectDatas(patientDB).size() > 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "이미 등록된 차트번호입니다.");
			}
			else {
				param.setCHART_NO("C" + CommonUtil.getDateTimeEx());
			}
			
			{
				PatientVO patientDB = new PatientVO();
				patientDB.setHOSPITAL_ID(SESSION.getID());
				patientDB.setMOBILE(param.getMOBILE());
				if(patientService.selectDatas(patientDB).size() > 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "이미 등록된 휴대폰번호입니다.");
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setCREATED_BY(SESSION.getID());
			param.setUPDATED_BY(SESSION.getID());
			
			patientService.insertData(param);
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/patient/view/{PATIENT_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("PATIENT_SEQ") String PATIENT_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}			
						
			PatientVO patientDB = new PatientVO();
			patientDB.setPATIENT_SEQ(PATIENT_SEQ);
			patientDB = patientService.selectData(patientDB);
			if(patientDB == null) return CommonUtil.Go404();
			if(!patientDB.getHOSPITAL_ID().equals(SESSION.getID())) CommonUtil.Go404();			
						
			RecipeVO recipeVO = new RecipeVO();
			recipeVO.setHOSPITAL_ID(SESSION.getID());
			recipeVO.setSAVE_TEMP("N");
			recipeVO.setPATIENT_SEQ(PATIENT_SEQ);
			List<RecipeVO> list_recipe = recipeService.selectDatas(recipeVO);
			
			ModelAndView mav = new ModelAndView("/patient/view_patient");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("VO", patientDB);
			mav.addObject("LIST_RECIPE", list_recipe);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/patient/update/{PATIENT_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView update(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("PATIENT_SEQ") String PATIENT_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
						
			PatientVO patientDB = new PatientVO();
			patientDB.setPATIENT_SEQ(PATIENT_SEQ);
			patientDB = patientService.selectData(patientDB);
			if(patientDB == null) return CommonUtil.Go404();
			if(!patientDB.getHOSPITAL_ID().equals(SESSION.getID())) CommonUtil.Go404();	
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//성별
			codeSubDB.setGROUP_CD("SEX_TP");			
			List<CodeSubVO> list_sex_tp = codeSubService.selectDatas(codeSubDB);
			
			//사상체질
			codeSubDB.setGROUP_CD("PATIENT_TP");			
			List<CodeSubVO> list_patient_tp = codeSubService.selectDatas(codeSubDB);
					
			RecipeVO recipeVO = new RecipeVO();
			recipeVO.setHOSPITAL_ID(SESSION.getID());
			recipeVO.setPATIENT_SEQ(PATIENT_SEQ);
			recipeVO.setSAVE_TEMP("N");
			List<RecipeVO> list_recipe = recipeService.selectDatas(recipeVO);
			
			ModelAndView mav = new ModelAndView("/patient/update_patient");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("VO", patientDB);
			mav.addObject("LIST_SEX_TP", list_sex_tp);
			mav.addObject("LIST_PATIENT_TP", list_patient_tp);
			mav.addObject("LIST_RECIPE", list_recipe);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/patient/update", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String update(HttpServletRequest request, HttpSession session, HttpServletResponse response, PatientVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getPATIENT_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");
		if(CommonUtil.isNull(param.getCHART_NO())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 002");		
		if(CommonUtil.isNull(param.getNAME())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");
		//if(CommonUtil.isNull(param.getSEX())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 004");
		//if(CommonUtil.isNull(param.getBIRTH())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");		
		//if(CommonUtil.isNull(param.getTEL())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");
		if(CommonUtil.isNull(param.getMOBILE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
		if(CommonUtil.isNull(param.getZIPCODE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
		if(CommonUtil.isNull(param.getADDRESS())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");
		if(CommonUtil.isNull(param.getADDRESS2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 010");
		if(CommonUtil.isNull(param.getPATIENT_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 011");			
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}
			
			if(!CommonUtil.isNull(param.getCHART_NO())) {
				PatientVO patientDB = new PatientVO();
				patientDB.setHOSPITAL_ID(SESSION.getID());
				patientDB.setCHART_NO(param.getCHART_NO());
				List<PatientVO> list = patientService.selectDatas(patientDB); 
				
				if(list.size() > 0) {
					patientDB = list.get(0);
					if(!patientDB.getPATIENT_SEQ().equals(param.getPATIENT_SEQ())) {
						return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "이미 등록된 차트번호입니다.");
					}					
				}
			}
			
			{
				PatientVO patientDB = new PatientVO();
				patientDB.setHOSPITAL_ID(SESSION.getID());
				patientDB.setMOBILE(param.getMOBILE());
				List<PatientVO> list = patientService.selectDatas(patientDB); 
				
				if(list.size() > 0) {
					patientDB = list.get(0);
					if(!patientDB.getPATIENT_SEQ().equals(param.getPATIENT_SEQ())) {
						return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "이미 등록된 휴대폰번호입니다.");
					}					
				}
			}
			
			PatientVO patientDB = new PatientVO();
			patientDB.setPATIENT_SEQ(param.getPATIENT_SEQ());
			patientDB = patientService.selectData(patientDB);
			if(patientDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			if(!patientDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");			
						
			patientDB.setPATIENT_TP(param.getPATIENT_TP());
			patientDB.setCHART_NO(param.getCHART_NO());		
			patientDB.setNAME(param.getNAME());
			patientDB.setSEX(param.getSEX());
			patientDB.setBIRTH(param.getBIRTH());		
			patientDB.setTEL(param.getTEL());
			patientDB.setMOBILE(param.getMOBILE());
			patientDB.setZIPCODE(param.getZIPCODE());
			patientDB.setADDRESS(param.getADDRESS());
			patientDB.setADDRESS2(param.getADDRESS2());
			patientDB.setREMARK(param.getREMARK());		
			patientDB.setUPDATED_BY(SESSION.getID());			
			patientService.updateData(patientDB);
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/patient/delete", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String delete(HttpServletRequest request, HttpSession session, HttpServletResponse response, PatientVO param) {		
		Logger.info();	
		
		if(param.getLIST_PATIENT_SEQ() == null || param.getLIST_PATIENT_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			}
			
			
			for(int i = 0; i < param.getLIST_PATIENT_SEQ().size(); i++) {
				PatientVO patientDB = new PatientVO();
				patientDB.setPATIENT_SEQ(param.getLIST_PATIENT_SEQ().get(i));
				patientDB = patientService.selectData(patientDB);
				if(patientDB != null) {
					//자기 환자인지 체크 필요
					if(patientDB.getHOSPITAL_ID().equals(SESSION.getID())) {
						patientDB.setDEL_YN("Y");
						patientDB.setUPDATED_BY(SESSION.getID());
						
						patientService.updateData(patientDB);
					}
				}
			}
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/patient/excel/download", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, PatientVO param) {		
		Logger.info();		
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			int nTotalItemCount = patientService.selectDataCount(param);
			param.setTotalItemCount(nTotalItemCount);
			param.setItemPerPage(nTotalItemCount);
			List<PatientVO> list = patientService.selectDataList(param);
			
			ModelAndView mav = new ModelAndView(new ExcelPatient());
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}
