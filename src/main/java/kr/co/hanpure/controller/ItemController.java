package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.item.ItemService;
import kr.co.hanpure.database.item.ItemVO;

@Controller
public class ItemController {	
	@Autowired
	private HospitalService hospitalService;	
	
	@Autowired
	private ItemService itemService;	
	
	private String DEPTH1 = "";
	private String DEPTH2 = "";
	
	@ResponseBody
	@RequestMapping(value = "/item/selectDatas", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectDatas(HttpServletRequest request, HttpSession session, HttpServletResponse response, ItemVO param) {		
		Logger.info();	
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			}
			
			//한의원 ID
			param.setHOSPITAL_ID(SESSION.getID());
			
			List<ItemVO> list = itemService.selectDatas(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
}
