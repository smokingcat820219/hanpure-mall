package kr.co.hanpure.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.conflict.ConflictService;
import kr.co.hanpure.database.conflict.ConflictVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.issue.IssueService;
import kr.co.hanpure.database.poison.PoisonService;
import kr.co.hanpure.database.poison.PoisonVO;
import kr.co.hanpure.database.recipe.RecipeService;
import kr.co.hanpure.database.recipe.RecipeVO;
import kr.co.hanpure.database.recipein.RecipeInService;
import kr.co.hanpure.database.recipein.RecipeInVO;

@Controller
public class MyRecipeController {	
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private PoisonService poisonService;
	
	@Autowired
	private ConflictService conflictService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private IssueService issueService;
	
	@Autowired
	private RecipeInService recipeInService;
	
	
	private String DEPTH1 = "처방관리";
	private String DEPTH2 = "나의처방";
	
	@RequestMapping(value = "/myrecipe/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//처방구분
			codeSubDB.setGROUP_CD("RECIPE_TP");			
			List<CodeSubVO> list_recipe_tp = codeSubService.selectDatas(codeSubDB);
			
			ModelAndView mav = new ModelAndView("/myrecipe/list_myrecipe");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_RECIPE_TP", list_recipe_tp);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/myrecipe/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setPRIVATE_YN("Y");
			
			CodeSubVO codeSubDB = new CodeSubVO();
			
			//특수탕전
			codeSubDB.setGROUP_CD("SPECIAL_TP");			
			Map<String, String> map_special_tp = codeSubService.selectDatasMap(codeSubDB);
			
			//제형
			codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
			Map<String, String> map_circle_size = codeSubService.selectDatasMap(codeSubDB);
			
			//부형제
			codeSubDB.setGROUP_CD("DOUGH_TP");			
			Map<String, String> map_dough_tp = codeSubService.selectDatasMap(codeSubDB);
			
			//연조엑스
			codeSubDB.setGROUP_CD("SOFT_EXTRACT_TP");			
			Map<String, String> map_soft_extract_tp = codeSubService.selectDatasMap(codeSubDB);
			
			//주수상반
			codeSubDB.setGROUP_CD("DRINK_TP");			
			Map<String, String> map_drink_tp = codeSubService.selectDatasMap(codeSubDB);
			
			//분말도
			codeSubDB.setGROUP_CD("POWDER_TP");			
			Map<String, String> map_powder_tp = codeSubService.selectDatasMap(codeSubDB);

			//농축
			codeSubDB.setGROUP_CD("PRESS_TP");			
			Map<String, String> map_press_tp = codeSubService.selectDatasMap(codeSubDB);
			
			
			int nTotalItemCount = recipeService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<RecipeVO> list = recipeService.selectDataList(param);
			for(int i = 0; i < list.size(); i++) {
				//탕전
				if(list.get(i).getRECIPE_TP().equals("001")) {
					//특수탕전
					String INFO4 = list.get(i).getINFO4();
					list.get(i).setINFO4(map_special_tp.get(INFO4));
					
					String INFO10 = list.get(i).getINFO10();
					list.get(i).setINFO10(map_drink_tp.get(INFO10));
				}
				//제환
				else if(list.get(i).getRECIPE_TP().equals("002")) {
					//농축
					String INFO2 = list.get(i).getINFO2();
					list.get(i).setINFO2(map_press_tp.get(INFO2));
					
					//제형
					String INFO3 = list.get(i).getINFO3();
					list.get(i).setINFO3(map_circle_size.get(INFO3));
					
					//부형제
					String INFO4 = list.get(i).getINFO4();
					list.get(i).setINFO4(map_dough_tp.get(INFO4));
					
					//분말도
					String INFO5 = list.get(i).getINFO5();
					list.get(i).setINFO5(map_powder_tp.get(INFO5));
				}
				//연조엑스
				else if(list.get(i).getRECIPE_TP().equals("003")) {
					
				}
			}
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/myrecipe/selectPagePrivate", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPagePrivate(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}			
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setPRIVATE_YN("Y");
			
			int nTotalItemCount = recipeService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<RecipeVO> list = recipeService.selectDataList(param);			
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/myrecipe/selectDetail", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectDetail(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getRECIPE_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");			
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(param.getRECIPE_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			if(!recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());			
			List<RecipeInVO> list = recipeInService.selectDatas(recipeInVO);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/myrecipe/write", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView write(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//처방구분
			codeSubDB.setGROUP_CD("RECIPE_TP");			
			List<CodeSubVO> list_recipe_tp = codeSubService.selectDatas(codeSubDB);
			
			//특수탕전
			codeSubDB.setGROUP_CD("SPECIAL_TP");			
			List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
			
			//제형
			codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
			List<CodeSubVO> list_circle_size = codeSubService.selectDatas(codeSubDB);
			
			//부형제
			codeSubDB.setGROUP_CD("DOUGH_TP");			
			List<CodeSubVO> list_dough_tp = codeSubService.selectDatas(codeSubDB);
			
			//분말도
			codeSubDB.setGROUP_CD("POWDER_TP");			
			List<CodeSubVO> list_powder_tp = codeSubService.selectDatas(codeSubDB);
			
			//농축
			codeSubDB.setGROUP_CD("PRESS_TP");			
			List<CodeSubVO> list_press_tp = codeSubService.selectDatas(codeSubDB);
			
			//독성분
			String POISON = "";
			List<PoisonVO> list_poison = poisonService.selectDatas(new PoisonVO());
			for(int i = 0; i < list_poison.size(); i++) {
				if(!POISON.equals("")) {
					POISON += ",";
				}
				
				POISON += list_poison.get(i).getHERBS_CD();
			}			
			
			List<ConflictVO> list_conflict = conflictService.selectDatas(new ConflictVO());			
			
			ModelAndView mav = new ModelAndView("/myrecipe/write_myrecipe");
			mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_RECIPE_TP", list_recipe_tp);
			mav.addObject("LIST_SPECIAL_TP", list_special_tp);
			mav.addObject("LIST_CIRCLE_SIZE", list_circle_size);
			mav.addObject("LIST_DOUGH_TP", list_dough_tp);
			mav.addObject("LIST_POWDER_TP", list_powder_tp);
			mav.addObject("LIST_PRESS_TP", list_press_tp);
			mav.addObject("POISON", POISON);
			mav.addObject("CONFLICT", CommonUtil.ListToJSONArray(list_conflict));
			
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/myrecipe/write", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO recipe, RecipeInVO recipeIn) {
		Logger.info();	
		
		if(CommonUtil.isNull(recipe.getRECIPE_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");		
		if(CommonUtil.isNull(recipe.getRECIPE_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 002");
		if(CommonUtil.isNull(recipe.getDRUG_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");
				
		if(CommonUtil.isNull(recipe.getINFO1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 004");
		if(CommonUtil.isNull(recipe.getINFO2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");		
		
		//if(CommonUtil.isNull(recipe.getPACKAGE1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		if(recipeIn.getLIST_DRUG_CD() == null || recipeIn.getLIST_DRUG_CD().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");
		if(recipeIn.getLIST_DRUG_NM() == null || recipeIn.getLIST_DRUG_NM().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
		if(recipeIn.getLIST_ORIGIN() == null || recipeIn.getLIST_ORIGIN().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
		if(recipeIn.getLIST_TURN() == null || recipeIn.getLIST_TURN().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");
		if(recipeIn.getLIST_PRICE() == null || recipeIn.getLIST_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 010");		
		if(recipeIn.getLIST_CHUP1() == null || recipeIn.getLIST_CHUP1().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 011");
		if(recipeIn.getLIST_TOTAL_QNTT() == null || recipeIn.getLIST_TOTAL_QNTT().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");
		if(recipeIn.getLIST_TOTAL_PRICE() == null || recipeIn.getLIST_TOTAL_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 013");
		
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_DRUG_NM().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 014");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_ORIGIN().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 015");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TURN().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 016");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 017");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_CHUP1().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 018");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TOTAL_QNTT().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 019");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TOTAL_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 020");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}
			
			recipe.setHOSPITAL_ID(SESSION.getID());
			recipe.setPRIVATE_YN("Y");			
			String RECIPE_CD = issueService.createCode("R");
			recipe.setRECIPE_CD(RECIPE_CD);				
			recipe.setCREATED_BY(SESSION.getID());
			recipe.setUPDATED_BY(SESSION.getID());
			
			recipe.setRECIPE_CNT(String.valueOf(recipeIn.getLIST_DRUG_CD().size()));
			
			recipeService.insertData(recipe);
			
			String RECIPE_SEQ = recipe.getRECIPE_SEQ();
			
			for(int i = 0; i < recipeIn.getLIST_DRUG_CD().size(); i++) {
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setSORT(String.valueOf(i + 1));
				recipeInVO.setRECIPE_SEQ(RECIPE_SEQ);
				recipeInVO.setDRUG_CD(recipeIn.getLIST_DRUG_CD().get(i));
				recipeInVO.setDRUG_NM(recipeIn.getLIST_DRUG_NM().get(i));				
				recipeInVO.setORIGIN(recipeIn.getLIST_ORIGIN().get(i));
				recipeInVO.setPRICE(recipeIn.getLIST_PRICE().get(i));
				recipeInVO.setCHUP1(recipeIn.getLIST_CHUP1().get(i));
				recipeInVO.setTURN(recipeIn.getLIST_TURN().get(i));
				recipeInVO.setTOTAL_QNTT(recipeIn.getLIST_TOTAL_QNTT().get(i));
				recipeInVO.setTOTAL_PRICE(recipeIn.getLIST_TOTAL_PRICE().get(i));
				
				recipeInService.insertData(recipeInVO);
			}				
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/myrecipe/update/{RECIPE_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView update(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("RECIPE_SEQ") String RECIPE_SEQ) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(RECIPE_SEQ);
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null) return CommonUtil.Go404();
			if(!recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.Go404();
			if(!recipeDB.getPRIVATE_YN().equals("Y")) return CommonUtil.Go404();
						
			RecipeInVO recipeInVO = new RecipeInVO();
			recipeInVO.setRECIPE_SEQ(recipeDB.getRECIPE_SEQ());			
			List<RecipeInVO> list_recipe = recipeInService.selectDatas(recipeInVO);
			
			CodeSubVO codeSubDB = new CodeSubVO();
			///처방구분
			codeSubDB.setGROUP_CD("RECIPE_TP");			
			List<CodeSubVO> list_recipe_tp = codeSubService.selectDatas(codeSubDB);
			
			//특수탕전
			codeSubDB.setGROUP_CD("SPECIAL_TP");			
			List<CodeSubVO> list_special_tp = codeSubService.selectDatas(codeSubDB);
			
			//제형
			codeSubDB.setGROUP_CD("CIRCLE_SIZE");			
			List<CodeSubVO> list_circle_size = codeSubService.selectDatas(codeSubDB);
			
			//부형제
			codeSubDB.setGROUP_CD("DOUGH_TP");			
			List<CodeSubVO> list_dough_tp = codeSubService.selectDatas(codeSubDB);
			
			//분말도
			codeSubDB.setGROUP_CD("POWDER_TP");			
			List<CodeSubVO> list_powder_tp = codeSubService.selectDatas(codeSubDB);
			
			//농축
			codeSubDB.setGROUP_CD("PRESS_TP");			
			List<CodeSubVO> list_press_tp = codeSubService.selectDatas(codeSubDB);
			
			//주수상반
			codeSubDB.setGROUP_CD("DRINK_TP");			
			Map<String, String> map_drink = codeSubService.selectDatasMap(codeSubDB);
						
			//독성분
			String POISON = "";
			List<PoisonVO> list_poison = poisonService.selectDatas(new PoisonVO());
			for(int i = 0; i < list_poison.size(); i++) {
				if(!POISON.equals("")) {
					POISON += ",";
				}
				
				POISON += list_poison.get(i).getHERBS_CD();
			}
			
			List<ConflictVO> list_conflict = conflictService.selectDatas(new ConflictVO());			
			
			ModelAndView mav = new ModelAndView("/myrecipe/update_myrecipe");
			mav.addObject("SESSION", SESSION);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("RECIPE", recipeDB);
			mav.addObject("LIST_RECIPE", list_recipe);
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_RECIPE_TP", list_recipe_tp);
			mav.addObject("LIST_SPECIAL_TP", list_special_tp);
			mav.addObject("LIST_CIRCLE_SIZE", list_circle_size);
			mav.addObject("LIST_DOUGH_TP", list_dough_tp);
			mav.addObject("LIST_POWDER_TP", list_powder_tp);
			mav.addObject("LIST_PRESS_TP", list_press_tp);
			mav.addObject("MAP_DRINK", map_drink);
			mav.addObject("POISON", POISON);
			mav.addObject("CONFLICT", CommonUtil.ListToJSONArray(list_conflict));
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/myrecipe/update", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String update(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO recipe, RecipeInVO recipeIn) {		
		Logger.info();	
		
		if(CommonUtil.isNull(recipe.getRECIPE_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 001");
		if(CommonUtil.isNull(recipe.getRECIPE_TP())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 002");		
		if(CommonUtil.isNull(recipe.getRECIPE_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 003");
		if(CommonUtil.isNull(recipe.getDRUG_NM())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 004");
				
		if(CommonUtil.isNull(recipe.getINFO1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 005");
		if(CommonUtil.isNull(recipe.getINFO2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 006");		
		
		if(CommonUtil.isNull(recipe.getRE_HOTPOT(), new String[] {"Y", "N"})) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 007");
		
		//if(CommonUtil.isNull(recipe.getPACKAGE1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		
		if(recipeIn.getLIST_DRUG_CD() == null || recipeIn.getLIST_DRUG_CD().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 008");
		if(recipeIn.getLIST_DRUG_NM() == null || recipeIn.getLIST_DRUG_NM().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 009");
		if(recipeIn.getLIST_ORIGIN() == null || recipeIn.getLIST_ORIGIN().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 010");
		if(recipeIn.getLIST_TURN() == null || recipeIn.getLIST_TURN().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 011");
		if(recipeIn.getLIST_PRICE() == null || recipeIn.getLIST_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 012");		
		if(recipeIn.getLIST_CHUP1() == null || recipeIn.getLIST_CHUP1().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 013");
		if(recipeIn.getLIST_TOTAL_QNTT() == null || recipeIn.getLIST_TOTAL_QNTT().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 014");
		if(recipeIn.getLIST_TOTAL_PRICE() == null || recipeIn.getLIST_TOTAL_PRICE().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 015");
		
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_DRUG_NM().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 016");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_ORIGIN().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 017");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TURN().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 018");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 019");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_CHUP1().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 020");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TOTAL_QNTT().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 021");
		if(recipeIn.getLIST_DRUG_CD().size() != recipeIn.getLIST_TOTAL_PRICE().size()) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 022");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "로그인 후 이용해 주세요.");		
			}
			
			RecipeVO recipeDB = new RecipeVO();
			recipeDB.setRECIPE_SEQ(recipe.getRECIPE_SEQ());
			recipeDB = recipeService.selectData(recipeDB);
			if(recipeDB == null || !recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다. Error 023"); 
			if(!recipeDB.getPRIVATE_YN().equals("Y")) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			recipeDB.setRECIPE_TP(recipe.getRECIPE_TP());
			recipeDB.setRECIPE_NM(recipe.getRECIPE_NM());
			recipeDB.setINFO1(recipe.getINFO1());
			recipeDB.setINFO2(recipe.getINFO2());
			recipeDB.setINFO3(recipe.getINFO3());
			recipeDB.setINFO4(recipe.getINFO4());
			recipeDB.setINFO5(recipe.getINFO5());
			recipeDB.setINFO6(recipe.getINFO6());
			recipeDB.setINFO7(recipe.getINFO7());
			recipeDB.setINFO8(recipe.getINFO8());
			recipeDB.setINFO9(recipe.getINFO9());
			recipeDB.setINFO10(recipe.getINFO10());
			recipeDB.setINFO10_QNTT(recipe.getINFO10_QNTT());
			recipeDB.setINFO11(recipe.getINFO11());
			
			recipeDB.setMARKING_TP(recipe.getMARKING_TP());
			recipeDB.setMARKING_TEXT(recipe.getMARKING_TEXT());
			
			recipeDB.setPACKAGE1(recipe.getPACKAGE1());
			recipeDB.setPACKAGE2(recipe.getPACKAGE2());
			recipeDB.setPACKAGE3(recipe.getPACKAGE3());
			recipeDB.setPACKAGE4(recipe.getPACKAGE4());			
			recipeDB.setVACCUM(recipe.getVACCUM());	
			
			recipeDB.setRE_HOTPOT(recipe.getRE_HOTPOT());
			
			recipeDB.setUPDATED_BY(SESSION.getID());
			
			recipeDB.setRECIPE_CNT(String.valueOf(recipeIn.getLIST_DRUG_CD().size()));
			
			recipeService.updateData(recipeDB);		
			
			String RECIPE_SEQ = recipe.getRECIPE_SEQ();
			
			recipeInService.deleteDataAll(RECIPE_SEQ);
			
			for(int i = 0; i < recipeIn.getLIST_DRUG_CD().size(); i++) {
				RecipeInVO recipeInVO = new RecipeInVO();
				recipeInVO.setSORT(String.valueOf(i + 1));
				recipeInVO.setRECIPE_SEQ(RECIPE_SEQ);
				recipeInVO.setDRUG_CD(recipeIn.getLIST_DRUG_CD().get(i));
				recipeInVO.setDRUG_NM(recipeIn.getLIST_DRUG_NM().get(i));				
				recipeInVO.setORIGIN(recipeIn.getLIST_ORIGIN().get(i));
				recipeInVO.setPRICE(recipeIn.getLIST_PRICE().get(i));
				recipeInVO.setCHUP1(recipeIn.getLIST_CHUP1().get(i));
				recipeInVO.setTURN(recipeIn.getLIST_TURN().get(i));
				recipeInVO.setTOTAL_QNTT(recipeIn.getLIST_TOTAL_QNTT().get(i));
				recipeInVO.setTOTAL_PRICE(recipeIn.getLIST_TOTAL_PRICE().get(i));
				
				recipeInService.insertData(recipeInVO);
			}					
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/myrecipe/delete", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String delete(HttpServletRequest request, HttpSession session, HttpServletResponse response, RecipeVO param) {		
		Logger.info();	
		
		if(param.getLIST_RECIPE_SEQ() == null || param.getLIST_RECIPE_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {		
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			for(int i = 0; i < param.getLIST_RECIPE_SEQ().size(); i++) {
				RecipeVO recipeDB = new RecipeVO();
				recipeDB.setRECIPE_SEQ(param.getLIST_RECIPE_SEQ().get(i));
				recipeDB = recipeService.selectData(recipeDB);
				if(recipeDB != null && recipeDB.getHOSPITAL_ID().equals(SESSION.getID())) {
					if(recipeDB.getPRIVATE_YN().equals("Y")) {
						recipeDB.setDEL_YN("Y");
						recipeDB.setUPDATED_BY(SESSION.getID());
						recipeService.updateData(recipeDB);
					}
				}
			}
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
}
