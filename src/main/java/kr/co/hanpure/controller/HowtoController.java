package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.howto.HowtoService;
import kr.co.hanpure.database.howto.HowtoVO;
import kr.co.hanpure.database.howto_attach.HowtoAttachService;
import kr.co.hanpure.database.howto_attach.HowtoAttachVO;

@Controller
public class HowtoController {	
	@Autowired
	private HowtoService howtoService;
	
	@Autowired
	private HowtoAttachService howtoAttachService;
	
	@Autowired
	private HospitalService hospitalService;
	
	private String DEPTH1 = "처방관리";
	private String DEPTH2 = "조제지시 및 복용법";
	
	@RequestMapping(value = "/howto/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/howto/list_howto");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/howto/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, HowtoVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getTYPE())) {
			param.setTYPE("M");
		}
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");	
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			List<HowtoVO> list = howtoService.selectDatas(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));
			data.put("totalno", list.size());	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/howto/selectFiles", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectFiles(HttpServletRequest request, HttpSession session, HttpServletResponse response, HowtoAttachVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getHOWTO_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");	
			}
			
			HowtoAttachVO howtoAttachDB = new HowtoAttachVO();
			howtoAttachDB.setHOWTO_SEQ(param.getHOWTO_SEQ());
			List<HowtoAttachVO> list = howtoAttachService.selectDatas(howtoAttachDB);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/howto/write", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView write(HttpServletRequest request, HttpSession session, HttpServletResponse response, String TYPE) {		
		Logger.info();		
		
		if(CommonUtil.isNull(TYPE, new String[] {"M", "E"})) {
			TYPE = "M";
		}
		
		try {		
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/common/pop_write_howto");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("TYPE", TYPE);
			return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/howto/write", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, HttpServletResponse response, HowtoVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getTYPE(), new String[] {"M", "E"})) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		if(CommonUtil.isNull(param.getTITLE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getCONTENT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");			
		
		try {			
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			if(param.getTYPE().equals("M")) {
				if(howtoService.selectDatas(param).size() > 19) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "조제지시는 최대 20개까지 등록이 가능합니다.\r\n기존 내용을 삭제 후 신규등록 해 주세요.");
			}
			else {
				if(howtoService.selectDatas(param).size() > 9) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "복용법은 최대 10개까지 등록이 가능합니다.\r\n기존 내용을 삭제 후 신규등록 해 주세요.");
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			param.setCREATED_BY(SESSION.getID());
			param.setUPDATED_BY(SESSION.getID());
			
			howtoService.insertData(param);
			
			String HOWTO_SEQ = param.getHOWTO_SEQ();
			Logger.debug("HOWTO_SEQ : " + HOWTO_SEQ);	
			
			for(int i = 0; i < param.getLIST_FILENAME().size(); i++) {
				HowtoAttachVO howtoAttachVO = new HowtoAttachVO();
				howtoAttachVO.setHOWTO_SEQ(HOWTO_SEQ);
				howtoAttachVO.setFILENAME(param.getLIST_FILENAME().get(i));
				howtoAttachVO.setFILEPATH(param.getLIST_FILEPATH().get(i));
				howtoAttachVO.setCREATED_BY(SESSION.getID());
				howtoAttachVO.setUPDATED_BY(SESSION.getID());
				howtoAttachService.insertData(howtoAttachVO);
			}
						
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/howto/update/{HOWTO_SEQ}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView update(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("HOWTO_SEQ") String HOWTO_SEQ) {		
		Logger.info();	
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			HowtoVO howtoDB = new HowtoVO();
			howtoDB.setHOWTO_SEQ(HOWTO_SEQ);
			howtoDB = howtoService.selectData(howtoDB);
			if(howtoDB == null) return CommonUtil.Go404();
			if(!howtoDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.Go404();
			
			HowtoAttachVO howtoAttachVO = new HowtoAttachVO();
			howtoAttachVO.setHOWTO_SEQ(HOWTO_SEQ);
			List<HowtoAttachVO> list = howtoAttachService.selectDatas(howtoAttachVO);
			
			ModelAndView mav = new ModelAndView("/common/pop_update_howto");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("VO", howtoDB);			
			mav.addObject("LIST", list);
			
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/howto/update", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String update(HttpServletRequest request, HttpSession session, HttpServletResponse response, HowtoVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getHOWTO_SEQ())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
		if(CommonUtil.isNull(param.getTITLE())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(CommonUtil.isNull(param.getCONTENT())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");			
		
		try {			
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			HowtoVO howtoDB = new HowtoVO();
			howtoDB.setHOWTO_SEQ(param.getHOWTO_SEQ());
			howtoDB = howtoService.selectData(howtoDB);
			if(howtoDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			if(!howtoDB.getHOSPITAL_ID().equals(SESSION.getID())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			
			
			howtoDB.setTITLE(param.getTITLE());
			howtoDB.setCONTENT(param.getCONTENT());
			howtoDB.setUPDATED_BY(SESSION.getID());
			
			howtoService.updateData(param);
			
			String HOWTO_SEQ = param.getHOWTO_SEQ();
			Logger.debug("HOWTO_SEQ : " + HOWTO_SEQ);	
			
			
			//현재 있는 파일 가지고 와서 비교???
			HowtoAttachVO howtoAttachDB = new HowtoAttachVO();
			howtoAttachDB.setHOWTO_SEQ(HOWTO_SEQ);
			howtoAttachService.deleteDataAll(howtoAttachDB);
			
			for(int i = 0; i < param.getLIST_FILENAME().size(); i++) {
				HowtoAttachVO howtoAttachVO = new HowtoAttachVO();
				howtoAttachVO.setHOWTO_SEQ(HOWTO_SEQ);
				howtoAttachVO.setFILENAME(param.getLIST_FILENAME().get(i));
				howtoAttachVO.setFILEPATH(param.getLIST_FILEPATH().get(i));
				howtoAttachVO.setCREATED_BY(SESSION.getID());
				howtoAttachVO.setUPDATED_BY(SESSION.getID());
				howtoAttachService.insertData(howtoAttachVO);
			}
			
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/howto/delete", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String delete(HttpServletRequest request, HttpSession session, HttpServletResponse response, HowtoVO param) {		
		Logger.info();	
		
		if(param.getLIST_HOWTO_SEQ() == null || param.getLIST_HOWTO_SEQ().size() == 0) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			for(int i = 0; i < param.getLIST_HOWTO_SEQ().size(); i++) {
				HowtoVO howtoDB = new HowtoVO();
				howtoDB.setHOWTO_SEQ(param.getLIST_HOWTO_SEQ().get(i));
				howtoDB = howtoService.selectData(howtoDB);
				if(howtoDB != null) {
					if(howtoDB.getHOSPITAL_ID().equals(SESSION.getID())) {
						howtoService.deleteData(howtoDB);
					}
				}
			}
			
			return CommonUtil.returnAjax(CommonUtil.RETURN.SUCCESS, "");
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}
	}
}
