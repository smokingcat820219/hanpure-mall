package kr.co.hanpure.controller;


import java.util.Base64;
import java.util.Base64.Encoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;

@Controller
public class SummerNoteContoller {	
	@Value("${service.file.uploadurl}")
	private String fileUploadPath;
	
	@ResponseBody
	@RequestMapping(value = "/summernote/upload", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String upload(HttpServletRequest request, HttpServletResponse response, HttpSession session, MultipartFile upfile) {	
		Logger.info();
		
		if(upfile != null && !upfile.isEmpty()) {	
			System.out.println("file OK");
			
			if(CommonUtil.isEnableFile(upfile) && CommonUtil.isImagefile(upfile.getOriginalFilename())) {				
				JSONObject data = new JSONObject();
				
				try {
					byte[] bytes = upfile.getBytes();
					Encoder encoder = Base64.getEncoder(); 
	                byte[] encodedBytes = encoder.encode(bytes);
	                
	                String sOriName = upfile.getOriginalFilename();
	                String sExt = sOriName.substring(sOriName.lastIndexOf(".") + 1); 
	                String sBase64 = String.format("data:image/%s;base64, %s", sExt, new String(encodedBytes));
	                
	                data.put("result", CommonUtil.RETURN.SUCCESS);
					data.put("url", sBase64);
					data.put("filename", sOriName);
					
				}catch(Exception e) {
					e.printStackTrace();
				}
                
                return data.toString();
			}
			
			return "";
		}
		else {
			System.out.println("file NG");
			
			return "";
		}
	}
}
