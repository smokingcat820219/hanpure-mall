package kr.co.hanpure.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.code_sub.CodeSubService;
import kr.co.hanpure.database.code_sub.CodeSubVO;
import kr.co.hanpure.database.drug.DrugService;
import kr.co.hanpure.database.drug.DrugVO;
import kr.co.hanpure.database.herbs.HerbsService;
import kr.co.hanpure.database.herbs.HerbsVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.workplace.WorkplaceService;
import kr.co.hanpure.excel.ExcelInsurance;

@Controller
public class DrugController {			
	@Autowired
	private CodeSubService codeSubService;
	
	@Autowired
	private HerbsService herbsService;
	
	@Autowired
	private DrugService drugService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private WorkplaceService workplaceService;
	
	
	private String DEPTH1 = "처방사전";
	private String DEPTH2 = "약재목록";
	
	@RequestMapping(value = "/drug/list", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			CodeSubVO codeSubDB = new CodeSubVO();
			//성
			codeSubDB.setGROUP_CD("HABITUDE_TP");			
			List<CodeSubVO> list_habitude_tp = codeSubService.selectDatas(codeSubDB);
			
			//미
			codeSubDB.setGROUP_CD("TEMPER_TP");			
			List<CodeSubVO> list_temper_tp = codeSubService.selectDatas(codeSubDB);
			
			//귀경
			codeSubDB.setGROUP_CD("ORGAN_TP");			
			List<CodeSubVO> list_organ_tp = codeSubService.selectDatas(codeSubDB);			
			
			ModelAndView mav = new ModelAndView("/drug/list_drug");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("LIST_HABITUDE_TP", list_habitude_tp);	
			mav.addObject("LIST_TEMPER_TP", list_temper_tp);
			mav.addObject("LIST_ORGAN_TP", list_organ_tp);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@RequestMapping(value = "/drug/list_insurance", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list_insurance(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}		
			
			ModelAndView mav = new ModelAndView("/drug/list_insurance");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", "첩약약재정보");			
			mav.addObject("YEAR", CommonUtil.getYear());
			mav.addObject("MONTH", CommonUtil.getMonth());
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/drug/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, DrugVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			int nTotalItemCount = drugService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<DrugVO> list = drugService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@ResponseBody
	@RequestMapping(value = "/drug/selectPageInsurance", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPageInsurance(HttpServletRequest request, HttpSession session, HttpServletResponse response, DrugVO param) {		
		Logger.info();	
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}			
			
			if(CommonUtil.isNull(param.getYear())) {
				param.setYear(CommonUtil.getYear());
			}
			
			if(CommonUtil.isNull(param.getMonth())) {
				param.setMonth(CommonUtil.getMonth());
			}
			
			String period = "";
			
			if(param.getMonth().equals("05") || param.getMonth().equals("06") || param.getMonth().equals("07")) {
				String sdate1 = String.format("%s-01-01", param.getYear());
				String edate1 = String.format("%s-03-31", param.getYear());
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("08") || param.getMonth().equals("09") || param.getMonth().equals("10")) {
				String sdate1 = String.format("%s-04-01", param.getYear());
				String edate1 = String.format("%s-06-30", param.getYear());
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("11") || param.getMonth().equals("12")) {
				String sdate1 = String.format("%4d-07-01", CommonUtil.parseInt(param.getYear()));
				String edate1 = String.format("%4d-09-30", CommonUtil.parseInt(param.getYear()));
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("01")) {
				String sdate1 = String.format("%4d-07-01", CommonUtil.parseInt(param.getYear()) - 1);
				String edate1 = String.format("%4d-09-30", CommonUtil.parseInt(param.getYear()) - 1);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("02") || param.getMonth().equals("03") || param.getMonth().equals("04")) {
				String sdate1 = String.format("%4d-10-01", CommonUtil.parseInt(param.getYear()) - 1);
				String edate1 = String.format("%4d-12-31", CommonUtil.parseInt(param.getYear()) - 1);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else {
				return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");		
			}
			
			period = String.format("%s ~ %s", param.getSdate1(), param.getEdate1());
			
			String WH_CD = workplaceService.selectWhCd();
			param.setWH_CD(WH_CD);
			
			int nTotalItemCount = drugService.selectInsuranceCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<DrugVO> list = drugService.selectInsuranceList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			data.put("period", period);
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}	
	
	@RequestMapping(value = "/drug/view/{DRUG_CD}/", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpSession session, HttpServletResponse response, @PathVariable("DRUG_CD") String DRUG_CD) {		
		Logger.info();	
		
		try {		
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			DrugVO drugDB = new DrugVO();
			drugDB.setDRUG_CD(DRUG_CD);
			drugDB = drugService.selectData(drugDB);
			if(drugDB == null) return CommonUtil.Go404();
			
			HerbsVO herbsDB = new HerbsVO();
			herbsDB.setHERBS_CD(drugDB.getHERBS_CD());
			herbsDB = herbsService.selectData(herbsDB);
			if(herbsDB == null) return CommonUtil.Go404();
						
			ModelAndView mav = new ModelAndView("/drug/view_drug");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("VO", drugDB);
			mav.addObject("HERBS", herbsDB);			
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}	
	
	
	@RequestMapping(value = "/drug/insurance/excel/download", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, DrugVO param) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			param.setPage(1);
			
			if(CommonUtil.isNull(param.getYear())) {
				param.setYear(CommonUtil.getYear());
			}
			
			if(CommonUtil.isNull(param.getMonth())) {
				param.setMonth(CommonUtil.getMonth());
			}
			
			String period = "";
			
			if(param.getMonth().equals("05") || param.getMonth().equals("06") || param.getMonth().equals("07")) {
				String sdate1 = String.format("%s-01-01", param.getYear());
				String edate1 = String.format("%s-03-31", param.getYear());
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("08") || param.getMonth().equals("09") || param.getMonth().equals("10")) {
				String sdate1 = String.format("%s-04-01", param.getYear());
				String edate1 = String.format("%s-06-30", param.getYear());
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("11") || param.getMonth().equals("12")) {
				String sdate1 = String.format("%4d-07-01", CommonUtil.parseInt(param.getYear()));
				String edate1 = String.format("%4d-09-30", CommonUtil.parseInt(param.getYear()));
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("01")) {
				String sdate1 = String.format("%4d-07-01", CommonUtil.parseInt(param.getYear()) - 1);
				String edate1 = String.format("%4d-09-30", CommonUtil.parseInt(param.getYear()) - 1);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getMonth().equals("02") || param.getMonth().equals("03") || param.getMonth().equals("04")) {
				String sdate1 = String.format("%4d-10-01", CommonUtil.parseInt(param.getYear()) - 1);
				String edate1 = String.format("%4d-12-31", CommonUtil.parseInt(param.getYear()) - 1);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else {
				return CommonUtil.Go404();
			}
			
			period = String.format("%s ~ %s", param.getSdate1(), param.getEdate1());
			
			String WH_CD = workplaceService.selectWhCd();
			param.setWH_CD(WH_CD);
			
			int nTotalItemCount = drugService.selectInsuranceCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			param.setItemPerPage(nTotalItemCount);
			List<DrugVO> list = drugService.selectInsuranceList(param);
			
			ModelAndView mav = new ModelAndView(new ExcelInsurance());
			mav.addObject("LIST", list);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}
