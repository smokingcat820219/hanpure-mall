package kr.co.hanpure.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.bom.BomService;
import kr.co.hanpure.database.bom.BomVO;
import kr.co.hanpure.database.bom_in.BomInService;
import kr.co.hanpure.database.bom_in.BomInVO;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;

@Controller
public class BomController {		
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private BomService bomService;
	
	@Autowired
	private BomInService bomInService;	
		
	@ResponseBody
	@RequestMapping(value = "/bom/selectPage", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectPage(HttpServletRequest request, HttpSession session, HttpServletResponse response, BomVO param) {		
		Logger.info();	
		
		CommonUtil.ShowValues(param);
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.LOGIN, "로그인 후 이용해 주세요.");	
			}
			
			if(!CommonUtil.isNull(param.getSearchText())) {
				String searchText = param.getSearchText();
				if(searchText.indexOf("[") > -1) {
					searchText = searchText.replaceAll("[", "[[]");
					param.setSearchText(searchText);
				}
			}
			
			int nTotalItemCount = bomService.selectDataCount(param);			
			param.setTotalItemCount(nTotalItemCount);
			List<BomVO> list = bomService.selectDataList(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));		
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("<ul>");  
			sb.append("	<li class='prev'><a href='javascript:' onclick=\"setHash(" + param.getJumpPrevPage() + ")\"><span title='이전'>&lt;</span></a></li>");		
			
			for(int i = param.getPageBegin(); i <= param.getPageEnd(); i++) {
				if(param.getPage() == i) {					
					sb.append("	<li class='on'><a href='javascript:'><span>" + i + "</span></a></li>"); 
				}
				else {						
					sb.append("	<li><a href='javascript:' onclick=\"setHash(" + i + ")\"><span>" + i + "</span></a></li>"); 
				}
			}
			
			sb.append("	<li class='next'><a href='javascript:' onclick=\"setHash(" + param.getJumpNextPage() + ")\"><span title='다음'>&gt;</span></a></li>");
			sb.append("</ul>"); 
			
			data.put("pagenation", sb.toString());
			
			int nStartNo = param.getTotalItemCount() - ((param.getPage() - 1) * param.getItemPerPage());
			data.put("startno", nStartNo);			
			data.put("totalno", nTotalItemCount);	
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/bom/selectDatas", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectDatas(HttpServletRequest request, HttpSession session, HttpServletResponse response, BomVO param) {		
		Logger.info();	
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.LOGIN, "로그인 후 이용해 주세요.");	
			}
			
			List<BomVO> list = bomService.selectDatas(param);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));					
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/bom/selectItems", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String selectItems(HttpServletRequest request, HttpSession session, HttpServletResponse response, BomInVO param) {		
		Logger.info();	
				
		if(CommonUtil.isNull(param.getBOM_CD())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.LOGIN, "로그인 후 이용해 주세요.");	
			}
			
			BomVO bomDB = new BomVO();
			bomDB.setBOM_CD(param.getBOM_CD());
			bomDB = bomService.selectData(bomDB);
			if(bomDB == null) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			List<BomInVO> list = new ArrayList<BomInVO>();
			
			BomInVO bomInVO = new BomInVO();
			bomInVO.setBOM_CD(bomDB.getBOM_CD());			
			
			list = bomInService.selectItems(list, bomInVO);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("data", CommonUtil.ObjectToJSONObject(bomDB));
			data.put("list", CommonUtil.ListToJSONArray(list));		
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
}
