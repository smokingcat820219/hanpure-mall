package kr.co.hanpure.controller;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;

/**
 * 상극알람 관리
 *
 */
@Controller
public class LedController {
	@ResponseBody
	@RequestMapping(value = "/led/getFlag", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String getFlag(String led1, String led2, String led3, String led4, String led5, String led6, String led7) {		
		Logger.info();
		
		System.out.println("led1 : " + led1);
		System.out.println("led2 : " + led2);
		System.out.println("led3 : " + led3);
		System.out.println("led4 : " + led4);
		System.out.println("led5 : " + led5);
		System.out.println("led6 : " + led6);
		System.out.println("led7 : " + led7);
		
		try {
			String LED = String.format("0%s%s%s%s%s%s%s", led7, led6, led5, led4, led3, led2, led1);
			short led_status = Short.valueOf(LED, 2);
			
			System.out.println(String.format("%d %X", led_status, led_status));
			
			byte body[] = new byte[2];
			body [0] = (byte)(led_status & 0xFF);
			body [1] = (byte)((led_status >> 8 )& 0xFF);
			
			String HEX = String.format("%X", led_status);
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("data", HEX);
			return data.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다.");	
	}
	
	/*
	@ResponseBody
	@RequestMapping(value = "/led/getBoxList", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String getBoxList() {		
		Logger.info();	
		
		try {
			String URL = "http://192.168.219.250/cgi-bin/get_boxlist.cgi";
			HttpGet httpGet = new HttpGet(URL);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			CloseableHttpResponse response = httpclient.execute(httpGet);
			
			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);
					
					result = result.replaceAll("\r\n", "");
					result = result.replaceAll("\\n", "");
					result = result.replaceAll(" ", "");
					
					System.out.println("result : " + result);
					
					int idx1 = result.indexOf("<body>");
					int idx2 = result.indexOf("</body>");
					
					result = result.substring(idx1 + 6, idx2);
					
					System.out.println("result : " + result);
					
					JSONObject json = CommonUtil.StringToJSONObject(result);
					JSONArray array = (JSONArray)json.get("nodes");
					
					
					
					JSONObject data = new JSONObject();
					data.put("result", CommonUtil.RETURN.SUCCESS);
					data.put("list", array);

					return data.toString();
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다.");	
	}	
	
	@ResponseBody
	@RequestMapping(value = "/led/ledOn", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String ledOn(LedVO param) {		
		Logger.info();	
		
		System.out.println("node_id : " + param.getNode_id());
		System.out.println("box_id : " + param.getBox_id());
		System.out.println("led1 : " + param.getLed1());
		System.out.println("led2 : " + param.getLed2());
		System.out.println("led3 : " + param.getLed3());
		System.out.println("led4 : " + param.getLed4());
		System.out.println("led5 : " + param.getLed5());
		System.out.println("led6 : " + param.getLed6());
		System.out.println("led7 : " + param.getLed7());
		
		try {
			String LED = String.format("0%s%s%s%s%s%s%s", param.getLed7(), param.getLed6(), param.getLed5(), param.getLed4(), param.getLed3(), param.getLed2(), param.getLed1());
			short led_status = Short.valueOf(LED, 2);
			
			System.out.println(String.format("%d %X", led_status, led_status));
			
			byte body[] = new byte[2];
			body [0] = (byte)(led_status & 0xFF);
			body [1] = (byte)((led_status >> 8 )& 0xFF);
			
			//String HEX = String.format("%02X%02X", body[1], body[0]);
			String HEX = String.format("%X", led_status);
			
			System.out.println("HEX : " + HEX);
			
			String URL = String.format("http://192.168.219.250/cgi-bin/led_on.cgi?node_id=%s&flag=%s", param.getNode_id(), HEX);
			HttpGet httpGet = new HttpGet(URL);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			CloseableHttpResponse response = httpclient.execute(httpGet);
			
			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);
					
					result = result.replaceAll("\r\n", "");
					result = result.replaceAll("\\n", "");
					result = result.replaceAll(" ", "");
					
					System.out.println("result : " + result);
					
					if(result.indexOf("OK") > -1) {
						JSONObject data = new JSONObject();
						data.put("result", CommonUtil.RETURN.SUCCESS);
						data.put("message", "성공");
						return data.toString();
					}
					else {
						return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다.");						
					}
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다.");	
	}
	
	@ResponseBody
	@RequestMapping(value = "/led/ledOff", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String ledOff(LedVO param) {		
		Logger.info();	
		
		System.out.println("node_id : " + param.getNode_id());
		System.out.println("box_id : " + param.getBox_id());
		System.out.println("led1 : " + param.getLed1());
		System.out.println("led2 : " + param.getLed2());
		System.out.println("led3 : " + param.getLed3());
		System.out.println("led4 : " + param.getLed4());
		System.out.println("led5 : " + param.getLed5());
		System.out.println("led6 : " + param.getLed6());
		System.out.println("led7 : " + param.getLed7());
		
		try {
			String LED = String.format("0%s%s%s%s%s%s%s", param.getLed7(), param.getLed6(), param.getLed5(), param.getLed4(), param.getLed3(), param.getLed2(), param.getLed1());
			short led_status = Short.valueOf(LED, 2);
			
			System.out.println(String.format("%X", led_status));
			
			byte body[] = new byte[2];
			body [0] = (byte)(led_status & 0xFF);
			body [1] = (byte)((led_status >> 8 )& 0xFF);
			
			String HEX = String.format("%02X%02X", body[1], body[0]);
			
			System.out.println("HEX : " + HEX);
			
			String URL = String.format("http://192.168.219.250/cgi-bin/led_off.cgi?node_id=%s&flag=%s", param.getNode_id(), HEX);
			HttpGet httpGet = new HttpGet(URL);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			CloseableHttpResponse response = httpclient.execute(httpGet);
			
			try {
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "UTF-8");
					HttpEntity entity = response.getEntity();
					EntityUtils.consume(entity);
					
					result = result.replaceAll("\r\n", "");
					result = result.replaceAll("\\n", "");
					result = result.replaceAll(" ", "");
					
					System.out.println("result : " + result);
					
					if(result.indexOf("OK") > -1) {
						JSONObject data = new JSONObject();
						data.put("result", CommonUtil.RETURN.SUCCESS);
						data.put("message", "성공");
						return data.toString();
					}
					else {
						return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다.");						
					}
				} else {
					System.out.println("Error : " + response.getStatusLine().getStatusCode());
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				response.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다.");	
	}
	*/
}
