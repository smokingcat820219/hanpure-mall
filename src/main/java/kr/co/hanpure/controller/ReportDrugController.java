package kr.co.hanpure.controller;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import framework.smokingcat.logger.Logger;
import framework.smokingcat.util.CommonUtil;
import kr.co.hanpure.database.hospital.HospitalService;
import kr.co.hanpure.database.hospital.HospitalVO;
import kr.co.hanpure.database.report.ReportService;
import kr.co.hanpure.database.report.ReportVO;
import kr.co.hanpure.excel.ExcelReportDrug;

@Controller
public class ReportDrugController {	
	@Autowired
	private HospitalService hospitalService;
	
	@Autowired
	private ReportService reportService;
	
	private String DEPTH1 = "통계 및 보고서";
	private String DEPTH2 = "분석 보고서";
	
	@RequestMapping(value = "/report/list_drug", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView list_drug(HttpServletRequest request, HttpSession session, HttpServletResponse response) {		
		Logger.info();		
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			ModelAndView mav = new ModelAndView("/report/list_drug");
			mav.addObject("DEPTH1", DEPTH1);
			mav.addObject("DEPTH2", DEPTH2);
			mav.addObject("TODAY1", CommonUtil.getYear() + "-" + CommonUtil.getMonth() + "-01");
			mav.addObject("TODAY2", CommonUtil.getToday());
			mav.addObject("YEAR", CommonUtil.getYear());
			mav.addObject("MONTH", CommonUtil.getMonth());
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/report/list_drug", produces = "text/json; charset=UTF-8", method=RequestMethod.POST)
	public String list_drug(HttpServletRequest request, HttpSession session, HttpServletResponse response, ReportVO param) {		
		Logger.info();	
		
		if(CommonUtil.isNull(param.getSORT(), new String[] {"1", "2", "3"})) {
			param.setSORT("1");
		}
		
		if(CommonUtil.isNull(param.getSearchField(), new String[] {"1", "2", "3"})) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		if(param.getSearchField().equals("1")) {	//일별
			if(CommonUtil.isNull(param.getSdate1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			if(CommonUtil.isNull(param.getEdate1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		}
		else if(param.getSearchField().equals("2")) {	//월별
			if(CommonUtil.isNull(param.getYear1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			if(CommonUtil.isNull(param.getYear2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			
			if(CommonUtil.isNull(param.getMonth1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			if(CommonUtil.isNull(param.getMonth2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");			
		}
		else if(param.getSearchField().equals("3")) {	//연별
			if(CommonUtil.isNull(param.getYear1())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
			if(CommonUtil.isNull(param.getYear2())) return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "잘못된 접근입니다.");
		}
		
		try {
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				return CommonUtil.returnAjax(CommonUtil.RETURN.LOGIN, "로그인 후 이용해 주세요.");	
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			if(param.getSearchField().equals("1")) {	//일별
					
			}
			else if(param.getSearchField().equals("2")) {	//월별
				String sdate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear1()), CommonUtil.parseInt(param.getMonth1()), 1);
				String edate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear2()), CommonUtil.parseInt(param.getMonth2()), 1);
				
				edate1 = CommonUtil.getLastDate(edate1);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getSearchField().equals("3")) {	//연별
				String sdate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear1()), 1, 1);
				String edate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear2()), 12, 31);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			
			List<ReportVO> list = reportService.selectDatasByDrug(param);	
			
			JSONObject data = new JSONObject();
			data.put("result", CommonUtil.RETURN.SUCCESS);
			data.put("list", CommonUtil.ListToJSONArray(list));
			return data.toString();					
		}catch(Exception e) {
			e.printStackTrace();
			return CommonUtil.returnAjax(CommonUtil.RETURN.FAIL, "에러가 발생 했습니다. ERROR : " + e.getMessage());		
		}	
	}
	
	@RequestMapping(value = "/report/list_drug/excel/download", produces = "text/json; charset=UTF-8", method=RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpSession session, HttpServletResponse response, ReportVO param) {		
		Logger.info();		
		
		CommonUtil.ShowValues(param);
		
		try {	
			HospitalVO SESSION = hospitalService.GetLoginSession(session);
			if(SESSION == null) {
				ModelAndView mav = new ModelAndView("back");
				return mav;
			}
			
			param.setHOSPITAL_ID(SESSION.getID());
			
			if(param.getSearchField().equals("1")) {	//일별
					
			}
			else if(param.getSearchField().equals("2")) {	//월별
				String sdate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear1()), CommonUtil.parseInt(param.getMonth1()), 1);
				String edate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear2()), CommonUtil.parseInt(param.getMonth2()), 1);
				
				edate1 = CommonUtil.getLastDate(edate1);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			else if(param.getSearchField().equals("3")) {	//연별
				String sdate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear1()), 1, 1);
				String edate1 = String.format("%04d-%02d-%02d", CommonUtil.parseInt(param.getYear2()), 12, 31);
				
				param.setSdate1(sdate1);
				param.setEdate1(edate1);
			}
			
			List<ReportVO> list = reportService.selectDatasByDrug(param);
			
			ModelAndView mav = new ModelAndView(new ExcelReportDrug());
			mav.addObject("LIST", list);
			mav.addObject("VO", param);
		   	return mav;
		}catch(Exception e) {	
			e.printStackTrace();
			return CommonUtil.GoErrorPage(e, Logger.getClassName(), CommonUtil.getMethodName());
		}
	}
}
