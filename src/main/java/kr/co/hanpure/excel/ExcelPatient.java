package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.patient.PatientVO;

public class ExcelPatient extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		TagLibDisplay taglibdisplay = new TagLibDisplay();
		
		List<PatientVO> LIST = (List<PatientVO>)model.get("LIST");
		Excel excel = new Excel(workbook);

		String excelName = String.format("환자정보");	

		String title[] = {"No", "차트번호", "환자명", "성별", "생년월일", "휴대폰", "사상체질", "주소", "최근처방일"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);		
		
		if(LIST != null) {
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();
				
				excel.setValue(0, String.valueOf(i + 1));
				excel.setValue(1, LIST.get(i).getCHART_NO());
				excel.setValue(2, LIST.get(i).getNAME());
				excel.setValue(3, LIST.get(i).getSEX_NM());
				excel.setValue(4, LIST.get(i).getBIRTH());	
				excel.setValue(5, LIST.get(i).getMOBILE());
				excel.setValue(6, taglibdisplay.getEditor(LIST.get(i).getPATIENT_TP_NM()));
				
				String address = "";
				if(LIST.get(i).getZIPCODE() != null && !LIST.get(i).getZIPCODE().equals("")) {
					address += "(" + LIST.get(i).getZIPCODE() + ")";
				}
				if(LIST.get(i).getADDRESS() != null && !LIST.get(i).getADDRESS().equals("")) {
					address += LIST.get(i).getADDRESS() + " ";
				}
				if(LIST.get(i).getADDRESS2() != null && !LIST.get(i).getADDRESS2().equals("")) {
					address += LIST.get(i).getADDRESS2();
				}
				
				excel.setValue(7, address);				
				excel.setValue(8, LIST.get(i).getRECIPE_DT());
			}
		}
		
		excel.save(response, excelName);
	}
	
}
