package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.pdata.PdataVO;

public class ExcelPdataList extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		TagLibDisplay taglibdisplay = new TagLibDisplay();
		
		List<PdataVO> LIST = (List<PdataVO>)model.get("LIST");
		Excel excel = new Excel(workbook);

		String excelName = String.format("방제사전");	

		String title[] = {"No", "출전", "방제명", "약재구성", "약미"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);	
		
		if(LIST != null) {
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();				
				    			
				excel.setValue(0, String.valueOf(i + 1));
				excel.setValue(1, taglibdisplay.getEditor(LIST.get(i).getPRE_SRC_BOOK()));
				excel.setValue(2, taglibdisplay.getEditor(LIST.get(i).getPRE_NM()));
				excel.setValue(3, taglibdisplay.getEditor(LIST.get(i).getPRE_MED()));
				excel.setValue(4, LIST.get(i).getPRE_MED_QNTT());
			}
		}
		
		excel.save(response, excelName);
	}
	
}
