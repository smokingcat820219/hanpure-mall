package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.report.ReportVO;

public class ExcelReportDrug extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		ReportVO VO = (ReportVO)model.get("VO");
		List<ReportVO> LIST = (List<ReportVO>)model.get("LIST");
		
		Excel excel = new Excel(workbook);

		String excelName = String.format("분석보고서-처방 약재 분석");	

		String title[] = {"순번", "약재명", "주문 수량", "최대수량순위", "주문 금액", "최대금액순위"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);		
		
		excel.colSpan(0, 1);
		
		TagLibDisplay tagLibDisplay = new TagLibDisplay();		
		
		if(LIST != null) {
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();
				
				excel.setValue(0, String.valueOf(i + 1));
				excel.setValue(1, tagLibDisplay.getEditor(LIST.get(i).getDRUG_NM()));
				excel.setValue(2, CommonUtil.NumberFormat(LIST.get(i).getTOTAL_QNTT()));
				excel.setValue(3, CommonUtil.NumberFormat(LIST.get(i).getRANK_TOTAL_QNTT()));
				excel.setValue(4, CommonUtil.NumberFormat(LIST.get(i).getTOTAL_PRICE()));
				excel.setValue(5, CommonUtil.NumberFormat(LIST.get(i).getRANK_TOTAL_PRICE()));
			}
		}
		
		excel.save(response, excelName);
	}
	
}
