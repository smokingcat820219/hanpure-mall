package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.drug.DrugVO;

public class ExcelInsurance extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		List<DrugVO> LIST = (List<DrugVO>)model.get("LIST");
		Excel excel = new Excel(workbook);

		String excelName = String.format("첩약 약재목록");	

		String title[] = {"No", "본초명", "주성분코드", "대표코드", "제품코드", "약재명", "원산지", "제조사", "총입고량(g)", "1g당 구입가", "구입가"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);		
		
		TagLibDisplay taglibdisplay = new TagLibDisplay();
		
				
		if(LIST != null) {
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();
				
				excel.setValue(0, String.valueOf(i + 1));
				excel.setValue(1, LIST.get(i).getHERBS_NM());
				excel.setValue(2, LIST.get(i).getINSURANCE_CODE1());
				excel.setValue(3, LIST.get(i).getINSURANCE_CODE2());
				excel.setValue(4, LIST.get(i).getINSURANCE_CODE3());
				excel.setValue(5, taglibdisplay.getEditor(LIST.get(i).getDRUG_NM2()));
				excel.setValue(6, taglibdisplay.getEditor(LIST.get(i).getORIGIN()));
				excel.setValue(7, taglibdisplay.getEditor(LIST.get(i).getMAKER()));
				
				float fPrice = CommonUtil.parseFloat(LIST.get(i).getPRICE());
				float fQntt = CommonUtil.parseFloat(LIST.get(i).getQNTT());
				float fSpec = CommonUtil.parseFloat(LIST.get(i).getSPEC());
				
				float fTotalQntt = fQntt * fSpec;
				
				
				float fGram1 = 0;
    			if(fPrice != 0 && fTotalQntt != 0) {
    				fGram1 = fPrice / fTotalQntt;
    			}	
				
				excel.setValue(8, taglibdisplay.getNumber(String.valueOf(fTotalQntt)));
				excel.setValue(9, taglibdisplay.getNumber(String.valueOf(fGram1)));
				excel.setValue(10, taglibdisplay.getNumber(String.valueOf(fPrice)));
			}
		}
		
		excel.save(response, excelName);
	}
	
}
