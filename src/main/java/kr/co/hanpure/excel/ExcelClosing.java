package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.order.OrderVO;

public class ExcelClosing extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		List<OrderVO> LIST = (List<OrderVO>)model.get("LIST");
		Excel excel = new Excel(workbook);

		String excelName = String.format("처방결산");	

		String title[] = {"번호", "구분", "상태", "처방의", "처방명", "환자명", "처방일시", "약재비", "탕전비/분말비", "특수탕전비/농축비", "향미제/제형비/스틱포장비", "앰플/부형제", "녹용별전/청소비", "주수상반/부재료", "조제비/수공비/조제탕전비", "포장비", "배송비", "기타", "합계"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);
		
		TagLibDisplay tagLibDisplay = new TagLibDisplay();
		
		if(LIST != null) {
			int nPrice1 = 0;
			int nPrice2 = 0;
			int nPrice3 = 0;
			int nPrice4 = 0;
			int nPrice5 = 0;
			int nPrice6 = 0;
			int nPrice7 = 0;
			int nPrice8 = 0;
			int nPrice9 = 0;
			int nPrice10 = 0;
			int nPrice11 = 0;
			int nAmount = 0;					
			
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();
				
				excel.setValue(0, String.valueOf(i + 1));
				excel.setValue(1, LIST.get(i).getORDER_TP_NM());
				excel.setValue(2, LIST.get(i).getORDER_STATUS_NM());
				excel.setValue(3, LIST.get(i).getDOCTOR());	
				excel.setValue(4, tagLibDisplay.getEditor(LIST.get(i).getORDER_NM()));
				excel.setValue(5, LIST.get(i).getPATIENT_NM());
				excel.setValue(6, LIST.get(i).getORDER_DT());
				excel.setValue(7, CommonUtil.NumberFormat(LIST.get(i).getPRICE1()));
				excel.setValue(8, CommonUtil.NumberFormat(LIST.get(i).getPRICE2()));
				excel.setValue(9, CommonUtil.NumberFormat(LIST.get(i).getPRICE3()));
				excel.setValue(10, CommonUtil.NumberFormat(LIST.get(i).getPRICE4()));
				excel.setValue(11, CommonUtil.NumberFormat(LIST.get(i).getPRICE5()));
				excel.setValue(12, CommonUtil.NumberFormat(LIST.get(i).getPRICE6()));
				excel.setValue(13, CommonUtil.NumberFormat(LIST.get(i).getPRICE7()));
				excel.setValue(14, CommonUtil.NumberFormat(LIST.get(i).getPRICE8()));
				excel.setValue(15, CommonUtil.NumberFormat(LIST.get(i).getPRICE9()));
				excel.setValue(16, CommonUtil.NumberFormat(LIST.get(i).getPRICE10()));
				excel.setValue(17, CommonUtil.NumberFormat(LIST.get(i).getPRICE11()));
				excel.setValue(18, CommonUtil.NumberFormat(LIST.get(i).getAMOUNT()));
				
				nPrice1 += CommonUtil.parseInt(LIST.get(i).getPRICE1());
				nPrice2 += CommonUtil.parseInt(LIST.get(i).getPRICE2());
				nPrice3 += CommonUtil.parseInt(LIST.get(i).getPRICE3());
				nPrice4 += CommonUtil.parseInt(LIST.get(i).getPRICE4());
				nPrice5 += CommonUtil.parseInt(LIST.get(i).getPRICE5());
				nPrice6 += CommonUtil.parseInt(LIST.get(i).getPRICE6());
				nPrice7 += CommonUtil.parseInt(LIST.get(i).getPRICE7());
				nPrice8 += CommonUtil.parseInt(LIST.get(i).getPRICE8());
				nPrice9 += CommonUtil.parseInt(LIST.get(i).getPRICE9());
				nPrice10 += CommonUtil.parseInt(LIST.get(i).getPRICE10());
				nPrice11 += CommonUtil.parseInt(LIST.get(i).getPRICE11());
				nAmount += CommonUtil.parseInt(LIST.get(i).getAMOUNT());
			}
			
			excel.addRow();
			
			excel.colSpan(0, 6);
			excel.setValue(0, "합계");
			excel.setValue(7, CommonUtil.NumberFormat(nPrice1));
			excel.setValue(8, CommonUtil.NumberFormat(nPrice2));
			excel.setValue(9, CommonUtil.NumberFormat(nPrice3));
			excel.setValue(10, CommonUtil.NumberFormat(nPrice4));
			excel.setValue(11, CommonUtil.NumberFormat(nPrice5));
			excel.setValue(12, CommonUtil.NumberFormat(nPrice6));
			excel.setValue(13, CommonUtil.NumberFormat(nPrice7));
			excel.setValue(14, CommonUtil.NumberFormat(nPrice8));
			excel.setValue(15, CommonUtil.NumberFormat(nPrice9));
			excel.setValue(16, CommonUtil.NumberFormat(nPrice10));
			excel.setValue(17, CommonUtil.NumberFormat(nPrice11));			
			excel.setValue(18, CommonUtil.NumberFormat(nAmount));
		}
		
		excel.save(response, excelName);
	}
	
}
