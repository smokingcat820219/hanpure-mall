package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.order.OrderVO;

public class ExcelOrderList extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		List<OrderVO> LIST = (List<OrderVO>)model.get("LIST");
		Excel excel = new Excel(workbook);

		String excelName = String.format("주문내역");	

		String title[] = {"No", "구분", "처방의", "처방명", "환자명", "주문금액", "주문일시", "결제", "주문상태", "배송요청일", "보내는사람", "받는사람", "택배사", "운송장번호"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);		
		
		int nSum = 0;
		
		if(LIST != null) {
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();				
				
				excel.setValue(0, String.valueOf(i + 1));
				excel.setValue(1, LIST.get(i).getORDER_TP_NM());
				excel.setValue(2, LIST.get(i).getDOCTOR());
				excel.setValue(3, LIST.get(i).getORDER_NM());
				excel.setValue(4, LIST.get(i).getPATIENT_NM());
				excel.setValue(5, CommonUtil.NumberFormat(LIST.get(i).getORDER_AMOUNT()));
				excel.setValue(6, LIST.get(i).getORDER_DT());
				
				if(LIST.get(i).getPAYMENT_STATUS().equals("Y")) {
					excel.setValue(7, "완료");
				}
				else {
					excel.setValue(7, "대기");
				}
				
				if(LIST.get(i).getORDER_STATUS().equals("Y")) {
					if(LIST.get(i).getPOP().equals("007")) {
						excel.setValue(8, "완료");
					}
					else {
						excel.setValue(8, "조제중");
					}
				}
				else {
					excel.setValue(8, LIST.get(i).getORDER_STATUS_NM());
				}				
				
				excel.setValue(9, LIST.get(i).getDELIVERY_DT());
				excel.setValue(10, LIST.get(i).getSEND_NM());
				excel.setValue(11, LIST.get(i).getRECV_NM());
				excel.setValue(12, LIST.get(i).getDELIVERY_NM());
				excel.setValue(13, LIST.get(i).getDELIVERY_CD());
				
				nSum += CommonUtil.parseInt(LIST.get(i).getORDER_AMOUNT());
			}
			
			excel.addRow();				
			
			excel.setValue(0, "합계");
			excel.setValue(1, "합계");
			excel.setValue(2, "합계");
			excel.setValue(3, "합계");
			excel.setValue(4, "합계");
			excel.setValue(5, CommonUtil.NumberFormat(nSum));
			
			excel.colSpan(0,  4);
		}
		
		excel.save(response, excelName);
	}
	
}
