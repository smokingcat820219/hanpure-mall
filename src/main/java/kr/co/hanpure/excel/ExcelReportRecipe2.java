package kr.co.hanpure.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import framework.smokingcat.taglib.TagLibDisplay;
import framework.smokingcat.util.CommonUtil;
import framework.smokingcat.util.Excel;
import kr.co.hanpure.database.report.ReportVO;

public class ExcelReportRecipe2 extends AbstractXlsxView  {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		ReportVO VO = (ReportVO)model.get("VO");
		List<ReportVO> LIST = (List<ReportVO>)model.get("LIST");
		
		Excel excel = new Excel(workbook);

		String excelName = String.format("분석보고서-처방 분석(유입구분)");	

		String title[] = {"기간별", "다이어트", "교통사고/상해", "비염", "여드름/피부", "어린이", "기타"};
				
		int columnCount = title.length;
		excel.SetColumnCount(columnCount);
		
		excel.addHeader();
		excel.setText(title);		
		
		TagLibDisplay tagLibDisplay = new TagLibDisplay();		
		
		if(LIST != null) {
			for (int i = 0; i < LIST.size(); i++) {
				excel.addRow();
				
				if(VO.getSearchField().equals("1")) {
					excel.setValue(0, LIST.get(i).getORDER_DT() + "(" + LIST.get(i).getDAY_OF_WEEK() + ")");
				}
				else {
					excel.setValue(0, LIST.get(i).getORDER_DT());
				}
				
				excel.setValue(1, CommonUtil.NumberFormat(LIST.get(i).getINFLOW_001()));
				excel.setValue(2, CommonUtil.NumberFormat(LIST.get(i).getINFLOW_002()));
				excel.setValue(3, CommonUtil.NumberFormat(LIST.get(i).getINFLOW_003()));
				excel.setValue(4, CommonUtil.NumberFormat(LIST.get(i).getINFLOW_004()));
				excel.setValue(5, CommonUtil.NumberFormat(LIST.get(i).getINFLOW_005()));
				excel.setValue(6, CommonUtil.NumberFormat(LIST.get(i).getINFLOW_006()));
			}
		}
		
		excel.save(response, excelName);
	}
	
}
