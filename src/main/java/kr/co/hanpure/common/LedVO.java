package kr.co.hanpure.common;

import org.apache.ibatis.type.Alias;

@Alias("LedVO")
public class LedVO {
	private String node_id;
	private String box_id;
	private String led1;
	private String led2;
	private String led3;
	private String led4;
	private String led5;
	private String led6;
	private String led7;
	
	public String getNode_id() {
		return node_id;
	}
	public void setNode_id(String node_id) {
		this.node_id = node_id;
	}
	public String getBox_id() {
		return box_id;
	}
	public void setBox_id(String box_id) {
		this.box_id = box_id;
	}
	public String getLed1() {
		return led1;
	}
	public void setLed1(String led1) {
		this.led1 = led1;
	}
	public String getLed2() {
		return led2;
	}
	public void setLed2(String led2) {
		this.led2 = led2;
	}
	public String getLed3() {
		return led3;
	}
	public void setLed3(String led3) {
		this.led3 = led3;
	}
	public String getLed4() {
		return led4;
	}
	public void setLed4(String led4) {
		this.led4 = led4;
	}
	public String getLed5() {
		return led5;
	}
	public void setLed5(String led5) {
		this.led5 = led5;
	}
	public String getLed6() {
		return led6;
	}
	public void setLed6(String led6) {
		this.led6 = led6;
	}
	public String getLed7() {
		return led7;
	}
	public void setLed7(String led7) {
		this.led7 = led7;
	}
}
