package kr.co.hanpure.common;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;

public class Api {
	public static String getUserInfo(String hostial_id) {
		try {
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			HttpPost httpPost = new HttpPost("https://hanpuremall.co.kr/api/");

			JSONObject json = new JSONObject();
			json.put("mb_id", hostial_id);
			json.put("publicKey", "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALan1/Q5+DtJAtKKYB6BfFIGYRWcVOHwDCfSUL28WnZHp/LnJnHOAaExAB3jxohP65ikAa6dyIoXvyMevhACqTkCAwEAAQ==");
			
			System.out.println("url : " + "https://hanpuremall.co.kr/api/");
			System.out.println("json : " + json.toString());
			
			StringEntity params =new StringEntity(json.toString());
			httpPost.setEntity(params);
			httpPost.setHeader("Content-type", "application/json");
			
			CloseableHttpResponse response_ = httpclient.execute(httpPost);

			if (response_.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String sResult = EntityUtils.toString(response_.getEntity());
				HttpEntity entity = response_.getEntity();
				EntityUtils.consume(entity);
				
				sResult = StringEscapeUtils.unescapeJava(sResult);
				
				System.out.println("sResult : " + sResult);
				
				return sResult;
				
			} 
			else {
				String sResult = EntityUtils.toString(response_.getEntity());

				System.out.println("sResult : " + sResult);
				
				return "";
			}	
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return "";			
	}
}
