package kr.co.hanpure.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import kr.co.hanpure.interceptor.InterceptorEx;

@Configuration
public class MvcConfigurationEx implements WebMvcConfigurer {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new InterceptorEx())
		.excludePathPatterns("/images/**", "/css/**", "/fonts/**", "/js/**", "/include/**");
	}
}
