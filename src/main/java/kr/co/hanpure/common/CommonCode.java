package kr.co.hanpure.common;

public class CommonCode {	
	public static final String USER_SESSION_KEY = "HANPURE";
	
	public String getPaging() {
		String sPage = "";
		sPage += "<ul>";
		sPage += " 	<li class='prev'><a href='#'><span title='이전'>&lt;&lt;</span></a></li>";
		sPage += "	<li class='prev'><a href='#'><span title='이전'>&lt;</span></a></li>";
		sPage += "	<li class='on'><a href='#'><span>1</span></a></li>";
		sPage += "	<li><a href='#'><span>2</span></a></li>";
		sPage += "	<li><a href='#'><span>3</span></a></li>";
		sPage += "	<li><a href='#'><span>4</span></a></li>";
		sPage += "	<li>...</li>";
		sPage += "	<li><a href='#'><span>9</span></a></li>";						
		sPage += "	<li><a href='#'><span>10</span></a></li>";
		sPage += "	<li class='next'><a href='#'><span title='다음'>&gt;</span></a></li>";
		sPage += "	<li class='next'><a href='#'><span title='다음'>&gt;&gt;</span></a></li>";
		sPage += "</ul>";
		
		return sPage;
	}
	/*
	public interface PRICE {
		public static final int COLLECTION = 5000;
		public static final int ANALYSIS = 15000;
		public static final int PREDICTION = 35000;
	}
	
	public interface SERVICE_ITEM {
		public static final String TRIAL = "F";
		public static final String COLLECTION = "C";
		public static final String ANALYSIS = "A";
		public static final String PREDICTION = "P";
	}
	
	public interface STATUS_SERVICE {
		public static final String READY = "0";
		public static final String PAYMENT_READY = "1";
		public static final String PAYMENT_CONFIRM = "2";		
		public static final String PAYMENT = "3";
		public static final String REJECT = "4";
		public static final String REFUND_READY = "5";
		public static final String REFUND_COMPLETE = "6";
		public static final String END = "7";
	}
	
	public interface DIVISION_FILE {
		public static final String NOTICE = "N";
		public static final String FAQ = "F";
		public static final String Q_OF_QNA = "Q";
		public static final String A_OF_QNA = "A";
	}
	
	public interface DIVISION_SERVICE {
		public static final String CAP = "1";
		public static final String MONITORING_OF_3D = "2";
		public static final String ETC = "3";
	}
	
	public static String getCompanyCode(String BIZ_SEQ) {
		return String.format("C%08d", Integer.parseInt(BIZ_SEQ));
	}
	*/
}