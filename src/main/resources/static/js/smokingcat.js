var ajaxRunning = false;

$(function() {	
	$('.number').keyup(function(e) {
		InpuOnlyNumber(this);	
	});	
	$('.phone').keyup(function(e) {
		inputOnlyPhone(this);	
	});		
	$('.email').keyup(function(e) {
		inputOnlyEmail(this);	
	});	
	$('.ip').keyup(function(e) {
		inputOnlyIP(this);	
	});	
	$('.date').keyup(function(e) {
		InpuOnlyNumber(this);			
		this.value = GetBirth(this.value);
	});	
	$('.biz_id').keyup(function(e) {
		inputOnlyBizId(this);	
	});	
	$('.duns').keyup(function(e) {
		inputOnlyDUNSNumber(this);	
	});	
	$('.daum_post').click(function() {
		OpenDaumZip();
    });
    $('.float').keyup(function(e) {
		InpuOnlyFloat(this);	
	});	    
});

function inputOnlyBizId(tag) {
	tag.value = tag.value.replace(/[^0-9]/g,'');
	
	var rt = "";
	var biz_id = tag.value; 
	for(var i = 0; i < biz_id.length; i++) {		
		rt += biz_id.substring(i, i + 1);
		
		if(rt.length == 3) {
			rt += "-";
		}
		else if(rt.length == 6) {
			rt += "-";
		}
		
		if(rt.length == 12) {
			break;
		}
	} 
	
	tag.value = rt;
	
	/*
	var biz_id = tag.value; 
	if(biz_id.length <= 5) {
		tag.value = (biz_id.replace(/([0-9]{3})-([0-9]+)/, '$1-$2'));
	}
	else if(biz_id > 5) {
		tag.value = (biz_id.replace(/([0-9]{3})-([0-9]{2})-([0-9]+)/, '$1-$2-$3'));
	}
	*/
}

function inputOnlyDUNSNumber(tag) {
	tag.value = tag.value.replace(/[^0-9]/g,'');
	
	var biz_id = tag.value; 
	if(biz_id.length <= 5) {
		tag.value = (biz_id.replace(/([0-9]{2})-([0-9]+)/, '$1-$2'));
	}
	else if(biz_id > 5) {
		tag.value = (biz_id.replace(/([0-9]{2})-([0-9]{3})-([0-9]+)/, '$1-$2-$3'));
	}
}

function validationBizid(bizid) {
	bizid = bizid.replace(/[^0-9]/g,'');
	
	//var regExp = /[0-9]{3,3}-[0-9]{2,2}-[0-9]{5,5}$/;
	var regExp = /[0-9]{3,3}[0-9]{2,2}[0-9]{5,5}$/;
	if(!bizid.match(regExp)) {
		return false;
	}	
	return true;
}

function validationIP(ip) {
	var regExp = /[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/;		
	if(!ip.match(regExp)) {
		return false;
	}	
	return true;
}

function validationMobile(mobile) {
	mobile = mobile.replace(/[^0-9]/g,'');	
	
	//var regExp = /(010|011|016|017|018|019)-[0-9]{3,4}-[0-9]{4,4}$/;
	var regExp = /(010|011|016|017|018|019)[0-9]{3,4}[0-9]{4,4}$/;
	if(!mobile.match(regExp)) {
		return false;
	}	
	return true;
}

function validationTel(tel) {
	tel = tel.replace(/[^0-9]/g,'');	
	
	//var regExp = /(02|051|053|032|062|042|052|044|031|033|043|041|063|061|054|055|064|070)-[0-9]{3,4}-[0-9]{4,4}$/;
	var regExp = /(02|051|053|032|062|042|052|044|031|033|043|041|063|061|054|055|064|070)[0-9]{3,4}[0-9]{4,4}$/;
	if(!tel.match(regExp)) {
		return false;
	}	
	return true;
}

function validationEmail(email) {
	var regExp = /[0-9a-zA-Z][-_0-9a-zA-Z]+@[-_0-9a-zA-Z]+(\.[-_0-9a-zA-Z]+){1,2}$/;		
	if(!email.match(regExp)) {
		return false;
	}	
	return true;
}

function validationDate(date) {
	var regExp = /[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}$/;		
	if(!date.match(regExp)) {
		return false;
	}	
	return true;
}

function validationBirth(birth) {
	birth = birth.replace(/[^0-9]/g,'');	
	
	if(birth.length != 8) {
		return false;
	}
	
	var birth1 = birth.substring(0, 4);
	var birth2 = birth.substring(4, 6);
	var birth3 = birth.substring(6, 8);
	
	console.log("birth1 : " + birth1);
	console.log("birth2 : " + birth2);
	console.log("birth3 : " + birth3);
	
	birth1 = parseInt(birth1);
	birth2 = parseInt(birth2);
	birth3 = parseInt(birth3);
	
	if(birth1 < 1900 || birth1 > 2100) {
		return false;
	}
	if(birth2 < 0 || birth2 > 12) {
		return false;
	}
	if(birth3 < 0 || birth3 > 31) {
		return false;
	}
	
	return true;
}

function getOnlyNumber(value) {
	return value.replace(/[^0-9]/g,'');			
}

function getOnlyFloat(value) {
	return value.replace(/[^-\.0-9]/g,'');	
}

function InpuOnlyFloat(tag) {
	var value = tag.value;
	value = value.replace(/[^-\.0-9]/g,'');
	
	var words = value.split('.');
	
	if(words.length == 1) {
		tag.value = value;
	}
	else {
		tag.value = words[0] + "." + words[1].substring(0, 2);
	}	
}

function InpuOnlyNumber(tag) {
	tag.value = tag.value.replace(/[^0-9]/g,'');			
}

function inputOnlyPhone(tag) {
	tag.value = tag.value.replace(/[^0-9-]/g,'');	
}

function inputOnlyDate(tag) {
	tag.value = tag.value.replace(/[^0-9-]/g,'');	
}

function inputOnlyIP(tag) {
	tag.value = tag.value.replace(/[^0-9.]/g,'');	
}

function inputOnlyEmail(tag) {
	tag.value = tag.value.replace(/[^-_@.0-9a-zA-Z]/g,'');	
}

function validationPwd(pwd, min_length, max_length) {	
	var cnt = 0;
	
	var pattern1 = /[0-9]/g;
	var pattern2 = /[a-z]/g;
	var pattern3 = /[A-Z]/g;
	var pattern4 = /[~!@#$%^&*()_+-.,;:\\\/|<>"'`]/g;
	
	if(pwd.length < min_length) {		
		return false;
	}
	
	if(pwd.length > max_length) {		
		return false;
	}
	
	if(pwd.match(pattern1)) {
		cnt = cnt + 1;
	}
	if(pwd.match(pattern2)) {
		cnt = cnt + 1;
	}
	//if(pwd.match(pattern3)) {
	//	cnt = cnt + 1;
	//}
	//if(pwd.match(pattern4)) {
	//	cnt = cnt + 1;
	//}
	
	if(cnt < 2) {		
		return false;
	}
	
	return true;
}

/*
function CheckBizID(bizID) {	
	// bizID는 숫자만 10자리로 해서 문자열로 넘긴다.
	var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
	var tmpBizID, i, chkSum = 0, c2, remander;

	bizID = bizID.replace(/-/gi, '');

	for (i = 0; i <= 7; i++) {
		chkSum += checkID[i] * bizID.charAt(i);
	}

	c2 = "0" + (checkID[8] * bizID.charAt(8));
	c2 = c2.substring(c2.length - 2, c2.length);

	chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));

	remander = (10 - (chkSum % 10)) % 10;

	if (Math.floor(bizID.charAt(9)) == remander) {
		return true; // OK! 
	}

	return false;
}

//IE버전 확인
function getVersionOfIE() { 
	 var word; 
	 var version = "N/A"; 

	 var agent = navigator.userAgent.toLowerCase(); 
	 var name = navigator.appName; 

	 // IE old version ( IE 10 or Lower ) 
	 if ( name == "Microsoft Internet Explorer" ) 
	 {
		 word = "msie "; 
	 }
	 else 
	 { 
		 // IE 11 
		 if ( agent.search("trident") > -1 ) word = "trident/.*rv:"; 

		 // IE 12  ( Microsoft Edge ) 
		 else if ( agent.search("edge/") > -1 ) word = "edge/"; 
	 } 

	 var reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 

	 if ( reg.exec( agent ) != null  ) 
		 version = RegExp.$1 + RegExp.$2; 

	 return version; 
}

function CheckFloat(value) {
	if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
		return Number(value);
	}
	else {
		return NaN;
	}	
}
*/

function ShowCSS(obj) {
	ajaxRunning = true;
	$(obj).css("display", "inline-block");   
}

function HideCSS(obj) {
	ajaxRunning = false;
	$(obj).css("display", "none");  
}

function NumberFormat(val) {
	val = "" + val;
	val = "" + val.replace(/,/gi,''); // 콤마 제거
	var idx = val.indexOf('.');
	if(idx > -1) {
		var strArr = val.split('.');
		while (/(\d+)(\d{3})/.test(strArr[0].toString())) {
			strArr[0] = strArr[0].toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
		}
		
		if(strArr[1] == "0") {
			return strArr[0];
		}
		else if(strArr[1] == "00") {
			return strArr[0];	
		}
		else {
			return strArr[0] + "." + strArr[1];	
		}
	}
	else {
		while (/(\d+)(\d{3})/.test(val.toString())) {
			val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
		}
		return val;
	}	
}

//<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
function OpenDaumZip() {
	new daum.Postcode({
		oncomplete: function(data) {        
			if($("#zipcode").length > 0) {
				$("#zipcode").val(data.zonecode);
			}
			if($("#address").length > 0) {
				$("#address").val(data.roadAddress);
			}
			if($("#address1").length > 0) {
				$("#address1").val(data.roadAddress);
			}
		}
	}).open();
}

function unEntity(str){
	//return str.replace(/&amp;/g, "&");
   return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

function GetOneLine(value) {	
	value = value.replace(/\s+/, "");//왼쪽 공백제거
	value = value.replace(/\s+$/g, "");//오른쪽 공백제거
	value = value.replace(/\n/g, " ");//행바꿈제거
	value = value.replace(/\r/g, " ");//엔터제거
	
	/*
	value = value.replace(/</g, "&lt;");
	value = value.replace(/>/g, "&gt;");	
	
	value = value.replace(/\(/g, "&#40;");
	value = value.replace(/\)/g, "&#41;");		 
	
	value = value.replace(/\"/gi, "&quot;");
	value = value.replace(/\'/gi, "&#39;");
	*/
	
	value = value.replace(/</g, "");
	value = value.replace(/>/g, "");
	
	value = value.replace(/\(/g, "");
	value = value.replace(/\)/g, "");		 
	
	value = value.replace(/\"/gi, " ");
	value = value.replace(/\'/gi, " ");
	
	return value;
}

function GetValue(id) {
	var value = $.trim($("#" + id).val());
	return value;
}

function GetRadioValue(name) {
	var value = $(":radio[name='" + name + "']:checked").val();	
	return value;
}

function SetRadioValue(name, value, checked) {
	$("input:radio[name ='" + name + "']:input[value='" + value + "']").attr("checked", checked);
}

function isChecked(id) {
	var isCheck = $("input:checkbox[id='" +id + "']").is(":checked");
	if(isCheck) {
		return "Y";
	}
	else {
		return "N";
	}		
}

function GetCheckedLength(name) {
	return $("input:checkbox[name='" + name + "']:checked").length;
}

function GetMobile(mobile) {	
	var m1 = mobile.substring(0, 3);
	var m2 = mobile.substring(3, mobile.length - 4);
	var m3 = mobile.substring(mobile.length - 4);
	
	return m1 + "-" + m2 + "-" + m3;
}

function GetTel(tel) {	
	var m1 = tel.substring(0, 2);
	var m2 = tel.substring(0, 3);
	
	if(m1 == "02") {
		var t1 = tel.substring(0, 2);
		var t2 = tel.substring(2, tel.length - 4);
		var t3 = tel.substring(tel.length - 4);
		
		return t1 + "-" + t2 + "-" + t3;
	}
	else if(m2 == "051" || m2 == "053" || m2 == "032" || m2 == "062" || m2 == "042" || m2 == "052" || m2 == "044" || m2 == "031" || m2 == "033" || m2 == "043" || m2 == "041"
		|| m2 == "063"|| m2 ==  "061"|| m2 == "054" || m2 == "055" || m2 == "064" || m2 == "070") {
		
		var t1 = tel.substring(0, 3);
		var t2 = tel.substring(3, tel.length - 4);
		var t3 = tel.substring(tel.length - 4);
		
		return t1 + "-" + t2 + "-" + t3;
	}
	else {
		return GetMobile(tel);
	}
}

function GetBirth(birth) {
	var m1 = birth.substring(0, 4);
	var m2 = birth.substring(4, 6);
	var m3 = birth.substring(6, 8);
	
	return m1 + "-" + m2 + "-" + m3;
}

function GetBizID(bizid) {
	if(bizid.length == 9) {
		var m1 = bizid.substring(0, 2);
		var m2 = bizid.substring(2, 5);
		var m3 = bizid.substring(5, 9);
		
		return m1 + "-" + m2 + "-" + m3;
	}
	else if(bizid.length == 10) {
		var m1 = bizid.substring(0, 3);
		var m2 = bizid.substring(3, 5);
		var m3 = bizid.substring(5, 10);
		
		return m1 + "-" + m2 + "-" + m3;
	}
	else {		
		return bizid;
	}
}

function GetManwon(salary) {
	var salary2 = parseFloat(salary);
	salary2 = salary2 / 10000;
	
	return salary2 + "만";
}

function GetMaskName(name) {	
	return name.substring(0, 1) + "**";
}

function GetBytesLength(str) {	
	if(!str) return 0;
	
	var size = 0;
	
	for(var i = 0; i < str.length; i++) {
		var c = escape(str.charAt(i));
		if(c.length == 1) {					//기본 아스키코드
			size++;
		}
		else if(c.indexOf("%u") != -1) {	//한글 혹은 기타
			size += 2;
		}
		else {
			size++;
		}
	}
	
	return size;
}

function setCookie(c_name, value, exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value)
			+ ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name) {
			return unescape(y);
		}
	}
}

function fixedNumer(value, length) {
	value = value + '';
    return value.length >= length ? value : new Array(length - value.length + 1).join('0') + value;
}

function getDateTime() {
	var today = new Date(); 

	var year = today.getFullYear(); // 년도
	var month = today.getMonth() + 1;  // 월
	var date = today.getDate();  // 날짜
	
	var hours = today.getHours(); // 시
	var minutes = today.getMinutes();  // 분
	var seconds = today.getSeconds();  // 초

	var result = year + "-";
	result += fixedNumer(month, 2) + "-";
	result += fixedNumer(date, 2) + " ";
	result += fixedNumer(hours, 2) + ":";
	result += fixedNumer(minutes, 2) + ":";
	result += fixedNumer(seconds, 2);

	return result;
}	

function dateDiff(date1, date2) {	
	
}

function calcAge(birth) {
	var today = getToday();
	var year = today.split("-");
	
	var year2 = birth.split("-");
	
	var age = parseInt(year[0]) - parseInt(year2[0]) + 1;
	
	return age;
}

function getHour() {
	var today = new Date(); 

	return today.getHours();
}	

function getToday() {
	var today = new Date(); 

	var year = today.getFullYear(); // 년도
	var month = today.getMonth() + 1;  // 월
	var date = today.getDate();  // 날짜

	var result = year + "-";
	result += fixedNumer(month, 2) + "-";
	result += fixedNumer(date, 2);

	return result;
}	

function getPreDate(yyyymmdd, n) {
	const dates = yyyymmdd.split('-');
	if(dates.length != 3) return date;	
	
	var today = new Date(parseInt(dates[0]), parseInt(dates[1]) - 1, parseInt(dates[2]));
	
	var data = today.getDate();
	today.setDate(data + n);	
	
	var year = today.getFullYear(); 	// 년도
	var month = today.getMonth() + 1;  	// 월
	var date = today.getDate();  		// 날짜

	var result = year + "-";
	result += fixedNumer(month, 2) + "-";
	result += fixedNumer(date, 2);

	return result;
}

function getPreMonth(yyyymmdd, n) {
	const dates = yyyymmdd.split('-');
	if(dates.length != 3) return date;
	
	var today = new Date(parseInt(dates[0]), parseInt(dates[1]) - 1, parseInt(dates[2]));
	
	var data = today.getMonth();
	today.setMonth(data + n);	
	
	var year = today.getFullYear(); 	// 년도
	var month = today.getMonth() + 1;  	// 월
	var date = today.getDate();  		// 날짜

	var result = year + "-";
	result += fixedNumer(month, 2) + "-";
	result += fixedNumer(date, 2);

	return result;
}

function getPreYear(yyyymmdd, n) {
	const dates = yyyymmdd.split('-');
	if(dates.length != 3) return date;
	
	var today = new Date(parseInt(dates[0]), parseInt(dates[1]) - 1, parseInt(dates[2]));
	
	var data = today.getFullYear();		
	today.setYear(data + n);
	
	var year = today.getFullYear(); 	// 년도
	var month = today.getMonth() + 1;  	// 월
	var date = today.getDate();  		// 날짜

	var result = year + "-";
	result += fixedNumer(month, 2) + "-";
	result += fixedNumer(date, 2);

	return result;
}

function getDateDiff(date1, date2) {
	const dates1 = date1.split('-');
	const dates2 = date2.split('-');
	
	if(dates1.length != 3) return -99999;
	if(dates2.length != 3) return -99999;	
	
	var start_date = new Date(parseInt(dates1[0]), parseInt(dates1[1]) - 1, parseInt(dates1[2]));
	var end_date = new Date(parseInt(dates2[0]), parseInt(dates2[1]) - 1, parseInt(dates2[2]));
	
	var miliseconds = end_date.getTime() - start_date.getTime();
	var days = parseInt(miliseconds / 86400000);	//1000 * 60 * 60 * 24
	
	return days;
}

/*
function getPreDate(mode, n) {
	var today = new Date(); 

	if(mode == "Y") {
		var data = today.getFullYear();		
		today.setYear(data + n);
	}
	else if(mode == "M") {
		var data = today.getMonth();
		today.setMonth(data + n);
	}
	else {
		var data = today.getDate();
		today.setDate(data + n);
	}	
	
	var year = today.getFullYear(); // 년도
	var month = today.getMonth() + 1;  // 월
	var date = today.getDate();  // 날짜

	var result = year + "-";
	result += fixedNumer(month, 2) + "-";
	result += fixedNumer(date, 2);

	return result;
}	
*/

function getPeriod(n) {
	n = "" + n;	
	n = parseInt(n);
	
	if(n < 13) {
		return n + "개월";
	}
	else {
		return (n / 12) + "년";
	}
}	

function getDefaultString(text1, text2) {
	if(text1 != null && text1 != undefined && text1 != 'undefined') {
		return text1;
	}
	else {
		return text2;
	}
}	

function getDate10(text1) {	
	if(text1 != null && text1 != undefined && text1 != 'undefined') {
		if(text1.length > 10) {
			return text1.substring(0, 10);
		}
		else {
			return text1;
		}		
	}
	else {
		return "-";
	}
}	

function getDate19(text1) {	
	if(text1 != null && text1 != undefined && text1 != 'undefined') {
		if(text1.length > 19) {
			return text1.substring(0, 19);
		}
		else {
			return text1;
		}		
	}
	else {
		return "-";
	}
}	

function GetBR(str) {
    if(str == undefined || str == null) {
        return "";
    }

    str = str.replace(/\n/g, '<br/>');
    return str;
}

function GetNation(bank) {
	if(bank == '1') { return '경남은행'; }
	else if(bank == '2') { return '광주은행'; }
	else if(bank == '3') { return '국민은행'; }
	else if(bank == '4') { return '기업은행'; }
	else if(bank == '5') { return '농협중앙회'; }
	else if(bank == '6') { return '농협회원조합'; }
	else if(bank == '7') { return '대구은행'; }
	else if(bank == '8') { return '도이치은행'; }
	else if(bank == '9') { return '부산은행'; }
	else if(bank == '10') { return '산업은행'; }
	else if(bank == '11') { return '상호저축은행'; }
	else if(bank == '12') { return '새마을금고'; }
	else if(bank == '13') { return '수협중앙회'; }
	else if(bank == '14') { return '신한금융투자'; }
	else if(bank == '15') { return '신한은행'; }
	else if(bank == '16') { return '신협중앙회'; }
	else if(bank == '17') { return '외환은행'; }
	else if(bank == '18') { return '우리은행'; }
	else if(bank == '19') { return '우체국'; }
	else if(bank == '20') { return '전북은행'; }
	else if(bank == '21') { return '제주은행'; }
	else if(bank == '22') { return '카카오뱅크'; }
	else if(bank == '23') { return '케이뱅크'; }
	else if(bank == '24') { return '하나은행'; }
	else if(bank == '25') { return '한국씨티은행'; }
	else if(bank == '26') { return 'HSBC은행'; }
	else if(bank == '27') { return 'SC제일은행'; }
	else { return ""; }
}

function GetBank(bank) {
	if(bank == '35') { return '경남은행'; }
	else if(bank == '29') { return '광주은행'; }
	else if(bank == '7') { return '국민은행'; }
	else if(bank == '5') { return '기업은행'; }
	else if(bank == '15') { return '농협중앙회'; }
	else if(bank == '17') { return '농협회원조합'; }
	else if(bank == '25') { return '대구은행'; }
	else if(bank == '47') { return '도이치은행'; }
	else if(bank == '27') { return '부산은행'; }
	else if(bank == '3') { return '산업은행'; }
	else if(bank == '41') { return '상호저축은행'; }
	else if(bank == '37') { return '새마을금고'; }
	else if(bank == '11') { return '수협중앙회'; }
	else if(bank == '36') { return '신한금융투자'; }
	else if(bank == '60') { return '신한은행'; }
	else if(bank == '39') { return '신협중앙회'; }
	else if(bank == '9') { return '외환은행'; }
	else if(bank == '19') { return '우리은행'; }
	else if(bank == '56') { return '우체국'; }
	else if(bank == '33') { return '전북은행'; }
	else if(bank == '31') { return '제주은행'; }
	else if(bank == '68') { return '카카오뱅크'; }
	else if(bank == '67') { return '케이뱅크'; }
	else if(bank == '59') { return '하나은행'; }
	else if(bank == '23') { return '한국씨티은행'; }
	else if(bank == '45') { return 'HSBC은행'; }
	else if(bank == '21') { return 'SC제일은행'; }
	else { return ""; }
}

function JsonToString(json) {
	return JSON.stringify(json);	
}

function getUUID() { // UUID v4 generator in JavaScript (RFC4122 compliant)
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 3 | 8);
    return v.toString(16);
  });
}

function getDayOfWeek(today) {
	var week = ['일', '월', '화', '수', '목', '금', '토'];
	var dayOfWeek = week[new Date(today).getDay()];
	return dayOfWeek;
}

function secondsToHour(seconds) {
	var hour = parseInt(seconds / 3600);
	var min = parseInt((seconds % 3600) / 60);
	var sec = parseInt(seconds % 60);

	var time = "";
	if(hour > 0) {
		time += hour + "시 ";
	}
	if(min > 0) {
		time += min + "분 ";
	}
	if(sec > 0) {
		time += sec + "초";
	}
	
	return time;
}

function secondsToMinute(seconds) {
	var hour = parseInt(seconds / 3600);
	var min = parseInt((seconds % 3600) / 60);
	var sec = parseInt(seconds % 60);

	var time = "";
	if(hour > 0) {
		time += hour + "시 ";
	}
	if(min > 0) {
		time += min + "분 ";
	}
	if(sec > 0) {
		time += sec + "초";
	}
	
	return min;
}