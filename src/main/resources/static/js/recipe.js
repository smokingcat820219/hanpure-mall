function setRecipeInfo(recipe_nm, info1, info2, info3, info4, info5, info6, info7, info8, info9, info10, info10_nm, info10_qntt, info10_price, info11, marking_tp, marking_text, package1, package2, package3, package4, package1_nm, package2_nm, package3_nm, package4_nm, package1_tp, package2_tp, package3_tp, package4_tp, vaccum) {
	$("#recipe_nm").val(recipe_nm);
	
	setMarking(marking_tp, marking_text);
	
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		$("#form1 #info1").val(info1);
		$("#form1 #info2").val(info2);
		$("#form1 #info3").val(info3);
		$("#form1 #info4").val(info4);
		//$("#form1 #info5").val(info5);
		//$("#form1 #info6").val(info6);
		$("#form1 #info7").val(info7);		
		$("#form1 #info8").val(info8);
		$("#form1 #info9").val(info9);
		
		if(info10 != "") {
			$("#form1 #info10").html(info10_nm + "/" + info10_qntt + "ml");
			
			$("#form1 #info10").attr("code", info10);
			$("#form1 #info10").attr("name", info10_nm);
			$("#form1 #info10").attr("price", info10_price);
			$("#form1 #info10").attr("value", info10_qntt);	
		}						
		else {
			$("#form1 #info10").html("선택 없음");			
			$("#form1 #info10").attr("code", "");
			$("#form1 #info10").attr("price", "");
			$("#form1 #info10").attr("value", "");
		}
		
		$("#form1 #info11").val(info11);
		
		if(package1) {
			$("#form1 #btnPackage1").attr("bom_cd", package1);
			$("#form1 #btnPackage1").attr("bom_nm", package1_nm);
			$("#form1 #btnPackage1").attr("bom_tp", package1_tp);
			$("#form1 #btnPackage1").attr("vaccum", vaccum);
			$("#form1 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("1", package1);
		}
		else {
			$("#form1 #btnPackage1").attr("bom_cd", "");
			$("#form1 #btnPackage1").attr("bom_nm", "");
			$("#form1 #btnPackage1").attr("bom_tp", "");
			$("#form1 #btnPackage1").attr("vaccum", vaccum);
			$("#form1 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form1 #img_package1").html("");
		}
		
		if(package2) {
			$("#form1 #btnPackage2").attr("bom_cd", package2);
			$("#form1 #btnPackage2").attr("bom_nm", package2_nm);
			$("#form1 #btnPackage2").attr("bom_tp", package2_tp);
			$("#form1 #btnPackage2").attr("vaccum", vaccum);
			$("#form1 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			getItemInfo("2", package2);
		}
		else {
			$("#form1 #btnPackage2").attr("bom_cd", "");
			$("#form1 #btnPackage2").attr("bom_nm", "");
			$("#form1 #btnPackage2").attr("bom_tp", "");
			$("#form1 #btnPackage2").attr("vaccum", vaccum);
			$("#form1 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form1 #img_package2").html("");
		}
		
		if(package3) {
			$("#form1 #btnPackage3").attr("bom_cd", package3);
			$("#form1 #btnPackage3").attr("bom_nm", package3_nm);
			$("#form1 #btnPackage3").attr("bom_tp", package3_tp);
			$("#form1 #btnPackage3").attr("vaccum", vaccum);
			$("#form1 #btnPackage3").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("3", package3);
		}
		else {
			$("#form1 #btnPackage3").attr("bom_cd", "");
			$("#form1 #btnPackage3").attr("bom_nm", "");
			$("#form1 #btnPackage3").attr("bom_tp", "");
			$("#form1 #btnPackage3").attr("vaccum", vaccum);
			$("#form1 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form1 #img_package3").html("");
		}				
	}
	else if(recipe_tp == "003") {
		$("#form2 #info1").val(info1);
		$("#form2 #info2").val(info2);
		$("#form2 #info3").val(info3);
		$("#form2 #info4").val(info4);
		$("#form2 #info5").html(info5);
		$("#form2 #info6").html(info6);
		$("#form2 #info7").html(info7);		
		
		if(package1) {
			$("#form2 #btnPackage1").attr("bom_cd", package1);
			$("#form2 #btnPackage1").attr("bom_nm", package1_nm);
			$("#form2 #btnPackage1").attr("bom_tp", package1_tp);
			$("#form2 #btnPackage1").attr("vaccum", vaccum);
			$("#form2 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("1", package1);
		}
		else {
			$("#form2 #btnPackage1").attr("bom_cd", "");
			$("#form2 #btnPackage1").attr("bom_nm", "");
			$("#form2 #btnPackage1").attr("bom_tp", "");
			$("#form2 #btnPackage1").attr("vaccum", vaccum);
			$("#form2 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form2 #img_package1").html("");
		}
		
		if(package2) {
			$("#form2 #btnPackage2").attr("bom_cd", package2);
			$("#form2 #btnPackage2").attr("bom_nm", package2_nm);
			$("#form2 #btnPackage2").attr("bom_tp", package2_tp);
			$("#form2 #btnPackage2").attr("vaccum", vaccum);
			$("#form2 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			getItemInfo("2", package2);
		}
		else {
			$("#form2 #btnPackage2").attr("bom_cd", "");
			$("#form2 #btnPackage2").attr("bom_nm", "");
			$("#form2 #btnPackage2").attr("bom_tp", "");
			$("#form2 #btnPackage2").attr("vaccum", vaccum);
			$("#form2 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form2 #img_package2").html("");
		}
		
		if(package3) {
			$("#form2 #btnPackage3").attr("bom_cd", package3);
			$("#form2 #btnPackage3").attr("bom_nm", package3_nm);
			$("#form2 #btnPackage3").attr("bom_tp", package3_tp);
			$("#form2 #btnPackage3").attr("vaccum", vaccum);
			$("#form2 #btnPackage3").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("3", package3);
		}
		else {
			$("#form2 #btnPackage3").attr("bom_cd", "");
			$("#form2 #btnPackage3").attr("bom_nm", "");
			$("#form2 #btnPackage3").attr("bom_tp", "");
			$("#form2 #btnPackage3").attr("vaccum", vaccum);
			$("#form2 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form2 #img_package3").html("");
		}	
		
		if(package4) {
			$("#form2 #btnPackage4").attr("bom_cd", package4);
			$("#form2 #btnPackage4").attr("bom_nm", package4_nm);
			$("#form2 #btnPackage4").attr("bom_tp", package4_tp);
			$("#form2 #btnPackage4").attr("vaccum", vaccum);
			$("#form2 #btnPackage4").html("<span>" + package4_nm + "</span>");	
			
			getItemInfo("4", package4);
		}
		else {
			$("#form2 #btnPackage4").attr("bom_cd", "");
			$("#form2 #btnPackage4").attr("bom_nm", "");
			$("#form2 #btnPackage4").attr("bom_tp", "");
			$("#form2 #btnPackage4").attr("vaccum", vaccum);
			$("#form2 #btnPackage4").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form2 #img_package4").html("");
		}			
	}
	else if(recipe_tp == "003") {
		$("#form3 #info1").val(info1);
		$("#form3 #info2").val(info2);
		
		if(package1) {
			$("#form3 #btnPackage1").attr("bom_cd", package1);
			$("#form3 #btnPackage1").attr("bom_nm", package1_nm);
			$("#form3 #btnPackage1").attr("bom_tp", package1_tp);
			$("#form3 #btnPackage1").attr("vaccum", vaccum);
			$("#form3 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("1", package1);
		}
		else {
			$("#form3 #btnPackage1").attr("bom_cd", "");
			$("#form3 #btnPackage1").attr("bom_nm", "");
			$("#form3 #btnPackage1").attr("bom_tp", "");
			$("#form3 #btnPackage1").attr("vaccum", vaccum);
			$("#form3 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form3 #img_package1").html("");
		}
		
		if(package2) {
			$("#form3 #btnPackage2").attr("bom_cd", package2);
			$("#form3 #btnPackage2").attr("bom_nm", package2_nm);
			$("#form3 #btnPackage2").attr("bom_tp", package2_tp);
			$("#form3 #btnPackage2").attr("vaccum", vaccum);
			$("#form3 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			getItemInfo("2", package2);
		}
		else {
			$("#form3 #btnPackage2").attr("bom_cd", "");
			$("#form3 #btnPackage2").attr("bom_nm", "");
			$("#form3 #btnPackage2").attr("bom_tp", "");
			$("#form3 #btnPackage2").attr("vaccum", vaccum);
			$("#form3 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form3 #img_package2").html("");
		}
		
		if(package3) {
			$("#form3 #btnPackage3").attr("bom_cd", package3);
			$("#form3 #btnPackage3").attr("bom_nm", package3_nm);
			$("#form3 #btnPackage3").attr("bom_tp", package3_tp);
			$("#form3 #btnPackage3").attr("vaccum", vaccum);
			$("#form3 #btnPackage3").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("3", package3);
		}
		else {
			$("#form3 #btnPackage3").attr("bom_cd", "");
			$("#form3 #btnPackage3").attr("bom_nm", "");
			$("#form3 #btnPackage3").attr("bom_tp", "");
			$("#form3 #btnPackage3").attr("vaccum", vaccum);
			$("#form3 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form3 #img_package3").html("");
		}				
	}
	else if(recipe_tp == "004") {
		$("#form4 #info1").val(info1);
		$("#form4 #info2").val(info2);
		$("#form4 #info3").val(info3);
		$("#form4 #info4").val(info4);
		//$("#form4 #info5").val(info5);
		//$("#form4 #info6").val(info6);
		$("#form4 #info7").val(info7);		
		$("#form4 #info8").val(info8);
		$("#form4 #info9").val(info9);
		
		if(info10 != "") {
			$("#form4 #info10").html(info10_nm + "/" + info10_qntt + "ml");
			
			$("#form4 #info10").attr("code", info10);
			$("#form4 #info10").attr("name", info10_nm);
			$("#form4 #info10").attr("price", info10_price);
			$("#form4 #info10").attr("value", info10_qntt);	
		}						
		else {
			$("#form4 #info10").html("선택 없음");			
			$("#form4 #info10").attr("code", "");
			$("#form4 #info10").attr("price", "");
			$("#form4 #info10").attr("value", "");
		}
		
		$("#form4 #info11").val(info11);
		
		if(package1) {
			$("#form4 #btnPackage1").attr("bom_cd", package1);
			$("#form4 #btnPackage1").attr("bom_nm", package1_nm);
			$("#form4 #btnPackage1").attr("bom_tp", package1_tp);
			$("#form4 #btnPackage1").attr("vaccum", vaccum);
			$("#form4 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("1", package1);
		}
		else {
			$("#form4 #btnPackage1").attr("bom_cd", "");
			$("#form4 #btnPackage1").attr("bom_nm", "");
			$("#form4 #btnPackage1").attr("bom_tp", "");
			$("#form4 #btnPackage1").attr("vaccum", vaccum);
			$("#form4 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form4 #img_package1").html("");
		}
		
		if(package2) {
			$("#form4 #btnPackage2").attr("bom_cd", package2);
			$("#form4 #btnPackage2").attr("bom_nm", package2_nm);
			$("#form4 #btnPackage2").attr("bom_tp", package2_tp);
			$("#form4 #btnPackage2").attr("vaccum", vaccum);
			$("#form4 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			getItemInfo("2", package2);
		}
		else {
			$("#form4 #btnPackage2").attr("bom_cd", "");
			$("#form4 #btnPackage2").attr("bom_nm", "");
			$("#form4 #btnPackage2").attr("bom_tp", "");
			$("#form4 #btnPackage2").attr("vaccum", vaccum);
			$("#form4 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form4 #img_package2").html("");
		}
		
		if(package3) {
			$("#form4 #btnPackage3").attr("bom_cd", package3);
			$("#form4 #btnPackage3").attr("bom_nm", package3_nm);
			$("#form4 #btnPackage3").attr("bom_tp", package3_tp);
			$("#form4 #btnPackage3").attr("vaccum", vaccum);
			$("#form4 #btnPackage3").html("<span>" + package1_nm + "</span>");	
			
			getItemInfo("3", package3);
		}
		else {
			$("#form4 #btnPackage3").attr("bom_cd", "");
			$("#form4 #btnPackage3").attr("bom_nm", "");
			$("#form4 #btnPackage3").attr("bom_tp", "");
			$("#form4 #btnPackage3").attr("vaccum", vaccum);
			$("#form4 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
						
			$("#form4 #img_package3").html("");
		}				
	}
}

//function setDrug(herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price, absorption_rate) {
function setDrug(herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price_a, price_b, price_c, price_d, price_e, absorption_rate) {	
	var price_class = $("#btnSearchHospital").attr("price_class");
	if(!price_class) {
		alert("한의원을 먼저 선택해 주세요.");
		return;
	}
	
	var price = price_a;
	
	if(price_class == "B") {
		price = price_b;
	}
	else if(price_class == "C") {
		price = price_c;
	}
	else if(price_class == "D") {
		price = price_d;
	}
	else if(price_class == "E") {
		price = price_e;
	}	
	
	var length = $("input:checkbox[name='drug_cd']").length;
	console.log("length : " + length);
	console.log("drug_cd : " + drug_cd);
	
	for(var i = 0; i < length; i++) {			
		if(drug_cd == $("input:checkbox[name='drug_cd']").eq(i).val()) {
			//alert(drug_nm + "(" + drug_cd + ")는 이미 추가된 약재입니다.");
			//return;
			toastMsg('이미 선택한 약재입니다.','ty3');
			return;
		}
	}
	
	var isPoison = false;
	if(POISON.indexOf(herbs_cd) > -1) {
		isPoison = true;
	}
	
	var sHTML = "";
	
	sHTML += "<tr>";
	sHTML += "	<td><label class='inp_checkbox'><input type='checkbox' class='chk' name='drug_cd' value='" + drug_cd + "' data-herbs='" + herbs_cd + "' data-name='" + drug_nm + "' data-rate='" + absorption_rate + "' data-price='" + price + "' data-stock='" + stock_qntt + "' data-origin='" + origin + "' /><span class='d'></span></label></td>";
	sHTML += "	<td class='no'></td>";
	sHTML += "	<td>" + drug_nm + "<span class='poison'></span>";
	
	if(isPoison) {
		sHTML += "		<button type='button' class='btn_cir1 ty2' onclick=\"toastMsg('독성 약재가 있습니다','ty2')\"><span>독</span></button>";
	}				
	
	sHTML += "	</td>";
	
	sHTML += "	<td>";
	
	if(turn == "2") {
		sHTML += "		<select name='turn' class='select1 w100p'>";
		sHTML += "			<option value='1'>선전</option>";
		sHTML += "			<option value='2' selected>일반</option>";
		sHTML += "			<option value='3'>후하</option>";
		sHTML += "		</select>";
	}
	else if(turn == "3") {
		sHTML += "		<select name='turn' class='select1 w100p'>";
		sHTML += "			<option value='1'>선전</option>";
		sHTML += "			<option value='2'>일반</option>";
		sHTML += "			<option value='3' selected>후하</option>";
		sHTML += "		</select>";
	}
	else {
		sHTML += "		<select name='turn' class='select1 w100p'>";
		sHTML += "			<option value='1'>선전</option>";
		sHTML += "			<option value='2'>일반</option>";
		sHTML += "			<option value='3'>후하</option>";
		sHTML += "		</select>";	
	}
	
	sHTML += "	</td>";
	sHTML += "	<td>" + origin + "</td>";
	sHTML += "	<td class='ta-r price'>" + NumberFormat(price) + "원</td>";
	//sHTML += "	<td class='ta-r'>" + NumberFormat(stock_qntt) + "g</td>";
	sHTML += "	<td class='ta-r'><div class='fx'><input type='text' name='chup1' class='inp_txt chup1' style='width:80%' value='" + chup1 + "'>g</div></td>";
	sHTML += "	<td class='ta-r total_qntt'></td>";
	sHTML += "	<td class='ta-r total_price'></td>";
	sHTML += "</tr>";
	
	$("#tbody").append(sHTML);
	
	$('.chup1').keyup(function(e) {
		console.log("chup1");
		
		InpuOnlyFloat(this);
		
		calc();
	});	
	
	$('.chup1').click(function(e) {
		$(this).select();
	});	
	
	checkConflict();
	
	calc();
}

function checkConflict() {
	var length = $("input:checkbox[name='drug_cd']").length;

	var herbs_cd_array = "";
	for(var i = 0; i < length; i++) {			
		if(herbs_cd_array != "") {
			herbs_cd_array += ",";
		}
		
		herbs_cd_array += $("input:checkbox[name='drug_cd']").eq(i).data("herbs");
	}
	
	console.log("herbs_cd_array : " + herbs_cd_array);
	
	
	for(var i = 0; i < length; i++) {			
		var herbs = $("input:checkbox[name='drug_cd']").eq(i).data("herbs");
		
		var cnt = 0;
		
		for(var k = 0; k < CONFLICT.length; k++) {
			if(CONFLICT[k].HERBS_CD.indexOf(herbs) > -1) {	//현재 본초코드가 상극 알람에 존재
				var temp = CONFLICT[k].HERBS_CD.split(",");
				
				for(var z = 0; z < temp.length; z++) {
					if(temp[z] != herbs) {
						if(herbs_cd_array.indexOf(temp[z]) > -1) {
							cnt++;
							
							if(cnt > 0) {
								break;
							}
						}	
					}						
				}
			}
			
			if(cnt > 0) {
				break;
			}
		}
		
		console.log("cnt : " + cnt);
		
		if(cnt > 0) {
			$(".poison").eq(i).html("<button type='button' class='btn_cir1 ty1' onclick=\"toastMsg('상극 약재가 있습니다','ty1')\"><span>상</span></button>");
		}
		else {
			$(".poison").eq(i).html("");
		}			
	}
}

function calc() {
	console.log("calc()");
	
	var recipe_tp = GetRadioValue("recipe_tp");
	var length = $("input:checkbox[name='drug_cd']").length;
	
	//탕전
	if(recipe_tp == "001") {
		var chup = parseFloat($("#form1 #info1").val());	//첩수
		var pack = parseFloat($("#form1 #info2").val());	//팩수
		var pack_size = parseFloat($("#form1 #info3").val());	//팩 용량
				
		var sum_total_chup = 0;
		var sum_total_qntt = 0;
		var sum_total_price = 0;
		var sum_water = pack_size * (pack + 2);					//추출량 = 팩 용량 * (팩 수 + 2) =
		
		for(var i = 0; i < length; i++) {
			$(".no").eq(i).html(i + 1);
			
			var chup1 = parseFloat($("input[name='chup1']").eq(i).val());
			var price = parseFloat($("input:checkbox[name='drug_cd']").eq(i).data("price"));
			var rate = parseFloat($("input:checkbox[name='drug_cd']").eq(i).data("rate"));
						
			var total_qntt = chup1 * chup;
			total_qntt = parseFloat(total_qntt).toFixed(2);
			total_qntt = parseFloat(total_qntt);	
			
			$(".total_qntt").eq(i).html(NumberFormat(total_qntt) + "g");
			
			//물량
			var water = 0;
			water += total_qntt;							//약재흡수량 = (총 약재량) 합계 = (첩당 약재량 * 첩의 개수) 합계 =				
			water += parseInt(total_qntt * 100 / rate);		//증발량 = (부위별 흡수량) 합계 =
			
			sum_water += water;
			//물량
			
			
			var total_price = price * total_qntt;
			total_price = parseFloat(total_price).toFixed(0);
			total_price = parseFloat(total_price);			
			
			$(".total_price").eq(i).html(NumberFormat(total_price) + "원");
			
			sum_total_chup += chup1;
			sum_total_qntt += total_qntt;
			sum_total_price += total_price;
		}	
		
		sum_water = parseInt(sum_water);	
		
		$("#sum_total_chup").html(NumberFormat(sum_total_chup.toFixed(2)));
		$("#sum_total_qntt").html(NumberFormat(sum_total_qntt.toFixed(2)));
		$("#sum_total_price").html(NumberFormat(sum_total_price.toFixed(0)));
		
		$("#form1 #info6").html(NumberFormat(sum_water));				
	}
	//제환
	else if(recipe_tp == "002") {
		var chup = parseInt($("#form2 #info1").val());	//첩수
		console.log("chup : " + chup);	
		
		var sum_total_chup = 0;
		var sum_total_qntt = 0;
		var sum_total_price = 0;
		
		for(var i = 0; i < length; i++) {
			$(".no").eq(i).html(i + 1);
			
			var chup1 = parseFloat($("input[name='chup1']").eq(i).val());
			var price = parseFloat($("input:checkbox[name='drug_cd']").eq(i).data("price"));
			var rate = parseFloat($("input:checkbox[name='drug_cd']").eq(i).data("rate"));
						
			var total_qntt = chup1 * chup;
			total_qntt = parseFloat(total_qntt).toFixed(2);
			total_qntt = parseFloat(total_qntt);
			
			$(".total_qntt").eq(i).html(NumberFormat(total_qntt) + "g");
			
			var total_price = price * total_qntt;
			total_price = parseFloat(total_price).toFixed(0);
			total_price = parseFloat(total_price);
			
			$(".total_price").eq(i).html(NumberFormat(total_price) + "원");
			
			sum_total_chup += chup1;
			sum_total_qntt += total_qntt;
			sum_total_price += total_price;
		}	
		
		$("#sum_total_chup").html(NumberFormat(sum_total_chup.toFixed(2)));
		$("#sum_total_qntt").html(NumberFormat(sum_total_qntt.toFixed(2)));
		$("#sum_total_price").html(NumberFormat(sum_total_price.toFixed(0)));
		
		$("#form2 #info6").html(NumberFormat((sum_total_qntt * 0.1).toFixed(2)));
		$("#form2 #info7").html(NumberFormat((sum_total_qntt * 0.1).toFixed(2)));
		$("#form2 #info8").html(NumberFormat((sum_total_qntt * 0.8).toFixed(2)));	
	}
	//연조엑스
	else if(recipe_tp == "003") {
		var chup = $("#form3 #info1").val();				
		var po = $("#form3 #info2").val();
		
		chup = parseInt(chup);
		po = parseInt(po);
		
		var sum_total_chup = 0;
		var sum_total_qntt = 0;
		var sum_total_price = 0;
		
		for(var i = 0; i < length; i++) {
			$(".no").eq(i).html(i + 1);
			
			var chup1 = parseInt($("input[name='chup1']").eq(i).val());
			var price = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("price"));
			var rate = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("rate"));
			
			console.log("chup1 : " + chup1);
			console.log("price : " + price);
			
			var total_qntt = chup1 * chup;
			console.log("total_qntt : " + total_qntt);
			$(".total_qntt").eq(i).html(NumberFormat(total_qntt) + "g");
			
			var total_price = price * total_qntt;
			console.log("total_price : " + total_price);
			$(".total_price").eq(i).html(NumberFormat(total_price) + "원");
			
			sum_total_chup += chup1;
			sum_total_qntt += total_qntt;
			sum_total_price += total_price;
		}	
		
		$("#sum_total_chup").html(NumberFormat(sum_total_chup.toFixed(0)));
		$("#sum_total_qntt").html(NumberFormat(sum_total_qntt.toFixed(0)));
		$("#sum_total_price").html(NumberFormat(sum_total_price.toFixed(0)));		
	}
}

function allCheck(obj) {
	if($(obj).prop("checked")) {
		$(".chk").prop("checked", true);
	}
	else {
		$(".chk").prop("checked", false);
	}
}

function deleteDrug() {
	console.log("deleteDrug()");
	
	var length = GetCheckedLength("drug_cd");
	if(length == 0) {
		alert("하나 이상 선택해 주세요.");
		return;
	}
	
	for(var i = 0; i < length; i++) {
		$("input:checkbox[name='drug_cd']:checked").eq(0).closest("tr").remove();
	}	
	
	checkConflict();
	
	calc();
}

function deleteDrugAll() {
	console.log("deleteDrugAll()");
	
	var length = $("input:checkbox[name='drug_cd']").length;		
	for(var i = 0; i < length; i++) {
		$("input:checkbox[name='drug_cd']").eq(0).closest("tr").remove();
	}	
	
	checkConflict();
	
	calc();
}

function setRecipeTp(recipe_tp) {
	console.log("setRecipeTp()", recipe_tp);
	
	var hospital_id = $("#btnSearchHospital").attr("hospital_id");
	if(!hospital_id) hospital_id = "";
	
	$("#form1").hide();
	$("#form2").hide();
	$("#form3").hide();
	$("#form4").hide();	
	
	if(recipe_tp == "001") {
		$("#form1").show();
	}
	else if(recipe_tp == "002") {
		$("#form2").show();
	}
	else if(recipe_tp == "003") {
		$("#form3").show();
	}
	else if(recipe_tp == "004") {
		$("#form4").show();
	}
	
	if(recipe_tp == "004") {
		$("#tab_add3").hide();
		$("#tab_add4").hide();
		
		$("#tab_drug").html("첩약처방");
		
		$("#tab2_c2_iframe").attr("src", "/common/search_insurance");	
		
		tab(".tab_c2", 1);
	}
	else {		
		$("#tab_add3").show();
		$("#tab_add4").show();
		
		$("#tab_drug").html("나의처방");
		
		$("#tab2_c2_iframe").attr("src", "/common/search_myrecipe?RECIPE_TP=" + recipe_tp + "&HOSPITAL_ID=" + hospital_id);
		$("#tab2_c3_iframe").attr("src", "/common/search_prerecipe?RECIPE_TP=" + recipe_tp + "&HOSPITAL_ID=" + hospital_id);
	}		
}

function setDrink(code, name, value, price) {
	console.log("setDrink code : " + code);
	console.log("setDrink name : " + name);
	console.log("setDrink value : " + value);
	console.log("setDrink price : " + price);
	
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		if(code) {		
			$("#form1 #info10").attr("code", code);
			$("#form1 #info10").attr("name", name);
			$("#form1 #info10").attr("value", value);
			$("#form1 #info10").attr("price", price);		
		
			$("#form1 #info10").html(name + "/" + NumberFormat(value) + "mL");
		}
		else {		
			$("#form1 #info10").attr("code", "");
			$("#form1 #info10").attr("name", "");
			$("#form1 #info10").attr("value", "");
			$("#form1 #info10").attr("price", "");
		
			$("#form1 #info10").html(name);	
		}
	}
	else if(recipe_tp == "004") {
		if(code) {		
			$("#form4 #info10").attr("code", code);
			$("#form4 #info10").attr("name", name);
			$("#form4 #info10").attr("value", value);
			$("#form4 #info10").attr("price", price);		
		
			$("#form4 #info10").html(name + "/" + NumberFormat(value) + "mL");
		}
		else {		
			$("#form4 #info10").attr("code", "");
			$("#form4 #info10").attr("name", "");
			$("#form4 #info10").attr("value", "");
			$("#form4 #info10").attr("price", "");
		
			$("#form4 #info10").html(name);	
		}
	}	
	
	calc();
}

function showDrink() {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		var url = "/common/search_drink";
		url += "?code=" +  $.trim($("#form1 #info9").attr("code"));
		url += "&value=" +  $.trim($("#form1 #info9").attr("value"));
		
		showPopup(400, 300, url);	
	}
	else if(recipe_tp == "004") {
		var url = "/common/search_drink";
		url += "?code=" +  $.trim($("#form4 #info9").attr("code"));
		url += "&value=" +  $.trim($("#form4 #info9").attr("value"));
		
		showPopup(400, 300, url);	
	}
	
}

function showMarking() {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		var marking_tp = $("#form1 #btnMarking").attr("marking_tp");
		var marking_text = $("#form1 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
	else if(recipe_tp == "002") {
		var marking_tp = $("#form2 #btnMarking").attr("marking_tp");
		var marking_text = $("#form2 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
	else if(recipe_tp == "003") {
		var marking_tp = $("#form3 #btnMarking").attr("marking_tp");
		var marking_text = $("#form3 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
	else if(recipe_tp == "004") {
		var marking_tp = $("#form4 #btnMarking").attr("marking_tp");
		var marking_text = $("#form4 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
}

function setMarking(marking_tp, marking_text) {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {		
		$("#form1 #btnMarking").attr("marking_tp", marking_tp);
		$("#form1 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form1 #btnMarking").html("<span>" + marking_text + "</span>");								
		$("#form1 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
	else if(recipe_tp == "002") {		
		$("#form2 #btnMarking").attr("marking_tp", marking_tp);
		$("#form2 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form2 #btnMarking").html("<span>" + marking_text + "</span>");						
		$("#form2 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
	else if(recipe_tp == "003") {		
		$("#form3 #btnMarking").attr("marking_tp", marking_tp);
		$("#form3 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form3 #btnMarking").html("<span>" + marking_text + "</span>");
		$("#form3 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
	else if(recipe_tp == "004") {		
		$("#form4 #btnMarking").attr("marking_tp", marking_tp);
		$("#form4 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form4 #btnMarking").html("<span>" + marking_text + "</span>");								
		$("#form4 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
}

function showPackage(pkg) {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		var bom_cd = $("#form1 #btnPackage" + pkg).attr("bom_cd");
		var vaccum = $("#form1 #btnPackage" + pkg).attr("vaccum");
		if(!vaccum) vaccum = "";
		if(!bom_cd) bom_cd = "";
		
		var bom_tp = "001";
		if(pkg == "2") {
			bom_tp = "002";
		}
		else if(pkg == "3") {
			bom_tp = "003";
		}		
		
		var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
		console.log("showPackage001() : " + url);
		showPopup(800, 600, url);
	}
	else if(recipe_tp == "002") {
		var bom_cd = $("#form2 #btnPackage" + pkg).attr("bom_cd");
		var vaccum = $("#form2 #btnPackage" + pkg).attr("vaccum");
		if(!bom_cd) bom_cd = "";
		if(!vaccum) vaccum = "";
		
		if(pkg == "4") {
			var bom_tp = "003";
			
			var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
			console.log("showPackage001() : " + url);
			showPopup(800, 600, url);	
			return;
		}			
			
		var info3 = $.trim($("#form2 #info3").val());
		if(info3 == "004") {
			//탄자대
			if(pkg == "3") {
				var bom_tp = "";
				var package2 = $("#form2 #btnPackage2").attr("bom_tp");
				if(package2 == "012") {		//지퍼백
					alert("지퍼백은 포장상자를 선택할 수 없습니다.");
					return;
				}
				else if(package2 == "009") {	//크리스탈 청병
					bom_tp = "013";
				}
				else if(package2 == "010") {	//일반청병
					bom_tp = "014";
				}
				else {					
					alert("소포장을 선택해 주세요.");
					alert(package2);
					return;	
				}

				var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
				console.log("showPackage001() : " + url);
				showPopup(800, 600, url);	
			}
			else if(pkg == "2") {
				var bom_tp = "";
				var package1 = $("#form2 #btnPackage1").attr("bom_tp");
				if(package1 == "006") {		//내지
					bom_tp = "009, 010, 012";
				}
				else if(package1 == "007") {	//사탕포장
					bom_tp = "011, 012";
				}
				else if(package1 == "008") {	//벌크포장
					alert("벌크포장은 소포장을 선택할 수 없습니다.");
					return;
				}
				else {
					alert("내지를 선택해 주세요.");
					return;	
				}				

				var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
				console.log("showPackage001() : " + url);
				showPopup(800, 600, url);	
			}
			else if(pkg == "1") {
				var bom_tp = "006, 007, 008";
				
				var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
				console.log("showPackage001() : " + url);
				showPopup(800, 600, url);	
			}
		}
		else {	//탄자대가 아닌 경우
			if(pkg == "3") {
				var bom_tp = "";
				var btnPackage2 = $("#form2 #btnPackage2").attr("bom_tp");
				if(btnPackage2 == "015") {		//환제 소포장
					bom_tp = "018";
				}
				else if(btnPackage2 == "016") {	//환제 스틱
					bom_tp = "019";
				}
				else if(btnPackage2 == "017") {	//자일리통
					alert("자일리톨통은 소포장을 선택할 수 없습니다.");
					return;
				}
				else {
					alert("소포장을 선택해 주세요.");
					return;	
				}
				
				var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
				console.log("showPackage001() : " + url);
				showPopup(800, 600, url);	
			}
			else if(pkg == "2") {
				var bom_tp = "015, 016, 017";
				
				var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
				console.log("showPackage001() : " + url);
				showPopup(800, 600, url);	
			}
			else if(pkg == "1") {
				alert("탄자대만 내지 선택이 가능합니다.");
				return;	
			}
		}
	}
	else if(recipe_tp == "003") {
		var bom_cd = $("#form4 #btnPackage" + pkg).attr("bom_cd");
		var vaccum = $("#form4 #btnPackage" + pkg).attr("vaccum");
		if(!vaccum) vaccum = "";
		
		var bom_tp = "004";
		if(pkg == "2") {
			bom_tp = "005";
		}
		else if(pkg == "3") {
			bom_tp = "003";
		}
		
		if(!bom_cd) bom_cd = "";
		var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
		console.log("showPackage001() : " + url);
		showPopup(800, 600, url);
	}
	else if(recipe_tp == "004") {
		var bom_cd = $("#form4 #btnPackage" + pkg).attr("bom_cd");
		var vaccum = $("#form4 #btnPackage" + pkg).attr("vaccum");
		if(!vaccum) vaccum = "";
		
		var bom_tp = "001";
		if(pkg == "2") {
			bom_tp = "002";
		}
		else if(pkg == "3") {
			bom_tp = "003";
		}
		
		if(!bom_cd) bom_cd = "";
		var url = "/common/pop_package?pkg=" + pkg + "&bom_tp=" + bom_tp + "&bom_cd=" + bom_cd + "&vaccum=" + vaccum;
		console.log("showPackage001() : " + url);
		showPopup(800, 600, url);
	}
}

function setPackage(pkg, bom_cd, bom_nm, bom_tp, vaccum) {
	console.log("setPackage", pkg, bom_cd, bom_nm, vaccum);
	
	var recipe_tp = GetRadioValue("recipe_tp");	
	if(recipe_tp == "001") {
		$("#form1 #btnPackage" + pkg).attr("item_info", "");
		
		if(bom_cd) {
			$("#form1 #btnPackage" + pkg).attr("bom_cd", bom_cd);
			$("#form1 #btnPackage" + pkg).attr("bom_nm", bom_nm);
			$("#form1 #btnPackage" + pkg).attr("bom_tp", bom_tp);
			$("#form1 #btnPackage" + pkg).attr("vaccum", vaccum);
			
			$("#form1 #btnPackage" + pkg).html("<span>" + bom_nm + "</span>");
			$("#form1 #img_package" + pkg).html("");	
			
			getItemInfo(pkg, bom_cd);
		}
		else {
			$("#form1 #btnPackage" + pkg).attr("bom_cd", "");
			$("#form1 #btnPackage" + pkg).attr("bom_nm", "");
			$("#form1 #btnPackage" + pkg).attr("bom_tp", "");
			$("#form1 #btnPackage" + pkg).attr("vaccum", "");	
			
			$("#form1 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>");
			$("#form1 #img_package" + pkg).html("");	
			
			calc();		
		}	
	}
	else if(recipe_tp == "002") {
		$("#form2 #btnPackage" + pkg).attr("item_info", "");
		
		if(pkg == "1") {
			$("#form2 #btnPackage2").attr("bom_cd", "");
			$("#form2 #btnPackage2").attr("bom_nm", "");
			$("#form2 #btnPackage2").attr("bom_tp", "");
			$("#form2 #btnPackage2").attr("vaccum", "");
			$("#form2 #btnPackage2").html("<span>" + "선택하세요" + "</span>");
			$("#form2 #img_package2").html("");	
			
			$("#form2 #btnPackage3").attr("bom_cd", "");
			$("#form2 #btnPackage3").attr("bom_nm", "");
			$("#form2 #btnPackage3").attr("bom_tp", "");
			$("#form2 #btnPackage3").attr("vaccum", "");
			$("#form2 #btnPackage3").html("<span>" + "선택하세요" + "</span>");
			$("#form2 #img_package3").html("");	
			
			calc();		
		}
		else if(pkg == "2") {
			$("#form2 #btnPackage3").attr("bom_cd", "");
			$("#form2 #btnPackage3").attr("bom_nm", "");
			$("#form2 #btnPackage3").attr("bom_tp", "");
			$("#form2 #btnPackage3").attr("vaccum", "");
			$("#form2 #btnPackage3").html("<span>" + "선택하세요" + "</span>");
			$("#form2 #img_package3").html("");	
			
			calc();		
		}
		
		if(bom_cd) {
			$("#form2 #btnPackage" + pkg).attr("bom_cd", bom_cd);
			$("#form2 #btnPackage" + pkg).attr("bom_nm", bom_nm);
			$("#form2 #btnPackage" + pkg).attr("bom_tp", bom_tp);
			$("#form2 #btnPackage" + pkg).attr("vaccum", vaccum);
			$("#form2 #btnPackage" + pkg).html("<span>" + bom_nm + "</span>");
			$("#form2 #img_package" + pkg).html("");	
			
			getItemInfo(pkg, bom_cd);
		}
		else {
			$("#form2 #btnPackage" + pkg).attr("bom_cd", "");
			$("#form2 #btnPackage" + pkg).attr("bom_nm", "");
			$("#form2 #btnPackage" + pkg).attr("bom_tp", "");
			$("#form2 #btnPackage" + pkg).attr("vaccum", "");	
			
			$("#form2 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>");
			$("#form2 #img_package" + pkg).html("");	
			
			calc();		
		}	
	}
	else if(recipe_tp == "003") {	
		$("#form3 #btnPackage" + pkg).attr("item_info", "");
			
		if(bom_cd) {
			$("#form3 #btnPackage" + pkg).attr("bom_cd", bom_cd);
			$("#form3 #btnPackage" + pkg).attr("bom_nm", bom_nm);
			$("#form3 #btnPackage" + pkg).attr("bom_tp", bom_tp);
			$("#form3 #btnPackage" + pkg).attr("vaccum", vaccum);				
			$("#form3 #btnPackage" + pkg).html("<span>" + bom_nm + "</span>");
			$("#form3 #img_package" + pkg).html("");	
			
			getItemInfo(pkg, bom_cd);
		}
		else {
			$("#form3 #btnPackage" + pkg).attr("bom_cd", "");
			$("#form3 #btnPackage" + pkg).attr("bom_nm", "");
			$("#form3 #btnPackage" + pkg).attr("bom_tp", "");
			$("#form3 #btnPackage" + pkg).attr("vaccum", "");				
			$("#form3 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>");
			$("#form3 #img_package" + pkg).html("");	
			
			calc();		
		}	
	}
	else if(recipe_tp == "004") {
		$("#form4 #btnPackage" + pkg).attr("item_info", "");
		
		if(bom_cd) {
			$("#form4 #btnPackage" + pkg).attr("bom_cd", bom_cd);
			$("#form4 #btnPackage" + pkg).attr("bom_nm", bom_nm);
			$("#form4 #btnPackage" + pkg).attr("bom_tp", bom_tp);
			$("#form4 #btnPackage" + pkg).attr("vaccum", vaccum);				
			$("#form4 #btnPackage" + pkg).html("<span>" + bom_nm + "</span>");
			$("#form4 #img_package" + pkg).html("");	
			
			getItemInfo(pkg, bom_cd);
		}
		else {
			$("#form4 #btnPackage" + pkg).attr("bom_cd", "");
			$("#form4 #btnPackage" + pkg).attr("bom_nm", "");
			$("#form4 #btnPackage" + pkg).attr("bom_tp", "");
			$("#form4 #btnPackage" + pkg).attr("vaccum", "");				
			$("#form4 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>");
			$("#form4 #img_package" + pkg).html("");
			
			calc();			
		}	
	}
}		
	
/*
function setPackage(pkg, item_cd, item_nm, qntt, price, make_price, filepath, vaccum) {	
	var recipe_tp = GetRadioValue("recipe_tp");	
	
	if(recipe_tp == "001" || recipe_tp == "004") {
		if(pkg == "2" && vaccum == "Y") {
			console.log("진공포장");
			if($("#msg_make").length > 0) {
				var msg_make = GetValue("msg_make");
				msg_make = msg_make.replace("[진공포장]", "");
				console.log("msg_make", msg_make);
				msg_make = "[진공포장] " + msg_make;
				$("#msg_make").val(msg_make);	
			}				
		}
		else if(pkg == "2" && vaccum == "N") {
			console.log("진공포장X");
			if($("#msg_make").length > 0) {
				var msg_make = GetValue("msg_make");
				msg_make = msg_make.replace("[진공포장]", "");
				console.log("msg_make", msg_make);
				$("#msg_make").val(msg_make);	
			}	
		}  	
	}		
	
	if(UUID) {
		if(recipe_tp == "001" || recipe_tp == "004") {
			$("#btnPackage" + pkg + "_" + UUID).attr("item_cd", item_cd);
			$("#btnPackage" + pkg + "_" + UUID).attr("item_nm", item_nm);
			$("#btnPackage" + pkg + "_" + UUID).attr("qntt", qntt);
			$("#btnPackage" + pkg + "_" + UUID).attr("price", price);
			$("#btnPackage" + pkg + "_" + UUID).attr("make_price", make_price);
			$("#btnPackage" + pkg + "_" + UUID).attr("vaccum", vaccum);
			
			if(item_nm) {
				$("#btnPackage" + pkg + "_" + UUID).removeClass("blue");
				$("#btnPackage" + pkg + "_" + UUID).addClass("gray");
				
				$("#btnPackage" + pkg + "_" + UUID).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#btnPackage" + pkg + "_" + UUID).removeClass("gray");
				$("#btnPackage" + pkg + "_" + UUID).addClass("blue");				
				$("#btnPackage" + pkg + "_" + UUID).html("<span>" + "선택하세요" + "</span>")
			}			
			
			if(filepath) {
				$("#img_package" + pkg + "_" + UUID).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#img_package" + pkg + "_" + UUID).html("");
			}								
			
			if(pkg == "1") {
				$("#btnPackage2" + "_" + UUID).html("<span>선택하세요</span>");	
				$("#btnPackage3" + "_" + UUID).html("<span>선택하세요</span>");	
				
				$("#btnPackage2" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage2" + "_" + UUID).attr("item_nm", "");					
	
				$("#btnPackage3" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage3" + "_" + UUID).attr("item_nm", "");
				
				$("#btnPackage2" + "_" + UUID).removeClass("gray");
				$("#btnPackage3" + "_" + UUID).removeClass("gray");
	
				$("#btnPackage2" + "_" + UUID).addClass("blue");
				$("#btnPackage3" + "_" + UUID).addClass("blue");
	
				$("#img_package2" + "_" + UUID).html("");
				$("#img_package3" + "_" + UUID).html("");
			}
			else if(pkg == "2") {	
				$("#btnPackage3" + "_" + UUID).html("<span>선택하세요</span>");
				
				$("#btnPackage3" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage3" + "_" + UUID).attr("item_nm", "");
				
				$("#btnPackage3" + "_" + UUID).removeClass("gray");
	
				$("#btnPackage3" + "_" + UUID).addClass("blue");
	
				$("#img_package3" + "_" + UUID).html("");
			}
		}
		else if(recipe_tp == "002") {										
			$("#btnPackage" + pkg + "_" + UUID).attr("item_cd", item_cd);
			$("#btnPackage" + pkg + "_" + UUID).attr("item_nm", item_nm);
			$("#btnPackage" + pkg + "_" + UUID).attr("qntt", qntt);
			$("#btnPackage" + pkg + "_" + UUID).attr("price", price);
			$("#btnPackage" + pkg + "_" + UUID).attr("make_price", make_price);
			
			if(item_nm) {
				$("#btnPackage" + pkg + "_" + UUID).removeClass("blue");
				$("#btnPackage" + pkg + "_" + UUID).addClass("gray");
				
				$("#btnPackage" + pkg + "_" + UUID).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#btnPackage" + pkg + "_" + UUID).removeClass("gray");
				$("#btnPackage" + pkg + "_" + UUID).addClass("blue");
				
				$("#btnPackage" + pkg + "_" + UUID).html("<span>" + "선택하세요" + "</span>")
			}
			
			if(filepath) {
				$("#img_package" + pkg + "_" + UUID).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#img_package" + pkg + "_" + UUID).html("");
			}	
			
			if(pkg == "1") {
				$("#btnPackage2" + "_" + UUID).html("<span>선택하세요</span>");	
				$("#btnPackage3" + "_" + UUID).html("<span>선택하세요</span>");	
				
				$("#btnPackage2" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage2" + "_" + UUID).attr("item_nm", "");					
	
				$("#btnPackage3" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage3" + "_" + UUID).attr("item_nm", "");
				
				$("#btnPackage2" + "_" + UUID).removeClass("gray");
				$("#btnPackage3" + "_" + UUID).removeClass("gray");
	
				$("#btnPackage2" + "_" + UUID).addClass("blue");
				$("#btnPackage3" + "_" + UUID).addClass("blue");
	
				$("#img_package2" + "_" + UUID).html("");
				$("#img_package3" + "_" + UUID).html("");
			}
			else if(pkg == "2") {	
				$("#btnPackage3" + "_" + UUID).html("<span>선택하세요</span>");
				
				$("#btnPackage3" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage3" + "_" + UUID).attr("item_nm", "");
				
				$("#btnPackage3" + "_" + UUID).removeClass("gray");
	
				$("#btnPackage3" + "_" + UUID).addClass("blue");
	
				$("#img_package3" + "_" + UUID).html("");
			}
		}
		else if(recipe_tp == "003") {							
			$("#btnPackage" + pkg + "_" + UUID).attr("item_cd", item_cd);
			$("#btnPackage" + pkg + "_" + UUID).attr("item_nm", item_nm);
			$("#btnPackage" + pkg + "_" + UUID).attr("qntt", qntt);
			$("#btnPackage" + pkg + "_" + UUID).attr("price", price);
			$("#btnPackage" + pkg + "_" + UUID).attr("make_price", make_price);
			
			if(item_nm) {
				$("#btnPackage" + pkg + "_" + UUID).removeClass("blue");
				$("#btnPackage" + pkg + "_" + UUID).addClass("gray");
				
				$("#btnPackage" + pkg + "_" + UUID).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#btnPackage" + pkg + "_" + UUID).removeClass("gray");
				$("#btnPackage" + pkg + "_" + UUID).addClass("blue");
				
				$("#btnPackage" + pkg + "_" + UUID).html("<span>" + "선택하세요" + "</span>")
			}
			
			if(filepath) {
				$("#img_package" + pkg + "_" + UUID).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#img_package" + pkg + "_" + UUID).html("");
			}	
			
			if(pkg == "1") {
				$("#btnPackage2" + "_" + UUID).html("<span>선택하세요</span>");	
				$("#btnPackage3" + "_" + UUID).html("<span>선택하세요</span>");	
				
				$("#btnPackage2" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage2" + "_" + UUID).attr("item_nm", "");					
	
				$("#btnPackage3" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage3" + "_" + UUID).attr("item_nm", "");
				
				$("#btnPackage2" + "_" + UUID).removeClass("gray");
				$("#btnPackage3" + "_" + UUID).removeClass("gray");
	
				$("#btnPackage2" + "_" + UUID).addClass("blue");
				$("#btnPackage3" + "_" + UUID).addClass("blue");
	
				$("#img_package2" + "_" + UUID).html("");
				$("#img_package3" + "_" + UUID).html("");
				
				$("#btnPackage" + pkg + "_" + UUID).attr("vaccum", vaccum);
			}
			else if(pkg == "3") {	
				$("#btnPackage3" + "_" + UUID).html("<span>선택하세요</span>");
				
				$("#btnPackage3" + "_" + UUID).attr("item_cd", "");
				$("#btnPackage3" + "_" + UUID).attr("item_nm", "");
				
				$("#btnPackage3" + "_" + UUID).removeClass("gray");
	
				$("#btnPackage3" + "_" + UUID).addClass("blue");
	
				$("#img_package3" + "_" + UUID).html("");
			}
		}	
	}
	else {
		if(recipe_tp == "001") {
			$("#form1 #btnPackage" + pkg).attr("item_cd", item_cd);
			$("#form1 #btnPackage" + pkg).attr("item_nm", item_nm);
			$("#form1 #btnPackage" + pkg).attr("qntt", qntt);
			$("#form1 #btnPackage" + pkg).attr("price", price);
			$("#form1 #btnPackage" + pkg).attr("make_price", make_price);
			$("#form1 #btnPackage" + pkg).attr("vaccum", vaccum);
			
			if(item_nm) {
				$("#form1 #btnPackage" + pkg).removeClass("blue");
				$("#form1 #btnPackage" + pkg).addClass("gray");
				
				$("#form1 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#form1 #btnPackage" + pkg).removeClass("gray");
				$("#form1 #btnPackage" + pkg).addClass("blue");
				
				$("#form1 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
			}
			
			
			if(filepath) {
				$("#form1 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#form1 #img_package" + pkg).html("");
			}								
			
			if(pkg == "1") {
				$("#form1 #btnPackage2").html("<span>선택하세요</span>");	
				$("#form1 #btnPackage3").html("<span>선택하세요</span>");	
				
				$("#form1 #btnPackage2").attr("item_cd", "");
				$("#form1 #btnPackage2").attr("item_nm", "");					
	
				$("#form1 #btnPackage3").attr("item_cd", "");
				$("#form1 #btnPackage3").attr("item_nm", "");
				
				$("#form1 #btnPackage2").removeClass("gray");
				$("#form1 #btnPackage3").removeClass("gray");
	
				$("#form1 #btnPackage2").addClass("blue");
				$("#form1 #btnPackage3").addClass("blue");
	
				$("#form1 #img_package2").html("");
				$("#form1 #img_package3").html("");
			}
			else if(pkg == "2") {	
				$("#form1 #btnPackage3").html("<span>선택하세요</span>");
				
				$("#form1 #btnPackage3").attr("item_cd", "");
				$("#form1 #btnPackage3").attr("item_nm", "");
				
				$("#form1 #btnPackage3").removeClass("gray");
	
				$("#form1 #btnPackage3").addClass("blue");
	
				$("#form1 #img_package3").html("");
			}
		}
		else if(recipe_tp == "002") {										
			$("#form2 #btnPackage" + pkg).attr("item_cd", item_cd);
			$("#form2 #btnPackage" + pkg).attr("item_nm", item_nm);
			$("#form2 #btnPackage" + pkg).attr("qntt", qntt);
			$("#form2 #btnPackage" + pkg).attr("price", price);
			$("#form2 #btnPackage" + pkg).attr("make_price", make_price);
			
			if(item_nm) {
				$("#form2 #btnPackage" + pkg).removeClass("blue");
				$("#form2 #btnPackage" + pkg).addClass("gray");
				
				$("#form2 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#form2 #btnPackage" + pkg).removeClass("gray");
				$("#form2 #btnPackage" + pkg).addClass("blue");
				
				$("#form2 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
			}
			
			if(filepath) {
				$("#form2 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#form2 #img_package" + pkg).html("");
			}	
			
			if(pkg == "1") {
				$("#form2 #btnPackage2").html("<span>선택하세요</span>");	
				$("#form2 #btnPackage3").html("<span>선택하세요</span>");	
				
				$("#form2 #btnPackage2").attr("item_cd", "");
				$("#form2 #btnPackage2").attr("item_nm", "");					
	
				$("#form2 #btnPackage3").attr("item_cd", "");
				$("#form2 #btnPackage3").attr("item_nm", "");
				
				$("#form2 #btnPackage2").removeClass("gray");
				$("#form2 #btnPackage3").removeClass("gray");
	
				$("#form2 #btnPackage2").addClass("blue");
				$("#form2 #btnPackage3").addClass("blue");
	
				$("#form2 #img_package2").html("");
				$("#form2 #img_package3").html("");
			}
			else if(pkg == "2") {	
				$("#form2 #btnPackage3").html("<span>선택하세요</span>");
				
				$("#form2 #btnPackage3").attr("item_cd", "");
				$("#form2 #btnPackage3").attr("item_nm", "");
				
				$("#form2 #btnPackage3").removeClass("gray");
	
				$("#form2 #btnPackage3").addClass("blue");
	
				$("#form2 #img_package3").html("");
			}
		}
		else if(recipe_tp == "003") {							
			$("#form3 #btnPackage" + pkg).attr("item_cd", item_cd);
			$("#form3 #btnPackage" + pkg).attr("item_nm", item_nm);
			$("#form3 #btnPackage" + pkg).attr("qntt", qntt);
			$("#form3 #btnPackage" + pkg).attr("price", price);
			$("#form3 #btnPackage" + pkg).attr("make_price", make_price);
			
			if(item_nm) {
				$("#form3 #btnPackage" + pkg).removeClass("blue");
				$("#form3 #btnPackage" + pkg).addClass("gray");
				
				$("#form3 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#form3 #btnPackage" + pkg).removeClass("gray");
				$("#form3 #btnPackage" + pkg).addClass("blue");
				
				$("#form3 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
			}
			
			if(filepath) {
				$("#form3 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#form3 #img_package" + pkg).html("");
			}	
			
			if(pkg == "1") {
				$("#form3 #btnPackage2").html("<span>선택하세요</span>");	
				$("#form3 #btnPackage3").html("<span>선택하세요</span>");	
				
				$("#form3 #btnPackage2").attr("item_cd", "");
				$("#form3 #btnPackage2").attr("item_nm", "");					
	
				$("#form3 #btnPackage3").attr("item_cd", "");
				$("#form3 #btnPackage3").attr("item_nm", "");
				
				$("#form3 #btnPackage2").removeClass("gray");
				$("#form3 #btnPackage3").removeClass("gray");
	
				$("#form3 #btnPackage2").addClass("blue");
				$("#form3 #btnPackage3").addClass("blue");
	
				$("#form3 #img_package2").html("");
				$("#form3 #img_package3").html("");
			}
			else if(pkg == "2") {	
				$("#form3 #btnPackage3").html("<span>선택하세요</span>");
				
				$("#form3 #btnPackage3").attr("item_cd", "");
				$("#form3 #btnPackage3").attr("item_nm", "");
				
				$("#form3 #btnPackage3").removeClass("gray");
	
				$("#form3 #btnPackage3").addClass("blue");
	
				$("#form3 #img_package3").html("");
			}
		}	
		else if(recipe_tp == "004") {
			$("#form4 #btnPackage" + pkg).attr("item_cd", item_cd);
			$("#form4 #btnPackage" + pkg).attr("item_nm", item_nm);
			$("#form4 #btnPackage" + pkg).attr("qntt", qntt);
			$("#form4 #btnPackage" + pkg).attr("price", price);
			$("#form4 #btnPackage" + pkg).attr("make_price", make_price);
			$("#form4 #btnPackage" + pkg).attr("vaccum", vaccum);
			
			if(item_nm) {
				$("#form4 #btnPackage" + pkg).removeClass("blue");
				$("#form4 #btnPackage" + pkg).addClass("gray");
				
				$("#form4 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
			}
			else {
				$("#form4 #btnPackage" + pkg).removeClass("gray");
				$("#form4 #btnPackage" + pkg).addClass("blue");
				
				$("#form4 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
			}
			
			
			if(filepath) {
				$("#form4 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
			}
			else {
				$("#form4 #img_package" + pkg).html("");
			}								
			
			if(pkg == "1") {
				$("#form4 #btnPackage2").html("<span>선택하세요</span>");	
				$("#form4 #btnPackage3").html("<span>선택하세요</span>");	
				
				$("#form4 #btnPackage2").attr("item_cd", "");
				$("#form4 #btnPackage2").attr("item_nm", "");					
	
				$("#form4 #btnPackage3").attr("item_cd", "");
				$("#form4 #btnPackage3").attr("item_nm", "");
				
				$("#form4 #btnPackage2").removeClass("gray");
				$("#form4 #btnPackage3").removeClass("gray");
	
				$("#form4 #btnPackage2").addClass("blue");
				$("#form4 #btnPackage3").addClass("blue");
	
				$("#form4 #img_package2").html("");
				$("#form4 #img_package3").html("");
			}
			else if(pkg == "2") {	
				$("#form4 #btnPackage3").html("<span>선택하세요</span>");
				
				$("#form4 #btnPackage3").attr("item_cd", "");
				$("#form4 #btnPackage3").attr("item_nm", "");
				
				$("#form4 #btnPackage3").removeClass("gray");
	
				$("#form4 #btnPackage3").addClass("blue");
	
				$("#form4 #img_package3").html("");
			}
		}	
	}
	
	calc();											
}
*/

function showBox002() {
	console.log("showBox002");
	
	$("#form2 #btnPackage1").attr("bom_cd", "");
	$("#form2 #btnPackage1").attr("bom_nm", "");
	$("#form2 #btnPackage1").attr("bom_tp", "");
	$("#form2 #btnPackage1").attr("vaccum", "");
	$("#form2 #btnPackage1").html("<span>" + "선택하세요" + "</span>");
	$("#form2 #img_package1").html("");	
			
	$("#form2 #btnPackage2").attr("bom_cd", "");
	$("#form2 #btnPackage2").attr("bom_nm", "");
	$("#form2 #btnPackage2").attr("bom_tp", "");
	$("#form2 #btnPackage2").attr("vaccum", "");
	$("#form2 #btnPackage2").html("<span>" + "선택하세요" + "</span>");
	$("#form2 #img_package2").html("");	
	
	$("#form2 #btnPackage3").attr("bom_cd", "");
	$("#form2 #btnPackage3").attr("bom_nm", "");
	$("#form2 #btnPackage3").attr("bom_tp", "");
	$("#form2 #btnPackage3").attr("vaccum", "");
	$("#form2 #btnPackage3").html("<span>" + "선택하세요" + "</span>");
	$("#form2 #img_package3").html("");	
	
	$("#form2 #btnPackage4").attr("bom_cd", "");
	$("#form2 #btnPackage4").attr("bom_nm", "");
	$("#form2 #btnPackage4").attr("bom_tp", "");
	$("#form2 #btnPackage4").attr("vaccum", "");
	$("#form2 #btnPackage4").html("<span>" + "선택하세요" + "</span>");
	$("#form2 #img_package4").html("");	
	
	calc();
}

function changeChup() {							
	var value = $('#form3 #info1 option:selected').text();
	console.log("value : " + value);
	
	value = value.replace(/[^0-9]/g,'');	
	value = parseInt(value) * 3;	
	
	$("#form3 #info2").html(NumberFormat(value));
	
	calc();
}

function getItemInfo(pkg, bom_cd) {		
	console.log("getItemInfo : " + bom_cd);
	if(!bom_cd) return; 
	
	//if(ajaxRunning) return;
	
	var formData = new FormData();
	formData.append("BOM_CD", bom_cd);
	
    var url = "/bom/selectItems";
    $.ajax({
        type:"POST",
        url:url,
        data:formData,
        cache: false,
        processData: false,  // file전송시 필수
        contentType: false,  // file전송시 필수
        beforeSend: function() {
        	//ShowCSS($(".loadingbar"));
        },
        success:function(response) {
        	//HideCSS($(".loadingbar"));

			var items = new Array();
			
            if(response.result == 200) {  
				var recipe_tp = GetRadioValue("recipe_tp");
            	recipe_tp = parseInt(recipe_tp);
            	   
            	var sHTML = "";
            	sHTML += "	<div class='owl-carousel' id='rolling" + pkg + "'>";
            	for(var i = 0; i < response.list.length; i++) {	
            		sHTML += "		<div class='item'>";
        			sHTML += "			<div class='img_prd long'>";
        			
        			var filepath = response.list[i].FILEPATH;
        			var item_nm = response.list[i].ITEM_NM;
        			
        			if(filepath) {
        				sHTML += "				<img src='/download?filepath=" + filepath + "' title='" + item_nm + "' />";
        			}
        			else {
        				sHTML += "				<img src='/images/common/img_noimg.jpg' title='" + item_nm + "' />";
        			}
					
					sHTML += "			</div>";
					sHTML += "		</div>";				
					
					console.log("ITEM_CD", response.list[i].ITEM_CD);
					console.log("ITEM_NM", response.list[i].ITEM_NM);
					console.log("SPEC", response.list[i].SPEC);
					console.log("WEIGHT", response.list[i].WEIGHT);
					console.log("PRICE_A", response.list[i].PRICE_A);
					console.log("PRICE_B", response.list[i].PRICE_B);
					console.log("MAKE_PRICE", response.list[i].MAKE_PRICE);	
					
					var item = new Object();
					item.item_cd = response.list[i].ITEM_CD;
					item.item_nm = response.list[i].ITEM_NM;
					item.spec = response.list[i].SPEC;
					item.weight = response.list[i].WEIGHT;
					item.price_a = response.list[i].PRICE_A;
					item.price_b = response.list[i].PRICE_B;
					item.make_price = response.list[i].MAKE_PRICE;
					
					items.push(item);									
            	}
            	
            	sHTML += "	</div>";
            	
            	var json = JSON.stringify(items);
            	console.log("####", json);
            	
            	$("#form" + recipe_tp + " #btnPackage" + pkg).attr("item_info", json);            	
            	
            	$("#form" + recipe_tp + " #img_package" + pkg).html(sHTML);
            	var slider = $("#form" + recipe_tp + " #rolling" + pkg);
        		slider.owlCarousel({
        			loop: $("#form" + recipe_tp + " #rolling" + pkg + " .item").length > 1 ? true: false,
        			margin:0,
        			nav:false,
        			dots: $("#form" + recipe_tp + " #rolling" + pkg + " .item").length > 1 ? true: false,
        			items:1,
        			smartSpeed:1500,
        			autoplay:true,
        			autoplayTimeout:5000,
        			autoplayHoverPause:false,
        			mouseDrag : $("#form" + recipe_tp + " #rolling" + pkg + " .item").length > 1 ? true: false,
        		});

        		slider.trigger('refresh.owl.carousel');
            	
            	calc();		          	
            }
            else {
            	alert(response.message);
            }
        },
        error:function(request, status, error) {
        	//HideCSS($(".loadingbar"));

            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }
    });
}

function selectStick(value) {
	var nStart = 300;
	if(value == "100") {
		nStart = 300;
	}
	else if(value == "200") {
		nStart = 600;
	}
	else if(value == "300") {
		nStart = 900;
	}
	else if(value == "400") {
		nStart = 1200;
	}
	
	var sHTML = "";
	for(var i = nStart; i <= 1200; i += 300) {
		sHTML += "<option value='" + i + "'>" + NumberFormat(i) + " 포</option>";
	}
	
	$("#form3 #info2").html(sHTML);
}

function setHospital(id, company, doctor, tel, mobile, zipcode, address, address2, price_class) {		
	$("#btnSearchHospital").html("<span>" + company + "</span>");	
	$("#btnSearchHospital").attr("hospital_id", id);
	$("#btnSearchHospital").attr("hospital_name", company);
	$("#btnSearchHospital").attr("hospital_tel", tel);
	$("#btnSearchHospital").attr("hospital_mobile", mobile);
	$("#btnSearchHospital").attr("hospital_zipcode", zipcode);
	$("#btnSearchHospital").attr("hospital_address", address);
	$("#btnSearchHospital").attr("hospital_address2", address2);
	$("#btnSearchHospital").attr("price_class", price_class);
	
	HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_A;
	HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_A;
	
	if(price_class == "B") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_B;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_B;
	}
	else if(price_class == "C") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_C;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_C;
	}
	else if(price_class == "D") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_D;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_D;
	}
	else if(price_class == "E") {
		HANPURE.PRICE_HOTPOT = HANPURE.PRICE_HOTPOT_E;
		HANPURE.PRICE_MAKE = HANPURE.PRICE_MAKE_E;
	}
	
	var sHTML = "<option value=''>선택하세요</option>";
	var doctors = doctor.split("|");
	
	for(var i = 0; i < doctors.length; i++) {
		if(i == 0){
			sHTML += "<option value='" + doctors[i] + "' selected>" + doctors[i] + "</option>";
		}
		else {
			sHTML += "<option value='" + doctors[i] + "'>" + doctors[i] + "</option>";	
		}			
	}
	
	$("#doctor").html(sHTML);
	
	var recipe_tp = GetRadioValue("recipe_tp");
	$("#tab2_c2_iframe").attr("src", "/common/search_myrecipe?RECIPE_TP=" + recipe_tp + "&HOSPITAL_ID=" + id);
	$("#tab2_c3_iframe").attr("src", "/common/search_prerecipe?RECIPE_TP=" + recipe_tp + "&HOSPITAL_ID=" + id);
	
	$("#btnInit").trigger("click");
	
	goPage();
}	
	
function OpenDaumZipEx(type) {
	new daum.Postcode({
		oncomplete: function(data) {
			$("#" + type + "_zipcode").val(data.zonecode);
			$("#" + type + "_address").val(data.roadAddress);
		}
	}).open();
}

function setSender(value) {
	if(value == "1") {
		var name = $("#btnSearchHospital").attr("hospital_name");
		var tel = $("#btnSearchHospital").attr("hospital_tel");
		var mobile = $("#btnSearchHospital").attr("hospital_mobile");
		var zipcode = $("#btnSearchHospital").attr("hospital_zipcode");
		var address = $("#btnSearchHospital").attr("hospital_address");
		var address2 = $("#btnSearchHospital").attr("hospital_address2");
		
		if(!name) name = "";
		if(!tel) tel = "";
		if(!mobile) mobile = "";
		if(!zipcode) zipcode = "";
		if(!address) address = "";
		if(!address2) address2 = "";
		
		
		$("#send_nm").val(name);
		if(tel) {
			var tel_array = tel.split("-");
			if(tel_array.length == 3) {
				$("#send_tel1").val(tel_array[0]);
				$("#send_tel2").val(tel_array[1]);
				$("#send_tel3").val(tel_array[2]);
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
		}
		else {
			$("#send_tel1").val("");
			$("#send_tel2").val("");
			$("#send_tel3").val("");
		}
		if(mobile) {
			var mobile_array = mobile.split("-");
			if(mobile_array.length == 3) {
				$("#send_mobile1").val(mobile_array[0]);
				$("#send_mobile2").val(mobile_array[1]);
				$("#send_mobile3").val(mobile_array[2]);
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
		}
		else {
			$("#send_mobile1").val("");
			$("#send_mobile2").val("");
			$("#send_mobile3").val("");
		}
		
		$("#send_zipcode").val(zipcode);
		$("#send_address").val(address);
		$("#send_address2").val(address2);
	}
	else if(value == "2") {
		//탕전실
		$("#send_nm").val("탕전실");
		
		var tel = HANPURE.TEL;
		if(tel) {
			var tel_array = tel.split("-");
			if(tel_array.length == 3) {
				$("#send_tel1").val(tel_array[0]);
				$("#send_tel2").val(tel_array[1]);
				$("#send_tel3").val(tel_array[2]);
			}
			else {
				$("#send_tel1").val("");
				$("#send_tel2").val("");
				$("#send_tel3").val("");
			}
		}
		else {
			$("#send_tel1").val("");
			$("#send_tel2").val("");
			$("#send_tel3").val("");
		}
		
		var mobile = "${HANPURE.MOBILE}";
		if(mobile) {
			var mobile_array = mobile.split("-");
			if(mobile_array.length == 3) {
				$("#send_mobile1").val(mobile_array[0]);
				$("#send_mobile2").val(mobile_array[1]);
				$("#send_mobile3").val(mobile_array[2]);
			}
			else {
				$("#send_mobile1").val("");
				$("#send_mobile2").val("");
				$("#send_mobile3").val("");
			}
		}
		else {
			$("#send_mobile1").val("");
			$("#send_mobile2").val("");
			$("#send_mobile3").val("");
		}
		
		$("#send_zipcode").val(HANPURE.ZIPCODE);
		$("#send_address").val(HANPURE.ADDRESS);
		$("#send_address2").val(HANPURE.ADDRESS2);
	}
	else if(value == "3") {
		//수동입력
		$("#send_nm").val("");
		$("#send_tel1").val("");
		$("#send_tel2").val("");
		$("#send_tel3").val("");
		$("#send_mobile1").val("");
		$("#send_mobile2").val("");
		$("#send_mobile3").val("");
		$("#send_zipcode").val("");
		$("#send_address").val("");
		$("#send_address2").val("");
	}	
}
	
function setRecver(value) {
	if(value == "1") {
		var name = $("#patient_seq").attr("patient_name");
		var tel = $("#patient_seq").attr("patient_tel");
		var mobile = $("#patient_seq").attr("patient_mobile");
		var zipcode = $("#patient_seq").attr("patient_zipcode");
		var address = $("#patient_seq").attr("patient_address");
		var address2 = $("#patient_seq").attr("patient_address2");
		
		if(!name) name = "";
		if(!tel) tel = "";
		if(!mobile) mobile = "";
		if(!zipcode) zipcode = "";
		if(!address) address = "";
		if(!address2) address2 = "";
		
		$("#recv_nm").val(name);
		
		if(tel) {
			var tel_array = tel.split("-");
			if(tel_array.length == 3) {
				$("#recv_tel1").val(tel_array[0]);
				$("#recv_tel2").val(tel_array[1]);
				$("#recv_tel3").val(tel_array[2]);
			}
			else {
				$("#recv_tel1").val("");
				$("#recv_tel2").val("");
				$("#recv_tel3").val("");
			}
		}
		else {
			$("#recv_tel1").val("");
			$("#recv_tel2").val("");
			$("#recv_tel3").val("");
		}
		
		if(mobile) {
			var mobile_array = mobile.split("-");
			if(mobile_array.length == 3) {
				$("#recv_mobile1").val(mobile_array[0]);
				$("#recv_mobile2").val(mobile_array[1]);
				$("#recv_mobile3").val(mobile_array[2]);
			}
			else {
				$("#recv_mobile1").val("");
				$("#recv_mobile2").val("");
				$("#recv_mobile3").val("");
			}
		}
		else {
			$("#recv_mobile1").val("");
			$("#recv_mobile2").val("");
			$("#recv_mobile3").val("");
		}
		
		$("#recv_zipcode").val(zipcode);
		$("#recv_address").val(address);
		$("#recv_address2").val(address2);
	}
	else if(value == "2") {
		var name = $("#btnSearchHospital").attr("hospital_name");
		var tel = $("#btnSearchHospital").attr("hospital_tel");
		var mobile = $("#btnSearchHospital").attr("hospital_mobile");
		var zipcode = $("#btnSearchHospital").attr("hospital_zipcode");
		var address = $("#btnSearchHospital").attr("hospital_address");
		var address2 = $("#btnSearchHospital").attr("hospital_address2");
		
		if(!name) name = "";
		if(!tel) tel = "";
		if(!mobile) mobile = "";
		if(!zipcode) zipcode = "";
		if(!address) address = "";
		if(!address2) address2 = "";
		
		$("#recv_nm").val(name);
		
		if(tel) {
			var tel_array = tel.split("-");
			if(tel_array.length == 3) {
				$("#recv_tel1").val(tel_array[0]);
				$("#recv_tel2").val(tel_array[1]);
				$("#recv_tel3").val(tel_array[2]);
			}
			else {
				$("#recv_tel1").val("");
				$("#recv_tel2").val("");
				$("#recv_tel3").val("");
			}
		}
		else {
			$("#recv_tel1").val("");
			$("#recv_tel2").val("");
			$("#recv_tel3").val("");
		}
		
		if(mobile) {
			var mobile_array = mobile.split("-");
			if(mobile_array.length == 3) {
				$("#recv_mobile1").val(mobile_array[0]);
				$("#recv_mobile2").val(mobile_array[1]);
				$("#recv_mobile3").val(mobile_array[2]);
			}
			else {
				$("#recv_mobile1").val("");
				$("#recv_mobile2").val("");
				$("#recv_mobile3").val("");
			}
		}
		else {
			$("#recv_mobile1").val("");
			$("#recv_mobile2").val("");
			$("#recv_mobile3").val("");
		}
		
		$("#recv_zipcode").val(zipcode);
		$("#recv_address").val(address);
		$("#recv_address2").val(address2);
	}
	else if(value == "3") {
		$("#recv_nm").val("");
		
		$("#recv_tel1").val("");
		$("#recv_tel2").val("");
		$("#recv_tel3").val("");
		
		$("#recv_mobile1").val("");
		$("#recv_mobile2").val("");
		$("#recv_mobile3").val("");
		
		$("#recv_zipcode").val("");
		$("#recv_address").val("");
		$("#recv_address2").val("");
	}	
}
























