function setHowTo(type, content) {
	console.log("setHowTo", type, content);
	
	if(type == "M") {
		sn$('#howtomake').summernote('pasteHTML', content);		
	}
	else if(type == "E") {
		sn$('#howtoeat').summernote('pasteHTML', content);				
	}			
}

function setHowToFile(type, filename, filepath) {
	console.log("setHowToFile", type, filename, filepath);
	
	if(type == "M") {
		var sHTML = "";			
		sHTML += "<div class='filebox_txt make_file' data-name='" + filename + "' data-path='" + filepath + "' style='display: inline-block;'>";
		sHTML += "	<em class='upload-name2'><a href='/download?filepath=" + filename + "&filename=" + filename + "'>" + filename + "</a></em>";
		sHTML += "	<button type='button' class='btn_file_del' onclick='deleteFile(this)'><span>삭제</span></button>";
		sHTML += "</div>";
		
		$("#howtomake_file").append(sHTML);		
	}
	else if(type == "E") {
		var sHTML = "";			
		sHTML += "<div class='filebox_txt eat_file' data-name='" + filename + "' data-path='" + filepath + "' style='display: inline-block;'>";
		sHTML += "	<em class='upload-name2'><a href='/download?filepath=" + filename + "&filename=" + filename + "'>" + filename + "</a></em>";
		sHTML += "	<button type='button' class='btn_file_del' onclick='deleteFile(this)'><span>삭제</span></button>";
		sHTML += "</div>";
		
		$("#howtoeat_file").append(sHTML);				
	}			
}

function uploadFile1() {
	var filename = $("#upfile1").val();
	if(filename) {
		var formData = new FormData();	    
		formData.append("upfile", document.getElementById('upfile1').files[0]);
		var url = "/uploadFile";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {			            	
	            	$("#filename").val(response.filename);
	            	$("#filepath").val(response.filepath);
	            	
	            	var sHTML = "";
	            	
	            	sHTML += "<div class='filebox_txt make_file' data-name='" + response.filename + "' data-path='" + response.filepath + "' style='display: inline-block;'>";
	            	sHTML += "	<em class='upload-name2'>" + response.filename + "</em>";
	            	sHTML += "	<button type='button' class='btn_file_del' onclick='deleteFile(this)'><span>삭제</span></button>";
	            	sHTML += "</div>";
	            	
	            	$("#howtomake_file").append(sHTML);
	            }
	            else {
	            	alert(response.message);
	            }
	            
	            $("#upfile1").val("");
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
}

function uploadFile2() {
	var filename = $("#upfile2").val();
	if(filename) {
		var formData = new FormData();	    
		formData.append("upfile", document.getElementById('upfile2').files[0]);
		var url = "/uploadFile";
	    $.ajax({
	        type:"POST",
	        url:url,
	        data:formData,
	        cache: false,
	        processData: false,  // file전송시 필수
	        contentType: false,  // file전송시 필수
	        beforeSend: function() {
	        	ShowCSS($(".loadingbar"));
	        },
	        success:function(response) {
	        	HideCSS($(".loadingbar"));
	        	
	            if(response.result == 200) {			            	
	            	$("#filename").val(response.filename);
	            	$("#filepath").val(response.filepath);
	            	
	            	var sHTML = "";
	            	
	            	sHTML += "<div class='filebox_txt eat_file' data-name='" + response.filename + "' data-path='" + response.filepath + "' style='display: inline-block;'>";
	            	sHTML += "	<em class='upload-name2'>" + response.filename + "</em>";
	            	sHTML += "	<button type='button' class='btn_file_del' onclick='deleteFile(this)'><span>삭제</span></button>";
	            	sHTML += "</div>";
	            	
	            	$("#howtoeat_file").append(sHTML);
	            }
	            else {
	            	alert(response.message);
	            }
	            
	            $("#upfile2").val("");
	        },
	        error:function(request, status, error) {
	        	HideCSS($(".loadingbar"));
	        	
	            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	        }
	    });
	}
}

function deleteFile(obj) {		
	$(obj).parent().remove();
}