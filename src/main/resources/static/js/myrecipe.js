function setRecipeInfo(recipe_nm, info1, info2, info3, info4, info5, info6, info7, info8, info9, info9_nm, info9_price, info10, marking_tp, marking_text, package1, package1_nm, package1_filepath, package2, package2_nm, package2_filepath, package3, package3_nm, package3_filepath, vaccum) {
	$("#recipe_nm").val(recipe_nm);
	
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		$("#form1 #info1").val(info1);
		$("#form1 #info2").val(info2);
		$("#form1 #info3").val(info3);
		$("#form1 #info4").val(info4);
		//$("#form1 #info5").val(info5);
		//$("#form1 #info6").val(info6);
		$("#form1 #info7").val(info7);		
		$("#form1 #info8").val(info8);
		
		if(info9 != "") {
			$("#form1 #info9").html(info9_nm + "/" + info10 + "ml");
			$("#form1 #info9").attr("code", info9);
			$("#form1 #info9").attr("name", info9_nm);
			$("#form1 #info9").attr("price", info9_price);
			$("#form1 #info9").attr("value", info10);	
		}						
		else {
			$("#form1 #info9").html("선택없음");
			$("#form1 #info9").attr("code", "");
			$("#form1 #info9").attr("value", "");
		}
		
		if(marking_tp) {
			$("#form1 #btnMarking").attr("marking_tp", marking_tp);
			$("#form1 #btnMarking").attr("marking_text", marking_text);
			$("#form1 #btnMarking").html(marking_text);	
			
			$("#form1 #btnMarking").removeClass("blue");
			$("#form1 #btnMarking").addClass("gray");
			
			$("#form1 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
		}
		else {
			$("#form1 #btnMarking").attr("marking_tp", "");
			$("#form1 #btnMarking").attr("marking_text", "");
			$("#form1 #btnMarking").html("선택하세요");	
			
			$("#form1 #btnPackage1").removeClass("gray");
			$("#form1 #btnPackage1").addClass("blue");
			
			$("#form1 #img_marking").html("");
		}
		
		if(package1) {
			$("#form1 #btnPackage1").attr("item_cd", package1);
			$("#form1 #btnPackage1").attr("item_nm", package1_nm);
			$("#form1 #btnPackage1").attr("vaccum", vaccum);
			$("#form1 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			$("#form1 #btnPackage1").removeClass("blue");
			$("#form1 #btnPackage1").addClass("gray");
			
			$("#form1 #img_package1").html("<img src='/download?filepath=" + package1_filepath + "' alt='사진'>");
		}
		else {
			$("#form1 #btnPackage1").attr("item_cd", "");
			$("#form1 #btnPackage1").attr("item_nm", "");
			$("#form1 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form1 #btnPackage1").removeClass("gray");
			$("#form1 #btnPackage1").addClass("blue");
			
			$("#form1 #img_package1").html("");
		}
		
		if(package2) {
			$("#form1 #btnPackage2").attr("item_cd", package2);
			$("#form1 #btnPackage2").attr("item_nm", package2_nm);
			$("#form1 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			$("#form1 #btnPackage2").removeClass("blue");
			$("#form1 #btnPackage2").addClass("gray");
			
			$("#form1 #img_package2").html("<img src='/download?filepath=" + package2_filepath + "' alt='사진'>");	
		}
		else {
			$("#form1 #btnPackage2").attr("item_cd", "");
			$("#form1 #btnPackage2").attr("item_nm", "");
			$("#form1 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form1 #btnPackage2").removeClass("gray");
			$("#form1 #btnPackage2").addClass("blue");
			
			$("#form1 #img_package2").html("");
		}
		
		if(package3) {
			$("#form1 #btnPackage3").attr("item_cd", package3);
			$("#form1 #btnPackage3").attr("item_nm", package3_nm);
			$("#form1 #btnPackage3").html("<span>" + package3_nm + "</span>");
			
			$("#form1 #btnPackage3").removeClass("blue");
			$("#form1 #btnPackage3").addClass("gray");	
			
			$("#form1 #img_package3").html("<img src='/download?filepath=" + package3_filepath + "' alt='사진'>");
		}
		else {
			$("#form1 #btnPackage3").attr("item_cd", "");
			$("#form1 #btnPackage3").attr("item_nm", "");
			$("#form1 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form1 #btnPackage3").removeClass("gray");
			$("#form1 #btnPackage3").addClass("blue");
			
			$("#form1 #img_package3").html("");
		}		
	}
	else if(recipe_tp == "002") {
		$("#form2 #info1").val(info1);
		$("#form2 #info2").val(info2);
		$("#form2 #info3").val(info3);
		$("#form2 #info4").val(info4);
		
		if(marking_tp) {
			$("#form2 #btnMarking").attr("marking_tp", marking_tp);
			$("#form2 #btnMarking").attr("marking_text", marking_text);
			$("#form2 #btnMarking").html(marking_text);	
			
			$("#form2 #btnMarking").removeClass("blue");
			$("#form2 #btnMarking").addClass("gray");
			
			$("#form2 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
		}
		else {
			$("#form2 #btnMarking").attr("marking_tp", "");
			$("#form2 #btnMarking").attr("marking_text", "");
			$("#form2 #btnMarking").html("선택하세요");	
			
			$("#form2 #btnPackage1").removeClass("gray");
			$("#form2 #btnPackage1").addClass("blue");
			
			$("#form2 #img_marking").html("");
		}
		
		if(package1) {
			$("#form2 #btnPackage1").attr("item_cd", package1);
			$("#form2 #btnPackage1").attr("item_nm", package1_nm);
			$("#form2 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			$("#form2 #btnPackage1").removeClass("blue");
			$("#form2 #btnPackage1").addClass("gray");
			
			$("#form2 #img_package1").html("<img src='/download?filepath=" + package1_filepath + "' alt='사진'>");
		}
		else {
			$("#form2 #btnPackage1").attr("item_cd", "");
			$("#form2 #btnPackage1").attr("item_nm", "");
			$("#form2 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form2 #btnPackage1").removeClass("gray");
			$("#form2 #btnPackage1").addClass("blue");
			
			$("#form2 #img_package1").html("");
		}
		
		if(package2) {
			$("#form2 #btnPackage2").attr("item_cd", package2);
			$("#form2 #btnPackage2").attr("item_nm", package2_nm);
			$("#form2 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			$("#form2 #btnPackage2").removeClass("blue");
			$("#form2 #btnPackage2").addClass("gray");
			
			$("#form2 #img_package2").html("<img src='/download?filepath=" + package2_filepath + "' alt='사진'>");	
		}
		else {
			$("#form2 #btnPackage2").attr("item_cd", "");
			$("#form2 #btnPackage2").attr("item_nm", "");
			$("#form2 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form2 #btnPackage2").removeClass("gray");
			$("#form2 #btnPackage2").addClass("blue");
			
			$("#form2 #img_package2").html("");
		}
		
		if(package3) {
			$("#form2 #btnPackage3").attr("item_cd", package3);
			$("#form2 #btnPackage3").attr("item_nm", package3_nm);
			$("#form2 #btnPackage3").html("<span>" + package3_nm + "</span>");
			
			$("#form2 #btnPackage3").removeClass("blue");
			$("#form2 #btnPackage3").addClass("gray");	
			
			$("#form2 #img_package3").html("<img src='/download?filepath=" + package3_filepath + "' alt='사진'>");
		}
		else {
			$("#form2 #btnPackage3").attr("item_cd", "");
			$("#form2 #btnPackage3").attr("item_nm", "");
			$("#form2 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form2 #btnPackage3").removeClass("gray");
			$("#form2 #btnPackage3").addClass("blue");
			
			$("#form2 #img_package3").html("");
		}	
	}
	else if(recipe_tp == "003") {
		$("#form3 #info1").val(info1);
		$("#form3 #info2").html(NumberFormat(info2));
		
		if(marking_tp) {
			$("#form3 #btnMarking").attr("marking_tp", marking_tp);
			$("#form3 #btnMarking").attr("marking_text", marking_text);
			$("#form3 #btnMarking").html(marking_text);	
			
			$("#form3 #btnMarking").removeClass("blue");
			$("#form3 #btnMarking").addClass("gray");
			
			$("#form3 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
		}
		else {
			$("#form3 #btnMarking").attr("marking_tp", "");
			$("#form3 #btnMarking").attr("marking_text", "");
			$("#form3 #btnMarking").html("선택하세요");	
			
			$("#form3 #btnPackage1").removeClass("gray");
			$("#form3 #btnPackage1").addClass("blue");
			
			$("#form3 #img_marking").html("");
		}
		
		if(package1) {
			$("#form3 #btnPackage1").attr("item_cd", package1);
			$("#form3 #btnPackage1").attr("item_nm", package1_nm);
			$("#form3 #btnPackage1").html("<span>" + package1_nm + "</span>");	
			
			$("#form3 #btnPackage1").removeClass("blue");
			$("#form3 #btnPackage1").addClass("gray");
			
			$("#form3 #img_package1").html("<img src='/download?filepath=" + package1_filepath + "' alt='사진'>");
		}
		else {
			$("#form3 #btnPackage1").attr("item_cd", "");
			$("#form3 #btnPackage1").attr("item_nm", "");
			$("#form3 #btnPackage1").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form3 #btnPackage1").removeClass("gray");
			$("#form3 #btnPackage1").addClass("blue");
			
			$("#form3 #img_package1").html("");
		}
		
		if(package2) {
			$("#form3 #btnPackage2").attr("item_cd", package2);
			$("#form3 #btnPackage2").attr("item_nm", package2_nm);
			$("#form3 #btnPackage2").html("<span>" + package2_nm + "</span>");	
			
			$("#form3 #btnPackage2").removeClass("blue");
			$("#form3 #btnPackage2").addClass("gray");
			
			$("#form3 #img_package2").html("<img src='/download?filepath=" + package2_filepath + "' alt='사진'>");	
		}
		else {
			$("#form3 #btnPackage2").attr("item_cd", "");
			$("#form3 #btnPackage2").attr("item_nm", "");
			$("#form3 #btnPackage2").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form3 #btnPackage2").removeClass("gray");
			$("#form3 #btnPackage2").addClass("blue");
			
			$("#form3 #img_package2").html("");
		}
		
		if(package3) {
			$("#form3 #btnPackage3").attr("item_cd", package3);
			$("#form3 #btnPackage3").attr("item_nm", package3_nm);
			$("#form3 #btnPackage3").html("<span>" + package3_nm + "</span>");
			
			$("#form3 #btnPackage3").removeClass("blue");
			$("#form3 #btnPackage3").addClass("gray");	
			
			$("#form3 #img_package3").html("<img src='/download?filepath=" + package3_filepath + "' alt='사진'>");
		}
		else {
			$("#form3 #btnPackage3").attr("item_cd", "");
			$("#form3 #btnPackage3").attr("item_nm", "");
			$("#form3 #btnPackage3").html("<span>" + "선택하세요" + "</span>");	
			
			$("#form3 #btnPackage3").removeClass("gray");
			$("#form3 #btnPackage3").addClass("blue");
			
			$("#form3 #img_package3").html("");
		}	
	}
}

function setDrug(herbs_cd, drug_cd, drug_nm, stock_qntt, chup1, turn, origin, price, absorption_rate) {
	var length = $("input:checkbox[name='drug_cd']").length;
	console.log("length : " + length);
	console.log("drug_cd : " + drug_cd);
	
	for(var i = 0; i < length; i++) {			
		if(drug_cd == $("input:checkbox[name='drug_cd']").eq(i).val()) {
			//alert(drug_nm + "(" + drug_cd + ")는 이미 추가된 약재입니다.");
			//return;
			toastMsg('이미 선택한 약재입니다.','ty3');
			return;
		}
	}
	
	var isPoison = false;
	if(POISON.indexOf(herbs_cd) > -1) {
		isPoison = true;
	}
	
	var sHTML = "";
	
	sHTML += "<tr>";
	sHTML += "	<td><label class='inp_checkbox'><input type='checkbox' class='chk' name='drug_cd' value='" + drug_cd + "' data-herbs='" + herbs_cd + "' data-name='" + drug_nm + "' data-rate='" + absorption_rate + "' data-price='" + price + "' data-stock='" + stock_qntt + "' data-origin='" + origin + "' /><span class='d'></span></label></td>";
	sHTML += "	<td class='no'></td>";
	sHTML += "	<td>" + drug_nm + "<span class='poison'></span>";
	
	if(isPoison) {
		sHTML += "		<button type='button' class='btn_cir1 ty2' onclick=\"toastMsg('독성 약재가 있습니다','ty2')\"><span>독</span></button>";
	}
	//sHTML += "		<button type='button' class='btn_cir1 ty1' onclick=\"toastMsg('상극 약재가 있습니다','ty1')\"><span>상</span></button>";
	//sHTML += "		<button type='button' class='btn_cir1 ty2' onclick=\"toastMsg('독성 약재가 있습니다','ty2')\"><span>독</span></button>";						
	
	sHTML += "	</td>";
	
	sHTML += "	<td>";
	
	if(turn == "2") {
		sHTML += "		<select name='turn' class='select1 w100p'>";
		sHTML += "			<option value='1'>선전</option>";
		sHTML += "			<option value='2' selected>일반</option>";
		sHTML += "			<option value='3'>후하</option>";
		sHTML += "		</select>";
	}
	else if(turn == "3") {
		sHTML += "		<select name='turn' class='select1 w100p'>";
		sHTML += "			<option value='1'>선전</option>";
		sHTML += "			<option value='2'>일반</option>";
		sHTML += "			<option value='3' selected>후하</option>";
		sHTML += "		</select>";
	}
	else {
		sHTML += "		<select name='turn' class='select1 w100p'>";
		sHTML += "			<option value='1'>선전</option>";
		sHTML += "			<option value='2'>일반</option>";
		sHTML += "			<option value='3'>후하</option>";
		sHTML += "		</select>";	
	}
	
	sHTML += "	</td>";
	sHTML += "	<td>" + origin + "</td>";
	sHTML += "	<td class='ta-r price'>" + NumberFormat(price) + "원</td>";
	sHTML += "	<td class='ta-r'>" + NumberFormat(stock_qntt) + "g</td>";
	sHTML += "	<td class='ta-r'><div class='fx'><input type='text' name='chup1' class='inp_txt chup1' value='" + chup1 + "'>g</div></td>";
	sHTML += "	<td class='ta-r total_qntt'></td>";
	sHTML += "	<td class='ta-r total_price'></td>";
	sHTML += "</tr>";
	
	$("#tbody").append(sHTML);
	
	$('.chup1').keyup(function(e) {
		console.log("chup1");
		
		InpuOnlyNumber(this);
		
		calc();
	});	
	
	$('.chup1').click(function(e) {
		$(this).select();
	});	
	
	checkConflict();
	
	calc();
}

function checkConflict() {
	var length = $("input:checkbox[name='drug_cd']").length;

	var herbs_cd_array = "";
	for(var i = 0; i < length; i++) {			
		if(herbs_cd_array != "") {
			herbs_cd_array += ",";
		}
		
		herbs_cd_array += $("input:checkbox[name='drug_cd']").eq(i).data("herbs");
	}
	
	console.log("herbs_cd_array : " + herbs_cd_array);
	
	
	for(var i = 0; i < length; i++) {			
		var herbs = $("input:checkbox[name='drug_cd']").eq(i).data("herbs");
		
		var cnt = 0;
		
		for(var k = 0; k < CONFLICT.length; k++) {
			if(CONFLICT[k].HERBS_CD.indexOf(herbs) > -1) {	//현재 본초코드가 상극 알람에 존재
				var temp = CONFLICT[k].HERBS_CD.split(",");
				
				for(var z = 0; z < temp.length; z++) {
					if(temp[z] != herbs) {
						if(herbs_cd_array.indexOf(temp[z]) > -1) {
							cnt++;
							
							if(cnt > 0) {
								break;
							}
						}	
					}						
				}
			}
			
			if(cnt > 0) {
				break;
			}
		}
		
		console.log("cnt : " + cnt);
		
		if(cnt > 0) {
			$(".poison").eq(i).html("<button type='button' class='btn_cir1 ty1' onclick=\"toastMsg('상극 약재가 있습니다','ty1')\"><span>상</span></button>");
		}
		else {
			$(".poison").eq(i).html("");
		}			
	}
}

function calc() {
	console.log("calc()");
	
	var recipe_tp = GetRadioValue("recipe_tp");
	var length = $("input:checkbox[name='drug_cd']").length;
	
	//탕전
	if(recipe_tp == "001") {
		var chup = parseInt($("#form1 #info1").val());	//첩수
		var pack = parseInt($("#form1 #info2").val());	//팩수
		var pack_size = parseInt($("#form1 #info3").val());	//팩 용량
		
		console.log("chup : " + chup);
		console.log("pack : " + pack);
		
		var sum_chup1 = 0;
		var sum_total_qntt = 0;
		var sum_total_price = 0;
		var sum_water = pack_size * (pack + 2);					//추출량 = 팩 용량 * (팩 수 + 2) =
		
		for(var i = 0; i < length; i++) {
			$(".no").eq(i).html(i + 1);
			
			var chup1 = parseInt($("input[name='chup1']").eq(i).val());
			var price = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("price"));
			var rate = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("rate"));
			
			console.log("chup1 : " + chup1);
			console.log("price : " + price);
			console.log("rate : " + rate);
			
			var total_qntt = chup1 * chup;
			console.log("total_qntt : " + total_qntt);
			$(".total_qntt").eq(i).html(NumberFormat(total_qntt) + "g");
			
			//물량
			var water = 0;
			water += total_qntt;							//약재흡수량 = (총 약재량) 합계 = (첩당 약재량 * 첩의 개수) 합계 =				
			water += parseInt(total_qntt * 100 / rate);		//증발량 = (부위별 흡수량) 합계 =
			
			sum_water += water;
			//물량
			
			
			var total_price = price * total_qntt;
			console.log("total_price : " + total_price);
			$(".total_price").eq(i).html(NumberFormat(total_price) + "원");
			
			sum_chup1 += chup1;
			sum_total_qntt += total_qntt;
			sum_total_price += total_price;
		}	
		
		$("#sum_chup1").html(NumberFormat(sum_chup1.toFixed(0)));
		$("#sum_total_qntt").html(NumberFormat(sum_total_qntt.toFixed(0)));
		$("#sum_total_price").html(NumberFormat(sum_total_price.toFixed(0)));
		
		$("#form1 #info6").html(NumberFormat(sum_water));			
	}
	//제환
	else if(recipe_tp == "002") {
		var chup = parseInt($("#form2 #info1").val());	//첩수
		console.log("chup : " + chup);	
		
		var sum_chup1 = 0;
		var sum_total_qntt = 0;
		var sum_total_price = 0;
		
		for(var i = 0; i < length; i++) {
			$(".no").eq(i).html(i + 1);
			
			var chup1 = parseInt($("input[name='chup1']").eq(i).val());
			var price = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("price"));
			var rate = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("rate"));
			
			console.log("chup1 : " + chup1);
			console.log("price : " + price);
			
			var total_qntt = chup1 * chup;
			console.log("total_qntt : " + total_qntt);
			$(".total_qntt").eq(i).html(NumberFormat(total_qntt) + "g");
			
			var total_price = price * total_qntt;
			console.log("total_price : " + total_price);
			$(".total_price").eq(i).html(NumberFormat(total_price) + "원");
			
			sum_chup1 += chup1;
			sum_total_qntt += total_qntt;
			sum_total_price += total_price;
		}	
		
		$("#sum_chup1").html(NumberFormat(sum_chup1.toFixed(0)));
		$("#sum_total_qntt").html(NumberFormat(sum_total_qntt.toFixed(0)));
		$("#sum_total_price").html(NumberFormat(sum_total_price.toFixed(0)));
		
		$("#form2 #info6").html(NumberFormat((sum_total_qntt * 0.1).toFixed(0)));
		$("#form2 #info7").html(NumberFormat((sum_total_qntt * 0.1).toFixed(0)));
		$("#form2 #info8").html(NumberFormat((sum_total_qntt * 0.8).toFixed(0)));			
	}
	//연조엑스
	else if(recipe_tp == "003") {
		var chup = $("#form3 #info1 option:selected").text();
		chup = chup.replace(/[^0-9]/g,'');	
		console.log("chup : " + chup);	
		
		var sum_chup1 = 0;
		var sum_total_qntt = 0;
		var sum_total_price = 0;
		
		for(var i = 0; i < length; i++) {
			$(".no").eq(i).html(i + 1);
			
			var chup1 = parseInt($("input[name='chup1']").eq(i).val());
			var price = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("price"));
			var rate = parseInt($("input:checkbox[name='drug_cd']").eq(i).data("rate"));
			
			console.log("chup1 : " + chup1);
			console.log("price : " + price);
			
			var total_qntt = chup1 * chup;
			console.log("total_qntt : " + total_qntt);
			$(".total_qntt").eq(i).html(NumberFormat(total_qntt) + "g");
			
			var total_price = price * total_qntt;
			console.log("total_price : " + total_price);
			$(".total_price").eq(i).html(NumberFormat(total_price) + "원");
			
			sum_chup1 += chup1;
			sum_total_qntt += total_qntt;
			sum_total_price += total_price;
		}	
		
		$("#sum_chup1").html(NumberFormat(sum_chup1.toFixed(0)));
		$("#sum_total_qntt").html(NumberFormat(sum_total_qntt.toFixed(0)));
		$("#sum_total_price").html(NumberFormat(sum_total_price.toFixed(0)));
	}
}

function allCheck(obj) {
	if($(obj).prop("checked")) {
		$(".chk").prop("checked", true);
	}
	else {
		$(".chk").prop("checked", false);
	}
}

function deleteDrug() {
	console.log("deleteDrug()");
	
	var length = GetCheckedLength("drug_cd");
	if(length == 0) {
		alert("하나 이상 선택해 주세요.");
		return;
	}
	
	for(var i = 0; i < length; i++) {
		$("input:checkbox[name='drug_cd']:checked").eq(0).closest("tr").remove();
	}	
	
	checkConflict();
	
	calc();
}

function deleteDrugAll() {
	console.log("deleteDrugAll()");
	
	var length = $("input:checkbox[name='drug_cd']").length;		
	for(var i = 0; i < length; i++) {
		$("input:checkbox[name='drug_cd']").eq(0).closest("tr").remove();
	}	
	
	checkConflict();
	
	calc();
}

function setRecipeTp(value) {
	console.log("setRecipeTp()", value);
	
	$("#form1").hide();
	$("#form2").hide();
	$("#form3").hide();
	
	if(value == "001") {
		$("#form1").show();
		calc();			
	}
	else if(value == "002") {
		$("#form2").show();
		calc();
	}
	else if(value == "003") {
		$("#form3").show();
	}
	
	$("#tab2_c2_iframe").attr("src", "/common/search_myrecipe?RECIPE_TP=" + value);
	$("#tab2_c3_iframe").attr("src", "/common/search_prerecipe?RECIPE_TP=" + value);
}

function setDrink(code, name, value, price) {
	console.log("setDrink code : " + code);
	console.log("setDrink name : " + name);
	console.log("setDrink value : " + value);
	console.log("setDrink price : " + price);
	
	if(code) {		
		$("#form1 #info9").attr("code", code);
		$("#form1 #info9").attr("name", name);
		$("#form1 #info9").attr("value", value);
		$("#form1 #info9").attr("price", price);		
	
		$("#form1 #info9").html(name + "/" + NumberFormat(value) + "mL");
	}
	else {		
		$("#form1 #info9").attr("code", "");
		$("#form1 #info9").attr("name", "");
		$("#form1 #info9").attr("value", "");
		$("#form1 #info9").attr("price", "");
	
		$("#form1 #info9").html(name);	
	}
}

function showDrink() {
	var url = "/common/search_drink";
	url += "?code=" +  $.trim($("#form1 #info9").attr("code"));
	url += "&value=" +  $.trim($("#form1 #info9").attr("value"));
	
	showPopup(400, 300, url);
}

function showMarking() {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		var marking_tp = $("#form1 #btnMarking").attr("marking_tp");
		var marking_text = $("#form1 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
	else if(recipe_tp == "002") {
		var marking_tp = $("#form2 #btnMarking").attr("marking_tp");
		var marking_text = $("#form2 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
	else if(recipe_tp == "003") {
		var marking_tp = $("#form3 #btnMarking").attr("marking_tp");
		var marking_text = $("#form3 #btnMarking").attr("marking_text");
		
		if(!marking_tp) marking_tp = "";
		if(!marking_text) marking_text = "";
		var url = "/common/pop_marking?marking_tp=" + marking_tp + "&marking_text=" + marking_text;
		console.log("showPouch() : " + url);
		showPopup(800, 400, url);
	}
}

function setMarking(marking_tp, marking_text) {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		$("#form1 #btnMarking").removeClass("blue");
		$("#form1 #btnMarking").addClass("gray");		
		
		$("#form1 #btnMarking").attr("marking_tp", marking_tp);
		$("#form1 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form1 #btnMarking").html("<span>" + marking_text + "</span>");								
		$("#form1 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
	else if(recipe_tp == "002") {
		$("#form2 #btnMarking").removeClass("blue");
		$("#form2 #btnMarking").addClass("gray");		
		
		$("#form2 #btnMarking").attr("marking_tp", marking_tp);
		$("#form2 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form2 #btnMarking").html("<span>" + marking_text + "</span>");						
		$("#form2 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
	else if(recipe_tp == "003") {
		$("#form3 #btnMarking").removeClass("blue");
		$("#form3 #btnMarking").addClass("gray");		
		
		$("#form3 #btnMarking").attr("marking_tp", marking_tp);
		$("#form3 #btnMarking").attr("marking_text", marking_text);								
		
		$("#form3 #btnMarking").html("<span>" + marking_text + "</span>");
		$("#form3 #img_marking").html("<img src='/images/tmp_img.jpg' alt='사진'><p>" + marking_text + "</p>");
	}
}

function showPackage(pkg, division) {
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		var item_cd = $("#form1 #btnPackage" + pkg).attr("item_cd");
		var vaccum = $("#form1 #btnPackage" + pkg).attr("vaccum");
		if(!vaccum) vaccum = "";
		
		if(!item_cd) item_cd = "";
		var url = "/common/pop_package?pkg=" + pkg + "&division=" + division + "&item_cd=" + item_cd + "&vaccum=" + vaccum;
		console.log("showPackage001() : " + url);
		showPopup(800, 600, url);
	}
	else if(recipe_tp == "002") {
		if(pkg == "3") {
			var item_cd = $("#form2 #btnPackage2").attr("item_cd");
			var item_nm = $("#form2 #btnPackage2").attr("item_nm");
			if(!item_cd) {
				alert("소박스를 선택해 주세요.");
				return;
			}
			
			if(item_nm.indexOf("틴케이스") > -1) {
				alert(item_nm + "는 상자포장을 할 수 없습니다.");
				return;
			}
			
			if($("#form2 #info3").val() != "004") {
				alert("탄자대만 상자보장이 가능합니다.");
				return;
			} 
		}
		else if(pkg == "2") {
			var item_cd = $("#form2 #btnPackage1").attr("item_cd");
			var item_nm = $("#form2 #btnPackage1").attr("item_nm");
			if(!item_cd) {
				alert("소포장을 선택해 주세요.");
				return;
			}
			
			if($("#form2 #info3").val() == "004") {
				division = "007";
			} 
			
			if(item_nm.indexOf("벌크") > -1) {
				alert(item_nm + "은 소박스와 상자포장을 선택 할 수 없습니다.");
				return;
			}
			else if(item_nm.indexOf("지퍼백") > -1) {
				alert(item_nm + "은 소박스를 선택 할 수 없습니다.");
				return;
			}
			else if(item_nm.indexOf("자일리톨") > -1) {
				alert(item_nm + "은 소박스를 선택 할 수 없습니다.");
				return;
			}
			else if(item_nm.indexOf("사탕") > -1) {
				division = "008";
			}
		}
		else {
			if($("#form2 #info3").val() == "004") {
				division = "006";
			} 						
		}			
		
		var item_cd = $("#form2 #btnPackage" + pkg).attr("item_cd");
		if(!item_cd) item_cd = "";
		var url = "/common/pop_package?pkg=" + pkg + "&division=" + division + "&item_cd=" + item_cd;
		console.log("showPackage002() : " + url);
		showPopup(800, 600, url);
	}
	else if(recipe_tp == "003") {
		if($("#form3 #info3").val() == "004") {
			division = "007";
		} 	
		
		var item_cd = $("#form3 #btnPackage" + pkg).attr("item_cd");
		if(!item_cd) item_cd = "";
		var url = "/common/pop_package?pkg=" + pkg + "&division=" + division + "&item_cd=" + item_cd;
		console.log("showPackage003() : " + url);
		showPopup(800, 600, url);
	}
}

function setPackage(pkg, item_cd, item_nm, qntt, price, make_price, filepath, vaccum) {	
	var recipe_tp = GetRadioValue("recipe_tp");
	if(recipe_tp == "001") {
		$("#form1 #btnPackage" + pkg).attr("item_cd", item_cd);
		$("#form1 #btnPackage" + pkg).attr("item_nm", item_nm);
		$("#form1 #btnPackage" + pkg).attr("qntt", qntt);
		$("#form1 #btnPackage" + pkg).attr("price", price);
		$("#form1 #btnPackage" + pkg).attr("make_price", make_price);
		$("#form1 #btnPackage" + pkg).attr("vaccum", vaccum);
		
		if(item_nm) {
			$("#form1 #btnPackage" + pkg).removeClass("blue");
			$("#form1 #btnPackage" + pkg).addClass("gray");
			
			$("#form1 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
		}
		else {
			$("#form1 #btnPackage" + pkg).removeClass("gray");
			$("#form1 #btnPackage" + pkg).addClass("blue");
			
			$("#form1 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
		}
		
		
		if(filepath) {
			$("#form1 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
		}
		else {
			$("#form1 #img_package" + pkg).html("");
		}								
		
		if(pkg == "1") {
			$("#form1 #btnPackage2").html("<span>선택하세요</span>");	
			$("#form1 #btnPackage3").html("<span>선택하세요</span>");	
			
			$("#form1 #btnPackage2").attr("item_cd", "");
			$("#form1 #btnPackage2").attr("item_nm", "");					

			$("#form1 #btnPackage3").attr("item_cd", "");
			$("#form1 #btnPackage3").attr("item_nm", "");
			
			$("#form1 #btnPackage2").removeClass("gray");
			$("#form1 #btnPackage3").removeClass("gray");

			$("#form1 #btnPackage2").addClass("blue");
			$("#form1 #btnPackage3").addClass("blue");

			$("#form1 #img_package2").html("");
			$("#form1 #img_package3").html("");			
		}
		else if(pkg == "2") {	
			$("#form1 #btnPackage3").html("<span>선택하세요</span>");
			
			$("#form1 #btnPackage3").attr("item_cd", "");
			$("#form1 #btnPackage3").attr("item_nm", "");
			
			$("#form1 #btnPackage3").removeClass("gray");

			$("#form1 #btnPackage3").addClass("blue");

			$("#form1 #img_package3").html("");
		}
	}
	else if(recipe_tp == "002") {										
		$("#form2 #btnPackage" + pkg).attr("item_cd", item_cd);
		$("#form2 #btnPackage" + pkg).attr("item_nm", item_nm);
		$("#form2 #btnPackage" + pkg).attr("qntt", qntt);
		$("#form2 #btnPackage" + pkg).attr("price", price);
		$("#form2 #btnPackage" + pkg).attr("make_price", make_price);
		
		if(item_nm) {
			$("#form2 #btnPackage" + pkg).removeClass("blue");
			$("#form2 #btnPackage" + pkg).addClass("gray");
			
			$("#form2 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
		}
		else {
			$("#form2 #btnPackage" + pkg).removeClass("gray");
			$("#form2 #btnPackage" + pkg).addClass("blue");
			
			$("#form2 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
		}
		
		if(filepath) {
			$("#form2 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
		}
		else {
			$("#form2 #img_package" + pkg).html("");
		}	
		
		if(pkg == "1") {
			$("#form2 #btnPackage2").html("<span>선택하세요</span>");	
			$("#form2 #btnPackage3").html("<span>선택하세요</span>");	
			
			$("#form2 #btnPackage2").attr("item_cd", "");
			$("#form2 #btnPackage2").attr("item_nm", "");					

			$("#form2 #btnPackage3").attr("item_cd", "");
			$("#form2 #btnPackage3").attr("item_nm", "");
			
			$("#form2 #btnPackage2").removeClass("gray");
			$("#form2 #btnPackage3").removeClass("gray");

			$("#form2 #btnPackage2").addClass("blue");
			$("#form2 #btnPackage3").addClass("blue");

			$("#form2 #img_package2").html("");
			$("#form2 #img_package3").html("");
		}
		else if(pkg == "2") {	
			$("#form2 #btnPackage3").html("<span>선택하세요</span>");
			
			$("#form2 #btnPackage3").attr("item_cd", "");
			$("#form2 #btnPackage3").attr("item_nm", "");
			
			$("#form2 #btnPackage3").removeClass("gray");

			$("#form2 #btnPackage3").addClass("blue");

			$("#form2 #img_package3").html("");
		}
	}
	else if(recipe_tp == "003") {							
		$("#form3 #btnPackage" + pkg).attr("item_cd", item_cd);
		$("#form3 #btnPackage" + pkg).attr("item_nm", item_nm);
		$("#form3 #btnPackage" + pkg).attr("qntt", qntt);
		$("#form3 #btnPackage" + pkg).attr("price", price);
		$("#form3 #btnPackage" + pkg).attr("make_price", make_price);
		
		if(item_nm) {
			$("#form3 #btnPackage" + pkg).removeClass("blue");
			$("#form3 #btnPackage" + pkg).addClass("gray");
			
			$("#form3 #btnPackage" + pkg).html("<span>" + item_nm + "</span>")	
		}
		else {
			$("#form3 #btnPackage" + pkg).removeClass("gray");
			$("#form3 #btnPackage" + pkg).addClass("blue");
			
			$("#form3 #btnPackage" + pkg).html("<span>" + "선택하세요" + "</span>")
		}
		
		if(filepath) {
			$("#form3 #img_package" + pkg).html("<img src='/download?filepath=" + filepath + "' alt='사진'>");	
		}
		else {
			$("#form3 #img_package" + pkg).html("");
		}	
		
		if(pkg == "1") {
			$("#form3 #btnPackage2").html("<span>선택하세요</span>");	
			$("#form3 #btnPackage3").html("<span>선택하세요</span>");	
			
			$("#form3 #btnPackage2").attr("item_cd", "");
			$("#form3 #btnPackage2").attr("item_nm", "");					

			$("#form3 #btnPackage3").attr("item_cd", "");
			$("#form3 #btnPackage3").attr("item_nm", "");
			
			$("#form3 #btnPackage2").removeClass("gray");
			$("#form3 #btnPackage3").removeClass("gray");

			$("#form3 #btnPackage2").addClass("blue");
			$("#form3 #btnPackage3").addClass("blue");

			$("#form3 #img_package2").html("");
			$("#form3 #img_package3").html("");
			
			$("#form1 #btnPackage" + pkg).attr("vaccum", vaccum);
		}
		else if(pkg == "2") {	
			$("#form3 #btnPackage3").html("<span>선택하세요</span>");
			
			$("#form3 #btnPackage3").attr("item_cd", "");
			$("#form3 #btnPackage3").attr("item_nm", "");
			
			$("#form3 #btnPackage3").removeClass("gray");

			$("#form3 #btnPackage3").addClass("blue");

			$("#form3 #img_package3").html("");
		}
	}	
	
	calc();											
}

function showBox002(value) {
	console.log("showBox002 : " + value);
	
	$("#form2 #btnPackage1").html("<span>선택하세요</span>");	
	$("#form2 #btnPackage2").html("<span>선택하세요</span>");	
	$("#form2 #btnPackage3").html("<span>선택하세요</span>");	
	
	$("#form2 #btnPackage1").attr("item_cd", "");
	$("#form2 #btnPackage1").attr("item_nm", "");
	
	$("#form2 #btnPackage2").attr("item_cd", "");
	$("#form2 #btnPackage2").attr("item_nm", "");					
	
	$("#form2 #btnPackage3").attr("item_cd", "");
	$("#form2 #btnPackage3").attr("item_nm", "");
	
	$("#form2 #btnPackage1").removeClass("gray");
	$("#form2 #btnPackage2").removeClass("gray");
	$("#form2 #btnPackage3").removeClass("gray");
	
	$("#form2 #btnPackage1").addClass("blue");
	$("#form2 #btnPackage2").addClass("blue");
	$("#form2 #btnPackage3").addClass("blue");
	
	$("#form2 #img_package1").html("");
	$("#form2 #img_package2").html("");
	$("#form2 #img_package3").html("");
	
	calc();
}

function changeChup() {							
	var value = $('#form3 #info1 option:selected').text();
	console.log("value : " + value);
	
	value = value.replace(/[^0-9]/g,'');	
	value = parseInt(value) * 3;	
	
	$("#form3 #info2").html(NumberFormat(value));
}