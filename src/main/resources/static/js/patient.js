$(function() {		
	$('#btnAddPatient').click(function(e) {
		console.log("btnAddPatient");
		showPopup(700, 850, "/patient/write");
	});
	
	$('#searchPatient').keyup(function(e) {
        if(e.keyCode == 13) {
        	var url = "/common/search_patient?searchText=" + GetValue("searchPatient");
        	console.log("url : " + url);
        	
        	$("#search_patient").attr("src", url);
        	
        	$("#patient_tab2").trigger("click");
        }
    });		
});
		
function goPage() {
	var url = "/common/search_patient?searchText=" + GetValue("searchText");
	console.log("url : " + url);
	
	$("#search_patient").attr("src", url);
}

function setSearchPatientText(searchText) {
	console.log("setSearchPatientText()");
	$("#searchPatient").val(searchText);
}

function setPatient(patient_seq, name, chart_no, sex_nm, birth, recipe_dt, mobile, zipcode, address, address2, remark) {
	console.log("setPatient()");
	$("#name").val(name);
	$("#patient_seq").val(patient_seq);

	$("#patient_name").html(name);
	$("#patient_chart_no").html(chart_no);

	var sex_age = sex_nm;
	if (birth) {
		sex_age += "/" + calcAge(birth);
	}
	$("#patient_sex").html(sex_age);

	$("#patient_recipe_dt").html(recipe_dt);
	$("#patient_mobile").html(mobile);
	$("#patient_address").html(
			"(" + zipcode + ")" + address + " " + address2);
	$("#patient_remark").html(remark);

	$("#patient_tab1").trigger("click");
}